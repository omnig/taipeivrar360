<?php

// 使用 PHP Script 自動部署 git 項目

#在服务器上
// .在專案下輸入

// sudo chown -R www-data:www-data sshm_test/

// git remote set-url origin https://omnig:omnig2015@bitbucket.org/omniguider/sshm.git

$target = __DIR__; // Server上 web 目錄
$wwwUser = 'www-data';
$wwwGroup = 'www-data';

$cmds = array(
    "cd $target ",
    "cd /var/www/vrar360_web_front",
    "git pull",
    "chown -R {$wwwUser}:{$wwwGroup} $target/",
);
foreach ($cmds as $cmd) {
        if($cmd=="git pull"){
   	    	$text = system($cmd);
   	    	post_web_hook_to_slack($text,"桃園[前台]",$target);
   	    }else{
   	    	system($cmd);
   	    }
}
exit;


function post_web_hook_to_slack($text="",$project="default",$host="default"){

	$webhook_url = "https://hooks.slack.com/services/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";
	$json_data = [
	    "text" => "[".$project."-".$host."]".$text
	];

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $webhook_url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type:application/json']);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_data));
	curl_exec($ch);
	curl_close($ch);
}