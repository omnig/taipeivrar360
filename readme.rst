###################
初始設定
###################

查看phpinfo

1.設定最大上傳限制(upload_max_filesize)

PHP=>
mac : /Applications/MAMP/bin/php/php5.5.38/conf/php.ini
ubuntu : /etc/php5/apache2/php.ini

#file_uploads = On
#memory_limit = 320M
#upload_max_filesize = 320M
#post_max_size = 320M
#max_input_time = 60
#max_execution_time = 300


2.設定Composser

安裝
http://www.arthurtoday.com/2013/01/ubuntu-install-php-composer.html


Composser install

(1)修改 application/config/config.php
$config['composer_autoload'] = 'vendor/autoload.php';


###################
設定資料庫-PHP-APACHE檔案上傳最大限制
###################

＊mysql 上傳檔案大小限制
進入 mysql 去設定 GLOBAL 參數
# mysql -u root -p
mysql> set global max_allowed_packet = 300000000 ;

(300MB)


＊PHP 記憶體 大小配置
在程式前加
ini_set("memory_limit","2048M");

或是在php.ini 設定


資料庫設定
max_allowed_packet
> set global max_allowed_packet=335544320;

查看最大上傳包檔
show variables like 'max_allowed_packet';


###################
已建立API
###################
client http://localhost/vrar360/vrphoto
1.上傳Photo
輸入參數 (form-data method) :
(1) myfile : 輸入檔案

URL : http://localhost/vrar360/api/upload_video
結果 :
error => 返回錯誤訊息
succeuss => 返回成功訊息

2.上傳video
輸入參數 (form-data method) :
(1) myfile : 輸入檔案

URL : http://localhost/vrar360/api/upload_photo
結果 :
error => 返回錯誤訊息
succeuss => 返回成功訊息




##############
後台規劃
#############

1.首頁管理 - fa fa-codepen
			(1) 橫幅管理 fa fa-photo (alias)
			(2) 主題區管理   fa fa-map-marker

2.主題管理  fa fa-edit


3.VR360管理-   fa fa-fire 
			(1)民眾360照片審核 fa fa-photo
			(2)民眾360影片審核 fa fa-film
			(3)局處VR照片上傳 icon-user
			(4)局處VR影片上傳 icon-user

4.AR管理  - fa fa-puzzle-piece 
			(1)民眾AR審核 fa fa-cubes
			(2)局處AR上傳 icon-user

5.會員管理 fa fa-group

6.任務管理 - (1)任務管理 fa fa-newspaper-o
			(2)獎勵管理 fa fa-paper-plane-o

7.勘誤回報管理 fa  fa-file-text

8.平台設定 - (1)服務條款
			(2)隱私權政策

9.權限設定 - (1) 系統管理員管理
			(2) 局處管理員管理
			(3) 用戶群組

10. 備份檔案管理 fa fa-magic

11.局處管理 fa fa-sitemap

12.關鍵字管理









1.預設
http://localhost/vrar360/api/get_search_index
2.依局處ID 搜尋
http://localhost/vrar360/api/get_search_index?d=1
3.依關鍵字 搜尋
http://localhost/vrar360/api/get_search_index?k=關鍵
4.依局處ID 關鍵字 搜尋
http://localhost/vrar360/api/get_search_index?d=1&k=關鍵

4.增加排序條件
可在URL 直接帶
?dapartment_id=13&order=hot //熱門
?dapartment_id=13&order=new //最新



#################
測試市民卡
#################


curl -X POST https://163.29.253.66/web-cardapi/rest/V1/query/vrweb --insecure

curl -X POST https://163.29.253.65/web-cardapi/rest/V1/query/vrweb --insecure




###############
Admin 群組管理
###############

$adm_group =  $this->session->login_admin->adm_group;

$where_array = array(
    'ag_group' => $adm_group
);
$department_list = $this->Common_model->get_db("tp_admin_group", $where_array);
$data["department_list"] = $department_list;



$where_array = array(
	't_enabled'=>'Y',
	't_del'=>'N'
);
$topic_list = $this->Common_model->get_db("tp_topic", $where_array);
$data["topic_list"] = $topic_list;









$adm_group =  $this->session->login_admin->adm_group;


$where_array = array(
    'ag_group' => $adm_group,
    't_del' =>'N',
    'tdar_del'=>'N');

時間設定
$vr_ar_result[$key]['interactive_time'] = date('d M Y H:i',strtotime($value->collection_timestamp))." UTC";



######################################新增SQL################
1.任務新增單位
ALTER TABLE `tp_mission` ADD `dep_id` INT(11) NOT NULL AFTER `rw_id`;

2.獎勵新增單位
ALTER TABLE `tp_reward_group` ADD `dep_id` INT(11) NOT NULL AFTER `rw_id`;
