-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- 主機： localhost:3306
-- 產生時間： 2019 年 09 月 16 日 05:09
-- 伺服器版本： 5.7.25-log
-- PHP 版本： 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- 資料庫： `nmns`
--

-- --------------------------------------------------------

--
-- 資料表結構 `n_geofence`
--

CREATE TABLE `n_geofence` (
  `g_id` int(11) NOT NULL,
  `g_title` varchar(128) NOT NULL COMMENT '標題',
  `g_lat` varchar(64) NOT NULL COMMENT '經度',
  `g_lng` varchar(64) NOT NULL COMMENT '緯度',
  `g_range` varchar(6) NOT NULL COMMENT '範圍(公尺)',
  `g_mail_enabled` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT '信件通知管理者',
  `g_create_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立時間',
  `g_enabled` enum('Y','N') NOT NULL DEFAULT 'Y' COMMENT '是否啟用',
  `g_del` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT '是否刪除'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='geofence資訊';

-- --------------------------------------------------------

--
-- 資料表結構 `n_geofence_record`
--

CREATE TABLE `n_geofence_record` (
  `gr_id` int(11) NOT NULL,
  `gt_id` int(11) NOT NULL COMMENT 'geofence_trigger ID',
  `u_id` int(11) DEFAULT NULL COMMENT 'user id',
  `gr_device_id` varchar(256) NOT NULL COMMENT '裝置ID',
  `gr_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '領取時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='geofence領取紀錄';

-- --------------------------------------------------------

--
-- 資料表結構 `n_geofence_relation`
--

CREATE TABLE `n_geofence_relation` (
  `gr_id` int(11) NOT NULL,
  `g_id` int(11) NOT NULL COMMENT 'geofence id',
  `gt_id` int(11) NOT NULL COMMENT 'geofence_trigger id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='geofence關連表';

-- --------------------------------------------------------

--
-- 資料表結構 `n_geofence_trigger`
--

CREATE TABLE `n_geofence_trigger` (
  `gt_id` int(11) NOT NULL,
  `gt_title` varchar(128) NOT NULL COMMENT '標題',
  `gt_title_en` varchar(128) DEFAULT NULL COMMENT '英文標題',
  `gt_title_jp` varchar(128) DEFAULT NULL COMMENT '日文標題',
  `gt_description` varchar(256) DEFAULT NULL COMMENT '描述',
  `gt_description_en` varchar(256) DEFAULT NULL COMMENT '英文描述',
  `gt_description_jp` varchar(256) DEFAULT NULL COMMENT '日文描述',
  `gt_image` varchar(64) DEFAULT NULL COMMENT '圖片',
  `gt_target` enum('ALL','MEMBER') NOT NULL DEFAULT 'ALL' COMMENT '發送對象',
  `gt_start_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '開始時間',
  `gt_end_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '結束時間',
  `gt_create_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立時間',
  `gt_timelimit_push` enum('Y','N') NOT NULL DEFAULT 'Y' COMMENT '是否指定時間內推播',
  `gt_mail_enabled` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT '信件通知管理者',
  `gt_enabled` enum('Y','N') NOT NULL DEFAULT 'Y' COMMENT '是否啟用',
  `gt_del` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT '是否刪除'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='geofence推播資訊';

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `n_geofence`
--
ALTER TABLE `n_geofence`
  ADD PRIMARY KEY (`g_id`);

--
-- 資料表索引 `n_geofence_record`
--
ALTER TABLE `n_geofence_record`
  ADD PRIMARY KEY (`gr_id`),
  ADD UNIQUE KEY `gr_id` (`gr_id`),
  ADD KEY `u_id` (`u_id`),
  ADD KEY `gt_id` (`gt_id`);

--
-- 資料表索引 `n_geofence_relation`
--
ALTER TABLE `n_geofence_relation`
  ADD PRIMARY KEY (`gr_id`),
  ADD KEY `g_id` (`g_id`),
  ADD KEY `gt_id` (`gt_id`);

--
-- 資料表索引 `n_geofence_trigger`
--
ALTER TABLE `n_geofence_trigger`
  ADD PRIMARY KEY (`gt_id`);

--
-- 在傾印的資料表使用自動增長(AUTO_INCREMENT)
--

--
-- 使用資料表自動增長(AUTO_INCREMENT) `n_geofence`
--
ALTER TABLE `n_geofence`
  MODIFY `g_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動增長(AUTO_INCREMENT) `n_geofence_record`
--
ALTER TABLE `n_geofence_record`
  MODIFY `gr_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動增長(AUTO_INCREMENT) `n_geofence_relation`
--
ALTER TABLE `n_geofence_relation`
  MODIFY `gr_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動增長(AUTO_INCREMENT) `n_geofence_trigger`
--
ALTER TABLE `n_geofence_trigger`
  MODIFY `gt_id` int(11) NOT NULL AUTO_INCREMENT;


RENAME TABLE `vrar360_2`.`n_geofence` TO `vrar360_2`.`ty_geofence`;
RENAME TABLE `vrar360_2`.`n_geofence_record` TO `vrar360_2`.`ty_geofence_record`;
RENAME TABLE `vrar360_2`.`n_geofence_relation` TO `vrar360_2`.`ty_geofence_relation`;
RENAME TABLE `vrar360_2`.`n_geofence_trigger` TO `vrar360_2`.`ty_geofence_trigger`;


ALTER TABLE `ty_config` ADD `c_index_text` TEXT NOT NULL COMMENT '首頁說明文字' AFTER `c_web_lng`;
ALTER TABLE `ty_config` CHANGE `c_index_text` `c_index_text` CHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '預設首頁說明文字' COMMENT '首頁說明文字';


--------------------------好友位置---------------------------------------


--
-- 資料表結構 `n_group`
--

CREATE TABLE `n_group` (
  `g_id` int(11) NOT NULL,
  `g_title` varchar(128) DEFAULT NULL COMMENT '群組標題',
  `g_key` varchar(64) NOT NULL COMMENT '群組key',
  `g_remind_timestamp` timestamp NULL DEFAULT NULL COMMENT '提醒時間',
  `g_collection_timestamp` timestamp NULL DEFAULT NULL COMMENT '集合時間',
  `ap_id` int(11) DEFAULT NULL COMMENT '集合地點',
  `u_id` int(11) DEFAULT NULL COMMENT '管理者ID',
  `g_device_id` varchar(256) DEFAULT NULL COMMENT '管理者裝置ID',
  `g_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立時間',
  `g_end_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '結束時間',
  `g_enabled` enum('Y','N','C') NOT NULL DEFAULT 'Y' COMMENT '是否啟用',
  `g_del` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT '是否刪除'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='群組資料';

-- --------------------------------------------------------

--
-- 資料表結構 `n_group_relation`
--

CREATE TABLE `n_group_relation` (
  `gr_id` int(11) NOT NULL,
  `g_id` int(11) NOT NULL COMMENT '群組ID',
  `u_id` int(11) DEFAULT NULL COMMENT '使用者ID',
  `gr_name` varchar(64) DEFAULT NULL COMMENT '自訂名稱',
  `gr_device_id` varchar(128) DEFAULT NULL COMMENT '裝置ID',
  `gr_tel` varchar(12) DEFAULT NULL COMMENT '聯絡電話',
  `gr_role` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0:管理者 1:使用者',
  `gr_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '加入時間',
  `gr_del` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT '是否刪除'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='群組關聯資料';

-- --------------------------------------------------------

--
-- 資料表結構 `n_user_last_location`
--

CREATE TABLE `n_user_last_location` (
  `ul_id` int(11) NOT NULL,
  `u_id` int(11) DEFAULT NULL COMMENT '使用者ID',
  `ul_device_id` varchar(128) NOT NULL COMMENT '裝置ID',
  `ul_lat` varchar(64) NOT NULL COMMENT '使用者經度',
  `ul_lng` varchar(64) NOT NULL COMMENT '使用者緯度',
  `af_plan_id` varchar(128) DEFAULT NULL COMMENT '室內地圖ID',
  `ul_timestamp` timestamp NULL DEFAULT NULL COMMENT '所在時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='使用者最新位置紀錄';

-- --------------------------------------------------------

--
-- 資料表結構 `n_user_location`
--

CREATE TABLE `n_user_location` (
  `ul_id` int(11) NOT NULL,
  `u_id` int(11) DEFAULT NULL COMMENT '使用者ID',
  `ul_device_id` varchar(128) NOT NULL COMMENT '裝置ID',
  `ul_lat` varchar(64) NOT NULL COMMENT '使用者經度',
  `ul_lng` varchar(64) NOT NULL COMMENT '使用者緯度',
  `af_plan_id` varchar(128) DEFAULT NULL COMMENT '室內地圖ID',
  `ul_timestamp` timestamp NULL DEFAULT NULL COMMENT '所在時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='使用者位置紀錄';

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `n_group`
--
ALTER TABLE `n_group`
  ADD PRIMARY KEY (`g_id`),
  ADD KEY `u_id` (`u_id`);

--
-- 資料表索引 `n_group_relation`
--
ALTER TABLE `n_group_relation`
  ADD PRIMARY KEY (`gr_id`),
  ADD KEY `g_id` (`g_id`),
  ADD KEY `u_id` (`u_id`);

--
-- 資料表索引 `n_user_last_location`
--
ALTER TABLE `n_user_last_location`
  ADD PRIMARY KEY (`ul_id`),
  ADD KEY `u_id` (`u_id`),
  ADD KEY `af_plan_id` (`af_plan_id`),
  ADD KEY `ul_timestamp` (`ul_timestamp`),
  ADD KEY `ul_device_id` (`ul_device_id`);

--
-- 資料表索引 `n_user_location`
--
ALTER TABLE `n_user_location`
  ADD PRIMARY KEY (`ul_id`),
  ADD KEY `u_id` (`u_id`),
  ADD KEY `af_plan_id` (`af_plan_id`),
  ADD KEY `ul_timestamp` (`ul_timestamp`),
  ADD KEY `ul_device_id` (`ul_device_id`);

--
-- 在傾印的資料表使用自動增長(AUTO_INCREMENT)
--

--
-- 使用資料表自動增長(AUTO_INCREMENT) `n_group`
--
ALTER TABLE `n_group`
  MODIFY `g_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動增長(AUTO_INCREMENT) `n_group_relation`
--
ALTER TABLE `n_group_relation`
  MODIFY `gr_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動增長(AUTO_INCREMENT) `n_user_last_location`
--
ALTER TABLE `n_user_last_location`
  MODIFY `ul_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動增長(AUTO_INCREMENT) `n_user_location`
--
ALTER TABLE `n_user_location`
  MODIFY `ul_id` int(11) NOT NULL AUTO_INCREMENT;


RENAME TABLE `vrar360_2`.`n_group` TO `vrar360_2`.`ty_group`;
RENAME TABLE `vrar360_2`.`n_group_relation` TO `vrar360_2`.`ty_group_relation`;
RENAME TABLE `vrar360_2`.`n_user_last_location` TO `vrar360_2`.`ty_user_last_location`;
RENAME TABLE `vrar360_2`.`n_user_location` TO `vrar360_2`.`ty_user_location`;

