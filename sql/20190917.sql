
--
-- 資料庫： `alishan`
--

-- --------------------------------------------------------

--
-- 資料表結構 `ty_viewpoints`
--

CREATE TABLE `ty_viewpoints` (
  `v_id` int(11) NOT NULL,
  `v_dataid` varchar(32) DEFAULT NULL COMMENT '資料ID',
  `v_name` varchar(128) NOT NULL COMMENT '名稱',
  `v_desc` text NOT NULL COMMENT '描述',
  `v_name_en` varchar(128) NOT NULL COMMENT '英文名稱',
  `v_desc_en` text NOT NULL COMMENT '英文描述',
  `a_id` int(6) NOT NULL COMMENT '郵遞區號',
  `v_address` varchar(256) DEFAULT NULL COMMENT '地址',
  `v_address_en` varchar(256) DEFAULT NULL COMMENT '英文地址名稱',
  `v_class` int(6) DEFAULT NULL COMMENT '類別',
  `v_level` int(6) DEFAULT NULL COMMENT '等級',
  `v_web` varchar(128) DEFAULT NULL COMMENT '網站',
  `v_tel` varchar(16) DEFAULT NULL COMMENT '電話',
  `v_image` varchar(128) DEFAULT NULL COMMENT '圖片網址',
  `v_ticketinfo` varchar(128) DEFAULT NULL COMMENT '票價資訊',
  `v_opentime` varchar(128) DEFAULT NULL COMMENT '開放時間',
  `v_travellinginfo` varchar(256) DEFAULT NULL COMMENT '旅遊資訊',
  `v_lat` float NOT NULL COMMENT '坐標LAT',
  `v_lng` float NOT NULL COMMENT '坐標LNG',
  `v_remarks` varchar(128) DEFAULT NULL COMMENT '備註',
  `v_changetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '資料更新時間',
  `v_enabled` enum('Y','N') DEFAULT 'Y' COMMENT '是否啟用',
  `v_del` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT '是否刪除',
  `v_api_from` varchar(32) NOT NULL COMMENT '資料來源',
  `v_update_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='景點列表';

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `ty_viewpoints`
--
ALTER TABLE `ty_viewpoints`
  ADD PRIMARY KEY (`v_id`),
  ADD KEY `a_zip` (`a_id`);

--
-- 在傾印的資料表使用自動增長(AUTO_INCREMENT)
--

--
-- 使用資料表自動增長(AUTO_INCREMENT) `ty_viewpoints`
--
ALTER TABLE `ty_viewpoints`
  MODIFY `v_id` int(11) NOT NULL AUTO_INCREMENT;



--
-- 資料表結構 `ty_image_generate`
--

CREATE TABLE `ty_image_generate` (
  `i_id` int(11) NOT NULL,
  `i_title` varchar(32) DEFAULT NULL COMMENT '圖片簡述',
  `i_height` int(6) NOT NULL COMMENT '圖片長',
  `i_width` int(6) NOT NULL COMMENT '圖片寬',
  `i_type` set('FIT_INNER','FIT_INNER_NO_ZOOM_IN','FIT_OUTER') NOT NULL COMMENT '縮放模式',
  `i_link` varchar(128) NOT NULL COMMENT '圖片連結',
  `i_create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '圖片建立時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='圖片上傳表';

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `ty_image_generate`
--
ALTER TABLE `ty_image_generate`
  ADD UNIQUE KEY `i_id` (`i_id`);

--
-- 在傾印的資料表使用自動增長(AUTO_INCREMENT)
--

--
-- 使用資料表自動增長(AUTO_INCREMENT) `ty_image_generate`
--
ALTER TABLE `ty_image_generate`
  MODIFY `i_id` int(11) NOT NULL AUTO_INCREMENT;