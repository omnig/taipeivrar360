ALTER TABLE `ty_geofence_trigger` ADD `g_topic` INT(11) NOT NULL COMMENT '前往主題ID' AFTER `gt_target`,
ADD `g_go_point_type` VARCHAR(5) NOT NULL COMMENT '前往類型類別' AFTER `g_topic`,
ADD `g_go_point_id` INT(11) NOT NULL COMMENT '前往類型ID' AFTER `g_go_point_type`,
ADD `gt_lat` VARCHAR(64) NOT NULL COMMENT '前往位置緯度' AFTER `g_go_point_id`,
ADD `gt_lng` VARCHAR(64) NOT NULL COMMENT '前往位置經度' AFTER `gt_lat`;


ALTER TABLE `ty_geofence_trigger` CHANGE `g_topic` `gt_topic` INT(11) NOT NULL COMMENT '前往主題ID';
ALTER TABLE `ty_geofence_trigger` CHANGE `g_go_point_type` `gt_go_point_type` VARCHAR(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '前往類型類別';
ALTER TABLE `ty_geofence_trigger` CHANGE `g_go_point_id` `gt_go_point_id` INT(11) NOT NULL COMMENT '前往類型ID';


ALTER TABLE `ty_nine_grid` ADD `ng_describe` VARCHAR(25) NULL COMMENT '關卡描述' AFTER `ng_title`;