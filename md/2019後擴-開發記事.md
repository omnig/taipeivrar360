PTT-Pages7.
APP首頁增加各功能操作說明頁-新增API
API 名稱 : get_index_description(後台API清單有可測試)



PTT-Pages10.
Geofence推播 - 新增API
API 名稱 : get_geofence(後台API清單有可測試)



PTT-Pages13.
朋友位置 - 新增API
API 名稱1 : add_group(建立群組)
API 名稱2 : join_group(加入群組)
API 名稱3 : update_group(更新群組資訊)
API 名稱4 : leave_group(離開群組)
API 名稱5 : kick_by_group_admin(指定踢出群組)
API 名稱6 : send_user_location(回傳好友位置以及更新自身位置)




PTT-Pages19
AR內容互動 - 修改API
API 名稱 : get_pattern_content(於POST MAN 可以測試)
在API 回傳欄位有 tdar_interactive_text 與 tdar_interactive_url
若tdar_interactive_url有網址則前往網頁
若tdar_interactive_text 有文字則顯示文字"不"跳至連結網址
若兩項欄位皆為null則沒有互動文字
(建議在頁面顯示文字:點擊模型會有文字互動唷or點擊模型查看更多資訊以利呈現給使用者了解)




PTT-Pages21
鄰近主題與熱門景點推播 - 
修改API -get_index_wt (電子看板API-在輸入參數type 填 view 即可顯示景點列表)
修改API -get_guider_all (返回key 新增 view景點資料)
新增API -get_guider_view (與get_guider_topic、get_guider_poi、get_guider_all輸入參數一樣)


PTT-Pages23
任務活動 - 
修改API - get_nine_grid 
九宮格元素變為九個，另外新增ng_describe(關卡說明)欄位


PTT-Pages27、28
智能化推播(AR導航模式)
修改API - get_geofence
新增3個欄位
gt_lat: "25.006045569842914"
gt_lng: "121.04995965957164"
gt_point_info: "[2018農業博覽會]-景點-美食專區出入口"
