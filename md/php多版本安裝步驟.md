
步驟一 ：將要使用的PHP 版本以及相關套件安裝完成

sudo su
add-apt-repository ppa:ondrej/php
apt-get update
apt-get install libapache2-mod-fastcgi  php7.2-fpm libapache2-mod-fastcgi php7.2-fpm php7.2 php7.2-dev php7.2-mbstring php7.2-mysql php7.2-zip php7.2-gd php7.2-xml php7.2-curl php7.2-intl php7.2-json  php-dev libmcrypt-dev php-pear
apt-get install php7.2-bcmath php7.2-soap
apt install libapache2-mod-fcgid 
apt install libapache2-mod-fastcgi


sudo update-alternatives --set php /usr/bin/php7.2
sudo pecl channel-update pecl.php.net
sudo pecl install mcrypt-1.0.1

sudo a2enmod rewrite
sudo a2enmod headers

sudo a2enmod actions alias proxy_fcgi fastcgi
sudo systemctl restart apache2

apt-get install libcouchbase-dev libcouchbase2-bin build-essential -y
pecl install couchbase 

cp /etc/php/7.2/mods-available/json.ini /etc/php/7.2/mods-available/mcrypt.ini
cp /etc/php/7.2/mods-available/json.ini /etc/php/7.2/mods-available/couchbase.ini

ln -s /etc/php/7.2/mods-available/mcrypt.ini /etc/php/7.2/fpm/conf.d/20-mcrypt.ini
ln -s /etc/php/7.2/mods-available/couchbase.ini /etc/php/7.2/fpm/conf.d/99-couchbase.ini


sudo a2enmod actions 
sudo a2enmod fastcgi


service php7.2-fpm status   




步驟二 ：設定Apache 虛擬站台config
<VirtualHost *:80>
...
<IfModule mod_fastcgi.c>
    <FilesMatch ".+\.ph(p[345]?|t|tml)$">
    	 SetHandler "proxy:unix:/run/php/php7.2-fpm.sock|fcgi://localhost"
    </FilesMatch>
</IfModule>
...
</VirtualHost>


Now you can add the  <FilesMatch> directive to .htaccess

PHP 5.6 Example
.htaccess
<FilesMatch \.php$>
    # Apache 2.4.10+ can proxy to unix socket
    SetHandler "proxy:unix:/var/run/php/php5.6-fpm.sock|fcgi://localhost/"
</FilesMatch>
PHP 7.2 Example
.htaccess
<FilesMatch \.php$>
    # Apache 2.4.10+ can proxy to unix socket
    CGIPassAuth on
    SetHandler "proxy:unix:/var/run/php/php7.2-fpm.sock|fcgi://localhost/"
</FilesMatch>

