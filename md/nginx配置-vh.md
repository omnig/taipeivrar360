server {
    listen       80;
    listen       443 ssl http2;
    server_name vr-api.tycg.gov.tw;

    ssl_certificate /etc/apache2/cert2019/server-chain.crt;
    ssl_certificate_key /etc/apache2/cert2019/server_no.key;
    ssl_session_cache shared:SSL:20m;
    ssl_session_timeout 60m;

    charset utf-8;
    access_log  /var/log/nginx/vr-api.access.log;
    error_log   /var/log/nginx/vr-api.error.log;

    if ($scheme = http) {
        return 301 https://$server_name$request_uri;
    }

    # Deny for accessing .htaccess files for Nginx
    location ~ /\.ht {
        deny all;
    }

    # Deny for accessing codes
    location ~* ^/(application|system|tests|sql|scripts)/ {
        return 404;
    }
    location ~* ^/.(key|git|md|gitignore|gitmodules)/ {
        return 404;
    }
    location ~* \.(key|md|sql)?$ {
        return 404;
    }

    root /var/www/vrar360_api;
    #autoindex on;
    index index.php index.html index.htm index.nginx-debian.html;

    location @anyplace_api {
        rewrite ^ /anyplace_api$request_uri last;
    }

    location @anyplace_api {
        rewrite ^ /anyplace_api$request_uri last;
    }
    location /anyplace_api {
        try_files $uri $uri/ @rewrite;
    }
    location @rewrite {
        rewrite ^/(.*)$ /anyplace_api/index.php?url=$1 last;
    }

    location / {
        try_files $uri $uri/ /index.php?/$request_uri;
    }

    # set expiration of assets to MAX for caching
    location ~* \.(ico|css|js|gif|jpe?g|png)(\?[0-9]+)?$ {
        root /var/www/vrar360_api/;
        expires max;
        log_not_found off;
        try_files $uri $uri/ /index.php?/$request_uri;
    }

    location ~* \.php$ {
        if (!-e $request_filename) {
              return 404;
        }
        fastcgi_index index.php;
        fastcgi_intercept_errors on;
        fastcgi_split_path_info ^(.+\.php)(.*)$;
        include fastcgi_params;
        fastcgi_pass 127.0.0.1:9000;
        fastcgi_param SCRIPT_FILENAME /var/www/vrar360_api/$fastcgi_script_name;


        fastcgi_param  QUERY_STRING       $query_string;
        fastcgi_param  REQUEST_METHOD     $request_method;
        fastcgi_param  CONTENT_TYPE       $content_type;
        fastcgi_param  CONTENT_LENGTH     $content_length;

        fastcgi_param  REQUEST_URI        $request_uri;
        fastcgi_param  DOCUMENT_URI       $document_uri;
        fastcgi_param  DOCUMENT_ROOT      $document_root;
        fastcgi_param  SERVER_PROTOCOL    $server_protocol;

        fastcgi_param  GATEWAY_INTERFACE  CGI/1.1;
        fastcgi_param  SERVER_SOFTWARE    nginx/$nginx_version;

        fastcgi_param  REMOTE_ADDR        $remote_addr;
        fastcgi_param  REMOTE_PORT        $remote_port;
        fastcgi_param  SERVER_ADDR        $server_addr;
        fastcgi_param  SERVER_PORT        $server_port;
        fastcgi_param  SERVER_NAME        $server_name;
    }
}