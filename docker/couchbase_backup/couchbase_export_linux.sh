#!/bin/bash
Couchbase_HOST="127.0.0.1"
USER_NAME="anyplace"
USER_PASSWORD="123456"
BACKUPS_DIR="/var/data/"


rm -r /var/data/backup-couchbase-view backup-couchbase-view.tar

cd $BACKUPS_DIR
/opt/couchbase/bin/cbbackup http://localhost:8091 /var/data/backup-couchbase-view -u $USER_NAME -p $USER_PASSWORD

tar cvf backup-couchbase-view.tar backup-couchbase-view
