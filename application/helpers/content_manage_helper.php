<?php

/*
 * 取得內容管理資料
 * 以cm_key為key，回傳相對內容
 */

function get_content($cm_keys = array()) {
    $ci = & get_instance();

    $where_array = array(
        'cm_key' => $cm_keys
    );
    $content_manage = $ci->Common_model->get_db('content_manage', $where_array);

    $ret = array();

    if ($content_manage) {
        foreach ($content_manage as $row) {
            $ret[$row->cm_key] = $row->cm_text;
        }
    }

    return $ret;
}
