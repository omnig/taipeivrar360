<?php

/*
 * 將html編輯器中的純文字取出
 */

function get_pure_text($str) {
    $str = strip_tags($str);
    $str = str_replace("&nbsp;", "", $str);
    $str = str_replace("\r", "", $str);
    $str = str_replace("\n", "", $str);
    $str = str_replace(" ", "", $str);

    return $str;
}

function _confirm_redirect($msg, $url) {
    echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">";
    echo "<script nonce=\"cm1vaw==\">";
    echo "if (confirm('$msg')) {";
    echo "location.href='$url';";
    echo "}";
    echo "</script>";
    exit();
}

function _alert_redirect($msg, $url) {
    echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">";
    echo "<script nonce=\"cm1vaw==\">";
    echo "alert('" . $msg . "');";
    echo "location.href='$url';";
    echo "</script>";
    exit();
}

function _alert($msg) {
    echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">";
    echo "<script nonce=\"cm1vaw==\">";
    echo "alert('$msg');";
    echo "window.history.back();";
    echo "</script>";
    exit();
}

function _alert_notice($msg) {
    echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">";
    echo "<script nonce=\"cm1vaw==\">";
    echo "alert('$msg');";
    echo "window.history.back();";
    echo "</script>";
}

function limit_string($str, $max_bytes, $tail = '...') {
    $ret = $str;
    if (mb_strlen($str) > $max_bytes) {
        $ret = mb_substr($str, 0, $max_bytes - 3) . $tail;
    }
    return $ret;
}

function price_number_format($price) {

    if ($price - (int) $price > 0) {
        return number_format($price, 2);
    } else {
        return number_format($price, 0);
    }
}

/*
 * 設定顯示訊息，跳轉網頁即會顯示
 */

function show_message($title = '', $sub_title = '', $description = '') {
    $ci = & get_instance();

    $msg = array(
        "title" => $title,
        "sub_title" => $sub_title,
        "description" => $description
    );

    $ci->session->set_userdata('show_message', $msg);
    $ci->show_message = $msg;
}

/*
 * 經緯度轉距離
 */

function get_dist($lng1, $lat1, $lng2, $lat2) {
    $r = 6371.137;
    $dlat = deg2rad($lat2 - $lat1);
    $dlng = deg2rad($lng2 - $lng1);

    $a = pow(sin($dlat / 2), 2) +
            cos(deg2rad((double) $lat1)) *
            cos(deg2rad((double) $lat2)) *
            pow(sin($dlng / 2), 2);

    $c = 2 * atan2(sqrt($a), sqrt(1 - $a));

    return round($r * $c * 1000);
}

/*
 * 距離轉經緯度差距
 */

function meter_to_deg_diff($dist) {
    return $dist * 0.00000900900901;
}

/*
 * 取得完整網站url(含query string)
 */

function current_full_url() {
    $CI = & get_instance();

    $url = $CI->config->item("server_base_url") . uri_string();
    return $CI->input->server('QUERY_STRING') ? $url . '?' . $CI->input->server('QUERY_STRING') : $url;
}

/*
 * 取得經緯度
 */

function address_to_latlon($address) {
    $base_url = "https://maps.googleapis.com/maps/api/geocode/json?&sensor=false&language=zh-tw&address=";
    $request_url = $base_url . urlencode($address);
    $jsonString = curl_query($request_url);

    $result = json_decode($jsonString);

    if ($result->status != "OK") {
        $ret = array(
            "lat" => 0,
            "lon" => 0
        );
        return $ret;
    }

    $ret = array(
        "lat" => $result->results[0]->geometry->location->lat,
        "lon" => $result->results[0]->geometry->location->lng
    );

    return $ret;
}

/*
 * 以pano_id取得完整網址
 */

function get_pano_url($pano_id) {
    $CI = & get_instance();

    return "http://{$pano_id}.{$CI->config->item("pano3d_base_domain")}" . "{$CI->config->item("pano3d_base_url")}";
}

/*
 * 以網址取得pano_id
 */

function get_pano_id($url) {
    $CI = & get_instance();

    $pos = strpos($url, $CI->config->item("pano3d_base_domain"));

    if ($pos === false) {
        return $url;
    }

    if ($pos == 0) {
        return "";
    }

    $pano_id = substr($url, 0, $pos - 1);

    return $pano_id;
}

/*
 * 取得遠端IP
 */

function get_remote_addr() {
    $ip = isset($_SERVER["HTTP_X_FORWARDED_FOR"]) ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];
    return $ip;
}

/*
 * 驗證帳號複雜度
 */

function valid_account($candidate) {
    //密碼長度
    $str_len = strlen($candidate);

    if ($str_len < 6 || $str_len > 12) {
        return FALSE;
    }

    //檢查密碼必要元素
    $reg = array(
        //'/[A-Z]/', //大寫
        '/[a-z]/', //小寫
        //'/[!@#$%^&*()\-_=+{};:,<.>]/', // 特殊符號
        '/[0-9]/'  //數字
    );

    $total_valid_bytes = 0;
    foreach ($reg as $row) {
        $valid_bytes = preg_match_all($row, $candidate);
        if ($valid_bytes < 1) {
            return FALSE;
        }

        $total_valid_bytes += $valid_bytes;
    }

    //確認是否所有字元都是合法字元
    if ($str_len != $total_valid_bytes) {
        return FALSE;
    }

    return TRUE;
}

/*
 * 驗證密碼複雜度
 */

function valid_pass($candidate) {
    //密碼長度
    $str_len = strlen($candidate);

    if ($str_len < 6 || $str_len > 16) {
        return FALSE;
    }

    //檢查密碼必要元素
    $reg = array(
        //'/[A-Z]/', //大寫
        '/[a-z]/', //小寫
        //'/[!@#$%^&*()\-_=+{};:,<.>]/', // 特殊符號
        '/[0-9]/'  //數字
    );

    $total_valid_bytes = 0;
    foreach ($reg as $row) {
        $valid_bytes = preg_match_all($row, $candidate);
        if ($valid_bytes < 1) {
            return FALSE;
        }

        $total_valid_bytes += $valid_bytes;
    }

    //確認是否所有字元都是合法字元
    if ($str_len != $total_valid_bytes) {
        return FALSE;
    }

    return TRUE;
}

/*
 * 驗證輸入字串限定長度
 */

function valid_string_lenth($string="",$limit=10){

    if($limit < mb_strlen($string, "utf-8")){
        return FALSE;
    }else{
        return TRUE;
    }
}

/*
 * 取得活動序號
 */

function random_str($length)
{
    //生成一个包含 大寫英文字母, 小寫英文字母, 數字
    $arr = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
 
    $str = '';
    $arr_len = count($arr);
    for ($i = 0; $i < $length; $i++)
    {
        $rand = mt_rand(0, $arr_len-1);
        $str.=$arr[$rand];
    }
 
    return $str;
}



function mapping_admin_topic($t_id){
    $CI = & get_instance();

    $where_array = array('t_id' => $t_id );

    $topic_list = $CI->Common_model->get_one("topic", $where_array);
    $admin_gooup_id = $topic_list->t_organizer;

    $where_array = array(
        "ag_id" => $admin_gooup_id);


    $admin_list = $CI->Common_model->get_db_join("tp_admin_group as adg", $where_array, "tp_admin as ad" , "adg.ag_group = ad.adm_group","LEFT","adm_id","ASC");

    $admin_who  = $admin_list [0];
    
    return $admin_who ;
}

function mapping_topic_admin($amdin_id){
    $CI = & get_instance();


}


function time_sort_asc($a, $b){

//下面的upload_time是要排序的陣列索引，可以替換成你要排序的東西(例如:english或是math)

    if($a['upload_time'] == $b['upload_time']) return 0;

    return ($a['upload_time'] > $b['upload_time']) ? 1 : -1;

}

function time_sort_desc($a, $b){

//下面的upload_time是要排序的陣列索引，可以替換成你要排序的東西(例如:english或是math)

    if($a['upload_time'] == $b['upload_time']) return 0;

    return ($a['upload_time'] > $b['upload_time']) ? -1 : 1;

}

function tid_return_topic_name($tid){
    $CI = & get_instance();

    $result = $CI->Common_model->get_one_field("topic", "t_name", $where_array = array('t_id' => $tid));

    return $result;
}


function get_tag_name($table ="keyword_department_vr_photo",$uuid_name ="dvp_id" ,$uuid_value=3){
    $CI = & get_instance();
    $keywords_name = array();
    $where_array = array(
        $uuid_name => $uuid_value
    );

    //get_key_word
    $keywords = $CI->Common_model->get_db_join("$table as dvp", $where_array , "keywords as ks" , "dvp.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");

    foreach ($keywords as $keywords_key => $keywords_value) {
        $keywords_name[$keywords_key] = $keywords_value->kwd_name;
    }

    return $keywords_name;

}

function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

/*
用 局處代號尋找局處ID
 */

function adname_to_groupid($adname){
    $CI = & get_instance();
    $where_array = array(
        'ag_group' => $adname
    );
    $topic_list = $CI->Common_model->get_one("tp_admin_group", $where_array);
    return $topic_list->ag_id ;
}



/*
用 局處ID尋找合法局處Topic
 */

function adid_to_topic_name($ad_id){
    $CI = & get_instance();
    $where_array = array(
        'ag_id' => $ad_id
    );
    $topic_list = $CI->Common_model->get_one("tp_admin_group", $where_array);
    return $topic_list->ag_group ;
}


/*
用 局處代號驗證是否為合法權限
 */

function admin_validation($table_name,$adname,$advalue,$field_name,$field_value){
    $CI = & get_instance();
    $where_array = array(
        $adname => adname_to_groupid($advalue),
        $field_name => $field_value
    );
    $resulr_list = $CI->Common_model->get_one($table_name, $where_array);

    // echo json_encode($advalue);
    // exit;
    if($resulr_list==NULL || count($resulr_list)==0){

        _alert("您沒有合法權限");
    }


}



/*
獲取Anyplace poi Query 資訊
 */

function get_anyplace_poi_query($puid="",$lat,$lng){

    $ci = & get_instance();
    $where_array = array(
        "ap_puid" => $puid
    );

    $poi_info = $ci->Common_model->get_one("ap_pois", $where_array);

    //取得相關資料
    $where_array = array(
        'af_id' => $poi_info->af_id
    );
    $row = $ci->Common_model->get_db_join('ap_floors as af', $where_array, 'ap_building as ab', 'ab.ab_id=af.ab_id', 'inner');
    $row = $row[0];

    $where_array = array(
        'ac_id' => $poi_info->ac_id
    );
    $type = $ci->Common_model->get_one_field('ap_category', 'ac_title_en', $where_array);

    //更新Anyplace DB
    $query_info = array(
        'puid' => $puid,
        'buid' => $row->ab_buid,
        'floor_name' => $row->af_name,
        'floor_number' => $row->af_number,
        'name' => $poi_info->ap_name,
        'description' => $poi_info->ap_desc,
        'pois_type' => $type,
        'is_door' => ($poi_info->ap_is_door == 'Y') ? 'true' : 'false',
        'is_building_entrance' => ($poi_info->ap_is_building_entrance == 'Y') ? 'true' : 'false',
        'coordinates_lat' => $lat,
        'coordinates_lon' => $lng
    );

    return $query_info;
}

/*
 * 取得YoutubeId
 */
 
function getYoutubeIdFromUrl($url) {
    $parts = parse_url($url);
    if(isset($parts['query'])){
        parse_str($parts['query'], $qs);
        if(isset($qs['v'])){
            return $qs['v'];
        }else if(isset($qs['vi'])){
            return $qs['vi'];
        }
    }
    if(isset($parts['path'])){
        $path = explode('/', trim($parts['path'], '/'));
        return $path[count($path)-1];
    }
    return false;
}

/*
 * 取得靜態地圖範圍
 */

function get_static_map_circle($lat, $lng, $rad, $width, $height) {

    $url = "https://maps.googleapis.com/maps/api/staticmap?center=" . $lat . "," . $lng . "&size=" . $width . "x" . $height .
            "&maptype=roadmap&markers=color:blue%7Clabel:G%7C" . $lat . "," . $lng . "&path=color:0xff660080|weight:1|fillcolor:0xff660033";

    $staticMapSrc = "";

    $r = 6371;

    $_lat = ($lat * pi()) / 180;
    $_lng = ($lng * pi()) / 180;
    $d = ($rad / 1000) / $r;

    $i = 0;
    $detail = 20;

    for ($i = 0; $i <= 360; $i += $detail) {
        $brng = $i * pi() / 180;

        $pLat = asin(sin($_lat) * cos($d) + cos($_lat) * sin($d) * cos($brng));
        $pLng = (($_lng + atan2(sin($brng) * sin($d) * cos($_lat), cos($d) - sin($_lat) * sin($pLat))) * 180) / pi();
        $pLat = ($pLat * 180) / pi();

        $staticMapSrc = $staticMapSrc . "|" . $pLat . "," . $pLng;
    }

    return $url . $staticMapSrc;
}


function rid_change_to_vertiy_code($m_id){

    $result = $m_id * 942;
    $result /= 1000;
    $result *= 3810;
    $result %= 10000;
    $result = str_pad($result,4,"0");

    return $result;
}
