<?php

function youtube_client_upload($access_token,$video_path,$video_title,$video_description){

	$videoPath =$video_path;
	$ci = & get_instance();
	$key_location = dirname(__DIR__).'/OAuth/client_secret_811181325009-0mop4r7ohh8jskev30pq07riv45ic7cj.json';
	$client = new Google_Client();
    $client->setApplicationName("vrar360");
    $client->setAuthConfig($key_location);
    $client->setIncludeGrantedScopes(true);
    $client->setAccessType('offline');
    $client->addScope("email");
    $client->addScope("profile");
    $client->addScope('https://www.googleapis.com/auth/streetviewpublish');
    $client->addScope('https://www.googleapis.com/auth/youtube');
    $client->setAccessToken($access_token);

    $youtube = new Google_Service_YouTube($client);
    $htmlBody = "";
    try{

	    $snippet = new Google_Service_YouTube_VideoSnippet();
	    $snippet->setTitle($video_title);
	    $snippet->setDescription($video_description);
	    // $snippet->setTags(array("tag1", "tag2"));

	    // Numeric video category. See
	    // https://developers.google.com/youtube/v3/docs/videoCategories/list
	    //非營利組織與行動主義 代號 29
	    $snippet->setCategoryId("29");

	    // Set the video's status to "public". Valid statuses are "public",
	    // "private" and "unlisted".
	    $status = new Google_Service_YouTube_VideoStatus();
	    $status->privacyStatus = "public";

	    // Associate the snippet and status objects with a new video resource.
	    $video = new Google_Service_YouTube_Video();
	    $video->setSnippet($snippet);
	    $video->setStatus($status);

	    // Specify the size of each chunk of data, in bytes. Set a higher value for
	    // reliable connection as fewer chunks lead to faster uploads. Set a lower
	    // value for better recovery on less reliable connections.
	    $chunkSizeBytes = 1 * 1024 * 1024;

	    // Setting the defer flag to true tells the client to return a request which can be called
	    // with ->execute(); instead of making the API call immediately.
	    $client->setDefer(true);

	    // Create a request for the API's videos.insert method to create and upload the video.
	    $insertRequest = $youtube->videos->insert("status,snippet", $video);

	    // Create a MediaFileUpload object for resumable uploads.
	    $media = new Google_Http_MediaFileUpload(
	        $client,
	        $insertRequest,
	        'video/*',
	        null,
	        true,
	        $chunkSizeBytes
	    );
	    $media->setFileSize(filesize($videoPath));


	    // Read the media file and upload it chunk by chunk.
	    $status = false;
	    $handle = fopen($videoPath, "rb");
	    while (!$status && !feof($handle)) {
	      $chunk = fread($handle, $chunkSizeBytes);
	      $status = $media->nextChunk($chunk);
	    }

	    fclose($handle);

	    // If you want to make other calls after the file upload, set setDefer back to false
	    $client->setDefer(false);


	    // $htmlBody .= "<h3>Video Uploaded</h3><ul>";
	    // $htmlBody .= sprintf('<li>%s (%s)</li>',
	    //     $status['snippet']['title'],
	    //     $status['id']);

	    // $htmlBody .= '</ul>';
	    return $status['id'];
	} catch (Google_Service_Exception $e) {
	    $htmlBody .= sprintf('<p>A service error occurred: <code>%s</code></p>',
	        htmlspecialchars($e->getMessage()));
	  } catch (Google_Exception $e) {
	    $htmlBody .= sprintf('<p>An client error occurred: <code>%s</code></p>',
	        htmlspecialchars($e->getMessage()));
	  }

	echo $htmlBody;
	exit();

}

