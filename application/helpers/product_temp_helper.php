<?php

function temp_to_online_product($pt_id) {

    $ci = & get_instance();

    //取得temp商品清單
    $where_array = array(
        "pt_id" => $pt_id
    );
    $product_temp = $ci->Common_model->get_one("product_temp", $where_array);

    //準備新增資料庫
    $insert_array = array(
        "s_id" => $product_temp->s_id,
        "p_name" => $product_temp->pt_name,
        "p_description" => $product_temp->pt_description,
        "p_image" => $product_temp->pt_image,
        "p_detail_image1" => $product_temp->pt_detail_image1,
        "p_detail_image2" => $product_temp->pt_detail_image2,
        "p_detail_image3" => $product_temp->pt_detail_image3,
        "p_detail_image4" => $product_temp->pt_detail_image4,
        "p_product_intro_html" => $product_temp->pt_product_intro_html,
        "p_origin_price" => $product_temp->pt_origin_price,
        "p_price" => $product_temp->pt_price,
        "p_inventory" => $product_temp->pt_inventory,
        "p_inventory_alert" => $product_temp->pt_inventory_alert
    );

    $ci->Common_model->insert_db("product", $insert_array);

    $p_id = $ci->Common_model->get_insert_id();

    //更新分類聯結
    $update_array = array(
        "p_id" => $p_id
    );
    $where_array = array(
        "pt_id" => $pt_id
    );
    $ci->Common_model->update_db("product_category_relation", $update_array, $where_array);

    if ($p_id) {
        //刪除temp商品
        $ci->Common_model->delete_db("product_temp", $where_array);
    }
    return $p_id;
}

function delete_product_temp($pt_id) {

    $ci = & get_instance();

    //取得temp商品清單
    $where_array = array(
        "pt_id" => $pt_id
    );
    $product_temp = $ci->Common_model->get_db("product_temp", $where_array);

    if (!$product_temp) {
        return false;
    }

    foreach ($product_temp as $row) {
        //刪除商品照片
        ($row->pt_image) ? $ci->upload_lib->delete("upload/product/" . $row->pt_image) : "";
        ($row->pt_detail_image1) ? $ci->upload_lib->delete("upload/product/" . $row->pt_detail_image1) : "";
        ($row->pt_detail_image2) ? $ci->upload_lib->delete("upload/product/" . $row->pt_detail_image2) : "";
        ($row->pt_detail_image3) ? $ci->upload_lib->delete("upload/product/" . $row->pt_detail_image3) : "";
        ($row->pt_detail_image4) ? $ci->upload_lib->delete("upload/product/" . $row->pt_detail_image4) : "";
        //刪除商品temp
        $ci->Common_model->delete_db("product_temp", $where_array);
        //刪除分類關聯
        $ci->Common_model->delete_db("product_category_relation", $where_array);
    }

    return true;
}
