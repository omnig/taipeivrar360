<?php


require_once('vendor/lsolesen/pel/autoload.php');

require_once('vendor/lsolesen/pel/src/PelJpeg.php');
require_once('vendor/lsolesen/pel/src/PelExif.php');
require_once('vendor/lsolesen/pel/src/PelTiff.php');
require_once('vendor/lsolesen/pel/src/PelIfd.php');
require_once('vendor/lsolesen/pel/src/PelEntryAscii.php');
require_once('vendor/lsolesen/pel/src/PelEntry.php');
require_once('vendor/lsolesen/pel/src/PelEntryTime.php');
require_once('vendor/lsolesen/pel/src/PelTag.php');
require_once('vendor/lsolesen/pel/src/PelEntryRational.php');
require_once('vendor/lsolesen/pel/src/PelEntrySByte.php');
require_once('vendor/lsolesen/pel/src/PelEntrySLong.php');
require_once('vendor/lsolesen/pel/src/PelEntryShort.php');
require_once('vendor/lsolesen/pel/src/PelEntryNumber.php');
require_once('vendor/lsolesen/pel/src/PelEntryWindowsString.php');

use lsolesen\pel\PelJpeg;
use lsolesen\pel\PelJpegContent;
use lsolesen\pel\PelExif;
use lsolesen\pel\Pel;
use lsolesen\pel\PelTiff;
use lsolesen\pel\PelIfd;
use lsolesen\pel\PelEntryAscii;
use lsolesen\pel\PelEntry;
use lsolesen\pel\PelEntryTime;
use lsolesen\pel\PelTag;
use lsolesen\pel\PelEntryRational;
use lsolesen\pel\PelEntrySByte;
use lsolesen\pel\PelEntryShort;
use lsolesen\pel\PelEntrySLong;
use lsolesen\pel\PelEntryNumber;
use lsolesen\pel\PelEntryWindowsString;


function writ_photo_exif($path="" , $lat="" , $lng=""){


	$pelJpeg = new PelJpeg($path);

	$pelExif = $pelJpeg->getExif();
	if ($pelExif == null) {
	    $pelExif = new PelExif();
	    $pelJpeg->setExif($pelExif);
	}

	$pelTiff = $pelExif->getTiff();
	if ($pelTiff == null) {
	    $pelTiff = new PelTiff();
	    $pelExif->setTiff($pelTiff);
	}

	$pelIfd0 = $pelTiff->getIfd();
	if ($pelIfd0 == null) {
	    $pelIfd0 = new PelIfd(PelIfd::IFD0);
	    $pelTiff->setIfd($pelIfd0);
	}

 	$pelSubIfdGps = new PelIfd(PelIfd::GPS);
	$pelIfd0->addSubIfd($pelSubIfdGps);

	setGeolocation($pelSubIfdGps, $lat, $lng);

	$pelJpeg->saveFile($path);

	//讀取EXIF
	$exif = @exif_read_data($path, 0, true);
	// $exif = exif_read_data("upload/vrphoto/檔案_000.jpeg", 0, true);
	// echo json_encode($exif);
	// exit();

}

function setGeolocation(
    $pelSubIfdGps, $latitudeDegreeDecimal, $longitudeDegreeDecimal) {
    $latitudeRef = ($latitudeDegreeDecimal >= 0) ? 'N' : 'S';
    $latitudeDegreeMinuteSecond
            = degreeDecimalToDegreeMinuteSecond(abs($latitudeDegreeDecimal));
    $longitudeRef= ($longitudeDegreeDecimal >= 0) ? 'E' : 'W';
    $longitudeDegreeMinuteSecond
            = degreeDecimalToDegreeMinuteSecond(abs($longitudeDegreeDecimal));

    // $GPS_TIME_STAMP1 = degreeDecimalToDegreeMinuteSecond(abs());

    $pelSubIfdGps->addEntry(new PelEntryAscii(
            PelTag::GPS_LATITUDE_REF, $latitudeRef));
    $pelSubIfdGps->addEntry(new PelEntryRational(
            PelTag::GPS_LATITUDE, 
            array($latitudeDegreeMinuteSecond['degree'], 1), 
            array($latitudeDegreeMinuteSecond['minute'], 1), 
            array(round($latitudeDegreeMinuteSecond['second'] * 1000), 1000)));
    $pelSubIfdGps->addEntry(new PelEntryAscii(
            PelTag::GPS_LONGITUDE_REF, $longitudeRef));
    $pelSubIfdGps->addEntry(new PelEntryRational(
            PelTag::GPS_LONGITUDE, 
            array($longitudeDegreeMinuteSecond['degree'], 1), 
            array($longitudeDegreeMinuteSecond['minute'], 1), 
            array(round($longitudeDegreeMinuteSecond['second'] * 1000), 1000)));
    // $pelSubIfdGps->addEntry(new PelEntryAscii(
    //         PelTag::GPS_ALTITUDE, "50082/845"));
    // $pelSubIfdGps->addEntry(new PelEntryRational(
    //         PelTag::GPS_TIME_STAMP, 
    //         array("12", 1), 
    //         array("49", 1), 
    //         array("2247", 100)));
    // $pelSubIfdGps->addEntry(new PelEntryAscii(
    //         PelTag::GPS_DATE_STAMP, "2017:08:16"));

}

function degreeDecimalToDegreeMinuteSecond($degreeDecimal) {
    $degree = floor($degreeDecimal);
    $remainder = $degreeDecimal - $degree;
    $minute = floor($remainder * 60);
    $remainder = ($remainder * 60) - $minute;
    $second = $remainder * 60;
    return array('degree' => $degree, 'minute' => $minute, 'second' => $second);
}



?>
