<?php

/*
 * 取得排序規則
 */

function get_sort_rule($page_id, $default_order_by = '', $default_order = '', $default_limit = 0) {
    $ci = & get_instance();

    $where_array = array(
        'cs_page' => $page_id,
        'cs_enabled' => 'Y'
    );
    $sort = $ci->Common_model->get_one('config_sort', $where_array);

    if ($sort && $sort->cs_sort_field) {
        $order_by = $sort->cs_sort_field;
        $order = $sort->cs_sort_dir;
        $limit = $sort->cs_rows_per_page;
    } else {
        //預設值
        $order_by = $default_order_by;
        $order = $default_order;
        $limit = $default_limit;
    }

    $ret = array(
        "order_by" => $order_by,
        "order_dir" => $order,
        "order" => "{$order_by} {$order}",
        "limit" => $limit
    );

    return $ret;
}
