<?php

function array_to_xml($array, $xml = false) {

    if ($xml === false) {
        $xml = new SimpleXMLElement('<Response/>');
    }

    foreach ($array as $key => $value) {
        if (is_numeric($key)) {
            $key = 'item';
        }
        if (is_array($value)) {
            array_to_xml($value, $xml->addChild($key));
        } else {
            if ($value) {
                $xml->addChild($key, htmlspecialchars($value));
            }
        }
    }

    return $xml->asXML();
}
