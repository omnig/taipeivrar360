<?php

function get_mapping_result($type, $item, $query_array = array()) {

    $ci = & get_instance();

    $api_name = 'mapping/' . $item . '/' . $type;
    $url = $ci->config->item('anyplace_api_url') . $api_name;

    $query_array['access_token'] = $ci->config->item('anyplace_key');

    $result = curl_query($url, "post", $query_array);
    $result = json_decode($result, true);

    return $result;
}

function get_navi_result($item, $query_array = array()) {

    $ci = & get_instance();

    $api_name = 'navigation/' . $item;
    $url = $ci->config->item('anyplace_api_url') . $api_name;

    $query_array['access_token'] = $ci->config->item('anyplace_key');
    
    $result = curl_query($url, "post", $query_array);
    $result = json_decode($result, true);

    return $result;
}

function delete_poi($ap_id) {

    $ci = & get_instance();

    //取得poi資訊
    $where_array = array(
        "ap_id" => $ap_id
    );
    $poi = $ci->Common_model->get_one("ap_pois", $where_array);

    if (!$poi) {
        return false;
    }

    $where_array = array(
        'af_id' => $poi->af_id
    );
    $row = $ci->Common_model->get_db_join('ap_floors as af', $where_array, 'ap_building as ab', 'af.ab_id=ab.ab_id', 'inner');
    $row = $row[0];

    //刪除Anyplace POI
    $query_info = array(
        'puid' => $poi->ap_puid,
        'buid' => $row->ab_buid
    );
    $result = get_mapping_result('delete', 'pois', $query_info);

    if ($result['status_code'] != 200) {
        _alert_notice('更新失敗 [' . $result['status_code'] . ':' . $result['message'] . ']。\n\n請再試一次，或洽系統管理員。');
    }

    //刪除資料庫
    $where_array = array(
        "ap_id" => $ap_id
    );
    $ci->Common_model->delete_db('ap_pois', $where_array);

    //刪除關聯
    $ci->Common_model->delete_db('exh_relation', $where_array);

    return true;
}
