<?php

function verify_sso_data($apTicket) {

    $local_userinfo = new UserInfoAuthorization();
    $local_userobj = new User();
    $local_userobj->setProperty("userDN", '0a7333cc-9280-47a4-9a5d-7e5769990fc0');
    $local_userobj->setProperty("givenName", '泛喬一');
    $local_userobj->setProperty("userPrincipalName", "ic-yun@mail.taipei.gov.tw");
    $local_userobj->setProperty("IDN", "0a7333cc-9280-47a4-9a5d-7e5769990fc0");
    $local_userobj->setProperty("orgID", "379110000G");
    $local_userobj->setProperty("depID", "379110000G");
    $local_userobj->setProperty("orgName", "工務局");
    $local_userobj->setProperty("depName", "工務局");

    $local_userinfo->setProperty("user", $local_userobj);
    $local_userinfo->setProperty("result", "true");
    return $local_userinfo;

    //應用系統帳號
    $APID = "inav";
    //驗證票證的 web service
    // $WS_apVerifyPath = "http://sso.gov.taipei/Tpe_aspx/SSO_Service/ap.asmx";
    $WS_apVerifyPath = "http://myaccount-api.gov.taipei/Tpe_aspx/SSO_Service/ap.asmx";


    //web service 請求字串
    $strxml = "<?xml version='1.0' encoding='utf-8'?>";
    $strxml .= "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>";
    $strxml .= "<soap:Body>";
    $strxml .= "<VerifyTicket xmlns='http://tempuri.org/'>";
    $strxml .= "<apticket>" . $apTicket . "</apticket>";
    $strxml .= "<apid>" . $APID . "</apid>";
    $strxml .= "</VerifyTicket>";
    $strxml .= "</soap:Body>";
    $strxml .= "</soap:Envelope>";

    //呼叫 web service
    try {
        $h = curl_init($WS_apVerifyPath);
        curl_setopt($h, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
        curl_setopt($h, CURLOPT_HEADER, 0);
        curl_setopt($h, CURLOPT_POSTFIELDS, $strxml);
        curl_setopt($h, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($h, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($h, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($h, CURLOPT_CUSTOMREQUEST, "POST");


        $returnXML = curl_exec($h);
        curl_close($h);
    } catch (Exception $ex) {
        return false;
        //echo $ex;
        //exit();
    }

    //解譯 XML
    $userinfo = ParsingXML($returnXML);

    return $userinfo;
}

//解譯 XML 回傳值範例
function ParsingXML($xml) {

    $ci = & get_instance();

    $local_userinfo = new UserInfoAuthorization();

    $doc = new DOMDocument();
    //轉碼
    $doc->loadXML($xml);
    $verifyNode = $doc->getElementsByTagName("verify-service");

    //結果
    $resultNode = $verifyNode->item(0)->getElementsByTagName("result");
    $result = $resultNode->item(0)->nodeValue;
    $local_userinfo->setProperty("result", $result);

    if ($result == "true") {
        //成功
        //userDN
        $userDNNode = $verifyNode->item(0)->getElementsByTagName("userDN");
        $userDN = $userDNNode->item(0)->nodeValue;
        //sAMAccountName
        $sAMAccountNameNode = $verifyNode->item(0)->getElementsByTagName("sAMAccountName");
        $sAMAccountName = $sAMAccountNameNode->item(0)->nodeValue;
        //givenName
        $givenNameNode = $verifyNode->item(0)->getElementsByTagName("givenName");
        $givenName = $givenNameNode->item(0)->nodeValue;
        //userPrincipalName
        $userPrincipalNameNode = $verifyNode->item(0)->getElementsByTagName("userPrincipalName");
        $userPrincipalName = $userPrincipalNameNode->item(0)->nodeValue;
        //IDN
        $IDNNode = $verifyNode->item(0)->getElementsByTagName("IDN");
        $IDN = $IDNNode->item(0)->nodeValue;
        //orgID
        $orgIDNode = $verifyNode->item(0)->getElementsByTagName("orgID");
        $orgID = $orgIDNode->item(0)->nodeValue;
        //depID
        $depIDNode = $verifyNode->item(0)->getElementsByTagName("depID");
        $depID = $depIDNode->item(0)->nodeValue;

        //CN=A120087460,OU=應用服務組,OU=臺北市政府資訊局,OU=臺北市政府,DC=taipei,DC=gov,DC=tw
        $OUName = explode(',', $userDN);
        //orgName
        $orgName = str_replace($OUName[3], '', $OUName[2]);
        //depName
        $depName = str_replace('OU=', '', $OUName[1]);

        //使用者資料
        $local_userobj = new User();
        $local_userobj->setProperty("userDN", $userDN);
        $local_userobj->setProperty("givenName", $givenName);
        $local_userobj->setProperty("userPrincipalName", $userPrincipalName);
        $local_userobj->setProperty("IDN", $IDN);
        $local_userobj->setProperty("orgID", $orgID);
        $local_userobj->setProperty("depID", $depID);
        $local_userobj->setProperty("orgName", $orgName);
        $local_userobj->setProperty("depName", $depName);

        $local_userinfo->setProperty("user", $local_userobj);
    } else {
        //失敗
        $apidNode = $verifyNode->item(0)->getElementsByTagName("apid");
        $local_userinfo->setProperty("apid", $apidNode->item(0)->nodeValue);
        $ipNode = $verifyNode->item(0)->getElementsByTagName("ip");
        $local_userinfo->setProperty("ip", $ipNode->item(0)->nodeValue);
        $descriptionNode = $verifyNode->item(0)->getElementsByTagName("description");
        $local_userinfo->setProperty("description", $descriptionNode->item(0)->nodeValue);
    }

    return $local_userinfo;
}

// 進行應用系統身分認證
function request_basic_authentication() {
    $ci = & get_instance();

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://myaccount-api.gov.taipei/app/request_basic_authentication/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => '{
      "APP_PRIVATE_ID": "317e7fde8b0890bfa467b75526eecb60",
      "APP_PRIVATE_PASSWD": "883d451ac118ae1737e818c6eb82be41"
    }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
    ));
    $response = curl_exec($curl);
    curl_close($curl);

    $response = json_decode($response, true);
    $response = (object) $response;

    if (isset($response->PRIVILEGED_APP_SSO_TOKEN)) {
        $ci->session->PRIVILEGED_APP_SSO_TOKEN = $response->PRIVILEGED_APP_SSO_TOKEN;
    }

    return $response;
}

// 查詢登入用戶內部代碼 ， 拿取 APP_COMPANY_UUID
function get_node_uuid($PRIVILEGED_APP_SSO_TOKEN, $PUBLIC_APP_USER_SSO_TOKEN_TO_QUERY) {
    $ci = & get_instance();
    // if (ENVIRONMENT=="docker-dev") {
    //     $ci = & get_instance();
    //     $ci->load->library("couchbase",$ci->config->config);
    //     $cache_data = $ci->couchbase->get("get_node_uuid");
    //     if ($cache_data) {
    //         $cache_data = json_decode($cache_data->value,true);
    //         $cache_data = (object)$cache_data;
    //         $ci->session->APP_COMPANY_UUID = $cache_data->APP_COMPANY_UUID;
    //         return (object)$cache_data;
    //     }
    // }

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://myaccount-api.gov.taipei/app_user/get_node_uuid/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => '{
      "PRIVILEGED_APP_SSO_TOKEN": "' . $PRIVILEGED_APP_SSO_TOKEN . '",
      "PUBLIC_APP_USER_SSO_TOKEN_TO_QUERY": "' . $PUBLIC_APP_USER_SSO_TOKEN_TO_QUERY . '"
    }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    // if (ENVIRONMENT=="docker-dev") {
    //     $response = json_decode($response,true);
    //     $response = (object)$response;
    //     if (isset($response->ERROR_CODE)&&$response->ERROR_CODE=="0") {
    //         $ci->couchbase->set("get_node_uuid",$response,28800);
    //         $ci->session->APP_COMPANY_UUID = $response->APP_COMPANY_UUID;
    //     }
    // }

    $response = json_decode($response, true);
    $response = (object) $response;

    if (isset($response->APP_COMPANY_UUID)) {
        $ci->session->APP_COMPANY_UUID = $response->APP_COMPANY_UUID;
    }

    return $response;
}

// 列出北市府部門節點清單
function list_dept_nodes($PRIVILEGED_APP_SSO_TOKEN, $APP_COMPANY_UUID) {

    // if (ENVIRONMENT=="docker-dev") {
    //     $ci = & get_instance();
    //     $ci->load->library("couchbase",$ci->config->config);
    //     $cache_data = $ci->couchbase->get("list_sub_dept_nodes");
    //     if ($cache_data) {
    //        $cache_data = json_decode($cache_data->value,true);
    //         return (object)$cache_data;
    //     }
    // }

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://myaccount-api.gov.taipei/org_tree/list_dept_nodes/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => '{
      "PRIVILEGED_APP_SSO_TOKEN": "' . $PRIVILEGED_APP_SSO_TOKEN . '",
      "APP_COMPANY_UUID": "' . $APP_COMPANY_UUID . '"
    }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    // if (ENVIRONMENT=="docker-dev") {
    //     $ci->couchbase->set("list_sub_dept_nodes",$response,28800);
    // }
    $response = json_decode($response, true);

    return (object) $response;
}

function list_sub_dept_nodes($PRIVILEGED_APP_SSO_TOKEN, $APP_COMPANY_UUID, $APP_DEPT_NODE_UUID) {

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://myaccount-api.gov.taipei/org_tree/list_sub_dept_nodes/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => '{
      "PRIVILEGED_APP_SSO_TOKEN": "' . $PRIVILEGED_APP_SSO_TOKEN . '",
      "APP_COMPANY_UUID": "' . $APP_COMPANY_UUID . '",
      "APP_DEPT_NODE_UUID": "' . $APP_DEPT_NODE_UUID . '"
    }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    
    $response = json_decode($response, true);

    return (object) $response;
}

// 取回多重部門節點屬性
function get_dept_nodes($PRIVILEGED_APP_SSO_TOKEN, $APP_COMPANY_UUID, array $APP_DEPT_NODE_UUID_LIST) {

    // if (ENVIRONMENT=="docker-dev") {
    //     $ci = & get_instance();
    //     $ci->load->library("couchbase",$ci->config->config);
    //     $cache_data = $ci->couchbase->get("get_dept_nodes");
    //     if ($cache_data) {
    //         $cache_data = json_decode($cache_data->value,true);
    //         return (object)$cache_data;
    //     }
    // }

    $uuid_list = "";
    $last_key = array_key_last($APP_DEPT_NODE_UUID_LIST);
    foreach ($APP_DEPT_NODE_UUID_LIST as $key => $value) {
        if ($last_key != $key) {
            $uuid_list .= '"' . $value . '",';
        } else {
            $uuid_list .= '"' . $value . '"';
        }
    }

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://myaccount-api.gov.taipei/org_tree/get_dept_nodes/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => '{  
       "PRIVILEGED_APP_SSO_TOKEN": "' . $PRIVILEGED_APP_SSO_TOKEN . '",
       "APP_COMPANY_UUID": "' . $APP_COMPANY_UUID . '",
       "APP_DEPT_NODE_UUID_LIST": [
          ' . $uuid_list . '
        ],
       "APP_DEPT_BASIC_PROFILE": {
            "APP_DEPT_FULL_PATH": "",
            "APP_DEPT_NAME": "",
            "APP_DEPT_DESC": "",
            "APP_DEPT_CODE_NO": ""
        }
    }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    // if (ENVIRONMENT=="docker-dev") {
    //     $ci->couchbase->set("get_dept_nodes",$response,28800);
    // }
    $response = json_decode($response, true);

    return (object) $response;
}

// 列出部門人員節點清單（該層）
function list_sub_user_nodes($PRIVILEGED_APP_SSO_TOKEN, $APP_COMPANY_UUID, $APP_DEPT_NODE_UUID) {
    // if (ENVIRONMENT=="docker-dev") {
    //     $ci = & get_instance();
    //     $ci->load->library("couchbase",$ci->config->config);
    //     $cache_data = $ci->couchbase->get("list_sub_user_nodes_".$APP_DEPT_NODE_UUID);
    //     if ($cache_data) {
    //         $cache_data = json_decode($cache_data->value,true);
    //         return (object)$cache_data;
    //     }
    // }

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://myaccount-api.gov.taipei/org_tree/list_sub_user_nodes/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => '{
        "PRIVILEGED_APP_SSO_TOKEN": "' . $PRIVILEGED_APP_SSO_TOKEN . '",
        "APP_COMPANY_UUID": "' . $APP_COMPANY_UUID . '",
        "APP_DEPT_NODE_UUID": "' . $APP_DEPT_NODE_UUID . '"
      }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    // if (ENVIRONMENT=="docker-dev") {
    //     $ci->couchbase->set("list_sub_user_nodes_".$APP_DEPT_NODE_UUID,$response,28800);
    // }
    $response = json_decode($response, true);

    return (object) $response;
}

// 取回人員節點屬性
function get_user_node($PRIVILEGED_APP_SSO_TOKEN, $APP_COMPANY_UUID, $APP_USER_NODE_UUID, $return_obj = false) {

    $ci = & get_instance();

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://myaccount-api.gov.taipei/org_tree/get_user_node/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => '{
        "PRIVILEGED_APP_SSO_TOKEN": "' . $PRIVILEGED_APP_SSO_TOKEN . '",
        "APP_COMPANY_UUID": "' . $APP_COMPANY_UUID . '",
        "APP_USER_NODE_UUID": "' . $APP_USER_NODE_UUID . '",
        "APP_USER_BASIC_PROFILE":{
            "APP_DEPT_NODE_UUID":"",
            "APP_USER_LOGIN_ID":"",
            "APP_USER_EMPNO":"",
            "APP_USER_CHT_NAME":"",
            "APP_USER_ENG_NAME":"",
            "APP_USER_OFFICE_PHONE_NO":"",
            "APP_USER_MOBILE_PHONE_NO":"",
            "APP_USER_EMAIL":"",
            "APP_USER_OFFICE_FAX_NO":"",
            "APP_USER_OFFICE_ADDRESS":"",
            "APP_USER_OFFICE_ZIP_CODE":"",
            "APP_USER_STATUS":"",
            "APP_USER_NODE_LAST_UPDATE_TIME":"",
            "APP_USER_NODE_LAST_UPDATE_TAG":""
        }
    }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);

    if ($return_obj) {

        $result = (object) json_decode($response, true);
        if (isset($result->ERROR_CODE) && $result->ERROR_CODE == "0") {
            $dep_data = $ci->Common_model->get_one("t_department_node", array("dn_node_uuid" => $result->APP_USER_BASIC_PROFILE["APP_DEPT_NODE_UUID"]));

            $depID = $dep_data->dn_org_id;
            $depName = $dep_data->dn_title;

            $org_data = $ci->Common_model->get_one("t_department_node", array("dn_node_uuid" => $dep_data->dn_parent_node_uuid));
            $orgID = $org_data->dn_org_id;
            $orgName = $org_data->dn_title;

            //使用者資料
            $local_userobj = new User();
            $local_userobj->setProperty("userDN", "");
            $local_userobj->setProperty("givenName", $result->APP_USER_BASIC_PROFILE["APP_USER_CHT_NAME"]);
            $local_userobj->setProperty("userPrincipalName", $result->APP_USER_BASIC_PROFILE["APP_USER_LOGIN_ID"]);
            $local_userobj->setProperty("IDN", "");
            $local_userobj->setProperty("orgID", $orgID);
            $local_userobj->setProperty("depID", $depID);
            $local_userobj->setProperty("orgName", $orgName);
            $local_userobj->setProperty("depName", $depName);

            return $local_userobj;
        } else {
            return false;
        }
    }

    $response = json_decode($response, true);

    return (object) $response;
}

// 取回多重人員節點屬性
function get_user_nodes($PRIVILEGED_APP_SSO_TOKEN, $APP_COMPANY_UUID, $APP_USER_NODE_UUID_LIST) {

    $uuid_list = "";
    $last_key = array_key_last($APP_USER_NODE_UUID_LIST);
    foreach ($APP_USER_NODE_UUID_LIST as $key => $value) {
        if ($last_key != $key) {
            $uuid_list .= '"' . $value . '",';
        } else {
            $uuid_list .= '"' . $value . '"';
        }
    }

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://myaccount-api.gov.taipei/org_tree/get_user_nodes/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => '{
        "PRIVILEGED_APP_SSO_TOKEN": "' . $PRIVILEGED_APP_SSO_TOKEN . '",
        "APP_COMPANY_UUID": "' . $APP_COMPANY_UUID . '",
        "APP_USER_NODE_UUID_LIST":[
          ' . $uuid_list . '
        ],
        "APP_USER_BASIC_PROFILE":{
            "APP_DEPT_NODE_UUID":"",
            "APP_USER_LOGIN_ID":"",
            "APP_USER_EMPNO":"",
            "APP_USER_CHT_NAME":"",
            "APP_USER_ENG_NAME":"",
            "APP_USER_OFFICE_PHONE_NO":"",
            "APP_USER_MOBILE_PHONE_NO":"",
            "APP_USER_EMAIL":"",
            "APP_USER_OFFICE_FAX_NO":"",
            "APP_USER_OFFICE_ADDRESS":"",
            "APP_USER_OFFICE_ZIP_CODE":"",
            "APP_USER_STATUS":"",
            "APP_USER_NODE_LAST_UPDATE_TIME":"",
            "APP_USER_NODE_LAST_UPDATE_TAG":""
        }
    }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);

    $response = json_decode($response, true);

    return (object) $response;
}

class User {

    public $userDN;
    public $sAMAccountName;
    public $givenName;
    public $userPrincipalName;
    public $IDN;
    public $orgID;
    public $depID;
    public $orgName;
    public $depName;

    public function setProperty($propname, $propvalue) {
        switch ($propname) {
            case "userDN":
                $this->userDN = $propvalue;
                break;
            case "sAMAccountName":
                $this->sAMAccountName = $propvalue;
                break;
            case "givenName":
                $this->givenName = $propvalue;
                break;
            case "userPrincipalName":
                $this->userPrincipalName = $propvalue;
                break;
            case "IDN":
                $this->IDN = $propvalue;
                break;
            case "orgID":
                $this->orgID = $propvalue;
                break;
            case "depID":
                $this->depID = $propvalue;
                break;
            case "orgName":
                $this->orgName = $propvalue;
                break;
            case "depName":
                $this->depName = $propvalue;
                break;
        }
    }

}

class UserInfoAuthorization {

    public $result;
    public $apid;
    public $ip;
    public $description;
    public $user;

    public function setProperty($propname, $propvalue) {
        switch ($propname) {
            case "result":
                $this->result = $propvalue;
                break;
            case "apid":
                $this->apid = $propvalue;
                break;
            case "ip":
                $this->ip = $propvalue;
                break;
            case "description":
                $this->description = $propvalue;
                break;
            case "user":
                $this->user = $propvalue;
                break;
        }
    }

}
