<?php

/* ==================================
 * 以curl方式呼叫、取回資料
 * ================================== */

function curl_query($url, $http_type = 'post', $parms_array = array()) {
    //參數array轉成 q1=v1&q2=v2 的字串
    $PostData = http_build_query($parms_array);
    $data_string = json_encode($parms_array);

    //以get方式傳送
    if (strtolower($http_type) == 'get') {
        if (strpos($url, '?') === FALSE) {
            $url = "{$url}?{$PostData}";
        } else {
            $url = "{$url}&{$PostData}";
        }
    }

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 'false');

    if (strtolower($http_type) == 'post') {
        //以post方式傳送
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json;',            
            'Content-Length: ' . strlen($data_string))
        );
    }

    // 執行
    $ret = curl_exec($ch);

    // 關閉CURL連線
    curl_close($ch);

    return $ret;
}

function br2nl($string) {
    return preg_replace('/\<br(\s*)?\/?\>/i', "\n", $string);
}
