<?php
/*
Google 環景圖片上傳 Helper
＊分為3步驟需依序完成
＊執行前限制: 需要取得合法授權accesstoken
*/



//返回上傳URL 
function  get_upload_url($accessToken=""){

	$ci = & get_instance();

	$cur_upload_url = curl_init();
    curl_setopt_array($cur_upload_url, array(
     CURLOPT_URL => $ci->config->item('stv_upload_url'),
     CURLOPT_RETURNTRANSFER => true,
     CURLOPT_ENCODING => "" ,
     CURLOPT_CUSTOMREQUEST => "POST",
     CURLOPT_HTTPHEADER => array(
       "authorization: Bearer $accessToken",
          "content-type: application/json",
          "Content-Length: 0"
          ), 
    ));

    $response = curl_exec($cur_upload_url);
    $re = '/https?:\/\/[^"]*/';
    $str = $response;
    preg_match($re, $str, $matches, PREG_OFFSET_CAPTURE, 0);
    $upload_url = $_SESSION['UploadRef'] = $matches[0][0];

	return $upload_url;
}

//上傳Meta Data
//返回ID
function upload_meta_data($upload_url,$access_token,$latVal,$langVal){
  $ci = & get_instance();
  $photoId = substr(strrchr($upload_url, "/"), 1);
  $time_stamp = strtotime("now");
  
  $curl_meta = curl_init();

  curl_setopt_array($curl_meta, array(
      CURLOPT_URL => $ci->config->item('stv_upload_meta'),
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_POSTFIELDS => '{
                      "uploadReference":
                      {
                        "uploadUrl": "'.$upload_url.'"
                      },
                      "pose":
                       {
                         "heading": 359,
                         "latLngPair":
                         {
                           "latitude": '.$latVal.',
                           "longitude": '.$langVal.'
                         }
                      },
                      "captureTime":
                      {
                        "seconds":  '.$time_stamp.'
                      },
                    }',
      CURLOPT_HTTPHEADER => array(
         "authorization: Bearer $access_token",
         "content-type: application/json"
      )
    ));

    $response_meta = curl_exec($curl_meta);
    $response_json = json_decode($response_meta, true);
    
    if(!isset($response_json['photoId']['id'])){
      _alert("上傳圖片有問題Google Street View 不採用(圖片原始資料遺失)，或非正常登入".$response_meta);
      exit;
    }

    $photoID = $response_json['photoId']['id'];



    if(curl_errno($curl_meta)){
        // this would be your first hint that something went wrong
        die('Couldn\'t send request: ' . curl_error($curl_meta));

    }else{
        //check the HTTP status code of the request.
        $resultStatus = curl_getinfo($curl_meta, CURLINFO_HTTP_CODE);

        if($resultStatus != 200){
          die('Request failed: HTTP status code: ' . $resultStatus);
          return $curl_meta;

        }else{
         // 成功擷取UID  
         $photoID = $response_json['photoId']['id'];
         return  $photoID ;
        }

    }
}

function  get_photo_id($accessToken="",$photo_id=""){

  $ci = & get_instance();

  $cur_upload_url = curl_init();
    curl_setopt_array($cur_upload_url, array(
     CURLOPT_URL => $ci->config->item('stv_get_photo').$photo_id,
     CURLOPT_RETURNTRANSFER => true,
     CURLOPT_ENCODING => "" ,
     CURLOPT_CUSTOMREQUEST => "GET",
     CURLOPT_HTTPHEADER => array(
       "authorization: Bearer $accessToken",
          "content-type: application/json",
          "Content-Length: 0"
          ), 
    ));

    $response = curl_exec($cur_upload_url);
    $resultStatus = curl_getinfo($cur_upload_url, CURLINFO_HTTP_CODE);

    //錯誤控制
    if ($resultStatus != 200) {
      $response = json_decode($response,TRUE);

      if ($response["error"]["code"]==503) {
        $response["error"]["information"] = "目前正在上傳環景圖片至Google Services中，請您稍後再更新。";
      }


      echo json_encode($response);
      exit();
    }else{
      $response = json_decode($response,TRUE);
      return $response['shareLink'];
    }


    
    // 

    // $upload_url = $_SESSION['UploadRef'] = $matches[0][0];

  return $upload_url;
}



?>