<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_initial3_schema extends CI_Migration {

        public function up()
        {       
                $this->dbforge->drop_table('amikeblog');
                $this->dbforge->add_field(array(
                        'blog_id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'blog_name' => array(
                                'type' => 'VCHAR',
                                'constraint' => 30,
                                'unsigned' => TRUE,
                        ),
                        'blog_title' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'blog_description' => array(
                                'type' => 'TEXT',
                                'null' => TRUE,
                        ),
                ));
                $this->dbforge->add_key(array('blog_id', 'blog_title'), TRUE);

                $this->dbforge->create_table('amikeblog');
                
        }

        public function down()
        {
                $this->dbforge->drop_table('amikeblog');
                // $this->dbforge->drop_table('mikeblog');
                
        }
}