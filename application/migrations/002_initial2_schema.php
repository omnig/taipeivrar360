<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_initial2_schema extends CI_Migration {

        public function up()
        {
                $this->dbforge->drop_table('blog');
        }

        public function down()
        {
                $this->dbforge->drop_table('blog');
        }
}