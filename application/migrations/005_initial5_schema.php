<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_initial5_schema extends CI_Migration {

        public function up()
        {       
                
                $this->dbforge->add_field(array(
                        'photo_id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'photo_name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 100,
                                'unsigned' => TRUE,
                        ),
                        'photo_path' => array(
                                'type' => 'TEXT',
                                // 'constraint' => 32,
                                'unsigned' => TRUE,
                        ),
                        'EXIF_json' => array(
                                'type' => 'TEXT',
                                'unsigned' => TRUE,
                        ),
                        'upload_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
                ));
                $this->dbforge->add_key(array('photo_id', 'photo_name'), TRUE);

                $this->dbforge->create_table('vr360_photo');
                
        }

        public function down()
        {
                $this->dbforge->drop_table('vr360_photo');
                
        }
}