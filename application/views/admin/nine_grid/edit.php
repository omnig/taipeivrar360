<!DOCTYPE html>
<!--[if IE 8]> <html lang="zh-TW" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="zh-TW" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>
        <link rel="stylesheet" type="text/css" href="<?= base_url("assets/global/plugins/bootstrap-summernote/summernote.css") ?>">
        <link href="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>
        <link rel="shortcut icon" href="<?= base_url("favicon.ico") ?>"/>
        <link href="<?= base_url("assets/plugins/jquery-ui/jquery-ui.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("assets/base/css/custom.css") ?>" rel="stylesheet"></link>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //上方標題列 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered form-fit">
                                <div class="portlet-title">
                                    <div class="caption"> <i class="icon-pencil font-green-haze"></i> <span class="caption-subject font-green-haze bold uppercase"></span> <span class="caption-helper"> <?= $this->lang->line("修改後別忘了儲存資料唷!") ?> </span> </div>
                                    <div class="pull-right"> <a href="javascript:;" class="btn blue tooltips submit" id="btn_submit" data-original-title="<?= $this->lang->line("儲存") ?>"><i class="fa fa-save"></i></a> <a href="<?= base_url("{$this->controller_name}/{$table_name}?m_id=$m_id") ?>" class="btn btn-default tooltips" data-original-title="返回"><i class="fa fa-reply"></i></a> </div>
                                </div>
                                <div class="portlet-body form">
                                    <form action="<?= base_url("{$this->controller_name}/{$table_name}/edit_go") ?>" enctype="multipart/form-data" class="form-horizontal form-row-seperated" method="post">
                                        <div class="form-body">
                                            <div class="text-center"><h2>關卡 基本資料</h2></div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">關卡名稱:</label>
                                                <div class="col-md-6">
                                                    <input type="text" name="<?= $field_prefix ?>_title" class="form-control" placeholder="<?= $this->lang->line("標題") ?>" value="<?= ${$table_name}->{"{$field_prefix}_title"} ?>" />
                                                    <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_title">
                                                        <button class="close" data-close="alert"></button>
                                                        <span> <?= $this->lang->line("請輸入") ?>關卡名稱</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">關卡描述:</label>
                                                <div class="col-md-6">
                                                    <input type="text" name="<?= $field_prefix ?>_describe" class="form-control" placeholder="描述" value="<?= ${$table_name}->{"{$field_prefix}_describe"} ?>" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">關卡提示:</label>
                                                <div class="col-md-6">
                                                    <input type="text" name="<?= $field_prefix ?>_notice" class="form-control" placeholder="提示" value="<?= ${$table_name}->{"{$field_prefix}_notice"} ?>" />
                                                </div>
                                            </div>

                                            <!-- 所屬局處 -->
                                            <div class="form-group">
                                                <label class="control-label col-md-3">所屬局處:</label>
                                                <div class="col-md-6">
                                                    <select class="form-control" name="d_id">
                                                        <option value="">請選擇</option>
                                                        <?php foreach ($department_list as $row): ?>
                                                            <option value="<?= $row->ag_id ?>" <?= (${$table_name}->{"d_id"} == $row->ag_id) ? ' selected' : '' ?>> <?= $row->ag_name ?> </option>
                                                        <?php endforeach ?> 
                                                    </select>
                                                </div>
                                            </div>

                                            <!-- 關卡POI選擇 -->
                                            <div class="form-group">
                                                <label class="control-label col-md-3">關卡POI選擇:</label>
                                                <div class="col-md-6">
                                                    <select class="form-control" name="ap_id">
                                                        <option value="">請選擇</option>
                                                        <?php foreach ($poi_list as $row): ?>
                                                            <option value="<?= $row->ap_id ?>%%%<?= $row->d_id ?>" <?= (${$table_name}->{"ap_id"} == $row->ap_id) ? ' selected' : '' ?>> <?= $row->ap_name ?> </option>
                                                        <?php endforeach ?> 
                                                    </select>
                                                </div>
                                            </div>

                                            <!-- 觸發闖關距離(公尺) -->
                                            <div class="form-group">
                                                <label class="control-label col-md-3">觸發闖關距離(公尺):</label>
                                                <div class="col-md-6">
                                                    <input type="number" min="0" name="<?= $field_prefix ?>_trigger_distance" class="form-control" value="<?= ${$table_name}->{"{$field_prefix}_trigger_distance"} ?>" />
                                                </div>
                                            </div>

                                            <!-- 過關方式選擇 -->
                                            <div class="form-group">
                                                <label class="control-label col-md-3">闖關模式:</label>
                                                <div class="col-md-6">
                                                    <select class="form-control" name="<?= $field_prefix ?>_pass_method">
                                                        <option value="touchdown" <?=(${$table_name}->{"{$field_prefix}_pass_method"} === 'touchdown') ? 'selected' : ''?>>
                                                            到點闖關(抵達目的地後過關，不做任何設定)
                                                        </option>
                                                        <option value="AR" <?=(${$table_name}->{"{$field_prefix}_pass_method"} === 'AR') ? 'selected' : ''?>>
                                                            AR闖關(依選擇的關卡POI[原景點POI]之AR綁定方式，顯示AR後過關)
                                                        </option>
                                                        <option value="QA" <?=(${$table_name}->{"{$field_prefix}_pass_method"} === 'QA') ? 'selected' : ''?>>
                                                            問答闖關(設定題目，問答正確後過關)
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- 顯示POI綁定之AR資料 -->
                                            <div class="AR_div">
                                                <div class="text-center"><h2>AR 互動資料</h2></div>
                                                <?php if(isset($poi)):?>
                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">綁定AR:</label>
                                                        <div class="col-md-6">
                                                            <select class="form-control" name="tdar_id" disabled>
                                                                <option  data-type="init" value="">無</option>
                                                                <?php foreach ($ar_list as $row): ?>
                                                                    <option data-type="<?= $row->tdar_content_type ?>" value="<?= $row->tdar_id ?>" <?= ($poi->{"tdar_id"} == $row->tdar_id) ? ' selected' : '' ?>> <?= $row->tdar_name ?> </option>
                                                                <?php endforeach ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">AR啟動方式:</label>
                                                        <div class="col-md-6">
                                                            <select class="form-control" name="ap_ar_active_method" id="ap_ar_active_method" disabled>
                                                                <option value=""></option>
                                                                <option value="0" <?= ("0" == $poi->ap_ar_active_method) ? ' selected' : '' ?>> 靠近自動呈現 </option>
                                                                <option value="1" <?= ("1" == $poi->ap_ar_active_method) ? ' selected' : '' ?>> 點擊呈現 </option>
                                                                <option value="2" <?= ("2" == $poi->ap_ar_active_method) ? ' selected' : '' ?>> 圖像辨識 </option>
                                                            </select>
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <?php if (!is_null($ar_image)) :?>
                                                                <img id="preview_image_path" src="<?php echo $this->config->item('server_base_url')."/upload_image/".$ar_image->dai_image_path;?>" alt=""/>
                                                                <?php endif;?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">是否顯示於辨識圖上:</label>
                                                        <div class="col-md-6" id="cover_identify_image">
                                                            <label><input type="radio" value='Y' name="ap_cover_identify_image" <?= ("Y" == $poi->ap_cover_identify_image) ? ' checked' : '' ?> disabled/> 是</label>
                                                            <label><input type="radio" value='N' name="ap_cover_identify_image" <?= ("N" == $poi->ap_cover_identify_image) ? ' checked' : '' ?> disabled/> 否</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">AR互動文字(20字內):</label>
                                                        <div class="col-md-6">
                                                            <input type="text" name="ap_ar_text" class="form-control" value="<?= $poi->ap_ar_text ?>" disabled/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">AR互動URL:</label>
                                                        <div class="col-md-6">
                                                            <input type="text" name="ap_ar_url" class="form-control" value="<?= $poi->ap_ar_url ?>" disabled/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">AR呈現高度:</label>
                                                        <div class="col-md-6">
                                                            <select class="form-control" name="ap_ar_heigh" disabled>
                                                                <option value=""></option>
                                                                <option value="0" <?= ("0" == $poi->ap_ar_heigh) ? ' selected' : '' ?>> 高空 </option>
                                                                <option value="1" <?= ("1" == $poi->ap_ar_heigh) ? ' selected' : '' ?>> 中層 </option>
                                                                <option value="2" <?= ("2" == $poi->ap_ar_heigh) ? ' selected' : '' ?>> 地面 </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">AR呈現大小:</label>
                                                        <div class="col-md-6" id="vsize" >
                                                            <label><input type='radio' name="ap_ar_vsize" value="B" <?= ("B" == $poi->ap_ar_vsize) ? ' checked' : '' ?> disabled/> 大</label>
                                                            <label><input type='radio' name="ap_ar_vsize" value="N" <?= ("N" == $poi->ap_ar_vsize) ? ' checked' : '' ?> disabled/> 中</label>
                                                            <label><input type='radio' name="ap_ar_vsize" value="S" <?= ("S" == $poi->ap_ar_vsize) ? ' checked' : '' ?> disabled/> 小</label>

                                                            <div class="clearfix">
                                                                <label for="" class="label font-blue"><i class="fa fa-info-circle"></i> 僅於 3d model 上有效</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">AR感應距離(公尺):</label>
                                                        <div class="col-md-6">
                                                            <input onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" name="ap_ar_distance" class="form-control" value="<?= $poi->ap_ar_distance ?>" disabled/>
                                                        </div>
                                                    </div>
                                                    <div class="alert alert-danger text-center">
                                                        <label class="label font-red-thunderbird font-lg bold">AR互動資料僅供顯示，如需修改編輯。請至關卡POI[原景點POI]進行編輯</label>
                                                    </div>
                                                </div>
                                                <?php else:?>
                                                    <div class="alert alert-danger text-center">
                                                        <label class="label font-red-thunderbird font-lg bold">確定關卡POI並儲存後才會顯示AR互動資料欄位</label>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <!-- 關卡題目選擇 -->
                                            <div class="QA_div">
                                                <div class="text-center"><h2>題目 資料</h2></div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">題目類型:</label>
                                                    <div class="col-md-6">
                                                        <select class="form-control" name="q_style">
                                                            <option value="是非"  <?=((isset($question))&&$question->q_style==='是非')?'selected':''?>>是非</option>
                                                            <option value="單選"  <?=((isset($question))&&$question->q_style==='單選')?'selected':''?>>單選</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <!-- 是非題 -->
                                                <div class="form-group qb">
                                                    <label class="control-label col-md-3">題目型態:</label>
                                                    <div class="col-md-6">
                                                        <select class="form-control" name="qb_style">
                                                            <option value="文字" <?=((isset($question->detail->qb_style))&&$question->detail->qb_style==='文字')?'selected':''?>>文字</option>
    <!--                                                        <option value="文+圖片" >文+圖片</option>-->
    <!--                                                        <option value="文+影片" >文+影片</option>-->
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group qb">
                                                    <label class="control-label col-md-3">題目名稱:</label>
                                                    <div class="col-md-6">
                                                        <textarea class="form-control" name="qb_title" placeholder="題目名稱"><?=$question->detail->qb_title ?? ''?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group qb image-block">
                                                    <label class="control-label col-md-3">圖片</label>
                                                    <div class="col-md-6">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                                                <img src="" alt=""/>
                                                            </div>
                                                            <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> <?= $this->lang->line("選取圖片") ?> </span>
                                                                            <span class="fileinput-exists"> <?= $this->lang->line("更換") ?> </span>
                                                                            <input type="file" name="qb_image" accept="image/*">
                                                                        </span>
                                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?= $this->lang->line("移除") ?> </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group qb video-block">
                                                    <label class="control-label col-md-3">影片位置:</label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="qb_video" class="form-control" placeholder="影片位置" />
                                                    </div>
                                                </div>
                                                <div class="form-group qb">
                                                    <label class="control-label col-md-3">答案:</label>
                                                    <div class="col-md-6">
                                                        <select class="form-control" name="qb_answer">
                                                            <option value="Y" <?=((isset($question->detail->qb_answer))&&$question->detail->qb_answer==='Y')?'selected':''?>>Y</option>
                                                            <option value="N" <?=((isset($question->detail->qb_answer))&&$question->detail->qb_answer==='N')?'selected':''?>>N</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group qb">
                                                    <label class="control-label col-md-3">答案提示:</label>
                                                    <div class="col-md-6">
                                                        <textarea class="form-control" name="qb_tip" rows="5"><?=$question->detail->qb_tip ?? ''?></textarea>
                                                    </div>
                                                </div>

                                                <!-- 單選題 -->
                                                <div class="form-group qs">
                                                    <label class="control-label col-md-3">題目型態:</label>
                                                    <div class="col-md-6">
                                                        <select class="form-control" name="qs_style">
                                                            <option value="文字" <?=((isset($question->detail->qs_style))&&$question->detail->qs_style==='文字')?'selected':''?>>文字</option>
    <!--                                                        <option value="文+圖片">文+圖片</option>-->
    <!--                                                        <option value="文+影片">文+影片</option>-->
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group qs">
                                                    <label class="control-label col-md-3">題目名稱:</label>
                                                    <div class="col-md-6">
                                                        <textarea class="form-control" name="qs_title" placeholder="題目名稱"><?=$question->detail->qs_title ?? ''?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group qs image-block">
                                                    <label class="control-label col-md-3">圖片</label>
                                                    <div class="col-md-6">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                                                <img src="" alt=""/>
                                                            </div>
                                                            <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> <?= $this->lang->line("選取圖片") ?> </span>
                                                                            <span class="fileinput-exists"> <?= $this->lang->line("更換") ?> </span>
                                                                            <input type="file" name="qs_image" accept="image/*">
                                                                        </span>
                                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?= $this->lang->line("移除") ?> </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group qs video-block">
                                                    <label class="control-label col-md-3">影片位置:</label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="qs_video" class="form-control" placeholder="影片位置" />
                                                    </div>
                                                </div>
                                                <div class="form-group qs">
                                                    <label class="control-label col-md-3">選擇1:</label>
                                                    <div class="col-md-6">
                                                        <textarea class="form-control" name="qs_option1" placeholder="選擇1"><?=$question->detail->qs_option1 ?? ''?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group qs">
                                                    <label class="control-label col-md-3">選擇2:</label>
                                                    <div class="col-md-6">
                                                        <textarea class="form-control" name="qs_option2" placeholder="選擇2"><?=$question->detail->qs_option2 ?? ''?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group qs">
                                                    <label class="control-label col-md-3">選擇3:</label>
                                                    <div class="col-md-6">
                                                        <textarea class="form-control" name="qs_option3" placeholder="選擇3"><?=$question->detail->qs_option3 ?? ''?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group qs">
                                                    <label class="control-label col-md-3">選擇4:</label>
                                                    <div class="col-md-6">
                                                        <textarea class="form-control" name="qs_option4" placeholder="選擇4"><?=$question->detail->qs_option4 ?? ''?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group qs">
                                                    <label class="control-label col-md-3">答案:</label>
                                                    <div class="col-md-6">
                                                        <select class="form-control" name="qs_answer">
                                                            <option value="1" <?=((isset($question->detail->qs_answer))&&$question->detail->qs_answer==='1')?'selected':''?>>1</option>
                                                            <option value="2" <?=((isset($question->detail->qs_answer))&&$question->detail->qs_answer==='2')?'selected':''?>>2</option>
                                                            <option value="3" <?=((isset($question->detail->qs_answer))&&$question->detail->qs_answer==='3')?'selected':''?>>3</option>
                                                            <option value="4" <?=((isset($question->detail->qs_answer))&&$question->detail->qs_answer==='4')?'selected':''?>>4</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group qs">
                                                    <label class="control-label col-md-3">答案提示:</label>
                                                    <div class="col-md-6">
                                                        <textarea class="form-control" name="qs_tip" rows="5"><?=$question->detail->qs_tip ?? ''?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">

                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-6">
                                                    <input type="hidden" name="<?= "{$field_prefix}_id" ?>" value="<?= ${$table_name}->{"{$field_prefix}_id"} ?>" />
                                                    <input type="hidden" name="m_id" value="<?= $m_id?>"  />
                                                    <button type="submit" class="btn blue"><i class="fa fa-save"></i> <?= $this->lang->line("儲存") ?> </button>
                                                    <a href="<?= base_url("{$this->controller_name}/{$table_name}?m_id={$m_id}") ?>" class="btn btn-default tooltips" data-original-title="<?= $this->lang->line("取消") ?>"><?= $this->lang->line("取消") ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊 ?>

        <!-- END CONTAINER -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊  ?>

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/select2/select2.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-summernote/summernote.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-summernote/lang/summernote-zh-TW.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
        <?php if ($this->session->i18n == 'zh') : ?>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
        <?php endif; ?>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("assets/global/scripts/metronic.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/global/scripts/datatable_{$this->session->i18n}.js") ?>"></script>
        <script src="<?= base_url("assets/admin/pages/scripts/table-ajax_{$this->session->i18n}.js") ?>"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("js/geofence.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("js/google_map.js") ?>" type="text/javascript"></script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= $this->config->item("google")["api_key"] ?>&callback=area_select_map.initMap"></script>
         <script type="text/javascript" nonce="cm1vaw==">
            $(function () {
                $('#btn_submit').on('click', function () {
                    if (on_submit()) {
                        $('form').submit();
                    }
                });

                $('form').submit(on_submit);

                function on_submit() {
                    var result = true;

                    $('.validation').each(function () {
                        var name = $(this).attr('for');

                        if (!$('*[name=' + name + ']').val()) {
                            $(this).show();
                            $('*[name=' + name + ']').focus();
                            event.preventDefault();
                            result = false;
                        } else {
                            $(this).hide();
                        }
                    });

                    return result;
                }

            });
        </script>
         <script nonce="cm1vaw==">
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
            });


            $('[name="d_id"]').change(function() {
              let d_id = $(this).val();
              let currentVal = $('[name="ap_id"]').val();
              if(d_id === ''){
                $('[name="ap_id"]').find('option').show();
              }else{
                let stay = false;
                $('[name="ap_id"]').find('option').each(function() {
                  let value = $(this).val();
                  if(value === ''){
                    $(this).show();
                  }else{
                    if((new RegExp('%%%'+d_id)).test(value)){
                      $(this).show();
                      if(currentVal === value){
                        stay = true;
                      }
                    }else{
                      $(this).hide();
                    }
                  }
                });
                if(!stay){
                  $('[name="ap_id"]').val("");
                }
              }
            });
            $('[name="d_id"]').change();

            //闖關模式選擇
            const pass_method_div = {'touchdown':'touchdown_div','AR':'AR_div','QA':'QA_div'};
            $(Object.values(pass_method_div).map(v => '.' + v).join(', ')).addClass('display-none')
            $('.' + pass_method_div[$("select[name=<?= $field_prefix ?>_pass_method]").val()]).removeClass('display-none')
            $("select[name=<?= $field_prefix ?>_pass_method]").change(function () {
                $(Object.values(pass_method_div).map(v => '.' + v).join(', ')).addClass('display-none')
                $('.' + pass_method_div[this.value]).removeClass('display-none')
            })
            //題目選擇
            const q_style = <?= json_encode($q_style) ?>;
            $(Object.values(q_style).map(v => '.' + v).join(', ')).addClass('display-none')
            $('.' + q_style[$("select[name=q_style]").val()]).removeClass('display-none')
            $("select[name=q_style]").change(function () {
                $(Object.values(q_style).map(v => '.' + v).join(', ')).addClass('display-none')
                $('.' + q_style[this.value]).removeClass('display-none')
                if ($('.' + q_style[this.value] + ' select[name=' + q_style[this.value] + '_style]').val() != '文+圖片') {
                    $('.' + q_style[this.value] + '.image-block').addClass('display-none')
                }
                if ($('.' + q_style[this.value] + ' select[name=' + q_style[this.value] + '_style]').val() != '文+影片') {
                    $('.' + q_style[this.value] + '.video-block').addClass('display-none')
                }
            })

            $(".image-block, .video-block").addClass('display-none')
            $(Object.values(q_style).map(v => '.' + v + ' select[name=' + v + '_style]').join(', ')).change(function () {
                $('.' + q_style[$("select[name=q_style]").val()] + '.image-block, ' + '.' + q_style[$("select[name=q_style]").val()] + '.video-block').addClass('display-none')
                if (this.value == '文+圖片') {
                    $('.' + q_style[$("select[name=q_style]").val()] + '.image-block').removeClass('display-none')
                }
                if (this.value == '文+影片') {
                    $('.' + q_style[$("select[name=q_style]").val()] + '.video-block').removeClass('display-none')
                }
            })
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
