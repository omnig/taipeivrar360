<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>

        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //上方標題 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="note note-danger">
                                <p> <?= $this->lang->line("說明: 您可以透過刪除、搜索，來管理列表。") ?></p>
                            </div>
                            <div class="portlet">
                                <div class="portlet-title">
                                    <div class="actions"> 
                                        <a href="<?= base_url("{$this->controller_name}/{$table_name}") ?>" class="btn default blue-stripe"><i class="fa fa-list"></i> <?= $item_name ?>列表</a>
                                        <a href="<?= base_url("{$this->controller_name}/nearby_category") ?>" class="btn default green-stripe"> <i class="fa fa-sitemap"></i> <span class="hidden-480"> 分類管理 </span> </a>
                                        <a href="<?= base_url("{$this->controller_name}/{$table_name}/add") ?>" class="btn default purple-stripe"> <i class="fa fa-plus"></i> <span class="hidden-480"> <?= $this->lang->line("新增") . $item_name ?> </span> </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <?php $arr = []; ?>
                                            <select id="select-category" class="form-control">
                                                <option value="0">全部分類</option>
                                                <?php foreach ($nearby_views as $row): ?>
                                                    <?php if (!in_array($row->nc_id, $arr)): ?>
                                                        <option value="<?= $row->nc_id ?>"><?= $row->nc_title . (($row->nc_enabled == 'Y') ? '' : " [已關閉]") ?></option>
                                                        <?php array_push($arr, $row->nc_id); ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <?php $arr = []; ?>
                                            <select id="select-enabled" class="form-control">
                                                <option value="0">全部顯示</option>
                                                <option value="Y"><?= $this->lang->line("啟用中") ?></option>
                                                <option value="N"><?= $this->lang->line("已關閉") ?></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div id="map" class="map"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊 ?>

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/select2/select2.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
        <?php if ($this->session->i18n == 'zh') : ?>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
        <?php endif; ?>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("assets/global/scripts/metronic_{$this->session->i18n}.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("js/map_list.js") ?>" type="text/javascript"></script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= $this->config->item("google")["api_key"] ?>&callback=map_list.initMap"></script>
         <script nonce="cm1vaw==">
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Metronic.setAssetsPath('<?= base_url('assets') ?>/');
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
                map_list.init('<?= base_url() ?>');
                setTimeout(function () {
                    var selected_typeid = $("#select-category").val();
                    var selected_enabled = $("#select-enabled").val();
                    set_views_map(selected_typeid, selected_enabled);
                }, 300);
            });

            $("#select-category").change(function () {
                var selected_typeid = $(this).val();
                var select_enabled = $("#select-enabled").val();
                set_views_map(selected_typeid, select_enabled);
            });

            $("#select-enabled").change(function () {
                var selected_typeid = $("#select-category").val();
                var select_enabled = $(this).val();
                set_views_map(selected_typeid, select_enabled);
            });

            function set_views_map(nc_id, is_enabled) {
                map_list.clear_all();
<?php foreach ($nearby_views as $idx => $row): ?>
                    var enabled = "<?= $row->nv_enabled ?>";
                    if ((nc_id == "0" || nc_id ==<?= $row->nc_id ?>) && (is_enabled == "0" || is_enabled == enabled)) {
                        var content = "名稱：<?= $row->nv_title . "<br>" ?>";
                        content += "描述：<?= replace_rn_text($row->nv_description)."<br>" ?>";
                        content += "地址：<?= $row->nv_address . "<br>" ?>";
                        content += "<?= ($row->nv_image) ? "<img width='200px' src='" . base_url("upload/nearby_views") . "/" . $row->nv_image . "' >" : "" ?>";
                        content += "<br><a href='<?= base_url("sa/nearby_views/edit?nv_id=") . $row->nv_id ?>' class='btn btn-xs default blue-stripe btn-operating'><i class='fa fa-pencil'>編輯</i></a>";
                        map_list.set_views(<?= $idx ?>, "<?= $row->nv_title ?>",<?= ($row->nv_lat) ? $row->nv_lat : 0 ?>,<?= ($row->nv_lng) ? $row->nv_lng : 0 ?>, "<?= $row->nc_title ?>", enabled, content);
                    }
<?php endforeach; ?>
                map_list.fit_bounce();
            }
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
