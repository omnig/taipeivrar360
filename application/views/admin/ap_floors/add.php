<!DOCTYPE html>
<!--[if IE 8]> <html lang="zh-TW" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="zh-TW" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>
        <link rel="stylesheet" type="text/css" href="<?= base_url("assets/global/plugins/bootstrap-summernote/summernote.css") ?>">
        <link href="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //上方標題列 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered form-fit">
                                <div class="portlet-title">
                                    <div class="caption"> <i class="icon-pencil font-green-haze"></i> <span class="caption-subject font-green-haze bold uppercase"></span> <span class="caption-helper"> <?= $this->lang->line("修改後別忘了儲存資料唷!") ?> </span> </div>
                                    <div class="pull-right"> <a href="javascript:;" class="btn blue tooltips submit" id="btn_submit" data-original-title="<?= $this->lang->line("儲存") ?>"><i class="fa fa-save"></i></a> <a href="<?= base_url("{$this->controller_name}/{$table_name}") . '?ab_id=' . $this->input->get('ab_id') ?>" class="btn btn-default tooltips" data-original-title="返回"><i class="fa fa-reply"></i></a> </div>
                                </div>
                                <div class="portlet-body form">
                                    <form action="<?= base_url("{$this->controller_name}/{$table_name}/add_go") ?>" enctype="multipart/form-data" class="form-horizontal form-row-seperated" method="post">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">樓層:</label>
                                            <div class="col-md-6">
                                                <input type="text" name="<?= $field_prefix ?>_number" class="form-control" placeholder="請輸入樓層數字 (不可重覆，新增後無法修改)" value=""/>
                                                <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_number">
                                                    <button class="close" data-close="alert"></button>
                                                    <span> <?= $this->lang->line("請輸入") ?>樓層數字 </span> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3"><?= $this->lang->line("名稱") ?>:</label>
                                            <div class="col-md-6">
                                                <input type="text" name="<?= $field_prefix ?>_name" class="form-control" placeholder="請輸入樓層名稱" value=""/>
                                                <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_name">
                                                    <button class="close" data-close="alert"></button>
                                                    <span> <?= $this->lang->line("請輸入") ?>樓層名稱 </span> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3"><?= $this->lang->line("描述") ?>:</label>
                                            <div class="col-md-6">
                                                <input type="text" name="<?= $field_prefix ?>_desc" class="form-control" placeholder="請輸入樓層描述 (選填)"  value="" />
                                                <div class="alert alert-danger display-hide" for="<?= $field_prefix ?>_desc">
                                                    <button class="close" data-close="alert"></button>
                                                    <span> <?= $this->lang->line("請輸入") ?>樓層描述 </span> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">排序:</label>
                                            <div class="col-md-6">
                                                <input type="number" name="<?= $field_prefix ?>_order" class="form-control" placeholder="排序數字"  value="1" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">室內地圖Plan ID (室內定位用):</label>
                                            <div class="col-md-6">
                                                <input type="text" name="<?= $field_prefix ?>_plan_id" class="form-control" placeholder="請輸入Plan ID (地圖資料夾名稱，選填)"  value="" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">預設顯示底圖 (室內定位用):</label>
                                            <div class="col-md-6">
                                                <select class="form-control" name="<?= $field_prefix ?>_map">
                                                    <option value="Y"><?= $this->lang->line("是") ?></option>
                                                    <option value="N" selected ><?= $this->lang->line("否") ?></option>
                                                </select>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3">左下坐標 (室內定位用):</label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control lat" name="af_bottom_left_lat" value="<?= $ab_lat ?>" placeholder="請輸入經度" >
                                                <input type="text" class="form-control lng" name="af_bottom_left_lng" value="<?= $ab_lng ?>" placeholder="請輸入緯度" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">右上坐標 (室內定位用):</label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control rt_lat" name="af_top_right_lat" value="<?= $ab_lat ?>" placeholder="請輸入經度" >
                                                <input type="text" class="form-control rt_lng" name="af_top_right_lng" value="<?= $ab_lng ?>" placeholder="請輸入緯度" >
                                                (右上左下座標要包覆整個樓層，建議拉大範圍)
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3"></label>
                                            <div class="col-md-6">
                                                <div id="map" style="width:100%;height:400px"></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3"></label>
                                            <div class="col-md-6">
                                                <label class="control-label newlat" hidden></label>
                                                <label class="control-label newlng" hidden></label>
                                                <label class="control-label new_rt_lat" hidden></label>
                                                <label class="control-label new_rt_lng" hidden></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3"><?= $this->lang->line("狀態") ?>:</label>
                                            <div class="col-md-6">
                                                <select class="form-control" name="<?= $field_prefix ?>_enabled">
                                                    <option value="Y"><?= $this->lang->line("啟用中") ?></option>
                                                    <option value="N"><?= $this->lang->line("已關閉") ?></option>
                                                </select>
                                            </div>
                                        </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-6">
                                            <input type="hidden" name="ab_id" value="<?= $this->input->get("ab_id") ?>">
                                            <button type="submit" class="btn blue"><i class="fa fa-save"></i> <?= $this->lang->line("儲存") ?> </button>
                                            <a href="<?= base_url("{$this->controller_name}/{$table_name}" . '?ab_id=' . $this->input->get('ab_id')) ?>" class="btn btn-default tooltips" data-original-title="<?= $this->lang->line("取消") ?>"><?= $this->lang->line("取消") ?></a> 
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->
    <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊 ?>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js") ?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/global/plugins/select2/select2.min.js") ?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-summernote/summernote.min.js") ?>"></script> 
    <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-summernote/lang/summernote-zh-TW.js") ?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
    <?php if ($this->session->i18n == 'zh') : ?>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
    <?php endif; ?>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?= base_url("assets/global/scripts/metronic.js") ?>" type="text/javascript"></script>
    <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
    <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>
    <script src="<?= base_url("js/geofence_floor.js") ?>" type="text/javascript"></script>
    <script src="<?= base_url("js/google_map.js") ?>" type="text/javascript"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= $this->config->item("google")["api_key"] ?>&callback=area_select_map.initMap"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
     <script type="text/javascript" nonce="cm1vaw==">
        $(function () {
            $('#btn_submit').on('click', function () {
                if (on_submit()) {
                    $('form').submit();
                }
            });

            $('form').submit(on_submit);

            function on_submit() {
                var result = true;

                $('.validation').each(function () {
                    var name = $(this).attr('for');

                    if (!$('*[name=' + name + ']').val()) {
                        $(this).show();
                        $('*[name=' + name + ']').focus();
                        event.preventDefault();
                        result = false;
                    } else {
                        $(this).hide();
                    }
                });

                return result;
            }
        });
    </script>
     <script nonce="cm1vaw==">
        jQuery(document).ready(function () {
            Metronic.init(); // init metronic core components
            Layout.init(); // init current layout
            QuickSidebar.init(); // init quick sidebar
        });
        area_select_map.init('<?= base_url() ?>');

        function checkall(selector) {
            $(selector).prop('checked', true);
            $(selector).parent().addClass('checked');
        }
        function uncheck(selector) {
            $(selector).prop('checked', false);
            $(selector).parent().removeClass('checked');
        }
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
