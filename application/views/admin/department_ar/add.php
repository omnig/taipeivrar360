<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>

        <link href="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url("assets/base/css/custom.css") ?>" rel="stylesheet"></link>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //上方標題列 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->

                            <?php include APPPATH . "views/{$this->controller_name}/templates/loading_bar.php"  //載入進度條 ?>
                            <div id="response_div"></div>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <form id="ajax-form" action="<?= base_url("{$this->controller_name}/{$table_name}/add_go") ?>" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data">
                                <div class="portlet">
                                    <div class="portlet-title">
                                        <div class="caption">
                                        </div>
                                        <div class="actions btn-set">
                                            <a href="<?= base_url("{$this->controller_name}/{$table_name}")  ?>" class="btn default"><i class="fa fa-angle-left"></i> <?= $this->lang->line("返回") ?></a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="tabbable">
                                            <div class="tab-content no-space">
                                                <div class="tab-pane active" id="upload_ar">
                                                    <div class="form-body">
                                                        
                                                        <!-- 所屬局處 -->
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">所屬局處:<span class="required"> * </span></label>
                                                            <div class="col-md-4">
                                                                <?= $department_list[0]->ag_name ?>
                                                            </div>
                                                        </div>

                                                        <!-- AR名稱 -->
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">AR名稱:<span class="required"> * </span></label>
                                                            <div class="col-md-4">
                                                                <input type="text" name="<?= $field_prefix ?>_name" class="form-control"/>
                                                                <div class="alert alert-danger display-hide validation validation-r" for="<?= $field_prefix ?>_name">
                                                                    <button class="close" data-close="alert"></button>
                                                                    <span> <?= $this->lang->line("請輸入") ?>AR名稱 </span> 
                                                                </div>
                                                            </div>

                                                        </div>
                                                        
                                                        <!-- AR 呈現類別 -->
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">AR呈現內容類別:<span class="required"> * </span></label>
                                                            <div class="col-md-4">
                                                                <select class="form-control" name="<?= $field_prefix ?>_content_type" id="<?= $field_prefix ?>_content_type">
                                                                    <option value="text">文字</option>
                                                                    <option value="image">圖片</option>
                                                                    <option value="video">影片</option>
                                                                    <option value="3d_model">3D模型</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group hide" id="share_other">
                                                            <label class="control-label col-md-3">是否於平台分享共用:<span class="required"> * </span></label>
                                                            <div class="col-md-4">
                                                                <select class="form-control" name="share_other">
                                                                    <option value="Y">是</option>
                                                                    <option value="N">否</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <!-- <div class="form-group hide" id="interactive_text">
                                                            <label class="control-label col-md-3">互動文字(20字):</label>
                                                            <div class="col-md-4">
                                                                <textarea class="form-control col-md-6" type="text" name="<?= $field_prefix ?>_interactive_text" row="5"></textarea>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">互動URL:</label>
                                                            <div class="col-md-4">
                                                                <input type="url" class="form-control col-md-6" name="<?= $field_prefix ?>_interactive_url" placeholder="若有填互動文字，則此欄無效">
                                                            </div>
                                                        </div> -->

                                                        <!-- AR 呈現內容 -->
                                                        <div class="form-group">
                                                            <div class="control-label col-md-3">
                                                                上傳或選擇AR呈現內容:<span class="required"> * </span>
                                                            </div>
                                                            <div class="col-md-4" id="text_div">
                                                                <div class="col-md-12">
                                                                    <label class="control-label col-md-3">輸入文字(限定20字)</label>
                                                                    <textarea class="col-md-6" type="text" name="<?= $field_prefix ?>_content_path" row="5"></textarea>
                                                                    <div class="col-md-12 text-right">
                                                                        <button type="button" class="btn green btn_submit">><i class="fa fa-check"></i> 上傳</button>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="col-md-6 hide" id="image_div">
                                                                <div class="col-md-12">
                                                                    <img class="preview">
                                                                    <div class="size"></div>
                                                                    <label class="btn btn-default">選取圖片 
                                                                        <input type="file" name="<?= $field_prefix ?>_content_image"  id="<?= $field_prefix ?>_content_image" accept="image/*" hidden>
                                                                    </label>
                                                                    <div class="col-md-12 text-right">
                                                                        <button type="button" class="btn green btn_submit"><i class="fa fa-check"></i> 上傳</button>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6 hide" id="video_div">
                                                                <div class="col-md-12">
                                                                    <div class="size"></div>
                                                                    <label class="btn btn-default">選取影片 <input type="file" name="<?= $field_prefix ?>_content_video" id="<?= $field_prefix ?>_content_video" accept="video/mp4" hidden></label>
                                                                    <div class="col-md-12">
                                                                        <input type="checkbox" name="<?= $field_prefix ?>_is_transparent" value="Y"> 是否為去背影片
                                                                    </div>
                                                                    <div class="col-md-12 text-right">
                                                                        <button type="button" class="btn green btn_submit"><i class="fa fa-check"></i> 上傳</button>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-8 hide" id="youtube_div">
                                                                <label class="control-label col-md-2">YouTube連結:</label>
                                                                
                                                                    <input class="control-label col-md-6"type="text" name="<?= $field_prefix ?>_video" id="video_url">
                                                                <input type="hidden" name="<?= $field_prefix ?>_video_id" id="video_id" >
                                                                <div class="col-md-12">
                                                                    <button  id="review" type="button" class="btn blue col-md-2 col-md-offset-4"><i class="fa fa-save"></i> 瀏覽 </button>
                                                                </div>
                                                                <div id ="video_viwer">
                                                                        <img src="https://lh4.googleusercontent.com/-Fpuxqdoa1V4/VG7pWZQN2hI/AAAAAAAAIwc/hPqw6THm5Q8/s0/6140894980496841.png">                                                               
                                                                </div>

                                                            </div>

                                                            <div class="col-md-6 hide" id="3d_model_div">
                                                                <div class="col-md-12">
                                                                    <div class="col-md-12">
                                                                        <div class="size"></div>
                                                                        <label class="btn btn-default">選取3D Model <input type="file" name="<?= $field_prefix ?>_content_model" id="<?= $field_prefix ?>_content_model" accept="*" hidden></label>
                                                                    </div>
                                                                    
                                                                    
                                                                    <div class="col-md-12 text-right">
                                                                        <button type="button" class="btn green btn_submit"><i class="fa fa-check"></i> 上傳</button>
                                                                    </div>
                                                                    <div id="3d_frame"></div>
                                                                </div>
                                                            </div>
                                                            <!-- test -->
                                                        </div>


                                                        <div class="form-group hide" id="content_degree">
                                                            <label class="control-label col-md-3">3D呈現角度:<span class="required"> * </span></label>
                                                            <div class="col-md-4">
                                                                <select class="form-control" name="<?= $field_prefix ?>_content_degree" id="<?= $field_prefix ?>_content_degree">
                                                                    <option value="180">180度平面呈現</option>
                                                                    <option value="90">90度垂直呈現</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        

                                                        <div class="form-group hide" id="select_share_image">
                                                            <div class="col-md-12 ">
                                                                <row>
                                                                <?php foreach ($share_image as $row) : ?>

                                                                    <label class=" btn-default no-border col-md-4" >
                                                                        <input type="radio" name="share_image" value="<?=$row->tdar_content_path?>" />
                                                                        <?= $row->tdar_name?>
                                                                        <img width="200px" src="<?= base_url('upload/image/'.$row->tdar_content_path)?>">
                                                                    </label>

                                                                <?php endforeach ?>
                                                                </row>
                                                            </div>
                                                            <div class="col-md-12 text-right">
                                                                <button type="button" class="btn green btn_submit"><i class="fa fa-check"></i> 上傳</button>
                                                            </div>
                                                        </div>

                                                        <div class="form-group hide" id="select_share_video">
                                                            <div class="col-md-12 ">
                                                                <row>
                                                                <?php foreach ($share_video as $row) : ?>

                                                                    <label class=" btn-default no-border col-md-4" >
                                                                        <input type="radio" name="share_video" value="<?=$row->tdar_content_path?>" />
                                                                        <?= $row->tdar_name?>
                                                                        <a href="<?= base_url('upload_video/'.$row->tdar_content_path)?>" target="_blank"  class="btn blue "> <i class="fa fa-save"></i> 瀏覽 </a>
                                                                    </label>

                                                                <?php endforeach ?>
                                                                </row>
                                                            </div>
                                                            <div class="col-md-12 text-right">
                                                                <button type="button" class="btn green btn_submit"><i class="fa fa-check"></i> 上傳</button>
                                                            </div>
                                                        </div>

                                                        <div class="form-group hide" id="select_share_ar">
                                                            <div class="col-md-12">
                                                                <a href="http://www.wikitude.com/download/">
                                                                    WT3瀏覽器下載（在Tools標籤選擇相容作業系統版本軟體）
                                                                </a>
                                                            </div>
                                                            <div class="col-md-12 ">
                                                                <row>
                                                                <?php foreach ($share_3d as $row) : ?>

                                                                    <label class=" btn-default no-border col-md-4" >
                                                                        <input type="radio" name="share_3d" value="<?=$row->tdar_id?>" />
                                                                        <?= $row->tdar_name?>
                                                                        <a href="<?=$this->config->item('server_base_url').'upload_model/'.$row->tdar_content_path ?>" target="_blank"  class="btn blue "> <i class="fa fa-save"></i> WT3 下載 </a>
                                                                    </label>

                                                                <?php endforeach ?>
                                                                </row>
                                                            </div>
                                                            <div class="col-md-12 text-right">
                                                                <button type="button" class="btn green btn_submit"><i class="fa fa-check"></i> 上傳</button>
                                                            </div>
                                                        </div>



                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" class="csrfClass" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                            </form>
                        </div>

                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊 ?>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/select2/select2.min.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-summernote/summernote.min.js") ?>"></script> 
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-summernote/lang/summernote-zh-TW.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
            <?php if ($this->session->i18n == 'zh') : ?>
                <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
            <?php endif; ?>
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN PAGE LEVEL SCRIPTS -->
            <script src="<?= base_url("assets/global/scripts/metronic.js") ?>" type="text/javascript"></script>
            <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
            <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>
            <script src="<?= base_url("assets/global/scripts/datatable_{$this->session->i18n}.js") ?>"></script>
            <script src="<?= base_url("assets/admin/pages/scripts/table-ajax_{$this->session->i18n}.js") ?>"></script>
            <!-- END PAGE LEVEL SCRIPTS -->
            <script src="<?= base_url("js/geofence.js") ?>" type="text/javascript"></script>
            <script src="<?= base_url("js/google_map.js") ?>" type="text/javascript"></script>
            <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= $this->config->item("google")["api_key"] ?>&callback=initMap"></script>
             <script type="text/javascript" nonce="cm1vaw==">

                $(function () {

                    var lat = 0;
                    var lng = 0;
                    $('#review').on('click', function(e){

                        $('#video_viwer').empty();
                        var video_url = $('#video_url').val();
                        var video_id =  youtube_parser(video_url);

                        $('#video_id').val(video_id);
                        var new_youtube_frame = '<iframe width="560" height="315" src="https://www.youtube.com/embed/'+video_id+'" frameborder="0" allowfullscreen></iframe><br>'+'<div class="col-md-12 text-right"><button type="button" class="btn green btn_submit"><i class="fa fa-check"></i> 上傳</button></div>';
                        if(video_id){
                            $('#video_viwer').append(new_youtube_frame);
                        }else{
                            alert("Youtube URL Error");
                        }
                        
                        
                    });

                    $('#button_address').on('click', function () {
                        var address_val =  $('#input_address').val();
                        area_select_map.search_address(address_val);
                    });

                    function youtube_parser(url){
                        var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
                        var match = url.match(regExp);
                        return (match&&match[7].length==11)? match[7] : false;
                    }

                    $('#t_id').change(function(){
                        var topic = $('#t_id').val();
                        $.getJSON("<?=base_url('/api/get_topic?topic_id=')?>"+topic, function(){     
                        }).success(function(result){
                            // 得到主題資訊
                            // 經度 
                            lat = result.data[0].t_lat;
                            // 緯度 
                            lng = result.data[0].t_lng

                            $( "#<?= $field_prefix ?>_lat" ).val(lat);
                            $( "#<?= $field_prefix ?>_lng" ).val(lng);
                            
                            area_select_map.set_center(lat,lng);


                        }).error(function(result){
                            lat = <?= $this->config->item('default_lat') ?>;
                            lng = <?= $this->config->item('default_lng') ?>;

                            area_select_map.set_center(lat,lng);
                        });
                    });
                    
                    var contetn_type = "";
                    $('#<?= $field_prefix ?>_content_type').change(function(){   
                        contetn_type = $("#<?= $field_prefix ?>_content_type").val();
                        switch(contetn_type){
                            case "text":
                                $("#text_div").removeClass('hide').addClass('show');
                                $("#interactive_text").removeClass('show').addClass('hide');
                                $("#image_div").removeClass('show').addClass('hide');  
                                $("#video_div").removeClass('show').addClass('hide'); 
                                $("#youtube_div").removeClass('show').addClass('hide'); 
                                $("#3d_model_div").removeClass('show').addClass('hide'); 
                                $("#content_degree").removeClass('show').addClass('hide');
                                $("#share_other").removeClass('show').addClass('hide');
                                $("#select_share_image").removeClass('show').addClass('hide');
                                $("#select_share_video").removeClass('show').addClass('hide');
                                $("#select_share_ar").removeClass('show').addClass('hide');
                                    break;
                            case "image":
                                $("#text_div").removeClass('show').addClass('hide');  
                                $("#image_div").removeClass('hide').addClass('show'); 
                                $("#interactive_text").removeClass('hide').addClass('show');
                                $("#video_div").removeClass('show').addClass('hide');  
                                $("#youtube_div").removeClass('show').addClass('hide'); 
                                $("#3d_model_div").removeClass('show').addClass('hide'); 
                                $("#content_degree").removeClass('show').addClass('hide');
                                $("#share_other").removeClass('hide').addClass('show');
                                $("#select_share_image").removeClass('show').addClass('hide');
                                $("#select_share_video").removeClass('show').addClass('hide');
                                $("#select_share_ar").removeClass('show').addClass('hide');
                                    break;
                            case "video":
                                $("#text_div").removeClass('show').addClass('hide');  
                                $("#image_div").removeClass('show').addClass('hide');  
                                $("#video_div").removeClass('hide').addClass('show');
                                $("#interactive_text").removeClass('hide').addClass('show');
                                $("#youtube_div").removeClass('show').addClass('hide');  
                                $("#3d_model_div").removeClass('show').addClass('hide'); 
                                $("#content_degree").removeClass('show').addClass('hide');
                                $("#share_other").removeClass('hide').addClass('show');
                                $("#select_share_image").removeClass('show').addClass('hide');
                                $("#select_share_video").removeClass('show').addClass('hide');
                                $("#select_share_ar").removeClass('show').addClass('hide');
                                    break;
                            case "youtube":
                                $("#text_div").removeClass('show').addClass('hide');  
                                $("#image_div").removeClass('show').addClass('hide');  
                                $("#video_div").removeClass('show').addClass('hide');
                                $("#youtube_div").removeClass('hide').addClass('show');
                                $("#interactive_text").removeClass('hide').addClass('show');
                                $("#3d_model_div").removeClass('show').addClass('hide'); 
                                $("#content_degree").removeClass('show').addClass('hide');
                                $("#share_other").removeClass('hide').addClass('show');
                                $("#select_share_image").removeClass('show').addClass('hide');
                                $("#select_share_video").removeClass('show').addClass('hide');
                                $("#select_share_ar").removeClass('show').addClass('hide');
                                    break;
                            case "3d_model":
                                $("#text_div").removeClass('show').addClass('hide');  
                                $("#image_div").removeClass('show').addClass('hide');  
                                $("#video_div").removeClass('show').addClass('hide');
                                $("#youtube_div").removeClass('show').addClass('hide');   
                                $("#3d_model_div").removeClass('hide').addClass('show');
                                $("#content_degree").removeClass('hide').addClass('show');
                                $("#interactive_text").removeClass('hide').addClass('show');
                                $("#share_other").removeClass('hide').addClass('show');
                                $("#select_share_image").removeClass('show').addClass('hide');
                                $("#select_share_video").removeClass('show').addClass('hide');
                                $("#select_share_ar").removeClass('show').addClass('hide');
                                    break;
                            case "select_share_image":
                                $("#text_div").removeClass('show').addClass('hide');  
                                $("#image_div").removeClass('show').addClass('hide');  
                                $("#video_div").removeClass('show').addClass('hide');
                                $("#youtube_div").removeClass('show').addClass('hide');   
                                $("#3d_model_div").removeClass('show').addClass('hide');
                                $("#content_degree").removeClass('show').addClass('hide');
                                $("#share_other").removeClass('show').addClass('hide');
                                $("#select_share_image").removeClass('hide').addClass('show');
                                $("#interactive_text").removeClass('hide').addClass('show');
                                $("#select_share_video").removeClass('show').addClass('hide');
                                $("#select_share_ar").removeClass('show').addClass('hide');
                                    break;
                            case "select_share_video":
                                $("#text_div").removeClass('show').addClass('hide');  
                                $("#image_div").removeClass('show').addClass('hide');  
                                $("#video_div").removeClass('show').addClass('hide');
                                $("#youtube_div").removeClass('show').addClass('hide');   
                                $("#3d_model_div").removeClass('show').addClass('hide');
                                $("#content_degree").removeClass('show').addClass('hide');
                                $("#share_other").removeClass('show').addClass('hide');
                                $("#select_share_image").removeClass('show').addClass('hide');
                                $("#select_share_video").removeClass('hide').addClass('show');
                                $("#interactive_text").removeClass('hide').addClass('show');
                                $("#select_share_ar").removeClass('show').addClass('hide');
                                    break;
                            case "select_share_ar":
                                $("#text_div").removeClass('show').addClass('hide');  
                                $("#image_div").removeClass('show').addClass('hide');  
                                $("#video_div").removeClass('show').addClass('hide');
                                $("#youtube_div").removeClass('show').addClass('hide');   
                                $("#3d_model_div").removeClass('show').addClass('hide');
                                $("#content_degree").removeClass('show').addClass('hide');
                                $("#share_other").removeClass('show').addClass('hide');
                                $("#select_share_image").removeClass('show').addClass('hide');
                                $("#select_share_video").removeClass('show').addClass('hide');
                                $("#select_share_ar").removeClass('hide').addClass('show');
                                $("#interactive_text").removeClass('hide').addClass('show');
                                    break;
                            default:
                                $("#text_div").removeClass('hide').addClass('show'); 
                                $("#image_div").removeClass('show').addClass('hide');  
                                $("#video_div").removeClass('show').addClass('hide');
                                $("#youtube_div").removeClass('show').addClass('hide');   
                                $("#3d_model_div").removeClass('show').addClass('hide'); 
                                $("#content_degree").removeClass('show').addClass('hide');
                                $("#share_other").removeClass('hide').addClass('show');
                                $("#interactive_text").removeClass('hide').addClass('show');
                                    break;
                        }
                    });
                    
                     function format_float(num, pos)
                    {
                        var size = Math.pow(10, pos);
                        return Math.round(num * size) / size;
                    }
                 
                    function preview(input) {
                 
                        if (input.files && input.files[0]) {
                            var reader = new FileReader();
                            
                            reader.onload = function (e) {
                                $('.preview').attr('scr', e.target.result);

                                var KB = format_float(e.total / 1024, 2);
                                $('.size').text("檔案大小：" + KB + " KB");

                                if (KB/1000>1){
                                    var MB = format_float(KB / 1024, 2);
                                    $('.size').text("檔案大小：" + MB + " MB");
                                }
                            }


                 
                            reader.readAsDataURL(input.files[0]);
                        }
                    }

                    function video_preview(input) {
                 
                        if (input.files && input.files[0]) {
                            var reader = new FileReader();
                            
                            reader.onload = function (e) {
                                var KB = format_float(e.total / 1024, 2);
                                $('.size').text("檔案大小：" + KB + " KB");

                                if (KB/1000>1){
                                    var MB = format_float(KB / 1024, 2);
                                    $('.size').text("檔案大小：" + MB + " MB");
                                }
                            }
                            reader.readAsDataURL(input.files[0]);
                        }
                    }

                    function three_model_preview(input) {
                    var result = "";
                 
                    if (input.files && input.files[0]) {

                        var reader = new FileReader();

                            reader.readAsDataURL(input.files[0]);
                            // reader.readAsText(input.files[0]);
                            reader.onload = function (e) {
                                var KB = format_float(e.total / 1024, 2);
                                $('.size').text("檔案大小：" + KB + " KB");

                                if (KB/1000>1){
                                    var MB = format_float(KB / 1024, 2);
                                    $('.size').text("檔案大小：" + MB + " MB");
                                }
                            }
                        }

                    }

                    //偵測圖片上傳更改
                     $("body").on("change", "#<?= $field_prefix ?>_content_image", function (){
                        preview(this);
                    });

                    //偵測影片上傳更改
                     $("body").on("change", "#<?= $field_prefix ?>_content_video", function (){
                        video_preview(this);
                    });

                     //偵測3D檔案上傳更改
                     $("body").on("change", "#<?= $field_prefix ?>_content_3d_model", function (){
                        three_model_preview(this);
                    });

                });

            </script>
             <script type="text/javascript" nonce="cm1vaw==">
                jQuery(document).ready(function () {
                    Metronic.init(); // init metronic core components
                    Layout.init(); // init current layout
                    QuickSidebar.init(); // init quick sidebar

                    $('.btn_submit').on('click', function () {

                        event.preventDefault();
                            
                            // 
                            // $('form').submit();
                        var form = $('#ajax-form');

                        // Stop the browser from submitting the form.
                        // document.getElementById("loading_bar").style.display = "block";

                        // Serialize the form data.
                        // var formData = $(form).serialize();
                        var formData = new FormData(form[0]);

                        // Submit the form using AJAX.
                        $.ajax({
                            type: 'POST',
                            url: $(form).attr('action'),
                            data:formData,
                            cache:false,
                            contentType: false,
                            processData: false
                        }).done(function(response) {
                            document.getElementById("loading_bar").style.display = "none";

                            if (response.indexOf("script")!=-1) {
                                $("#response_div").html(response);
                            }else{
                                response = JSON.parse(response)
                                alert(response.msg);
                                $('.csrfClass').val(response.csrf_token);
                            }
                            // alert(response);
                            // console.log(response);
                        }).fail(function(data) {
                            console.log(data);
                        });
                        
                    });
                    
                });
                area_select_map.init('<?= base_url() ?>');
                setTimeout(function () {
                }, 500);


                // 跳窗事件，做選擇判斷
                function open_ar_list_window(){

                    var ar_list_window = popitup('<?=$this->config->item("base_url")."sa/ar_list"?>','ar_list');
                    var ar_id = "";

                    ar_list_window.onbeforeunload = function(){
                           // set warning message
                           $.getJSON('return_ar_id', function(){     
                           }).success(function(result){
                               
                               //ajax 讀取
                                ar_id = result.ar_id;
                                ar_data = result.ar_data;

                                ar_scan_image = '<?php echo $this->config->item('server_base_url')."upload_image/";?>'+ar_data.tdar_image_path;

                                ar_find_image = '<?php echo $this->config->item('server_base_url')."upload/image/";?>'+ar_data.tdar_findpho_path;

                                $("#preview_image_path").attr('src',ar_scan_image);
                                $("#preview_findpho_path").attr('src',ar_find_image);


                           }).error(function(result){
                                console.log(result);
                           });

                     };
                }

            </script>
            <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
