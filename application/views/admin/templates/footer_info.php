<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        <?= date("Y") ?> &copy; <?= $this->lang->line("company_name") ?><?= $this->lang->line("版權所有") ?>
    </div>
</div>
<!-- END FOOTER -->

<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?= base_url("assets/global/plugins/respond.min.js") ?>"></script>
<script src="<?= base_url("assets/global/plugins/excanvas.min.js") ?>"></script> 
<![endif]-->
<script src="<?= base_url("assets/global/plugins/jquery.min.js") ?>" type="text/javascript"></script>
<script src="<?= base_url("assets/global/plugins/jquery-migrate.min.js") ?>" type="text/javascript"></script>
<script src="<?= base_url("assets/global/plugins/bootstrap/js/bootstrap.min.js") ?>" type="text/javascript"></script>
<script src="<?= base_url("assets/global/plugins/jquery.blockui.min.js") ?>" type="text/javascript"></script>
<script src="<?= base_url("assets/global/plugins/jquery.cokie.min.js") ?>" type="text/javascript"></script>
<script src="<?= base_url("assets/global/plugins/uniform/jquery.uniform.min.js") ?>" type="text/javascript"></script>
<script src="<?= base_url("assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js") ?>" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="<?= base_url("js/common_functions.js") ?>" type="text/javascript"></script>
<script src="<?= base_url("js/query_string.js") ?>" type="text/javascript"></script>
<script src="<?= base_url("js/dialog.js") ?>" type="text/javascript"></script>
