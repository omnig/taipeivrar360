<title><?= $meta["title"] ?></title>
<meta name="author" content="<?= $meta["author"] ?>"/>

<link rel="icon" href="<?= base_url("favicon.png") ?>" type="image/png"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<?= base_url("assets/global/plugins/font-awesome/css/font-awesome.min.css") ?>" rel="stylesheet" type="text/css"/>
<link href="<?= base_url("assets/global/plugins/simple-line-icons/simple-line-icons.min.css") ?>" rel="stylesheet" type="text/css"/>
<link href="<?= base_url("assets/global/plugins/bootstrap/css/bootstrap.min.css") ?>" rel="stylesheet" type="text/css"/>
<link href="<?= base_url("assets/global/plugins/uniform/css/uniform.default.css") ?>" rel="stylesheet" type="text/css"/>
<link href="<?= base_url("assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css") ?>" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="<?= base_url("assets/global/css/components.css") ?>" rel="stylesheet" type="text/css"/>
<link href="<?= base_url("assets/global/css/plugins.css") ?>" rel="stylesheet" type="text/css"/>
<link href="<?= base_url("assets/admin/layout/css/layout.css") ?>" rel="stylesheet" type="text/css"/>
<link id="style_color" href="<?= base_url("assets/admin/layout/css/themes/{$this->theme}.css") ?>" rel="stylesheet" type="text/css"/>
<link href="<?= base_url("assets/admin/layout/css/custom.css") ?>" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="stylesheet" type="text/css" href="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css") ?>"/>