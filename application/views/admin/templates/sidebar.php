<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="logo">
                <a href="<?= base_url() ?>">
                    <img src="<?= base_url("images/taipei_logo.jpeg") ?>" alt="logo" class="img-responsive"/>
                </a>
            </li>
            <?php foreach ($this->menu as $id => $row) : ?>
                <?php if (isset($row['heading'])) : //分行欄位 ?>
                    <li>
                        <a href="#">
                            <h4 class="title"><?= $this->lang->line($id) ?></h4>
                        </a>
                    </li>
                    <?php continue ?>
                <?php endif; ?>

                <li class="<?= ($menu_category == $id || (isset($row['sub_menu']) && in_array($menu_category, array_keys($row['sub_menu'])))) ? 'active' : '' ?>">
                    <a href="<?= base_url("{$this->controller_name}/{$id}") ?>"> 
                        <i class="<?= $row['icon'] ?>"></i> 
                        <span class="title"><?= $this->lang->line($row['title']) ?></span>
                        <?php if (isset($row['sub_menu'])) : //顯示展開/收合子menu箭頭 ?>
                            <span class="arrow<?= (in_array($menu_category, array_keys($row['sub_menu']))) ? ' open' : '' ?>"></span> 
                        <?php endif; ?>

                        <?php if ($menu_category == $id || (isset($row['sub_menu']) && in_array($menu_category, array_keys($row['sub_menu'])))) :  //顯示右箭頭 ?>
                            <span class="selected"></span>
                        <?php endif; ?>
                    </a>

                    <?php if (!isset($row['sub_menu'])) continue; //沒有內層子menu ?>

                    <ul class="sub-menu">
                        <?php foreach ($row['sub_menu'] as $sub_id => $sub_row) : ?>
                            <li class="<?= $menu_category == $sub_id ? 'active' : '' ?>">
                                <a href="<?= base_url("{$this->controller_name}/{$sub_id}") ?>">
                                    <i class="<?= $sub_row['icon'] ?>"></i> <?= $this->lang->line($sub_row['title']) ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
