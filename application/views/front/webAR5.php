<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>GeoAR.js demo</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://aframe.io/releases/0.9.2/aframe.min.js"></script>
    <!--
    <script src="https://unpkg.com/aframe-look-at-component@0.8.0/dist/aframe-look-at-component.min.js"></script>
    <script src="https://raw.githack.com/AR-js-org/AR.js/master/aframe/build/aframe-ar-nft.js"></script>
    //-->
    <style type="text/css">
        .info_panel {
            font-size: 40px;
        }
    </style>
</head>

 <script type="text/javascript" nonce="cm1vaw==">
    window.onload = function() {
        navigator.geolocation.getCurrentPosition((position) => {
            pos_lati = position.coords.latitude;
            pos_long = position.coords.longitude;
            $("#loc_lati").html(pos_lati);
            $("#loc_long").html(pos_long);
            $("#plate_01").attr("gps-entity-place", "latitude: " + pos_lati + "; longitude: " + pos_long + ";");
        });

        let options = {
            enableHighAccuracy: false,
            timeout: 5000,
            maximumAge: 0
        };

        id = navigator.geolocation.watchPosition(getMyPos, getMyPos, options);
    }

    const getMyPos = function(result) {
        console.log(result);
        let d = new Date();
        let location_tag = document.createElement("div");
        let LocStr = ('0' + d.getHours()).slice(-2) + ":";
        LocStr += ('0' + d.getMinutes()).slice(-2) + ":";
        LocStr += ('0' + d.getSeconds()).slice(-2) + " => ";
        LocStr += result.coords.latitude + " , " + result.coords.longitude;

        location_tag.innerHTML = LocStr;

        $("#info_block").append(location_tag);

        if (result.coords) {
            $("#loc_lati").html(result.coords.latitude);
            $("#loc_long").html(result.coords.longitude);
        } else {
            $("#info_block").html('ERROR-' + result.code + ' : ' + result.message);
        }
    }

    function compassHeading(alpha, beta, gamma) {

        // Convert degrees to radians
        var alphaRad = alpha * (Math.PI / 180);
        var betaRad = beta * (Math.PI / 180);
        var gammaRad = gamma * (Math.PI / 180);

        // Calculate equation components
        var cA = Math.cos(alphaRad);
        var sA = Math.sin(alphaRad);
        var cB = Math.cos(betaRad);
        var sB = Math.sin(betaRad);
        var cG = Math.cos(gammaRad);
        var sG = Math.sin(gammaRad);

        // Calculate A, B, C rotation components
        var rA = -cA * sG - sA * sB * cG;
        var rB = -sA * sG + cA * sB * cG;
        var rC = -cB * cG;

        // Calculate compass heading
        var compassHeading = Math.atan(rA / rB);

        // Convert from half unit circle to whole unit circle
        if (rB < 0) {
            compassHeading += Math.PI;
        } else if (rA < 0) {
            compassHeading += 2 * Math.PI;
        }

        // Convert radians to degrees
        compassHeading *= 180 / Math.PI;

        console.log(compassHeading);
        return compassHeading;

    }

    window.addEventListener('deviceorientation', function(evt) {
        console.log(evt);
        var heading = null;

        if (evt.absolute === true && evt.alpha !== null) {
            heading = compassHeading(evt.alpha, evt.beta, evt.gamma);
        }

        // Do something with 'heading'...
        $("#loc_alpha").html(evt.alpha);
        $("#loc_beta").html(evt.beta);
        $("#loc_gamma").html(evt.gamma);

    }, false);
</script>

<body style='margin: 0; overflow: hidden;'>
    <div class="info_panel">
        <div>Latitude&nbsp;:&nbsp;<span id="loc_lati"></span></div>
        <div>Longitude&nbsp;:&nbsp;<span id="loc_long"></span></div>
        <div>Alpha&nbsp;:&nbsp;<span id="loc_alpha"></span></div>
        <div>Beta&nbsp;:&nbsp;<span id="loc_beta"></span></div>
        <div>Gamma&nbsp;:&nbsp;<span id="loc_gamma"></span></div>
        <div id="info_block" style="height : 80vh; overflow-y : auto"></div>
    </div>
</body>