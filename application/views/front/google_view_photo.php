<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->

<!--[if !IE]><!-->

<html lang="en">

    <!--<![endif]-->

    <!-- BEGIN HEAD -->

    <head>

        <meta charset="utf-8"/>

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>



        <link href="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css") ?>" rel="stylesheet" type="text/css" />

        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>

        <style type="text/css">

        

        </style>

    </head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->

    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->

        <div class="page-container">

            <!-- BEGIN CONTENT -->

            <div class="page-content-wrapper">

                <div class="page-content">

                    <!-- BEGIN PAGE HEADER-->

                    <div class="row">

                    </div>

                    <!-- END PAGE HEADER-->

                    <!-- BEGIN PAGE CONTENT-->

                    <div class="row">

                        <div class="col-md-12">

                             <div id="vrview"></div>

                        </div>



                    </div>

                    <!-- END PAGE CONTENT-->

                </div>

            </div>

            <!-- END CONTENT -->

        </div>

        <!-- END CONTAINER -->



        <!-- BEGIN PAGE LEVEL PLUGINS -->

            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js") ?>"></script>

            <script type="text/javascript" src="<?= base_url("assets/global/plugins/select2/select2.min.js") ?>"></script>

            <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>

            <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>

            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-summernote/summernote.min.js") ?>"></script> 

            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-summernote/lang/summernote-zh-TW.js") ?>"></script>

            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>

            <?php if ($this->session->i18n == 'zh') : ?>

                <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>

            <?php endif; ?>

            <!-- END PAGE LEVEL PLUGINS -->

            <!-- BEGIN PAGE LEVEL SCRIPTS -->

            <script src="<?= base_url("assets/global/scripts/metronic.js") ?>" type="text/javascript"></script>

            <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>

            <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>

            <script src="<?= base_url("assets/global/scripts/datatable_{$this->session->i18n}.js") ?>"></script>

            <script src="<?= base_url("assets/admin/pages/scripts/table-ajax_{$this->session->i18n}.js") ?>"></script>

            <!-- END PAGE LEVEL SCRIPTS -->

            <script src="//storage.googleapis.com/vrview/2.0/build/vrview.min.js"></script>

            

             <script type="text/javascript" nonce="cm1vaw==">

                var client_width = screen.width;

                var client_height = screen.height;



                window.addEventListener('load', onVrViewLoad)

                function onVrViewLoad() {

                  var vrView = new VRView.Player('#vrview', {

                    //image: 'http://vrar360.dev.utobonus.com/upload/vrphoto/89d425d624c1.jpg',
		            image: '<?=$image_path ?>',
                    // video: 'http://vrar360.dev.utobonus.com/upload_video/40870b144e05.mp4',

                    width: client_width,

                    height: client_height,

                    is_stereo: false

                  });

                }



            </script>

            

            <!-- END JAVASCRIPTS -->

    </body>

    <!-- END BODY -->

</html>


