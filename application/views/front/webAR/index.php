<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>台北 VR Taipei</title>
    <base href="/">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <style>
    </style>
</head>

<body class="font-chinese overflow-hidden">
    <?php include APPPATH . "views/{$this->controller_name}/templates/google_tag.php" ?>
    <?php include APPPATH . "views/{$this->controller_name}/templates/header_menu.php" ?>
    <?php include APPPATH . "views/{$this->controller_name}/templates/footer.php" ?>
     <script nonce="cm1vaw==">
    </script>
</body>

</html>