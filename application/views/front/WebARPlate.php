<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://aframe.io/releases/1.2.0/aframe.min.js"></script>
    <script src="https://raw.githack.com/AR-js-org/AR.js/master/aframe/build/aframe-ar-nft.js"></script>
    <script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDgGVTAMIyky-A-Udkhic7MvUrSya6mdwU&v=beta&chinnel=2"></script>
    <!--<script src="js/ARPlate.js"></script>//-->
    <script src="<?= base_url("js/FOV_tracker.js?v=" . date("His")) ?>"></script>
     <script nonce="cm1vaw==">
        let dataFile = "<?= base_url("upload/data/locations.json?v=" . date("His")) ?>";
        setFOVRange(120);
        setPlateSize(250, 30);

        $.getJSON(dataFile, function(json) {
            setLocations(json);
        });
    </script>
</head>

<body>
    <!--<a-scene vr-mode-ui="enabled: false"></a-scene>//-->
    <div id="screen_size" style="top : 0px; left : 0px; color : #fff; position : fixed; z-index : 10000"></div>
    <div id="infoPanel">====== info panel ======</div>
    <a-scene vr-mode-ui="enabled: false"></a-scene>
</body>

</html>