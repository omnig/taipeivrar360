<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>

        <!-- BEGIN: BASE PLUGINS  -->
        <link href="<?= base_url("assets/plugins/cubeportfolio/css/cubeportfolio.min.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("assets/plugins/owl-carousel/owl.carousel.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("assets/plugins/owl-carousel/owl.theme.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("assets/plugins/owl-carousel/owl.transitions.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("assets/plugins/fancybox/jquery.fancybox.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("assets/plugins/slider-for-bootstrap/css/slider.css") ?>" rel="stylesheet" type="text/css" />
        <!-- END: BASE PLUGINS -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?= base_url("assets/base/css/plugins.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("assets/base/css/components.css") ?>" id="style_components" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("assets/base/css/themes/default.css") ?>" rel="stylesheet" id="style_theme" type="text/css" />
        <link href="<?= base_url("assets/base/css/custom.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("css/front_mobile.css") ?>" rel="stylesheet" type="text/css" />
        <!-- END THEME STYLES -->
    </head>
    <body class="c-layout-header-fixed c-layout-header-mobile-fixed c-layout-header-topbar c-layout-header-topbar-collapse">
        <?php include APPPATH . "views/{$this->controller_name}/templates/fb_js_sdk.php"   //Facebook SDK ?>
        <!-- BEGIN: LAYOUT/HEADERS/HEADER-1 -->
        <!-- BEGIN: HEADER -->
        <header class="c-layout-header c-layout-header-4 c-layout-header-default-mobile" data-minimize-offset="80">
            <?php include APPPATH . "views/{$this->controller_name}/templates/header_top.php"   //最上端切換語系列 ?>
            <?php include APPPATH . "views/{$this->controller_name}/templates/header_menu.php"  //主menu ?>
        </header>
        <!-- END: HEADER -->
        <!-- END: LAYOUT/HEADERS/HEADER-1 -->
        <!-- BEGIN: CONTENT/USER/FORGET-PASSWORD-FORM -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/forget_password_form.php"  //忘記密碼表單 ?>
        <!-- END: CONTENT/USER/FORGET-PASSWORD-FORM -->
        <!-- BEGIN: CONTENT/USER/SIGNUP-FORM -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/signup_form.php"  //註冊表單 ?>
        <!-- END: CONTENT/USER/SIGNUP-FORM -->
        <!-- BEGIN: CONTENT/USER/LOGIN-FORM -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/login_form.php"  //登入表單 ?>
        <!-- END: CONTENT/USER/LOGIN-FORM -->

        <div class="c-layout-page">
            <!-- BEGIN: CONTENT/BARS/BAR-1 -->
            <div class="c-content-box c-size-md">
                <div class="container">
                    <div class="c-content-bar-1 c-opt-1">
                        <h3 class="c-font-uppercase c-font-bold"><?= $alert["caption"] ?></h3>
                        <p class=""> 
                            <?= $alert["content"] ?>
                        </p>
                        <?php if ($alert["return"]): ?>
                            <button type="button" onclick="window.close()" class="btn btn-md c-btn-square c-theme-btn c-btn-uppercase c-btn-bold hide goback_btns"><?= $this->lang->line("關閉") ?></button>
                            <button type="button" onclick="location.href = '<?= $alert["return_uri"] ?>'" class="btn btn-md c-btn-square c-theme-btn c-btn-uppercase c-btn-bold goback_btns"><?= $this->lang->line("返回") ?></button>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <!-- END: CONTENT/BARS/BAR-1 -->
        </div>

        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_no_info.php"  //尾部資訊 ?>

        <!-- BEGIN: LAYOUT PLUGINS -->
        <script src="<?= base_url("assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/plugins/owl-carousel/owl.carousel.min.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/plugins/counterup/jquery.counterup.min.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/plugins/counterup/jquery.waypoints.min.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/plugins/fancybox/jquery.fancybox.pack.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/plugins/slider-for-bootstrap/js/bootstrap-slider.js") ?>" type="text/javascript"></script>
        <!-- END: LAYOUT PLUGINS -->
        <!-- BEGIN: THEME SCRIPTS -->
        <script src="<?= base_url("assets/base/js/components.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/base/js/components-shop.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/base/js/app.js") ?>" type="text/javascript"></script>
         <script nonce="cm1vaw==">
                            $(document).ready(function ()
                            {
                                App.init(); // init core    
                                if (window.opener) {
                                    $('.goback_btns').toggleClass("hide");
                                }
                            });
        </script>
        <!-- END: THEME SCRIPTS -->
        <!-- END: LAYOUT/BASE/BOTTOM -->
    </body>

</html>