<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/hiukim/mind-ar-js@1.1.4/dist/mindar-image.prod.js"></script>
    <script src="https://aframe.io/releases/1.2.0/aframe.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/donmccurdy/aframe-extras@v6.1.1/dist/aframe-extras.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/hiukim/mind-ar-js@1.1.4/dist/mindar-image-aframe.prod.js"></script>
    <script src="https://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>
    <script src="https://unpkg.com/merge-images@2.0.0/dist/index.umd.js"></script>
     <script nonce="cm1vaw==">
        document.addEventListener("DOMContentLoaded", function () {
            const sceneEl = document.querySelector('a-scene');
            const arSystem = sceneEl.systems["mindar-image-system"];
            const fixedVideo1 = document.querySelector('#fixedVideo1');
            const playFixedVideo1btn = document.querySelector('#playFixedVideo1btn');
            const pauseFixedVideo1btn = document.querySelector('#pauseFixedVideo1btn');
            window.onload = function () {
                AFRAME.registerComponent('target2', {
                    init: function () {
                        var marker = this.el;

                        this.vid = document.getElementById("video1");

                        marker.addEventListener('targetFound', function () {
                            console.log('target2 targetFound!');
                            fixedVideo1.setAttribute('visible', true);
                            playFixedVideo1btn.setAttribute('visible', true);
                            pauseFixedVideo1btn.setAttribute('visible', true);
                            // this.vid.play();
                            $('#icon_area').css('display', 'block');
                        }.bind(this));

                        marker.addEventListener('targetLost', function () {
                            console.log('target2 targetLost!');
                            // this.vid.pause();
                            // this.vid.currentTime = 0;
                            // this.vid.play();
                            // $('#fixedVideo1').attr('visible', 'true');
                            // arSystem.pause(true);
                        }.bind(this));

                        playFixedVideo1btn.addEventListener("click", function () {
                            console.log("playFixedVideo1btn click");
                            this.vid.play();
                        }.bind(this));

                        pauseFixedVideo1btn.addEventListener("click", function () {
                            console.log("pauseFixedVideo1btn click");
                            this.vid.pause();
                        }.bind(this));
                    }
                });
                // AFRAME.registerComponent('target3', {
                //     init: function () {
                //         var marker = this.el;
                //
                //         this.vid = document.querySelector("#video2");
                //
                //         marker.addEventListener('targetFound', function () {
                //             console.log('target3 targetFound!');
                //             this.vid.play();
                //         }.bind(this));
                //
                //         marker.addEventListener('targetLost', function () {
                //             console.log('target3 targetLost!');
                //             this.vid.pause();
                //             this.vid.currentTime = 0;
                //             this.vid.play();
                //             $('#fixedVideo2').attr('visible', 'true');
                //             // this.fixedVid.play();
                //             arSystem.pause(true);
                //         }.bind(this));
                //     }
                // });
            };
            AFRAME.registerComponent('target1', {
                init: function () {
                    this.el.addEventListener('targetLost', function () {
                        console.log('target1 targetLost');
                        $('#fixedModel1').attr('visible', 'true');
                        // arSystem.pause(true);
                        $('#icon_area').css('display', 'block');
                    }.bind(this));
                    this.el.addEventListener('targetFound', function () {
                        console.log('target1 targetFound');
                        $('#fixedModel1').attr('visible', 'false');
                        $('#icon_area').css('display', 'block');
                    }.bind(this));
                }
            });
            // AFRAME.registerComponent('target4', {
            //     init: function () {
            //         this.el.addEventListener('targetLost', function () {
            //             console.log('target4 targetLost');
            //             $('#fixedImg1').attr('visible', 'true');
            //             // arSystem.pause(true);
            //         }.bind(this));
            //     }
            // });

            const reset_btn = document.getElementById('reset_btn');

            reset_btn.addEventListener("click", function () {
                console.log("reset_btn click");
                allFixModelHidden();
                stopAllVideo();
                $('#icon_area').css('display', 'none');
                arSystem.start();
            });

        });

        function allFixModelHidden() {
            $('#fixedModel1').attr('visible', 'false');
            $('#fixedModel2').attr('visible', 'false');
            document.querySelector('#fixedVideo1').setAttribute('visible', false);
            $('#fixedVideo2').attr('visible', 'false');
            $('#fixedImg1').attr('visible', 'false');
            $('#playFixedVideo1btn').attr('visible', 'false');
            document.querySelector('#playFixedVideo1btn').setAttribute('visible', false);
            $('#pauseFixedVideo1btn').attr('visible', 'false');
            document.querySelector('#pauseFixedVideo1btn').setAttribute('visible', false);
        }
        function stopAllVideo(){
            document.getElementById('video1').pause();
            document.getElementById('video1').currentTime = 0;
        }
    </script>
</head>
<body>
<?php
$root = '../../';
?>
<a-scene mindar-image="imageTargetSrc: <?=$root?>images/webAR/mindARTarget/qrcode0420.mind;"
         color-space="sRGB" renderer="colorManagement: true, physicallyCorrectLights" vr-mode-ui="enabled: false"
         device-orientation-permission-ui="enabled: false" id="screenshotDiv">
    <a-entity id="fixedModel1" gltf-model="url(<?=$root?>images/webAR/models/8331_A_Toa.gltf)" position="0 -10 0"
              scale="0.3 0.3 0.3" visible="false"></a-entity>
    <a-entity id="fixedModel2" gltf-model="url(<?=$root?>images/webAR/models/8387_Monkey3.gltf)" position="0 -10 0"
              scale="0.3 0.3 0.3" visible="false"></a-entity>
    <a-entity>
        <a-video id="fixedVideo1" src="#video1" webkit-playsinline playsinline width="18"
                 height="10" position="0 10 55" visible="false"></a-video>
        <a-image id="playFixedVideo1btn" class="clickable" src="#playBtn" position="-3 3 55"
                 height="3" width="3" visible="false"></a-image>
        <a-image id="pauseFixedVideo1btn" class="clickable" src="#pauseBtn" position="3 3 55"
                 height="3" width="3" visible="false"></a-image>
    </a-entity>
    <a-entity>
        <a-video id="fixedVideo2" src="#video2" webkit-playsinline playsinline width="18"
                 height="10" position="0 10 55" visible="false"></a-video>
    </a-entity>

    <a-entity>
        <a-image id="fixedImg1" src="<?=$root?>images/webAR/images/bird.png" position="0 -10 0"
                 scale="0.3 0.3 0.3" visible="false"></a-image>
    </a-entity>


    <a-assets>
        <a-asset-item id="model1" src="<?=$root?>images/webAR/models/8331_A_Toa.gltf"></a-asset-item>
        <a-asset-item id="model2" src="<?=$root?>images/webAR/models/8387_Monkey3.gltf"></a-asset-item>
        <a-asset-item id="model3" src="<?=$root?>images/webAR/models/phoenix_bird-gltf/scene.gltf"></a-asset-item>
        <video preload="auto" id="video1" loop controls
               crossorigin webkit-playsinline playsinline src="<?=$root?>images/webAR/videos/demo.mp4">
        </video>
        <video preload="auto" id="video2" loop controls
               crossorigin webkit-playsinline playsinline src="<?=$root?>images/webAR/videos/soccer1.webm">
        </video>
        <img id="img1" src="<?=$root?>images/webAR/images/bird.png"/>
        <img id="playBtn" src="<?=$root?>images/webAR/icon/play.png"/>
        <img id="pauseBtn" src="<?=$root?>images/webAR/icon/pause.png"/>
    </a-assets>

    <a-entity target1 mindar-image-target="targetIndex: 0">
        <a-gltf-model rotation="0 0 0 " position="0 -0.25 0.1" scale="0.005 0.005 0.005" src="#model1" animation-mixer>
    </a-entity>

    <a-entity target2 mindar-image-target="targetIndex: 1">
    </a-entity>

    <a-entity target3 mindar-image-target="targetIndex: 2">
        <a-video src="#video2" webkit-playsinline playsinline width="1.42" height="0.8" position="0 0 0"></a-video>
    </a-entity>

    <a-entity target4 mindar-image-target="targetIndex: 3">
        <a-plane src="#img1" position="0 0 0" height="0.552" width="1" rotation="0 0 0"></a-plane>
    </a-entity>

    <a-camera position="0 10 100" look-controls="enabled: false" cursor="fuse: false; rayOrigin: mouse;"
              raycaster="far: 10000; objects: .clickable"></a-camera>

</a-scene>

<div style="position:fixed;bottom:50px;width:100%;display: none;" id="icon_area">
    <img id="reset_btn" src="<?=base_url()?>images/webAR/icon/reset.png" style="width: 15%;margin-left: 45%;">
</div>
</body>
</html>