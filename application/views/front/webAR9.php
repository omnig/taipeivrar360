<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://aframe.io/releases/1.2.0/aframe.min.js"></script>
    <script src="https://raw.githack.com/AR-js-org/AR.js/master/aframe/build/aframe-ar-nft.js"></script>
    <script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDgGVTAMIyky-A-Udkhic7MvUrSya6mdwU&v=beta&chinnel=2"></script>
     <script nonce="cm1vaw==">
        //Latitude : 緯度
        let latitude = null;
        //Longitude : 經度
        let longitude = null;
        // alpha: 行動裝置水平放置時，繞 Z 軸旋轉的角度，數值為 0 度到 360 度。
        let alpha = null;
        // beta: 行動裝置水平放置時，繞 X 軸旋轉的角度，數值為 -180 度到 180 度。
        let beta = null;
        // gamma: 行動裝置水平放置時，繞 Z 軸旋轉的角度，數值為 -90 度到 90 度。
        let gamma = null;
        //heading: 行動裝置面對的角度(與正北方的夾角)
        let heading = null;
        //altitude: 行動裝置的海拔高度
        let altitude = null;
        //秀 debug 數據
        let showDebug = false;

        let tagWidth = 250;

        let tagHeight = 30;

        let FOV_Range = 120;

        let FOV_Far = 500;

        let fontSize = 20;

        let placeArr = [];

        let logoBGC = "rgb(114, 177, 194)";

        let radarWidth = 100;

        let initAngle = null;

        let map = null;

        //標的物陣列
        let TargetArr = [
            /*{
                        'name': '台北101',
                        'latitude': 25.034186758084147,
                        'longitude': 121.56451480720568
                    }, {
                        'name': '中正記念堂',
                        'latitude': 25.035403271438646,
                        'longitude': 121.52178790370975
                    }, {
                        'name': '國父紀念館',
                        'latitude': 25.04003708096523,
                        'longitude': 121.5602622477364
                    }, {
                        'name': '龍山寺',
                        'latitude': 25.038352079573528,
                        'longitude': 121.49877303930711
                    }, {
                        'name': '台北市政府',
                        'latitude': 25.03758106088042,
                        'longitude': 121.56444080732292
                    }, {
                        'name': '故宮博物院',
                        'latitude': 25.103027039567703,
                        'longitude': 121.56444080732292
                    }, {
                        'name': '三軍總醫院',
                        'latitude': 25.071682540812997,
                        'longitude': 121.59242301938741
                    }, */
            {
                'name': '鴻圖',
                'latitude': 25.080804279834944,
                'longitude': 121.56477248197842,
                'color': 'rgb(255, 0, 255)',
                'top': 0,
                'distance': 0,
                'image': 'Omniguider_logo.png',
                'text': '鴻圖的關鍵技術在於圖像創新應用，擁有3D地圖專利技術，以2D/3D室內外地圖為基底，結合室內外定位、Beacon推播及VR/AR等關鍵技術，發展智慧商城、智慧博物館、智慧圖書館、智慧大樓及智慧園區等多面向應用。鴻圖的技術及商業發展，縱向在持續深化各項產品技術，發展創新產品，在智慧城市3D導引導覽服務領域中，具備領導技術的經驗。橫向則在整合各項技術，提供完整的智慧場域解決方案，可滿足政府大型公共場所、商業型態的商圈、店家、展場、私人展覽館，以及商圈等各種場域與情境。',
                'video': '',
                'youtube': ''
            }, {
                'name': '沒圖的定位點',
                'latitude': 25.080504279834944,
                'longitude': 121.56497248197842,
                'color': 'rgb(0, 60, 255)',
                'top': 0,
                'distance': 0,
                'image': '',
                'text': '這是一個沒有圖的定位點',
                'video': '',
                'youtube': ''
            }, {
                'name': 'Acecombat7',
                'latitude': 25.081004279834944,
                'longitude': 121.56507248197842,
                'color': 'rgb(0, 255, 25)',
                'top': 0,
                'distance': 0,
                'image': '',
                'text': 'ACE COMBAT系列，是能讓玩家能化身為王牌駕駛員，駕駛愛機在追求擬真天空的美麗空間內，享受360度自由飛行的爽快感、讓玩家親自判斷狀況並選擇敵人來破壞的痛快感、享受克服難關達成感的飛行射擊遊戲。本作是系列最新作品，依然由「PROJECT ACES」來擔任開發。本作的主題是「天空革新」，遊戲內立體包覆天空的雲層會擴散，氣流與雷雲等環境變化會晃動機體並擾亂HUD，能讓玩家更加「體驗天空」。在受環境影響的新纏鬥世界中，體驗系列作中最具魅力的空中戰鬥跟速度感吧。',
                'video': '',
                'youtube': 'IKy6ilkRyok'
            }, {
                'name': '台北101',
                'latitude': 25.080104279834944,
                'longitude': 121.56457248197842,
                'color': 'rgb(255, 255, 0)',
                'top': 0,
                'distance': 0,
                'image': 'Taipei101.jpg',
                'text': '台北101是超高大樓，是綠建築，是購物中心，是觀景台，更是台灣的指標。台北101是台灣最著名的景點與指標建築，全台最大的國際級購物中心精品，珠寶， 手錶，米其林一條街與異國風相互輝映讓美食餐廳更接近，全世界最大的鼎泰豐串連著世界饕客，觀景台則進入您走向台北的天空看見台灣的歷史，讓自然美景佐著人文韻味 ，搭配每季獨有的台灣特展，成就一場看見台灣的美好旅行。',
                'video': '',
                'youtube': ''
            }
        ];

        class LocationPanel {

            constructor(dataModel, z_index) {
                let tagPanel = document.createElement("div");
                tagPanel.setAttribute("id", "loc_" + z_index);
                dataModel.top = z_index * (tagHeight * 2);

                let strCSS = "top : " + dataModel.top + "px;";
                strCSS += "left : 0px;";
                strCSS += "width : " + tagWidth + "px; ";
                strCSS += "min-height : " + tagHeight + "px; ";
                strCSS += "position : fixed; ";
                strCSS += "z-index : " + z_index * 10 + "; ";

                tagPanel.setAttribute("style", strCSS);
                tagPanel.setAttribute("onclick", "show_dailog(" + (z_index - 1) + ")");

                let baseObj = document.createElement("div");

                let strBOCSS = "padding : 5px;";
                strBOCSS += "line-height : 30px; ";
                strBOCSS += "font-size : " + fontSize + "px; ";
                strBOCSS += "background : rgba(255, 255, 255, .8);";
                strBOCSS += "border-radius : 5px;";

                baseObj.setAttribute("style", strBOCSS);
                tagPanel.append(baseObj);

                let baseBoard = document.createElement("table");
                let BL1 = document.createElement("tr");
                let logoField = document.createElement("td");
                logoField.setAttribute("width", tagHeight * 2 + 5);
                logoField.setAttribute("rowspan", 2);
                BL1.append(logoField);

                let logoObj = document.createElement("div");
                let logoObjCSS = "width : " + (tagHeight * 2) + "px; ";
                logoObjCSS += "height : " + (tagHeight * 2) + "px; ";
                logoObjCSS += "line-height : " + (tagHeight * 2) + "px; ";
                logoObjCSS += "background : " + dataModel.color + "; ";
                logoObjCSS += "font-family : Arial; ";
                logoObjCSS += "font-size : " + Math.round(tagHeight * 0.8) + "px; ";
                logoObjCSS += "color : rgb(255, 255, 255); ";
                logoObjCSS += "text-align : center; ";
                logoObjCSS += "vertical-align : middle; ";
                logoObjCSS += "border-radius : 5px; ";
                logoObj.setAttribute("style", logoObjCSS);
                logoObj.innerHTML = "AR";

                logoField.append(logoObj);

                let locNameField = document.createElement("td");
                locNameField.setAttribute("style", "font-weight : 800");
                BL1.append(locNameField);

                let BL2 = document.createElement("tr");
                let locDistanceField = document.createElement("td");
                BL2.append(locDistanceField);

                baseBoard.append(BL1);
                baseBoard.append(BL2);
                baseObj.append(baseBoard);

                let mainBoard = document.createElement("div");
                let mainBoardCSS = "width : 100%;";
                mainBoardCSS += "min-height : " + tagHeight + "px;";
                mainBoardCSS += "line-height : 30px;";

                mainBoard.setAttribute("style", mainBoardCSS);
                mainBoard.innerHTML = dataModel.name;

                let aglBoard = document.createElement("div");
                aglBoard.setAttribute("id", "agl_" + z_index);

                let AnchorPanel = document.createElement("div");
                tagPanel.append(AnchorPanel);

                let AnchorObj = document.createElement("div");
                let AnchorCSS = "width : 0px;";
                AnchorCSS += "height : 0px;";
                AnchorCSS += " border-top : 10px rgba(255, 255, 255, .8) solid;";
                AnchorCSS += " border-left : 6px transparent solid;";
                AnchorCSS += " border-right : 6px transparent solid;";
                AnchorCSS += " margin-left : auto;";
                AnchorCSS += " margin-right : auto;";

                AnchorObj.setAttribute("style", AnchorCSS);

                AnchorPanel.append(AnchorObj);

                locNameField.append(mainBoard);
                locDistanceField.append(aglBoard);
                document.body.appendChild(tagPanel);

                baseObj.addEventListener("click", function(event) {});
            }
        }

        //計算兩標的之間的角度
        let getTwoPointAngle = function(px1, py1, px2, py2) {
            /*
            const x = px1 - px2;
            const y = py1 - py2;

            const z = Math.sqrt(x * x + y * y);

            const angle = Math.round((Math.asin(y / z) / Math.PI) * 180); //最終角度

            return angle;
            */
            let dx = px2 - px1
            let dy = py2 - py1
            let ang = Math.atan2(dy, dx) * 180 / Math.PI;

            return ang > 0 ? ang : 360 + ang;
        }

        //計算兩座標的距離 (unit : 單位 K-公里 / N-英里 / N-海里)
        let getDistance = function(lat1, lon1, lat2, lon2, unit) {
            if ((lat1 == lat2) && (lon1 == lon2)) {
                return 0;
            } else {
                var radlat1 = Math.PI * lat1 / 180;
                var radlat2 = Math.PI * lat2 / 180;
                var theta = lon1 - lon2;
                var radtheta = Math.PI * theta / 180;
                var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
                if (dist > 1) {
                    dist = 1;
                }
                dist = Math.acos(dist);
                dist = dist * 180 / Math.PI;
                dist = dist * 60 * 1.1515;
                if (unit == "K") {
                    dist = dist * 1.609344
                }
                if (unit == "N") {
                    dist = dist * 0.8684
                }
                return dist;
            }
        }

        //判斷瀏覽器是否支持陀螺儀
        if (window.DeviceOrientationEvent) {
            window.addEventListener("deviceorientation", function(event) {
                // alpha: rotation around z-axis
                alpha = event.alpha;
                beta = event.beta;
                gamma = event.gamma;

                let rotateDegrees_IOS = null;
                let rotateDegrees_Android = null;
                // gamma: left to right

                //     判斷是否為 iOS 裝置
                if (event.webkitCompassHeading) {
                    heading = event.webkitCompassHeading; // iOS 裝置必須使用 event.webkitCompassHeading

                    rotateDegrees_IOS = heading;
                } else {
                    heading = alpha;

                    rotateDegrees_Android = heading;
                }

                navigator.geolocation.getCurrentPosition((position) => {
                    //console.log(position);
                    latitude = position.coords.latitude; //緯度
                    longitude = position.coords.longitude; //經度
                    //heading = position.coords.heading; //面向方位(相對於正北方的角度)
                    //altitude = position.coords.altitude; //海拔高度
                    //altitude = position.coords.altitude; //海拔高度
                });

                if (showDebug) {
                    $("#coordinates").html("");

                    TargetArr.forEach(function(coordinate) {
                        let cod_obj = document.createElement("div");
                        let viewAgl = getTwoPointAngle(latitude, longitude, coordinate.latitude, coordinate.longitude) - heading;
                        initAngle = viewAgl;
                        let inFOV = viewAgl < (FOV_Range / 2 + 1) && viewAgl > -(FOV_Range / 2 + 1);
                        let name_tag = document.createElement("span");
                        if (!inFOV) name_tag.setAttribute("style", "color : rgb(255, 0 , 0)");
                        name_tag.innerHTML = coordinate.name;

                        let loc_tag = document.createElement("span");
                        loc_tag.innerHTML = " => " + (viewAgl);

                        let dec_tag = document.createElement("span");
                        dec_tag.innerHTML = " => 距離 : " + Math.round(getDistance(latitude, longitude, coordinate.latitude, coordinate.longitude, "K") * 1000, 2) + "M";


                        cod_obj.append(name_tag);
                        //cod_obj.append(loc_tag);
                        cod_obj.append(dec_tag);

                        $("#coordinates").append(cod_obj);
                    });

                    $("#info_latitude").html(latitude);
                    $("#info_longitude").html(longitude);
                    $("#info_heading").html(heading);
                }

                //console.log(TargetArr);

                let ObjID = 1;

                TargetArr.sort(function(a, b) {
                    return a.distance - b.distance;
                });

                TargetArr.forEach(function(coordinate, index) {
                    let viewAgl = getTwoPointAngle(latitude, longitude, coordinate.latitude, coordinate.longitude) - heading;
                    let inFOV = viewAgl < (FOV_Range / 2 + 1) && viewAgl > -(FOV_Range / 2 + 1);
                    let scale = 1.2 + -0.4 * (coordinate.distance / TargetArr[TargetArr.length - 1].distance);
                    let distance = Math.round(getDistance(latitude, longitude, coordinate.latitude, coordinate.longitude, "K") * 1000, 2);

                    if (inFOV && distance < FOV_Far) {
                        let panelLeft = "calc(" + Math.round((viewAgl + (FOV_Range / 2)) / FOV_Range * 100) + "vw - " + tagWidth / 2 + "px)";
                        $("#agl_" + (index + 1)).html("距離 : " + distance + "M");
                        $("#loc_" + (index + 1)).css("left", panelLeft);

                        coordinate.distance = distance;
                    }

                    $("#loc_" + (index + 1)).css("display", inFOV ? "block" : "none");
                    $("#loc_" + (index + 1)).css("transform", "scale(" + scale + " , " + scale + ")");
                    if (beta > 0) {
                        $("#loc_" + (index + 1)).css("top", coordinate.top + (beta - 90) * 10);
                    }
                    //console.log("dot_" + ObjID + " => " + (latitude - coordinate.latitude) + " , " + (longitude - coordinate.longitude));
                    let dotLeft = radarWidth / 2 + (latitude - coordinate.latitude) * radarWidth * 300;
                    let dotTop = radarWidth / 2 + (longitude - coordinate.longitude) * radarWidth * 300;
                    $("#dot_" + index).css("margin-left", dotLeft + "px");
                    $("#dot_" + index).css("margin-top", dotTop + "px");
                    ObjID++;
                });

                //$("#logger_panel").html(beta);

                $("#dotBoard").css("transform", "rotate(" + (90 - (heading - initAngle)) + "deg)");
                //$("#map").css("transform", "rotate(" + (0 - (heading - initAngle)) + "deg)");
                $("#heading_cursor").css("transform", "rotate(" + (0 - (heading - initAngle)) + "deg)");

                if (map == null && latitude != null && longitude != null) {
                    map = new google.maps.Map(document.getElementById("map"), {
                        center: {
                            lat: latitude,
                            lng: longitude
                        },
                        zoom: 18,
                        heading: Math.round(heading)
                    });
                } else {
                    //map.setHeading(heading);
                    try {
                        if (map != null) {
                            map.moveCamera({
                                center: new google.maps.LatLng(latitude, longitude),
                                heading: Math.round(heading)
                            });
                            $("#logger_panel").html(map.getHeading());
                        }
                    } catch (e) {
                        $("#logger_panel").html(e);
                    }
                }
            }, true);
        }

        let show_dailog = function(tid) {
            let titleContainer = $(".dailog_title");
            let contentContainer = $(".dialog_content");
            let targetLoc = TargetArr[tid];
            titleContainer.html(targetLoc.name);

            contentContainer.html("");

            if (targetLoc.image != '') {
                let contentImg = document.createElement("img");
                contentImg.setAttribute("class", "contentImg");
                contentImg.setAttribute("src", "/images/webAR/upload/" + targetLoc.image);

                contentContainer.append(contentImg);
            }

            if (targetLoc.youtube != "") {
                let youtubeStr = "<style>.embed-container { position: relative; padding-bottom: 56.25%; margin : 20px 0px; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style>";
                youtubeStr += "<div class='embed-container'>";
                youtubeStr += "<iframe id='yt_player' width='100%' src='https://www.youtube-nocookie.com/embed/" + targetLoc.youtube;
                youtubeStr += "?rel=0' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen>";
                youtubeStr += "</iframe></div>";
                contentContainer.html(youtubeStr);

                setTimeout(() => {
                    $("#yt_player").trigger("click");
                }, 1000);
            }

            let contentText = document.createElement("div");
            contentText.setAttribute("class", "dialog_content");
            contentText.innerHTML = targetLoc.text;

            contentContainer.append(contentText);


            $("#info_dailog").addClass("show_dailog");


        }

        let close_dailog = function() {
            $("#info_dailog").removeClass("show_dailog");
        }

        window.onload = function() {
            let p_index = 1;

            /*
            TargetArr.forEach(function(coordinate) {
                let panelEntity = new LocationPanel(coordinate, p_index);
                p_index++;
            });
            */

            let radarBoard = document.createElement("div");
            let radarBoardCSS = "top : 4vh; ";
            radarBoardCSS += "left : 5vw; ";
            radarBoardCSS += "width : " + radarWidth + "px; ";
            radarBoardCSS += "height : " + radarWidth + "px; ";
            radarBoardCSS += "border-radius : " + Math.round(radarWidth / 2) + "px; ";
            radarBoardCSS += "background : rgba(114, 177, 194, .5); ";
            radarBoardCSS += "position : fixed; ";
            radarBoardCSS += "z-index : 1000; ";

            radarBoard.setAttribute("style", radarBoardCSS);

            let radaInner = document.createElement("div");
            let radarInnerCSS = "width : " + radarWidth / 2 + "px; ";
            radarInnerCSS += "height : " + radarWidth / 2 + "px; ";
            radarInnerCSS += "margin-top : " + radarWidth / 4 + "px; ";
            radarInnerCSS += "margin-left : " + radarWidth / 4 + "px; ";
            radarInnerCSS += "border-radius : " + Math.round(radarWidth / 4) + "px; ";
            radarInnerCSS += "background : rgba(114, 177, 194, .5); ";
            radarInnerCSS += "position : absolute; ";
            radarInnerCSS += "z-index : 1001; ";

            radaInner.setAttribute("style", radarInnerCSS);

            let dotBoard = document.createElement("div");
            let dotBoardCSS = "width : " + radarWidth + "px; ";
            dotBoardCSS += "height : " + radarWidth + "px; ";
            dotBoardCSS += "position : absolute; ";
            dotBoardCSS += "z-index : 1002; ";
            //dotBoardCSS += "background : rgba(255, 255, 0, .2); ";

            dotBoard.setAttribute("id", "dotBoard");
            dotBoard.setAttribute("style", dotBoardCSS);

            TargetArr.forEach(function(dot, index) {
                //dot.distance = Math.round(getDistance(latitude, longitude, dot.latitude, dot.longitude, "K") * 1000, 2);
                //產生虛擬招牌
                let panelEntity = new LocationPanel(dot, index + 1);
                let dotObj = document.createElement("div");
                let dotObjCss = "width : 3px; ";
                dotObjCss += "height : 3px; ";
                let dotMarginLeft = ((latitude - dot.latitude));
                let dotMarginTop = ((longitude - dot.longitude));
                //console.log(dotMarginLeft + " , " + dotMarginTop);
                dotObjCss += "margin-left : " + radarWidth + "px; ";
                dotObjCss += "margin-top : " + radarWidth + "px; ";
                dotObjCss += "background : " + dot.color + "; ";
                dotObjCss += "position : absolute; ";
                dotObjCss += "z-index : " + (1003 + index) + "; ";
                //console.log(dotObjCss);

                dotObj.setAttribute("id", "dot_" + index);
                dotObj.setAttribute("style", dotObjCss);
                dotBoard.append(dotObj);
            });

            let centerDotObj = document.createElement("div");
            let centerDotObjCss = "width : 3px; ";
            centerDotObjCss += "height : 3px; ";
            centerDotObjCss += "margin-left : " + radarWidth / 2 + "px; ";
            centerDotObjCss += "margin-top : " + radarWidth / 2 + "px; ";
            centerDotObjCss += "background : rgb(255, 0, 0); ";
            centerDotObjCss += "position : absolute; ";
            centerDotObjCss += "z-index : 2000; ";
            centerDotObj.setAttribute("style", centerDotObjCss);
            dotBoard.append(centerDotObj);

            radarBoard.append(radaInner);
            radarBoard.append(dotBoard);
            document.body.append(radarBoard);
        }
    </script>
    <style type="text/css">
        .info_dailog {
            top: 1vh;
            left: 2vw;
            width: 96vw;
            height: 0px;
            padding: 10px;
            /*background : rgba(0, 0, 0, .2);*/
            position: fixed;
            z-index: 6000;
            overflow: hidden;
            opacity: 0;
            transition: all 0.5s;
        }

        .dailog_body {
            max-width: 375px;
            width: 100%;
            min-height: 540px;
            height: 100%;
            padding: 20px;
            background: rgb(255, 255, 255);
            border-radius: 20px;
        }

        .show_dailog {
            height: 84vh;
            opacity: 1;
        }

        .dailog_title {
            font-size: 24px;
            font-weight: 800;
            letter-spacing: 1px;
        }

        .dialog_content {
            height: 90%;
            color: rgba(0, 0, 0, .8);
            text-align: justify;
            letter-spacing: 1px;
            overflow-y: auto;
        }

        .contentImg {
            width: 100%;
            margin-bottom: 20px;
            border-radius: 5px;
        }

        .gg_map {
            /*切成依螢幕寬度的半圓*/
            height: 50vw;
            width: 99vw;
            border-radius: 100vw 100vw 0 0;

            /*固定在最下面*/
            position: fixed !important;
            left: 0;
            bottom: 0;
            background: rgba(255, 0, 0, .3);
            overflow: hidden;
        }
    </style>
</head>

<body>
    <div class="info_board" style="display : none">
        <div>Latitude&nbsp;:&nbsp;<span id="info_latitude"></span></div>
        <div>Longitude&nbsp;:&nbsp;<span id="info_longitude"></span></div>
        <div>Heading&nbsp;:&nbsp;<span id="info_heading"></span></div>
        <div id="coordinates"></div>
    </div>
    <div id="info_dailog" class="info_dailog">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" style="top : 20px; right : 15px; width : 24px; opacity : .5; position : absolute; " onclick="close_dailog()">
            <path d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM175 208.1L222.1 255.1L175 303C165.7 312.4 165.7 327.6 175 336.1C184.4 346.3 199.6 346.3 208.1 336.1L255.1 289.9L303 336.1C312.4 346.3 327.6 346.3 336.1 336.1C346.3 327.6 346.3 312.4 336.1 303L289.9 255.1L336.1 208.1C346.3 199.6 346.3 184.4 336.1 175C327.6 165.7 312.4 165.7 303 175L255.1 222.1L208.1 175C199.6 165.7 184.4 165.7 175 175C165.7 184.4 165.7 199.6 175 208.1V208.1z" />
        </svg>
        <div class="dailog_body">
            <div class="dailog_title"></div>
            <div class="dialog_content"></div>
        </div>
    </div>
    <a-scene vr-mode-ui="enabled: false"></a-scene>
    <div id="logger_panel" style="left : 0px; bottom : 0px; width : 100vw; height : 300px; overflow : auto; color : rgb(36, 255, 0); font-size : 12px; position : fixed; z-index : 3000; display : none;"></div>
    <div id="map_container" class="gg_map">
        <div id="heading_cursor" style="width : 35px; height : 35px; bottom : 10px; margin-left : calc((100% - 35px) / 2); text-align : center; position : absolute; z-index : 3010;">
            <div style="width : 0px; height : 0px; margin-left : auto; margin-right : auto; border-top : 0px; border-bottom : 5px rgba(0,66,255,.5) solid; border-left : 3px transparent solid; border-right : 3px transparent solid"></div>
            <div style="width : 16px; height : 16px; margin-left : auto; margin-right : auto; border-radius : 8px; border : 2px rgba(255, 255, 255, .5) solid; background : rgba(0,66,255,.5)"></div>
        </div>
        <div id="map" style="width : 1000px; height : 1000px; margin-left : calc(50vw - 500px); margin-top : calc(50vw - 500px)"></div>
    </div>
</body>

</html>