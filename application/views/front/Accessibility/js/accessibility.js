let acc_menu = function() {
    let menuData = [];
    let menuTarget = "";
    let openPath = [];
    let lastOpenCase = "";
    this.navDots = false;
    var nDots;

    this.loadData = function(dataSource) {
        $.getJSON(dataSource, function(json) {
            menuData = json;
            console.log(menuData)
        });
    }

    this.drawMenu = function(dataSource, target) {
        nDots = this.navDots;
        $.getJSON(dataSource, function(json) {
            menuData = json;
            if (menuData == [] || menuData.length == 0) {
                alert("尚未載入資料");
            } else {
                $("." + target).append(drawing(menuData));
            }
        });

        drawing = function(data, l_tag) {
            //判斷是否為選單第一層
            let isFirstLayer = typeof l_tag == "undefined" || l_tag.indexOf("Item_") == -1;
            let menuItemCase = document.createElement("ul");
            if (isFirstLayer) {
                if (nDots) {
                    let accessPoint = document.createElement("li");
                    let accessPointLink = document.createElement("a");
                    $(accessPointLink).attr("href", "#");
                    $(accessPointLink).css("letterSpacing", "2px");
                    $(accessPointLink).html(":::");
                    $(accessPoint).addClass("mainMenuItem");
                    $(accessPoint).append(accessPointLink);
                    $(menuItemCase).append(accessPoint);
                }
            } else {
                $(menuItemCase).css("position", "absolute");
                $(menuItemCase).attr("id", l_tag.replace("Item", "Case"));
                $(menuItemCase).addClass("hide");
            }
            let idNo = 0;

            data.forEach(function(itemData) {
                let itemLabel = isFirstLayer ? "Item_" + idNo : l_tag + "_" + idNo;
                let menuItemContainer = document.createElement("li");
                $(menuItemContainer).on("mouseover", function() {
                    //let targetTag = itemLabel.replace("Item", "Case");
                    //if (openPath.indexOf(targetTag) < 0) openPath.push(targetTag);
                    caseSwitch(itemLabel);
                });
                if (isFirstLayer) {
                    $(menuItemContainer).on("mouseleave", function() {
                        //openPath.splice(itemLabel.replace("Item", "Case"));
                        console.log(itemLabel + " ==> Leave");
                        caseSwitch("");
                    });
                }
                let menuLabel = itemLabel.replace("Item", "Menu");
                $(menuItemContainer).attr("id", menuLabel);
                $(menuItemContainer).addClass(isFirstLayer ? "mainMenuItem" : itemData.css);
                let menuItemBody = document.createElement("table");
                $(menuItemBody).attr({ border: 0, cellpeading: 0, cellspacing: 0 });
                $(menuItemBody).css("width", "100%");
                let menuItemRowFirst = document.createElement("tr");
                let menuItemCellLeft = document.createElement("td");
                $(menuItemCellLeft).css("width", "0px");
                let menuItemCellMain = document.createElement("td");
                $(menuItemCellMain).css("white-space", "nowrap");
                let menuItemCellRight = document.createElement("td");
                $(menuItemCellRight).css("width", "0px");
                $(menuItemRowFirst).append(menuItemCellLeft);
                $(menuItemRowFirst).append(menuItemCellMain);
                $(menuItemRowFirst).append(menuItemCellRight);
                let menuItemRowSecond = document.createElement("tr");
                let menuItemCellBottom = document.createElement("td");
                $(menuItemCellBottom).css("colspan", "3");
                $(menuItemRowSecond).append(menuItemCellBottom);

                $(menuItemBody).append(menuItemRowFirst);
                $(menuItemBody).append(menuItemRowSecond);

                $(menuItemContainer).append(menuItemBody);
                $(menuItemCase).append(menuItemContainer);
                let menuText = "";
                let linkTag = document.createElement("a");
                let linkLabel = itemLabel.replace("Item", "Link");
                $(linkTag).on("focus", function() {
                    caseSwitch(itemLabel);
                    readWord($(linkTag).html());
                });
                $(linkTag).attr("id", linkLabel);
                $(linkTag).attr("href", itemData.link != "" && itemData.link.length > 0 ? itemData.link : "#");
                $(linkTag).attr("alt", itemData.text);
                $(linkTag).html(itemData.text);
                $(menuItemCellMain).append(linkTag);

                if (itemData.subMenu.length > 0) {
                    if (isFirstLayer || itemData.drop == "bottom") {
                        $(menuItemCellBottom).append(drawing(itemData.subMenu, itemLabel));
                    } else {
                        if (itemData.drop == "left") {
                            $(menuItemCellLeft).append(drawing(itemData.subMenu, itemLabel));
                        } else {
                            $(menuItemCellRight).append(drawing(itemData.subMenu, itemLabel));
                        }
                    }
                }
                idNo++;
            });

            console.log(menuItemCase);
            return menuItemCase;
        }

        caseSwitch = function(overItem) {
            if (lastOpenCase != overItem && (lastOpenCase.indexOf(overItem) < 0 || overItem == "")) {
                console.log("do caseSitch ======================================");
                reviewCase(menuData, overItem);

                lastOpenCase = overItem;
            }
        }

        reviewCase = function(menuData, overItem, parentItem) {
            let loopID = 0;
            console.log(overItem);
            menuData.forEach(function(menuItem) {
                let caseID = typeof parentItem == "undefined" ? "Case_" + loopID : parentItem + "_" + loopID;
                if (overItem.replace("Item", "Case").indexOf(caseID) > -1) {
                    $("#" + caseID).removeClass("hide");
                } else {
                    $("#" + caseID).addClass("hide");
                }

                console.log(overItem.replace("Item", "Case") + " , " + caseID);
                if (menuItem.subMenu.length > 0) {
                    //console.log(caseID.replace("Case", "Item") + " , " + caseID);
                    reviewCase(menuItem.subMenu, overItem, caseID);
                }

                loopID++;
            });
        }
    }
}

$(document).ready(function() {
    $('input').on("focus", function() {
        readWord($(this).attr("placeholder"));
        /*
        speechSynthesis.cancel();
        msg.text = $(this).attr("placeholder");
        speechSynthesis.speak(msg);
        */
    });


    $('a').on("focus", function() {
        let firstChild = $(this).children(":first")[0];
        if (firstChild.tagName == "IMG") {
            readWord($(firstChild).attr("title"));
        }
        /*
        if($(this).first())
        readWord($(this).attr("title"));
        */
    });
});

var readWord = function(wText) {
    speechSynthesis.cancel();
    msg.text = wText;
    speechSynthesis.speak(msg);
}
var msg = new SpeechSynthesisUtterance();
msg.lang = 'zh'; //漢語
msg.volume = 50; // 聲音的音量
msg.rate = 0.7; //語速，數值，預設值是1，範圍是0.1到10
msg.pitch = 1.5; // 表示說話的音高，數值，範圍從0（最小）到2（最大）。預設值為1
var msgPlayed = false;
var msgPlayTime = 0;