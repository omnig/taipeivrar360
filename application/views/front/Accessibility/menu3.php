<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/accessibility2.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!--<script src="js/ARPlate.js"></script>//-->
    <script src="<?= "js/accessibility2.js?v=" . date("His") ?>"></script>
     <script nonce="cm1vaw==">
        window.onload = function(){
            let menuGenerator = new acc_menu();
            console.log(menuGenerator);
            menuGenerator.navDots = true;
            menuGenerator.drawMenu("data/main_menu.json", "main_menu");
        }
    </script>
</head>

<body>
    <br>
    <nav class="main_menu"></nav>
</body>

</html>