<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Location Based AR.js demo</title>
    <script src="js/aframe.min.js"></script>
    <script src="js/aframe-look-at-component.min.js"></script>
    <script src="js/aframe-ar-nft.js"></script>
     <script nonce="cm1vaw==">
        let Latitude = 0;
        let Longitude = 0;
        var id, target, options;

        function success(pos) {
            var crd = pos.coords;
            Latitude = crd.latitude;
            Longitude = crd.longitude;
            document.getElementById("loc_lati").innerHTML = Latitude;
            document.getElementById("loc_long").innerHTML = Longitude;
            console.log("your position is ==> " + crd.latitude + " , " + crd.longitude);
            
            //document.querySelector('a-text').setAttribute('gps-entity-place', `latitude: ${crd.latitude}; longitude: ${crd.longitude};`)

            if (target.latitude === crd.latitude && target.longitude === crd.longitude) {
                console.log('Congratulations, you reached the target');
                navigator.geolocation.clearWatch(id);
            }
        }

        function error(err) {
            console.warn('ERROR(' + err.code + '): ' + err.message);
        }

        target = {
            latitude: 0,
            longitude: 0
        };

        options = {
            enableHighAccuracy: false,
            timeout: 100,
            maximumAge: 0
        };

        //id = navigator.geolocation.watchPosition(success, error, options);

        window.onload = () => {
            navigator.geolocation.getCurrentPosition((position) => {
                document.querySelector('a-text').setAttribute('gps-entity-place', `latitude: ${position.coords.latitude}; longitude: ${position.coords.longitude};`)
                document.querySelector('a-entity').setAttribute('gps-entity-place', `latitude: ${position.coords.latitude}; longitude: ${position.coords.longitude};`)
            });
        }
    </script>
    <style type="text/css">
        .location_pad{
            top : 10px;
            left : 10px;
            width : 300px;
            height : 30px;
            font-size : 12px;
            color : #00ff00;
            position : fixed;
            z-index : 999;
        }
    </style>
</head>

<body style="margin: 0; overflow: hidden;">
    <div class="location_pad">
        Your&nbsp;position&nbsp;=>&nbsp;
        <span class="location_text">Latitude&nbsp;:&nbsp;<span id="loc_lati"></span></span>
        <span class="location_text">Longitude&nbsp;:&nbsp;<span id="loc_long"></span></span>
    </div>
    <a-scene vr-mode-ui="enabled: false" embedded arjs="sourceType: webcam; sourceWidth:1280; sourceHeight:960; displayWidth: 1280; debugUIEnabled: true;">
        <a-text value="No markers around" look-at="[gps-camera]" scale="5 5 5" rotation="180 0 0" gps-entity-place="latitude: <add-your-latitude>; longitude: <add-your-longitude>;"></a-text>
        <a-entity gltf-model="/application/views/front/assets/magnemite/scene.gltf" rotation="0 180 0" scale="0.005 0.005 0.005" gps-entity-place="longitude: 481.566685; latitude: 25.078930;" animation-mixer/>
        <a-camera gps-camera rotation-reader> </a-camera>
    </a-scene>
</body>

</html>