<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name="apple-mobile-web-app-capable" content="yes">

    <title>Google map demo</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <style>
        /* Set the size of the div element that contains the map */
        #map {
            /*切成依螢幕寬度的半圓*/
            height: 50vw;
            width: 99vw;
            border-radius: 100vw 100vw 0 0;

            /*固定在最下面*/
            position: fixed !important;
            left: 0;
            bottom: 0;
        }
    </style>
</head>


<body>
<div>Device Type : <span id="device_type">not trigger</span></div>
<div>Geolocation Heading : <span id="geolocation_heading">not trigger</span></div>
<div>test step : <span id="step">not trigger</span></div>
<div>Hybrid Compass : <span id="compass">not trigger</span></div>

<div id="map"></div>

 <script nonce="cm1vaw==">
    let map, infoWindow, self_maker, load_flag = false;
    const isIOS = /iPad|iPhone|iPod/.test(navigator.userAgent);
    $('#device_type').html((isIOS) ? 'IOS' : 'Android');
    console.log((isIOS) ? 'is ios' : 'not ios');

    function initMap() {
        map = new google.maps.Map(document.getElementById("map"), {
            center: {lat: 25.03818104353788, lng: 121.56434310542541},
            zoom: 18,
        });
        infoWindow = new google.maps.InfoWindow();

        if (navigator.geolocation) {
            $('#step').html(1);
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    $('#step').html(2);
                    const pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                    };
                    self_maker = new google.maps.Marker({
                        position: pos,
                        map: map
                    });
                    self_maker.setIcon({
                        path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                        scale: 8,
                        // rotation: heading,
                    });
                    $('#step').html(3);

                    startCompass();
                },
                () => {
                    handleLocationError(true, infoWindow, map.getCenter());
                }
            );

            navigator.geolocation.watchPosition(
                (position) => {
                    const pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                    };
                    const heading = position.coords.heading;
                    console.log('watchPosition' + pos);
                    console.log('watchPosition' + heading);
                    $('#geolocation_heading').html(heading);
                    map.panTo(pos);
                    self_maker.setPosition(pos);
                },
                () => {
                    handleLocationError(true, infoWindow, map.getCenter());
                },
                {
                    enableHighAccuracy: true,//開啟高精確度
                    timeout: Infinity,//機器能夠等待方法回傳位置的最長時間（單位是毫秒）
                    maximumAge: 0,//位置緩存時間，以ms為單位
                }
            );
        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
        }
    }

    //處理陀螺儀
    function startCompass() {
        $('#step').html(4);
        if (window.DeviceOrientationEvent) {
            $('#step').html(5);
            if (isIOS) {
                $('#step').html(6);
                window.DeviceOrientationEvent.requestPermission().then(response => {
                    if (response === 'granted') {
                        $('#step').html(7);
                        window.addEventListener("deviceorientation", compassHandler, true);
                    } else if (result.state === 'prompt') {
                        $('#step').html(8);
                        handleLocationError('need prompt', infoWindow, map.getCenter())
                    } else {
                        $('#step').html(9);
                        handleLocationError('不支援DeviceOrientationEvent', infoWindow, map.getCenter())
                    }
                }).catch(function (error) {
                    $('#step').html(10);
                    $('#step').html(error)
                    handleLocationError(error, infoWindow, map.getCenter())
                })
            } else {
                $('#step').html(11);
                window.addEventListener("deviceorientationabsolute", compassHandler, true);
            }
        } else {
            $('#step').html(12);
            console.log('該裝置不支援DeviceOrientationEvent');
            handleLocationError(false, infoWindow, map.getCenter())
        }
    }

    function compassHandler(e) {
        $('#step').html(13);
        let compass = 0;
        if (isIOS) {
            compass = e.webkitCompassHeading
        } else {
            compass = Math.abs(e.alpha - 360)
        }
        $('#step').html(14);
        self_maker.setIcon({
            path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
            scale: 8,
            rotation: compass,
        });
        $('#compass').html(compass);
    }

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        if (typeof browserHasGeolocation === "boolean") {
            infoWindow.setContent(
                browserHasGeolocation
                    ? "Error: 無法取得當前位置資訊，請同意取得位置權限。"
                    : "Error: 您的裝置或瀏覽器不支援定位功能。"
            );
        }else{
            infoWindow.setContent(browserHasGeolocation);
        }
        infoWindow.open(map);
    }
</script>

<script async
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDgGVTAMIyky-A-Udkhic7MvUrSya6mdwU&callback=initMap"></script>
</body>
</html>