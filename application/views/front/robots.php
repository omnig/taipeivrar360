<?php if (ENVIRONMENT === 'production') : ?>
    User-agent: *
    Allow: /
    Disallow: /admin
    Disallow: /sa
    Disallow: /.git
    Disallow: /user_guide
    sitemap: <?= $this->config->item("server_base_url") ?>sitemap.xml
<?php else: ?>
    User-agent: *
    Disallow: /
<?php endif; 