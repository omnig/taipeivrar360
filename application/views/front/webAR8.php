<!DOCTYPE html>
<html>

<head>
    <title>Hello, WebVR! - A-Frame</title>
    <meta name="description" content="Hello, WebVR! - A-Frame">
    <script src="https://aframe.io/releases/1.2.0/aframe.min.js"></script>
    <script src="https://unpkg.com/aframe-html-shader@0.2.0/dist/aframe-html-shader.min.js"></script>
    <script src="https://unpkg.com/aframe-look-at-component@0.8.0/dist/aframe-look-at-component.min.js"></script>
    <script src="https://raw.githack.com/AR-js-org/AR.js/master/aframe/build/aframe-ar-nft.js"></script>
</head>

<body>
     <script nonce="cm1vaw==">
        AFRAME.registerComponent('update-repeat', {
            dependencies: ['material'],

            init: function() {
                var texture = this.el.components.material.material.map;
                texture.repeat = new THREE.Vector2(-1, 1);
                texture.needsUpdate = true;
            }
        });
    </script>

    <div id="texture" style="width: 200px; height: 100px; position: fixed; left: 0; top: 0; z-index: -1; overflow: hidden">
        <div style="width : 100%; height : 40px; padding : 5px; border-radius : 5px; background : rgb(255, 255, 255)">
            <div style="width : 40px; height : 40px; line-height : 40px; border-radius : 5px; background : rgb(114, 177, 194); color : rgb(255, 255, 255); text-align : center; vertical-align : middle; font-family : Arial, Helvetica, sans-serif; display : inline-block;">AR</div>
            <div style="width : 120px; height : 40px; line-height : 20px; display : inline-block;">
                <span style="font-weight : 800">鴻圖測試111</span>
            </div>
        </div>
        <div style="width : 0px; height : 0px; border-top : 10px #ffffff solid; border-left : 5px transparent solid; border-right : 5px transparent solid; border-bottom : 0px; margin-left : auto; margin-right : auto;"></div>
    </div>
    <a-scene
        cursor='rayOrigin: mouse; fuse: true; fuseTimeout: 0;'
        raycaster="objects: [clickhandler];"
        vr-mode-ui="enabled: false"
		embedded
		arjs='sourceType: webcam; debugUIEnabled: true;'>
        <a-entity geometry="primitive: plane; height: 10; width: 20" material="shader: html; target: #texture; side: double; width: 500; height: 300; transparent: true" look-at="[gps-camera]" gps-entity-place="latitude: 25.0805982; longitude: 121.5646119;"></a-entity>
        <a-camera  gps-camera rotation-reader></a-camera>
    </a-scene>
</body>

</html>