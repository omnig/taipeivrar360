<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!--
    <script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDgGVTAMIyky-A-Udkhic7MvUrSya6mdwU&v=beta&chinnel=2"></script>
    //-->
    <script src="https://unpkg.com/@googlemaps/markerclusterer/dist/index.min.js"></script>
    <script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDgGVTAMIyky-A-Udkhic7MvUrSya6mdwU&callback=initMap">
    </script>
     <script nonce="cm1vaw==">
        let map;
        const taipeiOneOOne = {
            lat: 25.034186758084147,
            lng: 121.56451480720568
        };

        function initMap() {
            map = new google.maps.Map(document.getElementById("map"), {
                center: taipeiOneOOne,
                zoom: 10,
            });
            const infoWindow = new google.maps.InfoWindow({
                content: "",
                disableAutoPan: true,
            });
            // Create an array of alphabetical characters used to label the markers.
            const labels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            // Add some markers to the map.
            const markers = locations.map((position, i) => {
                const label = labels[i % labels.length];
                const marker = new google.maps.Marker({
                    position,
                    label,
                });

                // markers can only be keyboard focusable when they have click listeners
                // open info window when marker is clicked
                marker.addListener("click", () => {
                    infoWindow.setContent(label);
                    infoWindow.open(map, marker);
                });
                return marker;
            });

            // Add a marker clusterer to manage the markers.
            new MarkerClusterer({
                markers,
                map
            });
        }

        const locations = [{
                lat: 25.034186758084147,
                lng: 121.56451480720568
            },
            {
                lat: 25.080804279834944,
                lng: 121.56477248197842
            }
        ];

        window.initMap = initMap;
    </script>
    <style>
        #map {
            height: 100%;
        }

        html,
        body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>
</head>

<body>
    <div id="map"></div>
</body>

</html>