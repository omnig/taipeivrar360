<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>

        <!-- BEGIN: BASE PLUGINS  -->
        <link href="<?= base_url("assets/plugins/cubeportfolio/css/cubeportfolio.min.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("assets/plugins/owl-carousel/owl.carousel.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("assets/plugins/owl-carousel/owl.theme.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("assets/plugins/owl-carousel/owl.transitions.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("assets/plugins/fancybox/jquery.fancybox.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("assets/plugins/slider-for-bootstrap/css/slider.css") ?>" rel="stylesheet" type="text/css" />
        <!-- END: BASE PLUGINS -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?= base_url("assets/base/css/plugins.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("assets/base/css/components.css") ?>" id="style_components" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("assets/base/css/themes/default.css") ?>" rel="stylesheet" id="style_theme" type="text/css" />
        <link href="<?= base_url("assets/base/css/custom.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("css/front.css") ?>" rel="stylesheet" type="text/css" />
        <!-- END THEME STYLES -->
    </head>
    <body class="c-layout-header-fixed c-layout-header-mobile-fixed c-layout-header-topbar c-layout-header-topbar-collapse">
        <?php include APPPATH . "views/{$this->controller_name}/templates/fb_js_sdk.php"   //Facebook SDK ?>
        <!-- BEGIN: LAYOUT/HEADERS/HEADER-1 -->
        <!-- BEGIN: HEADER -->
        <header class="c-layout-header c-layout-header-4 c-layout-header-default-mobile" data-minimize-offset="80">
            <?php include APPPATH . "views/{$this->controller_name}/templates/header_top.php"   //最上端切換語系列 ?>
            <?php include APPPATH . "views/{$this->controller_name}/templates/header_menu.php"  //主menu ?>
        </header>
        <!-- END: HEADER -->
        <!-- END: LAYOUT/HEADERS/HEADER-1 -->
        <!-- BEGIN: CONTENT/USER/FORGET-PASSWORD-FORM -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/forget_password_form.php"  //忘記密碼表單 ?>
        <!-- END: CONTENT/USER/FORGET-PASSWORD-FORM -->
        <!-- BEGIN: CONTENT/USER/SIGNUP-FORM -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/signup_form.php"  //註冊表單 ?>
        <!-- END: CONTENT/USER/SIGNUP-FORM -->
        <!-- BEGIN: CONTENT/USER/LOGIN-FORM -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/login_form.php"  //登入表單 ?>
        <!-- END: CONTENT/USER/LOGIN-FORM -->

        <div class="c-layout-page">
            <div class="c-content-box c-size-md">
                <div class="container">
                    <div class="row c-page-faq-2">
                        <div class="col-sm-3">
                            <ul class="nav nav-tabs c-faq-tabs" data-tabs="tabs">
                                <?php foreach ($faq_category as $id => $row) : ?>
                                    <li class="<?= ($id == 0) ? 'active' : '' ?>">
                                        <a href="#tab_fc_id_<?= $row->fc_id ?>" data-toggle="tab"><?= $row->{"fc_title_{$this->session->i18n}"} ?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <div class="col-sm-9">
                            <div class="tab-content">
                                <?php foreach ($faq_category as $id => $row) : ?>
                                    <div class="tab-pane<?= ($id == 0) ? ' active' : '' ?>" id="tab_fc_id_<?= $row->fc_id ?>">
                                        <div class="c-content-title-1">
                                            <h3 class="c-font-uppercase c-font-bold"><?= $row->{"fc_title_{$this->session->i18n}"} ?></h3>
                                            <div class="c-line-left"></div>
                                        </div>
                                        <div class="c-content-accordion-1">
                                            <div class="panel-group" id="accordion<?= $id ?>" role="tablist">
                                                <?php if (isset($faq[$row->fc_id])) : ?>
                                                    <?php foreach ($faq[$row->fc_id] as $qid => $question) : ?>
                                                        <div class="panel">
                                                            <div class="panel-heading" role="tab" id="heading<?= $question->f_id ?>">
                                                                <h4 class="panel-title">
                                                                    <a class="align_item<?= ($qid == 0) ? ' active' : '' ?>" data-toggle="collapse" data-parent="#accordion<?= $id ?>" href="#collapse<?= $question->f_id ?>" aria-expanded="false" aria-controls="collapse<?= $question->f_id ?>">
                                                                        <i class="c-theme-font fa fa-map-marker c-theme-font"></i> <?= $question->{"f_title_{$this->session->i18n}"} ?>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse<?= $question->f_id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?= $question->f_id ?>">
                                                                <div class="panel-body"> <?= nl2br($question->{"f_description_{$this->session->i18n}"}) ?> </div>
                                                                <?php if ($question->f_image_filename) : ?>
                                                                    <div class="panel-body">
                                                                        <img class="img-responsive" src="<?= base_url("upload/faq/{$question->f_image_filename}") ?>">
                                                                    </div>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php"  //尾部資訊 ?>

        <!-- BEGIN: LAYOUT PLUGINS -->
        <script src="<?= base_url("assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/plugins/owl-carousel/owl.carousel.min.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/plugins/counterup/jquery.counterup.min.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/plugins/counterup/jquery.waypoints.min.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/plugins/fancybox/jquery.fancybox.pack.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/plugins/slider-for-bootstrap/js/bootstrap-slider.js") ?>" type="text/javascript"></script>
        <!-- END: LAYOUT PLUGINS -->
        <!-- BEGIN: THEME SCRIPTS -->
        <script src="<?= base_url("assets/base/js/components.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/base/js/components-shop.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/base/js/app.js") ?>" type="text/javascript"></script>
         <script nonce="cm1vaw==">
            $(document).ready(function ()
            {
                App.init(); // init core    
            });
        </script>
        <!-- END: THEME SCRIPTS -->
        <!-- END: LAYOUT/BASE/BOTTOM -->
    </body>

</html>