<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>GeoAR.js demo</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://aframe.io/releases/0.9.2/aframe.min.js"></script>
    <script src="https://unpkg.com/aframe-look-at-component@0.8.0/dist/aframe-look-at-component.min.js"></script>
    <script src="https://raw.githack.com/AR-js-org/AR.js/master/aframe/build/aframe-ar-nft.js"></script>
    <style type="text/css">
        .info_panel{
            top : 10px;
            left : 10px;
            width : 100vw;
            min-height : 100px;
            position : fixed;
            z-index : 999;
            color : #00ff00;
            font-size : 12px;
        }

        .control_panel{
            left : 10px;
            bottom : 30px;
            position : fixed;
            z-index : 999;
        }

        .val_btn{
            margin-left : 20px;
        }
    </style>
</head>

 <script nonce="cm1vaw==">
    let pos_lati = 0;
    let pos_long = 0;
    let target_btn = "";
    let other_btn = "";

    AFRAME.registerComponent('clickhandler', {
        init: function() {
            this.el.addEventListener('click', () => {
                alert('Clicked!')
            });
    }});

    window.onload = function(){
        navigator.geolocation.getCurrentPosition((position) => {
            pos_lati = position.coords.latitude;
            pos_long = position.coords.longitude;
            $("#loc_lati").html(pos_lati);
            $("#loc_long").html(pos_long);
            $("#plate_01").attr("gps-entity-place", "latitude: " + (pos_lati - 10) + "; longitude: " + pos_long + ";");
        });
    }

    loc_sel_change = function(btn_obj){
        target_btn = $(btn_obj).attr("id");
        other_btn = target_btn == "btn_lati" ? "btn_long" : "btn_lati";
        //console.log(target_btn);

        $("#" + target_btn).removeClass("btn-secondary");
        $("#" + target_btn).addClass("btn-light");
        $("#" + other_btn).removeClass("btn-light");
        $("#" + other_btn).addClass("btn-secondary");
        //console.log($("#" + target_btn).attr("class"));
        //console.log($("#" + other_btn).attr("class"));
    }

    pos_change = function(ct){
        console.log(ct);
        console.log(target_btn + " , " + other_btn);
        if(target_btn != "" && other_btn != ""){
            if(target_btn == "btn_lati"){
                pos_lati += ct;
            }else{
                pos_long += ct;
            }

            $("#plate_01").attr("gps-entity-place", "latitude: " + pos_lati + "; longitude: " + pos_long + ";");
            $("#loc_lati").html(pos_lati);
            $("#loc_long").html(pos_long);
            console.log(pos_lati + " , " + pos_long);
        }
    }
</script>

<body style='margin: 0; overflow: hidden;'>
    <div class="info_panel">
        <div>Latitude&nbsp;:&nbsp;<span id="loc_lati"></span></div>
        <div>Longitude&nbsp;:&nbsp;<span id="loc_long"></span></div>
    </div>
    <div class="control_panel">
        <buttpn id="btn_lati" type="button" class="btn btn-sm btn-secondary" onclick="loc_sel_change(this)">Latitude</buttpn>
        <buttpn id="btn_long" type="button" class="btn btn-sm btn-secondary" onclick="loc_sel_change(this)">Longitude</buttpn>
        <button type="button" class="btn btn-sm btn-secondary val_btn" onclick="pos_change(1)">+</buttpn>
        <button type="button" class="btn btn-sm btn-secondary val_btn" onclick="pos_change(1)">-</buttpn>
    </div>
    <a-scene
        cursor='rayOrigin: mouse; fuse: true; fuseTimeout: 0;'
        raycaster="objects: [clickhandler];"
        vr-mode-ui="enabled: false"
		embedded
		arjs='sourceType: webcam; debugUIEnabled: false;'>

        <a-image id="plate_01" clickhandler width="10" height="2" src="./images/AR_text_panel_01.png" look-at="[gps-camera]" gps-entity-place="latitude: 25.0805982; longitude: 121.5646119;"></a-image>
        <a-sphere id="sphere_01" look-at="[gps-camera]" gps-entity-place="latitude: 25.0805982; longitude: 121.5646119;" radius="0.25" color="#EF2D5E"></a-sphere>
        <!--<a-sphere id="sphere_02" look-at="[gps-camera]" gps-entity-place="latitude: 25.0803982; longitude: 121.5646119;" radius="0.25" color="#1aba27"></a-sphere>//-->
        <a-camera gps-camera rotation-reader>
		</a-camera>
	</a-scene>
</body>