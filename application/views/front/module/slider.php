
<div class="margin-top-40" data-slider="owl" data-items="1" data-auto-play="4000" data-single-item="true">
    <div class="owl-carousel owl-theme c-theme">
        <?php foreach ($home_slide as $id => $row) : ?>
            <div class="c-content-product-2 c-bg-white c-border">
                <div class="c-content-overlay">
                    <a href="<?= $row->hs_link ?>">
                        <img width="100%" src="<?= base_url("upload/home_slide/{$row->hs_path}") ?>" />
                    </a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>