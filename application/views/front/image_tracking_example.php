<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <title>Image tracking demo</title>

    <script src='<?=base_url('assets/js/aframe-master.min.js')?>'></script>
    <script src='<?=base_url('assets/js/aframe-ar-nft.js')?>'></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


     <script nonce="cm1vaw==">
        window.onload = function () {
            AFRAME.registerComponent('videohandler', {
                init: function () {
                    var marker = this.el;

                    this.vid = document.querySelector("#vid");

                    marker.addEventListener('markerFound', function () {
                        this.vid.play();
                    }.bind(this));

                    marker.addEventListener('markerLost', function () {
                        this.vid.pause();
                        this.vid.currentTime = 0;
                    }.bind(this));
                }
            });
        };
    </script>

     <script nonce="cm1vaw==">
        AFRAME.registerComponent('markerhandler1', {
            init: function () {
                let model = document.querySelector('#girl_model');
                this.el.addEventListener('markerFound', function () {
                    //alert('front card detecting!');
                    $('#girl_model').attr('visible','true');
                    let markerPosition = this.el.object3D.position;
                    console.log(markerPosition);

                    checkPostion = setInterval(() => {
                        model.setAttribute("position", this.el.getAttribute("position"));
                        // model.setAttribute("rotation", this.el.getAttribute("rotation"));

                        // console.log('nft-marker:'+this.el.getAttribute("position"));
                        // console.log('nft-marker:'+model.getAttribute("position"));
                        // do what you want with the distance:
                        //console.log(distance);
                    }, 100);
                }.bind(this));

                this.el.addEventListener('markerLost', function () {
                    $('#girl_model').attr('visible','false');
                    clearInterval(checkPostion);
                }.bind(this));
            }
        });
        AFRAME.registerComponent('markerhandler2', {
            init: function () {

                this.el.addEventListener('markerFound', function () {
                    //alert('front card detecting!');
                    //$('#girl_model2').attr('visible','true');
                }.bind(this));

                this.el.addEventListener('markerLost', function () {
                    $('#girl_model2').attr('visible','false');
                }.bind(this));
            }
        });
    </script>


    <!-- style for the loader -->
    <style>
        .arjs-loader {
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0;
            left: 0;
            background-color: rgba(0, 0, 0, 0.8);
            z-index: 9999;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .arjs-loader div {
            text-align: center;
            font-size: 1.25em;
            color: white;
        }
    </style>
</head>

<body style="margin : 0px; overflow: hidden;">
<!-- minimal loader shown until image descriptors are loaded. Loading may take a while according to the device computational power -->
<div class="arjs-loader">
    <div>Loading, please wait...</div>
</div>
<a-scene
        vr-mode-ui="enabled: false;"
        renderer="logarithmicDepthBuffer: true; precision: medium;"
        embedded
        arjs="trackingMethod: best; sourceType: webcam; debugUIEnabled: false;"
>

    <a-entity id="girl_model" gltf-model="url(../images/webAR/models/8331_A_Toa.gltf)" position="0 0 0" scale="0.25 0.25 0.25" visible="false"></a-entity>
    <a-entity id="girl_model2" gltf-model="url(../images/webAR/models/8387_Monkey3.gltf)" position="0 0 0" scale="0.25 0.25 0.25" visible="false"></a-entity>


    <a-assets>
        <video preload="auto" id="vid" response-type="arraybuffer" loop
               crossorigin webkit-playsinline playsinline>
            <source src="../images/webAR/videos/demo.mp4">
        </video>
    </a-assets>

    <a-nft
            markerhandler1
            type="nft"
            url="https://ar-taipei-test.omniguider.com/images/webAR/image_descriptors/qr1"
            smooth="true"
            smoothCount="20"
            smoothTolerance="1"
            smoothThreshold="2"
    >
<!--        <a-entity-->
<!--                gltf-model="url(../images/webAR/models/8331_A_Toa.gltf)"-->
<!--                scale="1 1 1"-->
<!--                rotation='-90 0 0'-->
<!--                position="30 70 0"-->
<!--        >-->
<!--        </a-entity>-->
    </a-nft>

    <a-nft
            markerhandler2
            type="nft"
            url="https://ar-taipei-test.omniguider.com/images/webAR/image_descriptors/qr2"
            smooth="true"
            smoothCount="10"
            smoothTolerance=".01"
            smoothThreshold="5"
    >
        <!-- as a child of the a-nft entity, you can define the content to show. here's a GLTF model entity -->
        <a-entity
                gltf-model="url(../images/webAR/models/8387_Monkey3.gltf)"
                scale="1 1 1"
                rotation='-90 0 0'
                position="0 0 0"
        >
        </a-entity>
    </a-nft>

    <a-nft
            type="nft"
            url="https://ar-taipei-test.omniguider.com/images/webAR/image_descriptors/card_back"
            smooth="true"
            smoothCount="10"
            smoothTolerance=".01"
            smoothThreshold="5"
    >
        <!-- as a child of the a-nft entity, you can define the content to show. here's a GLTF model entity -->
        <a-entity
                gltf-model="url(../images/webAR/models/Duck.gltf)"
                scale="50 50 50"
                rotation='-90 0 0'
                position="0 0 0"
        >
        </a-entity>
    </a-nft>

    <a-nft
            videohandler
            type="nft"
            url="https://ar-taipei-test.omniguider.com/images/webAR/image_descriptors/qr3"
            smooth="true"
            smoothCount="10"
            smoothTolerance=".01"
            smoothThreshold="5"
    >
        <!--        <a-entity position="0 2 -4" rotation="0 0 0" geometry="primitive:plane;"-->
        <!--                  material="shader:gif;src:url(https://media0.giphy.com/media/xT9IgsOaEG15pWz17y/giphy.gif)">-->
        <!--        </a-entity>-->
        <a-video
                src="#vid"
                position='0 0 0'
                rotation='-90 0 0'
                width='160'
                height='90'
                transparent='true'
        >
        </a-video>
    </a-nft>
    <a-entity camera position="0 10 100"></a-entity>
</a-scene>

 <script nonce="cm1vaw==">
    window.addEventListener('load', () => {
        const camera = document.querySelector('[camera]');
        const marker = document.querySelector('a-nft');
        let check;

        // marker.addEventListener('markerFound', () => {
        //     let cameraPosition = camera.object3D.position;
        //     let markerPosition = marker.object3D.position;
        //     let distance = cameraPosition.distanceTo(markerPosition)
        //
        //     check = setInterval(() => {
        //         cameraPosition = camera.object3D.position;
        //         markerPosition = marker.object3D.position;
        //         distance = cameraPosition.distanceTo(markerPosition)
        //
        //         // do what you want with the distance:
        //         //console.log(distance);
        //     }, 100);
        // });
        //
        // marker.addEventListener('markerLost', () => {
        //     clearInterval(check);
        // })
    })
</script>
</body>