<!DOCTYPE html>
<html>

<head>
    <title>Hello, WebVR! - A-Frame</title>
    <meta name="description" content="Hello, WebVR! - A-Frame">
    <script src="https://aframe.io/releases/0.6.0/aframe.min.js"></script>
    <script src="https://unpkg.com/aframe-html-shader@0.2.0/dist/aframe-html-shader.min.js" />
</head>

<body>
     <script nonce="cm1vaw==">
        AFRAME.registerComponent('update-repeat', {
            dependencies: ['material'],

            init: function() {
                var texture = this.el.components.material.material.map;
                texture.repeat = new THREE.Vector2(-1, 1);
                texture.needsUpdate = true;
            }
        });
    </script>

    <div id="texture" style="width: 100%; height: 100%; position: fixed; left: 0; top: 0; z-index: -1; overflow: hidden">
        <p style="position: relative; top: 20px; font-size: 72px">哈囉</p>
        <p style="position: relative; top: 20px; font-size: 48px">curvy text</p>
    </div>

    <a-scene>
        <a-entity geometry="primitive: plane; radius: 3; width: 3; height: 3; segmentsRadial: 48; thetaLength: -160; openEnded: true" material="shader: html; target: #texture; side: double; width: 500; height: 300; transparent: true" update-repeat position="0 0 -4" rotation="0 0 0" gps-entity-place="latitude: 25.0805982; longitude: 121.5646119;"></a-entity>
        <a-camera gps-camera rotation-reader>
		</a-camera>
      <a-sky color=" #FAFAFA">
            </a-sky>
    </a-scene>
</body>

</html>