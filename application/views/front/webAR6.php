<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
     <script nonce="cm1vaw==">
        if (window.DeviceOrientationEvent) {
            window.addEventListener("deviceorientation", function(event) {
                // alpha: rotation around z-axis
                var rotateDegrees = event.alpha;
                let rotateDegrees_IOS = null;
                let rotateDegrees_Android = null;
                // gamma: left to right


                //     判斷是否為 iOS 裝置
                if (event.webkitCompassHeading) {
                    rotateDegrees = event.webkitCompassHeading; // iOS 裝置必須使用 event.webkitCompassHeading
                    /*
                    compass.style.WebkitTransform = 'rotate(-' + rotateDegrees + 'deg)';
                    show.innerHTML = rotateDegrees;
                    */
                    rotateDegrees_IOS = rotateDegrees;
                } else {
                    rotateDegrees = event.alpha;
                    /*
                    webkitAlpha = rotateDegrees;
                    if (!window.chrome) {
                        webkitAlpha = rotateDegrees - 270;
                    }
                    */
                    rotateDegrees_Android = rotateDegrees;
                }


                var leftToRight = event.gamma;
                // beta: front back motion
                var frontToBack = event.beta;
                //Latitude
                let latitude = null;
                //Longitude
                let longitude = null;

                navigator.geolocation.getCurrentPosition((position) => {
                    latitude = position.coords.latitude;
                    longitude = position.coords.longitude;
                });

                handleOrientationEvent(frontToBack, leftToRight, rotateDegrees, rotateDegrees_IOS, rotateDegrees_Android);
            }, true);
        }

        let options = {
            enableHighAccuracy: false,
            timeout: 5000,
            maximumAge: 0
        };

        function compassHeading(alpha, beta, gamma) {

            // Convert degrees to radians
            var alphaRad = alpha * (Math.PI / 180);
            var betaRad = beta * (Math.PI / 180);
            var gammaRad = gamma * (Math.PI / 180);

            // Calculate equation components
            var cA = Math.cos(alphaRad);
            var sA = Math.sin(alphaRad);
            var cB = Math.cos(betaRad);
            var sB = Math.sin(betaRad);
            var cG = Math.cos(gammaRad);
            var sG = Math.sin(gammaRad);

            // Calculate A, B, C rotation components
            var rA = -cA * sG - sA * sB * cG;
            var rB = -sA * sG + cA * sB * cG;
            var rC = -cB * cG;

            // Calculate compass heading
            var cHeading = Math.atan(rA / rB);

            // Convert from half unit circle to whole unit circle
            if (rB < 0) {
                cHeading += Math.PI;
            } else if (rA < 0) {
                cHeading += 2 * Math.PI;
            }

            // Convert radians to degrees
            cHeading *= 180 / Math.PI;

            return cHeading;

        }

        var handleOrientationEvent = function(frontToBack, leftToRight, rotateDegrees, rotateDegrees_IOS, rotateDegrees_Android) {
            // do something amazing
            $("#loc_alpha").html(rotateDegrees);
            $("#loc_beta").html(frontToBack);
            $("#loc_gamma").html(leftToRight);
            $("#loc_compass").html(compassHeading(rotateDegrees, frontToBack, leftToRight));
            $("#loc_alpha_ios").html(rotateDegrees_IOS);
            $("#loc_alpha_and").html(rotateDegrees_Android);
        };

        window.onload = function() {
            navigator.geolocation.getCurrentPosition((position) => {
                pos_lati = position.coords.latitude;
                pos_long = position.coords.longitude;
                $("#loc_lati").html(pos_lati);
                $("#loc_long").html(pos_long);
            });

            let options = {
                enableHighAccuracy: false,
                timeout: 5000,
                maximumAge: 0
            };

            id = navigator.geolocation.watchPosition(getMyPos, getMyPos, options);
        }

        const getMyPos = function(result) {
            console.log(result);
            let d = new Date();
            let location_tag = document.createElement("div");
            let LocStr = ('0' + d.getHours()).slice(-2) + ":";
            LocStr += ('0' + d.getMinutes()).slice(-2) + ":";
            LocStr += ('0' + d.getSeconds()).slice(-2) + " => ";
            LocStr += result.coords.latitude + " , " + result.coords.longitude;

            location_tag.innerHTML = LocStr;

            if (result.coords) {
                $("#loc_lati").html(result.coords.latitude);
                $("#loc_long").html(result.coords.longitude);
            } else {}
        }
    </script>
    <style type="text/css">
        .info_panel {
            font-size: 20px;
        }
    </style>
</head>

<body>
    <div id="info_panel" class="info_panel">
        <div>Latitude&nbsp;:&nbsp;<span id="loc_lati"></span></div>
        <div>Longitude&nbsp;:&nbsp;<span id="loc_long"></span></div>
        <div>RotateDegrees&nbsp;:&nbsp;<span id="loc_alpha"></span></div>
        <div>RotateDegrees-IOS&nbsp;:&nbsp;<span id="loc_alpha_ios"></span></div>
        <div>RotateDegrees-Android&nbsp;:&nbsp;<span id="loc_alpha_and"></span></div>
        <div>FrontToBack&nbsp;:&nbsp;<span id="loc_beta"></span></div>
        <div>LeftToRight&nbsp;:&nbsp;<span id="loc_gamma"></span></div>
        <div>CompassHeading&nbsp;:&nbsp;<span id="loc_compass"></span></div>
    </div>
</body>

</html>