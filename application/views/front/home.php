<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>桃園市政府 VR Taoyuan</title>
    <base href="/">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <style>
    #pre-loader {
        display: flex;
        justify-content: center;
        align-items: center;
        width: 100%;
        height: 100vh;
        background: #fff;
        position: absolute;
    }

    @font-face {
        font-family: msjhbd;
        src: url(<?=base_url("")?>assets/msjhbd.ttf);
    }

    * {
        font-family: "msjhbd", "微軟正黑體", \5FAE\8EDF\6B63\9ED1\9AD4, \65B0\7D30\660E\9AD4, "Noto Sans TC", "sans-serif" !important;
    }

    *.fa {
        font-family: "FontAwesome" !important;
    }
    </style>
    <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>
</head>

<body class="font-chinese overflow-hidden">
    <?php include APPPATH . "views/{$this->controller_name}/templates/google_tag.php" ?>
    <?php include APPPATH . "views/{$this->controller_name}/templates/header_menu.php" ?>
    <!-- <div id="pre-loader" style="z-index:9999;" class="flex justify-center items-center w-full h-screen bg-white absolute">
        <img style="width:250px; height:250px;" src="<?=base_url("")?>assets/images/giphy.gif" alt="">
	</div> -->
    <?php include APPPATH . "views/{$this->controller_name}/templates/footer.php" ?>
     <script nonce="cm1vaw==">
    </script>
</body>

</html>