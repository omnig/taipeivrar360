<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>GeoAR.js demo</title>
    <script src="https://aframe.io/releases/1.0.4/aframe.min.js"></script>
    <script src="https://raw.githack.com/AR-js-org/AR.js/master/aframe/build/aframe-ar-nft.js"></script>
</head>

<body style='margin: 0; overflow: hidden;'>
     <script type="text/javascript" nonce="cm1vaw==">
            AFRAME.registerComponent('clickhandler', {
                init: function() {
                    console.log("thie.el ======> ");
                    console.log(this.el);
                    this.el.addEventListener('click', () => {
                        alert('Clicked!')
                    });
                }
            });
    </script>
    <a-scene cursor='rayOrigin: mouse; fuse: true; fuseTimeout: 0;' raycaster="objects: [clickhandler];" vr-mode-ui="enabled: false" embedded arjs='sourceType: webcam; sourceWidth:1280; sourceHeight:960; displayWidth: 1280; displayHeight: 960; debugUIEnabled: true;'>

        <!--<a-text value="This will always face the user."  look-at="[gps-camera]" scale="5 5 5"
            gps-entity-place="latitude: 25.0805982; longitude: 121.5646119;" transparent="true"></a-text>
        
        <a-image width="8" height="1.5" src="./images/AR_text_panel_01.png" look-at="[gps-camera]" gps-entity-place="latitude: 25.0805982; longitude: 121.5646119;"></a-image>
        <a-text value="a-text虛擬招牌" z-offset="1" look-at="[gps-camera]" scale="5 5 5"
            gps-entity-place="latitude: 25.0805982; longitude: 121.5646119;"></a-text>//-->
        <a-image clickhandler width="10" height="2" src="./images/AR_text_panel_01.png" rotate="90 0 0" look-at="[gps-camera]" gps-entity-place="latitude: 25.0805982; longitude: 121.5646119;"></a-image>
        <a-camera gps-camera rotation-reader>
        </a-camera>
    </a-scene>
</body>