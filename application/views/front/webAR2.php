<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>GeoAR.js demo</title>
    <script src="https://aframe.io/releases/1.3.0/aframe.min.js"></script>
    <script src="https://unpkg.com/aframe-look-at-component@0.8.0/dist/aframe-look-at-component.min.js"></script>
    <script src="https://raw.githack.com/AR-js-org/AR.js/master/aframe/build/aframe-ar-nft.js"></script>
     <script nonce="cm1vaw==">
        THREEx.ArToolkitContext.baseURL = 'https://raw.githack.com/jeromeetienne/ar.js/master/three.js/'
        let Latitude = 0;
        let Longitude = 0;
        var id, target, options;

        window.onload = () => {
            navigator.geolocation.getCurrentPosition((position) => {
                console.log(`your location => ${position.coords.latitude}, ${position.coords.longitude}`);
                /*document.querySelector('a-text').setAttribute('gps-entity-place', `latitude: ${position.coords.latitude}; longitude: ${position.coords.longitude};`);
                document.querySelector('a-entity').setAttribute('gps-entity-place', `latitude: ${position.coords.latitude}; longitude: ${position.coords.longitude};`);*/
                document.querySelector('a-box').setAttribute('gps-entity-place', `latitude: ${position.coords.latitude}; longitude: ${position.coords.longitude};`)
                console.log(document.querySelector('a-box').getAttribute('gps-entity-place'));
            });
        }
    </script>
</head>

<body style='margin: 0; overflow: hidden;'>
    <a-scene
        vr-mode-ui="enabled: false"
		embedded
        arjs='sourceType: webcam; debugUIEnabled: false;'>
        <a-entity gltf-model="/application/views/front/assets/magnemite/scene.gltf" rotation="0 180 0" scale="0.10 0.10 0.10" gps-entity-place="latitude: 25.0805982; longitude: 121.5646119;" animation-mixer/>
        <a-box material="color: green" scale="0.15 0.15 0.15" position="0 30 0" gps-entity-place="latitude: 25.0805982; longitude: 121.5646119"/>
        <a-link scale="0.15 0.15 0.15" title="test" position="0 50 0" gps-entity-place="latitude: 25.0805982; longitude: 121.5646119"/>
        <a-camera gps-camera rotation-reader></a-camera>
	</a-scene>
</body>