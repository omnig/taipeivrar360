<div class="border-b-2 sm:h-76px fixed w-full bg-white z-10">
    <header class="container mx-auto">
        <div class="flex justify-between items-center">
            <div class="px-2 block sm:hidden"><button class="appearance-none" type="button"><img alt="Menu"class="w-6 h-6"src="<?=base_url("")?>assets/svg/menu.svg"></button></div>
            <div><img alt="Logo"class="w-12 h-12 sm:h-18 sm:w-18"src="<?=base_url("")?>assets/svg/logo.svg"></div>
                <div class="hidden sm:block">
                    <div class="flex">
                        <button class="text-center mx-4" routerlink="/" tabindex="0">
                            <img alt="首頁"class="h-8 w-8"src="<?=base_url("")?>assets/svg/home.svg"><span class="block">首頁</span>
                        </button>
                        <button class="text-center mx-4" routerlink="/ar" tabindex="0">
                            <img alt="AR"class="h-8 w-8"src="<?=base_url("")?>assets/svg/ar.svg"><span class="block">AR</span>
                        </button>
                        <button class="text-center mx-4">
                            <img alt="檔案分享"class="h-8 w-8"src="<?=base_url("")?>assets/svg/upload.svg"><span class="block">檔案分享</span>
                        </button>
                        <button class="text-center mx-4">
                            <img alt="登入"class="h-8 w-8"src="<?=base_url("")?>assets/svg/user.svg"><span class="block">登入</span>
                        </button>
                        <button class="text-center mx-4" routerlink="/search" tabindex="0"><img alt="搜尋"class="h-8 w-8"src="<?=base_url("")?>assets/svg/search.svg"><span class="block">搜尋</span></button>
                    </div>
                </div>
                <div class="px-2 block sm:hidden"><a routerlink="/search"href="/search"><img alt=""class="w-5 h-5"src="<?=base_url("")?>assets/svg/search.svg"></a></div>
            </div>
    </header>
</div>
<nav id="menu" style="position: fixed; width: 250px; transition: all 300ms ease 0s; height: 100%; top: 0px; left: 0px; z-index: 1049;">
    <header>
        <!---->
        <app-sidebar-menu _nghost-c1="">
            <div>
                <div class="flex justify-center border-b">
                	<img alt="Logo" class="w-24 h-24" src="<?=base_url("")?>assets/svg/logo.svg">
                </div>
                    
                <div>
                	<a class="flex items-center border-b p-4">
                		<img alt="首頁" class="h-8 w-8" src="<?=base_url("")?>assets/svg/home.svg">
                		<span class="ml-4 font-bold">首頁</span>
                	</a>
                	<a class="flex items-center border-b p-4">
                		<img alt="AR" class="h-8 w-8" src="<?=base_url("")?>assets/svg/ar.svg">
                		<span class="ml-4 font-bold">AR</span>
                	</a>
                	<a class="flex items-center border-b p-4">
                		<img alt="檔案分享" class="h-8 w-8" src="<?=base_url("")?>assets/svg/upload.svg">
                		<span class="ml-4 font-bold">檔案分享</span></a>
                        
                        <a class="flex items-center border-b p-4"><img alt="登入" class="h-8 w-8" src="<?=base_url("")?>assets/svg/user.svg"><span class="ml-4 font-bold">登入</span></a>

                    </div>
                </div>
        </app-sidebar-menu>
    </header>
</nav>