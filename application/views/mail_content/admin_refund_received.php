<div style="width:640px">
    <div><img style="width: 100%" src="<?= $this->config->item("image_base_url") . "images/mail_header.jpg" ?>"></div>
    <div style="background-color:#8cd98c;text-align: center;line-height:50px;font-weight:bold">嬉街商城【店家已同意退貨】通知信</div>
    <div><?= $this->lang->line("site_name") ?>管理員 您好：</div>
    <div>&nbsp;</div>
    <div><?= $order_store->s_name_zh ?> 同意了一項退貨申請。</div>
    <div>&nbsp;</div>
    <table style="text-align: center; width: 100%">
        <tr style="width:100%;background-color: #d64635;color:#fff">
            <td colspan="2">使用者退貨資訊</td>
        </tr>
        <tr>
            <td style="background-color: #f7d8d4;">申請退貨時間</td>
            <td><?= $refund_request->rr_timestamp ?></td>
        </tr>
        <tr>
            <td style="background-color: #f7d8d4;"><?= $this->lang->line("#訂單編號")["zh"] ?></td>
            <td><a href="<?= $this->config->item("server_base_url") ?>sa/order/edit?o_id=<?= $order->o_id ?>"><?= $order->o_order_number ?></a></td>
        </tr>
        <tr>
            <td style="background-color: #f7d8d4;">退貨原因</td>
            <td><?= nl2br($refund_request->rr_reason) ?></td>
        </tr>
    </table>
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    <table style="text-align: center; width: 100%">
        <tr style="width:100%;background-color: #d64635;color:#fff">
            <td colspan="2">店家同意退貨資訊</td>
        </tr>
        <tr>
            <td style="background-color: #f7d8d4;">回覆時間</td>
            <td style="text-align: left"><?= date("Y-m-d H:i:s") ?></td>
        </tr>
        <tr>
            <td style="background-color: #f7d8d4;">店家回覆</td>
            <td style="text-align: left">同意退貨</td>
        </tr>
        <tr style="width:100%;background-color: #d64635;color:#fff">
            <td colspan="2">店家回覆內容</td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: left"><?= nl2br($rr_store_reply) ?></td>
        </tr>
    </table>
    <div>&nbsp;</div>

    <table style="text-align: center; width: 100%">
        <tr style="width:100%;background-color: #d64635;color:#fff">
            <td colspan="5">退貨明細</td>
        </tr>
        <tr style="background-color: #f7d8d4;color: #000">
            <td>退貨商品</td>
            <?php if ($order_item->oi_style): ?>
                <td>款式</td>
            <?php endif; ?>
            <td>單價</td>
            <td>退貨數量</td>
            <td>小計</td>
        </tr>
        <tr>
            <td style="text-align:center"><?= $order_item->oi_name ?></td>
            <?php if ($order_item->oi_style): ?>
                <td style="text-align:center"><?= ($order_item->oi_style) ?></td>
            <?php endif; ?>
            <td style="text-align:center"><?= number_format($order_item->oi_price) ?></td>
            <td style="text-align:center"><?= number_format($order_item->oi_qty) ?></td>
            <td style="text-align:center"><?= number_format($order_item->oi_price * $order_item->oi_qty) ?></td>
        </tr>
    </table>
    <?php include APPPATH . "views/mail_content/template/mail_footer_admin_refund.php" ?>
</div>