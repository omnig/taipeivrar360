<div style="width:640px">
    <div><img style="width: 100%" src="<?= $this->config->item("image_base_url") . "images/mail_header.jpg" ?>"></div>
    <div style="background-color:#daf1f0;text-align: center;line-height:50px;font-weight:bold"><?= $this->lang->line("#嬉街商城【已出貨】通知信")[$order->o_buyer_i18n] ?></div>
    <div><?= sprintf($this->lang->line("親愛的 #%s 您好:")[$order->o_buyer_i18n], $order->o_buyer_name) ?></div>
    <div>&nbsp;</div>
    <div><?= sprintf($this->lang->line("#您在 %s 訂購的商品已經出貨，近期請注意收件通知。")[$order->o_buyer_i18n], $this->lang->line("site_name")) ?></div>
    <div>&nbsp;</div>
    <table style="text-align: center; width: 100%">
        <tr width="100%" style="background-color:#26a69a;color:#fff">
            <td colspan="2"><?= $this->lang->line("#出貨資訊")[$order->o_buyer_i18n] ?></td>
        </tr>
        <tr>
            <td style="background-color: #daf1f0;"><?= $this->lang->line("#出貨店家")[$order->o_buyer_i18n] ?></td>
            <td><?= $order_store->s_name_zh ?></td>
        </tr>
        <tr>
            <td style="background-color: #daf1f0;"><?= $this->lang->line("#訂單編號")[$order->o_buyer_i18n] ?></td>
            <td><a href="<?= ($order->u_id) ? $this->config->item("server_base_url") . "order_detail/" . $order->o_id : $this->config->item("server_base_url") . "order_detail?o_id=" . $order->o_id . "&token=" . $order->o_token ?>"><?= $order->o_order_number ?></a></td>
        </tr>
        <tr>
            <td style="background-color: #daf1f0;"><?= $this->lang->line("#訂購時間")[$order->o_buyer_i18n] ?></td>
            <td><?= $order->o_create_timestamp ?></td>
        </tr>
        <tr>
            <td style="background-color: #daf1f0;"><?= $this->lang->line("#出貨時間")[$order->o_buyer_i18n] ?></td>
            <td><?= $now ?></td>
        </tr>
    </table>
    <div>&nbsp;</div>
    <table style="text-align: center; width: 100%">
        <tr width="100%" style="background-color:#26a69a;color:#fff">
            <td colspan="5"><?= $this->lang->line("#出貨明細")[$order->o_buyer_i18n] ?></td>
        </tr>
        <tr style="background-color: #daf1f0;color: #000">
            <td><?= $this->lang->line("#項次")[$order->o_buyer_i18n] ?></td>
            <td><?= $this->lang->line("#商品")[$order->o_buyer_i18n] ?></td>
            <td><?= $this->lang->line("#款式")[$order->o_buyer_i18n] ?></td>
            <td><?= $this->lang->line("#單價")[$order->o_buyer_i18n] ?></td>
            <td><?= $this->lang->line("#數量")[$order->o_buyer_i18n] ?></td>
            <td><?= $this->lang->line("#小計")[$order->o_buyer_i18n] ?></td>
        </tr>
        <?php foreach ($order_item as $id => $row) : ?>
            <tr style="<?= $id++ % 2 == 1 ? "background-color: #ddd" : "" ?>;<?= $row->oi_qty < 0 ? "color:red" : "" ?>">
                <td style="text-align:center"><?= $id ?></td>
                <td style="text-align:center"><?= $row->oi_name ?></td>
                <td style="text-align:center"><?= ($row->oi_style) ? $row->oi_style : "" ?></td>
                <td style="text-align:center"><?= number_format($row->oi_price) ?></td>
                <td style="text-align:center"><?= number_format($row->oi_qty) ?></td>
                <td style="text-align:center"><?= number_format($row->oi_price * $row->oi_qty) ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
    <?php include APPPATH . "views/mail_content/template/mail_footer_nopayinfo_{$order->o_buyer_i18n}.php" ?>
</div>