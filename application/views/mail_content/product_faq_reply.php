<div style="width:640px">
    <div><img style="width: 100%" src="<?= $this->config->item("image_base_url") . "images/mail_header.jpg" ?>"></div>
    <div style="background-color:#daf1f0;text-align: center;line-height:50px;font-weight:bold"><?= $this->lang->line("#嬉街商城【商品問與答】通知信")["zh"] ?></div>
    <div>&nbsp;</div>
    <div style="font-weight: bold"><?= sprintf($this->lang->line("#親愛的 %s 您好:")["zh"], $product_faq->u_name) ?></div>
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    <div>賣家已回覆您在「<a href="<?= $this->config->item("server_base_url") . "/product/" . $product_faq->p_id ?>" target="_blank"><?= $product_faq->p_name ?></a>」的商品問與答：</div>
    <table style="width:100%;line-height:30px;">
        <tr style="background-color:#87ceeb;text-align: center;">
            <td>
                發問內容
            </td>
        </tr>
        <tr>
            <td>
                <?= nl2br($product_faq->pf_question) ?>
            </td>
        </tr>
        <tr style="background-color:#87ceeb;text-align: center;">
            <td>
                回覆內容
            </td>
        </tr>
        <tr>
            <td>
                <?= nl2br($answer) ?>
            </td>
        </tr>
    </table>
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    <div style="font-size:14px">※請注意：您提出的問題賣家可選擇是否公開及回覆，公開後才會於頁面上顯示。※</div>
    <?php include APPPATH . "views/mail_content/template/mail_footer_zh.php" ?>
</div>
