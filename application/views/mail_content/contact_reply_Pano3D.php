<div><img src="<?= $this->config->item("image_base_url") . "images/pano3d_logo.png" ?>"</div>
<div style="width: 640px">
    <hr style="border-top-color: #ff6600" />
    <div><?= sprintf($this->lang->line("%s 您好："), $c_name) ?></div>
    <div>&nbsp;</div>
    <div>
        <?php
        echo sprintf($this->lang->line("contact_reply"), //
                $this->lang->line("site_name"), //
                $c_subject, //
                nl2br($c_content), //
                nl2br($c_reply_content), //
                $this->lang->line("site_name"))
        ?>
    </div>
</div>
