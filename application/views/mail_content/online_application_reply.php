<div><img style="width: 640px" src="<?= $this->config->item("image_base_url") . "images/mail_header.jpg" ?>"</div>
<div style="width: 640px">
    <div><?= sprintf($this->lang->line("%s 您好："), $oa_contact_name) ?></div>
    <div>&nbsp;</div>
    <div>
        <?php
        echo sprintf($this->lang->line("application_reply"), //
                $this->lang->line("site_name"), //
                $oa_company_name, //
                $oa_company_tax_id,
                $oa_contact_name,
                $oa_phone,
                $oa_address,
                nl2br($oa_reply_content), //
                $this->lang->line("site_name"))
        ?>
    </div>
</div>
