
<div>
    <div style="font-weight: bold;"><?= $act_title . $title ?>通知</div>
    <br>
    <div>操作帳號：<?= $sa_account ?></div>
    <div><?= $act_title ?>時間：<?= date('Y-m-d H:i:s') ?></div>
    <br>
    <br>
    <div style="font-weight: bold;"><?= $act_title ?>內容：</div>
    <div>
        <table width="100%" style="border-top:1px solid #ddd; border-bottom: 1px solid #ddd;text-align: center;line-height:25px">
            <tr style="font-weight: bolder; border-top: 1px solid #ddd;">
                <td>標題</td><td><?= $g_title ?></td>
            </tr>
            <tr style="font-weight: bolder; border-top: 1px solid #ddd;">
                <td>緯度</td><td><?= $g_lat ?></td>
            </tr>
            <tr style="font-weight: bolder; border-top: 1px solid #ddd;">
                <td>經度</td><td><?= $g_lng ?></td>
            </tr>
            <tr style="font-weight: bolder; border-top: 1px solid #ddd;">
                <td>設定範圍</td><td><?= $g_range ?></td>
            </tr>
            <tr style="font-weight: bolder; border-top: 1px solid #ddd;">
                <td>狀態</td><td><?= $g_enabled == 'Y' ? '啟用中' : '已關閉' ?></td>
            </tr>
            <tr style="font-weight: bolder; border-top: 1px solid #ddd;">
                <td>推播範圍</td><td><img width='350px' src='<?= get_static_map_circle($g_lat, $g_lng, $g_range, 350, 300, $this->config->item("google")["api_key"]) ?>'></td>
            </tr>
        </table>
    </div>
</div>
<br>
<br>
<div>※詳細資訊請至「<a href="<?= $this->config->item('server_base_url') . 'sa/geofence' . ($act_title !== '刪除' ? '/edit?g_id=' . $id : '') ?>" target="_blank">系統管理後台 - <?= $title ?></a>」查詢※</div>
<br>
<?php include APPPATH . "views/mail_content/template/mail_footer.php" ?>