<div style="width:640px">
    <div><img style="width: 100%" src="<?= $this->config->item("image_base_url") . "images/mail_header.jpg" ?>"></div>
    <div style="background-color:#daf1f0;text-align: center;line-height:50px;font-weight:bold"><?= $this->lang->line("#嬉街商城【退貨異動申請】通知信")[$order->o_buyer_i18n] ?></div>
    <div><?= sprintf($this->lang->line("#親愛的 %s 您好:")[$order->o_buyer_i18n], $order->o_buyer_name) ?></div>
    <div>&nbsp;</div>
    <div><?= sprintf($this->lang->line("#感謝您對 %s 的支持，您已完成退貨異動申請，已通知店家進行確認作業，請耐心等候。")[$order->o_buyer_i18n], $this->lang->line("site_name")) ?></div>
    <div>&nbsp;</div>
    <div>您的退貨資訊如下：</div>
    <table style="text-align: center; width: 100%">
        <tr style="width:100%;background-color: #d64635;color:#fff">
            <td colspan="2"><?= $this->lang->line("#退貨異動資訊")[$order->o_buyer_i18n] ?></td>
        </tr>
        <tr>
            <td style="background-color: #f7d8d4;"><?= $this->lang->line("#退貨狀態")[$order->o_buyer_i18n] ?></td>
            <?php if ($order_item->rr_status == "REQUEST"): ?>
                <td>修改退貨</td>
            <?php elseif ($order_item->rr_status == "CANCEL"): ?>
                <td>取消退貨</td>
            <?php else: ?>
                <td>未定義</td>
            <?php endif; ?>
        </tr>
        <tr>
            <td style="background-color: #f7d8d4;"><?= $this->lang->line("#訂單編號")[$order->o_buyer_i18n] ?></td>
            <td><a href="<?= ($order->u_id) ? $this->config->item("server_base_url") . "refund_request/" . $order->o_id : $this->config->item("server_base_url") . "refund_request?o_id=" . $order->o_id . "&token=" . $order->o_token ?>"><?= $order->o_order_number ?></a></td>
        </tr>
        <tr>
            <td style="background-color: #f7d8d4;"><?= $this->lang->line("#退款方式")[$order->o_buyer_i18n] ?></td>
            <td><?= $pay_settings->{"ps_name_{$order->o_buyer_i18n}"} ?></td>
        </tr>
        <!--tr>
            <td style="background-color: #f7d8d4;"><?= $this->lang->line("#異動後退款金額")[$order->o_buyer_i18n] ?></td>
            <td><?= $order->o_refund_amount ?>元</td>
        </tr-->
    </table>
    <div>&nbsp;</div>
    <table style="text-align: center; width: 100%">
        <tr style="width:100%;background-color: #d64635;color:#fff">
            <td colspan="5"><?= $this->lang->line("#退貨異動明細")[$order->o_buyer_i18n] ?></td>
        </tr>
        <tr style="background-color: #f7d8d4;color: #000">
            <td><?= $this->lang->line("#商品")[$order->o_buyer_i18n] ?></td>
            <?php if ($order_item->oi_style): ?>
                <td><?= $this->lang->line("#款式")[$order->o_buyer_i18n] ?></td>
            <?php endif; ?>
            <td><?= $this->lang->line("#單價")[$order->o_buyer_i18n] ?></td>
            <td><?= $this->lang->line("#退貨數")[$order->o_buyer_i18n] ?></td>
            <!--td><?= $this->lang->line("#小計")[$order->o_buyer_i18n] ?></td-->
        </tr>

        <tr style="background-color: #ddd">
            <td style="text-align:center"><?= $order_item->oi_name ?></td>
            <?php if ($order_item->oi_style): ?>
                <td style="text-align:center"><?= ($order_item->oi_style) ?></td>
            <?php endif; ?>
            <td style="text-align:center"><?= number_format($order_item->oi_price) ?></td>
            <td style="text-align:center"><?= number_format($order_item->oi_qty) ?></td>
            <!--td style="text-align:center"><?= number_format($order_item->oi_price * $order_item->oi_qty) ?></td-->
        </tr>

        <!--tr style="width:100%;background-color: #f7d8d4">
            <td colspan="4" style="text-align:right">異動後退款總額</td>
            <td colspan="1" style="color:red"><?= number_format($order->o_refund_amount) ?></td>
        </tr-->
    </table>
    <?php include APPPATH . "views/mail_content/template/mail_footer_nopayinfo_{$order->o_buyer_i18n}.php" ?>
</div>