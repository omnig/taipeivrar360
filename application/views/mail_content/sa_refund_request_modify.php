<div style="width:640px">
    <div><img style="width: 100%" src="<?= $this->config->item("image_base_url") . "images/mail_header.jpg" ?>"></div>
    <div style="background-color:#daf1f0;text-align: center;line-height:50px;font-weight:bold"><?= $this->lang->line("#嬉街商城【退貨異動申請】通知信")[$order->o_buyer_i18n] ?></div>
    <div><?= sprintf($this->lang->line("#%s admin 您好:")[$order->o_buyer_i18n], $this->lang->line("site_name")) ?></div>
    <div>&nbsp;</div>
    <div><?= sprintf($this->lang->line("#您從 %s 收到一筆取消退貨")[$order->o_buyer_i18n], $this->lang->line("site_name")) ?></div>
    <div>&nbsp;</div>
    <table style="text-align: center; width: 100%">
        <tr style="width:100%;background-color: #d64635;color:#fff">
            <td colspan="2"><?= $this->lang->line("#退貨資訊")[$order->o_buyer_i18n] ?></td>
        </tr>
        <tr>
            <td style="background-color: #f7d8d4;"><?= $this->lang->line("#訂單編號")[$order->o_buyer_i18n] ?></td>
            <td><a href="<?= $this->config->item("server_base_url") ?>sa/order/edit?o_id=<?= $order->o_id ?>"><?= $order->o_order_number ?></a></td>
        </tr>
    </table>
    <div>&nbsp;</div>
    <table style="text-align: center; width: 100%">
        <tr style="width:100%;background-color: #d64635;color:#fff">
            <td colspan="5"><?= $this->lang->line("#退貨明細")[$order->o_buyer_i18n] ?></td>
        </tr>
        <tr style="background-color: #f7d8d4;color: #000">
            <td><?= $this->lang->line("#商品")[$order->o_buyer_i18n] ?></td>
            <td><?= $this->lang->line("#單價")[$order->o_buyer_i18n] ?></td>
            <td><?= $this->lang->line("#原退貨數")[$order->o_buyer_i18n] ?></td>
            <td><?= $this->lang->line("#小計")[$order->o_buyer_i18n] ?></td>
        </tr>
        <tr>
            <td style="text-align:center"><?= $order_item->oi_name . ($order_item->oi_style) ? " - " . $order_item->oi_style : "" ?></td>
            <td style="text-align:center"><?= number_format($order_item->oi_price) ?></td>
            <td style="text-align:center"><?= number_format($order_item->oi_qty) ?></td>
            <td style="text-align:center"><?= number_format($order_item->oi_price * $order_item->oi_qty) ?></td>
        </tr>

    </table>
    <?php include APPPATH . "views/mail_content/template/mail_footer_sa_refund.php" ?>
</div>