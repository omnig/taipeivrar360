<div style="width:640px">
    <div><img style="width: 100%" src="<?= $this->config->item("image_base_url") . "images/mail_header.jpg" ?>"></div>
    <div style="background-color:#daf1f0;text-align: center;line-height:50px;font-weight:bold"><?= $this->lang->line("#嬉街商城【訂單成立】通知信")[$order->o_buyer_i18n] ?></div>
    <div>&nbsp;</div>
    <div style="font-weight: bold"><?= sprintf($this->lang->line("#親愛的 %s 您好:")[$order->o_buyer_i18n], $order->o_buyer_name) ?></div>
    <div>&nbsp;</div>
    <div><?= sprintf($this->lang->line("#感謝您對 %s 的支持，您已於 %s 完成商品訂購，使用「%s」之訂單已成立。")[$order->o_buyer_i18n], $this->lang->line("site_name"), $order->o_create_timestamp, $pay_settings->{"ps_name_{$order->o_buyer_i18n}"}) ?></div>
    <?php if ($pay_settings->ps_token == 'ATM') : ?>
        <div>&nbsp;</div>
        <div><?= $this->lang->line("請記錄以下轉帳帳戶資訊，並於7日內完成轉帳付款。") ?></div>
        <div><?= $this->lang->line("戶名：") ?><?= $this->lang->line("company_name") ?></div>
        <div><?= $this->lang->line("收款銀行") ?>: <?= $this->lang->line("中國信託商業銀行") ?></div>
        <div><?= $this->lang->line("銀行代碼") ?>: 822</div>
        <div><?= $this->lang->line("轉帳帳號") ?>: <?= $order->o_v_account ?></div>
        <div><?= sprintf($this->lang->line("#請於<span style='font-size:18px'><b>%d日內</b></span>完成轉帳付款，若未付款系統將自動取消訂單。")[$order->o_buyer_i18n], 7) ?></div>
    <?php endif; ?>
    <div>&nbsp;</div>
    <div><?= $this->lang->line("#您的訂購資料如下：")[$order->o_buyer_i18n] ?></div>
    <table style="text-align: center; width: 100%">
        <tr width="100%" style="background-color:#26a69a;color:#fff">
            <td colspan="2"><?= $this->lang->line("#訂單資料")[$order->o_buyer_i18n] ?></td>
        </tr>
        <tr>
            <td style="background-color: #daf1f0;"><?= $this->lang->line("#訂單編號")[$order->o_buyer_i18n] ?></td>
            <td style="color:darkblue"><a href="<?= ($order->u_id) ? $this->config->item("server_base_url") . "order_detail/" . $order->o_id : $this->config->item("server_base_url") . "order_detail?o_id=" . $order->o_id . "&token=" . $order->o_token ?>"><?= $order->o_order_number ?></a></td>
        </tr>
        <tr>
            <td style="background-color: #daf1f0;"><?= $this->lang->line("#收件人姓名")[$order->o_buyer_i18n] ?></td>
            <td style="color:darkblue"><?= $order->o_recipient_name ?></td>
        </tr>
        <tr>
            <td style="background-color: #daf1f0;"><?= $this->lang->line("#收件人地址")[$order->o_buyer_i18n] ?></td>
            <td style="color:darkblue"><?= $recipient_area->a_zip . $recipient_city->c_name . $recipient_area->a_name . $order->o_recipient_address ?></td>
        </tr>
        <tr>
            <td style="background-color: #daf1f0;"><?= $this->lang->line("#收件人電話")[$order->o_buyer_i18n] ?></td>
            <td style="color:darkblue"><?= $order->o_recipient_phone ?></td>
        </tr>
        <?php if ($order->o_company_tax_id): ?>
            <tr>
                <td style="background-color: #daf1f0;"><?= $this->lang->line("#公司統一編號")[$order->o_buyer_i18n] ?></td>
                <td style="color:darkblue"><?= $order->o_company_tax_id ?></td>
            </tr>
        <?php endif; ?>
    </table>
    <div>&nbsp;</div>
    <table style="text-align: center; width: 100%">
        <tr width="100%" style="background-color:#26a69a;color:#fff">
            <td colspan="5"><?= $this->lang->line("#商品明細")[$order->o_buyer_i18n] ?></td>
        </tr>
        <tr style="background-color: #daf1f0;color: #000">
            <td><?= $this->lang->line("#項次")[$order->o_buyer_i18n] ?></td>
            <td><?= $this->lang->line("#商品")[$order->o_buyer_i18n] ?></td>
            <td><?= $this->lang->line("#款式")[$order->o_buyer_i18n] ?></td>
            <td><?= $this->lang->line("#單價")[$order->o_buyer_i18n] ?></td>
            <td><?= $this->lang->line("#數量")[$order->o_buyer_i18n] ?></td>
            <td><?= $this->lang->line("#小計")[$order->o_buyer_i18n] ?></td>
        </tr>
        <?php foreach ($order_item as $id => $row) : ?>
            <tr style="<?= $id++ % 2 == 1 ? "background-color: #ddd" : "" ?>">
                <td style="text-align:center"><?= $id ?></td>
                <td style="text-align:center"><?= $row->oi_name ?></td>
                <td style="text-align:center"><?= ($row->oi_style) ? $row->oi_style : "" ?></td>
                <td style="text-align:center"><?= number_format($row->oi_price) ?></td>
                <td style="text-align:center"><?= number_format($row->oi_qty) ?></td>
                <td style="text-align:center"><?= number_format($row->oi_price * $row->oi_qty) ?></td>
            </tr>
        <?php endforeach; ?>
        <tr>
            <td colspan="4" style="text-align:right;"><?= $this->lang->line("#運費")[$order->o_buyer_i18n] ?></td>
            <td colspan="1" style="color:red;font-weight:bolder;"><?= number_format($order->o_shipment) ?></td>
        </tr>
        <tr>
            <td colspan="4" style="text-align:right;"><?= $this->lang->line("#訂單總金額")[$order->o_buyer_i18n] ?></td>
            <td colspan="1" style="color:red;font-weight:bolder;"><?= number_format($order->o_total_amount) ?></td>
        </tr>
    </table>
    <?php if ($order->o_note): ?>
        <table style="width: 100%">
            <tr width="100%" style="text-align: center;background-color:#26a69a;color:#fff">
                <td><?= $this->lang->line("#附記")[$order->o_buyer_i18n] ?></td>
            </tr>
            <tr>
                <td style="padding: 5px 15px"><?= nl2br($order->o_note) ?></td>
            </tr>
        </table>
    <?php endif; ?>
    <?php include APPPATH . "views/mail_content/template/mail_footer_{$order->o_buyer_i18n}.php" ?>
</div>
