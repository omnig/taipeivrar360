<div style="width:640px">
    <div><img style="width: 100%" src="<?= $this->config->item("image_base_url") . "images/mail_header.jpg" ?>"></div>
    <div style="background-color:#f9835e;text-align: center;line-height:50px;font-weight:bold"><?= $this->lang->line("#嬉街商城【新訂單】通知信")[$order->o_buyer_i18n] ?></div>
    <div style="font-weight: bold"><?= $this->lang->line("#親愛的 管理者 您好:")[$i18n] ?></div>
    <div>&nbsp;</div>
    <div><?= sprintf($this->lang->line("#您從 %s 接到一筆新訂單。")[$order->o_buyer_i18n], $this->lang->line("site_name")) ?></div>
    <?php if ($pay_settings->ps_token == 'CREDIT') : ?>
        <div><?= $this->lang->line("#買家已使用信用卡付款，請於<b>七個工作天內</b>，將買家訂購的商品送達，謝謝您。")[$i18n] ?></div>
    <?php else: ?>    
        <div><?= $this->lang->line("#買家已使用ATM付款，請於<b>七個工作天內</b>，將買家訂購的商品送達，謝謝您。")[$i18n] ?></div>
    <?php endif; ?>
    <div>&nbsp;</div>
    <table style="text-align: center; width: 100%">
        <tr width="100%" style="background-color:#f9835e;color:#fff">
            <td colspan="3">訂單資料</td>
        </tr>
        <tr style="background-color: #fdd9ce;">
            <td><?= $this->lang->line("#訂單編號")[$i18n] ?></td>
            <td><?= $this->lang->line("#訂購時間")[$i18n] ?></td>
            <td><?= $this->lang->line("#訂單總價")[$i18n] ?></td>
        </tr>
        <tr style="color:darkblue">
            <td><a href="<?= $this->config->item("server_base_url") ?>admin/order_store/edit?os_id=<?= $this_order_store->os_id ?>"><?= $order->o_order_number . "-" . $this_order_store->s_id ?></a></td>
            <td><?= $order->o_create_timestamp ?></td>
            <td><?= number_format($this_order_store->os_total_amount) ?></td>
        </tr>
    </table>
    <div>&nbsp;</div>
    <table style="text-align: center;width: 100%">
        <tr width="100%" style="background-color:#f9835e;color:#fff">
            <td colspan="5">出貨明細</td>
        </tr>
        <tr style="background-color: #fdd9ce;">
            <td><?= $this->lang->line("#項次")[$i18n] ?></td>
            <td><?= $this->lang->line("#商品")[$i18n] ?></td>
            <td><?= $this->lang->line("#款式")[$i18n] ?></td>
            <td><?= $this->lang->line("#單價")[$i18n] ?></td>
            <td><?= $this->lang->line("#數量")[$i18n] ?></td>
            <td><?= $this->lang->line("#小計")[$i18n] ?></td>
        </tr>
        <?php foreach ($order_item as $id => $row) : ?>
            <tr style="<?= $id++ % 2 == 1 ? "background-color: #ddd" : "" ?>">
                <td style="text-align:center"><?= $id ?></td>
                <td style="text-align:center"><?= $row->oi_name ?></td>
                <td style="text-align:center"><?= ($row->oi_style) ? $row->oi_style : "" ?></td>
                <td style="text-align:center"><?= number_format($row->oi_price) ?></td>
                <td style="text-align:center"><?= number_format($row->oi_qty) ?></td>
                <td style="text-align:center"><?= number_format($row->oi_price * $row->oi_qty) ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
    <div>&nbsp;</div>
    <?php if ($order->o_note): ?>
        <table style="width: 100%">
            <tr width="100%" style="text-align: center; background-color:#f9835e;color:#fff">
                <td><?= $this->lang->line("#附記")[$order->o_buyer_i18n] ?></td>
            </tr>
            <tr>
                <td style="padding: 5px 15px"><?= nl2br($order->o_note) ?></td>
            </tr>
        </table>
    <?php endif; ?>
    <?php include APPPATH . "views/mail_content/template/mail_footer_store_{$order->o_buyer_i18n}.php" ?>
</div>