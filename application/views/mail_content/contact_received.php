<div><img style="width: 640px" src="<?= $this->config->item("image_base_url") . "images/mail_header.jpg" ?>"</div>
<div style="width: 640px">
    <div>收到一筆「聯絡我們」訊息：</div>
    <div>&nbsp;</div>
    <div>收到時間：<?= date("Y-m-d H:i:s") ?></div>
    <div>姓名：<?= $name ?></div>
    <div>Email：<?= $email ?></div>
    <div>電話：<?= $phone ?></div>
    <div>主題：<?= $subject ?></div>
    <div>內容：<br /><?= nl2br($content) ?></div>
</div>