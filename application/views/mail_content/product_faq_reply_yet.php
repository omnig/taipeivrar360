<div style="width:640px">
    <div><img style="width: 100%" src="<?= $this->config->item("image_base_url") . "images/mail_header.jpg" ?>"></div>
    <div style="background-color:#faebd7;text-align: center;line-height:50px;font-weight:bold">嬉街商城【商品問與答尚未回覆】提醒通知信</div>
    <div>&nbsp;</div>
    <div><?= sprintf($this->lang->line("#親愛的 %s 您好:")[$admin->adm_i18n], $admin->adm_name) ?></div>
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    <div>您的 <a href="<?= $this->config->item("server_base_url") . "product/" . $product_faq->p_id ?>" target="_blank"><?= $product_faq->p_name ?></a> 於 <?= date("Y年m月d日", strtotime($product_faq->pf_create_timestamp)) ?> 有消費者提問：</div>
    <div>&nbsp;</div>
    <table style="width:100%;line-height:35px;padding:10px;">
        <tr style="background-color:#eee;text-align: center;font-weight: bold">
            <td>
                提問內容
            </td>
        </tr>
        <tr>
            <td>
                <?= nl2br($product_faq->pf_question) ?>
            </td>
        </tr>
    </table>
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    <div>請儘速至<a href="<?= $this->config->item("server_base_url") . "admin/product_faq" ?>" target="_blank">店家管理系統</a>後台進行處理及回覆，謝謝您。</div>
    <div>&nbsp;</div>
    <div style="font-size:14px">※請注意：買家提出的問題只有您看的到，您可選擇是否答覆，答覆後才會於頁面上顯示。※</div>
    <?php include APPPATH . "views/mail_content/template/mail_footer_store_{$admin->adm_i18n}.php" ?>
</div>
