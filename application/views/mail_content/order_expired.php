<div style="width:640px">
    <div><img style="width: 100%" src="<?= $this->config->item("image_base_url") . "images/mail_header.jpg" ?>"></div>
    <div style="background-color:#daf1f0;text-align: center;line-height:50px;font-weight:bold"><?= $this->lang->line("#嬉街商城【訂單逾期】通知信")[$order->o_buyer_i18n] ?></div>
    <div>&nbsp;</div>
    <div style="font-weight: bold"><?= sprintf($this->lang->line("#親愛的 %s 您好:")[$order->o_buyer_i18n], $order->o_buyer_name) ?></div>
    <div>&nbsp;</div>
    <div><?= sprintf($this->lang->line("#您的訂單(編號：%s)已逾付款期限，訂單已取消，請勿逕行付款，若您仍要購買訂單商品，請重新訂購。")[$order->o_buyer_i18n], $order->o_order_number) ?></div>
    <?php include APPPATH . "views/mail_content/template/mail_footer_{$order->o_buyer_i18n}.php" ?>
</div>
