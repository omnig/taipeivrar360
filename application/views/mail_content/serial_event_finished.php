<div style="width:640px;">
    <div><img style="width: 100%" src="<?= $this->config->item("image_base_url") . "images/mail_header.jpg" ?>"></div>
    <div style="background-color:#daf1f0;text-align: center;line-height:50px;font-weight:bold"><?= $serial_event->se_name ?>序號已領取完畢</div>
    <div style="line-height: 30px;text-align: center;">
        <div>&nbsp;</div>
        <div style="font-weight: bold;font-size: 20px;">重要提醒</div>
        <div>&nbsp;</div>
        <div>親愛的系統管理者，您好：</div>
        <div>&nbsp;</div>
        <div style="font-weight: bold;color:red">
        <div>使用者尚未領取序號：</div>
        <div>ID:<?= $user->u_id ?></div>
        <div>帳號:<?= $user->u_account ?></div>
        <div>EMAIL:<?= $user->u_email ?></div>
        <div>(請補送序號至會員的EMAIL信箱)</div>
        </div>
        <div>&nbsp;</div>
        <div>&nbsp;</div>
        <div>活動尚未結束，序號已被領取完畢...</div>
        <div>請儘速至<a href="<?= $this->config->item("server_base_url") ?>sa/serial_event" target="_blank" >系統管理後台</a>確認序號數量，謝謝。</div>
        <div>&nbsp;</div>
    </div>
    <?php include APPPATH . "views/mail_content/template/mail_footer_sa.php" ?>
</div>
