<div>&nbsp;</div>
<hr>
<div style="color:red; font-size:14px">【<?= $this->lang->line("site_name") ?>提醒】</div>
<div style="color:red; font-size:14px"></div>
<div style="font-size:14px">
<ul>
    <li>查詢買家資料，請至<a href="<?= $this->config->item("server_base_url") ?>admin/order_store" target="_blank">【訂單查詢】</a>。</li>
    <li>如有其它問題，請至<a href="<?= $this->config->item("server_base_url") ?>contact" target="_blank">【聯絡我們】</a>。</li>
</ul>
</div>
<hr>
<div style="background-color:#f17275;width:100%;height:15px"></div>
<div style="text-align:center"><a href="<?= $this->config->item("server_base_url") ?>" target="_blank"><?= $this->lang->line("site_name") ?></a></div>