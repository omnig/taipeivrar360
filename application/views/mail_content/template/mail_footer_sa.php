<div>&nbsp;</div>
<hr>
<div style="color:red; font-size:14px">【<?= $this->lang->line("site_name") ?>重要提醒】</div>
<div style="color:red; font-size:14px"></div>
<div style="font-size:14px">
<ul>
<li>請系統管理後台<a href="<?= $this->config->item("server_base_url") ?>sa/order" target="_blank">「訂單管理」</a>進行處理，謝謝您。</li>
</ul>
</div>
<hr>
<div style="background-color:#f17275;width:100%;height:15px"></div>
<div style="text-align:center"><a href="<?= $this->config->item("server_base_url") ?>" target="_blank"><?= $this->lang->line("site_name") ?></a></div>