<div>&nbsp;</div>
<hr>
<div style="color:red; font-size:14px">【<?= $this->lang->line("site_name") ?> Notice】</div>
<div style="color:red; font-size:14px"></div>
<div style="font-size:14px">
<ul>
    <li>If checking the status of order, please go to <a href="<?= $this->config->item("server_base_url") ?>admin/order_store" target="_blank">【order history】</a>.</li>
    <li>Please go to <a href="<?= $this->config->item("server_base_url") ?>contact" target="_blank">【contact】</a> if you have any further question.</li>
</ul>
</div>
<hr>
<div style="background-color:#f17275;width:100%;height:15px"></div>
<div style="text-align:center"><a href="<?= $this->config->item("server_base_url") ?>" target="_blank"><?= $this->lang->line("site_name") ?></a></div>