<div>&nbsp;</div>
<hr>
<div style="color:red; font-size:14px">【Anti-fraud Notice】</div>
<div style="color:red; font-size:14px">Please do not operate ATM  under instructions of other people in the ways of providing balance, changing payment methods and the setting of systematic investment plan etc.. If any circumstance  above is confronted, please call the number 165, the Anti-fraud line.</div>
<div style="font-size:14px">
    <ul>
        <li>If ATM transferring is employed as the payment method, please finish the transfer process in <span style="font-size:16px"><b>3 days</b></span>. The shipment would be canceled automatically if the payment is not received. </li>
        <li>If the payment is paid by credit card, the merchandise would arrive in <span style="font-size:16px"><b>7 working days</b></span>.</li>
        <li>If checking the status of payment or the progress of shipment is needed, please go to <a href="<?= $this->config->item("server_base_url") ?>order_history" target="_blank">【order history】</a>.</li>
        <li>Please go to <a href="<?= $this->config->item("server_base_url") ?>contact" target="_blank">【contact】</a>if you have any further question.</li>
    </ul>
</div>
<hr>
<div style="background-color:#f17275;width:100%;height:15px"></div>
<div style="text-align:center"><a href="<?= $this->config->item("server_base_url") ?>" target="_blank"><?= $this->lang->line("site_name") ?></a></div>
