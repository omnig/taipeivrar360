<div>&nbsp;</div>
<hr>
<div style="color:red; font-size:14px">【Anti-fraud Notice】</div>
<div style="color:red; font-size:14px">Please do not operate ATM  under instructions of other people in the ways of providing balance, changing payment methods and the setting of systematic investment plan etc.. If any circumstance  above is confronted, please call the number 165, the Anti-fraud line.</div>
<div style="font-size:14px">
    <ul>
        <li>If checking the status of payment or the progress of shipment is needed, please go to <a href="<?= $this->config->item("server_base_url") ?>admin/order_store" target="_blank">【order history】.</li>
        <li>Please go to <a href="<?= $this->config->item("server_base_url") ?>contact" target="_blank">【contact】</a> if you have any further question.</li>
    </ul>
</div>
<hr>
<div style="background-color:#f17275;width:100%;height:15px"></div>
<div style="text-align:center"><a href="<?= $this->config->item("server_base_url") ?>" target="_blank"><?= $this->lang->line("site_name") ?></a></div>
