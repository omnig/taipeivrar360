<div>&nbsp;</div>
<hr>
<div style="color:red; font-size:14px">【防詐騙提醒】</div>
<div style="color:red; font-size:14px">若您接獲任何電話要您依照指示操作ATM，提供餘額、變更付款方式或更改定期設定等，請不要依電話指示至ATM操作，並建議您可撥打165防詐騙專線。</div>
<div style="font-size:14px">
    <ul>
        <li>若您使用ATM轉帳，請於<span style="font-size:16px"><b>7日內</b></span>完成轉帳付款，若未付款系統將自動取消訂單。</li>
        <li>若您使用信用卡付款，我們將於<span style="font-size:16px"><b>七個工作天內</b></span>，將您訂購的商品送達。</li>
        <li>若要查詢訂單狀態或貨運進度，可至<a href="<?= $this->config->item("server_base_url") ?>order_history" target="_blank">【訂購紀錄】</a>。</li>
        <li>如有其它問題，請至<a href="<?= $this->config->item("server_base_url") ?>contact" target="_blank">【聯絡我們】</a>。</li>
    </ul>
</div>
<hr>
<div style="background-color:#f17275;width:100%;height:15px"></div>
<div style="text-align:center"><a href="<?= $this->config->item("server_base_url") ?>" target="_blank"><?= $this->lang->line("site_name") ?></a></div>
