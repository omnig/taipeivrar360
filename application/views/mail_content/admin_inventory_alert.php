<div style="width:640px">
    <div><img style="width: 100%" src="<?= $this->config->item("image_base_url") . "images/mail_header.jpg" ?>"></div>
    <div style="background-color:#ffe6e6;text-align: center;line-height:50px;font-weight:bold"><?= $this->lang->line("#嬉街商城【庫存不足】通知信")[$admin->adm_i18n] ?></div>
    <div><?= sprintf($this->lang->line("#%s admin 您好:")[$admin->adm_i18n], $admin->adm_name) ?></div>
    <div>&nbsp;</div>
    <div><?= sprintf($this->lang->line("#您從 %s 收到庫存不足通知信")[$admin->adm_i18n], $this->lang->line("site_name")) ?></div>
    <div>&nbsp;</div>
    <div>您架上的商品 <a href="<?= $this->config->item("server_base_url") . "admin/product/edit?p_id=" . $product->p_id ?>"><?= $product->p_name ?></a>：</div>
    <div>庫存剩餘 <span style="font-weight: bold;color:red"><?= $product->p_inventory ?></span> 個（已小於您設定警示數: <?= $product->p_inventory_alert ?> 個）</div>
    <div>&nbsp;</div>
    <div>請儘速處理您的庫存數量，並至店家管理後台進行庫存調整。</div>
    <div>&nbsp;</div>
    <?php include APPPATH . "views/mail_content/template/mail_footer_store_{$admin->adm_i18n}.php" ?>
</div>