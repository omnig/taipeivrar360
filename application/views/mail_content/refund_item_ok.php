<div style="width:640px">
    <div><img style="width: 100%" src="<?= $this->config->item("image_base_url") . "images/mail_header.jpg" ?>"></div>
    <div style="background-color:#daf1f0;text-align: center;line-height:50px;font-weight:bold"><?= $this->lang->line("#嬉街商城【退貨申請中】通知信")[$order->o_buyer_i18n] ?></div>
    <div><?= sprintf($this->lang->line("#親愛的 %s 您好:")[$order->o_buyer_i18n], $order->o_buyer_name) ?></div>
    <div>&nbsp;</div>
    <div><?= sprintf($this->lang->line("#退貨商品： %s 確認無誤")[$order->o_buyer_i18n], $order_item->oi_name) ?></div>
    <div>&nbsp;</div>
    <div><?= $this->lang->line("您的退貨資訊如下：") ?></div>
    <table style="text-align: center; width: 100%">
        <tr style="width:100%;background-color: #d64635;color:#fff">
            <td colspan="2"><?= $this->lang->line("#退貨資訊")[$order->o_buyer_i18n] ?></td>
        </tr>
        <tr>
            <td style="background-color: #f7d8d4;"><?= $this->lang->line("#訂單編號")[$order->o_buyer_i18n] ?></td>
            <td><a href="<?= ($order->u_id) ? $this->config->item("server_base_url") . "refund_request/" . $order->o_id : $this->config->item("server_base_url") . "refund_request?o_id=" . $order->o_id . "&token=" . $order->o_token ?>"><?= $order->o_order_number ?></a></td>
        </tr>
        <tr>
            <td style="background-color: #f7d8d4;"><?= $this->lang->line("#商品名稱")[$order->o_buyer_i18n] ?></td>
            <td><?= $order_item->oi_name . ($order_item->oi_style) ? " - " . $order_item->oi_style : "" ?></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: left"><?= $this->lang->line("#以上您申請退貨的商品，經檢查確認無誤，已進入退款流程")[$order->o_buyer_i18n] ?></td>
        </tr>
    </table>
    <div>&nbsp;</div>
    <?php include APPPATH . "views/mail_content/template/mail_footer_nopayinfo_{$order->o_buyer_i18n}.php" ?>
</div>