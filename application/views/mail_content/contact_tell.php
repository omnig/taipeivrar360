<div style="width: 640px">
    <?php $CI = & get_instance(); ?>

    <div>親愛的管理者 <?= sprintf("%s 您好：", $adm_name) ?></div>
    <div>&nbsp;</div>
    <div>
        <?php 
        echo sprintf("您有新的一筆勘誤通報如下<br /><br />%s %s %s %s %s %s", //
                nl2br("主題:".$topic)."<br /><br />", //
                nl2br("回覆者姓名:<br />".$responser)."<br /><br />", //
                nl2br("回覆者聯絡電話:<br />".$responser_phone)."<br /><br />", //
                nl2br("回覆者信箱:<br />".$responser_mail)."<br /><br />", //
                nl2br("回覆者回報資訊:<br />".$responser_text)."<br /><br /><br />", //
                "再請您回覆感謝您"."<br />")
        ?>
    </div>
</div>
