<div style="width:640px">
    <div><img style="width: 100%" src="<?= $this->config->item("image_base_url") . "images/mail_header.jpg" ?>"></div>
    <div style="background-color:#daf1f0;text-align: center;line-height:50px;font-weight:bold"><?= $this->lang->line("#嬉街商城【退貨異動申請】通知信")[$order->o_buyer_i18n] ?></div>
    <div><?= $this->lang->line("親愛的 管理者 您好:") ?></div>
    <div>&nbsp;</div>
    <?php if ($refund_request->rr_status == "REQUEST"): ?>
        <div><?= sprintf($this->lang->line("#您從 %s 收到一筆退貨申請異動單：修改退貨")[$order->o_buyer_i18n], $this->lang->line("site_name")) ?></div>
    <?php elseif ($refund_request->rr_status == "CANCEL"): ?>
        <div><?= sprintf($this->lang->line("#您從 %s 收到一筆退貨申請異動單：取消退貨")[$order->o_buyer_i18n], $this->lang->line("site_name")) ?></div>
    <?php else: ?>
        <div><?= sprintf($this->lang->line("#您從 %s 收到一筆退貨申請異動單")[$order->o_buyer_i18n], $this->lang->line("site_name")) ?></div>
    <?php endif; ?>
    <div>&nbsp;</div>
    <div style="padding: 30px 0px; border:2px #f7d8d4 groove"><?= sprintf($this->lang->line("申請異動原因： %s")[$order->o_buyer_i18n], $refund_request->rr_reason) ?></div>
    <div>&nbsp;</div>
    <table style="text-align: center; width: 100%">
        <tr style="width:100%;background-color: #d64635;color:#fff">
            <td colspan="2"><?= $this->lang->line("#退貨異動資訊")[$order->o_buyer_i18n] ?></td>
        </tr>
        <tr>
            <td style="background-color: #f7d8d4;"><?= $this->lang->line("#成立時間")[$order->o_buyer_i18n] ?></td>
            <td><?= $refund_request->rr_timestamp ?></td>
        </tr>
        <tr>
            <td style="background-color: #f7d8d4;"><?= $this->lang->line("#訂單編號")[$order->o_buyer_i18n] ?></td>
            <td><a href="<?= $this->config->item("server_base_url") ?>admin/order_store"><?= $order->o_order_number ?></a></td>
        </tr>
        <tr>
            <td style="background-color: #f7d8d4;"><?= $this->lang->line("#退款方式")[$order->o_buyer_i18n] ?></td>
            <?php if ($order->ps_id == 1): ?>
                <td>ATM</td>
            <?php elseif ($order->ps_id == 2): ?>
                <td>信用卡<td>
                <?php else: ?>
                <td>超商取貨付款</td>
            <?php endif; ?>
        </tr>
        <tr>
            <td style="background-color: #f7d8d4;"><?= $this->lang->line("#異動後退款金額")[$order->o_buyer_i18n] ?></td>
            <td><?= $order->o_refund_amount ?></td>
        </tr>
    </table>
    <div>&nbsp;</div>
    <table style="text-align: center; width: 100%">
        <tr style="width:100%;background-color: #d64635;color:#fff">
            <td colspan="5"><?= $this->lang->line("#退貨明細")[$order->o_buyer_i18n] ?></td>
        </tr>
        <tr style="background-color: #f7d8d4;color: #000">
            <td><?= $this->lang->line("#項次")[$order->o_buyer_i18n] ?></td>
            <td><?= $this->lang->line("#商品")[$order->o_buyer_i18n] ?></td>
            <td><?= $this->lang->line("#單價")[$order->o_buyer_i18n] ?></td>
            <td><?= $this->lang->line("#退貨數")[$order->o_buyer_i18n] ?></td>
            <td><?= $this->lang->line("#小計")[$order->o_buyer_i18n] ?></td>
        </tr>
        <?php foreach ($order_item as $id => $row) : ?>
            <tr style="<?= $id++ % 2 == 1 ? "background-color: #ddd" : "" ?>;<?= $row->oi_qty < 0 ? "color:red" : "" ?>">
                <td style="text-align:center"><?= $id ?></td>
                <td style="text-align:center"><?= $row->oi_name . ($order_item->oi_style) ? " - " . $order_item->oi_style : "" ?></td>
                <td style="text-align:center"><?= number_format($row->oi_price) ?></td>
                <td style="text-align:center"><?= number_format($row->oi_qty) ?></td>
                <td style="text-align:center"><?= number_format($row->oi_price * $row->oi_qty) ?></td>
            </tr>
        <?php endforeach; ?>
        <tr style="width:100%;background-color: #f7d8d4">
            <td colspan="4" style="text-align:right">異動後退款總額</td>
            <td colspan="1" style="color:red"><?= number_format($order->o_refund_amount) ?></td>
        </tr>
    </table>
    <?php include APPPATH . "views/mail_content/template/mail_footer_store_refund_{$order->o_buyer_i18n}.php" ?>
</div>