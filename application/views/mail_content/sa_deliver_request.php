<div style="width:640px">
    <div><img style="width: 100%" src="<?= $this->config->item("image_base_url") . "images/mail_header.jpg" ?>"></div>
    <div style="background-color:#daf1f0;text-align: center;line-height:50px;font-weight:bold"><?= $this->lang->line("#嬉街商城【尚未出貨】通知信")["zh"] ?></div>
    <div>&nbsp;</div>
    <div><?= $this->lang->line("#親愛的 管理者 您好:")["zh"] ?></div>
    <div>&nbsp;</div>
    <div><?= $this->lang->line("#訂單編號")["zh"] . "：<a href='" . $this->config->item("server_base_url") . "sa/order/edit?o_id=" . $order->o_id . "'>" . $order->o_order_number ?></a></div>
    <div><?= sprintf($this->lang->line("#買家已完成付款起算第 %s 天，尚未接獲出貨通知，請注意店家出貨情形，謝謝。")["zh"], $delay) ?></div>
    <?php if (!isset($admin->adm_i18n)): ?>
        <div style="color:red;font-weight: bolder;">※【請注意】此店家可能尚未設定管理員，請儘速至後台確認！※</div>
    <?php endif; ?>
    <?php include APPPATH . "views/mail_content/template/mail_footer_sa.php" ?>
</div>
