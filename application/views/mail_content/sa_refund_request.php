<div style="width:640px">
    <div><img style="width: 100%" src="<?= $this->config->item("image_base_url") . "images/mail_header.jpg" ?>"></div>
    <div style="background-color:#daf1f0;text-align: center;line-height:50px;font-weight:bold"><?= $this->lang->line("#嬉街商城【退貨申請】通知信")["zh"] ?></div>
    <div><?= sprintf($this->lang->line("#%s admin 您好:")["zh"], $this->lang->line("site_name")) ?></div>
    <div>&nbsp;</div>
    <div><?= sprintf($this->lang->line("#您從 %s 收到一筆退貨申請單。")["zh"], $this->lang->line("site_name")) ?></div>
    <div>&nbsp;</div>
    <table style="text-align: center; width: 100%">
        <tr style="width:100%;background-color: #d64635;color:#fff">
            <td colspan="2"><?= $this->lang->line("#退貨資訊")["zh"] ?></td>
        </tr>
        <tr>
            <td style="background-color: #f7d8d4;"><?= $this->lang->line("#成立時間")["zh"] ?></td>
            <td><?= date("Y-m-d H:i") ?></td>
        </tr>
        <tr>
            <td style="background-color: #f7d8d4;"><?= $this->lang->line("#訂單編號")["zh"] ?></td>
            <td><a href="<?= $this->config->item("server_base_url") ?>sa/order/edit?o_id=<?= $order->o_id ?>"><?= $order->o_order_number ?></a></td>
        </tr>
        <?php if ($order->ps_id == 1): ?>
            <tr>
                <td style="background-color: #f7d8d4;"><?= $this->lang->line("退款銀行名稱") ?></td>
                <td><?= $refund_info["o_refund_bank_name"] . "(" . $this->lang->line("銀行代碼") . ":" . $refund_info["o_refund_bank_code"] . ")" ?></td>
            </tr>
            <tr>
                <td style="background-color: #f7d8d4;"><?= $this->lang->line("退款分行名稱") ?></td>
                <td><?= $refund_info["o_refund_branch_name"] ?></td>
            </tr>
            <tr>
                <td style="background-color: #f7d8d4;"><?= $this->lang->line("退款戶頭名稱") ?></td>
                <td><?= $refund_info["o_refund_bank_account_name"] ?></td>
            </tr>
            <tr>
                <td style="background-color: #f7d8d4;"><?= $this->lang->line("退款帳號") ?></td>
                <td><?= $refund_info["o_refund_bank_account"] ?></td>
            </tr>
        <?php endif; ?>
    </table>

    <?php include APPPATH . "views/mail_content/template/mail_footer_sa_refund.php" ?>
</div>