
<div>
    <div style="font-weight: bold;"><?= $act_title . $title ?>通知</div>
    <br>
    <div>操作帳號：<?= $sa_account ?></div>
    <div><?= $act_title ?>時間：<?= date('Y-m-d H:i:s') ?></div>
    <br>
    <br>
    <div style="font-weight: bold;"><?= $act_title ?>內容：</div>
    <div>
        <table width="100%" style="border-top:1px solid #ddd; border-bottom: 1px solid #ddd;text-align: center;line-height:25px">
            <tr style="font-weight: bolder; border-top: 1px solid #ddd;">
                <td>標題</td><td><?= $gt_title ?></td>
            </tr>
            <tr style="font-weight: bolder; border-top: 1px solid #ddd;">
                <td>內容</td><td><?= $gt_description ?></td>
            </tr>
            <tr style="font-weight: bolder; border-top: 1px solid #ddd;">
                <td>圖片</td><td><?= $gt_image ? '<img width="100px" src="' . $this->config->item('image_base_url') . 'upload/geofence/' . $gt_image . '" >' : '無圖片' ?></td>
            </tr>
            <tr style="font-weight: bolder; border-top: 1px solid #ddd;">
                <td>開始時間</td><td><?= $gt_start_timestamp ?></td>
            </tr>
            <tr style="font-weight: bolder; border-top: 1px solid #ddd;">
                <td>結束時間</td><td><?= $gt_end_timestamp ?></td>
            </tr>
            <tr style="font-weight: bolder; border-top: 1px solid #ddd;">
                <td>指定時間內推播</td><td><?= $gt_timelimit_push == 'Y' ? '是' : '否' ?></td>
            </tr>
            <tr style="font-weight: bolder; border-top: 1px solid #ddd;">
                <td>狀態</td><td><?= $gt_enabled == 'Y' ? '啟用中' : '已關閉' ?></td>
            </tr>
            <?php if ($act_title !== '刪除'): ?>
                <tr style="font-weight: bolder; border-top: 1px solid #ddd;">
                    <td>推播範圍</td><td><img width='350px' src="<?= get_static_map_circle($g_lat, $g_lng, $g_range, 350, 300, $this->config->item("google")["api_key"]) ?>"></td>
                </tr>
            <?php endif; ?>
        </table>
    </div>
</div>
<br>
<br>
<div>※詳細資訊請至「<a href="<?= $this->config->item('server_base_url') . 'sa/geofence_trigger' . ($act_title !== '刪除' ? '/edit?gt_id=' . $id : '') ?>" target="_blank">系統管理後台 - <?= $title ?></a>」查詢※</div>
<br>
<?php include APPPATH . "views/mail_content/template/mail_footer.php" ?>