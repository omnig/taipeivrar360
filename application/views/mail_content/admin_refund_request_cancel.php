<div style="width:640px">
    <div><img style="width: 100%" src="<?= $this->config->item("image_base_url") . "images/mail_header.jpg" ?>"></div>
    <div style="background-color:#daf1f0;text-align: center;line-height:50px;font-weight:bold"><?= $this->lang->line("#嬉街商城【取消退貨】通知信")["zh"] ?></div>
    <div><?= $this->lang->line("#親愛的 管理者您好:")["zh"] ?></div>
    <div>&nbsp;</div>
    
    <div><?= sprintf($this->lang->line("#您從 %s 收到一筆取消退貨通知。")["zh"], $this->lang->line("site_name")) ?></div>
    <div>&nbsp;</div>
    <table style="text-align: center; width: 100%">
        <tr style="width:100%;background-color: #d64635;color:#fff">
            <td colspan="2"><?= $this->lang->line("#退貨資訊")["zh"] ?></td>
        </tr>
        <tr>
            <td style="background-color: #f7d8d4;"><?= $this->lang->line("#訂單編號")["zh"] ?></td>
            <td><a href="<?= $this->config->item("server_base_url") ?>sa/order/edit?o_id=<?= $order->o_id ?>"><?= $order->o_order_number ?></a></td>
        </tr>
    </table>
    <?php include APPPATH . "views/mail_content/template/mail_footer_admin_refund.php" ?>
</div>