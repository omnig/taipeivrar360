<div style="width:640px">
    <div><img style="width: 100%" src="<?= $this->config->item("image_base_url") . "images/mail_header.jpg" ?>"></div>
    <div style="background-color:#daf1f0;text-align: center;line-height:50px;font-weight:bold"><?= $this->lang->line("#嬉街商城【尚未出貨】通知信")[$admin->adm_i18n] ?></div>
    <div>&nbsp;</div>
    <div><?= $this->lang->line("#親愛的 管理者 您好:")[$admin->adm_i18n] ?></div>
    <div>&nbsp;</div>
    <div><?= $this->lang->line("#您的訂單編號")[$admin->adm_i18n] . "：<a href='" . $this->config->item("server_base_url") . "admin/order_store/edit?os_id=" . $order->os_id . "'>" . $order->o_order_number ?></a></div>
    <div><?= sprintf($this->lang->line("#買家已完成付款起算第 %s 天，尚未接獲出貨通知，請儘速處理您的訂單，謝謝。")[$admin->adm_i18n], $delay) ?></div>
    <div><?= sprintf($this->lang->line("#※若您已將商品寄送，請登入後台並設定該筆訂單編號狀態為「出貨」※")[$admin->adm_i18n]) ?></div>
    <?php include APPPATH . "views/mail_content/template/mail_footer_store_{$admin->adm_i18n}.php" ?>
</div>
