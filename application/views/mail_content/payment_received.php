<div style="width:640px">
    <div><img style="width: 100%" src="<?= $this->config->item("image_base_url") . "images/mail_header.jpg" ?>"></div>
    <div style="background-color:#daf1f0;text-align: center;line-height:50px;font-weight:bold"><?= $this->lang->line("#嬉街商城【已付款】通知信")[$order->o_buyer_i18n] ?></div>
    <div>&nbsp;</div>
    <div><?= sprintf($this->lang->line("#親愛的 %s 您好:")[$order->o_buyer_i18n], $order->o_buyer_name) ?></div>
    <div>&nbsp;</div>
    <div><?= sprintf($this->lang->line("#您在 %s 付款已經收到了。")[$order->o_buyer_i18n], $this->lang->line("site_name")) ?></div>
    <div>&nbsp;</div>
    <table style="text-align: center; width: 100%">
        <tr width="100%" style="background-color:#26a69a;color:#fff">
            <td colspan="2"><?= $this->lang->line("#付款資訊")[$order->o_buyer_i18n] ?></td>
        </tr>
        <tr>
            <td style="background-color: #daf1f0;"><?= $this->lang->line("#訂單編號")[$order->o_buyer_i18n] ?></td>
            <td><a href="<?= ($order->u_id) ? $this->config->item("server_base_url") . "order_detail/" . $order->o_id : $this->config->item("server_base_url") . "order_detail?o_id=" . $order->o_id . "&token=" . $order->o_token ?>"><?= $order->o_order_number ?></a></td>
        </tr>
        <tr>
            <td style="background-color: #daf1f0;"><?= $this->lang->line("#商品總價")[$order->o_buyer_i18n] ?></td>
            <td><?= number_format($order->o_item_amount) ?></td>
        </tr>
        <tr>
            <td style="background-color: #daf1f0;"><?= $this->lang->line("#運費")[$order->o_buyer_i18n] ?></td>
            <td><?= number_format($order->o_shipment) ?></td>
        </tr>
        <tr>
            <td style="background-color: #daf1f0;"><?= $this->lang->line("#訂單總價")[$order->o_buyer_i18n] ?></td>
            <td><?= number_format($order->o_total_amount) ?></td>
        </tr>
        <tr>
            <td style="background-color: #daf1f0;"><?= $this->lang->line("#訂購時間")[$order->o_buyer_i18n] ?></td>
            <td><?= $order->o_create_timestamp ?></td>
        </tr>
        <tr>
            <td style="background-color: #daf1f0;"><?= $this->lang->line("#付款方式")[$order->o_buyer_i18n] ?></td>
            <td><?= $pay_settings->{"ps_name_{$order->o_buyer_i18n}"} ?></td>
        </tr>
    </table>
    <?php include APPPATH . "views/mail_content/template/mail_footer_nopayinfo_{$order->o_buyer_i18n}.php" ?>
</div>