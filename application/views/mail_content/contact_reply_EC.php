<div style="width: 640px">
    <div><?= sprintf($this->lang->line("%s 您好："), $c_name) ?></div>
    <div>&nbsp;</div>
    <div>
        <?php
        echo sprintf($this->lang->line("contact_reply"), //
                $this->lang->line("site_name"), //
                $c_subject, //
                nl2br($c_content), //
                nl2br($c_reply_content), //
                $admin_data,
                $this->lang->line("site_name"))
        ?>
    </div>
</div>
