
<div>
    <div style="font-weight: bold;"><?= $act_title . $title ?>通知</div>
    <br>
    <div>操作帳號：<?= $sa_account ?></div>
    <div><?= $act_title ?>時間：<?= date('Y-m-d H:i:s') ?></div>
    <br>
    <br>
    <div style="font-weight: bold;"><?= $act_title ?>內容：</div>
    <div>
        <table width="100%" style="border-top:1px solid #ddd; border-bottom: 1px solid #ddd;text-align: center;line-height:25px">
            <tr style="font-weight: bolder; border-top: 1px solid #ddd;">
                <td>標題</td><td><?= $sp_title ?></td>
            </tr>
            <tr style="font-weight: bolder; border-top: 1px solid #ddd;">
                <td>內容</td><td><?= $sp_content ?></td>
            </tr>
            <tr style="font-weight: bolder; border-top: 1px solid #ddd;">
                <td>影片連結</td><td><?= $sp_video_url ? '<a href="' . $sp_video_url . '" target="_blank">影片連結，另開視窗</a>' : '' ?></td>
            </tr>
            <tr style="font-weight: bolder; border-top: 1px solid #ddd;">
                <td>圖片1</td><td><?= $sp_image1 ? '<img width="100px" src="' . $this->config->item('image_base_url') . 'upload/sys_push/' . $sp_image1 . '" >' : '無圖片' ?></td>
            </tr>
            <tr style="font-weight: bolder; border-top: 1px solid #ddd;">
                <td>圖片2</td><td><?= $sp_image2 ? '<img width="100px" src="' . $this->config->item('image_base_url') . 'upload/sys_push/' . $sp_image2 . '" >' : '無圖片' ?></td>
            </tr>
            <tr style="font-weight: bolder; border-top: 1px solid #ddd;">
                <td>圖片3</td><td><?= $sp_image3 ? '<img width="100px" src="' . $this->config->item('image_base_url') . 'upload/sys_push/' . $sp_image3 . '" >' : '無圖片' ?></td>
            </tr>
            <tr style="font-weight: bolder; border-top: 1px solid #ddd;">
                <td>開始時間</td><td><?= $sp_start_timestamp ?></td>
            </tr>
            <tr style="font-weight: bolder; border-top: 1px solid #ddd;">
                <td>結束時間</td><td><?= $sp_finish_timestamp ?></td>
            </tr>
        </table>
    </div>
</div>
<br>
<br>
<div>※詳細資訊請至「<a href="<?= $this->config->item('server_base_url') . 'sa/sys_push' . ($act_title !== '刪除' ? '/edit?sp_id=' . $id : '') ?>" target="_blank">系統管理後台 - <?= $title ?></a>」查詢※</div>
<br>
<?php include APPPATH . "views/mail_content/template/mail_footer.php" ?>