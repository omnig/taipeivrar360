<div style="width:640px">
    <div><img style="width: 100%" src="<?= $this->config->item("image_base_url") . "images/mail_header.jpg" ?>"></div>
    <div style="background-color:#ffff99;text-align: center;line-height:50px;font-weight:bold"> 嬉街商城【店家已同意退款】通知信</div>
    <div><?= $this->lang->line("site_name") ?> 管理員 您好</div>
    <div>&nbsp;</div>
    <div>收到一筆退款通知，訂單編號：<?= $order->o_order_number ?></div>
    <div>&nbsp;</div>
    <table style="text-align: center; width: 100%">
        <tr style="width:100%;background-color: #d64635;color:#fff">
            <td colspan="2">詳細資訊</td>
        </tr>
        <tr>
            <td style="background-color: #f7d8d4;"><?= $this->lang->line("#成立時間")["zh"] ?></td>
            <td><?= date("Y-m-d H:i") ?></td>
        </tr>
        <tr>
            <td style="background-color: #f7d8d4;"><?= $this->lang->line("#訂單編號")["zh"] ?></td>
            <td><a href="<?= $this->config->item("server_base_url") ?>sa/order/edit?o_id=<?= $order->o_id ?>"><?= $order->o_order_number ?></a></td>
        </tr>
    </table>
    <div>&nbsp;</div>

    <?php include APPPATH . "views/mail_content/template/mail_footer_sa_refund.php" ?>
</div>