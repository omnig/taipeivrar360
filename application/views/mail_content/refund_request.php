<div style="width:640px">
    <div><img style="width: 100%" src="<?= $this->config->item("image_base_url") . "images/mail_header.jpg" ?>"></div>
    <div style="background-color:#daf1f0;text-align: center;line-height:50px;font-weight:bold"><?= $this->lang->line("#嬉街商城【退貨申請中】通知信")[$order->o_buyer_i18n] ?></div>
    <div><?= sprintf($this->lang->line("#親愛的 %s 您好:")[$order->o_buyer_i18n], $order->o_buyer_name) ?></div>
    <div>&nbsp;</div>
    <div><?= sprintf($this->lang->line("#感謝您對 %s 的支持，您已於 %s 完成退貨申請，已通知店家進行確認作業，請耐心等候。")[$order->o_buyer_i18n], $this->lang->line("site_name"), date("Y-m-d H:i")) ?></div>
    <div>&nbsp;</div>
    <div><?= $this->lang->line("您的退貨資訊如下：") ?></div>
    <table style="text-align: center; width: 100%">
        <tr style="width:100%;background-color: #d64635;color:#fff">
            <td colspan="2"><?= $this->lang->line("#退貨資訊")[$order->o_buyer_i18n] ?></td>
        </tr>
        <tr>
            <td style="background-color: #f7d8d4;"><?= $this->lang->line("#訂單編號")[$order->o_buyer_i18n] ?></td>
            <td><a href="<?= ($order->u_id) ? $this->config->item("server_base_url") . "refund_request/" . $order->o_id : $this->config->item("server_base_url") . "refund_request?o_id=" . $order->o_id . "&token=" . $order->o_token ?>"><?= $order->o_order_number ?></a></td>
        </tr>
        <?php if ($order->ps_id == 1): ?>
            <tr>
                <td style="background-color: #f7d8d4;"><?= $this->lang->line("退款銀行名稱") ?></td>
                <td><?= $refund_info["o_refund_bank_name"] . "(" . $this->lang->line("銀行代碼") . ":" . $refund_info["o_refund_bank_code"] . ")" ?></td>
            </tr>
            <tr>
                <td style="background-color: #f7d8d4;"><?= $this->lang->line("退款分行名稱") ?></td>
                <td><?= $refund_info["o_refund_branch_name"] ?></td>
            </tr>
            <tr>
                <td style="background-color: #f7d8d4;"><?= $this->lang->line("退款戶頭名稱") ?></td>
                <td><?= $refund_info["o_refund_bank_account_name"] ?></td>
            </tr>
            <tr>
                <td style="background-color: #f7d8d4;"><?= $this->lang->line("退款帳號") ?></td>
                <td><?= $refund_info["o_refund_bank_account"] ?></td>
            </tr>
        <?php endif; ?>
    </table>
    <div>&nbsp;</div>
    <div><?= $this->lang->line("提醒您：退貨若造成商品未達免運標準，將可能再收取一筆運費。") ?></div>
    <?php include APPPATH . "views/mail_content/template/mail_footer_nopayinfo_{$order->o_buyer_i18n}.php" ?>
</div>