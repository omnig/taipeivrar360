<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

	<h4><?php echo $heading; ?></h4>

	<?php if ($ENVIRONMENT !== 'production'): ?>

		<?php echo $message; ?>

	<?php endif ?>

</div>

