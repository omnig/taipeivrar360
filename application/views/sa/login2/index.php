<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>

        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?= base_url("assets/admin/pages/css/login2.css") ?>" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL SCRIPTS -->
        <style type="text/css">
            .btn-google{
                color: #fff !important;
                background-color: #D14836;   
            }
                
        </style>
    </head>
    <!-- BEGIN BODY -->
    <body class="login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="<?= base_url() ?>">
                <img src="<?= base_url("images/taipei_logo.jpeg") ?>" alt=""/>
            </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="menu-toggler sidebar-toggler">
        </div>
        <!-- END SIDEBAR TOGGLER BUTTON -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="<?= base_url("{$this->controller_name}/login/go") ?>" method="post">
                <h3 class="form-title"><?= $this->lang->line("系統管理後台") ?></h3>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span><?= $this->lang->line("請輸入帳號密碼") ?></span>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9"><?= $this->lang->line("帳號") ?></label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="<?= $this->lang->line("帳號") ?>" name="account"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9"><?= $this->lang->line("密碼") ?></label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="<?= $this->lang->line("密碼") ?>" name="password"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9"><?= $this->lang->line("圖形驗證") ?></label>
                    <div class='row' style="display: flex">
                        <img class='col-xs-6' src='<?= base_url("upload/captcha/{$cap['filename']}") ?>' />
                        <div class="input-icon col-xs-6">
                            <i class="fa fa-key"></i>
                            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="<?= $this->lang->line("圖形驗證") ?>" name="captcha_word"/>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                <div class="form-actions">
                    <input type='hidden' name='captcha_id' value='<?= $cap['id'] ?>' />
                    <button type="submit" class="btn btn-success uppercase" style="background-color: #50E3C2;">
                        <?= $this->lang->line("登入") ?> <i class="m-icon-swapright m-icon-white"></i>
                    </button>
                    

                        
                </div>

            </form>
            <!-- END LOGIN FORM -->
        </div>
        <!-- END LOGIN -->
        <!-- BEGIN COPYRIGHT -->
        <div class="copyright">
            <?= date('Y') ?> &copy; <a href="<?= base_url() ?>"><?= $this->lang->line("company_name") ?></a><?= $this->lang->line("版權所有") ?>。
        </div>
        <!-- END COPYRIGHT -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
        <script src="<?= base_url("assets/global/plugins/respond.min.js") ?>"></script>
        <script src="<?= base_url("assets/global/plugins/excanvas.min.js") ?>"></script> 
        <![endif]-->
        <script src="<?= base_url("assets/global/plugins/jquery.min.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/global/plugins/jquery-migrate.min.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/global/plugins/bootstrap/js/bootstrap.min.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/global/plugins/jquery.blockui.min.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/global/plugins/jquery.cokie.min.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/global/plugins/uniform/jquery.uniform.min.js") ?>" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?= base_url("assets/global/plugins/jquery-validation/js/jquery.validate.min.js") ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("assets/global/scripts/metronic.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/demo.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/pages/scripts/login_{$this->session->i18n}.js") ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
         <script nonce="cm1vaw==">
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Metronic.setAssetsPath('<?= base_url('assets') ?>/');
                Layout.init(); // init current layout
                Login.init();
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>