<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>

        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //上方標題列 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <form>
                        <div class="row margin-bottom-10">
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-sm yellow-crusta pull-right"><i class="fa fa-arrow-right"></i></button>
                                <div class="date_picker_container pull-right margin-right-10">
                                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                        <input type="text" class="form-control form-filter input-sm" readonly name="end_date" value="<?= $end_date ?>">
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                </div>
                                <div class="date_picker_container pull-right margin-right-10">
                                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                        <input type="text" class="form-control form-filter input-sm" readonly name="begin_date" value="<?= $begin_date ?>">
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- BEGIN INTERACTIVE CHART PORTLET-->
                    <div class="portlet box <?= $chart_user["title_color"] ?>">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-user"></i><?= $chart_user["title"] ?>
                            </div>
                            <div class="tools">
                                <a href="csv_new_user?begin_date=<?= $begin_date ?>&end_date=<?= $end_date ?>" class="btn btn-xs <?= $chart_user["title_color"] ?>" data-toggle="tooltip" data-placement="top" title="<?= $this->lang->line("下載") ?>">
                                    <i class="fa fa-download"></i> <?= $this->lang->line("新會員") ?>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div id="chart_user" class="chart">
                            </div>
                        </div>
                    </div>
                    <div class="portlet box <?= $chart_user_sum["title_color"] ?>">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-user"></i><?= $chart_user_sum["title"] ?>
                            </div>
                            <div class="tools">
                                <a href="csv_total_user?begin_date=<?= $begin_date ?>&end_date=<?= $end_date ?>" class="btn btn-xs <?= $chart_user_sum["title_color"] ?>" data-toggle="tooltip" data-placement="top" title="<?= $this->lang->line("下載") ?>">
                                    <i class="fa fa-download"></i> <?= $this->lang->line("會員") ?>
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div id="chart_user_sum" class="chart">
                            </div>
                        </div>
                    </div>
                    <!-- END INTERACTIVE CHART PORTLET-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊 ?>

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
        <?php if ($this->session->i18n == 'zh') : ?>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
        <?php endif; ?>
        <!-- END PAGE LEVEL PLUGINS -->
        <script src="<?= base_url("assets/global/plugins/flot/jquery.flot.min.js") ?>"></script>
        <script src="<?= base_url("assets/global/plugins/flot/jquery.flot.resize.min.js") ?>"></script>
        <script src="<?= base_url("assets/global/plugins/flot/jquery.flot.pie.min.js") ?>"></script>
        <script src="<?= base_url("assets/global/plugins/flot/jquery.flot.stack.min.js") ?>"></script>
        <script src="<?= base_url("assets/global/plugins/flot/jquery.flot.crosshair.min.js") ?>"></script>
        <script src="<?= base_url("assets/global/plugins/flot/jquery.flot.categories.min.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/global/plugins/flot/jquery.flot.time.{$this->session->i18n}.js") ?>"></script>
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("assets/global/scripts/metronic_{$this->session->i18n}.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>

        <!-- END PAGE LEVEL SCRIPTS -->
         <script nonce="cm1vaw==">
            var ChartsFlotcharts = function () {

                return {
                    //main function to initiate the module

                    init: function () {
                        ChartsFlotcharts.initCharts();
                    },
                    initCharts: function () {

                        if (!jQuery.plot) {
                            return;
                        }

                        function chart_user() {
                            var plot = $.plot($("#chart_user"), <?= json_encode($chart_user["data"]) ?>, {
                                series: {
                                    lines: {
                                        show: true,
                                        lineWidth: 2,
                                        fill: true,
                                        fillColor: {
                                            colors: [{
                                                    opacity: 0.05
                                                }, {
                                                    opacity: 0.03
                                                }, {
                                                    opacity: 0.01
                                                }]
                                        }
                                    },
                                    points: {
                                        show: true,
                                        radius: 3,
                                        lineWidth: 1
                                    },
                                    shadowSize: 2
                                },
                                grid: {
                                    hoverable: true,
                                    clickable: true,
                                    tickColor: "#eee",
                                    borderColor: "#eee",
                                    borderWidth: 1
                                },
                                colors: ["#d12610", "#37b7f3", "#52e136"],
                                xaxis: {
                                    ticks: 7,
                                    tickDecimals: 0,
                                    tickColor: "#eee",
                                    mode: '<?= $chart_user["mode"] ?>'
                                },
                                yaxis: {
                                    ticks: 7,
                                    tickDecimals: 0,
                                    tickColor: "#eee",
                                }
                            });


                            function showTooltip(x, y, contents) {
                                $('<div id="tooltip">' + contents + '</div>').css({
                                    position: 'absolute',
                                    display: 'none',
                                    top: y + 5,
                                    left: x + 15,
                                    border: '1px solid #333',
                                    padding: '4px',
                                    color: '#fff',
                                    'border-radius': '3px',
                                    'background-color': '#333',
                                    opacity: 0.80
                                }).appendTo("body").fadeIn(200);
                            }

                            var previousPoint = null;
                            $("#chart_user").bind("plothover", function (event, pos, item) {
                                $("#x").text(pos.x.toFixed(2));
                                $("#y").text(pos.y.toFixed(2));

                                if (item) {
                                    if (previousPoint != item.dataIndex) {
                                        previousPoint = item.dataIndex;

                                        $("#tooltip").remove();
                                        var date = new Date(item.datapoint[0]);
                                        var month = date.getMonth() + 1;
                                        var day = date.getDate();
                                        month = (month < 10 ? "0" : "") + month;
                                        day = (day < 10 ? "0" : "") + day;

                                        var y = item.datapoint[1];
                                        var showDate = date.getFullYear() +
                                                showTooltip(item.pageX, item.pageY, item.series.label + "<br />" + date.getFullYear() + "-" + month + "-" + day + ": " + y);
                                    }
                                } else {
                                    $("#tooltip").remove();
                                    previousPoint = null;
                                }
                            });
                        }
                        
                        function chart_user_sum() {
                            var plot = $.plot($("#chart_user_sum"), <?= json_encode($chart_user_sum["data"]) ?>, {
                                series: {
                                    lines: {
                                        show: true,
                                        lineWidth: 2,
                                        fill: true,
                                        fillColor: {
                                            colors: [{
                                                    opacity: 0.05
                                                }, {
                                                    opacity: 0.03
                                                }, {
                                                    opacity: 0.01
                                                }]
                                        }
                                    },
                                    points: {
                                        show: true,
                                        radius: 3,
                                        lineWidth: 1
                                    },
                                    shadowSize: 2
                                },
                                grid: {
                                    hoverable: true,
                                    clickable: true,
                                    tickColor: "#eee",
                                    borderColor: "#eee",
                                    borderWidth: 1
                                },
                                colors: ["#d12610", "#37b7f3", "#52e136"],
                                xaxis: {
                                    ticks: 7,
                                    tickDecimals: 0,
                                    tickColor: "#eee",
                                    mode: '<?= $chart_user_sum["mode"] ?>'
                                },
                                yaxis: {
                                    ticks: 7,
                                    tickDecimals: 0,
                                    tickColor: "#eee",
                                }
                            });


                            function showTooltip(x, y, contents) {
                                $('<div id="tooltip">' + contents + '</div>').css({
                                    position: 'absolute',
                                    display: 'none',
                                    top: y + 5,
                                    left: x + 15,
                                    border: '1px solid #333',
                                    padding: '4px',
                                    color: '#fff',
                                    'border-radius': '3px',
                                    'background-color': '#333',
                                    opacity: 0.80
                                }).appendTo("body").fadeIn(200);
                            }

                            var previousPoint = null;
                            $("#chart_user_sum").bind("plothover", function (event, pos, item) {
                                $("#x").text(pos.x.toFixed(2));
                                $("#y").text(pos.y.toFixed(2));

                                if (item) {
                                    if (previousPoint != item.dataIndex) {
                                        previousPoint = item.dataIndex;

                                        $("#tooltip").remove();
                                        var date = new Date(item.datapoint[0]);
                                        var month = date.getMonth() + 1;
                                        var day = date.getDate();
                                        month = (month < 10 ? "0" : "") + month;
                                        day = (day < 10 ? "0" : "") + day;

                                        var y = item.datapoint[1];
                                        var showDate = date.getFullYear() +
                                                showTooltip(item.pageX, item.pageY, item.series.label + "<br />" + date.getFullYear() + "-" + month + "-" + day + ": " + y);
                                    }
                                } else {
                                    $("#tooltip").remove();
                                    previousPoint = null;
                                }
                            });
                        }

                        //graph
                        chart_user();
                        chart_user_sum();
                    }
                };

            }();
        </script>

         <script nonce="cm1vaw==">
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Metronic.setAssetsPath('<?= base_url('assets') ?>/');
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
                ChartsFlotcharts.init();

                $('.date-picker').datepicker({
                    language: "<?= $this->session->i18n == 'zh' ? "zh-TW" : "en" ?>",
                    rtl: Metronic.isRTL(),
                    autoclose: true
                });
                $(function () {
                    $('[data-toggle="tooltip"]').tooltip();
                });
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
