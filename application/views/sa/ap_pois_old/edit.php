<!DOCTYPE html>
<!--[if IE 8]> <html lang="zh-TW" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="zh-TW" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>
        <link rel="stylesheet" type="text/css" href="<?= base_url("assets/global/plugins/bootstrap-summernote/summernote.css") ?>">
        <link href="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>        
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //上方標題列 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <form action="<?= base_url("{$this->controller_name}/{$table_name}/edit_go") ?>" enctype="multipart/form-data" class="form-horizontal form-row-seperated" method="post">
                                <div class="portlet">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <input type="hidden" name="ab_id" value="<?= $this->input->get('ab_id') ?>" >
                                            <input type="hidden" name="af_id" value="<?= $this->input->get('af_id') ?>" >
                                        </div>
                                        <div class="actions btn-set">
                                            <input type="hidden" name="<?= $field_prefix ?>_id" value="<?= ${$table_name}->{"{$field_prefix}_id"} ?>" />
                                            <a href="<?= base_url("{$this->controller_name}/{$table_name}") . '?ab_id=' . $this->input->get('ab_id') . "&af_id=" . $this->input->get("af_id") ?>" class="btn default"><i class="fa fa-angle-left"></i> <?= $this->lang->line("返回") ?></a>
                                            <button type="submit" class="btn green"><i class="fa fa-check"></i> <?= $this->lang->line("儲存") ?></button>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="tabbable">
                                            <ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a href="#tab_general" data-toggle="tab">
                                                        <?= $this->lang->line("一般") ?>
                                                    </a>
                                                </li>
                                            </ul>
                                            <div class="tab-content no-space">
                                                <div class="tab-pane active" id="tab_general">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><?= "ID" ?>:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" readonly="" name="<?= $field_prefix ?>_puid" class="form-control" value="<?= ${$table_name}->{"{$field_prefix}_puid"} ?>"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><?= $this->lang->line("名稱") ?>:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="<?= $field_prefix ?>_name" class="form-control" value="<?= ${$table_name}->{"{$field_prefix}_name"} ?>"/>
                                                                <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_name">
                                                                    <button class="close" data-close="alert"></button>
                                                                    <span> <?= $this->lang->line("請輸入") ?><?= $this->lang->line("名稱") ?> </span> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><?= $this->lang->line("描述") ?>:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="<?= $field_prefix ?>_desc" class="form-control"  value="<?= ${$table_name}->{"{$field_prefix}_desc"} ?>" />
                                                                <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_desc">
                                                                    <button class="close" data-close="alert"></button>
                                                                    <span> <?= $this->lang->line("請輸入") ?><?= $this->lang->line("描述") ?> </span> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">分類:</label>
                                                            <div class="col-md-6">
                                                                <select class="form-control" name="ac_id">
                                                                    <option value="">請選擇分類</option>
                                                                    <?php foreach ($category as $row): ?>
                                                                        <option value="<?= $row->ac_id ?>"<?= ($row->ac_id == ${$table_name}->ac_id ) ? ' selected' : '' ?>><?= $row->ac_title_zh ?><?= ($row->ac_enabled == 'N') ? '[已關閉]' : '' ?></option>
                                                                    <?php endforeach ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">坐標:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control lat" name="<?= $field_prefix ?>_lat" value="<?= (!${$table_name}->{"{$field_prefix}_lat"}) ? 0 : ${$table_name}->{"{$field_prefix}_lat"} ?>" placeholder="請輸入經度" >
                                                                <input type="text" class="form-control lng" name="<?= $field_prefix ?>_lng" value="<?= (!${$table_name}->{"{$field_prefix}_lng"}) ? 0 : ${$table_name}->{"{$field_prefix}_lng"} ?>" placeholder="請輸入緯度" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"></label>
                                                            <div class="col-md-6">
                                                                <label class="control-label newlat" hidden></label>
                                                                <label class="control-label newlng" hidden></label>
                                                                <div id="map" style="width:100%;height:400px"></div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><?= $this->lang->line("建物入口") ?>:</label>
                                                            <div class="col-md-6">
                                                                <select class="form-control" name="<?= $field_prefix ?>_is_building_entrance">
                                                                    <option value="Y"<?= (${$table_name}->{"{$field_prefix}_is_building_entrance"} == 'Y') ? ' selected' : '' ?>><?= $this->lang->line("是") ?></option>
                                                                    <option value="N"<?= (${$table_name}->{"{$field_prefix}_is_building_entrance"} == 'N') ? ' selected' : '' ?>><?= $this->lang->line("否") ?></option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><?= $this->lang->line("狀態") ?>:</label>
                                                            <div class="col-md-6">
                                                                <select class="form-control" name="<?= $field_prefix ?>_enabled">
                                                                    <option value="Y"<?= (${$table_name}->{"{$field_prefix}_enabled"} == 'Y') ? ' selected' : '' ?>><?= $this->lang->line("啟用中") ?></option>
                                                                    <option value="N"<?= (${$table_name}->{"{$field_prefix}_enabled"} == 'N') ? ' selected' : '' ?>><?= $this->lang->line("已關閉") ?></option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- END PAGE CONTENT-->
                    </div>
                </div>
                <!-- END CONTENT -->
            </div>
            <!-- END CONTAINER -->
            <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊  ?>

            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/select2/select2.min.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-summernote/summernote.min.js") ?>"></script> 
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-summernote/lang/summernote-zh-TW.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
            <?php if ($this->session->i18n == 'zh') : ?>
                <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
            <?php endif; ?>
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN PAGE LEVEL SCRIPTS -->
            <script src="<?= base_url("assets/global/scripts/metronic.js") ?>" type="text/javascript"></script>
            <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
            <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>
            <script src="<?= base_url("assets/global/scripts/datatable_{$this->session->i18n}.js") ?>"></script>
            <script src="<?= base_url("assets/admin/pages/scripts/table-ajax_{$this->session->i18n}.js") ?>"></script>
            <!-- END PAGE LEVEL SCRIPTS -->
            <script src="<?= base_url("js/geofence.js") ?>" type="text/javascript"></script>
            <script src="<?= base_url("js/google_map.js") ?>" type="text/javascript"></script>
            <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= $this->config->item("google")["api_key"] ?>&callback=initMap"></script>
             <script type="text/javascript" nonce="cm1vaw==">
                $(function () {
                    $('#btn_submit').on('click', function () {
                        if (on_submit()) {
                            $('form').submit();
                        }
                    });

                    $('form').submit(on_submit);

                    function on_submit() {
                        var result = true;

                        $('.validation').each(function () {
                            var name = $(this).attr('for');

                            if ($('input[name=' + name + ']:radio').length > 0 && $('input[name=' + name + ']:radio:checked').length === 0) {
                                $(this).show();

                                //顯示此tab頁面
                                if ($(this).parents(".tab-pane")) {
                                    var id = $(this).parents(".tab-pane").attr("id");
                                    $('.nav-tabs a[href="#' + id + '"]').tab('show');
                                }

                                $('*[name=' + name + ']').focus();
                                event.preventDefault();
                                result = false;
                            } else if (!$('*[name=' + name + ']:not(radio)').val()) {
                                $(this).show();

                                //顯示此tab頁面
                                if ($(this).parents(".tab-pane")) {
                                    var id = $(this).parents(".tab-pane").attr("id");
                                    $('.nav-tabs a[href="#' + id + '"]').tab('show');
                                }

                                $('*[name=' + name + ']').focus();
                                event.preventDefault();
                                result = false;
                            } else {
                                $(this).hide();
                            }
                        });

                        return result;
                    }
                });

                $('.store_filter').keyup(function () {
                    var store_keyword = $(this).val();

                    $('.store_item').hide();
                    if (store_keyword) {
                        $('.store_item:contains("' + store_keyword + '")').show();
                    } else {
                        $('.store_item').show();
                    }
                });
            </script>
             <script nonce="cm1vaw==">
                jQuery(document).ready(function () {
                    Metronic.init(); // init metronic core components
                    Layout.init(); // init current layout
                    QuickSidebar.init(); // init quick sidebar
                });
                area_select_map.init('<?= base_url() ?>');
                setTimeout(function () {
                    area_select_map.set_tile_map("<?= $ap_floor->af_plan_id ?>",<?= $ap_floor->af_bottom_left_lat ?>,<?= $ap_floor->af_bottom_left_lng ?>,<?= $ap_floor->af_top_right_lat ?>,<?= $ap_floor->af_top_right_lng ?>);
                }, 500);
            </script>
            <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
