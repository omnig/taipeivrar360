<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>

        <link href="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url("assets/plugins/jquery-ui/jquery-ui.css") ?>" rel="stylesheet" type="text/css" />
        <link href='<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.css") ?>' rel='stylesheet'>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //上方標題列 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <form action="<?= base_url("{$this->controller_name}/{$table_name}/edit_go") ?>" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data">
                                <div class="portlet">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-edit"></i>編輯展品內容
                                        </div>
                                        <div class="actions btn-set">
                                            <input type="hidden" name="<?= "{$field_prefix}_id" ?>" value="<?= ${$table_name}->{"{$field_prefix}_id"} ?>" />
                                            <a href="<?= base_url("{$this->controller_name}/{$table_name}") ?>" class="btn default"><i class="fa fa-angle-left"></i> <?= $this->lang->line("返回") ?></a>
                                            <button type="submit" class="btn green"><i class="fa fa-check"></i> <?= $this->lang->line("儲存") ?></button>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="tabbable">
                                            <ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a href="#tab_general" data-toggle="tab">
                                                        導覽內容
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#tab_zh" data-toggle="tab">
                                                        中文
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#tab_en" data-toggle="tab">
                                                        英文
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#tab_jp" data-toggle="tab">
                                                        日文
                                                    </a>
                                                </li>
                                            </ul>
                                            <div class="tab-content no-space">
                                                <div class="tab-pane active" id="tab_general">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">展區區域:</label>
                                                            <div class="col-md-5">
                                                                <select class="form-control selected_theme" name="ep_id">
                                                                    <option value="">請選擇區域</option>
                                                                    <?php foreach ($exh_point as $row): ?>
                                                                        <option value="<?= $row->ep_id ?>" <?= ($row->ep_id == ${$table_name}->ep_id) ? 'selected' : '' ?>><?= "[" . $row->af_name . '] ' . $row->ea_title_zh . ' - ' . $row->ep_title_zh ?></option>
                                                                        <?php
                                                                        if ($row->ep_id == ${$table_name}->ep_id) {
                                                                            echo " <script nonce="cm1vaw==">setTimeout(function(){set_tile_map($row->ep_id)},500)</script>";
                                                                            $choosed_f_id = $row->af_id;
                                                                        }
                                                                        ?>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                                <input type="hidden" name="choosed_f_id" value="<?= $choosed_f_id ?>">
                                                                <input type="hidden" name="new_f_id" id="new_f_id" value="<?= $choosed_f_id ?>">
                                                                <div class="alert alert-danger display-hide validation" for="ep_id">
                                                                    <button class="close" data-close="alert"></button>
                                                                    <span> 請選擇區域 </span> 
                                                                </div>
                                                                <label class="label font-yellow"><i class="fa fa-warning"></i> 提醒：選擇不同樓層之區域需重新建立點位資料。</label>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <a href="<?= base_url("{$this->controller_name}/exh_area/add") ?>" class="form-control btn btn-default" title="前往新增展區" ><i class='fa fa-arrow-circle-right'></i> 新增</a>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">導覽時間:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="<?= $field_prefix ?>_tour_time" class="form-control" placeholder="預定導覽時間(分)" value="<?= ${$table_name}->{"{$field_prefix}_tour_time"} ?>" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <label class="control-label col-md-3"><?= $this->lang->line("圖片") ?>:</label>
                                                            <div class="col-md-4">
                                                                <?php
                                                                if (!${$table_name}->{"{$field_prefix}_image"}) {
                                                                    $img = "<img src='http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no%20picture' alt='' />";
                                                                    $fileinput_status = 'fileinput-new';
                                                                } else {
                                                                    $img = "<img src='" . '../../upload/exh/' . ${$table_name}->{"{$field_prefix}_image"} . "' />";
                                                                    $fileinput_status = 'fileinput-exists';
                                                                }
                                                                ?>
                                                                <div class="fileinput <?= $fileinput_status ?>" data-provides="fileinput">
                                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                                                        <?= $img ?>
                                                                    </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> <?= $this->lang->line("選取圖片") ?> </span>
                                                                            <span class="fileinput-exists"> <?= $this->lang->line("更換") ?> </span>
                                                                            <input type="file" name="<?= $field_prefix ?>_image" accept="image/*">
                                                                        </span>
                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?= $this->lang->line("移除") ?> </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">語音檔:</label>
                                                            <div class="col-md-6">
                                                                <?php
                                                                if (!${$table_name}->{"{$field_prefix}_audio"}) {
                                                                    $audio = '';
                                                                    $fileinput_status = 'fileinput-new';
                                                                } else {
                                                                    $audio = '<a href="' . $this->config->item('vs_base_url') . 'upload/audio/' . ${$table_name}->{"{$field_prefix}_audio"} . '">語音連結</a>';
                                                                    $fileinput_status = 'fileinput-exists';
                                                                }
                                                                ?>
                                                                <div class="fileinput <?= $fileinput_status ?>" data-provides="fileinput">
                                                                    <div class="input-group">
                                                                        <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput" >
                                                                            <i class="fa fa-file fileinput-exists"></i>
                                                                            <span class="fileinput-filename"><?= ${$table_name}->{"{$field_prefix}_audio"} ?></span>
                                                                        </div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> 上傳檔案 </span>
                                                                            <span class="fileinput-exists"> <?= $this->lang->line("更換") ?> </span>
                                                                            <input type="file" name="<?= $field_prefix ?>_audio" accept="mp3/*">
                                                                        </span>
                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">移除</a>
                                                                    </div>
                                                                </div>
                                                                <?php if ($fileinput_status == 'fileinput-exists'): ?>
                                                                    <audio id="player" src="<?= $this->config->item('vs_base_url') . ${$table_name}->{"{$field_prefix}_audio"} ?>" controls>
                                                                        HTML5 audio not supported
                                                                    </audio>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="is_pois" value="<?= ($exh_relation[0]->ap_id) ? 'Y' : 'N' ?>">
                                                        <?php if (count($exh_relation) == 0 || (!$exh_relation[0]->ap_id)): ?>
                                                            <!-- 設定POI資訊 -->
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">POI分類:</label>
                                                                <div class="col-md-6">
                                                                    <select class="form-control" name="ac_id">
                                                                        <option value="" selected>請選擇分類（選此項則跳過新增展品位置）</option>
                                                                        <?php foreach ($category as $row): ?>
                                                                            <option value="<?= $row->ac_id ?>"><?= $row->ac_title_zh ?></option>
                                                                        <?php endforeach ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">坐標:</label>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control lat" name="<?= $field_prefix ?>_lat" value="<?= $this->config->item('default_lat') ?>" placeholder="請輸入經度" >
                                                                    <input type="text" class="form-control lng" name="<?= $field_prefix ?>_lng" value="<?= $this->config->item('default_lng') ?>" placeholder="請輸入緯度" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"></label>
                                                                <div class="col-md-6">
                                                                    <label class="control-label newlat" hidden></label>
                                                                    <label class="control-label newlng" hidden></label>
                                                                    <div id="map" style="width:100%;height:400px"></div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"><?= $this->lang->line("建物入口") ?>:</label>
                                                                <div class="col-md-6">
                                                                    <select class="form-control" name="<?= $field_prefix ?>_is_building_entrance">
                                                                        <option value="Y"><?= $this->lang->line("是") ?></option>
                                                                        <option value="N" selected><?= $this->lang->line("否") ?></option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        <?php else: ?>
                                                            <div id="map" style="width:100%;height:0px"></div>
                                                        <?php endif; ?>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><?= $this->lang->line("狀態") ?>:</label>
                                                            <div class="col-md-6">
                                                                <select class="form-control" name="<?= $field_prefix ?>_enabled">
                                                                    <option value="Y" <?= ${$table_name}->{"{$field_prefix}_enabled"} == 'Y' ?>><?= $this->lang->line("啟用中") ?></option>
                                                                    <option value="N" <?= ${$table_name}->{"{$field_prefix}_enabled"} == 'N' ?>><?= $this->lang->line("已關閉") ?></option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class='form-group'>
                                                            <label class="control-label col-md-3"></label>
                                                            <div class="col-md-6">
                                                                <?php if (count($exh_relation) > 0 && ($exh_relation[0]->ap_id)): ?>
                                                                    <div class='portlet blue-hoki box'>
                                                                        <div class='portlet-title'>
                                                                            <div class='caption'><i class="fa fa-cogs"></i>POI管理</div>
                                                                        </div>
                                                                        <div class="portlet-body">
                                                                            <div class="table-responsive">
                                                                                <table class='table table-hover table-bordered table-striped'>
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>所屬建物</th>
                                                                                            <th>所在樓層</th>
                                                                                            <th>點位名稱</th>
                                                                                            <th>操作</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php foreach ($exh_relation as $row): ?>
                                                                                            <?php if ($row->ap_id): ?>
                                                                                                <tr>
                                                                                                    <td><?= $row->ab_name ?></td>
                                                                                                    <td><?= $row->af_name ?></td>
                                                                                                    <td><?= $row->ap_name ?></td>
                                                                                                    <td>
                                                                                                        <a href='<?= base_url("sa/ap_pois/edit?ab_id={$row->ab_id}&af_id={$row->af_id}&ap_id={$row->ap_id}") ?>' class='btn btn-xs default blue-stripe btn-operating'><i class="fa fa-pencil"></i>編輯</a>
                                                                                                        <a href ='javascript:if(confirm("確定刪除？")) location.href="<?= base_url("sa/ap_pois/del?ab_id={$row->ab_id}&af_id={$row->af_id}&ap_id={$row->ap_id}") ?>"' class='btn btn-xs default red-stripe btn-operating'><i class="fa fa-remove"></i>刪除</a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            <?php endif; ?>
                                                                                        <?php endforeach; ?>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div> 
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab_zh">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">中文名稱:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="<?= $field_prefix ?>_title_zh" class="form-control" placeholder="請輸入展品中文名稱" value="<?= ${$table_name}->{"{$field_prefix}_title_zh"} ?>"/>
                                                                <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_title_zh">
                                                                    <button class="close" data-close="alert"></button>
                                                                    <span> <?= $this->lang->line("請輸入") ?>展品中文名稱 </span> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">內容:</label>
                                                            <div class="col-md-6">
                                                                <textarea class="form-control" name="<?= $field_prefix ?>_content_zh" rows="8" placeholder="請輸入中文內容"><?= ${$table_name}->{"{$field_prefix}_content_zh"} ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab_en">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">英文名稱:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="<?= $field_prefix ?>_title_en" class="form-control" placeholder="請輸入展品英文名稱" value="<?= ${$table_name}->{"{$field_prefix}_title_en"} ?>"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">內容:</label>
                                                            <div class="col-md-6">
                                                                <textarea class="form-control" name="<?= $field_prefix ?>_content_en" rows="8" placeholder="請輸入英文內容"><?= ${$table_name}->{"{$field_prefix}_content_en"} ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab_jp">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">日文名稱:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="<?= $field_prefix ?>_title_jp" class="form-control" placeholder="請輸入展品日文名稱" value="<?= ${$table_name}->{"{$field_prefix}_title_jp"} ?>"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">內容:</label>
                                                            <div class="col-md-6">
                                                                <textarea class="form-control" name="<?= $field_prefix ?>_content_jp" rows="8" placeholder="請輸入日文內容"><?= ${$table_name}->{"{$field_prefix}_content_jp"} ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊    ?>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/select2/select2.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
        <?php if ($this->session->i18n == 'zh') : ?>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
        <?php endif; ?>
        <script type="text/javascript" src="<?= base_url("assets/plugins/jquery-ui/jquery-ui.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.js") ?>"></script>
        <script type='text/javascript' src='<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-sliderAccess.js") ?>'></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("assets/global/scripts/metronic.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("js/geofence.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("js/google_map.js") ?>" type="text/javascript"></script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= $this->config->item("google")["api_key"] ?>&callback=initMap"></script>
         <script type="text/javascript" nonce="cm1vaw==">
            $(function () {

                $('#btn_submit').on('click', function () {
                    if (on_submit()) {
                        $('form').submit();
                    }
                });

                $('form').submit(on_submit);

                function on_submit() {
                    var result = true;

                    $('.validation').each(function () {
                        var name = $(this).attr('for');

                        if (!$('*[name=' + name + ']').val()) {
                            $(this).show();
                            $('*[name=' + name + ']').focus();
                            event.preventDefault();
                            result = false;
                        } else {
                            $(this).hide();
                        }
                    });

                    return result;
                }
                $('.date-picker').datetimepicker({
                    dateFormat: 'yy-mm-dd',
                    timeFormat: 'HH:mm'
                });
            });
        </script>
         <script nonce="cm1vaw==">
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
            });
            area_select_map.init('<?= base_url() ?>');

            function checkall(selector) {
                $(selector).prop('checked', true);
                $(selector).parent().addClass('checked');
            }
            function uncheck(selector) {
                $(selector).prop('checked', false);
                $(selector).parent().removeClass('checked');
            }

            $(document).on('click', '[name=remove]', function () {
                $(this).parent().remove();
            });
            $(document).on('click', '[name=newremove]', function () {
                var count = $('[name=beacon]').length;
                if (count > 1) {
                    $(this).parent().remove();
                }
            });

            $('.selected_theme').change(function () {
                var val = $(this).val();
                var relation =<?= count($exh_relation) ?>;
                if (val == 0) {
                    $('.lat').val(<?= $this->config->item('default_lat') ?>);
                    $('.lng').val(<?= $this->config->item('default_lng') ?>);
                    if (relation === 0) {
                        area_select_map.set_center(<?= $this->config->item('default_lat') ?>, <?= $this->config->item('default_lng') ?>);
                        area_select_map.set_tile_map();
                    }
                }
                set_tile_map(val);
            });

            function set_tile_map(id) {
                if (!id) {
                    area_select_map.set_tile_map();
                }
                <?php foreach ($exh_point as $row): ?>
                    if (<?= $row->ep_id ?> == id) {
                        area_select_map.set_tile_map("<?= $row->af_plan_id ?>",<?= $row->af_bottom_left_lat ?>,<?= $row->af_bottom_left_lng ?>,<?= $row->af_top_right_lat ?>,<?= $row->af_top_right_lng ?>);
                        $('#new_f_id').val(<?= $row->af_id ?>);
                    }
                <?php endforeach; ?>
            }
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
