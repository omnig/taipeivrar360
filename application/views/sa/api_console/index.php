<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>

        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("css/api_console.css") ?>" rel="stylesheet" type="text/css" />
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div align="left">
                        <table class="layout_table" width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr class="main_layout_area">
                                <td width="180">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td height="40" align="left" valign="top">
                                                <ul class="api_list_ul">

                                                    <li class="api_list_title" id="debug_tool">主題相關</li>
                                                    <?php foreach ($topic_api as $row) : ?>
                                                        <li class="api_list_li debug_tool" id="<?= $row["id"] ?>"><b><?= $row["id"] ?></b> <br>(<?= $row["name"] ?>)</li>
                                                    <?php endforeach; ?>

                                                    <li class="api_list_title" id="debug_tool">POI相關</li>
                                                    <?php foreach ($poi_api as $row) : ?>
                                                        <li class="api_list_li debug_tool" id="<?= $row["id"] ?>"><b><?= $row["id"] ?></b> <br>(<?= $row["name"] ?>)</li>
                                                    <?php endforeach; ?>

                                                    <li class="api_list_title" id="debug_tool">LBS相關</li>
                                                    <?php foreach ($geofence_api as $row) : ?>
                                                        <li class="api_list_li debug_tool" id="<?= $row["id"] ?>"><b><?= $row["id"] ?></b> <br>(<?= $row["name"] ?>)</li>
                                                    <?php endforeach; ?>

                                                    <li class="api_list_title" id="debug_tool">任務相關</li>
                                                    <?php foreach ($mission_api as $row) : ?>
                                                        <li class="api_list_li debug_tool" id="<?= $row["id"] ?>"><b><?= $row["id"] ?></b> <br>(<?= $row["name"] ?>)</li>
                                                    <?php endforeach; ?>

                                                    <li class="api_list_title" id="debug_tool">AR相關</li>
                                                    <?php foreach ($pattern_api as $row) : ?>
                                                        <li class="api_list_li debug_tool" id="<?= $row["id"] ?>"><b><?= $row["id"] ?></b> <br>(<?= $row["name"] ?>)</li>
                                                    <?php endforeach; ?>

                                                    <li class="api_list_title" id="debug_tool">雷達相關</li>
                                                    <?php foreach ($radar_api as $row) : ?>
                                                        <li class="api_list_li debug_tool" id="<?= $row["id"] ?>"><b><?= $row["id"] ?></b> <br>(<?= $row["name"] ?>)</li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding: 0px 15px;">
                                    <table width="100%">
                                        <tr class="right_panel_tr" id="result_frame_tr">
                                            <td class="right_panel_td" id="result_frame_td">
                                                <iframe id="result_frame" name="result_frame" scrolling="auto" width="100%"></iframe>
                                            </td>
                                        </tr>

                                        <tr class="right_panel_tr" id="api_tr">
                                            <td class="right_panel_td" id="api_td">
                                                <div class="api_wrapper">
                                                    <?php foreach ($topic_api as $row) : ?>
                                                        <?php include "forms/form_{$row["id"]}.php"; ?>
                                                    <?php endforeach; ?>

                                                    <?php foreach ($poi_api as $row) : ?>
                                                        <?php include "forms/form_{$row["id"]}.php"; ?>
                                                    <?php endforeach; ?>

                                                    <?php foreach ($geofence_api as $row) : ?>
                                                        <?php include "forms/form_{$row["id"]}.php"; ?>
                                                    <?php endforeach; ?>

                                                    <?php foreach ($mission_api as $row) : ?>
                                                        <?php include "forms/form_{$row["id"]}.php"; ?>
                                                    <?php endforeach; ?>

                                                    <?php foreach ($pattern_api as $row) : ?>
                                                        <?php include "forms/form_{$row["id"]}.php"; ?>
                                                    <?php endforeach; ?>

                                                    <?php foreach ($radar_api as $row) : ?>
                                                        <?php include "forms/form_{$row["id"]}.php"; ?>
                                                    <?php endforeach; ?>

                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊 ?>

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/select2/select2.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
        <?php if ($this->session->i18n == 'zh') : ?>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
        <?php endif; ?>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("assets/global/scripts/metronic_{$this->session->i18n}.js") ?>"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>"></script>
        <script src="<?= base_url("assets/global/scripts/datatable_{$this->session->i18n}.js") ?>"></script>
        <script src="<?= base_url("js/jquery.google.sha1.js") ?>"></script>

         <script nonce="cm1vaw==">
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Metronic.setAssetsPath('<?= base_url('assets') ?>/');
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar

                $('#api_tr').on('submit', 'form', function(){
                    show_result_form();
                });
            });

            $(document).ready(function () {
                $('.api_div').hide();
                $('.api_list_title').on('click', function () {
                    $('.' + $(this).attr('id')).slideDown();
                });
                $('.api_list_li').on('click', function () {
                    var id = $(this).attr('id');

                    $('.right_panel_tr').hide();
                    $('#api_tr').show();

                    $('.api_div').hide();
                    $('#' + id + '_div').show();

                    $('.api_list_li').removeClass("active");
                    $(this).addClass("active");

                    document.result_frame.location.href = "about:blank";
                });

                $('#result_frame').on("load", function () {
                    var html = $(result_frame.document.body.innerHTML).html();
                    result_frame.document.body.innerHTML = html;
                });

                $('.api_list_li').show();
                $('.api_list').show();

                $('.api_div form').submit(function () {
                    on_form_submit($(this));
                });
            });

            function on_form_submit(theForm) {
                var encrypt_token = '<?= $this->config->item("encrypt_token") ?>';
                var now = $.now() / 1000;

                theForm.find("[name='timestamp']").val(now);
                theForm.find("[name='mac']").val($.sha1(encrypt_token + now));
            }

            function show_result_form() {
                $('.right_panel_tr').hide();
                $('#result_frame_tr').show();
            }
        </script>
    </body>
</html>