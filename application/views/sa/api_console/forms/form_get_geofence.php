<div class="api_div" id="<?= $row["id"] ?>_div">
    <form id="form_<?= $row["id"] ?>" name="form_<?= $row["id"] ?>" target="result_frame" action="<?= base_url("/api/{$row["id"]}") ?>" method="post">
        <div class="input_value_div">
            <center><h2><?= $row["id"] ?></h2></center>
            <center><?= "{$this->config->item("api_base_url")}/api/{$row["id"]}" ?></center>
            <br />
            <h4><?= $row["name"] ?></h4>
            <br />
            <h4>輸入資料：</h4>

            <span class="var_name">lat</span> 使用者lat (選填)<br/>
            <input class="long" name="user_lat" value="" placeholder="使用者緯度"/><br/>

            <span class="var_name">lng</span> 使用者lng (選填)<br/>
            <input class="long" name="user_lng" value="" placeholder="使用者經度"/><br/>

            <button class="btn btn-success" type="submit">送出</button>
        </div>
    </form>
</div>