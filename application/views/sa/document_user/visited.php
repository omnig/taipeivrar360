<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>

        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //上方標題列 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption"> <i class="icon-direction font-green-haze"></i> <span class="caption-subject font-green-haze bold uppercase"></span> <span class="caption-helper"><?= $user_name ?> 的<?= $small_title ?> </span> </div>
                                    <div class="pull-right"><a href="<?= base_url("{$this->controller_name}/{$menu_category}") ?>" class="btn btn-default tooltips" data-original-title="返回"><i class="fa fa-reply"></i></a> </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="portlet box red-pink">
                                                <div class="portlet-title">
                                                    <div class="caption"><i class="fa fa-eye"></i>參觀歷程紀錄</div>
                                                    <ul class="nav nav-tabs">
                                                        <li class="active"><a href="#tab_all" data-toggle="tab" aria-expanded="true">全部</a></li>
                                                        <li><a href="#tab_exhibits" data-toggle="tab" aria-expanded="true">展品</a></li>
                                                        <li><a href="#tab_theme" data-toggle="tab" aria-expanded="true">分廳</a></li>
                                                        <li><a href="#tab_time" data-toggle="tab" aria-expanded="true">設時</a></li>
                                                        <li><a href="#tab_audio" data-toggle="tab" aria-expanded="true">語音</a></li>
                                                    </ul>
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="tab-content" style="height:100%;max-height:500px;overflow: auto">
                                                        <div class="tab-pane active" id="tab_all">
                                                            <div class="clearfix">
                                                                <?php $count = 0 ?>
                                                                <?php foreach ($visited as $row): ?>
                                                                    <?php if ($row->theme_id): ?>
                                                                        <div class="form-group">
                                                                            <div class="col-md-12">
                                                                                <div class="btn blue-madison-stripe">
                                                                                    <span class="label label-primary">分廳</span> <?= $row->r_timestamp ?>
                                                                                    <label class="bold"><?= $row->tg_title ?></label>
                                                                                    <span class="badge badge-pill badge-default" title="<?= $row->r_close_timestamp ?>"><?= get_ch_timer($row->r_timer) ?></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php $count++ ?>
                                                                    <?php endif; ?>
                                                                    <?php if ($row->time_id): ?>
                                                                        <div class="form-group">
                                                                            <div class="col-md-12">
                                                                                <div class="btn blue-stripe">
                                                                                    <span class="label label-info">設時</span> <?= $row->r_timestamp ?>
                                                                                    <label class="bold"><?= $row->tg_time . ' 分' ?></label>
                                                                                    <span class="badge badge-pill badge-default" title="<?= $row->r_close_timestamp ?>"><?= get_ch_timer($row->r_timer) ?></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php $count++ ?>
                                                                    <?php endif; ?>
                                                                    <?php if ($row->e_id): ?>
                                                                        <div class="form-group">
                                                                            <div class="col-md-12">
                                                                                <div class="btn green-stripe">
                                                                                    <span class="label label-success">展品</span> <?= $row->r_timestamp ?>
                                                                                    <label class="bold"><?= $row->e_name ?></label>
                                                                                    <span class="badge badge-pill badge-default" title="<?= $row->r_close_timestamp ?>"><?= get_ch_timer($row->r_timer) ?></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php $count++ ?>
                                                                    <?php endif; ?>
                                                                    <?php if ($row->audio_e_id): ?>
                                                                        <div class="form-group">
                                                                            <div class="col-md-12">
                                                                                <div class="btn yellow-stripe">
                                                                                    <span class="label label-warning">語音</span> <?= $row->r_timestamp ?>
                                                                                    <label class="bold"><?= $row->ae_name ?></label>
                                                                                    <span class="badge badge-pill badge-default" title="<?= $row->r_close_timestamp ?>"><?= get_ch_timer($row->r_timer) ?></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php $count++ ?>
                                                                    <?php endif; ?>
                                                                <?php endforeach; ?>
                                                                <?php if ($count == 0): ?>
                                                                    <div class="form-group">
                                                                        <div class="col-md-12">
                                                                            <label class="form-control no-border text-center">尚無紀錄</label>
                                                                        </div>
                                                                    </div>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab_exhibits">
                                                            <div class="clearfix">
                                                                <?php $count = 0 ?>
                                                                <?php foreach ($visited as $row): ?>
                                                                    <?php if ($row->e_id): ?>
                                                                        <div class="form-group">
                                                                            <div class="col-md-12">
                                                                                <div class="btn green-stripe">
                                                                                    <span class="label label-success">展品</span> <?= $row->r_timestamp ?>
                                                                                    <label class="bold"><?= $row->e_name ?></label>
                                                                                    <span class="badge badge-pill badge-default"><?= get_ch_timer($row->r_timer) ?></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php $count++ ?>
                                                                    <?php endif; ?>
                                                                <?php endforeach; ?>
                                                                <?php if ($count == 0): ?>
                                                                    <div class="form-group">
                                                                        <div class="col-md-12">
                                                                            <label class="form-control no-border text-center">尚無紀錄</label>
                                                                        </div>
                                                                    </div>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab_theme">
                                                            <div class="clearfix">
                                                                <?php $count = 0 ?>
                                                                <?php foreach ($visited as $row): ?>
                                                                    <?php if ($row->theme_id): ?>
                                                                        <div class="form-group">
                                                                            <div class="col-md-12">
                                                                                <div class="btn blue-madison-stripe">
                                                                                    <span class="label label-primary">分廳</span> <?= $row->r_timestamp ?>
                                                                                    <label class="bold"><?= $row->tg_title ?></label>
                                                                                    <span class="badge badge-pill badge-default"><?= get_ch_timer($row->r_timer) ?></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php $count++ ?>
                                                                    <?php endif; ?>
                                                                <?php endforeach; ?>
                                                                <?php if ($count == 0): ?>
                                                                    <div class="form-group">
                                                                        <div class="col-md-12">
                                                                            <label class="form-control no-border text-center">尚無紀錄</label>
                                                                        </div>
                                                                    </div>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab_time">
                                                            <div class="clearfix">
                                                                <?php $count = 0 ?>
                                                                <?php foreach ($visited as $row): ?>
                                                                    <?php if ($row->time_id): ?>
                                                                        <div class="form-group">
                                                                            <div class="col-md-12">
                                                                                <div class="btn blue-stripe">
                                                                                    <span class="label label-info">設時</span> <?= $row->r_timestamp ?>
                                                                                    <label class="bold"><?= $row->tg_time . ' 分' ?></label>
                                                                                    <span class="badge badge-pill badge-default"><?= get_ch_timer($row->r_timer) ?></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php $count++ ?>
                                                                    <?php endif; ?>
                                                                <?php endforeach; ?>
                                                                <?php if ($count == 0): ?>
                                                                    <div class="form-group">
                                                                        <div class="col-md-12">
                                                                            <label class="form-control no-border text-center">尚無紀錄</label>
                                                                        </div>
                                                                    </div>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab_audio">
                                                            <div class="clearfix">
                                                                <?php $count = 0 ?>
                                                                <?php foreach ($visited as $row): ?>
                                                                    <?php if ($row->audio_e_id): ?>
                                                                        <div class="form-group">
                                                                            <div class="col-md-12">
                                                                                <div class="btn yellow-stripe">
                                                                                    <span class="label label-warning">語音</span> <?= $row->r_timestamp ?>
                                                                                    <label class="bold"><?= $row->ae_name ?></label>
                                                                                    <span class="badge badge-pill badge-default"><?= get_ch_timer($row->r_timer) ?></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php $count++ ?>
                                                                    <?php endif; ?>
                                                                <?php endforeach; ?>
                                                                <?php if ($count == 0): ?>
                                                                    <div class="form-group">
                                                                        <div class="col-md-12">
                                                                            <label class="form-control no-border text-center">尚無紀錄</label>
                                                                        </div>
                                                                    </div>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="portlet green box">
                                                <div class="portlet-title">
                                                    <div class="caption"><i class="fa fa-bookmark"></i>書籤收藏紀錄</div>
                                                    <ul class="nav nav-tabs">
                                                        <li class="active"><a href="#fav_all" data-toggle="tab" aria-expanded="true">全部</a></li>
                                                        <li><a href="#fav_exhibits" data-toggle="tab" aria-expanded="true">展品</a></li>
                                                        <li><a href="#fav_theme" data-toggle="tab" aria-expanded="true">分廳</a></li>
                                                        <li><a href="#fav_time" data-toggle="tab" aria-expanded="true">設時</a></li>
                                                    </ul>
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="row">
                                                        <div class="tab-content" style="height:100%;max-height:500px;overflow: auto">
                                                            <div class="tab-pane active" id="fav_all">
                                                                <div class="clearfix">
                                                                    <?php $count = 0 ?>
                                                                    <?php foreach ($favorite as $row): ?>
                                                                        <?php if ($row->theme_id): ?>
                                                                            <div class="form-group">
                                                                                <div class="col-md-12">
                                                                                    <div class="btn blue-madison-stripe">
                                                                                        <span class="label label-primary">分廳</span> <?= $row->f_timestamp ?>
                                                                                        <label class="bold"><?= $row->tg_title ?></label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <?php $count++ ?>
                                                                        <?php endif; ?>
                                                                        <?php if ($row->time_id): ?>
                                                                            <div class="form-group">
                                                                                <div class="col-md-12">
                                                                                    <div class="btn blue-stripe">
                                                                                        <span class="label label-info">設時</span> <?= $row->f_timestamp ?>
                                                                                        <label class="bold"><?= $row->tg_time . ' 分' ?></label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <?php $count++ ?>
                                                                        <?php endif; ?>
                                                                        <?php if ($row->e_id): ?>
                                                                            <div class="form-group">
                                                                                <div class="col-md-12">
                                                                                    <div class="btn green-stripe">
                                                                                        <span class="label label-success">展品</span> <?= $row->f_timestamp ?>
                                                                                        <label class="bold"><?= $row->e_name ?></label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <?php $count++ ?>
                                                                        <?php endif; ?>
                                                                    <?php endforeach; ?>
                                                                    <?php if ($count == 0): ?>
                                                                        <div class="form-group">
                                                                            <div class="col-md-12">
                                                                                <label class="form-control no-border text-center">尚無紀錄</label>
                                                                            </div>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane" id="fav_exhibits">
                                                                <div class="clearfix">
                                                                    <?php $count = 0 ?>
                                                                    <?php foreach ($favorite as $row): ?>
                                                                        <?php if ($row->e_id): ?>
                                                                            <div class="form-group">
                                                                                <div class="col-md-12">
                                                                                    <div class="btn green-stripe">
                                                                                        <span class="label label-success">展品</span> <?= $row->f_timestamp ?>
                                                                                        <label class="bold"><?= $row->e_name ?></label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <?php $count++ ?>
                                                                        <?php endif; ?>
                                                                    <?php endforeach; ?>
                                                                    <?php if ($count == 0): ?>
                                                                        <div class="form-group">
                                                                            <div class="col-md-12">
                                                                                <label class="form-control no-border text-center">尚無紀錄</label>
                                                                            </div>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane" id="fav_theme">
                                                                <div class="clearfix">
                                                                    <?php $count = 0 ?>
                                                                    <?php foreach ($favorite as $row): ?>
                                                                        <?php if ($row->theme_id): ?>
                                                                            <div class="form-group">
                                                                                <div class="col-md-12">
                                                                                    <div class="btn blue-madison-stripe">
                                                                                        <span class="label label-primary">分廳</span> <?= $row->f_timestamp ?>
                                                                                        <label class="bold"><?= $row->tg_title ?></label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <?php $count++ ?>
                                                                        <?php endif; ?>
                                                                    <?php endforeach; ?>
                                                                    <?php if ($count == 0): ?>
                                                                        <div class="form-group">
                                                                            <div class="col-md-12">
                                                                                <label class="form-control no-border text-center">尚無紀錄</label>
                                                                            </div>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane" id="fav_time">
                                                                <div class="clearfix">
                                                                    <?php $count = 0 ?>
                                                                    <?php foreach ($favorite as $row): ?>
                                                                        <?php if ($row->time_id): ?>
                                                                            <div class="form-group">
                                                                                <div class="col-md-12">
                                                                                    <div class="btn blue-stripe">
                                                                                        <span class="label label-info">設時</span> <?= $row->f_timestamp ?>
                                                                                        <label class="bold"><?= $row->tg_time . ' 分' ?></label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <?php $count++ ?>
                                                                        <?php endif; ?>
                                                                    <?php endforeach; ?>
                                                                    <?php if ($count == 0): ?>
                                                                        <div class="form-group">
                                                                            <div class="col-md-12">
                                                                                <label class="form-control no-border text-center">尚無紀錄</label>
                                                                            </div>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE CONTENT-->
                    </div>
                </div>
                <!-- END CONTENT -->
            </div>
            <!-- END CONTAINER -->
            <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊 ?>

            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/select2/select2.min.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
            <?php if ($this->session->i18n == 'zh') : ?>
                <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
            <?php endif; ?>
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN PAGE LEVEL SCRIPTS -->
            <script src="<?= base_url("assets/global/scripts/metronic.js") ?>" type="text/javascript"></script>
            <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
            <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>
            <!-- END PAGE LEVEL SCRIPTS -->
             <script type="text/javascript" nonce="cm1vaw==">
                $(function () {
                    $('.date-picker').datepicker({
                        language: 'zh-TW',
                        autoclose: true,
                        format: 'yyyy-mm-dd',
                        defaultViewDate: {
                            year: 1990,
                            month: 0,
                            day: 1
                        }
                    });

                    $('#btn_submit').on('click', function () {
                        if (on_submit()) {
                            $('form').submit();
                        }
                    });

                    $('form').submit(on_submit);

                    function on_submit() {
                        var result = true;

                        $('.validation').each(function () {
                            var name = $(this).attr('for');

                            if (!$('*[name=' + name + ']').val()) {
                                $(this).show();
                                $('*[name=' + name + ']').focus();
                                event.preventDefault();
                                result = false;
                            } else {
                                $(this).hide();
                            }
                        });

                        return result;
                    }
                });
            </script>
             <script nonce="cm1vaw==">
                jQuery(document).ready(function () {
                    Metronic.init(); // init metronic core components
                    Layout.init(); // init current layout
                    QuickSidebar.init(); // init quick sidebar
                });
            </script>
            <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
