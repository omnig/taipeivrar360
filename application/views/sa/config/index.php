<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>

        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>
        <link rel="shortcut icon" href="<?= base_url("favicon.ico") ?>"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //上方標題列 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered form-fit">
                                <div class="portlet-title">
                                    <div class="caption"> <i class="icon-pencil font-green-haze"></i> <span class="caption-subject font-green-haze bold uppercase"></span> <span class="caption-helper"> <?= $this->lang->line("修改後別忘了儲存資料唷!") ?> </span> </div>
                                    <div class="pull-right"> <a href="javascript:;" class="btn blue tooltips submit" id="btn_submit" data-original-title="<?= $this->lang->line("儲存") ?>"><i class="fa fa-save"></i></a> </div>
                                </div>
                                <div class="portlet-body form">
                                    <form action="<?= base_url("{$this->controller_name}/{$table_name}/index_go") ?>" class="form-horizontal form-row-seperated" method="post">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><?= $this->lang->line("客服Email") ?>:</label>
                                                <div class="col-md-4">
                                                    <input type="text" name="<?= $field_prefix ?>_service_email" class="form-control" value="<?= ${$table_name}->{"{$field_prefix}_service_email"} ?>"/>
                                                    <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_service_email">
                                                        <button class="close" data-close="alert"></button>
                                                        <span> <?= $this->lang->line("請輸入") ?><?= $this->lang->line("客服Email") ?> </span> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><?= $this->lang->line("管理員Email") ?>:</label>
                                                <div class="col-md-4">
                                                    <input type="text" name="<?= $field_prefix ?>_sa_email" class="form-control" value="<?= ${$table_name}->{"{$field_prefix}_sa_email"} ?>"/>
                                                    <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_sa_email">
                                                        <button class="close" data-close="alert"></button>
                                                        <span> <?= $this->lang->line("請輸入") ?><?= $this->lang->line("管理員Email") ?> </span> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><?= $this->lang->line("關鍵字") ?>:</label>
                                                <div class="col-md-4">
                                                    <input type="text" name="<?= $field_prefix ?>_meta_keyword" class="form-control"  value="<?= ${$table_name}->{"{$field_prefix}_meta_keyword"} ?>" />
                                                    <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_meta_keyword">
                                                        <button class="close" data-close="alert"></button>
                                                        <span> <?= $this->lang->line("請輸入") ?><?= $this->lang->line("關鍵字") ?> </span> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><?= $this->lang->line("網站描述") ?>:</label>
                                                <div class="col-md-4">
                                                    <input type="text" name="<?= $field_prefix ?>_meta_description" class="form-control"  value="<?= ${$table_name}->{"{$field_prefix}_meta_description"} ?>" />
                                                    <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_meta_description">
                                                        <button class="close" data-close="alert"></button>
                                                        <span> <?= $this->lang->line("請輸入") ?><?= $this->lang->line("網站描述") ?> </span> 
                                                    </div>
                                                </div>
                                            </div>
                <!--                             <div class="form-group">
                                                <label class="control-label col-md-3"><?= $this->lang->line("預設首頁") ?>:</label>
                                                <div class="col-md-4">
                                                    <input type="text" name="<?= $field_prefix ?>_default_page" class="form-control"  value="<?= ${$table_name}->{"{$field_prefix}_default_page"} ?>" placeholder="預設首頁請輸入：home" />
                                                    <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_default_page">
                                                        <button class="close" data-close="alert"></button>
                                                        <span> <?= $this->lang->line("請輸入") ?><?= $this->lang->line("預設首頁") ?> </span> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><?= $this->lang->line("預設首頁") . $this->lang->line("參數") ?>:</label>
                                                <div class="col-md-4">
                                                    <input type="text" name="<?= $field_prefix ?>_default_page_func" class="form-control"  value="<?= ${$table_name}->{"{$field_prefix}_default_page_func"} ?>" />
                                                </div>
                                            </div> -->
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><?= $this->lang->line("追蹤碼") ?>:</label>
                                                <div class="col-md-9">
                                                    <textarea name="<?= $field_prefix ?>_counter" rows="8" class="form-control"><?= ${$table_name}->{"{$field_prefix}_counter"} ?></textarea>
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><?= $this->lang->line("iOS App下載網址") ?>:</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="<?= $field_prefix ?>_ios_app_url" class="form-control"  value="<?= ${$table_name}->{"{$field_prefix}_ios_app_url"} ?>" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><?= $this->lang->line("Android App下載網址") ?>:</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="<?= $field_prefix ?>_android_app_url" class="form-control"  value="<?= ${$table_name}->{"{$field_prefix}_android_app_url"} ?>" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">APP預設緯度:</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="<?= $field_prefix ?>_app_lat" class="form-control"  value="<?= ${$table_name}->{"{$field_prefix}_app_lat"} ?>" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">APP預設經度:</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="<?= $field_prefix ?>_app_lng" class="form-control"  value="<?= ${$table_name}->{"{$field_prefix}_app_lng"} ?>" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">WEB預設緯度:</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="<?= $field_prefix ?>_web_lat" class="form-control"  value="<?= ${$table_name}->{"{$field_prefix}_web_lat"} ?>" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">WEB預設經度:</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="<?= $field_prefix ?>_web_lng" class="form-control"  value="<?= ${$table_name}->{"{$field_prefix}_web_lng"} ?>" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">首頁說明文字(限定100字):</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="<?= $field_prefix ?>_index_text" class="form-control"  value="<?= ${$table_name}->{"{$field_prefix}_index_text"} ?>" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">關卡預設觸發距離:</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="<?= $field_prefix ?>_trigger_distance" class="form-control"  value="<?= ${$table_name}->{"{$field_prefix}_trigger_distance"} ?>" />
                                                </div>
                                            </div>
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-4">
                                                    <button type="submit" class="btn blue"><i class="fa fa-save"></i> <?= $this->lang->line("儲存") ?> </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊 ?>

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/select2/select2.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
        <?php if ($this->session->i18n == 'zh') : ?>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
        <?php endif; ?>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("assets/global/scripts/metronic.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
         <script type="text/javascript" nonce="cm1vaw==">
            $(function () {
                $('#btn_submit').on('click', function () {
                    if (on_submit()) {
                        $('form').submit();
                    }
                });

                $('form').submit(on_submit);

                function on_submit() {
                    var result = true;

                    $('.validation').each(function () {
                        var name = $(this).attr('for');

                        if (!$('*[name=' + name + ']').val()) {
                            $(this).show();
                            $('*[name=' + name + ']').focus();
                            event.preventDefault();
                            result = false;
                        } else {
                            $(this).hide();
                        }
                    });

                    return result;
                }
            });
        </script>
         <script nonce="cm1vaw==">
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init() // init quick sidebar
            });
            function checkall(selector) {
                $(selector).prop('checked', true);
                $(selector).parent().addClass('checked');
            }
            function uncheck(selector) {
                $(selector).prop('checked', false);
                $(selector).parent().removeClass('checked');
            }
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
