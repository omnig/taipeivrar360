<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>

        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //上方標題 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="note note-danger">
                                <p> <?= $this->lang->line("說明: 您可以透過新增、編輯、刪除、搜索，來管理列表。") ?></p>
                            </div>
                            <div class="portlet">
                                <div class="portlet-title">
                                    <div class="actions"> 
                                        <a href="<?= base_url("{$this->controller_name}/{$table_name}/add") ?>" class="btn default purple-stripe"> <i class="fa fa-plus"></i> <span class="hidden-480"> <?= $this->lang->line("新增") ?><?= $item_name ?> </span> </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container">
                                        <div class="table-actions-wrapper"> <span> </span>
                                            <select class="table-group-action-input form-control input-inline input-small input-sm">
                                                <option value=""><?= $this->lang->line("選擇") ?>..</option>
                                                <option value="enable"><?= $this->lang->line("啟用") ?></option>
                                                <option value="disable"><?= $this->lang->line("關閉") ?></option>
                                                <option value="del"><?= $this->lang->line("刪除") ?></option>
                                            </select>
                                            <button class="btn btn-sm green table-group-action-submit"><i class="fa fa-check"></i> <?= $this->lang->line("變更") ?></button>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover" id="datatable_ajax">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="2%"> <input type="checkbox" class="group-checkable"></th>
                                                    <th width="5%"> <?= $this->lang->line("編號") ?>&nbsp;# </th>
                                                    <th> 主辦局處 </th>
                                                    <th> 主題群組名稱 </th>
                                                    <th> 主題群組描述 </th>
                                                    <th width="10%"> 圖片 </th>
                                                    <th> 主題數 </th>
                                                    <th width="10%"> <?= $this->lang->line("狀態") ?> </th>
                                                    <th width="10%"> <?= $this->lang->line("操作") ?> </th>
                                                </tr>
                                                <tr role="row" class="filter">
                                                    <td></td>
                                                    <td></td>
                                                    <td>
                                                        <select name="filters[d_id]" class="form-control form-filter input-sm">
                                                            <option></option>
                                                            <?php foreach ($department_list as $key => $value):?>
                                                                <option value="<?=$value->ag_id?>"><?= $value->ag_name?></option>
                                                            <?php endforeach;?>
                                                        </select>
                                                    </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="filters[<?= $field_prefix ?>_name]"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>
                                                        <select name="filters[<?= $field_prefix ?>_enabled]" class="form-control form-filter input-sm">
                                                            <option></option>
                                                            <option value="Y"><?= $this->lang->line("啟用中") ?></option>
                                                            <option value="N"><?= $this->lang->line("已關閉") ?></option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <div class="margin-bottom-5">
                                                            <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> <?= $this->lang->line("搜索") ?></button>
                                                        </div>
                                                        <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> <?= $this->lang->line("重設") ?></button>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊 ?>

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/select2/select2.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
        <?php if ($this->session->i18n == 'zh') : ?>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
        <?php endif; ?>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("assets/global/scripts/metronic_{$this->session->i18n}.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/global/scripts/datatable_{$this->session->i18n}.js") ?>"></script>
        <script src="<?= base_url("assets/admin/pages/scripts/table-ajax_{$this->session->i18n}.js") ?>"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
         <script nonce="cm1vaw==">
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Metronic.setAssetsPath('<?= base_url('assets') ?>/');
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
                TableAjax.init('<?= base_url("{$this->controller_name}/{$table_name}/ajax_list") ?>');
            });

            $('body').on('click', '.delete', function(){
                let id = $(this).attr('data');
                if(confirm("確定刪除？")){
                    location.href = '<?= $del_url.'?'.$field_prefix.'_id=' ?>' + id;
                }
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
