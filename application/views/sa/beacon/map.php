<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>

        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //上方標題 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="note note-danger">
                                <p> <?= $this->lang->line("說明: 您可以透過刪除、搜索，來管理列表。") ?></p>
                            </div>
                            <div class="portlet">
                                <div class="portlet-title">
                                    <div class="actions"> 
                                        <a href="<?= base_url("{$this->controller_name}/{$table_name}") ?>" class="btn default blue-stripe"><i class="fa fa-list"></i> Beacon列表</a>
                                        <a href="<?= base_url("{$this->controller_name}/{$table_name}/add") ?>" class="btn default purple-stripe"> <i class="fa fa-plus"></i> <span class="hidden-480"> <?= $this->lang->line("新增") ?>Beacon </span> </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <?php $arr = []; ?>
                                            <select id="select-floor" class="form-control">
                                                <option value="0">請選擇樓層</option>
                                                <?php foreach ($beacons as $row): ?>
                                                    <?php if (!in_array($row->af_id, $arr)): ?>
                                                        <option value="<?= $row->af_id ?>"><?= ($row->ab_name) ? $row->ab_name . "[" . $row->af_name . "]" : "尚未設定" ?></option>
                                                        <?php array_push($arr, $row->af_id); ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4 radio-list">
                                            <div class="row btn-group">
                                                <label class="btn no-border nowrap">
                                                    <input type="radio" name="select_range" class="radio-list" value="Y" checked="checked" >顯示最大距離範圍
                                                </label>
                                                <label class="btn no-border nowrap">
                                                    <input type="radio" name="select_range" class="radio-list" value="N" >不顯示最大距離範圍 
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div id="map" class="map"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊 ?>

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/select2/select2.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
        <?php if ($this->session->i18n == 'zh') : ?>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
        <?php endif; ?>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("assets/global/scripts/metronic_{$this->session->i18n}.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("js/map_list.js") ?>" type="text/javascript"></script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= $this->config->item("google")["api_key"] ?>&callback=map_list.initMap"></script>
         <script nonce="cm1vaw==">
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Metronic.setAssetsPath('<?= base_url('assets') ?>/');
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
                map_list.init('<?= base_url() ?>');
                setTimeout(function () {
                    var selected_id = $("#select-floor").val();
                    var selected_range = $("input[name=select_range]:checked").val();
                    set_beacon_map(selected_id, selected_range);
                }, 300);
            });


            $("#select-floor").change(function () {
                var selected_id = $(this).val();
                var selected_range = $("input[name=select_range]:checked").val();
                set_beacon_map(selected_id, selected_range);
            });

            $("input[name=select_range]").change(function () {
                var selected_range = $("input[name=select_range]:checked").val();
                var selected_id = $("#select-floor").val();
                set_beacon_map(selected_id, selected_range);
            });

            function set_beacon_map(selected_id, is_range) {
                map_list.clear_all();
                <?php foreach ($beacons as $idx => $row): ?>
                    var af_id = "<?= ($row->af_id) ? $row->af_id : '' ?>";
                    if (selected_id == "0" || selected_id == af_id) {
                        var content = "<?= "位置說明：" . $row->b_description . "<br>" . "樓層資訊：" . $row->ab_name . "[" . $row->af_name . "]" . "<br>" . "MAJOR：" . $row->b_major . "<br>" . "MINOR：" . $row->b_minor . "<br>" ?>";
                        content += "<?= "設置時間：" . $row->b_set_timestamp . "<br>" ?>";
                        content += "<?= ($row->b_image) ? "<img width='150px' src='" . base_url("upload/beacon") . "/" . $row->b_image . "' >" : '' ?>";
                        content += "<br><a href='<?= base_url("sa/beacon/edit?b_id=") . $row->b_id ?>' class='btn btn-xs default blue-stripe btn-operating'><i class='fa fa-pencil'>編輯</i></a>";
                        var range = (is_range == "Y") ?<?= $row->b_range ?> : 0;
                        map_list.set_beacon(<?= $idx ?>, "<?= $row->b_description ?>",<?= $row->b_minor ?>,<?= ($row->b_lat) ? $row->b_lat : 0 ?>,<?= ($row->b_lng) ? $row->b_lng : 0 ?>, range, "<?= ($row->b_voltage) ? $row->b_voltage : '' ?>", content);
                        map_list.set_tile_map(<?= $idx ?>, "<?= $row->af_plan_id ?>", "<?= $row->af_bottom_left_lat ?>", "<?= $row->af_bottom_left_lng ?>", "<?= $row->af_top_right_lat ?>", "<?= $row->af_top_right_lng ?>");
                    }
                <?php endforeach; ?>
                map_list.fit_bounce();
            }
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
