<!DOCTYPE html>
<!--[if IE 8]> <html lang="zh-TW" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="zh-TW" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>
        <link rel="stylesheet" type="text/css" href="<?= base_url("assets/global/plugins/bootstrap-summernote/summernote.css") ?>">
        <link href="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url("assets/plugins/jquery-ui/jquery-ui.css") ?>" rel="stylesheet" type="text/css" />
        <link href='<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.css") ?>' rel='stylesheet'>
        <link href="<?= base_url("assets/base/css/custom.css") ?>" rel="stylesheet"></link>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //上方標題列 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered form-fit">
                                <div class="portlet-title">
                                    <div class="caption"> <i class="icon-pencil font-green-haze"></i> <span class="caption-subject font-green-haze bold uppercase"></span> <span class="caption-helper"> <?= $this->lang->line("修改後別忘了儲存資料唷!") ?> </span> </div>
                                    <div class="pull-right"> <a href="javascript:;" class="btn blue tooltips submit" id="btn_submit" data-original-title="<?= $this->lang->line("儲存") ?>"><i class="fa fa-save"></i></a> <a href="<?= base_url("{$this->controller_name}/{$table_name}") ?>" class="btn btn-default tooltips" data-original-title="返回"><i class="fa fa-reply"></i></a> </div>
                                </div>
                                <div class="portlet-body form">
                                    <form action="<?= base_url("{$this->controller_name}/{$table_name}/edit_go") ?>" enctype="multipart/form-data" class="form-horizontal form-row-seperated" method="post">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">位置說明:</label>
                                                <div class="col-md-6">
                                                    <input type="text" name="<?= $field_prefix ?>_description" class="form-control" value="<?= ${$table_name}->{"{$field_prefix}_description"} ?>" placeholder="請輸入位置說明" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">UUID:</label>
                                                <div class="col-md-6">
                                                    <input type="text" name="<?= $field_prefix ?>_uuid" readonly class="form-control" value="<?= ${$table_name}->{"{$field_prefix}_uuid"} ?>"/>
                                                    <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_uuid">
                                                        <button class="close" data-close="alert"></button>
                                                        <span> <?= $this->lang->line("請輸入") ?>UUID </span> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">MAJOR:</label>
                                                <div class="col-md-6">
                                                    <input type="text" name="<?= $field_prefix ?>_major" class="form-control" value="<?= ${$table_name}->{"{$field_prefix}_major"} ?>"/>
                                                    <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_major">
                                                        <button class="close" data-close="alert"></button>
                                                        <span> <?= $this->lang->line("請輸入") ?>MAJOR </span> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">MINOR:</label>
                                                <div class="col-md-6">
                                                    <input type="text" name="<?= $field_prefix ?>_minor" class="form-control" value="<?= ${$table_name}->{"{$field_prefix}_minor"} ?>"/>
                                                    <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_minor">
                                                        <button class="close" data-close="alert"></button>
                                                        <span> <?= $this->lang->line("請輸入") ?>MINOR </span> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">硬體ID:</label>
                                                <div class="col-md-6">
                                                    <input type="text" name="<?= $field_prefix ?>_hwid" class="form-control" value="<?= ${$table_name}->{"{$field_prefix}_hwid"} ?>"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">MAC:</label>
                                                <?php
                                                $mac = str_split(${$table_name}->{"{$field_prefix}_mac"}, 2);
                                                $mac = implode(":", $mac);
                                                ?>
                                                <div class="col-md-6">
                                                    <input type="text" name="<?= $field_prefix ?>_mac" class="form-control" value="<?= $mac ?>"/>
                                                    <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_mac">
                                                        <button class="close" data-close="alert"></button>
                                                        <span> <?= $this->lang->line("請輸入") ?>MAC </span> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">場域資訊:</label>
                                                <div class="col-md-6">
                                                    <select class="form-control selected" name="af_id">
                                                        <?php foreach ($ap_floors as $key => $row) : ?>
                                                            <?php
                                                            if (${$table_name}->af_id == $row->af_id) {
                                                                echo " <script nonce=\"cm1vaw==\">setTimeout(function(){area_select_map.set_tile_map('$row->af_plan_id', $row->af_bottom_left_lat, $row->af_bottom_left_lng, $row->af_top_right_lat, $row->af_top_right_lng);},500);</script>";
                                                            }
                                                            ?>
                                                            <option value="<?= $row->af_id ?>" <?= (${$table_name}->{"af_id"} == $row->af_id) ? "selected" : "" ?> >
                                                                <?= $row->ab_name . " [" . $row->af_name . "]" ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">beacon坐標:</label>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control lat" name="<?= $field_prefix ?>_lat" value="<?= (!${$table_name}->{"{$field_prefix}_lat"}) ? 0 : ${$table_name}->{"{$field_prefix}_lat"} ?>" placeholder="請輸入經度" >
                                                    <input type="text" class="form-control lng" name="<?= $field_prefix ?>_lng" value="<?= (!${$table_name}->{"{$field_prefix}_lng"}) ? 0 : ${$table_name}->{"{$field_prefix}_lng"} ?>" placeholder="請輸入緯度" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"></label>
                                                <div class="col-md-6">
                                                    <label class="control-label newlat" hidden></label>
                                                    <label class="control-label newlng" hidden></label>
                                                    <div id="map"></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">最大距離:</label>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" name="<?= $field_prefix ?>_range" value="<?= ${$table_name}->{"{$field_prefix}_range"} ?>" placeholder="請輸入距離" >
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label col-md-3"><?= $this->lang->line("圖片") ?>:</label>
                                                <div class="col-md-6">
                                                    <?php
                                                    if (!${$table_name}->{"{$field_prefix}_image"}) {
                                                        $btn_remove = false;
                                                        $imgsrc = "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no%20picture";
                                                        $fileinput_status = 'fileinput-new';
                                                    } else {
                                                        $btn_remove = true;
                                                        $imgsrc = base_url("upload/beacon") . "/" . ${$table_name}->{"{$field_prefix}_image"};
                                                        $fileinput_status = 'fileinput-exists';
                                                    }
                                                    ?>
                                                    <?php if ($btn_remove): ?>
                                                        <div class="remove_image_root">
                                                            <label>
                                                                <input class="remove_image" name="remove_image[<?= $field_prefix ?>_image]" type="checkbox" /> 刪除圖片
                                                            </label>
                                                        </div>
                                                    <?php endif; ?>
                                                    <div class="fileinput <?= $fileinput_status ?>" data-provides="fileinput">
                                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                                            <img src='<?= $imgsrc ?>' alt='' />
                                                        </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> <?= $this->lang->line("選取圖片") ?> </span>
                                                                <span class="fileinput-exists"> <?= $this->lang->line("更換") ?> </span>
                                                                <input type="file" name="<?= $field_prefix ?>_image" accept="image/*">
                                                            </span>
                                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?= $this->lang->line("移除") ?> </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">設置時間:</label>
                                                <div class="col-md-6">
                                                    <input class="form-control date-picker" name="<?= $field_prefix ?>_set_timestamp" type="text" value="<?= ${$table_name}->{"{$field_prefix}_set_timestamp"} ?>" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><?= $this->lang->line("狀態") ?>:</label>
                                                <div class="col-md-6">
                                                    <select class="form-control" name="<?= $field_prefix ?>_enabled">
                                                        <option value="Y"<?= (${$table_name}->{"{$field_prefix}_enabled"} == 'Y') ? ' selected' : '' ?>><?= $this->lang->line("啟用中") ?></option>
                                                        <option value="N"<?= (${$table_name}->{"{$field_prefix}_enabled"} == 'N') ? ' selected' : '' ?>><?= $this->lang->line("已關閉") ?></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                                        </div>

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-6">
                                                    <input type="hidden" name="<?= "{$field_prefix}_id" ?>" value="<?= ${$table_name}->{"{$field_prefix}_id"} ?>" />
                                                    <button type="submit" class="btn blue"><i class="fa fa-save"></i> <?= $this->lang->line("儲存") ?> </button>
                                                    <a href="<?= base_url("{$this->controller_name}/{$table_name}") ?>" class="btn btn-default tooltips" data-original-title="<?= $this->lang->line("取消") ?>"><?= $this->lang->line("取消") ?></a> 
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊     ?>

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/select2/select2.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-summernote/summernote.min.js") ?>"></script> 
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-summernote/lang/summernote-zh-TW.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
        <?php if ($this->session->i18n == 'zh') : ?>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
        <?php endif; ?>
        <script type="text/javascript" src="<?= base_url("assets/plugins/jquery-ui/jquery-ui.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.js") ?>"></script>
        <script type='text/javascript' src='<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-sliderAccess.js") ?>'></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("assets/global/scripts/metronic.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= $this->config->item("google")["api_key"] ?>&callback=area_select_map.initMap"></script>
        <script src="<?= base_url("js/google_map.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("js/geofence.js") ?>" type="text/javascript"></script>
        
         <script type="text/javascript" nonce="cm1vaw==">
            $(function () {
                $('#btn_submit').on('click', function () {
                    if (on_submit()) {
                        $('form').submit();
                    }
                });
                $('form').submit(on_submit);
                function on_submit() {
                    var result = true;
                    $('.validation').each(function () {
                        var name = $(this).attr('for');
                        if (!$('*[name=' + name + ']').val()) {
                            $(this).show();
                            $('*[name=' + name + ']').focus();
                            event.preventDefault();
                            result = false;
                        } else {
                            $(this).hide();
                        }
                    });
                    return result;
                }
                $('.date-picker').datetimepicker({
                    dateFormat: 'yy-mm-dd',
                    timeFormat: 'HH:mm'
                });

                $('.remove_image').click(function () {
                    if ($(this).prop('checked')) {
                        $(this).parents('.remove_image_root').next().hide();
                        $(this).parents('.remove_image_root').next().next().hide();
                    } else {
                        $(this).parents('.remove_image_root').next().show();
                        $(this).parents('.remove_image_root').next().next().show();
                    }
                });
            });
        </script>
         <script nonce="cm1vaw==">
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
                area_select_map.init('<?= base_url() ?>');
            });

            $('.selected').change(function () {
                var val = $(this).val();
                if (val == 0) {
                    $('.lat').val(<?= $this->config->item('default_lat') ?>);
                    $('.lng').val(<?= $this->config->item('default_lng') ?>);
                    area_select_map.set_center(<?= $this->config->item('default_lat') ?>, <?= $this->config->item('default_lng') ?>);
                    area_select_map.set_tile_map();
                }
<?php foreach ($ap_floors as $row): ?>
                    if (val ==<?= $row->af_id ?>) {
                        var lat =<?= ($row->af_bottom_left_lat + $row->af_top_right_lat) / 2 ?>;
                        var lng =<?= ($row->af_bottom_left_lng + $row->af_top_right_lng) / 2 ?>;
                        $('.lat').val(lat);
                        $('.lng').val(lng);
                        area_select_map.set_center(lat, lng);
                        area_select_map.set_tile_map("<?= $row->af_plan_id ?>",<?= $row->af_bottom_left_lat ?>,<?= $row->af_bottom_left_lng ?>,<?= $row->af_top_right_lat ?>,<?= $row->af_top_right_lng ?>);
                    }
<?php endforeach; ?>
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
