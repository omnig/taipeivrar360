<div class="api_div" id="<?= $row["id"] ?>_div">
    <form id="form_<?= $row["id"] ?>" name="form_<?= $row["id"] ?>" target="result_frame" action="<?= base_url("/locapi/{$row["id"]}") ?>" method="get">

        <?php $ab_id = $this->Common_model->get_one_field('ap_building', 'ab_id'); ?>

        <div class="input_value_div">
            <center><h2><?= $row["id"] ?></h2></center>
            <center><?= "{$this->config->item("api_base_url")}/locapi/{$row["id"]}" . "?b=" . $ab_id ?></center>
            <br />
            <h4>說明：<?= $row["name"] ?></h4>
            <br />
            <h4>輸入資料：</h4>

            <span class="var_name">b</span> 建物ID<br/>
            <input class="long" name="b" value="<?= $ab_id ?>"/><br/>

            <input type="hidden" name="timestamp" />
            <input type="hidden" name="mac" />

            <button class="btn btn-success" type="submit">送出</button>
        </div>
    </form>
</div>