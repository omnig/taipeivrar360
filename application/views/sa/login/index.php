<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>

        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?= base_url("assets/admin/pages/css/login2.css") ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url("assets/base/css/custom.css") ?>" rel="stylesheet"></link>
        <!-- END PAGE LEVEL SCRIPTS -->
    </head>
    <!-- BEGIN BODY -->
    <body class="login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="<?= base_url() ?>">
                <img src="<?= base_url("images/taipei_logo.jpeg") ?>" alt=""/>
            </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="menu-toggler sidebar-toggler">
        </div>
        <!-- END SIDEBAR TOGGLER BUTTON -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- content -->
            <div class="text-center">
                <h4>請由<a href="http://taipeion.gov.taipei/"> 台北on </a>登入</h4>
                一般使用者無法登入，請洽各機關管理者開通權限
            </div>
        </div>
        <!-- END LOGIN -->
        <!-- BEGIN COPYRIGHT -->
        <div class="copyright">
            <!-- <?= date('Y') ?> &copy; <a href="<?= base_url() ?>"><?= $this->lang->line("company_name") ?></a><?= $this->lang->line("版權所有") ?>。 -->
        </div>
        <footer id="footer" class="footer">
            <div class="footer-wrapper">
                <div class="col-sm-12 footer_desc"></div>
                <div class="col-xs-12 bg-white">
                    <div class="col-md-6 col-sm-5 col-xs-5">
                        <div class="footer-brand"></div>
                    </div>
                    <div class="col-md-6 col-sm-7 col-xs-7">
                        <div class="copyright">
                        <?= date('Y') ?> &copy; <a href="<?= base_url() ?>"><?= $this->lang->line("company_name") ?></a> <?= $this->lang->line("版權所有") ?>                </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- END COPYRIGHT -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
        <script src="<?= base_url("assets/global/plugins/respond.min.js") ?>"></script>
        <script src="<?= base_url("assets/global/plugins/excanvas.min.js") ?>"></script> 
        <![endif]-->
        <script src="<?= base_url("assets/global/plugins/jquery.min.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/global/plugins/jquery-migrate.min.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/global/plugins/bootstrap/js/bootstrap.min.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/global/plugins/jquery.blockui.min.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/global/plugins/jquery.cokie.min.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/global/plugins/uniform/jquery.uniform.min.js") ?>" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?= base_url("assets/global/plugins/jquery-validation/js/jquery.validate.min.js") ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("assets/global/scripts/metronic.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/demo.js") ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
         <script nonce="cm1vaw==">
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Metronic.setAssetsPath('<?= base_url('assets') ?>/');
                Layout.init(); // init current layout
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>