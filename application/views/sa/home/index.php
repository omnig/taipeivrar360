<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>
        
        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("assets/plugins/jquery-ui/jquery-ui.css") ?>" rel="stylesheet" type="text/css" />
        <link href='<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.css") ?>' rel='stylesheet'>
        <link href="<?= base_url("assets/base/css/custom.css") ?>" rel="stylesheet"></link>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //頁面上方標題 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            請點側邊選單進行系統管理
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="dashboard-stat grey-salsa">
                                <div class="visual"><i class="fa fa-users"></i></div>
                                <div class="details">
                                    <div class="number"><?= $report->USE_COUNT??'N/A' ?></div>
                                    <div class="desc">當日使用人數</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="dashboard-stat grey-salsa">
                                <div class="visual"><i class="fa fa-users"></i></div>
                                <div class="details">
                                    <div class="number"><?= $report_week->USE_COUNT??'N/A' ?></div>
                                    <div class="desc">過去一個星期使用人數</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="dashboard-stat grey-salsa">
                                <div class="visual"><i class="fa fa-users"></i></div>
                                <div class="details">
                                    <div class="number"><?= $report_month->USE_COUNT??'N/A' ?></div>
                                    <div class="desc">過去一個月使用人數</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="dashboard-stat grey-salsa">
                                <div class="visual"><i class="fa fa-users"></i></div>
                                <div class="details">
                                    <div class="number"><?= $report->ONLINE_COUNT??'N/A' ?></div>
                                    <div class="desc">當日同時上線最高人數</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="dashboard-stat grey-salsa">
                                <div class="visual"><i class="fa fa-users"></i></div>
                                <div class="details">
                                    <div class="number"><?= $report_week->ONLINE_COUNT??'N/A' ?></div>
                                    <div class="desc">過去一個星期同時上線最高人數</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="dashboard-stat grey-salsa">
                                <div class="visual"><i class="fa fa-users"></i></div>
                                <div class="details">
                                    <div class="number"><?= $report_month->ONLINE_COUNT??'N/A' ?></div>
                                    <div class="desc">過去一個月同時上線最高人數</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="dashboard-stat grey-salsa">
                                <div class="visual"><i class="fa fa-users"></i></div>
                                <div class="details">
                                    <div class="number"><?= !is_null($report->CPU_USAGE_MAX) ? $report->CPU_USAGE_MAX.'%' : 'N/A' ?> | <?= !is_null($report->CPU_USAGE_MIN) ? $report->CPU_USAGE_MIN.'%' : 'N/A' ?></div>
                                    <div class="desc">當日CPU使用率 (最高 | 最低)</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="dashboard-stat grey-salsa">
                                <div class="visual"><i class="fa fa-users"></i></div>
                                <div class="details">
                                    <div class="number"><?= !is_null($report_week->CPU_USAGE_MAX) ? $report_week->CPU_USAGE_MAX.'%' : 'N/A' ?> | <?= !is_null($report_week->CPU_USAGE_MIN) ? $report_week->CPU_USAGE_MIN.'%' : 'N/A' ?></div>
                                    <div class="desc">過去一個星期CPU使用率 (最高 | 最低)</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="dashboard-stat grey-salsa">
                                <div class="visual"><i class="fa fa-users"></i></div>
                                <div class="details">
                                    <div class="number"><?= !is_null($report_month->CPU_USAGE_MAX) ? $report_month->CPU_USAGE_MAX.'%' : 'N/A' ?> | <?= !is_null($report_month->CPU_USAGE_MIN) ? $report_month->CPU_USAGE_MIN.'%' : 'N/A' ?></div>
                                    <div class="desc">過去一個月CPU使用率 (最高 | 最低)</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="dashboard-stat grey-salsa">
                                <div class="visual"><i class="fa fa-users"></i></div>
                                <div class="details">
                                    <div class="number"><?= $report->MEM_USAGE_MAX ? number_format($report->MEM_USAGE_MAX, 2).'%' : 'N/A' ?> | <?= $report->MEM_USAGE_MIN ? number_format($report->MEM_USAGE_MIN, 2).'%' : 'N/A' ?></div>
                                    <div class="desc">當日記憶體使用率 (最高 | 最低)</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="dashboard-stat grey-salsa">
                                <div class="visual"><i class="fa fa-users"></i></div>
                                <div class="details">
                                    <div class="number"><?= $report_week->MEM_USAGE_MAX ? number_format($report_week->MEM_USAGE_MAX, 2).'%' : 'N/A' ?> | <?= $report_week->MEM_USAGE_MIN ? number_format($report_week->MEM_USAGE_MIN, 2).'%' : 'N/A' ?></div>
                                    <div class="desc">過去一個星期記憶體使用率 (最高 | 最低)</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="dashboard-stat grey-salsa">
                                <div class="visual"><i class="fa fa-users"></i></div>
                                <div class="details">
                                    <div class="number"><?= $report_month->MEM_USAGE_MAX ? number_format($report_month->MEM_USAGE_MAX, 2).'%' : 'N/A' ?> | <?= $report_month->MEM_USAGE_MIN ? number_format($report_month->MEM_USAGE_MIN, 2).'%' : 'N/A' ?></div>
                                    <div class="desc">過去一個月記憶體使用率 (最高 | 最低)</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="h2">搜尋</div>
                            <form id="search" class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <input class="form-control date-picker" name="start_time" type="text" placeholder="請輸入起始日期" />
                                    </div>
                                    <div class="col-md-3">
                                        <input class="form-control date-picker" name="end_time" type="text" placeholder="請輸入結束日期" />
                                    </div>
                                    <div class="col-md-3">
                                        <a href="javascript:;" class="btn blue tooltips submit" id="btn_submit" data-original-title="搜尋">搜尋</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="dashboard-stat grey-salsa">
                                <div class="visual"><i class="fa fa-users"></i></div>
                                <div class="details">
                                    <div id="use_count" class="number"></div>
                                    <div class="desc">使用人數</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="dashboard-stat grey-salsa">
                                <div class="visual"><i class="fa fa-users"></i></div>
                                <div class="details">
                                    <div id="online_count" class="number"></div>
                                    <div class="desc">同時上線最高人數</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="dashboard-stat grey-salsa">
                                <div class="visual"><i class="fa fa-users"></i></div>
                                <div class="details">
                                    <div id="cpu_usage" class="number"></div>
                                    <div class="desc">CPU使用率 (最高 | 最低)</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="dashboard-stat grey-salsa">
                                <div class="visual"><i class="fa fa-users"></i></div>
                                <div class="details">
                                    <div id="mem_usage" class="number"></div>
                                    <div class="desc">記憶體使用率 (最高 | 最低)</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊 ?>
        
        <script src="<?= base_url("assets/global/scripts/metronic.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
        <?php if ($this->session->i18n == 'zh') : ?>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
        <?php endif; ?>
        <script type="text/javascript" src="<?= base_url("assets/plugins/jquery-ui/jquery-ui.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.js") ?>"></script>
        <script type='text/javascript' src='<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-sliderAccess.js") ?>'></script>
         <script nonce="cm1vaw==">
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init() // init quick sidebar

                $('.date-picker').datepicker({
                    dateFormat: 'yy-mm-dd'
                });

                $('body').on('click', '#btn_submit', function(){
                    let startDate = $('#search input[name=start_time]').val();
                    let endDate = $('#search input[name=end_time]').val();
                    console.log(startDate, endDate);
                    if(endDate === '' | startDate === ''){
                        alert('日期必填');
                    }
                    if(endDate < startDate){
                        alert('結束日期不可小於起始日期');
                    }
                    $.ajax({
                        type: 'GET',
                        url: '/sa/home/get_dashboard',
                        data:{
                            start_date: startDate,
                            end_date: endDate,
                        },
                        cache:false,
                        contentType: 'application/json',
                    }).done(function(response) {
                        console.log(response);
                        $('#use_count').html(response.USE_COUNT ?? 'N/A');
                        $('#online_count').html(response.ONLINE_COUNT ?? 'N/A');
                        $('#cpu_usage').html((response.CPU_USAGE_MAX ? ((Math.round(response.CPU_USAGE_MAX * 100) / 100) + '%') : 'N/A') + ' | ' + (response.CPU_USAGE_MIN ? ((Math.round(response.CPU_USAGE_MIN * 100) / 100) + '%') : 'N/A'));
                        $('#mem_usage').html((response.MEM_USAGE_MAX ? ((Math.round(response.MEM_USAGE_MAX * 100) / 100) + '%') : 'N/A') + ' | ' + (response.MEM_USAGE_MIN ? ((Math.round(response.MEM_USAGE_MIN * 100) / 100) + '%') : 'N/A'));
                    }).fail(function(error) {
                        console.log(error);
                    });
                })
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>