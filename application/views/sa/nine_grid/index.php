<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>

        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //上方標題 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="note note-danger">
                                <p> <?= $this->lang->line("說明: 您可以透過新增、編輯、刪除、搜索，來管理列表。") ?></p>
                            </div>
                            <div class="portlet">
                                <div class="portlet-title">
                                    <div class="actions">
                                        <!-- <a href="<?= base_url("{$this->controller_name}/{$table_name}/add?m_id=$mid") ?>" class="btn default purple-stripe"> <i class="fa fa-plus"></i> <span class="hidden-480"> <?= $this->lang->line("新增") ?></span> </a> -->
                                        <a href="<?= base_url("{$this->controller_name}/mission") ?>" class="btn default blue-stripe"> <i class="fa fa-arrow-left"></i> <span class="hidden-480"> 返回 </span> </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container">
                                        <table class="table table-striped table-bordered table-hover" id="datatable_ajax">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="3%"> NO.</th>
                                                    <th width="7%">  關卡名稱 </th>

                                                    <th width="7%"> 所屬局處 </th>

                                                    <th width="7%">  POI名稱 </th>
                                                    <th width="15%">  POI描述 </th>
                                                    <th width="7%">  闖關方式 </th>
                                                    <th width="7%"> 編輯 </th>
                                                </tr>

                                                <?php foreach ($nine_grid_list as $key => $value):?>
                                                      <tr>
                                                          <td><?= $key+1 ?></td>
                                                          <td><?= $value->ng_title?></td>

                                                          <td><?= $value->ag_name?></td>

                                                          <td><?= $value->ap_name?></td>
                                                          <td><?= $value->ap_desc?></td>
                                                          <td>
                                                              <?php foreach ($pass_method as $k_zh => $v_code){
                                                                  if($v_code===$value->ng_pass_method){
                                                                      echo $k_zh;
                                                                  }
                                                              }
                                                              ?>
                                                          </td>
                                                          <td>
                                                            <a href='<?= base_url('sa/nine_grid/edit?') ?>ng_id=<?= $value->ng_id?>&m_id=<?= $mid ?>' class='btn btn-xs default blue-stripe btn-operating'><i class='fa fa-pencil'></i> 編輯</a>
                                                          </td>
                                                      </tr>
                                                <?php endforeach;?>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊 ?>

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/select2/select2.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
        <?php if ($this->session->i18n == 'zh') : ?>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
        <?php endif; ?>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("assets/global/scripts/metronic_{$this->session->i18n}.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/global/scripts/datatable_{$this->session->i18n}.js") ?>"></script>
        <script src="<?= base_url("assets/admin/pages/scripts/table-ajax_{$this->session->i18n}.js") ?>"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
         <script nonce="cm1vaw==">
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Metronic.setAssetsPath('<?= base_url('assets') ?>/');
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
