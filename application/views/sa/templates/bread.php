<li>
    <i class="fa fa-home"></i>
    <a href="<?= base_url("/{$this->controller_name}/") ?>"><?= $this->lang->line("site_name") ?></a>
</li>
<?php if($bread): ?>
	<?php foreach ($bread as $id => $row) : ?>
	    <li>
	        <a href="<?= $row["url"] ?>"><?= $row["title"] ?></a>
	    </li>
	<?php endforeach; ?>
<?php endif;?>
