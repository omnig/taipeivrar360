
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <div id="systemHeader" class="title"><?= $this->lang->line("系統管理後台") ?></div>
            <div class="menu-toggler sidebar-toggler">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <!-- BEGIN LANGUAGE BAR -->
                <?php if(isset($this->session->userdata['expires_in'])):  ?>
                <li>
                    <div id="clockdiv">
                      <div>
                        <span class="minutes"></span>
                        <div class="smalltext">Minutes</div>
                      </div>
                      <div>
                        <span class="seconds"></span>
                        <div class="smalltext">Seconds</div>
                      </div>
                    </div>
                </li>
                <?php endif; ?>
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-language">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <?php if ($this->session->i18n == 'en') : ?>
                            <img alt="" src="<?= base_url("assets/global/img/flags/us.png") ?>">
                            <span class="langname"> English </span>
                        <?php endif; ?>
                            <?php if ($this->session->i18n == 'zh') : ?>
                            <img alt="" src="<?= base_url("assets/global/img/flags/tw.png") ?>">
                            <span class="langname"> 繁體中文 </span>
                        <?php endif; ?>
                        
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <!-- <li>
                            <a href="javascript:query_redir('','lang=en')">
                                <img alt="" src="<?= base_url("assets/global/img/flags/us.png") ?>"> English </a>
                        </li> -->
                        <li>
                            <a href="javascript:query_redir('','lang=zh')">
                                <img alt="" src="<?= base_url("assets/global/img/flags/tw.png") ?>"> 繁體中文 </a>
                        </li>
                    </ul>
                </li>
                <!-- END LANGUAGE BAR -->
                <!-- BEGIN USER LOGIN DROPDOWN -->
                <li class="dropdown dropdown-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <img alt="" class="img-circle" src="<?= base_url("assets/admin/layout/img/avatar.png") ?>"/>
                        <span class="username"><?= $this->login_sa->sa_name ?> </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?= base_url("{$this->controller_name}/profile") ?>">
                                <i class="icon-user"></i> <?= $this->lang->line("帳號管理") ?> </a>
                        </li>
                        <li class="divider">
                        </li>
                        <li>
                            <a href="<?= base_url("{$this->controller_name}/logout") ?>">
                                <i class="icon-key"></i> <?= $this->lang->line("登出") ?> </a>
                        </li>
                    </ul>
                </li>
                <!-- END USER LOGIN DROPDOWN -->
                <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                <li class="dropdown dropdown-quick-sidebar-toggler">
                    <a href="<?= base_url("{$this->controller_name}/logout") ?>" class="dropdown-toggle">
                        <i class="icon-logout"></i>
                    </a>
                </li>
                <!-- END QUICK SIDEBAR TOGGLER -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->

<?php if(isset($this->session->userdata['expires_in'])):  ?>
 <script type="text/javascript" nonce="cm1vaw==">
    function getTimeRemaining(endtime) {
      var t = Date.parse(endtime) - Date.parse(new Date());
      var seconds = Math.floor((t / 1000) % 60);
      var minutes = Math.floor((t / 1000 / 60) % 60);
      var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
      var days = Math.floor(t / (1000 * 60 * 60 * 24));
      return {
        'total': t,
        'days': days,
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
      };
    }

    function initializeClock(id, endtime) {
      var clock = document.getElementById(id);
      var minutesSpan = clock.querySelector('.minutes');
      var secondsSpan = clock.querySelector('.seconds');

      function updateClock() {
        var t = getTimeRemaining(endtime);

        minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

        if (t.total <= 0) {
          clearInterval(timeinterval);
          alert("超過登入限制時間");
          document.location.href = "<?= base_url('sa/logout') ?>";
        }
      }

      updateClock();
      var timeinterval = setInterval(updateClock, 1000);
    }

    var deadline = new Date(Date.parse(new Date()) + <?=$this->session->userdata['expires_in']?> * 1000);
    initializeClock('clockdiv', deadline);


</script>
<?php endif; ?>