<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>

        <link href="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url("assets/plugins/jquery-ui/jquery-ui.css") ?>" rel="stylesheet" type="text/css" />
        <link href='<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.css") ?>' rel='stylesheet'>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //上方標題列 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <form action="<?= base_url("{$this->controller_name}/{$table_name}/add_go") ?>" enctype="multipart/form-data" class="form-horizontal form-row-seperated" method="post">
                                <div class="portlet">
                                    <div class="portlet-title">
                                        <div class="caption">
                                        </div>
                                        <div class="actions btn-set">
                                            <a href="<?= base_url("{$this->controller_name}/{$table_name}") ?>" class="btn default"><i class="fa fa-angle-left"></i> <?= $this->lang->line("返回") ?></a>
                                            <button type="submit" class="btn green"><i class="fa fa-check"></i> <?= $this->lang->line("儲存") ?></button>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="tabbable">
                                            <ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a href="#tab_zh" data-toggle="tab">中文</a>
                                                </li>
                                                <li>
                                                    <a href="#tab_en" data-toggle="tab">英文</a>
                                                </li>
                                                <li>
                                                    <a href="#tab_jp" data-toggle="tab">日文</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content no-space">
                                                <div class="tab-pane active" id="tab_zh">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">中文名稱:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="<?= $field_prefix ?>_title_zh" class="form-control" placeholder="請輸入展區名稱" />
                                                                <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_title_zh">
                                                                    <button class="close" data-close="alert"></button>
                                                                    <span> <?= $this->lang->line("請輸入") ?>展區名稱 </span> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">中文內容:</label>
                                                            <div class="col-md-6">
                                                                <textarea class="form-control" name="<?= $field_prefix ?>_content_zh" rows="6" placeholder="請輸入展區內容"></textarea>
                                                                <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_content_zh">
                                                                    <button class="close" data-close="alert"></button>
                                                                    <span> <?= $this->lang->line("請輸入") ?>展區內容 </span> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">所屬樓層:
                                                            </label>
                                                            <div class="col-md-6">
                                                                <div class="form-control height-auto">
                                                                    <div class="scroller" style="height: 200px" data-always-visible="1">
                                                                        <ul class="list-unstyled">
                                                                            <?php foreach ($building as $b) : ?>
                                                                                <li>
                                                                                    <label>
                                                                                        <?= $b->ab_name ?>
                                                                                    </label>
                                                                                    <ul class="list-unstyled">
                                                                                        <?php foreach ($floors as $f): ?>
                                                                                            <?php if ($b->ab_id == $f->ab_id) : ?>
                                                                                                <li>
                                                                                                    <label>
                                                                                                        <input type="checkbox" class="category_chkbox" name="f_ids[]" value="<?= $f->af_id ?>"><?= $f->af_name ?>
                                                                                                    </label>
                                                                                                </li>
                                                                                            <?php endif; ?>
                                                                                        <?php endforeach; ?>
                                                                                    </ul>
                                                                                </li>
                                                                            <?php endforeach; ?>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <span class="help-block warning"> <?= $this->lang->line("請選取一個或樓層") ?> </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">選擇展廳:</label>
                                                            <div class="col-md-5">
                                                                <select class="form-control" name="eh_id">
                                                                    <?php foreach ($exh_hall as $row): ?>
                                                                        <option value="<?= $row->eh_id ?>"><?= $row->eh_title_zh ?></option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                                <div class="alert alert-danger display-hide validation" for="eh_id">
                                                                    <button class="close" data-close="alert"></button>
                                                                    <span> 請選擇展廳 </span> 
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <a href="<?= base_url("{$this->controller_name}/exh_hall/add") ?>" class="form-control btn btn-default" title="前往新增展廳" ><i class='fa fa-arrow-circle-right'></i> 新增</a>
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <label class="control-label col-md-3"><?= $this->lang->line("圖片") ?>:</label>
                                                            <div class="col-md-4">
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=<?= $this->lang->line("沒有圖片") ?>" alt=""/>
                                                                    </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> <?= $this->lang->line("選取圖片") ?> </span>
                                                                            <span class="fileinput-exists"> <?= $this->lang->line("更換") ?> </span>
                                                                            <input type="file" name="<?= $field_prefix ?>_image" accept="image/*">
                                                                        </span>
                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?= $this->lang->line("移除") ?> </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><?= $this->lang->line("狀態") ?>:</label>
                                                            <div class="col-md-6">
                                                                <select class="form-control" name="<?= $field_prefix ?>_enabled">
                                                                    <option value="Y"><?= $this->lang->line("啟用中") ?></option>
                                                                    <option value="N"><?= $this->lang->line("已關閉") ?></option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab_en">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">英文名稱:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="<?= $field_prefix ?>_title_en" class="form-control" placeholder="請輸入展區名稱" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">英文內容:</label>
                                                            <div class="col-md-6">
                                                                <textarea class="form-control" name="<?= $field_prefix ?>_content_en" rows="6" placeholder="請輸入展區內容"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab_jp">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">日文名稱:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="<?= $field_prefix ?>_title_jp" class="form-control" placeholder="請輸入展區名稱" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">日文內容:</label>
                                                            <div class="col-md-6">
                                                                <textarea class="form-control" name="<?= $field_prefix ?>_content_jp" rows="6" placeholder="請輸入展區內容"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊 ?>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js") ?>"></script>
<script type="text/javascript" src="<?= base_url("assets/global/plugins/select2/select2.min.js") ?>"></script>
<script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
<script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>
<script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
<?php if ($this->session->i18n == 'zh') : ?>
    <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
<?php endif; ?>
<script type="text/javascript" src="<?= base_url("assets/plugins/jquery-ui/jquery-ui.js") ?>"></script>
<script type="text/javascript" src="<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.js") ?>"></script>
<script type='text/javascript' src='<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-sliderAccess.js") ?>'></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?= base_url("assets/global/scripts/metronic.js") ?>" type="text/javascript"></script>
<script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
<script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

 <script type="text/javascript" nonce="cm1vaw==">
    $(function () {

        $('#btn_submit').on('click', function () {
            if (on_submit()) {
                $('form').submit();
            }
        });

        $('form').submit(on_submit);

        function on_submit() {
            var result = true;

            $('.validation').each(function () {
                var name = $(this).attr('for');

                if (!$('*[name=' + name + ']').val()) {
                    $(this).show();
                    $('*[name=' + name + ']').focus();
                    event.preventDefault();
                    result = false;
                } else {
                    $(this).hide();
                }
            });

            return result;
        }

    });
</script>
 <script nonce="cm1vaw==">
    jQuery(document).ready(function () {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        QuickSidebar.init(); // init quick sidebar
    });


    function checkall(selector) {
        $(selector).prop('checked', true);
        $(selector).parent().addClass('checked');
    }
    function uncheck(selector) {
        $(selector).prop('checked', false);
        $(selector).parent().removeClass('checked');
    }

    $(document).on('click', '[name=remove]', function () {
        $(this).parent().remove();
    });
    $(document).on('click', '[name=newremove]', function () {
        var count = $('[name=beacon]').length;
        if (count > 1) {
            $(this).parent().remove();
        }
    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
