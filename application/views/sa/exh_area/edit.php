<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>

        <link href="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url("assets/plugins/jquery-ui/jquery-ui.css") ?>" rel="stylesheet" type="text/css" />
        <link href='<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.css") ?>' rel='stylesheet'>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //上方標題列 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <form action="<?= base_url("{$this->controller_name}/{$table_name}/edit_go") ?>" enctype="multipart/form-data" class="form-horizontal form-row-seperated" method="post">
                                <div class="portlet">
                                    <div class="portlet-title">
                                        <div class="caption">
                                        </div>
                                        <div class="actions btn-set">
                                            <input type="hidden" name="<?= "{$field_prefix}_id" ?>" value="<?= ${$table_name}->{"{$field_prefix}_id"} ?>" />
                                            <a href="<?= base_url("{$this->controller_name}/{$table_name}") ?>" class="btn default"><i class="fa fa-angle-left"></i> <?= $this->lang->line("返回") ?></a>
                                            <button type="submit" class="btn green"><i class="fa fa-check"></i> <?= $this->lang->line("儲存") ?></button>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="tabbable">
                                            <ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a href="#tab_zh" data-toggle="tab">中文</a>
                                                </li>
                                                <li>
                                                    <a href="#tab_en" data-toggle="tab">英文</a>
                                                </li>
                                                <li>
                                                    <a href="#tab_jp" data-toggle="tab">日文</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content no-space">
                                                <div class="tab-pane active" id="tab_zh">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">中文名稱:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="<?= $field_prefix ?>_title_zh" class="form-control" placeholder="請輸入展區名稱" value="<?= ${$table_name}->{"{$field_prefix}_title_zh"} ?>" />
                                                                <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_title_zh">
                                                                    <button class="close" data-close="alert"></button>
                                                                    <span> <?= $this->lang->line("請輸入") ?>展區名稱 </span> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">中文內容:</label>
                                                            <div class="col-md-6">
                                                                <textarea class="form-control" name="<?= $field_prefix ?>_content_zh" rows="6" placeholder="請輸入展區內容"><?= ${$table_name}->{"{$field_prefix}_content_zh"} ?></textarea>
                                                                <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_content_zh">
                                                                    <button class="close" data-close="alert"></button>
                                                                    <span> <?= $this->lang->line("請輸入") ?>展區內容 </span> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">所屬樓層:
                                                            </label>
                                                            <div class="col-md-6">
                                                                <div class="form-control height-auto">
                                                                    <div class="scroller" style="height: 200px" data-always-visible="1">
                                                                        <ul class="list-unstyled">
                                                                            <?php foreach ($building as $b) : ?>
                                                                                <li>
                                                                                    <label>
                                                                                        <?= $b->ab_name ?>
                                                                                    </label>
                                                                                    <ul class="list-unstyled">
                                                                                        <?php foreach ($floors as $f): ?>
                                                                                            <?php if ($b->ab_id == $f->ab_id) : ?>
                                                                                                <?php
                                                                                                $checked = '';
                                                                                                foreach ($relation as $ar) {
                                                                                                    if ($ar->af_id == $f->af_id) {
                                                                                                        $checked = 'checked';
                                                                                                    }
                                                                                                }
                                                                                                ?>
                                                                                                <li>
                                                                                                    <label>
                                                                                                        <input type="checkbox" class="category_chkbox" name="f_ids[]" value="<?= $f->af_id ?>" <?= $checked ?>><?= $f->af_name ?>
                                                                                                    </label>
                                                                                                </li>
                                                                                            <?php endif; ?>
                                                                                        <?php endforeach; ?>
                                                                                    </ul>
                                                                                </li>
                                                                            <?php endforeach; ?>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <span class="help-block warning"> <?= $this->lang->line("請選取一個或樓層") ?> </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">選擇展廳:</label>
                                                            <div class="col-md-5">
                                                                <select class="form-control" name="eh_id">
                                                                    <option value="">請選擇展廳</option>
                                                                    <?php foreach ($exh_hall as $row): ?>
                                                                        <option value="<?= $row->eh_id ?>" <?= (${$table_name}->eh_id == $row->eh_id) ? 'selected' : '' ?>><?= $row->eh_title_zh ?></option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                                <div class="alert alert-danger display-hide validation" for="eh_id">
                                                                    <button class="close" data-close="alert"></button>
                                                                    <span> 請選擇展廳 </span> 
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <a href="<?= base_url("{$this->controller_name}/exh_hall/add") ?>" class="form-control btn btn-default" title="前往新增展廳" ><i class='fa fa-arrow-circle-right'></i> 新增</a>
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <label class="control-label col-md-3"><?= $this->lang->line("圖片") ?>:</label>
                                                            <div class="col-md-6">
                                                                <?php
                                                                if (!${$table_name}->{"{$field_prefix}_image"}) {
                                                                    $img = "<img src='http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no%20picture' alt='' />";
                                                                    $fileinput_status = 'fileinput-new';
                                                                } else {
                                                                    $img = "<img src='" . base_url("upload/exh_area") . "/" . ${$table_name}->{"{$field_prefix}_image"} . "' alt='' />";
                                                                    $fileinput_status = 'fileinput-exists';
                                                                }
                                                                ?>
                                                                <div class="fileinput <?= $fileinput_status ?>" data-provides="fileinput">
                                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                                                        <?= $img ?>
                                                                    </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> <?= $this->lang->line("選取圖片") ?> </span>
                                                                            <span class="fileinput-exists"> <?= $this->lang->line("更換") ?> </span>
                                                                            <input type="file" name="<?= $field_prefix ?>_image" accept="image/*">
                                                                        </span>
                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?= $this->lang->line("移除") ?> </a>
                                                                    </div>
                                                                </div>
                                                                <div>
                                                                    <label class="label font-blue"><i class='fa fa-info-circle'></i> 建議上傳橫幅相片，長寬為 4 : 3</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><?= $this->lang->line("狀態") ?>:</label>
                                                            <div class="col-md-6">
                                                                <select class="form-control" name="<?= $field_prefix ?>_enabled">
                                                                    <option value="Y" <?= (${$table_name}->eh_id == 'Y') ? 'selected' : '' ?>><?= $this->lang->line("啟用中") ?></option>
                                                                    <option value="N" <?= (${$table_name}->eh_id == 'N') ? 'selected' : '' ?>><?= $this->lang->line("已關閉") ?></option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab_en">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">英文名稱:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="<?= $field_prefix ?>_title_en" class="form-control" placeholder="請輸入展區名稱" value="<?= ${$table_name}->{"{$field_prefix}_title_en"} ?>" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">英文內容:</label>
                                                            <div class="col-md-6">
                                                                <textarea class="form-control" name="<?= $field_prefix ?>_content_en" rows="6" placeholder="請輸入展區內容"><?= ${$table_name}->{"{$field_prefix}_content_en"} ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab_jp">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">日文名稱:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="<?= $field_prefix ?>_title_jp" class="form-control" placeholder="請輸入展區名稱" value="<?= ${$table_name}->{"{$field_prefix}_title_jp"} ?>" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">日文內容:</label>
                                                            <div class="col-md-6">
                                                                <textarea class="form-control" name="<?= $field_prefix ?>_content_jp" rows="6" placeholder="請輸入展區內容"><?= ${$table_name}->{"{$field_prefix}_content_jp"} ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="portlet grey-cascade box">
                                                            <div class="portlet-title">
                                                                <div class="caption"><i class="fa fa-cog"></i>區域</div>
                                                            </div>
                                                            <div class="portlet-body">
                                                                <div class="table-responsive">
                                                                    <div class='padding-10'>
                                                                        <a data-toggle="modal" class='btn blue' href="#ep_new"><i class='fa fa-plus'></i> 新增</a>
                                                                        <div class="modal fade" id="ep_new" tabindex="-1" style="display: none;">
                                                                            <div class="modal-dialog">
                                                                                <div class="modal-content">
                                                                                    <div class="modal-header">
                                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                        <h4 class="modal-title">區域標題 / 新增</h4>
                                                                                    </div>
                                                                                    <div class="modal-body">
                                                                                        <div class='form-group'>
                                                                                            <label class='control-label col-md-3'>中文名稱：</label>
                                                                                            <div class='col-md-9'>
                                                                                                <input type="text" name='ep_title_zh' class="form-control" placeholder="請輸入區域中文" value="">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class='form-group'>
                                                                                            <label class='control-label col-md-3'>英文名稱：</label>
                                                                                            <div class='col-md-9'>
                                                                                                <input type="text" name='ep_title_en' class="form-control" placeholder="請輸入區域英文" value="">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class='form-group'>
                                                                                            <label class='control-label col-md-3'>日文名稱：</label>
                                                                                            <div class='col-md-9'>
                                                                                                <input type="text" name='ep_title_jp' class="form-control" placeholder="請輸入區域日文" value="">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class='form-group'>
                                                                                            <label class='control-label col-md-3'>狀態：</label>
                                                                                            <div class='col-md-9'>
                                                                                                <select id='ep_enabled' class='form-control'>
                                                                                                    <option value='Y' selected>啟用中</option>
                                                                                                    <option value='N'>已關閉</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <a href='javascript:add_exh_point()' class="btn blue">新增</a>
                                                                                        <button type="button" class="btn default" data-dismiss="modal">取消</button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <?php if (count($exh_point) > 0): ?>
                                                                        <table id="exh_point" class="table table-hover table-bordered table-striped">
                                                                            <thead>
                                                                                <tr>
                                                                                    <td width='25%'>中文名稱</td>
                                                                                    <td width='25%'>英文名稱</td>
                                                                                    <td width='25%'>日文名稱</td>
                                                                                    <td>狀態</td>
                                                                                    <td>編輯</td>
                                                                                    <td>刪除</td>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php foreach ($exh_point as $row): ?>
                                                                                    <tr>
                                                                                        <td><?= $row->ep_title_zh ?></td>
                                                                                        <td><?= $row->ep_title_en ?></td>
                                                                                        <td><?= $row->ep_title_jp ?></td>
                                                                                        <td><?= ($row->ep_enabled == 'Y') ? '<label class="label label-success label-sm">啟用中</label>' : '<label class="label label-danger label-sm">已關閉</label>' ?></td>
                                                                                        <td>
                                                                                            <a data-toggle="modal" class="" data-ep_id="<?= $row->ep_id ?>" href="#ep<?= $row->ep_id ?>">編輯</a>
                                                                                            <div class="modal fade" id="ep<?= $row->ep_id ?>" tabindex="-1" style="display: none;">
                                                                                                <div class="modal-dialog">
                                                                                                    <div class="modal-content">
                                                                                                        <form action="<?= base_url("{$this->controller_name}/{$table_name}/edit_point_go") ?>" enctype="multipart/form-data" class="form-horizontal form-row-seperated" method="post">
                                                                                                            <div class="modal-header">
                                                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                                                <h4 class="modal-title">區域標題 / 編輯</h4>
                                                                                                            </div>
                                                                                                            <div class="modal-body">
                                                                                                                <div class='form-group'>
                                                                                                                    <label class='control-label col-md-3'>中文名稱：</label>
                                                                                                                    <div class='col-md-9'>
                                                                                                                        <input type="text" name='ep_title_zh_<?= $row->ep_id ?>' class="form-control" placeholder="請輸入區域中文" value="<?= $row->ep_title_zh ?>">
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class='form-group'>
                                                                                                                    <label class='control-label col-md-3'>英文名稱：</label>
                                                                                                                    <div class='col-md-9'>
                                                                                                                        <input type="text" name='ep_title_en_<?= $row->ep_id ?>' class="form-control" placeholder="請輸入區域英文" value="<?= $row->ep_title_en ?>">
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class='form-group'>
                                                                                                                    <label class='control-label col-md-3'>日文名稱：</label>
                                                                                                                    <div class='col-md-9'>
                                                                                                                        <input type="text" name='ep_title_jp_<?= $row->ep_id ?>' class="form-control" placeholder="請輸入區域日文" value="<?= $row->ep_title_jp ?>">
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class='form-group'>
                                                                                                                    <label class='control-label col-md-3'>狀態：</label>
                                                                                                                    <div class='col-md-9'>
                                                                                                                        <select id='ep_enabled_<?= $row->ep_id ?>' class='form-control'>
                                                                                                                            <option value='Y' <?= $row->ep_enabled == 'Y' ? 'selected' : '' ?>>啟用中</option>
                                                                                                                            <option value='N' <?= $row->ep_enabled == 'N' ? 'selected' : '' ?>>已關閉</option>
                                                                                                                        </select>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="modal-footer">
                                                                                                                <a href='javascript:save_exh_point(<?= $row->ep_id ?>)' class="btn blue">儲存</a>
                                                                                                                <button type="button" class="btn default" data-dismiss="modal">取消</button>
                                                                                                            </div>
                                                                                                        </form>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td><a class="delete del_point" href="javascript:if(confirm('確定刪除？')) location.href='<?= base_url("sa/exh_area/del_point") . '?ea_id=' . $row->ea_id . '&ep_id=' . $row->ep_id ?>'">刪除</a></td>
                                                                                    </tr>
                                                                                <?php endforeach; ?>
                                                                            </tbody>
                                                                        </table>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊     ?>

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/select2/select2.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
        <?php if ($this->session->i18n == 'zh') : ?>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
        <?php endif; ?>
        <!-- END PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?= base_url("assets/plugins/jquery-ui/jquery-ui.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.js") ?>"></script>
        <script type='text/javascript' src='<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-sliderAccess.js") ?>'></script>
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("assets/global/scripts/metronic.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->

         <script type="text/javascript" nonce="cm1vaw==">
            $(function () {

                $('#btn_submit').on('click', function () {
                    if (on_submit()) {
                        $('form').submit();
                    }
                });

                $('form').submit(on_submit);

                function on_submit() {
                    var result = true;

                    $('.validation').each(function () {
                        var name = $(this).attr('for');

                        if (!$('*[name=' + name + ']').val()) {
                            $(this).show();
                            $('*[name=' + name + ']').focus();
                            event.preventDefault();
                            result = false;
                        } else {
                            $(this).hide();
                        }
                    });

                    return result;
                }
                $('.date-picker').datetimepicker({
                    dateFormat: 'yy-mm-dd',
                    timeFormat: 'HH:mm'
                });
            });
        </script>
         <script nonce="cm1vaw==">
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
            });

            function checkall(selector) {
                $(selector).prop('checked', true);
                $(selector).parent().addClass('checked');
            }
            function uncheck(selector) {
                $(selector).prop('checked', false);
                $(selector).parent().removeClass('checked');
            }
            $(document).on('click', '[name=remove]', function () {
                $(this).parent().remove();
            });

            function save_exh_point(ep_id) {
                var title_zh = $("input[name='ep_title_zh_" + ep_id + "']").val();
                var title_en = $("input[name='ep_title_en_" + ep_id + "']").val();
                var title_jp = $("input[name='ep_title_jp_" + ep_id + "']").val();
                var enabled = $("#ep_enabled_" + ep_id + " option:selected").val();
                var url = "<?= base_url('sa/exh_area/edit_point_go') . '?ea_id=' . $this->input->get('ea_id') . '&ep_id=' ?>" + ep_id;
                document.location.href = url + '&zh=' + title_zh + '&en=' + title_en + '&jp=' + title_jp + '&enabled=' + enabled;
            }

            function add_exh_point() {
                var title_zh = $("input[name='ep_title_zh']").val();
                var title_en = $("input[name='ep_title_en']").val();
                var title_jp = $("input[name='ep_title_jp']").val();
                var enabled = $("#ep_enabled option:selected").val();
                var url = "<?= base_url('sa/exh_area/add_point_go') . '?ea_id=' . $this->input->get('ea_id') ?>";
                document.location.href = url + '&zh=' + title_zh + '&en=' + title_en + '&jp=' + title_jp + '&enabled=' + enabled;
            }
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
