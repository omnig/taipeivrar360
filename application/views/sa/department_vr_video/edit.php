<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>

        <link href="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>

        <style type="text/css">
            .loading_bar{
                color: #669999;
                position: fixed;
                right: 25%;
                text-align: center;
                display: none;
                top:50%;
                z-index: 2;
                width: 675px;
                border: solid;
                background: #fff;

            }
            .loading_bar_text{
                float: left;
                font-size: 45px;
            }
            .loading_bar_img{
                float: left;
                margin-top: -48%;
                margin-left: 21%;
            }
        </style>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //上方標題列 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->

                            <?php include APPPATH . "views/{$this->controller_name}/templates/loading_bar.php"  //載入進度條 ?>
                            <div id="response_div"></div>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <form id="ajax-form" action="<?= base_url("{$this->controller_name}/{$table_name}/edit_go") ?>" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data">
                                <div class="portlet">
                                    <div class="portlet-title">
                                        <div class="caption">
                                        </div>
                                        <div class="actions btn-set">
                                            <a href="<?= base_url("{$this->controller_name}/{$table_name}")  ?>" class="btn default"><i class="fa fa-angle-left"></i> <?= $this->lang->line("返回") ?></a>
                                            <button  id="btn_submit" type="submit" class="btn green"><i class="fa fa-check"></i> <?= $this->lang->line("儲存") ?></button>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="tabbable">
                                            <ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a href="#tab_general" data-toggle="tab">
                                                        環景影片基本資料
                                                    </a>
                                                </li>

                                                <li>
                                                    <a href="#tab_keywords" data-toggle="tab">
                                                        關鍵字標籤
                                                    </a>
                                                </li>
                                            </ul>
                                            <div class="tab-content no-space">
                                                <div class="tab-pane active" id="tab_general">
                                                    <div class="form-body">
                                                        <input type="hidden" name="<?= $field_prefix ?>_id" value="<?= ${$table_name}->{"{$field_prefix}_id"} ?>">
                                                        <!-- 主題選擇 -->
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">主題選擇:<span class="required"> * </span></label>
                                                            <div class="col-md-6">
                                                                <select class="form-control" name="t_id" id="t_id">
                                                                    <option>請選擇</option>
                                                                    <?php foreach ($topic_list as $row): ?>
                                                                        <option value="<?= $row->t_id ?>" <?= (${$table_name}->{"t_id"} == $row->t_id) ? ' selected' : '' ?>> <?= $row->t_name ?> </option>
                                                                    <?php endforeach ?>                                                                
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <!-- 上傳局處 -->
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">上傳局處:<span class="required"> * </span></label>
                                                            <div class="col-md-6">
                                                                <select class="form-control" name="d_id">
                                                                    <option>請選擇</option>
                                                                    <?php foreach ($department_list as $row): ?>
                                                                        <option value="<?= $row->ag_id ?>" <?= (${$table_name}->{"d_id"} == $row->ag_id) ? ' selected' : '' ?>> <?= $row->ag_name ?> </option>
                                                                    <?php endforeach ?>                                                                
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <!-- VR360環景圖名稱 -->
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">VR360環景影片名稱:<span class="required"> * </span></label>
                                                            <div class="col-md-4">
                                                                <input type="text" name="<?= $field_prefix ?>_name" class="form-control" value="<?= ${$table_name}->{"{$field_prefix}_name"} ?>" />
                                                            </div>
                                                        </div>

                                                        <!-- VR360環景圖代表圖片 -->
                                                        <div class="form-group ">
                                                            <label class="control-label col-md-3">VR360環景影片代表圖片:<span class="required"> * </span></label>
                                                            <div class="col-md-9">
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                                                        <img src="<?= base_url("/").${$table_name}->{"{$field_prefix}_rep_img_path"} ?>" alt=""/>
                                                                    </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> <?= $this->lang->line("選取圖片") ?> </span>
                                                                            <span class="fileinput-exists"> <?= $this->lang->line("更換") ?> </span>
                                                                            <input type="file" name="<?= $field_prefix ?>_rep_img_path" accept="image/*">
                                                                        </span>
                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?= $this->lang->line("移除") ?> </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        

                                                        <!-- VR360環景圖描述 -->
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">VR360環景影片描述:</label>
                                                            <div class="col-md-6">
                                                                <textarea name="<?= $field_prefix ?>_description" rows="5" class="form-control"  /><?= ${$table_name}->{"{$field_prefix}_description"} ?></textarea>
                                                            </div>
                                                            (限定 100 字)
                                                        </div>
                                                        
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">坐標:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control lat" name="<?= $field_prefix ?>_lat" value="<?= ${$table_name}->{"{$field_prefix}_lat"} ?>" placeholder="請輸入經度" >
                                                                <input type="text" class="form-control lng" name="<?= $field_prefix ?>_lng" value="<?= ${$table_name}->{"{$field_prefix}_lng"} ?>" placeholder="請輸入緯度" >
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"></label>
                                                            <div class="col-md-6">
                                                                <label class="control-label newlat" hidden></label>
                                                                <label class="control-label newlng" hidden></label>
                                                                <div id="map" style="width:100%;height:400px"></div>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">VR360環景<?= $this->lang->line("狀態") ?>:</label>
                                                            <div class="col-md-4">
                                                                <select class="form-control" name="<?= $field_prefix ?>_state">
                                                                    <option value="Y"<?= (${$table_name}->{"{$field_prefix}_state"} == 'Y') ? ' selected' : '' ?>><?= $this->lang->line("啟用中") ?></option>
                                                                    <option value="N"<?= (${$table_name}->{"{$field_prefix}_state"} == 'N') ? ' selected' : '' ?>><?= $this->lang->line("已關閉") ?></option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-pane" id="tab_keywords">
                                                    <div class="form-body">
                                                        <?php for($i=1;$i<=5;$i++): ?>
                                                            <?php   if (isset($keywords[$i-1])) {
                                                                        $keyword = $keywords[$i-1];
                                                                    }else{
                                                                        $keyword = "";
                                                                    }
                                                            ?>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">標籤 <?=$i?>:</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="<?= $field_prefix ?>_tag[]" class="form-control" value="<?=$keyword ?>"/>
                                                                </div>
                                                            </div>
                                                        <?php endfor;?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊 ?>

        <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/select2/select2.min.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-summernote/summernote.min.js") ?>"></script> 
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-summernote/lang/summernote-zh-TW.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
            <?php if ($this->session->i18n == 'zh') : ?>
                <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
            <?php endif; ?>
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN PAGE LEVEL SCRIPTS -->
            <script src="<?= base_url("assets/global/scripts/metronic.js") ?>" type="text/javascript"></script>
            <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
            <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>
            <script src="<?= base_url("assets/global/scripts/datatable_{$this->session->i18n}.js") ?>"></script>
            <script src="<?= base_url("assets/admin/pages/scripts/table-ajax_{$this->session->i18n}.js") ?>"></script>
            <!-- END PAGE LEVEL SCRIPTS -->
            <script src="<?= base_url("js/geofence.js") ?>" type="text/javascript"></script>
            <script src="<?= base_url("js/google_map.js") ?>" type="text/javascript"></script>
            <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= $this->config->item("google")["api_key"] ?>&callback=initMap"></script>
             <script type="text/javascript" nonce="cm1vaw==">
                jQuery(document).ready(function () {
                    Metronic.init(); // init metronic core components
                    Layout.init(); // init current layout
                    QuickSidebar.init(); // init quick sidebar

                    $('#t_id').change(function(){
                        var topic = $('#t_id').val();
                        $.getJSON("<?=base_url('/api/get_topic?topic_id=')?>"+topic, function(){     
                        }).success(function(result){
                            // 得到主題資訊
                            // 經度 
                            lat = result.data[0].t_lat;
                            // 緯度 
                            lng = result.data[0].t_lng

                            $( "#<?= $field_prefix ?>_lat" ).val(lat);
                            $( "#<?= $field_prefix ?>_lng" ).val(lng);
                            
                            area_select_map.set_center(lat,lng);


                        }).error(function(result){
                            lat = <?= $this->config->item('default_lat') ?>;
                            lng = <?= $this->config->item('default_lng') ?>;

                            area_select_map.set_center(lat,lng);
                        });
                    });

                    $('#btn_submit').on('click', function () {

                        event.preventDefault();
                            
                            // 
                            // $('form').submit();
                        var form = $('#ajax-form');

                            // Stop the browser from submitting the form.
                            document.getElementById("loading_bar").style.display = "block";

                            // Serialize the form data.
                            // var formData = $(form).serialize();
                            var formData = new FormData($('#ajax-form')[0]);

                            // Submit the form using AJAX.
                            $.ajax({
                                type: 'POST',
                                url: $(form).attr('action'),
                                data:formData,
                                cache:false,
                                contentType: false,
                                processData: false
                            }).done(function(response) {
                                document.getElementById("loading_bar").style.display = "none";

                                if (response.indexOf("script")!=-1) {
                                    $("#response_div").html(response);
                                }else{
                                    alert(response); 
                                }
                                // alert(response);
                                // console.log(response);
                            }).fail(function(data) {
                                console.log(data);
                            });
                        
                    });
                    
                });
                area_select_map.init('<?= base_url() ?>');
                setTimeout(function () {
                }, 500);

            </script>
            
            <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
