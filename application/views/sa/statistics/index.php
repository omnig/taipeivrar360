<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>

        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //上方標題 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="dashboard-stat red">
                                <div class="visual"><i class="fa fa-user"></i></div>
                                <div class="details">
                                    <div class="number"><?= number_format($ga_result) ?></div>
                                    <div class="desc">VR網站瀏覽人數</div>
                                </div>
                                <a class="more" href="#"> -</a>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="dashboard-stat blue-madison">
                                <div class="visual"><i class="fa fa-book"></i></div>
                                <div class="details">
                                    <div class="number"><?= number_format($document_num) ?></div>
                                    <div class="desc">文章上傳總數</div>
                                </div>
                                <a class="more" href="<?= base_url('sa/document') ?>">前往管理</a>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="dashboard-stat green-meadow">
                                <div class="visual"><i class="fa fa-video-camera"></i></div>
                                <div class="details">
                                    <div class="number"><?= number_format($vrvideo_upload_num) ?></div>
                                    <div class="desc">VR影片上傳總數</div>
                                </div>
                                <a class="more" href="<?= base_url('sa/civil_vr_video') ?>">前往管理</a>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="dashboard-stat green-seagreen">
                                <div class="visual"><i class="fa fa-share"></i></div>
                                <div class="details">
                                    <div class="number"><?= number_format($document_share_num) ?></div>
                                    <div class="desc">文章分享總數</div>
                                </div>
                                <a class="more" href="<?= base_url('sa/document') ?>">前往管理</a>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="dashboard-stat blue-hoki">
                                <div class="visual"><i class="fa fa-image"></i></div>
                                <div class="details">
                                    <div class="number"><?= number_format($vrphoto_upload_num) ?></div>
                                    <div class="desc">VR圖片上傳總數</div>
                                </div>
                                <a class="more" href="<?= base_url('sa/civil_vr_photo') ?>">前往管理</a>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="dashboard-stat red-flamingo">
                                <div class="visual"><i class="fa fa-eye"></i></div>
                                <div class="details">
                                    <div class="number"><?= number_format($document_read_num) ?></div>
                                    <div class="desc">文章閱讀總數</div>
                                </div>
                                <a class="more" href="<?= base_url('sa/document') ?>">前往管理</a>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="dashboard-stat grey-gallery">
                                <div class="visual"><i class="fa fa-hand-o-right"></i></div>
                                <div class="details">
                                    <div class="number"><?= number_format($vr_share_num) ?></div>
                                    <div class="desc">影片圖片點擊分享總數</div>
                                </div>
                                <a class="more" href="<?= base_url('sa/civil_vr_photo') ?>">前往管理</a>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="dashboard-stat green-haze">
                                <div class="visual"><i class="fa fa-comments"></i></div>
                                <div class="details">
                                    <div class="number"><?= number_format($comments_num) ?></div>
                                    <div class="desc">留言總數</div>
                                </div>
                                <a class="more" href="<?= base_url('sa/comment') ?>">前往管理</a>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="dashboard-stat blue-sharp">
                                <div class="visual"><i class="fa fa-female"></i></div>
                                <div class="details">
                                    <div class="number"><?= number_format($member_num) ?></div>
                                    <div class="desc">會員人數</div>
                                </div>
                                <a class="more" href="<?= base_url('sa/user') ?>">前往管理</a>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="dashboard-stat yellow-gold">
                                <div class="visual"><i class="fa fa-clipboard"></i></div>
                                <div class="details">
                                    <div class="number"><?= number_format($proprietary_num) ?></div>
                                    <div class="desc">專有名詞總數</div>
                                </div>
                                <a class="more" href="<?= base_url('sa/proprietary') ?>">前往管理</a>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊 ?>

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/select2/select2.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
        <?php if ($this->session->i18n == 'zh') : ?>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
        <?php endif; ?>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("assets/global/scripts/metronic_{$this->session->i18n}.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/global/scripts/datatable_{$this->session->i18n}.js") ?>"></script>
        <script src="<?= base_url("assets/admin/pages/scripts/table-ajax_{$this->session->i18n}.js") ?>"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
         <script nonce="cm1vaw==">
            
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
