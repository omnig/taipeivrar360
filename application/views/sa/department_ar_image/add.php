<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>

        <link href="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url("assets/base/css/custom.css") ?>" rel="stylesheet"></link>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //上方標題列 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->

                            <?php include APPPATH . "views/{$this->controller_name}/templates/loading_bar.php"  //載入進度條 ?>
                            <div id="response_div"></div>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <form id="ajax-form" action="<?= base_url("{$this->controller_name}/{$table_name}/add_go") ?>" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data">
                                <div class="portlet">
                                    <div class="portlet-title">
                                        <div class="caption">
                                        </div>
                                        <div class="actions btn-set">
                                            <a href="<?= base_url("{$this->controller_name}/{$table_name}")  ?>" class="btn default"><i class="fa fa-angle-left"></i> <?= $this->lang->line("返回") ?></a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="tabbable">
                                            <div class="tab-content no-space">
                                                <div class="tab-pane active" id="upload_ar">
                                                    <div class="form-body">
                                                        
                                                        <!-- 所屬局處 -->
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">所屬局處:<span class="required"> * </span></label>
                                                            <div class="col-md-6">
                                                                <select class="form-control" name="d_id">
                                                                    <option>請選擇</option>
                                                                    <?php foreach ($department_list as $row): ?>
                                                                        <option value="<?= $row->ag_id ?>"> <?= $row->ag_name ?> </option>
                                                                    <?php endforeach ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <!-- 辨識圖名稱 -->
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">辨識圖名稱:<span class="required"> * </span></label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="<?= $field_prefix ?>_name" class="form-control"/>
                                                                <div class="alert alert-danger display-hide validation validation-r" for="<?= $field_prefix ?>_name">
                                                                    <button class="close" data-close="alert"></button>
                                                                    <span> <?= $this->lang->line("請輸入") ?>辨識圖名稱 </span> 
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <!-- AR掃瞄圖片 -->
                                                        <div class="form-group ">
                                                            <label class="control-label col-md-3">AR掃瞄圖片:<span class="required"> * </span></label>
                                                            <div class="col-md-6">
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                                                        <img id="preview_image_path" src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=<?= $this->lang->line("沒有圖片") ?>" alt=""/>
                                                                    </div>
                                                                    <div>
                                                                        <label class="label font-blue"><i class='fa fa-info-circle'></i> 建議上傳高解析度圖片與JPEG檔案</label>
                                                                        <br>
                                                                        <label class="label font-blue"><i class='fa fa-info-circle'></i> 此圖片用途是讓民眾掃描辨識用</label>
                                                                    </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> <?= $this->lang->line("選取圖片") ?> </span>
                                                                            <span class="fileinput-exists"> <?= $this->lang->line("更換") ?> </span>
                                                                            <input type="file" name="<?= $field_prefix ?>_image_path" accept="image/jpeg">
                                                                        </span>
                                                                        <span class="btn default" id="create_qrcode"> 產生 QR Code </span>
                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?= $this->lang->line("移除") ?> </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="qrcode" class="hide"></div>
                                                        <img id="qrcode_img" class="hide"/>

                                                        <!-- 描述 -->
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">描述:</label>
                                                            <div class="col-md-6">
                                                                <textarea name="<?= $field_prefix ?>_description" rows="5" class="form-control"/></textarea>
                                                                <div class="alert alert-danger display-hide validation validation-r" for="<?= $field_prefix ?>_description">
                                                                    <button class="close" data-close="alert"></button>
                                                                    <span> <?= $this->lang->line("請輸入") ?>描述 </span> 
                                                                </div>
                                                            </div>
                                                            (限定 20 字)
                                                        </div>
                                                    </div>
                                                    <div class="text-center"><h2>預設 AR 互動資料</h2></div>
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">綁定AR:</label>
                                                            <div class="col-md-6">
                                                                <select class="form-control" name="default_tdar_id">
                                                                    <option value="">請選擇</option>
                                                                    <?php foreach ($ar_list as $row): ?>
                                                                        <option value="<?= $row->tdar_id ?>"> <?= $row->tdar_name ?> </option>
                                                                    <?php endforeach ?> 
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">是否顯示於辨識圖上:</label>
                                                            <div class="col-md-6">
                                                                <label><input type="radio" value='Y' name="default_cover_identify_image"/> 是</label>
                                                                <label><input type="radio" value='N' name="default_cover_identify_image" checked/> 否</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">AR 互動文字(20字內):</label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="default_ar_text" class="form-control"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">AR 互動 URL:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="default_ar_url" class="form-control"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">AR呈現高度(離地幾公尺):</label>
                                                            <div class="col-md-6">
                                                                <input type="range" min="0" max="500" step="1"
                                                                       name="default_ar_heigh" value="0">
                                                                <output>0m</output>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">AR大小縮放比例:</label>
                                                            <div class="col-md-6">
                                                                <input type="range" min="0" max="500" step="5"
                                                                       name="default_ar_vsize" value="100">
                                                                <output>100%</output>
                                                                <div class="clearfix">
                                                                    <label for="" class="label font-blue"><i class="fa fa-info-circle"></i> 僅於 3d model 上有效</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-md-offset-3 text-center">
                                                        <button id="btn_submit" class=" btn green btn_submit" ><i class="fa fa-check"></i> 上傳</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                            </form>
                        </div>

                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊 ?>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/select2/select2.min.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-summernote/summernote.min.js") ?>"></script> 
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-summernote/lang/summernote-zh-TW.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
            <?php if ($this->session->i18n == 'zh') : ?>
                <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
            <?php endif; ?>
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN PAGE LEVEL SCRIPTS -->
            <script src="<?= base_url("assets/global/scripts/metronic.js") ?>" type="text/javascript"></script>
            <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
            <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>
            <script src="<?= base_url("assets/global/scripts/datatable_{$this->session->i18n}.js") ?>"></script>
            <script src="<?= base_url("assets/admin/pages/scripts/table-ajax_{$this->session->i18n}.js") ?>"></script>
            <!-- END PAGE LEVEL SCRIPTS -->
            <script src="<?= base_url("js/geofence.js") ?>" type="text/javascript"></script>
            <script src="<?= base_url("js/google_map.js") ?>" type="text/javascript"></script>
            <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= $this->config->item("google")["api_key"] ?>&callback=initMap"></script>
            
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/qr-code-generator-with-logo/dist/jquery-qrcode.min.js") ?>"></script>
             <script type="text/javascript" nonce="cm1vaw==">
                jQuery(document).ready(function () {
                    Metronic.init(); // init metronic core components
                    Layout.init(); // init current layout
                    QuickSidebar.init(); // init quick sidebar

                    $('.btn_submit').on('click', function () {

                        event.preventDefault();
                            
                            // 
                            // $('form').submit();
                        var form = $('#ajax-form');

                        // Stop the browser from submitting the form.
                        // document.getElementById("loading_bar").style.display = "block";

                        // Serialize the form data.
                        // var formData = $(form).serialize();
                        var formData = new FormData(form[0]);

                        // Submit the form using AJAX.
                        $.ajax({
                            type: 'POST',
                            url: $(form).attr('action'),
                            data:formData,
                            cache:false,
                            contentType: false,
                            processData: false
                        }).done(function(response) {
                            document.getElementById("loading_bar").style.display = "none";

                            if (response.indexOf("script")!=-1) {
                                $("#response_div").html(response);
                            }else{
                                alert(response); 
                            }
                            // alert(response);
                            // console.log(response);
                        }).fail(function(data) {
                            console.log(data);
                        });
                        
                    });

                    var canvas;
                    var dai_name;

                    $("#create_qrcode").click(async function(){

                        dai_name = $('input[name^=dai_name]').val();
                        if(dai_name === ''){
                            alert('請輸入辨識圖名稱');
                            return;
                        }

                        let uuid = _uuid();
                        // let url = `https://taipeipassjrsys.page.link/ardownload?_symbol=${uuid}`;
                        let url = `https://tppass.page.link/ardownload?_symbol=${uuid}`;
                        $('#qrcode').empty().qrcode({
                            render: 'canvas',
                            mode: 4,
                            size: 150,
                            mSize: 0.1,
                            mPosX: 0.5,
                            mPosY: 0.5,
                            image: $('<img src="/images/taipei_logo.jpeg">')[0],
                            text: url,//二維碼內容
                        });

                        canvas = $('#qrcode canvas')[0];
                        let src = canvas.toDataURL("image/png");
                        $('#qrcode_img').attr('src', src);
                    });

                    $('#qrcode_img').on('load', async function () {
                        canvas.width = 150;
                        canvas.height = 180;
                        var ctx = canvas.getContext('2d');
                        //设置画布背景
                        ctx.fillStyle = '#ffffff';
                        ctx.fillRect(0, 0, canvas.width, canvas.height);
                        //设置文字样式
                        ctx.fillStyle = '#000000';
                        ctx.font = 'bold ' + 15 + 'px Arial';
                        ctx.textAlign = 'center';
                        //文字描述
                        ctx.fillText(dai_name, 75, 170);
                        //绘制二维码
                        ctx.drawImage($("#qrcode_img")[0], 0, 0);
                        // =================================================================

                        let src = canvas.toDataURL("image/png");
                        let base64Response = await fetch(src);
                        let blob = await base64Response.blob();
                        let file = new File([blob], "qrcode.png",{type:"mime/type", lastModified:new Date().getTime()});
                        let container = new DataTransfer();
                        container.items.add(file);
                        let fileInputElement = $('input[name=<?= $field_prefix ?>_image_path]')[0];
                        fileInputElement.files = container.files;
                        $("#create_qrcode").parent().parent().find(".fileinput-preview #preview_image_path").attr('src', src);
                        $("#create_qrcode").closest(".fileinput-new").removeClass("fileinput-new").addClass("fileinput-exists");
                    });
                    
                });
                area_select_map.init('<?= base_url() ?>');


                // 跳窗事件，做選擇判斷
                function open_ar_list_window(){

                    var ar_list_window = popitup('<?=$this->config->item("base_url")."sa/ar_list"?>','ar_list');
                    var ar_id = "";

                    ar_list_window.onbeforeunload = function(){
                           // set warning message
                           $.getJSON('return_ar_id', function(){     
                           }).success(function(result){
                               
                               //ajax 讀取
                                ar_id = result.ar_id;
                                ar_data = result.ar_data;

                                ar_scan_image = '<?php echo $this->config->item('server_base_url')."upload_image/";?>'+ar_data.dai_image_path;

                                ar_find_image = '<?php echo $this->config->item('server_base_url')."upload/image/";?>'+ar_data.dai_findpho_path;

                                $("#preview_image_path").attr('src',ar_scan_image);
                                $("#preview_findpho_path").attr('src',ar_find_image);


                           }).error(function(result){
                                console.log(result);
                           });

                     };
                }

                
                $('body').on('input', 'input[name$=_ar_heigh]', function() {
                    // oninput="this.nextElementSibling.value = this.value+'m'"
                    $(this).next().val($(this).val()+'m');
                });
                $('body').on('input', 'input[name$=_ar_vsize]', function() {
                    // oninput="this.nextElementSibling.value = this.value+'m'"
                    $(this).next().val($(this).val()+'%');
                });

            </script>
            <!-- END JAVASCRIPTS -->
             <script type="text/javascript" nonce="cm1vaw==">
                function _uuid() {
                    var d = Date.now();
                    if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
                        d += performance.now(); //use high-precision timer if available
                    }
                    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                        var r = (d + Math.random() * 16) % 16 | 0;
                        d = Math.floor(d / 16);
                        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
                    });
                }
            </script>
    </body>
    <!-- END BODY -->
</html>
