<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>

        <link href="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url("assets/plugins/jquery-ui/jquery-ui.css") ?>" rel="stylesheet" type="text/css" />
        <link href='<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.css") ?>' rel='stylesheet'>
        <link href="<?= base_url("assets/plugins/select2/css/select2.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("assets/plugins/select2/css/select2_bootstrap.css") ?>" rel="stylesheet" type="text/css" />
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //上方標題列 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <form action="<?= base_url("{$this->controller_name}/{$table_name}/add_go") ?>" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data">
                                <div class="portlet">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-edit"></i>新增
                                        </div>
                                        <div class="actions btn-set">
                                            <a href="<?= base_url("{$this->controller_name}/{$table_name}") ?>" class="btn default"><i class="fa fa-angle-left"></i> <?= $this->lang->line("返回") ?></a>
                                            <button type="submit" class="btn green"><i class="fa fa-check"></i> <?= $this->lang->line("儲存") ?></button>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="tabbable">
                                            <ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a href="#tab_general" data-toggle="tab">
                                                        基本資料
                                                    </a>
                                                </li>
                                            </ul>
                                            <div class="tab-content no-space">
                                                <div class="tab-pane active" id="tab_general">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">名稱:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="<?= $field_prefix ?>_name" class="form-control" placeholder="請輸入名稱" />
                                                                <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_name">
                                                                    <button class="close" data-close="alert"></button>
                                                                    <span> <?= $this->lang->line("請輸入") ?>名稱 </span> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- <div class="form-group">
                                                            <label class="control-label col-md-3">英文名稱:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="<?= $field_prefix ?>_name_en" class="form-control" placeholder="請輸入名稱" />
                                                                <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_name_en">
                                                                    <button class="close" data-close="alert"></button>
                                                                    <span> <?= $this->lang->line("請輸入") ?>英文名稱 </span> 
                                                                </div>
                                                            </div>
                                                        </div> -->
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">聯絡電話:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control" name="<?= $field_prefix ?>_tel" value="" placeholder="請輸入聯絡電話" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">描述:</label>
                                                            <div class="col-md-6">
                                                                <textarea class="form-control" name="<?= $field_prefix ?>_desc" rows="5" placeholder="請輸入描述內容"></textarea>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">網址:</label>
                                                            <div class="col-md-6">
                                                                <input type="url" class="form-control" name="<?= $field_prefix ?>_web" value="" placeholder="請輸入網址" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">圖片:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control img_input" name="<?= $field_prefix ?>_image" value="" placeholder="請輸入圖片連結" >
                                                                <img class="img img-front" title="圖片預覽" style="width:250px"alt="圖片預覽" src="http://www.placehold.it/250x180/EFEFEF/AAAAAA&amp;text=no picture">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">票價資訊:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control" name="<?= $field_prefix ?>_ticketinfo" value="" placeholder="請輸入票價資訊" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">開放時間:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control" name="<?= $field_prefix ?>_opentime" value="" placeholder="請輸入開放時間" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">旅遊資訊:</label>
                                                            <div class="col-md-6">
                                                                <textarea class="form-control" name="<?= $field_prefix ?>_travellinginfo" rows="3" placeholder="請輸入旅遊資訊"></textarea>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">地址:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control searchaddr" name="<?= $field_prefix ?>_address" value="" placeholder="請輸入地址" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"></label>
                                                            <div class="col-md-6">
                                                                <label class="control-label newlat" hidden></label>
                                                                <label class="control-label newlng" hidden></label>
                                                                <input type="text" class="form-control lat" name="<?= $field_prefix ?>_lat" value="<?= $this->config->item('default_lat') ?>" placeholder="請輸入經度" >
                                                                <input type="text" class="form-control lng" name="<?= $field_prefix ?>_lng" value="<?= $this->config->item('default_lng') ?>" placeholder="請輸入緯度" >
                                                                <div id="map" style="width:100%;height:400px"></div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><?= $this->lang->line("狀態") ?>:</label>
                                                            <div class="col-md-6">
                                                                <select class="form-control" name="<?= $field_prefix ?>_enabled">
                                                                    <option value="Y"><?= $this->lang->line("啟用中") ?></option>
                                                                    <option value="N"><?= $this->lang->line("已關閉") ?></option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">是否推薦此景點:</label>
                                                            <div class="col-md-6">
                                                                <select class="form-control" name="<?= $field_prefix ?>_recommand">
                                                                    <option value="N">一般顯示即可</option>
                                                                    <option value="Y">強力推薦景點</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊 ?>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
        <?php if ($this->session->i18n == 'zh') : ?>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
        <?php endif; ?>
        <script type="text/javascript" src="<?= base_url("assets/plugins/jquery-ui/jquery-ui.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.js") ?>"></script>
        <script type='text/javascript' src='<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-sliderAccess.js") ?>'></script>
        <script type="text/javascript" src="<?= base_url("assets/plugins/select2/js/select2.min.js") ?>"></script>
        <script type='text/javascript' src='<?= base_url('assets/plugins/select2/js/i18n/zh-TW.js') ?>'></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("assets/global/scripts/metronic.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("js/geofence.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("js/google_map.js") ?>" type="text/javascript"></script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= $this->config->item("google")["api_key"] ?>&callback=area_select_map.initMap"></script>
         <script type="text/javascript" nonce="cm1vaw==">
            $(function () {

                $('#btn_submit').on('click', function () {
                    if (on_submit()) {
                        $('form').submit();
                    }
                });

                $('form').submit(on_submit);

                function on_submit() {
                    var result = true;

                    $('.validation').each(function () {
                        var name = $(this).attr('for');

                        if (!$('*[name=' + name + ']').val()) {
                            $(this).show();
                            $('*[name=' + name + ']').focus();
                            event.preventDefault();
                            result = false;
                        } else {
                            $(this).hide();
                        }
                    });

                    return result;
                }
                $('.date-picker').datetimepicker({
                    dateFormat: 'yy-mm-dd',
                    timeFormat: 'HH:mm'
                });
            });
        </script>
         <script nonce="cm1vaw==">
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
            });
            area_select_map.init('<?= base_url() ?>');

            function checkall(selector) {
                $(selector).prop('checked', true);
                $(selector).parent().addClass('checked');
            }
            function uncheck(selector) {
                $(selector).prop('checked', false);
                $(selector).parent().removeClass('checked');
            }

            $(document).on('click', '[name=remove]', function () {
                $(this).parent().remove();
            });

            $(".select_area").select2({
                allowClear: false,
                theme: 'bootstrap'
            });
            
            $('.img_input').keyup(function () {
                $('.img').attr('src', $(this).val());
            });

        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
