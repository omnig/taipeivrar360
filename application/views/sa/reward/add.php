<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>

        <link href="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url("assets/plugins/jquery-ui/jquery-ui.css") ?>" rel="stylesheet" type="text/css" />
        <link href='<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.css") ?>' rel='stylesheet'>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //上方標題列 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered form-fit">
                                <div class="portlet-title">
                                    <div class="caption"> <i class="icon-pencil font-green-haze"></i> <span class="caption-subject font-green-haze bold uppercase"></span> <span class="caption-helper"> <?= $this->lang->line("修改後別忘了儲存資料唷!") ?> </span> </div>
                                    <div class="pull-right"> 
                                        <a href="javascript:;" class="btn blue tooltips submit" id="btn_submit" data-original-title="<?= $this->lang->line("儲存") ?>"><i class="fa fa-save"></i></a> 
                                        <a href="<?= base_url("{$this->controller_name}/{$table_name}") ?>" class="btn btn-default tooltips" data-original-title="返回"><i class="fa fa-reply"></i></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <form action="<?= base_url("{$this->controller_name}/{$table_name}/add_go") ?>" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">分類:</label>
                                                <div class="col-md-5">
                                                    <select class="form-control selected_theme" name="nc_id">
                                                        <option value="">請選擇分類</option>
                                                        <?php foreach ($nearby_category as $row): ?>
                                                            <option value="<?= $row->nc_id ?>"><?= $row->nc_title . ( ($row->nc_enabled == "Y") ? '' : ' [已關閉]') ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <div class="alert alert-danger display-hide validation" for="nc_id">
                                                        <button class="close" data-close="alert"></button>
                                                        <span> 請選擇分類 </span> 
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <a href="<?= base_url("{$this->controller_name}/nearby_category/add") ?>" class="form-control btn btn-default" title="前往新增分類" ><i class='fa fa-arrow-circle-right'></i> 新增</a>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><?= $this->lang->line("標題") ?>:</label>
                                                <div class="col-md-6">
                                                    <input type="text" name="<?= $field_prefix ?>_title" class="form-control" placeholder="<?= $this->lang->line("標題") ?>" />
                                                    <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_title">
                                                        <button class="close" data-close="alert"></button>
                                                        <span> <?= $this->lang->line("請輸入") ?><?= $this->lang->line("標題") ?> </span> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><?= $this->lang->line("描述") ?>:</label>
                                                <div class="col-md-6">
                                                    <textarea class="form-control" name="<?= $field_prefix ?>_description" rows="3" placeholder="請輸入內容"></textarea>
                                                    <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_description">
                                                        <button class="close" data-close="alert"></button>
                                                        <span> <?= $this->lang->line("請輸入") ?><?= $this->lang->line("描述") ?> </span> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label col-md-3"><?= $this->lang->line("圖片") ?>:</label>
                                                <div class="col-md-6">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no image" alt=""/>
                                                        </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> <?= $this->lang->line("選取圖片") ?> </span>
                                                                <span class="fileinput-exists"> <?= $this->lang->line("更換") ?> </span>
                                                                <input type="file" name="<?= $field_prefix ?>_image" accept="image/*">
                                                            </span>
                                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?= $this->lang->line("移除") ?> </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><?= $this->lang->line("地址") ?>:</label>
                                                <div class="col-md-6">
                                                    <input type="text" name="<?= $field_prefix ?>_address" class="form-control searchaddr" placeholder="<?= $this->lang->line("地址") ?>" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">坐標:</label>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control lat" name="<?= $field_prefix ?>_lat" value="<?= $this->config->item('default_lat') ?>" placeholder="請輸入經度" >
                                                    <input type="text" class="form-control lng" name="<?= $field_prefix ?>_lng" value="<?= $this->config->item('default_lng') ?>" placeholder="請輸入緯度" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"></label>
                                                <div class="col-md-6">
                                                    <label class="control-label newlat" hidden></label>
                                                    <label class="control-label newlng" hidden></label>                                                    
                                                    <div id="map" style="height:300px"></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><?= $this->lang->line("狀態") ?>:</label>
                                                <div class="col-md-6">
                                                    <select class="form-control" name="<?= $field_prefix ?>_enabled">
                                                        <option value="Y"><?= $this->lang->line("啟用中") ?></option>
                                                        <option value="N"><?= $this->lang->line("已關閉") ?></option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-6">
                                                    <button type="submit" class="btn blue"><i class="fa fa-save"></i> <?= $this->lang->line("儲存") ?> </button>
                                                    <a href="<?= base_url("{$this->controller_name}/{$table_name}") ?>" class="btn btn-default tooltips" data-original-title="<?= $this->lang->line("取消") ?>"><?= $this->lang->line("取消") ?></a> 
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊 ?>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/select2/select2.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
        <?php if ($this->session->i18n == 'zh') : ?>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
        <?php endif; ?>
        <script type="text/javascript" src="<?= base_url("assets/plugins/jquery-ui/jquery-ui.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.js") ?>"></script>
        <script type='text/javascript' src='<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-sliderAccess.js") ?>'></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("assets/global/scripts/metronic.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("js/geofence.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("js/google_map.js") ?>" type="text/javascript"></script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= $this->config->item("google")["api_key"] ?>&callback=area_select_map.initMap"></script>
         <script type="text/javascript" nonce="cm1vaw==">
            $(function () {
                $('#btn_submit').on('click', function () {
                    if (on_submit()) {
                        $('form').submit();
                    }
                });

                $('form').submit(on_submit);

                function on_submit() {
                    var result = true;

                    $('.validation').each(function () {
                        var name = $(this).attr('for');

                        if (!$('*[name=' + name + ']').val()) {
                            $(this).show();
                            $('*[name=' + name + ']').focus();
                            event.preventDefault();
                            result = false;
                        } else {
                            $(this).hide();
                        }
                    });

                    return result;
                }
                $('.date-picker').datetimepicker({
                    dateFormat: 'yy-mm-dd',
                    timeFormat: 'HH:mm'
                });
            });
        </script>
         <script nonce="cm1vaw==">
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
            });
            area_select_map.init('<?= base_url() ?>');

            function checkall(selector) {
                $(selector).prop('checked', true);
                $(selector).parent().addClass('checked');
            }
            function uncheck(selector) {
                $(selector).prop('checked', false);
                $(selector).parent().removeClass('checked');
            }

            $(document).on('click', '[name=remove]', function () {
                $(this).parent().remove();
            });

        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
