<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>

        <link href="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url("assets/global/plugins/slim-select/css/slimselect.min.css") ?>" rel="stylesheet"></link>
        <link href="<?= base_url("assets/base/css/custom.css") ?>" rel="stylesheet"></link>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //上方標題列 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <form action="<?= base_url("{$this->controller_name}/{$table_name}/add_go") ?>" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data">
                                <div class="portlet">
                                    <div class="portlet-title">
                                        <div class="caption">
                                        </div>
                                        <div class="actions btn-set">
                                            <a href="<?= base_url("{$this->controller_name}/{$table_name}")  ?>" class="btn default"><i class="fa fa-angle-left"></i> <?= $this->lang->line("返回") ?></a>
                                            <button type="submit" class="btn green"><i class="fa fa-check"></i> <?= $this->lang->line("儲存") ?></button>
                                        </div>
                                    </div>
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">所屬局處:</label>
                                            <div class="col-md-6">
                                                <select name="d_id" class="form-control">
                                                    <option value=''>請選擇</option>
                                                    <?php foreach ($department_list as $row): ?>
                                                        <option value="<?= $row->ag_id ?>" ><?= $row->ag_name ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">主題類別:</label>
                                            <div class="col-md-6">
                                                <select name="tc_id" class="form-control">
                                                    <option value=''>請選擇</option>
                                                    <?php foreach ($topic_category as $row): ?>
                                                        <option value="<?= $row->tc_id ?>" ><?= $row->tc_title_zh ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">主題名稱:</label>
                                            <div class="col-md-6">
                                                <input type="text" name="<?= $field_prefix ?>_name" class="form-control"/>
                                            </div>
                                        </div>

                                        <!-- 主題描述 -->
                                        <div class="form-group">
                                            <label class="control-label col-md-3">主題描述:</label>
                                            <div class="col-md-6">
                                                <textarea name="<?= $field_prefix ?>_describe" rows="5" class="form-control"/></textarea>
                                            </div>
                                            (限定 100 字)
                                        </div>

                                        <!-- 主題代表圖片 -->
                                        <div class="form-group ">
                                            <label class="control-label col-md-3">主題代表圖片:<span class="required"> * </span></label>
                                            <div class="col-md-9">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=<?= $this->lang->line("沒有圖片") ?>" alt=""/>
                                                    </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> <?= $this->lang->line("選取圖片") ?> </span>
                                                            <span class="fileinput-exists"> <?= $this->lang->line("更換") ?> </span>
                                                            <input type="file" name="<?= $field_prefix ?>_path" accept="image/*">
                                                        </span>
                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?= $this->lang->line("移除") ?> </a>
                                                    </div>
                                                </div>
                                                (建議圖片尺寸800*460)
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">篩選局處:</label>
                                            <div class="col-md-6">
                                                <select class="form-control" id="dep_select" name="dep_id">
                                                    <option value=''>全部</option>
                                                    <?php foreach ($department_list as $row): ?>
                                                        <option value="<?= $row->ag_id ?>" ><?= $row->ag_name ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group tm">
                                            <label class="control-label col-md-3">POI選擇:</label>
                                            <div class="col-md-6">
                                                <select id="poi-select" multiple name="poi_id[]">
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">設定參觀順序:</label>
                                            <div class="col-md-6">
                                                <div class="portlet box blue">
                                                    <div class="portlet-title">
                                                        <div class="caption"><i class="fa fa-reorder"></i>請拖曳項目進行排序</div>
                                                    </div>
                                                    <div class="portlet-body">
                                                        <div id="items" class="list-group">
                                                        </div>
                                                        <input type="hidden" name="ap_ids">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">狀態:<span class="required"> * </span></label>
                                            <div class="col-md-6">
                                                <select class="form-control" name="<?= $field_prefix ?>_enabled">
                                                    <option value="Y">啟用中</option>
                                                    <option value="N">已關閉</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-6">
                                            <button type="submit" class="btn blue"><i class="fa fa-save"></i> <?= $this->lang->line("儲存") ?> </button>
                                            <a href="<?= base_url("{$this->controller_name}/{$table_name}") ?>" class="btn btn-default tooltips" data-original-title="<?= $this->lang->line("取消") ?>"><?= $this->lang->line("取消") ?></a> 
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊 ?>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js") ?>"></script>
            <!-- <script type="text/javascript" src="<?= base_url("assets/global/plugins/select2/select2.min.js") ?>"></script> -->
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-summernote/summernote.min.js") ?>"></script> 
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-summernote/lang/summernote-zh-TW.js") ?>"></script>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
            <?php if ($this->session->i18n == 'zh') : ?>
                <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
            <?php endif; ?>
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN PAGE LEVEL SCRIPTS -->
            <script src="<?= base_url("assets/global/scripts/metronic.js") ?>" type="text/javascript"></script>
            <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
            <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>
            <script src="<?= base_url("assets/global/scripts/datatable_{$this->session->i18n}.js") ?>"></script>
            <script src="<?= base_url("assets/admin/pages/scripts/table-ajax_{$this->session->i18n}.js") ?>"></script>
            <script src="<?= base_url("assets/global/plugins/slim-select/js/slimselect.min.js")?>"></script>
            <script src="<?= base_url("assets/global/plugins/sortable/sortable.min.js")?>"></script>
             <script type="text/javascript" nonce="cm1vaw==">
                $(function () {
                    $('#btn_submit').on('click', function () {
                        if (on_submit()) {
                            $('form').submit();
                        }
                    });

                    $('form').submit(on_submit);

                    function on_submit() {
                        var result = true;

                        $('.validation').each(function () {
                            var name = $(this).attr('for');

                            if ($('input[name=' + name + ']:radio').length > 0 && $('input[name=' + name + ']:radio:checked').length === 0) {
                                $(this).show();

                                //顯示此tab頁面
                                if ($(this).parents(".tab-pane")) {
                                    var id = $(this).parents(".tab-pane").attr("id");
                                    $('.nav-tabs a[href="#' + id + '"]').tab('show');
                                }

                                $('*[name=' + name + ']').focus();
                                event.preventDefault();
                                result = false;
                            } else if (!$('*[name=' + name + ']:not(radio)').val()) {
                                $(this).show();

                                //顯示此tab頁面
                                if ($(this).parents(".tab-pane")) {
                                    var id = $(this).parents(".tab-pane").attr("id");
                                    $('.nav-tabs a[href="#' + id + '"]').tab('show');
                                }

                                $('*[name=' + name + ']').focus();
                                event.preventDefault();
                                result = false;
                            } else {
                                $(this).hide();
                            }
                        });

                        return result;
                    }
                });


                let poiSelectOptions = [
                    <?php foreach ($poi_list as $row): ?>
                        { value: '<?= $row->ap_id ?>', text: '<?= $row->ap_name ?>', display: true, selected: false, dep: '<?= $row->d_id ?>' },
                    <?php endforeach; ?>
                ];
                const poiSelect = new SlimSelect({
                    select: '#poi-select',
                    closeOnSelect: false,
                    allowDeselectOption: true,
                    hideSelectedOption: true
                });
                poiSelect.setData(poiSelectOptions);

                let tempScrolArr = [];
                $('#poi-select').change(() => {
                    let selectVal = poiSelect.selected();
                    let result = [];
                    // 檢查變多變少
                    if(selectVal.length === tempScrolArr.length){
                        return;
                    }
                    if(selectVal.length > tempScrolArr.length){
                        // 變多
                        let diffItems = $(selectVal).not(tempScrolArr).toArray()
                        add_item(diffItems);
                    }
                    if(selectVal.length < tempScrolArr.length){
                        // 變少
                        let diffItems = $(tempScrolArr).not(selectVal).toArray()
                        remove_item(diffItems);
                    }
                    
                    sync_items_selected();
                });

                $("body").on('click', '.del-item', function () {
                    const id = $(this).parent().data('id')+''
                    $("#" + id).remove();
                    remove_item([id]);

                    sync_items_selected();
                })

                function sync_items_selected(){
                    const ap_ids = []
                    $("#items").find('.list-group-item').each(function () {
                        ap_ids.push($(this).data('id'))
                    })
                    $("input[name=ap_ids]").val(ap_ids);
                }

                function add_item(addItems){
                    let items = $("#items").find('.list-group-item');
                    let randomId = Math.random().toString(16).substr(2, 8);
                    for(let i in addItems){
                        let val = addItems[i];
                        let name = poiSelectOptions.find(element => element.value === val).text;
                        $("#items").append(`<div class="list-group-item" id="${randomId}" data-id="${val}">${name}<button class="btn btn-sm red del-item float-right" type="button" data-id="${randomId}"><i class="fa fa-times"></i></button></div>`);
                        tempScrolArr.push(val);
                    }
                }

                function remove_item(removeItems){
                    // 移除scrolbar
                    $("#items").find('.list-group-item').each(function () {
                        let id = $(this).data('id')+'';
                        let realId = $(this).attr('id');
                        if(removeItems.includes(id)){
                            $("#" + realId).remove();
                            let index = tempScrolArr.indexOf(id);
                            if(index > -1){
                                tempScrolArr.splice(index, 1);
                            }
                        }
                    });
                    // 移除select
                    let selectVal = poiSelect.selected();
                    let changeSelect = false;
                    for(let i in removeItems){
                        if(selectVal.includes(removeItems[i])){
                            let index = selectVal.indexOf(removeItems[i]);
                            selectVal.splice(index, 1);
                            changeSelect = true;
                        }
                    }
                    if(changeSelect){
                        poiSelect.set(selectVal);
                    }
                }

                const items = document.getElementById('items');
                const poiSortable = new Sortable(items, {
                    animation: 150,
                    ghostClass: 'blue-background-class',
                    onChange: e => {
                        let ap_ids = [];
                        $("#items").find('.list-group-item').each(function () {
                            ap_ids.push($(this).data('id'));
                        });
                        $("input[name=ap_ids]").val(ap_ids);
                    }
                });

                // 篩選不同局處
                $('#dep_select').change(() => {
                    let dep = $('#dep_select').val();
                    for(let i in poiSelectOptions){
                        poiSelectOptions[i].display = dep === '' || poiSelectOptions[i].dep === dep;
                        poiSelectOptions[i].selected = tempScrolArr.indexOf(poiSelectOptions[i].val) > -1;
                    }
                    
                    let selectVal = poiSelect.selected();
                    poiSelect.setData(poiSelectOptions);
                    poiSelect.set(selectVal);
                });

            </script>
             <script nonce="cm1vaw==">
                jQuery(document).ready(function () {
                    Metronic.init(); // init metronic core components
                    Layout.init(); // init current layout
                    QuickSidebar.init(); // init quick sidebar
                });
            </script>
            <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
