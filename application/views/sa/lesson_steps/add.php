<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>

        <link href="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url("assets/plugins/jquery-ui/jquery-ui.css") ?>" rel="stylesheet" type="text/css" />
        <link href='<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.css") ?>' rel='stylesheet'>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //上方標題列 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered form-fit">
                                <div class="portlet-title">
                                    <div class="caption"> <i class="icon-pencil font-green-haze"></i> <span class="caption-subject font-green-haze bold uppercase"></span> <span class="caption-helper"> <?= $this->lang->line("修改後別忘了儲存資料唷!") ?> </span> </div>
                                    <div class="pull-right"> 
                                        <a href="javascript:;" class="btn blue tooltips submit" id="btn_submit" data-original-title="<?= $this->lang->line("儲存") ?>"><i class="fa fa-save"></i></a> 
                                        <a href="<?= base_url("{$this->controller_name}/{$table_name}?l_id=").$l_id ?>" class="btn btn-default tooltips" data-original-title="返回"><i class="fa fa-reply"></i></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <form action="<?= base_url("{$this->controller_name}/{$table_name}/add_go?l_id=").$l_id ?>" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data">
                                        <div class="form-body">
                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3">步驟標題:</label>
                                                <div class="col-md-6">
                                                    <input type="text" name="<?= $field_prefix ?>_title" class="form-control" placeholder="<?= $this->lang->line("標題") ?>" />
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3">步驟內容(選填):</label>
                                                <div class="col-md-6">
                                                    <textarea name="<?= $field_prefix ?>_describe" rows="10" class="form-control" placeholder="請輸入文章內容" /></textarea>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3">步驟<?= $this->lang->line("圖片") ?>(選填):</label>
                                                <div class="col-md-6">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                                            <img src="https://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=<?= $this->lang->line("沒有圖片") ?>" alt=""/>
                                                        </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> <?= $this->lang->line("選取圖片") ?> </span>
                                                                <span class="fileinput-exists"> <?= $this->lang->line("更換") ?> </span>
                                                                <input type="file" name="<?= $field_prefix ?>_image" accept="image/*">
                                                            </span>
                                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?= $this->lang->line("移除") ?> </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            

                                            <div class="form-group">
                                                <label class="control-label col-md-3">步驟連結(選填):</label>
                                                <div class="col-md-6">
                                                    <input type="url" name="<?= $field_prefix ?>_link" class="form-control" placeholder="請輸入步驟連結" />
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3">步驟排序:</label>
                                                <div class="col-md-6">
                                                    <input type="number" name="<?= $field_prefix ?>_order" class="form-control"  value="1" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">步驟<?= $this->lang->line("狀態") ?>:</label>
                                                <div class="col-md-6">
                                                    <select class="form-control" name="<?= $field_prefix ?>_enabled">
                                                        <option value="Y"><?= $this->lang->line("啟用中") ?></option>
                                                        <option value="N"><?= $this->lang->line("已關閉") ?></option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>

                                        


                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-6">
                                                    <button type="submit" class="btn blue"><i class="fa fa-save"></i> <?= $this->lang->line("儲存") ?> </button>
                                                    <a href="<?= base_url("{$this->controller_name}/{$table_name}?l_id=").$l_id ?>" class="btn btn-default tooltips" data-original-title="<?= $this->lang->line("取消") ?>"><?= $this->lang->line("取消") ?></a> 
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊 ?>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/select2/select2.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
        <?php if ($this->session->i18n == 'zh') : ?>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
        <?php endif; ?>
        <script type="text/javascript" src="<?= base_url("assets/plugins/jquery-ui/jquery-ui.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.js") ?>"></script>
        <script type='text/javascript' src='<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-sliderAccess.js") ?>'></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("assets/global/scripts/metronic.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->

         <script type="text/javascript" nonce="cm1vaw==">
            $(function () {

                $('#btn_submit').on('click', function () {
                    if (on_submit()) {
                        $('form').submit();
                    }
                });

                $('form').submit(on_submit);

                function on_submit() {
                    var result = true;

                    $('.validation').each(function () {
                        var name = $(this).attr('for');

                        if (!$('*[name=' + name + ']').val()) {
                            $(this).show();
                            $('*[name=' + name + ']').focus();
                            event.preventDefault();
                            result = false;
                        } else {
                            $(this).hide();
                        }
                    });

                    return result;
                }
                $('.date-picker').datetimepicker({
                    dateFormat: 'yy-mm-dd',
                    timeFormat: 'HH:mm'
                });
            });
        </script>
         <script nonce="cm1vaw==">
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
            });

            function checkall(selector) {
                $(selector).prop('checked', true);
                $(selector).parent().addClass('checked');
            }
            function uncheck(selector) {
                $(selector).prop('checked', false);
                $(selector).parent().removeClass('checked');
            }

            $(document).on('click', '[name=remove]', function () {
                $(this).parent().remove();
            });
            $(document).on('click', '[name=newremove]', function () {
                var count = $('[name=beacon]').length;
                if (count > 1) {
                    $(this).parent().remove();
                }
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
