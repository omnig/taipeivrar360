<!DOCTYPE html>
<!--[if IE 8]> <html lang="zh-TW" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="zh-TW" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>
        <link rel="stylesheet" type="text/css" href="<?= base_url("assets/global/plugins/bootstrap-summernote/summernote.css") ?>">
        <link href="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url("assets/global/plugins/slim-select/css/slimselect.min.css") ?>" rel="stylesheet"></link>
        <link href="<?= base_url("assets/base/css/custom.css") ?>" rel="stylesheet"></link>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //上方標題列 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered form-fit">
                                <div class="portlet-title">
                                    <div class="caption"> <i class="icon-pencil font-green-haze"></i> <span class="caption-subject font-green-haze bold uppercase"></span> <span class="caption-helper"> <?= $this->lang->line("修改後別忘了儲存資料唷!") ?> </span> </div>
                                    <div class="pull-right">
                                        <a href="javascript:;" class="btn blue tooltips submit"
                                                               id="btn_submit"
                                                               data-original-title="<?=$this->lang->line("儲存")?>">
                                            <i class="fa fa-save"></i>
                                        </a>
                                        <a href="<?= base_url("{$this->controller_name}/{$table_name}") ?>" class="btn btn-default tooltips" data-original-title="返回">
                                            <i class="fa fa-reply"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <form action="<?= base_url("{$this->controller_name}/{$table_name}/edit_go") ?>" enctype="multipart/form-data" class="form-horizontal" method="post">
                                        <div class="text-center"><h2>POI 基本資料</h2></div>
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">所屬局處:<span class="required"> * </span></label>
                                                <div class="col-md-6">
                                                    <select class="form-control" name="d_id">
                                                        <option value=''>請選擇</option>
                                                        <?php foreach ($department_list as $row): ?>
                                                            <option value="<?= $row->ag_id ?>" <?= ($row->ag_id === ${$table_name}->{"d_id"})?'selected':'' ?>> <?= $row->ag_name ?> </option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><?= $this->lang->line("分類") ?>:<span class="required"> * </span></label>
                                                <div class="col-md-6">
                                                    <select class="form-control" name="ac_id">
                                                        <option>請選擇</option>
                                                        <?php foreach ($category_list as $row): ?>
                                                            <option value="<?= $row->ac_id ?>" <?= (${$table_name}->{"ac_id"} == $row->ac_id) ? ' selected' : '' ?>> <?= $row->ac_title_zh ?> </option>
                                                        <?php endforeach ?> 
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><?= $this->lang->line("名稱") ?>:<span class="required"> * </span></label>
                                                <div class="col-md-6">
                                                    <input type="text" name="<?= $field_prefix ?>_name" class="form-control" placeholder="請輸入 POI 名稱" value="<?= ${$table_name}->{"{$field_prefix}_name"} ?>"/>
                                                    <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_name">
                                                        <button class="close" data-close="alert"></button>
                                                        <span> <?= $this->lang->line("請輸入") ?> POI 名稱 </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><?= $this->lang->line("描述") ?>:<span class="required"> * </span></label>
                                                <div class="col-md-6">
                                                    <textarea name="<?= $field_prefix ?>_desc" rows="5" class="form-control" placeholder="請輸入 POI 描述"><?= ${$table_name}->{"{$field_prefix}_desc"} ?></textarea>
                                                    <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_desc">
                                                        <button class="close" data-close="alert"></button>
                                                        <span> <?= $this->lang->line("請輸入") ?> POI 描述 </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- POI圖片 -->
                                            <div class="form-group ">
                                                <label class="control-label col-md-3">POI圖片:<span class="required"> * </span></label>
                                                <div class="col-md-4">
                                                    <div class="fileinput fileinput-exists" data-provides="fileinput">
                                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                                            <img src="<?= $this->config->item('server_base_url').'/upload_image/'.${$table_name}->{"{$field_prefix}_image_path"} ?>" alt=""/>
                                                        </div>
                                                        <div>
                                                            <label class="label font-blue"><i class='fa fa-info-circle'></i> 圖檔大小限制10M</label>
                                                        </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> <?= $this->lang->line("選取圖片") ?> </span>
                                                                <span class="fileinput-exists"> <?= $this->lang->line("更換") ?> </span>
                                                                <input type="file" name="<?= $field_prefix ?>_image_path" accept="image/jpeg">
                                                            </span>
                                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?= $this->lang->line("移除") ?> </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">超連結文字(20字內):</label>
                                                <div class="col-md-6">
                                                    <input type="text" name="<?= $field_prefix ?>_hyperlink_text" class="form-control" placeholder="請輸入超連結文字" value="<?= ${$table_name}->{"{$field_prefix}_hyperlink_text"} ?>"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">超連結:</label>
                                                <div class="col-md-6">
                                                    <input type="text" name="<?= $field_prefix ?>_hyperlink_url" class="form-control" placeholder="請輸入超連結" value="<?= ${$table_name}->{"{$field_prefix}_hyperlink_url"} ?>"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">坐標:<span class="required"> * </span></label>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control lng" name="<?= $field_prefix ?>_lng" value="<?= (!${$table_name}->{"{$field_prefix}_lng"}) ? $default_lng : ${$table_name}->{"{$field_prefix}_lng"} ?>" placeholder="請輸入經度" >
                                                    <input type="text" class="form-control lat" name="<?= $field_prefix ?>_lat" value="<?= (!${$table_name}->{"{$field_prefix}_lat"}) ? $default_lat : ${$table_name}->{"{$field_prefix}_lat"} ?>" placeholder="請輸入緯度" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"></label>
                                                <div class="col-md-6">
                                                    <label class="control-label newlat" hidden></label>
                                                    <label class="control-label newlng" hidden></label>
                                                    <div id="map"></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">預設 Beacon:</label>
                                                <div class="col-md-6">
                                                    <select name="b_id" id="beacon-select"></select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">預設光標籤:</label>
                                                <div class="col-md-6">
                                                    <select name="ol_id" id="optical-label-select"></select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">影片連結:</label>
                                                <div class="col-md-6">
                                                    <input type="text" name="<?= $field_prefix ?>_video_hyperlink" class="form-control" placeholder="請輸入影片連結" value="<?= ${$table_name}->{"{$field_prefix}_video_hyperlink"} ?>"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">語音檔:</label>
                                                <div class="col-md-6">
                                                    <?php
                                                        if (!${$table_name}->{"{$field_prefix}_audio_path"}) {
                                                            $btn_remove = false;
                                                            $audio = '';
                                                            $fileinput_status = 'fileinput-new';
                                                        } else {
                                                            $btn_remove = true;
                                                            $audio = '<a href="' . $this->config->item('server_base_url') . '/upload/audio/' . ${$table_name}->{"{$field_prefix}_audio_path"} . '">語音連結</a>';
                                                            $fileinput_status = 'fileinput-exists';
                                                        }
                                                    ?>
                                                    <div class="fileinput <?= $fileinput_status ?>" data-provides="fileinput">
                                                        <div class="input-group">
                                                            <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput" >
                                                                <i class="fa fa-file fileinput-exists"></i>
                                                                <span class="fileinput-filename"><?= ${$table_name}->{"{$field_prefix}_audio_path"} ?></span>
                                                            </div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> 上傳檔案 </span>
                                                                <span class="fileinput-exists"> <?= $this->lang->line("更換") ?> </span>
                                                                <input type="file" name="<?= $field_prefix ?>_audio_path" accept="mp3/*">
                                                            </span>
                                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">移除</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-md-offset-3">
                                                    <audio id="player" src="<?= $this->config->item('server_base_url') . '/upload/audio/' . ${$table_name}->{"{$field_prefix}_audio_path"} ?>" controls>
                                                        HTML5 audio not supported
                                                    </audio>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><?= $this->lang->line("狀態") ?>:<span class="required"> * </span></label>
                                                <div class="col-md-6">
                                                    <select class="form-control" name="<?= $field_prefix ?>_enabled">
                                                        <option value="Y"<?= (${$table_name}->{"{$field_prefix}_enabled"} == 'Y') ? ' selected' : '' ?>><?= $this->lang->line("啟用中") ?></option>
                                                        <option value="N"<?= (${$table_name}->{"{$field_prefix}_enabled"} == 'N') ? ' selected' : '' ?>><?= $this->lang->line("已關閉") ?></option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-center"><h2>AR 互動資料</h2></div>
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">綁定AR:</label>
                                                <div class="col-md-6">
                                                    <select class="form-control" name="tdar_id">
                                                        <option  data-type="init" value="">請選擇</option>
                                                        <?php foreach ($ar_list as $row): ?>
                                                            <option data-type="<?= $row->tdar_content_type ?>" value="<?= $row->tdar_id ?>" <?= (${$table_name}->{"tdar_id"} == $row->tdar_id) ? ' selected' : '' ?>> <?= $row->tdar_name ?> </option>
                                                        <?php endforeach ?> 
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">AR啟動方式:</label>
                                                <div class="col-md-6">
                                                    <select class="form-control" name="<?= $field_prefix ?>_ar_active_method" id="<?= $field_prefix ?>_ar_active_method">
                                                        <option value=""></option>
                                                        <option value="0" <?= ("0" == ${$table_name}->{$field_prefix."_ar_active_method"}) ? ' selected' : '' ?>> 靠近自動呈現 </option>
                                                        <option value="1" <?= ("1" == ${$table_name}->{$field_prefix."_ar_active_method"}) ? ' selected' : '' ?>> 點擊呈現 </option>
                                                        <option value="2" <?= ("2" == ${$table_name}->{$field_prefix."_ar_active_method"}) ? ' selected' : '' ?>> 圖像辨識 </option>
                                                    </select>
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                                            <img id="preview_image_path" src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=<?= $this->lang->line("沒有圖片") ?>" alt=""/>
                                                        </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-exists" id="change_identify_image"> <?= $this->lang->line("更換") ?> </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="dai_id" value="<?= ${$table_name}->dai_id ?>">
                                                    <div class="clearfix">
                                                        <label for="" class="label font-blue"><i class="fa fa-info-circle"></i> 靠近自動呈現 與 點擊呈現 僅可選擇 3D model AR</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">是否顯示於辨識圖上:</label>
                                                <div class="col-md-6" id="cover_identify_image">
                                                    <label><input type="radio" value='Y' name="<?= $field_prefix ?>_cover_identify_image" <?= ("Y" == ${$table_name}->{$field_prefix."_cover_identify_image"}) ? ' checked' : '' ?>/> 是</label>
                                                    <label><input type="radio" value='N' name="<?= $field_prefix ?>_cover_identify_image" <?= ("N" == ${$table_name}->{$field_prefix."_cover_identify_image"}) ? ' checked' : '' ?>/> 否</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">AR互動文字(20字內):</label>
                                                <div class="col-md-6">
                                                    <input type="text" name="<?= $field_prefix ?>_ar_text" class="form-control" value="<?= ${$table_name}->{"{$field_prefix}_ar_text"} ?>"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">AR互動URL:</label>
                                                <div class="col-md-6">
                                                    <input type="text" name="<?= $field_prefix ?>_ar_url" class="form-control" value="<?= ${$table_name}->{"{$field_prefix}_ar_url"} ?>"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">AR呈現高度(離地幾公尺):</label>
                                                <div class="col-md-6" id="heigh">
                                                    <input type="range" min="0" max="500" step="1"
                                                           name="<?=$field_prefix?>_ar_heigh" value="<?=${$table_name}->{$field_prefix."_ar_heigh"}?>">
                                                    <output><?=${$table_name}->{$field_prefix."_ar_heigh"}?>m</output>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">AR大小縮放比例:</label>
                                                <div class="col-md-6" id="vsize">
                                                    <input type="range" min="0" max="500" step="5"
                                                           name="<?=$field_prefix?>_ar_vsize" value="<?=${$table_name}->{$field_prefix."_ar_vsize"}?>">
                                                    <output><?=${$table_name}->{$field_prefix."_ar_vsize"}?>%</output>
                                                    <div class="clearfix">
                                                        <label for="" class="label font-blue"><i class="fa fa-info-circle"></i> 僅於 3d model 上有效</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">AR感應距離(公尺):</label>
                                                <div class="col-md-6">
                                                    <input onafterpaste="this.value=this.value.replace(/\D/g,'')" name="<?= $field_prefix ?>_ar_distance" class="form-control" value="<?= ${$table_name}->{"{$field_prefix}_ar_distance"} ?>"/>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if($this->input->get('af_id') !== null) : ?>
                                            <input type="hidden" name="af_id" value="<?=$this->input->get('af_id')?>">
                                        <?php endif;?>
                                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-6">
                                                    <input type="hidden" name="<?= "{$field_prefix}_id" ?>" value="<?= ${$table_name}->{"{$field_prefix}_id"} ?>" />
                                                    <button type="submit" class="btn blue"><i class="fa fa-save"></i> <?= $this->lang->line("儲存") ?> </button>
                                                    <a href="<?= base_url("{$this->controller_name}/{$table_name}") ?>" class="btn btn-default tooltips" data-original-title="<?= $this->lang->line("取消") ?>"><?= $this->lang->line("取消") ?></a> 
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊 ?>

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/select2/select2.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-summernote/summernote.min.js") ?>"></script> 
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-summernote/lang/summernote-zh-TW.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
        <?php if ($this->session->i18n == 'zh') : ?>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
        <?php endif; ?>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("assets/global/scripts/metronic.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("js/geofence.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("js/google_map.js") ?>" type="text/javascript"></script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= $this->config->item("google")["api_key"] ?>&callback=area_select_map.initMap"></script>
        <script src="<?= base_url("assets/global/plugins/slim-select/js/slimselect.min.js")?>"></script>
         <script type="text/javascript" nonce="cm1vaw==">
            $(function () {
                $('#btn_submit').on('click', function () {
                    if (on_submit()) {
                        $('form').submit();
                    }
                });

                $('form').submit(on_submit);

                function on_submit() {
                    var result = true;

                    $('.validation').each(function () {
                        var name = $(this).attr('for');

                        if (!$('*[name=' + name + ']').val()) {
                            $(this).show();
                            $('*[name=' + name + ']').focus();
                            event.preventDefault();
                            result = false;
                        } else {
                            $(this).hide();
                        }
                    });

                    return result;
                }

                $("#<?= $field_prefix ?>_ar_active_method").change(() => {
                    let method = $("#<?= $field_prefix ?>_ar_active_method").val();
                    if(method === '2'){
                        $('select[name=tdar_id] option').attr('disabled', false);
                        open_ar_list_window();
                    }else{
                        $("#preview_image_path").closest(".fileinput").hide();
                        $("#cover_identify_image").parent().hide();
                        // 隱藏非 3D model
                        $('select[name=tdar_id] option[data-type!=3d_model]').attr('disabled', true);
                        $('select[name=tdar_id] option[data-type=init]').attr('disabled', false);
                        if($('select[name=tdar_id] option[data-type!=3d_model]:selected').length > 0){
                            $('select[name=tdar_id]').val('').change();
                        }
                    }
                    return;
                });

                $('#change_identify_image').click(() => {
                    open_ar_list_window();
                    return true;
                });
                // 跳窗事件，做選擇判斷
                function open_ar_list_window(){

                    var ar_list_window = popitup('<?=$this->config->item("base_url")."/sa/ar_image_list"?>','ar_image_list');
                    var ar_id = "";

                    ar_list_window.onbeforeunload = function(){
                        // set warning message
                        setTimeout(() => {
                            $.getJSON('return_ar_image_id', function(){     
                            }).success(function(result){
                                
                                //ajax 讀取
                                ar_id = result.dai_id;
                                ar_data = result.ar_image_data;

                                ar_scan_image = '<?php echo $this->config->item('server_base_url')."/upload_image/";?>'+ar_data.dai_image_path;

                                $("#preview_image_path").attr('src',ar_scan_image);
                                $("#preview_image_path").closest(".fileinput").removeClass("fileinput-new").addClass("fileinput-exists").show();
                                $("#preview_image_path").parent().parent().next().val(ar_id);

                                if(ar_data.default_tdar_id !== null && confirm("是否使用辨識圖預設AR?\n包含 互動文字、互動URL、呈現高度、呈現大小")){
                                    let heigh = ar_data.default_ar_heigh;
                                    let text = ar_data.default_ar_text;
                                    let url = ar_data.default_ar_url;
                                    let vsize = ar_data.default_ar_vsize;
                                    let cover_identify_image = ar_data.default_cover_identify_image;
                                    let tdar_id = ar_data.default_tdar_id;
                                    $('select[name="tdar_id"]').val(tdar_id);
                                    $("#cover_identify_image input").prop('checked', false).closest('span').removeClass('checked');
                                    $("#cover_identify_image input").filter('[value="'+cover_identify_image+'"]').prop('checked', true).closest('span').addClass('checked');
                                    $('input[name="<?= $field_prefix ?>_ar_text"]').val(text);
                                    $('input[name="<?= $field_prefix ?>_ar_url"]').val(url);
                                    $('input[name="<?= $field_prefix ?>_ar_heigh"]').val(heigh);
                                    $("#heigh output").html(heigh+'m');
                                    $('input[name="<?= $field_prefix ?>_ar_vsize"]').val(vsize);
                                    $("#vsize output").html(vsize+'%');

                                    // $('select[name="_ar_heigh"]').val(heigh); //解開註解記得補$field_prefix
                                    // console.log(vsize);
                                    // $("#vsize input").prop('checked', false).closest('span').removeClass('checked');
                                    // $("#vsize input").filter('[value="'+vsize+'"]').prop('checked', true).closest('span').addClass('checked');
                                }

                            }).error(function(result){
                                    console.log(result);
                            });
                        }, 500);

                    };
                    $("#cover_identify_image").parent().show();
                }

                $('input[name="<?= $field_prefix ?>_audio_path"]').change((event) => {
                    console.log(event.target.files.length);
                    let src = ""
                    if(event.target.files.length > 0){
                        src = URL.createObjectURL(event.target.files[0]);
                    }
                    $("#player").attr('src', src);
                    $("#player")[0].load();
                });
                


                let beaconSelectOptions = [
                    {placeholder: true, text: '請選擇預設beacon'},
                    <?php foreach ($beacon_list as $row): ?>
                        {
                            value: '<?= $row->b_id ?>',
                            text: '<?= $row->b_description ?><?= is_null($row->ap_name)? '' : ' - '.$row->ap_name ?>',
                            disabled: <?= $row->is_bind && $row->b_id !== ${$table_name}->b_id?'true':'false' ?>,
                            selected: <?= $row->b_id === ${$table_name}->b_id?'true':'false' ?>
                        },
                    <?php endforeach; ?>
                ];
                const beaconSelect = new SlimSelect({
                    select: '#beacon-select',
                    placeholder: '請選擇預設beacon',
                    closeOnSelect: false,
                    allowDeselect: true,
                    hideSelectedOption: false
                });
                beaconSelect.setData(beaconSelectOptions);

                let opticalLabelSelectOptions = [
                    {text: '', placeholder: true},
                    <?php foreach ($optical_label_list as $row): ?>
                        {
                            value: '<?= $row->ol_id ?>',
                            text: '<?= $row->ol_description ?>(<?= $row->ol_property_number ?>)<?= is_null($row->ap_name)? '' : ' - '.$row->ap_name ?>',
                            disabled: <?= $row->is_bind && $row->ol_id !== ${$table_name}->ol_id ? 'true' : 'false' ?>,
                            selected: <?= $row->ol_id === ${$table_name}->ol_id?'true':'false' ?>
                        },
                    <?php endforeach; ?>
                ];
                
                const opticalLabelSelect = new SlimSelect({
                    select: '#optical-label-select',
                    placeholder: '請選擇預設光標籤',
                    closeOnSelect: false,
                    allowDeselect: true,
                    hideSelectedOption: false
                });
                opticalLabelSelect.setData(opticalLabelSelectOptions);

                
                $('body').on('input', 'input[name$=_ar_heigh]', function() {
                    // oninput="this.nextElementSibling.value = this.value+'m'"
                    $(this).next().val($(this).val()+'m');
                });
                $('body').on('input', 'input[name$=_ar_vsize]', function() {
                    // oninput="this.nextElementSibling.value = this.value+'m'"
                    $(this).next().val($(this).val()+'%');
                });
                $('body').on('keyup', 'input[name$=_ar_distance]', function() {
                    $(this).val($(this).val().replace(/\D/g,''));
                });
            });
        </script>
         <script nonce="cm1vaw==">
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
                <?php if (is_null($ar_image) || "2" !== ${$table_name}->{$field_prefix."_ar_active_method"}) :?>
                    $("#preview_image_path").closest(".fileinput").hide();
                    $("#cover_identify_image").parent().hide();
                <?php else :?>
                    $("#preview_image_path").closest(".fileinput").removeClass("fileinput-new").addClass("fileinput-exists").show();
                    $("#preview_image_path").attr('src','<?php echo $this->config->item('server_base_url')."/upload_image/".$ar_image->dai_image_path;?>');
                    $("#cover_identify_image").parent().show();
                <?php endif ?>
            });
            area_select_map.init('<?= base_url() ?>');

            <?php if($this->input->get('af_id') !== null) { ?>
            setTimeout(function () {
                area_select_map.set_tile_map("<?= $floor->af_plan_id ?>",<?= $floor->af_bottom_left_lat ?>,<?= $floor->af_bottom_left_lng ?>,<?= $floor->af_top_right_lat ?>,<?= $floor->af_top_right_lng ?>);
            }, 200);
            <?php } ?>
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
