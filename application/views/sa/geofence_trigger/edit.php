<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <?php include APPPATH . "views/{$this->controller_name}/templates/meta.php" ?>

        <link href="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url("css/{$this->controller_name}.css") ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url("assets/plugins/jquery-ui/jquery-ui.css") ?>" rel="stylesheet" type="text/css" />
        <link href='<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.css") ?>' rel='stylesheet'>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
        <?php include APPPATH . "views/{$this->controller_name}/templates/header.php";   //上方狀態列 ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include APPPATH . "views/{$this->controller_name}/templates/sidebar.php";  //左側menu ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <?php include APPPATH . "views/{$this->controller_name}/templates/page_title.php";  //上方標題列 ?>
                            <ul class="page-breadcrumb breadcrumb">
                                <?php include APPPATH . "views/{$this->controller_name}/templates/bread.php";  //麵包屑 ?>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <form id="form" action="<?= base_url("{$this->controller_name}/{$table_name}/edit_go") ?>" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data">
                                <div class="portlet">
                                    <div class="portlet-title">
                                        <div class="caption"> <i class="icon-pencil font-green-haze"></i> <span class="caption-subject font-green-haze bold uppercase"></span> <span class="caption-helper"> <?= $this->lang->line("修改後別忘了儲存資料唷!") ?> </span> </div>
                                        <div class="pull-right"> 
                                            <input type="hidden" name="<?= "{$field_prefix}_id" ?>" value="<?= ${$table_name}->{"{$field_prefix}_id"} ?>" />
                                            <a href="javascript:;" class="btn blue tooltips submit" id="btn_submit" data-original-title="<?= $this->lang->line("儲存") ?>"><i class="fa fa-save"></i></a> 
                                            <a href="<?= base_url("{$this->controller_name}/{$table_name}") ?>" class="btn btn-default tooltips" data-original-title="返回"><i class="fa fa-reply"></i></a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="tabbable">
                                            <ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a href="#tab_zh" data-toggle="tab">
                                                        基本資料
                                                    </a>
                                                </li>
                                                <!-- <li>
                                                    <a href="#tab_en" data-toggle="tab">
                                                        英文
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#tab_jp" data-toggle="tab">
                                                        日文
                                                    </a>
                                                </li> -->
                                            </ul>
                                            <div class="tab-content no-space">
                                                <div class="tab-pane active" id="tab_zh">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><?= $this->lang->line("標題") ?>:</label>
                                                        <div class="col-md-6">
                                                            <input type="text" name="<?= $field_prefix ?>_title" class="form-control" placeholder="<?= $this->lang->line("標題") ?>" value="<?= ${$table_name}->{"{$field_prefix}_title"} ?>" />
                                                            <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_title">
                                                                <button class="close" data-close="alert"></button>
                                                                <span> <?= $this->lang->line("請輸入") ?><?= $this->lang->line("標題") ?> </span> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><?= $this->lang->line("內容") ?>:</label>
                                                        <div class="col-md-6">
                                                            <textarea class="form-control" name="<?= $field_prefix ?>_description" rows="6" placeholder="<?= $this->lang->line("內容") ?>"><?= ${$table_name}->{"{$field_prefix}_description"} ?></textarea>
                                                            <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_description">
                                                                <button class="close" data-close="alert"></button>
                                                                <span> <?= $this->lang->line("請輸入") ?><?= $this->lang->line("內容") ?> </span> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">選擇地理柵欄:</label>
                                                        <div class="col-md-6">
                                                            <select class="form-control selected" name="g_id">
                                                                <?php foreach ($geofence as $row): ?>
                                                                    <option value="<?= $row->g_id ?>" <?= ($row->g_id == ${$table_name}->g_id) ? "selected" : "" ?>><?= $row->g_title ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                            <div id="map" style="height:300px"></div>
                                                            <?php foreach ($geofence as $row): ?>
                                                                <?php if ($row->g_id == ${$table_name}->g_id): ?>
                                                                    <label class="lat"><?= $row->g_lat ?></label>,
                                                                    <label class="lng"><?= $row->g_lng ?></label>,
                                                                    <label class="range"><?= $row->g_range ?></label>
                                                                    <input type='hidden' class='lat' name='g_lat' value="<?= $row->g_lat ?>">
                                                                    <input type='hidden' class='lng' name='g_lng' value="<?= $row->g_lng ?>">
                                                                    <input type='hidden' class='range' name='g_range' value="<?= $row->g_range ?>">
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group ">
                                                        <label class="control-label col-md-3"><?= $this->lang->line("圖片") ?>:</label>
                                                        <div class="col-md-6">
                                                            <?php
                                                            if (!${$table_name}->{"{$field_prefix}_image"}) {
                                                                $btn_remove = false;
                                                                $img_src = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no%20picture';
                                                                $fileinput_status = 'fileinput-new';
                                                            } else {
                                                                $btn_remove = true;
                                                                $img_src = base_url("upload/geofence") . "/" . ${$table_name}->{"{$field_prefix}_image"};
                                                                $fileinput_status = 'fileinput-exists';
                                                            }
                                                            ?>
                                                            <?php if ($btn_remove): ?>
                                                                <div class="remove_image_root">
                                                                    <label>
                                                                        <input class="remove_image" name="remove_image[<?= $field_prefix ?>_image]" type="checkbox" /> 刪除圖片
                                                                    </label>
                                                                </div>
                                                            <?php endif; ?>
                                                            <div class="fileinput <?= $fileinput_status ?>" data-provides="fileinput">
                                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                                                    <img name='imgsrc' src="<?= $img_src ?>" alt=""/>
                                                                </div>
                                                                <div>
                                                                    <span class="btn default btn-file">
                                                                        <span class="fileinput-new"> <?= $this->lang->line("選取圖片") ?> </span>
                                                                        <span class="fileinput-exists"> <?= $this->lang->line("更換") ?> </span>
                                                                        <input type="file" name="<?= $field_prefix ?>_image" accept="image/*">
                                                                    </span>
                                                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?= $this->lang->line("移除") ?> </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">開始時間:</label>
                                                        <div class="col-md-6">
                                                            <input class="form-control date-picker" name="<?= $field_prefix ?>_start_timestamp" type="text" value="<?= ${$table_name}->{"{$field_prefix}_start_timestamp"} ?>" />
                                                            <label class="label font-yellow-lemon"><i class="fa fa-warning"></i> 限輸入隔天之後的日期</label>
                                                            <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_start_timestamp">
                                                                <button class="close" data-close="alert"></button>
                                                                <span> 請輸入開始時間 </span> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">結束時間:</label>
                                                        <div class="col-md-6">
                                                            <input class="form-control date-picker" name="<?= $field_prefix ?>_end_timestamp" type="text" value="<?= ${$table_name}->{"{$field_prefix}_end_timestamp"} ?>" />
                                                            <div class="alert alert-danger display-hide validation" for="<?= $field_prefix ?>_end_timestamp">
                                                                <button class="close" data-close="alert"></button>
                                                                <span> 請輸入結束時間 </span> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">發送對象:</label>
                                                        <div class="col-md-6">
                                                            <select class="form-control" name="<?= $field_prefix ?>_target">
                                                                <option value="ALL"<?= (${$table_name}->{"{$field_prefix}_target"} == 'ALL') ? ' selected' : '' ?>>全部</option>
                                                                <option value="MEMBER"<?= (${$table_name}->{"{$field_prefix}_target"} == 'MEMBER') ? ' selected' : '' ?>>僅限會員</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">指定時間內推播:</label>
                                                        <div class="col-md-6">
                                                            <select class="form-control" name="<?= $field_prefix ?>_timelimit_push">
                                                                <option value="Y"<?= (${$table_name}->{"{$field_prefix}_timelimit_push"} == 'Y') ? ' selected' : '' ?>><?= $this->lang->line("是") ?></option>
                                                                <option value="N"<?= (${$table_name}->{"{$field_prefix}_timelimit_push"} == 'N') ? ' selected' : '' ?>><?= $this->lang->line("否") ?></option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">寄發通知信件:</label>
                                                        <div class="col-md-6">
                                                            <select class="form-control" name="<?= $field_prefix ?>_mail_enabled">
                                                                <option value="Y" <?= ${$table_name}->{"{$field_prefix}_mail_enabled"} == 'Y' ? 'selected' : '' ?>><?= $this->lang->line("是") ?></option>
                                                                <option value="N" <?= ${$table_name}->{"{$field_prefix}_mail_enabled"} == 'N' ? 'selected' : '' ?>><?= $this->lang->line("否") ?></option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">選擇主題:</label>
                                                        <div class="col-md-6">
                                                            <select class="form-control" name="<?= $field_prefix ?>_topic" id="TopicSelect">

                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">選擇推播前往地點類別:</label>
                                                        <div class="col-md-6">
                                                            <select class="form-control" name="<?= $field_prefix ?>_go_point_type" id="TopicSelectorType">
                                                                
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group" id="sub_selector_ar" style="display: none;">
                                                        <label class="control-label col-md-3">選擇前往主題AR點位:</label>
                                                        <div class="col-md-6" >
                                                            <select class="form-control" name="<?= $field_prefix ?>_point_selector" id="ARSelector">
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group" id="sub_selector_point" style="display: none;">
                                                        <label class="control-label col-md-3">選擇前往主體景點:</label>
                                                        <div class="col-md-6" >
                                                            <select class="form-control" name="<?= $field_prefix ?>_point_selector" id="PointSelector">
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <input type='hidden' id='gt_lat' name='gt_lat' value="<?= ${$table_name}->gt_lat ?>">
                                                    <input type='hidden' id='gt_lng' name='gt_lng' value="<?= ${$table_name}->gt_lng ?>">

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"><?= $this->lang->line("狀態") ?>:</label>
                                                        <div class="col-md-6">
                                                            <select class="form-control" name="<?= $field_prefix ?>_enabled">
                                                                <option value="Y"<?= (${$table_name}->{"{$field_prefix}_enabled"} == 'Y') ? ' selected' : '' ?>><?= $this->lang->line("啟用中") ?></option>
                                                                <option value="N"<?= (${$table_name}->{"{$field_prefix}_enabled"} == 'N') ? ' selected' : '' ?>><?= $this->lang->line("已關閉") ?></option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab_en">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">英文<?= $this->lang->line("標題") ?>:</label>
                                                        <div class="col-md-6">
                                                            <input type="text" name="<?= $field_prefix ?>_title_en" class="form-control" placeholder="英文<?= $this->lang->line("標題") ?>" value="<?= ${$table_name}->{"{$field_prefix}_title_en"} ?>" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">英文<?= $this->lang->line("內容") ?>:</label>
                                                        <div class="col-md-6">
                                                            <textarea class="form-control" name="<?= $field_prefix ?>_description_en" rows="6" placeholder="英文<?= $this->lang->line("內容") ?>"><?= ${$table_name}->{"{$field_prefix}_description_en"} ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab_jp">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">日文<?= $this->lang->line("標題") ?>:</label>
                                                        <div class="col-md-6">
                                                            <input type="text" name="<?= $field_prefix ?>_title_jp" class="form-control" placeholder="日文<?= $this->lang->line("標題") ?>" value="<?= ${$table_name}->{"{$field_prefix}_title_jp"} ?>" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">日文<?= $this->lang->line("內容") ?>:</label>
                                                        <div class="col-md-6">
                                                            <textarea class="form-control" name="<?= $field_prefix ?>_description_jp" rows="6" placeholder="日文<?= $this->lang->line("內容") ?>"><?= ${$table_name}->{"{$field_prefix}_description_jp"} ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <?php include APPPATH . "views/{$this->controller_name}/templates/footer_info.php";  //尾部資訊 ?>

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/select2/select2.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/media/js/jquery.dataTables.min.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") ?>"></script>
        <?php if ($this->session->i18n == 'zh') : ?>
            <script type="text/javascript" src="<?= base_url("assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js") ?>" charset="UTF-8"></script>
        <?php endif; ?>
        <!-- END PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?= base_url("assets/plugins/jquery-ui/jquery-ui.js") ?>"></script>
        <script type="text/javascript" src="<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.js") ?>"></script>
        <script type='text/javascript' src='<?= base_url("assets/plugins/jquery-ui-timepicker-addon/jquery-ui-sliderAccess.js") ?>'></script>
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url("assets/global/scripts/metronic.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/layout.js") ?>" type="text/javascript"></script>
        <script src="<?= base_url("assets/admin/layout/scripts/quick-sidebar.js") ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->

        <script src="<?= base_url("js/geolocation.js") ?>" type="text/javascript"></script>
         <script type="text/javascript" nonce="cm1vaw==">
            $(function () {

                $('#btn_submit').on('click', function () {
                    if (on_submit()) {
                        $('form').submit();
                    }
                });

                $('#form').submit(on_submit);

                function on_submit() {
                    var result = true;

                    $('.validation').each(function () {
                        var name = $(this).attr('for');

                        if (!$('*[name=' + name + ']').val()) {
                            $(this).show();
                            $('*[name=' + name + ']').focus();
                            event.preventDefault();
                            result = false;
                        } else {
                            $(this).hide();
                        }
                    });

                    return result;
                }
                $('.date-picker').datetimepicker({
                    dateFormat: 'yy-mm-dd',
                    timeFormat: 'HH:mm'
                });

                $('.remove_image').click(function () {
                    if ($(this).prop('checked')) {
                        $(this).parents('.remove_image_root').next().hide();
                        $(this).parents('.remove_image_root').next().next().hide();
                    } else {
                        $(this).parents('.remove_image_root').next().show();
                        $(this).parents('.remove_image_root').next().next().show();
                    }
                });
            });
        </script>
         <script nonce="cm1vaw==">
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar


                //Topic component
                find_topic();     //topic注入元素
                topic_Listener(); //topic監聽事件
                topic_selector_type_Listener();
            });

            $("#map").html(function () {
                var imgsrc = get_static_map_circle($('.lat').html(), $('.lng').html(), $('.range').html(), 350, 300, "<?= $this->config->item("google")["api_key"] ?>");
                return "<img src='" + imgsrc + "' >";
            });

            $('.selected').change(function () {
                var val = $(this).val();
<?php foreach ($geofence as $row): ?>
                    if (val ==<?= $row->g_id ?>) {
                        var lat =<?= $row->g_lat ?>;
                        var lng =<?= $row->g_lng ?>;
                        var range =<?= $row->g_range ?>;
                        $('.lat').html(lat);
                        $('.lng').html(lng);
                        $('.range').html(range);
                        $("#map").html(function () {
                            var imgsrc = get_static_map_circle(lat, lng, range, 350, 300, "<?= $this->config->item("google")["api_key"] ?>");
                            return "<img src='" + imgsrc + "' >";
                        });
                    }
<?php endforeach; ?>
            });

            function checkall(selector) {
                $(selector).prop('checked', true);
                $(selector).parent().addClass('checked');
            }
            function uncheck(selector) {
                $(selector).prop('checked', false);
                $(selector).parent().removeClass('checked');
            }

            function find_topic(){
                  var url_at = "<?=$this->config->item("server_base_url").'api/get_topic_all'?>"; 
                  //清空原有select内的数据
                  $("#TopicSelect").empty();
                  $.ajax({ 
                      url:url_at , 
                      dataType:"json",
                      success:function(data){
                          
                          $("#TopicSelect").append("<option value='-1'>--請選擇主題--</option>");
                          $.each(data.data, function (index, item) {  
                              var id = item.id; 
                              var text = item.t_name; 
                              var lat = item.t_lat; 
                              var lng = item.t_lng;

                              var old_tid = <?=${$table_name}->{"{$field_prefix}_topic"}?>;
                              var old_go_point_type = '<?=${$table_name}->{"{$field_prefix}_go_point_type"}?>';
                              var old_go_point_id = <?=${$table_name}->{"{$field_prefix}_go_point_id"}?>;
                              if(id==old_tid) {
                                 $("#TopicSelect").append("<option value='"+id+"' data-lat='"+lat+"' data-lng='"+lng+"' selected>"+text+"</option>");
                                 find_topic_info(old_tid,old_go_point_type,old_go_point_id);
                              }else{
                                 $("#TopicSelect").append("<option value='"+id+"' data-lat='"+lat+"' data-lng='"+lng+"'>"+text+"</option>");
                              }
                             
                          }); 
                      }, 
                  error:function(XMLHttpRequest,textStatus, errorThrown) { 
                      // alert(errorThrown);
                     }
                  });
            }
            function find_topic_info(topic_id,old_go_point_type="",old_go_point_id=""){
                  var url_at = "<?=$this->config->item("server_base_url").'api/get_topic_info?topic_id='?>"+topic_id; 
                  //清空原有select内的数据
                  $("#TopicSelectorType").empty();
                  $.ajax({ 
                      url:url_at , 
                      dataType:"json",
                      success:function(data){

                          var public_ar_count = data.data[0].public_ar_count;
                          var public_poi_count = data.data[0].public_poi_count;  

                          if(old_go_point_type){
                            $("#TopicSelectorType").append("<option value='topic' <?php echo (${$table_name}->{"{$field_prefix}_go_point_type"}=='topic')?'selected':'';?>>主題中心點</option>");
                            $("#TopicSelectorType").append("<option value='ar' <?php echo (${$table_name}->{"{$field_prefix}_go_point_type"}=='ar')?'selected':'';?>>主題AR點位("+public_ar_count+")</option>");
                            $("#TopicSelectorType").append("<option value='point' <?php echo (${$table_name}->{"{$field_prefix}_go_point_type"}=='point')?'selected':'';?>>主體景點("+public_poi_count+")</option>");
                            $("#TopicSelectorType").change();
                          }else{
                            $("#TopicSelectorType").append("<option value='topic'>主題中心點</option>");
                            $("#TopicSelectorType").append("<option value='ar'>主題AR點位("+public_ar_count+")</option>");
                            $("#TopicSelectorType").append("<option value='point'>主體景點("+public_poi_count+")</option>");
                          }
                          


                      }, 
                  error:function(XMLHttpRequest,textStatus, errorThrown) { 
                      // alert(errorThrown);
                     }
                  });
            } 
            function get_topic_id(){
                var topicID = $("#TopicSelect").val();
                return topicID;
            }
            function topic_Listener(){
                $("#TopicSelect").on('change',function(){

                    $("#TopicSelectorType").val("topic");

                    find_topic_info(get_topic_id());

                    var lat = $('option:selected',this).data('lat');
                    var lng = $('option:selected',this).data('lng');
                    setting_gt_lat_lng(lat,lng);

                    $("#sub_selector_ar").hide();
                    $("#sub_selector_point").hide();
                });
            }

            function get_topic_selector_type(){
                var TopicSelectorType = $("#TopicSelectorType").val();

                return TopicSelectorType;
            }
            function topic_selector_type_Listener(old_go_point_id=""){
                $("#TopicSelectorType").on('change',function(){

                    var topicID = get_topic_id();
                    var selector_type = get_topic_selector_type();
                    var old_go_point_id = <?=${$table_name}->{"{$field_prefix}_go_point_id"}?>;

                    //顯示AR選項
                    if(topicID!=-1 && selector_type=="ar"){

                        $("#sub_selector_ar").show();
                        $("#sub_selector_point").hide();
                        var url_at = "<?=$this->config->item("server_base_url").'api/get_index?type=ar&t_id='?>"+topicID; 
                        //清空原有select内的数据
                        $("#ARSelector").empty();
                        $.ajax({ 
                            url:url_at , 
                            dataType:"json",
                            success:function(data){
                                
                                $("#ARSelector").append("<option value='-1'>--請選擇AR點位--</option>");
                                $.each(data.data, function (index, item) {  
                                    var id = item.id; 
                                    var text = item.name; 
                                    var lat = item.lat; 
                                    var lng = item.lng; 
                                    if(old_go_point_id){
                                        if(id==old_go_point_id){
                                            $("#ARSelector").append("<option value='"+id+"' data-lat='"+lat+"' data-lng='"+lng+"' selected>"+text+"</option>");
                                        }else{
                                            $("#ARSelector").append("<option value='"+id+"' data-lat='"+lat+"' data-lng='"+lng+"' >"+text+"</option>");
                                        }
                                        
                                    }else{
                                        $("#ARSelector").append("<option value='"+id+"' data-lat='"+lat+"' data-lng='"+lng+"' >"+text+"</option>");
                                    }
                                    
                                }); 

                                ar_selector_Listener();
                            }, 
                        error:function(XMLHttpRequest,textStatus, errorThrown) { 
                            // alert(errorThrown);
                           }
                        });

                    }else if(topicID!=-1 && selector_type=="point"){

                        $("#sub_selector_point").show();
                        $("#sub_selector_ar").hide();
                        var url_at = "<?=$this->config->item("server_base_url").'api/get_index?type=poi&t_id='?>"+topicID; 
                        //清空原有select内的数据
                        $("#PointSelector").empty();
                        $.ajax({ 
                            url:url_at , 
                            dataType:"json",
                            success:function(data){
                                
                                $("#PointSelector").append("<option value='-1'>--請選擇主題景點--</option>");
                                $.each(data.data, function (index, item) {  
                                    var id = item.id; 
                                    var text = item.name; 
                                    var lat = item.lat; 
                                    var lng = item.lng;

                                    if(old_go_point_id){
                                        if(id==old_go_point_id){
                                            $("#PointSelector").append("<option value='"+id+"' data-lat='"+lat+"' data-lng='"+lng+"' selected>"+text+"</option>");
                                        }else{
                                            $("#PointSelector").append("<option value='"+id+"' data-lat='"+lat+"' data-lng='"+lng+"' >"+text+"</option>");
                                        }
                                        
                                    }else{
                                        $("#PointSelector").append("<option value='"+id+"' data-lat='"+lat+"' data-lng='"+lng+"' >"+text+"</option>");
                                    }
                                }); 

                                point_selector_Listener();
                            }, 
                        error:function(XMLHttpRequest,textStatus, errorThrown) { 
                            // alert(errorThrown);
                           }
                        });
                    }else{
                        $("#sub_selector_ar").hide();
                        $("#sub_selector_point").hide();
                    }



                    //顯示景點選項
                });
            }
            function ar_selector_Listener(){
                $("#ARSelector").on('change',function(){
                    var lat = $('option:selected',this).data('lat');
                    var lng = $('option:selected',this).data('lng');
                    setting_gt_lat_lng(lat,lng);
                });
            }
            function point_selector_Listener(){
                $("#PointSelector").on('change',function(){
                    var lat = $('option:selected',this).data('lat');
                    var lng = $('option:selected',this).data('lng');
                    setting_gt_lat_lng(lat,lng);
                });
            }
            function setting_gt_lat_lng(lat,lng){

                $("#gt_lat").val(lat);
                $("#gt_lng").val(lng);
            }

            $(document).on('click', '[name=remove]', function () {
                $(this).parent().remove();
            });
            $(document).on('click', '[name=newremove]', function () {
                var count = $('[name=beacon]').length;
                if (count > 1) {
                    $(this).parent().remove();
                }
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
