<?php

date_default_timezone_set('Asia/Taipei');


header('Content-type: text/html; charset=utf-8');

$data["menu_name"] = $this->lang->line("登入");

//網址登入
$account = $this->input->get('account');
$password = $this->input->get('password');

//登入驗證
$adm = $this->login($account, $password, 'sso');

if (!$adm) {
    _alert_redirect("無系統功能權限，請洽系統管理員\\r數位創新中心-張先生 #51826", "https://login.gov.taipei/");
    exit();
}

$this->session->set_userdata("login_admin", $adm);


//紀錄log
$this->adm_log($page, $data["menu_name"], 'LOGIN', $adm->adm_id, '');

header("Location: " . base_url("{$this->controller_name}/home"));

exit();