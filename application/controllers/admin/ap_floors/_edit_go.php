<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = $this->lang->line("編輯");

//傳入值接收
$input_array = array(
    'ab_id' => $this->input->post('ab_id'),
    "{$data["field_prefix"]}_id" => $this->input->post("{$data["field_prefix"]}_id"),
    "{$data["field_prefix"]}_number" => $this->input->post("{$data["field_prefix"]}_number"),
    "{$data["field_prefix"]}_name" => $this->input->post("{$data["field_prefix"]}_name"),
    "{$data["field_prefix"]}_order" => $this->input->post("{$data["field_prefix"]}_order"),
    "{$data["field_prefix"]}_map" => $this->input->post("{$data["field_prefix"]}_map"),
    "{$data["field_prefix"]}_enabled" => $this->input->post("{$data["field_prefix"]}_enabled")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($index . $value . $this->lang->line("參數錯誤"));
        exit();
    }
}

$input_array["{$data["field_prefix"]}_desc"] = $this->input->post("{$data["field_prefix"]}_desc");
$input_array["{$data["field_prefix"]}_plan_id"] = $this->input->post("{$data["field_prefix"]}_plan_id");
$input_array["{$data["field_prefix"]}_bottom_left_lat"] = $this->input->post("{$data["field_prefix"]}_bottom_left_lat");
$input_array["{$data["field_prefix"]}_bottom_left_lng"] = $this->input->post("{$data["field_prefix"]}_bottom_left_lng");
$input_array["{$data["field_prefix"]}_top_right_lat"] = $this->input->post("{$data["field_prefix"]}_top_right_lat");
$input_array["{$data["field_prefix"]}_top_right_lng"] = $this->input->post("{$data["field_prefix"]}_top_right_lng");

//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one($data['table_name'], $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), $data["item_name"]));
    exit();
}

$update_array = array(
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "{$data["field_prefix"]}_desc" => $input_array["{$data["field_prefix"]}_desc"],
    "{$data["field_prefix"]}_number" => $input_array["{$data["field_prefix"]}_number"],
    "{$data["field_prefix"]}_plan_id" => $input_array["{$data["field_prefix"]}_plan_id"],
    "{$data["field_prefix"]}_order" => $input_array["{$data["field_prefix"]}_order"],
    "{$data["field_prefix"]}_map" => $input_array["{$data["field_prefix"]}_map"],
    "{$data["field_prefix"]}_bottom_left_lat" => $input_array["{$data["field_prefix"]}_bottom_left_lat"],
    "{$data["field_prefix"]}_bottom_left_lng" => $input_array["{$data["field_prefix"]}_bottom_left_lng"],
    "{$data["field_prefix"]}_top_right_lat" => $input_array["{$data["field_prefix"]}_top_right_lat"],
    "{$data["field_prefix"]}_top_right_lng" => $input_array["{$data["field_prefix"]}_top_right_lng"],
    "{$data["field_prefix"]}_enabled" => $input_array["{$data["field_prefix"]}_enabled"]
);
/*
//不更新Anyplace，會將底圖經緯度蓋掉
//更新Anyplace DB
$query_info = array(
    'buid' => $input_array['ab_buid'],
    'description' => $input_array["{$data["field_prefix"]}_desc"],
    'floor_name' => $input_array["{$data["field_prefix"]}_name"],
    'floor_number' => $input_array["{$data["field_prefix"]}_number"],
    'bottom_left_lat' => $data[$data['table_name']]->af_bottom_left_lat,
    'bottom_left_lng' => $data[$data['table_name']]->af_bottom_left_lng,
    'top_right_lat' => $data[$data['table_name']]->af_top_right_lat,
    'top_right_lng' => $data[$data['table_name']]->af_top_right_lng
);

//載入helper
$this->load->helper('anyplace_helper');
$result = get_mapping_result('update', 'floor', $query_info);

if ($result['status_code'] != 200) {
    _alert('更新失敗 [' . $result['status_code'] . ':' . $result['message'] . ']。\n\n請再試一次，或洽系統管理員。');
    exit();
}
*/
$this->Common_model->update_db($data['table_name'], $update_array, $where_array);

_alert_redirect('更新成功！', base_url("{$this->controller_name}/{$data['table_name']}") . "?ab_id=" . $this->input->get("ab_id"));
exit();
