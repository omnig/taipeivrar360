<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = '新增樓層';

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/add")
    )
);

$this->load->model("Common_model");
$data["ab_id"] = $this->input->get("ab_id");
$data["ab_data"] = $this->Common_model->get_one('ap_building', array("ab_id" => $data["ab_id"]));
if (!isset($data["ab_data"])) {
    _alert('錯誤的建物ID，請確認網址及ab_id');
    exit();
}

$data["ab_lat"] = $data["ab_data"]->ab_lat;
$data["ab_lng"] = $data["ab_data"]->ab_lng;