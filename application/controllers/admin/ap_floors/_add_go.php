<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = "新增";

//傳入值接收
$input_array = array(
    'ab_id' => $this->input->post('ab_id'),
    "{$data["field_prefix"]}_number" => $this->input->post("{$data["field_prefix"]}_number"),
    "{$data["field_prefix"]}_name" => $this->input->post("{$data["field_prefix"]}_name"),
    "{$data["field_prefix"]}_order" => $this->input->post("{$data["field_prefix"]}_order"),
    "{$data["field_prefix"]}_map" => $this->input->post("{$data["field_prefix"]}_map"),
    "{$data["field_prefix"]}_enabled" => $this->input->post("{$data["field_prefix"]}_enabled")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($index . $value . $this->lang->line("參數錯誤"));
        exit();
    }
}

$input_array["{$data["field_prefix"]}_desc"] = $this->input->post("{$data["field_prefix"]}_desc");
$input_array["{$data["field_prefix"]}_plan_id"] = $this->input->post("{$data["field_prefix"]}_plan_id");
$input_array["{$data["field_prefix"]}_bottom_left_lat"] = $this->input->post("{$data["field_prefix"]}_bottom_left_lat");
$input_array["{$data["field_prefix"]}_bottom_left_lng"] = $this->input->post("{$data["field_prefix"]}_bottom_left_lng");
$input_array["{$data["field_prefix"]}_top_right_lat"] = $this->input->post("{$data["field_prefix"]}_top_right_lat");
$input_array["{$data["field_prefix"]}_top_right_lng"] = $this->input->post("{$data["field_prefix"]}_top_right_lng");

//檢查樓層是否重覆
$where_array = array(
    'ab_id' => $input_array['ab_id'],
    'af_number' => $input_array["{$data["field_prefix"]}_number"]
);
$floor = $this->Common_model->get_one($data['table_name'], $where_array);

if ($floor) {
    _alert('重覆的樓層數字。');
    exit();
}


//取得建物ID
$where_array = array(
    'ab_id' => $input_array['ab_id']
);
$buid = $this->Common_model->get_one_field('ap_building', 'ab_buid', $where_array);

if (!$buid) {
    _alert('無法取得建物。');
    exit();
}

$fuid = $buid . '_' . $input_array["{$data["field_prefix"]}_number"];

$insert_array = array(
    'ab_id' => $input_array['ab_id'],
    "{$data["field_prefix"]}_fuid" => $fuid,
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "{$data["field_prefix"]}_desc" => $input_array["{$data["field_prefix"]}_desc"],
    "{$data["field_prefix"]}_number" => $input_array["{$data["field_prefix"]}_number"],
    "{$data["field_prefix"]}_plan_id" => $input_array["{$data["field_prefix"]}_plan_id"],
    "{$data["field_prefix"]}_order" => $input_array["{$data["field_prefix"]}_order"],
    "{$data["field_prefix"]}_map" => $input_array["{$data["field_prefix"]}_map"],
    "{$data["field_prefix"]}_bottom_left_lat" => $input_array["{$data["field_prefix"]}_bottom_left_lat"],
    "{$data["field_prefix"]}_bottom_left_lng" => $input_array["{$data["field_prefix"]}_bottom_left_lng"],
    "{$data["field_prefix"]}_top_right_lat" => $input_array["{$data["field_prefix"]}_top_right_lat"],
    "{$data["field_prefix"]}_top_right_lng" => $input_array["{$data["field_prefix"]}_top_right_lng"],
    "{$data["field_prefix"]}_enabled" => $input_array["{$data["field_prefix"]}_enabled"],
    "{$data["field_prefix"]}_create_timestamp" => date('Y-m-d H:i:s')
);


//新增Anyplace DB
//$query_info = array(
//    'buid' => $buid,
//    'is_published' => 'false',
//    'description' => $input_array["{$data["field_prefix"]}_desc"],
//    'floor_name' => $input_array["{$data["field_prefix"]}_name"],
//    'floor_number' => $input_array["{$data["field_prefix"]}_number"]
//);
//
////載入helper
//$this->load->helper('anyplace_helper');
//$result = get_mapping_result('add', 'floor', $query_info);
//
//if ($result['status_code'] !== 200) {
//    _alert('新增失敗 [' . $result['status_code'] . ':' . $result['message'] . ']。\n\n請再試一次，或洽系統管理員。');
//    exit();
//}

$this->Common_model->insert_db($data['table_name'], $insert_array);

_alert_redirect($this->lang->line("新增成功！"), base_url("{$this->controller_name}/{$data['table_name']}" . '?ab_id=' . $input_array['ab_id']));
exit();
