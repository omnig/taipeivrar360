<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = $this->lang->line("刪除");

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->get("{$data["field_prefix"]}_id")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data["table_name"]] = $this->Common_model->get_one($data["table_name"], $where_array);

if (!$data[$data["table_name"]]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), $data["item_name"]));
    exit();
}

//刪除(僅紀錄，不真的刪除)
$update_array = array(
    "{$data["field_prefix"]}_del" => "Y"
);
$this->Common_model->update_db($data["table_name"], $update_array, $where_array);


if ($data[$data["table_name"]]->g_mail_enabled == 'Y') {
    //寄發通知信
    $this->load->library('sendmail_lib');

    $data_array = array(
        'g_title' => $data[$data["table_name"]]->g_title,
        'g_lat' => $data[$data["table_name"]]->g_lat,
        'g_lng' => $data[$data["table_name"]]->g_lng,
        'g_range' => $data[$data["table_name"]]->g_range,
        'g_enabled' => $data[$data["table_name"]]->g_enabled
    );
    $data_array['id'] = $input_array["{$data["field_prefix"]}_id"];
    $data_array['title'] = $data["menu_name"];
    $data_array['act_title'] = '刪除';
    $data_array['sa_account'] = $this->login_sa->sa_account;

    //準備寄發通知信，告知系統管理者
    $mail_title = "【後台】" . $data_array['act_title'] . $data_array['title'] . "通知信_" . date('Y-m-d');
    $mail_body = $this->load->view("mail_content/sa_lbs_notice", $data_array, true);

    $this->sendmail_lib->set_sender($this->config->item("og_pm_mail"), "VRAR360管理後台系統通知");
    $this->sendmail_lib->set_content($mail_title, $mail_body);

    $this->sendmail_lib->set_receiver($this->config->item("og_pm_mail"), '系統管理員');
    $this->sendmail_lib->send_mail();
}


_alert_redirect($this->lang->line('刪除成功！'), base_url("{$this->controller_name}/{$data["table_name"]}"));
exit();
