<?php
header('Content-Type: application/json; charset=utf-8');

$ar_id = $_SESSION["dai_id"];
$ar_data = $_SESSION["ar_image_data"];

$data = array(
	"dai_id" => $ar_id,
	"ar_image_data" => $ar_data
);


exit(json_encode($data,JSON_PARTIAL_OUTPUT_ON_ERROR));