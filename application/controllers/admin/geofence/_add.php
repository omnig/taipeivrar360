<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("新增");

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/add")
    )
);

//取得園區
$data['config'] = $this->Common_model->get_db('config');
$data["default_lat"] = $this->site_config->c_app_lat;
$data["default_lng"] = $this->site_config->c_app_lng;

$data['category_list'] = $this->Common_model->get_db('ap_category', ['ac_enabled' => 'Y']);
// $data['ar_list'] = $this->Common_model->get_db('department_ar', ['tdar_state' => 'Y', 'tdar_del' => 'N']);
// $data["department_list"] = $this->Common_model->get_db("tp_admin_group", []);

$adm_group =  $this->session->login_admin->adm_group;

$where_array = array(
    'ag_group' => $adm_group
);
$data["department_list"] = $this->Common_model->get_db("tp_admin_group", $where_array);

$d_id = $data["department_list"][0]->ag_id;
$isEditor =  $this->session->login_admin->adm_is_editor === 'Y';
$where_array = [
    'tdar_state' => 'Y',
    'tdar_del' => 'N',
    "d_id = $d_id"
];
if($isEditor){
    $group_id = $this->session->login_admin->ag_id;
    $where_array['d_id'] = $group_id;
    // $where_array['editor'] = $this->session->login_admin->adm_id;
}
$dept_ar_list = $this->Common_model->get_db('department_ar', $where_array);

$where_array = [
    'tdar_state' => 'Y',
    'tdar_del' => 'N',
    "d_id != $d_id",
    "share_other = 'Y'"
];
$shared_ar_list = $this->Common_model->get_db('department_ar', $where_array);
$data['ar_list'] = array_merge($dept_ar_list, $shared_ar_list);
