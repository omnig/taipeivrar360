<?php

include_once "__config.php";

$model_name = ucfirst($data['table_name']) . '_model';
$this->load->model($model_name);

//傳入值接收
$input_array = array(
    "limit" => $this->input->post('length'),
    "skip" => $this->input->post('start'),
    "draw" => intval($this->input->get_post('draw')),
    "columns" => $this->input->post('columns'),
    "order" => $this->input->post('order'),
    "filters" => $this->input->post('filters'),
    "customActionType" => $this->input->post('customActionType'),
    "customActionName" => $this->input->post('customActionName'),
    "id" => $this->input->post('id')
);

//多筆資料管理
if ($input_array["customActionType"] == "group_action" && $input_array['id']) {
    $where_array = array(
        "{$data["field_prefix"]}_id" => $input_array['id']
    );

    switch ($input_array['customActionName']) {
        case 'enable' : //啟用
            $update_array = array(
                "{$data["field_prefix"]}_enabled" => 'Y'
            );
            $this->Common_model->update_db($data["table_name"], $update_array, $where_array);
            break;
        case 'disable' :    //停用
            $update_array = array(
                "{$data["field_prefix"]}_enabled" => 'N'
            );
            $this->Common_model->update_db($data["table_name"], $update_array, $where_array);
            break;
        case 'del' :    //刪除
            $this->Common_model->delete_db($data["table_name"], $where_array);
            $this->Common_model->delete_db("ap_floors", $where_array);
            break;
    }

    $records["customActionStatus"] = "OK";
    $records["customActionMessage"] = "變更完成!";
}

//資料分頁設定
$input_array['limit'] = intval($input_array['limit']) < 0 ? 0 : intval($input_array['limit']);
$input_array['skip'] = intval($this->input->get_post('start'));

if($this->input->get("af_id") !== null){
    $where_array = array(
        "ap.af_id" => $this->input->get("af_id")
    );
}else{
    $where_array = array();
}

$adm_group =  $this->session->login_admin->adm_group;
$isEditor =  $this->session->login_admin->adm_is_editor === 'Y';

$department = $this->Common_model->get_one("admin_group", ['ag_group' => $adm_group]);

$where_array['ap.d_id'] = $department->ag_id;

if($isEditor){
    $group_id = $this->session->login_admin->ag_id;
    $where_array['ap.d_id'] = $group_id;
    // $where_array['ab.editor'] = $this->session->login_admin->adm_id;
}


//依傳入參數過濾
if ($input_array['filters']) {
    $concat_where_string = "";

    foreach ($input_array['filters'] as $field_name => $value) {
        if ($value == '') {
            continue;
        }

        $where_string = "";

        switch ($field_name) {
            //比對完全相同的值
            case "{$data["field_prefix"]}_order" :
                $where_string = "({$field_name} = '{$value}')";
                break;
            //全文搜尋
            case "{$data["field_prefix"]}_name" :
            case "{$data["field_prefix"]}_desc" :
            case "{$data["field_prefix"]}_enabled" :
                $where_string = "({$field_name} LIKE '%{$value}%')";
                break;

            //其它特殊搜尋遇濾方式
            case 'create_from' :
                $where_string = "({$data["field_prefix"]}_create_timestamp >= '{$value} 00:00:00')";
                break;
            case 'create_to' :
                $where_string = "({$data["field_prefix"]}_create_timestamp <= '{$value} 23:59:59')";
                break;
            case 'ac_id' :
                $where_string = "(ap.ac_id = '$value')";
                break;
            case 'd_id' :
                $where_string = "(ap.d_id = '$value')";
                break;
            default :
                $where_string = "({$field_name} = '$value')";
                break;
        }

        if ($concat_where_string == '') {
            $concat_where_string = $where_string;
        } else {
            $concat_where_string = "$concat_where_string AND $where_string";
        }
    }

    if ($concat_where_string != '') {
        $where_array[] = $concat_where_string;
    }
}

//排序欄位，對映order的排序欄位
$sort_fields = array(
    false, false, "{$data["field_prefix"]}_name", "{$data["field_prefix"]}_desc", false, "{$data["field_prefix"]}_enabled", false
);

$order_by = '';
foreach ($input_array['order'] as $row) {
    if ($sort_fields[$row['column']]) {
        if ($order_by == '') {
            $order_by = "{$sort_fields[$row['column']]} {$row['dir']}";
            continue;
        }

        $order_by = "{$order},{$sort_fields[$row['column']]} {$sort_fields[$row['dir']]}";
    }
}

//計算資料總數
$iTotalRecords = $this->$model_name->get_list($order_by, '', 0, 0, $where_array, true);

//從資料庫取得本次資料
$ret = $this->$model_name->get_list($order_by, '', $input_array['limit'], $input_array['skip'], $where_array, false);

//回傳值
$records = array();
$records["data"] = array();

$status_list = array(
    'Y' => array("success" => $this->lang->line("啟用中")),
    'N' => array("default" => $this->lang->line("已關閉"))
);

$edit_url = base_url("{$this->controller_name}/{$data['table_name']}/edit");
$del_url = base_url("{$this->controller_name}/{$data['table_name']}/del");



//html表格內容
foreach ($ret as $i => $row) {
    $is_enabled = ($row->{"{$data["field_prefix"]}_enabled"} == 'Y') ? 'Y' : 'N';
    $status = $status_list[$is_enabled];
    $id = $row->{"{$data["field_prefix"]}_id"};
    $ap_floors_url = base_url("{$this->controller_name}/ap_floors") . "?{$data["field_prefix"]}_id={$row->{"{$data["field_prefix"]}_id"}}";
//    $update_floor_api = base_url("cli/ap_update_floor") . "?buid={$row->{"{$data["field_prefix"]}_buid"}}";
    $get_floors_api = base_url("locapi/get_floor") . "?b={$row->{"{$data["field_prefix"]}_id"}}";
    $img_url = base_url("upload_image/" . $row->{"{$data["field_prefix"]}_image_path"});
    $img_id = preg_replace("/\./", "", basename($img_url));
    switch($row->{"{$data["field_prefix"]}_ar_active_method"}){
        case '0':
            $active_str = '靠近自動呈現';
            break;
        case '1':
            $active_str = '點擊呈現';
            break;
        case '2':
            $active_str = '圖像辨識';
            break;
        default :
            $active_str = '';
            break;
    }
    $records["data"] [] = array(
        '<input type="checkbox" name="list_checkbox[]" value="' . $id . '">',
        $i + 1,
        $row->{"{$data["field_prefix"]}_name"},
        $row->ac_title_zh,
        $row->ag_name,
        $row->{"{$data["field_prefix"]}_desc"},
        "<img class='img-responsive hide' id='{$img_id}' src='{$img_url}' /><span class='img_span' id='{$img_id}_span'>點擊顯示圖片</span>",
        $row->tdar_name,
        $active_str,
        '<span class="label label-sm label-' . (key($status)) . '">' . (current($status)) . '</span>',
        "<a href='{$edit_url}?af_id={$row->af_id}&{$data["field_prefix"]}_id={$row->{"{$data["field_prefix"]}_id"}}' class='btn btn-xs default blue-stripe btn-operating'><i class='fa fa-pencil'></i> {$this->lang->line("編輯")}</a>"
        ."<a class='btn btn-xs default red-stripe btn-operating delete' data='{$row->{"{$data["field_prefix"]}_id"}}'><i class='fa fa-remove'></i> {$this->lang->line("刪除")}</a>"
    );
}

$records["draw"] = $input_array["draw"];
$records["recordsTotal"] = $iTotalRecords;
$records["recordsFiltered"] = $iTotalRecords;

echo json_encode($records);
exit();
