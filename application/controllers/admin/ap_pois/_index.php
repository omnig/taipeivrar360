<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line($data["menu_name"]);

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"] . ' - POI管理',
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    )
);

$data['category_list'] = $this->Common_model->get_db('ap_category', ['ac_enabled' => 'Y']);
$data["department_list"] = $this->Common_model->get_db("tp_admin_group", []);

$data["del_url"] = base_url("{$this->controller_name}/{$data['table_name']}/del");
