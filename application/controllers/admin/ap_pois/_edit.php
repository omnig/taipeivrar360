<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("編輯") . 'POI';

//取得資料
if($this->input->get('af_id')!==null){
    $where_array = array(
        'af_id' => $this->input->get('af_id')
    );
    $data['floor'] = $this->Common_model->get_one('ap_floors as af', $where_array);

    if (!isset($data['floor'])) {
        _alert('樓層不存在');
        exit();
    }
}

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/edit?{$data["field_prefix"]}_id={$this->input->get("{$data["field_prefix"]}_id")}")
    )
);

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->get("{$data["field_prefix"]}_id")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one($data['table_name'], $where_array);
$data[$data['table_name']]->b_id = null;
if($beacon = $this->Common_model->get_one('beacon_poi_relation', $where_array)){
    $data[$data['table_name']]->b_id = $beacon->b_id;
}

if(!is_null($data[$data['table_name']]->dai_id)){
    $data["ar_image"] = $this->Common_model->get_one('department_ar_image', ["dai_id" => $data[$data['table_name']]->dai_id]);
}else{
    $data["ar_image"] = null;
}

$data['category_list'] = $this->Common_model->get_db('ap_category', ['ac_enabled' => 'Y']);

$adm_group =  $this->session->login_admin->adm_group;

$where_array = array(
    'ag_group' => $adm_group
);
$data["department_list"] = $this->Common_model->get_db("admin_group", $where_array);

$d_id = $data["department_list"][0]->ag_id;
$isEditor =  $this->session->login_admin->adm_is_editor === 'Y';
$where_array = [
    'tdar_state' => 'Y',
    'tdar_del' => 'N',
    "d_id = $d_id"
];
if($isEditor){
    $group_id = $this->session->login_admin->ag_id;
    $where_array['d_id'] = $group_id;
    // $where_array['editor'] = $this->session->login_admin->adm_id;
}
$dept_ar_list = $this->Common_model->get_db('department_ar', $where_array);

$where_array = [
    'tdar_state' => 'Y',
    'tdar_del' => 'N',
    "d_id != $d_id",
    "share_other = 'Y'"
];
$shared_ar_list = $this->Common_model->get_db('department_ar', $where_array);
$data['ar_list'] = array_merge($dept_ar_list, $shared_ar_list);

$data["default_lat"] = $this->site_config->c_app_lat;
$data["default_lng"] = $this->site_config->c_app_lng;

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("%s不存在，請先設定%s"), $data["item_group_name"], $data["item_group_name"]));
    exit();
}

$model_name = 'Beacon_model';
$this->load->model($model_name);
$data['beacon_list'] = $this->Beacon_model->get_unbinded_beacons();
