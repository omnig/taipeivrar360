<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = $this->lang->line("編輯");

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->post("{$data["field_prefix"]}_id"),
    "{$data["field_prefix"]}_name" => $this->input->post("{$data["field_prefix"]}_name"),
    "{$data["field_prefix"]}_desc" => $this->input->post("{$data["field_prefix"]}_desc"),
    "{$data["field_prefix"]}_lat" => $this->input->post("{$data["field_prefix"]}_lat"),
    "{$data["field_prefix"]}_lng" => $this->input->post("{$data["field_prefix"]}_lng"),
    "{$data["field_prefix"]}_enabled" => $this->input->post("{$data["field_prefix"]}_enabled"),
    "ac_id" => $this->input->post("ac_id"),
    "af_id" => ($this->input->post("af_id") !== null) ? $this->input->post("af_id") : 1, //沒af_id的話直接讓POI歸屬室外建物
);

foreach ($input_array as $index => $value) {
    if (empty($value)) {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one($data['table_name'], $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), $data["item_name"]));
    exit();
}

$update_array = array(
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "{$data["field_prefix"]}_desc" => $input_array["{$data["field_prefix"]}_desc"],
    "{$data["field_prefix"]}_lat" => $input_array["{$data["field_prefix"]}_lat"],
    "{$data["field_prefix"]}_lng" => $input_array["{$data["field_prefix"]}_lng"],
    "{$data["field_prefix"]}_enabled" => $input_array["{$data["field_prefix"]}_enabled"],
    "ac_id" => $input_array["ac_id"],
    "{$data["field_prefix"]}_hyperlink_text" => $this->input->post("{$data["field_prefix"]}_hyperlink_text"),
    "{$data["field_prefix"]}_hyperlink_url" => $this->input->post("{$data["field_prefix"]}_hyperlink_url"),
    "{$data["field_prefix"]}_video_hyperlink" => $this->input->post("{$data["field_prefix"]}_video_hyperlink"),
    "tdar_id" => $this->input->post("tdar_id"),
    "{$data["field_prefix"]}_ar_active_method" => $this->input->post("{$data["field_prefix"]}_ar_active_method"),
    "dai_id" => $this->input->post("dai_id"),
    "{$data["field_prefix"]}_ar_text" => $this->input->post("{$data["field_prefix"]}_ar_text"),
    "{$data["field_prefix"]}_ar_url" => $this->input->post("{$data["field_prefix"]}_ar_url"),
    "{$data["field_prefix"]}_ar_distance" => $this->input->post("{$data["field_prefix"]}_ar_distance"),
    "{$data["field_prefix"]}_ar_heigh" => $this->input->post("{$data["field_prefix"]}_ar_heigh"),
    "{$data["field_prefix"]}_cover_identify_image" => $this->input->post("{$data["field_prefix"]}_cover_identify_image"),
    "{$data["field_prefix"]}_ar_vsize" => $this->input->post("{$data["field_prefix"]}_ar_vsize"),
);

//{$data["field_prefix"]}_image_path
// //上傳圖片處理
$upload_path = "upload_image/";
$file_type = 'jpg|jpeg|png';
$max_size = 30480; //30M


//圖片上傳檔案檢查
if (isset($_FILES["{$data["field_prefix"]}_image_path"]) && !empty($_FILES["{$data["field_prefix"]}_image_path"]["name"])) {
    //上傳代表圖片
    $field_name = "{$data["field_prefix"]}_image_path";
    $upload_result = $this->upload_lib->write_from_image($upload_path, $field_name, $file_type, $max_size);
    
    if (!isset($upload_result["upload_data"])) {
        //必須上傳圖片
        echo ($this->lang->line("圖片上傳失敗或沒有上傳圖片"));
        exit();
    }else{
        //上傳成功，記錄上傳檔名
        $update_array["{$data["field_prefix"]}_image_path"] = $upload_result["upload_data"]["file_name"];
    }
}



if (isset($_FILES["{$data["field_prefix"]}_audio_path"]) && !empty($_FILES["{$data["field_prefix"]}_audio_path"]["name"])) {
    //{$data["field_prefix"]}_audio_path
    // //上傳語音檔處理
    $upload_path = "upload/audio";
    $file_type = 'mp3';
    $max_size = 8192;
    
    //上傳語音檔
    $field_name = "{$data["field_prefix"]}_audio_path";
    $upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size);
    if (!isset($upload_result["upload_data"])) {
        //必須上傳影音檔
        echo ($this->lang->line("影音檔上傳失敗"));
        exit();
    }else{
        //上傳成功，記錄上傳檔名
        $update_array["{$data["field_prefix"]}_audio_path"] = $upload_result["upload_data"]["file_name"];
    }
}else{
    $update_array["{$data["field_prefix"]}_audio_path"] = null;
}

//更新Anyplace DB，後續要改成poi的邏輯而不是building的
//$query_info = array(
//    'buid' => $data[$data['table_name']]->{$data["field_prefix"]."_buid"},
//    'is_published' => 'false',
//    'name' => $input_array["{$data["field_prefix"]}_name"],
//    'description' => $input_array["{$data["field_prefix"]}_desc"],
//    'url' => '-',
//    'address' => '-',
//    'coordinates_lat' => $input_array["{$data["field_prefix"]}_lat"],
//    'coordinates_lon' => $input_array["{$data["field_prefix"]}_lng"]
//);

// //載入helper
// $this->load->helper('anyplace_helper');
// $result = get_mapping_result('update', 'building', $query_info);

// if ($result['status_code'] != 200) {
//     _alert('更新失敗 [' . $result['status_code'] . ':' . $result['message'] . ']。\n\n請再試一次，或洽系統管理員。');
//     exit();
// }

$this->Common_model->trans_start();
$this->Common_model->update_db($data['table_name'], $update_array, $where_array);

$this->Common_model->delete_db('beacon_poi_relation', $where_array);
if(!empty($this->input->post("b_id"))){
    $this->Common_model->insert_db('beacon_poi_relation', ['ap_id' => $input_array["ap_id"], 'b_id' => $this->input->post("b_id")]);
}
$this->Common_model->trans_complete();

if($this->input->post("af_id") !== null){
    _alert_redirect($this->lang->line("更新成功！"), base_url("{$this->controller_name}/{$data['table_name']}?af_id={$this->input->post('af_id')}"));
    exit();
}
_alert_redirect('更新成功！', base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
