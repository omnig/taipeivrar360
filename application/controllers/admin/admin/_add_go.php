<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("新增");

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_name" => $this->input->post("{$data["field_prefix"]}_name"),
    // "{$data["field_prefix"]}_group" => $this->input->post("{$data["field_prefix"]}_group"),
    "{$data["field_prefix"]}_account" => $this->input->post("{$data["field_prefix"]}_account"),
    "{$data["field_prefix"]}_enabled" => $this->input->post("{$data["field_prefix"]}_enabled")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

$input_array["{$data["field_prefix"]}_email"] = $this->input->post("{$data["field_prefix"]}_email");
$input_array["{$data["field_prefix"]}_password"] = $this->input->post("{$data["field_prefix"]}_password") ? $this->input->post("{$data["field_prefix"]}_password") : $input_array["{$data["field_prefix"]}_account"];

//確認有沒有重複的帳號
$where_array = array(
    "{$data["field_prefix"]}_account" => $input_array["{$data["field_prefix"]}_account"]
);

$duplicated = $this->Common_model->get_one($data['table_name'], $where_array);
if ($duplicated) {
    _alert("{$this->lang->line("重複的")}{$data["item_name"]}");
    exit();
}

//確認email格式
$this->load->helper('email');
if (!valid_email($input_array["{$data["field_prefix"]}_email"])) {
    _alert($this->lang->line("Email格式錯誤"));
    exit();
}

$adm_group =  $this->session->login_admin->adm_group;

$insert_array = array(
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "{$data["field_prefix"]}_group" => $adm_group,
    "{$data["field_prefix"]}_account" => $input_array["{$data["field_prefix"]}_account"],
    "{$data["field_prefix"]}_email" => $input_array["{$data["field_prefix"]}_email"],
    "{$data["field_prefix"]}_enabled" => $input_array["{$data["field_prefix"]}_enabled"],
    "{$data["field_prefix"]}_password" => $this->encrypt_password($input_array["{$data["field_prefix"]}_password"]),
    "{$data["field_prefix"]}_is_editor" => "Y"
);

$this->Common_model->insert_db($data['table_name'], $insert_array, $where_array);

_alert_redirect($this->lang->line("新增成功！"), base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
