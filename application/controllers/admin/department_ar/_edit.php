<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("編輯");

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->get("{$data["field_prefix"]}_id")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one("{$data["table_name"]}", $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("%s不存在，請先設定%s"), "AR", "AR"));
    exit();
}

//局處列表
$admin_group_id = adname_to_groupid($_SESSION['login_admin']->adm_group);

$where_array = array(
    "ag_id"=>$admin_group_id
);
$department_list = $this->Common_model->get_db("tp_admin_group", $where_array);
$data["department_list"] = $department_list;

switch ($data[$data['table_name']]->tdar_content_type) {
    case 'text':
        $data[$data['table_name']]->content = $data[$data['table_name']]->tdar_content_path;
        break;
    case 'image':
        $data[$data['table_name']]->content = $this->config->item('server_base_url')."/upload/image/".$data[$data['table_name']]->tdar_content_path;     
        break;
    case 'youtube':
        $data[$data['table_name']]->content = $data[$data['table_name']]->tdar_content_path;
        break;
    case '3d_model':
        $data[$data['table_name']]->content = $this->config->item('server_base_url')."/upload_model/".$data[$data['table_name']]->tdar_content_path;
        //選擇3D模型，顯示模組
        //並另開新視窗顯示
        $data[$data['table_name']]->ext = preg_replace('/^.*\.([^.]+)$/D', '$1', $data[$data['table_name']]->tdar_content_path);

        $data[$data['table_name']]->download_file = $data[$data['table_name']]->content;

        $data[$data['table_name']]->content = $this->config->item('server_base_url')."/".$data[$data['table_name']]->ext."?threed_files=".$data[$data['table_name']]->content;
        break;
    case 'video':
        $data[$data['table_name']]->content = $this->config->item('server_base_url')."/upload_video/".$data[$data['table_name']]->tdar_content_path;
        break;
}




// echo json_encode($data[$data['table_name']]);
// exit();


//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['menu_category']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/edit?{$data["field_prefix"]}_id={$this->input->get("{$data["field_prefix"]}_id")}")
    )
);
