<?php
header('Content-Type: application/json; charset=utf-8');
$ar_id = $this->session->userdata["ar_id"];
$ar_data = $this->session->userdata["ar_data"];

$data = array(
	"ar_id" => $ar_id,
	"ar_data" => $ar_data
);


exit(json_encode($data,JSON_PARTIAL_OUTPUT_ON_ERROR));