<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = "鑑定";


//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->get("{$data["field_prefix"]}_id")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data["table_name"]] = $this->Common_model->get_one($data["table_name"], $where_array);

if (!$data[$data["table_name"]]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), $data["item_name"]));
    exit();
}

// echo json_encode($data[$data["table_name"]]);
// exit();

$image_link =$this->config->item('server_base_url')."upload_image/".$data[$data["table_name"]]->tdar_image_path;

$mail_body="圖片如下方網址:<br>";
$mail_body.= $image_link."<br>";
$mail_body.= "再請您解析<br>";
$mail_body.= "====================AR點位名稱為=====================<br>";
$mail_body.= "[".$data[$data["table_name"]]->tdar_name."]<br>";
$mail_body.= "====================AR圖片名稱為=====================<br>";
$mail_body.= "[".$data[$data["table_name"]]->tdar_image_path."]<br>";
$mail_body.= "====================================================<br>";
$mail_body.= "<br>";
$mail_body.= "====================此為局處上傳AR點位================<br>";
$mail_body.= "====================請至局處AR管理更新狀態=================<br>";
$mail_body.= $this->config->item('server_base_url')."sa/og_ar_list <br>";
$mail_body.= "====================================================<br>";
$mail_body.= "<br>";
$mail_body.= "<br>";
$mail_body.= "<br>";
$mail_body.= "<br>";
$mail_body.= "<br>";
$mail_body.= "<br>";
$mail_body.= "====================WTC管理網址=====================<br>";
$mail_body.= $this->config->item('server_base_url')."sa/og_ar <br>";
$mail_body.= "====================局處AR管理=====================<br>";
$mail_body.= $this->config->item('server_base_url')."sa/og_ar_list <br>";
$mail_body.= "====================民眾AR管理=====================<br>";
$mail_body.= $this->config->item('server_base_url')."sa/og_ar_cv_list <br>";

//
//寄發通知信
$this->load->library('sendmail_lib');

//mail內容必須在載入library後
$this->sendmail_lib->set_sender("no-reply@omniguider.com", "[桃園發信機制]");
$this->sendmail_lib->set_content("[桃園VRAR360] AR Pattern 鑑定", $mail_body);



//發送email 
// $send_name = $this->config->item('og_arrd_name');
// $send_email = $this->config->item('og_arrd_mail');
// $this->sendmail_lib->set_receiver($send_email, $send_name);

// //給CC
// $send_name2 = $this->config->item('og_arrd_name2');
// $send_email2 = $this->config->item('og_arrd_mail2');
// $this->sendmail_lib->set_cc($send_email2, $send_name2);
// //
// $send_pm_name = $this->config->item('og_pm_name');
// $send_pm_email = $this->config->item('og_pm_mail');
// $this->sendmail_lib->set_cc($send_pm_name, $send_pm_email);

$receivers =  array(
    array(
        'receiver_email' =>  $this->config->item('og_arrd_mail1'),
        'receiver_name'  =>  $this->config->item('og_arrd_name1'),
    ),
    array(
        'receiver_email' =>  $this->config->item('og_pm_mail'),
        'receiver_name'  =>  $this->config->item('og_pm_name'),
    )
);


//寄發email
if ($this->sendmail_lib->send_multi_receiver_mail($receivers)) {
   //寄發成功 
   //更新鑑定狀態
	$update_array = array(
	    "{$data["field_prefix"]}_to_og" => 'Y'
	);
	$this->Common_model->update_db($data["table_name"], $update_array, $where_array);

	_alert_redirect("通知成功", base_url("{$this->controller_name}/{$data["table_name"]}"));
	exit();
}else{
	_alert_redirect("imca.vrar寄信有問題，請開啟低安全性權線，另洽工程師", base_url("{$this->controller_name}/{$data["table_name"]}"));
	exit();
}





