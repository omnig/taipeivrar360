<?php

include_once "__config.php";

$model_name = ucfirst($data['table_name']) . '_model';
$this->load->model($model_name);


//傳入值接收
$input_array = array(
    "limit" => $this->input->post('length'),
    "skip" => $this->input->post('start'),
    "draw" => intval($this->input->get_post('draw')),
    "columns" => $this->input->post('columns'),
    "order" => $this->input->post('order'),
    "filters" => $this->input->post('filters'),
    "customActionType" => $this->input->post('customActionType'),
    "customActionName" => $this->input->post('customActionName'),
    "id" => $this->input->post('id')
);

//多筆資料管理
if ($input_array["customActionType"] == "group_action" && $input_array['id']) {
    $where_array = array(
        "{$data["field_prefix"]}_id" => $input_array['id']
    );

    switch ($input_array['customActionName']) {
        case 'enable' : //啟用
            $update_array = array(
                "{$data["field_prefix"]}_state" => 'Y'
            );
            $this->Common_model->update_db($data["table_name"], $update_array, $where_array);
            break;
        case 'disable' :    //停用
            $update_array = array(
                "{$data["field_prefix"]}_state" => 'N'
            );
            $this->Common_model->update_db($data["table_name"], $update_array, $where_array);
            break;
        case 'del' :    //刪除
            $update_array = array(
                "{$data["field_prefix"]}_del" => 'Y'
            );
            $this->Common_model->update_db($data["table_name"], $update_array, $where_array);
            break;
    }

    $records["customActionStatus"] = "OK";
    $records["customActionMessage"] = "變更完成!";
}

//資料分頁設定
$input_array['limit'] = intval($input_array['limit']) < 0 ? 0 : intval($input_array['limit']);
$input_array['skip'] = intval($this->input->get_post('start'));

$adm_group =  $this->session->login_admin->adm_group;
$isEditor =  $this->session->login_admin->adm_is_editor === 'Y';

$where_array = array(
    'ag_group' => $adm_group
);
$department = $this->Common_model->get_one("tp_admin_group", $where_array);

$where_array = array(
    'd_id' => $department->ag_id,
    'is_civil' => 'N',
    'tdar_del'=>'N'
);

if($isEditor){
    $group_id = $this->session->login_admin->ag_id;
    $where_array['d_id'] = $group_id;
    // $where_array['editor'] = $this->session->login_admin->adm_id;
}

//依傳入參數過濾
if ($input_array['filters']) {
    $concat_where_string = "";

    foreach ($input_array['filters'] as $field_name => $value) {
        if ($value == '') {
            continue;
        }

        $where_string = "";

        switch ($field_name) {
            //比對完全相同的值
            case "d_id":
            case "{$data["field_prefix"]}_star" :
            $where_string = "({$field_name} = '{$value}')";
                break;

            //全文搜尋
            case "{$data["field_prefix"]}_name" :
            case "{$data["field_prefix"]}_state" :
            case "{$data["field_prefix"]}_description" :
            case "{$data["field_prefix"]}_enabled" :
                $where_string = "({$field_name} LIKE '%{$value}%')";
                break;

            //其它特殊搜尋遇濾方式
            case 'create_from' :
                $where_string = "({$data["field_prefix"]}_create_timestamp >= '{$value} 00:00:00')";
                break;
            case 'create_to' :
                $where_string = "({$data["field_prefix"]}_create_timestamp <= '{$value} 23:59:59')";
                break;
        }

        if ($concat_where_string == '') {
            $concat_where_string = $where_string;
        } else {
            $concat_where_string = "$concat_where_string AND $where_string";
        }
    }

    if ($concat_where_string != '') {
        $where_array[] = $concat_where_string;
    }
}

//排序欄位，對映order的排序欄位
$sort_fields = array(
    false,
    false, "t_name",false, false,"{$data["field_prefix"]}_star", "{$data["field_prefix"]}_name", "{$data["field_prefix"]}_description",  "{$data["field_prefix"]}_content_type","d_id", "{$data["field_prefix"]}_create_timestamp","{$data["field_prefix"]}_state","{$data["field_prefix"]}_characteristic_state","{$data["field_prefix"]}_characteristic_state","{$data["field_prefix"]}_to_og", false ,false
);

$order_by = "{$data["field_prefix"]}_create_timestamp DESC";
if (isset($input_array['order'] )) {
    foreach ($input_array['order'] as $row) {
        if ($sort_fields[$row['column']]) {
            if ($order_by == '') {
                $order_by = "{$sort_fields[$row['column']]} {$row['dir']}";
                continue;
            }

            $order_by = "{$sort_fields[$row['column']]} {$row['dir']}";
        }
    }
}


//計算資料總數
$iTotalRecords = $this->$model_name->get_list($order_by, '', 0, 0, $where_array, true);

//從資料庫取得本次資料
$ret = $this->$model_name->get_list($order_by, '', $input_array['limit'], $input_array['skip'], $where_array, false);

// echo json_encode($ret);
// exit();
//回傳值
$records = array();
$records["data"] = array();

$identification = base_url("{$this->controller_name}/department_ar/identification");
$edit_url = base_url("{$this->controller_name}/department_ar/edit");
$del_url = base_url("{$this->controller_name}/department_ar/del");
$route_url = base_url("{$this->controller_name}/department_ar/route_setting");
$turnon_url = base_url("{$this->controller_name}/department_ar/turn_on");
$turn_size_url = base_url("{$this->controller_name}/department_ar/turn_size");

//html表格內容
foreach ($ret as $i => $row) {
    

    // $pattern_state = ($row->{"{$data["field_prefix"]}_characteristic_state"} == 'Y') ? 'Y' : 'N';
    
    
    // if ($pattern_state==='N') {
    //     //代表未處理,顯示通知button
    //     $pattern_link = "<a href='{$identification}?{$data["field_prefix"]}_id={$row->{"{$data["field_prefix"]}_id"}}' class='btn btn-xs default green-stripe btn-operating'><i class='fa fa-eye'></i>通知鑑定</a>";

    // }else{
    //     //代表已處理,有特徵值
    //     $pattern_link = '<span class="label label-sm label-success">已建立特徵值</span>';
    // }


    $to_og_state = ($row->{"{$data["field_prefix"]}_to_og"} == 'Y') ? 'Y' : 'N';

    // if ($to_og_state==='N') {
    //     //代表未處理,顯示通知button
    //     $og_link = '<span class="label label-sm label-danger">尚未通知鑑定</span>';

    // }else{
    //     //代表已處理,有特徵值
    //     $og_link = '<span class="label label-sm label-success">已通知鑑定</span>';
    // }

    // if (!empty($row->ap_puid)) {
    //     $route_set = '<span class="label label-sm label-success">已通知製作路徑</span>';
    // }else{
    //     $route_set = "<a href='{$route_url}?{$data["field_prefix"]}_id={$row->{"{$data["field_prefix"]}_id"}}' class='btn btn-xs default blue-stripe btn-operating'><i class='fa fa-pencil'></i> 路徑製作通知</a>";
    // }
    

    $is_enabled = ($row->{"{$data["field_prefix"]}_state"} == 'Y') ? 'Y' : 'N';

    if ($is_enabled==='N') {
        //代表未處理,顯示啟用button

        $status = '<span class="label label-sm label-default">已關閉</span>';

    }else{
        //代表已處理,顯示啟用中
        $status = '<span class="label label-sm label-success">啟用中</span>';
    }

    // if($row->{"{$data["field_prefix"]}_validation_description"} === '上傳圖片無法辨識'){
    //     $route_set = '<span class="label label-sm label-danger">AR圖片無法辨識，請再次上傳</span>';
    //     $status = '<span class="label label-sm label-danger">x</span>';
    //     $pattern_link = '<span class="label label-sm label-danger">x</span>';
    //     $og_link = '<span class="label label-sm label-danger">x</span>';
    // }

    $id = $row->{"{$data["field_prefix"]}_id"};
    $img_url = base_url("upload_image/" . $row->{"{$data["field_prefix"]}_image_path"});
    $img_id = basename($img_url);
    $find_img_url = base_url("upload/image/" . $row->{"{$data["field_prefix"]}_findpho_path"});
    $find_img_id = basename($find_img_url);

    $vsize = $row->{"{$data["field_prefix"]}_vsize"};

    if($vsize==="N"){
        $vsize = "中";
    }else if ($vsize === "B") {
        $vsize = "大";
    }else{
        $vsize = "小";
    }

    $threed_model_button = "<br><a href='{$turn_size_url}?{$data["field_prefix"]}_id={$row->{"{$data["field_prefix"]}_id"}}&size=B' class='btn btn-xs default blue-stripe btn-operating'>大</a>";

    $threed_model_button .= "<a href='{$turn_size_url}?{$data["field_prefix"]}_id={$row->{"{$data["field_prefix"]}_id"}}&size=N' class='btn btn-xs default green-stripe btn-operating'>中</a>";

    $threed_model_button .= "<a href='{$turn_size_url}?{$data["field_prefix"]}_id={$row->{"{$data["field_prefix"]}_id"}}&size=S' class='btn btn-xs default red-stripe btn-operating'>小</a>";

    $records["data"] [] = array(
        '<input type="checkbox" name="list_checkbox[]" value="' . $id . '">',
        $i + 1,
        $row->{"{$data["field_prefix"]}_name"},
//        $row->{"{$data["field_prefix"]}_description"},
        $row->{"{$data["field_prefix"]}_content_type"}=="3d_model"?$row->{"{$data["field_prefix"]}_content_type"}."(".$vsize.")".$threed_model_button:$row->{"{$data["field_prefix"]}_content_type"},
        $row->ag_name,
        ($row->{"{$data["field_prefix"]}_create_timestamp"}) ? date("Y/m/d", strtotime($row->{"{$data["field_prefix"]}_create_timestamp"})) : '',
        $status,
        // $pattern_link,
        // $og_link,
        // $route_set,
        "<a href='{$edit_url}?{$data["field_prefix"]}_id={$row->{"{$data["field_prefix"]}_id"}}' class='btn btn-xs default blue-stripe btn-operating'><i class='fa fa-pencil'></i> {$this->lang->line("編輯")}</a>" .
        "<a class='btn btn-xs default red-stripe btn-operating delete' data='{$row->{"{$data["field_prefix"]}_id"}}'><i class='fa fa-remove'></i> {$this->lang->line("刪除")}</a>",
    );
}

$records["draw"] = $input_array["draw"];
$records["recordsTotal"] = $iTotalRecords;
$records["recordsFiltered"] = $iTotalRecords;

echo json_encode($records);
exit();
