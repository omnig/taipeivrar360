<?php

include_once "__config.php";
$data["table_name"] = "department_ar";

// echo json_encode($_REQUEST);
// exit;


$input_array = array(
    "tdar_id" => $this->input->get("tdar_id"),
    "size" =>$this->input->get("size")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert("參數錯誤");
        exit();
    }
}


//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"],
    "is_civil" => "N"
);

$data[$data['table_name']] = $this->Common_model->get_one("department_ar", $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), "使用者上傳3D模型不存在"));
    exit();
}


$where_array = array(
    'tdar_id' => $input_array['tdar_id'],
);

$update_array = array(
    'tdar_vsize' => $input_array['size']
);

$this->Common_model->update_db("department_ar", $update_array, $where_array);


_alert_redirect('更新成功！', base_url("{$this->controller_name}/department_ar"));
exit();







