<?php

include_once "__config.php";
ini_set("memory_limit","2048M");
set_time_limit(0);
use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;
// echo json_encode($_FILES);
// exit();
// echo json_encode($_REQUEST);
// exit();

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->post("{$data["field_prefix"]}_id"),
    "{$data["field_prefix"]}_name" => $this->input->post("{$data["field_prefix"]}_name"),
    "{$data["field_prefix"]}_content_type" => $this->input->post("{$data["field_prefix"]}_content_type"),
    "{$data["field_prefix"]}_state" => $this->input->post("{$data["field_prefix"]}_state")
);

//Input 中文敘述
$zh_array = array(
    "{$data["field_prefix"]}_name" => "AR名稱",
    "{$data["field_prefix"]}_content_type" => "AR呈現類別",
    "{$data["field_prefix"]}_state" => "狀態",
);


foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
//        echo ("參數錯誤:(".$zh_array[$index].")不能為空值");
        echo json_encode(
            array(
                'msg' => ("參數錯誤:(".$zh_array[$index].")不能為空值"),
                'csrf_token' => $this->security->get_csrf_hash()
            )
        );
        exit();
    }
}

$input_array["{$data["field_prefix"]}_description"] = $this->input->post("{$data["field_prefix"]}_description");
$input_array["{$data["field_prefix"]}_is_transparent"] = $this->input->post("{$data["field_prefix"]}_is_transparent")??'N';

//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one("department_ar", $where_array);

if (!$data[$data['table_name']]) {
//    echo (sprintf($this->lang->line("這個%s不存在！"), "AR圖片"));
    echo json_encode(
        array(
            'msg' => (sprintf($this->lang->line("這個%s不存在！"), "AR圖片")),
            'csrf_token' => $this->security->get_csrf_hash()
        )
    );
    exit();
}


$update_array = array(
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "{$data["field_prefix"]}_description"  => $input_array["{$data["field_prefix"]}_description"],
    "{$data["field_prefix"]}_content_type" => $input_array["{$data["field_prefix"]}_content_type"],
    "{$data["field_prefix"]}_state" => $input_array["{$data["field_prefix"]}_state"],
    "{$data["field_prefix"]}_is_transparent" => $input_array["{$data["field_prefix"]}_is_transparent"]
);


/******************************************
上傳ＡＲ 呈現內容
*******************************************/


//判斷圖/文/影/3D上傳
switch ($input_array["{$data["field_prefix"]}_content_type"]) {
    case 'text':
        # 直接上傳文字
        $update_array["{$data["field_prefix"]}_content_path"] = $this->input->post("{$data["field_prefix"]}_content_path");
        if($this->input->post("{$data["field_prefix"]}_content_path")===""){
//            echo "請填寫文字";
            echo json_encode(
                array(
                    'msg' => "請填寫文字",
                    'csrf_token' => $this->security->get_csrf_hash()
                )
            );
            exit();
        }
        if (mb_strlen($update_array["{$data["field_prefix"]}_content_path"], "utf-8")>21) {
//            echo ("文字超過20個字");
            echo json_encode(
                array(
                    'msg' => "文字超過20個字",
                    'csrf_token' => $this->security->get_csrf_hash()
                )
            );
            exit();
        }
        break;

    case 'image':
        # 上傳圖片路徑
        // //上傳圖片處理
        if ($_FILES["tdar_content_image"]["name"]!="") {
            $upload_path = "upload/image/";
            $file_type = 'gif|jpg|jpeg|png';
            $max_size = 4096;
            $resize_width = 264;
            $resize_height = 156;
            $fit_type = "FIT_OUTER";
            $max_width = 1920;
            $max_height = 1080;

            //上傳代表圖片
            $field_name = "{$data["field_prefix"]}_content_image";
            $upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);


            if (!isset($upload_result["upload_data"])) {
                //必須上傳圖片
//                echo ($this->lang->line("圖片上傳失敗或沒有上傳圖片"));
                echo json_encode(
                    array(
                        'msg' => ($this->lang->line("圖片上傳失敗或沒有上傳圖片")),
                        'csrf_token' => $this->security->get_csrf_hash()
                    )
                );
                exit();
            }
            //上傳成功，記錄上傳檔名
            $update_array["{$data["field_prefix"]}_content_path"] = $upload_result["upload_data"]["file_name"];
        }else{
//            echo ("請上傳圖片");
            echo json_encode(
                array(
                    'msg' => "請上傳圖片",
                    'csrf_token' => $this->security->get_csrf_hash()
                )
            );
            exit();
        }
        break;

    case 'video':
        # 上傳影片路徑
        if ($_FILES["tdar_content_video"]["name"]!="") {
            $upload_path = "upload_video/";
            $file_type = 'mp4';
            $max_size = 300000;
            $field_name = "{$data["field_prefix"]}_content_video";
            $upload_result = $this->upload_lib->write_from_video($upload_path, $field_name, $file_type, $max_size);

            if (!isset($upload_result["upload_data"])) {
                //必須上傳影片
//                echo ("影片上傳失敗，或沒有上傳影片");
                echo json_encode(
                    array(
                        'msg' => "影片上傳失敗，或沒有上傳影片",
                        'csrf_token' => $this->security->get_csrf_hash()
                    )
                );
                exit();
            }
            //上傳成功，記錄上傳檔名
            $update_array["{$data["field_prefix"]}_content_path"] = basename($upload_result["upload_data"]["file_name"]);
        }else{
//            echo ("請上傳影片");
            echo json_encode(
                array(
                    'msg' => "請上傳影片",
                    'csrf_token' => $this->security->get_csrf_hash()
                )
            );
            exit();
        }
        break;

    case 'youtube':
        # 上傳Youtube ID
        
        //記錄上傳ID
        $update_array["{$data["field_prefix"]}_content_path"] = $this->input->post("{$data["field_prefix"]}_video_id");
        break;

    case '3d_model':
        if($this->input->post("{$data["field_prefix"]}_animation_name")===""){
//            echo "請填寫動畫名稱";
            echo json_encode(
                array(
                    'msg' => "請填寫動畫名稱",
                    'csrf_token' => $this->security->get_csrf_hash()
                )
            );
            exit();
        }
        # 上傳3D 路徑
        if ($_FILES["tdar_content_model"]["name"]!="") {
            $upload_path = "upload_model/";
            $file_type = 'obj|deb|fbx';
            $max_size = 300000;
            $field_name = "{$data["field_prefix"]}_content_model";
            $upload_result = $this->upload_lib->write_from_model($upload_path, $field_name, $file_type, $max_size);
            if (!isset($upload_result["upload_data"])) {
                //必須上傳影片
//                echo ("3D模型上傳失敗，或沒有上傳3D模型");
                echo json_encode(
                    array(
                        'msg' => "3D模型上傳失敗，或沒有上傳3D模型",
                        'csrf_token' => $this->security->get_csrf_hash()
                    )
                );
                exit();
            }
            //上傳成功，記錄上傳檔名
            $update_array["{$data["field_prefix"]}_content_path"] = $upload_result["upload_data"]["file_name"];
            $update_array["{$data["field_prefix"]}_angle"] = $this->input->post("{$data["field_prefix"]}_content_degree");
        }

        break;

}

//更新AR
$this->Common_model->update_db("department_ar", $update_array, $where_array);

_alert_redirect('更新成功！', base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
