<?php

include_once "__config.php";


//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("新增");

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['menu_category']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/add")
    )
);



if ($this->controller_name=="admin") {

    $admin_group_id = adname_to_groupid($_SESSION['login_admin']->adm_group);

    $where_array = array(
        "ag_id"=>$admin_group_id
    );
    $department_list = $this->Common_model->get_db("tp_admin_group", $where_array);
    $data["department_list"] = $department_list;


    $where_array = array(
        "t_organizer" => $admin_group_id,
        't_enabled'=>'Y',
        't_del'=>'N'
    );

    $topic_list = $this->Common_model->get_db("tp_topic", $where_array);

    $data["topic_list"] = $topic_list;

    $where_array = array(
        "t_co_organizer like" => "%".$admin_group_id."%",
        't_enabled'=>'Y',
        't_del'=>'N'
    );

    // $where_array = array();
    $topic_list = $this->Common_model->get_db("tp_topic", $where_array);

    foreach ($topic_list as $key => $value) {
        array_push($data["topic_list"], $value);
    }
    
}else{
    $where_array = array();
    $department_list = $this->Common_model->get_db("tp_admin_group", $where_array);
    $data["department_list"] = $department_list;


    $where_array = array(
        't_enabled'=>'Y',
        't_del'=>'N'
    );
    $topic_list = $this->Common_model->get_db("tp_topic", $where_array);
    $data["topic_list"] = $topic_list;
}