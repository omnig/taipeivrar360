<?php

/*
 * ajax 新增 商店管理員
 */

include_once "__config.php";

//傳入值接收
$input_array = array(
    "sa_id" => $this->input->get('sa_id')
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

//檢查是否已標示已讀
// $where_array = array(
//     "sa_id" => $input_array["sa_id"],
//     "adm_id" => $this->login_admin->adm_id
// );
// $sar = $this->Common_model->get_one("store_announcement_relation", $where_array);

// if (!$sar) {
//     //標示已讀
//     $insert_array = array(
//         "sa_id" => $input_array["sa_id"],
//         "adm_id" => $this->login_admin->adm_id
//     );
//     $this->Common_model->insert_db("store_announcement_relation", $insert_array);
//     _alert("已讀");
// }

exit();
