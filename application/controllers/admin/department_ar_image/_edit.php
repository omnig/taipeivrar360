<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("編輯");

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->get("{$data["field_prefix"]}_id")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one("{$data["table_name"]}", $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("%s不存在，請先設定%s"), "辨識圖", "辨識圖"));
    exit();
}

//局處列表
$admin_group_id = adname_to_groupid($_SESSION['login_admin']->adm_group);

$where_array = array(
    "ag_id"=>$admin_group_id
);
$department_list = $this->Common_model->get_db("tp_admin_group", $where_array);
$data["department_list"] = $department_list;

$data["identification"] = base_url("{$this->controller_name}/department_ar_image/identification");

// 麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['menu_category']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/edit?{$data["field_prefix"]}_id={$this->input->get("{$data["field_prefix"]}_id")}")
    )
);

// 修改內容權限
$data['auth'] = 'user';



$d_id = $data["department_list"][0]->ag_id;
$isEditor =  $this->session->login_admin->adm_is_editor === 'Y';
$where_array = [
    'tdar_state' => 'Y',
    'tdar_del' => 'N',
    "d_id = $d_id"
];
if($isEditor){
    $group_id = $this->session->login_admin->ag_id;
    $where_array['d_id'] = $group_id;
    // $where_array['editor'] = $this->session->login_admin->adm_id;
}
$dept_ar_list = $this->Common_model->get_db('department_ar', $where_array);

$where_array = [
    'tdar_state' => 'Y',
    'tdar_del' => 'N',
    "d_id != $d_id",
    "share_other = 'Y'"
];
$shared_ar_list = $this->Common_model->get_db('department_ar', $where_array);
$data['ar_list'] = array_merge($dept_ar_list, $shared_ar_list);
