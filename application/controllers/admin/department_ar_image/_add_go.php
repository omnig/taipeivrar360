<?php
include_once "__config.php";
ini_set("memory_limit","2048M");
set_time_limit(0);
use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;
// echo json_encode($_REQUEST);
// exit();

// echo json_encode($_FILES);
// exit();


//application/octet-stream

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_name" => $this->input->post("{$data["field_prefix"]}_name"),
);

//Input 中文敘述
$zh_array = array(
    "{$data["field_prefix"]}_name" => "辨識圖名稱",
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        echo ("參數錯誤:(".$zh_array[$index].")不能為空值");
        exit();
    }
}

//圖片上傳檔案檢查
if (!isset($_FILES["{$data["field_prefix"]}_image_path"]) || empty($_FILES["{$data["field_prefix"]}_image_path"]["name"])) {
    //必須上傳圖片
    if (!isset($this->session->userdata["ar_data"])) {
        echo ("AR掃描圖片上傳失敗或沒有上傳圖片");
        exit();
    }
}

$input_array["{$data["field_prefix"]}_description"] = $this->input->post("{$data["field_prefix"]}_description");

$adm_group =  $this->session->login_admin->adm_group;

$where_array = array(
    'ag_group' => $adm_group
);
$department = $this->Common_model->get_one("tp_admin_group", $where_array);

$insert_array = array(
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "{$data["field_prefix"]}_description"  => $input_array["{$data["field_prefix"]}_description"],
    "d_id" => $department->ag_id,
    "{$data["field_prefix"]}_state" => "N",
    "default_tdar_id" => $this->input->post("default_tdar_id"),
    "default_ar_text" => $this->input->post("default_ar_text"),
    "default_ar_url" => $this->input->post("default_ar_url"),
    "default_ar_heigh" => $this->input->post("default_ar_heigh"),
    "default_ar_vsize" => $this->input->post("default_ar_vsize"),
    "default_cover_identify_image" => $this->input->post("default_cover_identify_image")
);
/******************************************
上傳ＡＲ 圖片
*******************************************/
//{$data["field_prefix"]}_image_path
// //上傳圖片處理
$upload_path = "upload_image/";
$file_type = 'jpg|jpeg|png';
$max_size = 30480; //30M

//上傳代表圖片
$field_name = "{$data["field_prefix"]}_image_path";
$upload_result = $this->upload_lib->write_from_image($upload_path, $field_name, $file_type, $max_size);

if(isset($this->session->userdata["ar_data"])){
    $insert_array["{$data["field_prefix"]}_image_path"] = $this->session->userdata["ar_data"]->tdar_image_path;
}else if (!isset($upload_result["upload_data"])) {
    //必須上傳圖片
    echo ($this->lang->line("圖片上傳失敗或沒有上傳圖片"));
    exit();
}else{
    //上傳成功，記錄上傳檔名
    $insert_array["{$data["field_prefix"]}_image_path"] = $upload_result["upload_data"]["file_name"];
}

$this->Common_model->insert_db("department_ar_image", $insert_array);
// @unlink($filename);

$ai_id = $this->Common_model->get_insert_id();

unset($this->session->userdata["ai_id"]);
unset($this->session->userdata["ar_data"]);

_alert_redirect($this->lang->line("新增成功！"), base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
