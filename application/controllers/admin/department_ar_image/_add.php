<?php

include_once "__config.php";


//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("新增");

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['menu_category']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/add")
    )
);



$admin_group_id = adname_to_groupid($_SESSION['login_admin']->adm_group);

$where_array = array(
    "ag_id"=>$admin_group_id
);
$department_list = $this->Common_model->get_db("tp_admin_group", $where_array);
$data["department_list"] = $department_list;



$where_array = array(
	't_enabled'=>'Y',
	't_del'=>'N'
);




$data["share_image"] = array();
$data["share_video"] = array();
$data["share_3d"] = array();

$model_name = ucfirst($data['table_name']) . '_model';
$this->load->model($model_name);


$data["share_image"] = $this->$model_name->get_share_image();
$data["share_video"] = $this->$model_name->get_share_video();
$data["share_3d"] = $this->$model_name->get_share_three_model();

$d_id = $data["department_list"][0]->ag_id;
$isEditor =  $this->session->login_admin->adm_is_editor === 'Y';
$where_array = [
    'tdar_state' => 'Y',
    'tdar_del' => 'N',
    "d_id = $d_id"
];
if($isEditor){
    $group_id = $this->session->login_admin->ag_id;
    $where_array['d_id'] = $group_id;
    // $where_array['editor'] = $this->session->login_admin->adm_id;
}
$dept_ar_list = $this->Common_model->get_db('department_ar', $where_array);

$where_array = [
    'tdar_state' => 'Y',
    'tdar_del' => 'N',
    "d_id != $d_id",
    "share_other = 'Y'"
];
$shared_ar_list = $this->Common_model->get_db('department_ar', $where_array);
$data['ar_list'] = array_merge($dept_ar_list, $shared_ar_list);
// $data['ar_list'] = $this->Common_model->get_db('department_ar', ['tdar_state' => 'Y', 'tdar_del' => 'N']);

// echo json_encode($data["share_image"]);
// exit;
// $this->load->helper('youtube_helper');
// $access_token = $this->session->access_token['access_token'];
// $youtube_link = youtube_client_upload($access_token,$video_path,$video_title,$video_description)
// exit();
