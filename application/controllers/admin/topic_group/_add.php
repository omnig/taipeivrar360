<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("新增");

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['menu_category']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/add")
    )
);



$adm_group =  $this->session->login_admin->adm_group;

$where_array = array(
    'ag_group' => $adm_group
);
$department_list = $this->Common_model->get_db("tp_admin_group", $where_array);

$data["department_list"] = $department_list;

// topic 列表
$data["topic_list"] = $this->Common_model->get_db("topic", ["d_id" => $department_list[0]->ag_id, "t_del" => "N"]);


// echo json_encode($topic_category);
// exit;