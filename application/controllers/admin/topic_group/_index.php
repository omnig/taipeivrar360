<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line($data["menu_name"]);

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    )
);


$where_array = array();
$department_list = $this->Common_model->get_db("tp_admin_group", $where_array);
//局處列表
$data["department_list"] = $department_list;
// echo json_encode($department_list);
// exit();

//取得主題類別ID
$this->load->model("Topic_category_model");
$topic_category = $this->Topic_category_model->get_list();
$data["topic_category"] = $topic_category;

$data["del_url"] = base_url("{$this->controller_name}/{$data['table_name']}/del");