<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("編輯");


//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/edit?{$data["field_prefix"]}_id={$this->input->get("{$data["field_prefix"]}_id")}")
    )
);

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->get("{$data["field_prefix"]}_id"),
    "m_id" => $this->input->get("m_id")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}
$data['m_id'] = $input_array['m_id'];

//透過m_id 搜尋九宮格內容
$where_array = array(
    "m_id" => $data['m_id']
);

$data["mission_grid"] = $this->Common_model->get_db($data['table_name'], $where_array);

if (!$data["mission_grid"] ) {
    _alert_redirect(sprintf($this->lang->line("這個%s不存在！"), $data["item_name"]),base_url("{$this->controller_name}/mission"));
    exit();
}


$first_mid = $data["mission_grid"][0]->ng_id;
$last_mid  = $data["mission_grid"][count($data["mission_grid"])-1]->ng_id;
foreach ($data["mission_grid"] as $key => $value) {
    $check_array[$value->ng_id]= $value->ap_id;
}


for ($i=$first_mid; $i < $input_array["{$data["field_prefix"]}_id"] ; $i++) { 


    if ($check_array[$i]==0) {
        // alert();

        _alert_redirect("請依序填寫關卡內容", base_url("{$this->controller_name}/nine_grid?m_id=".$data['m_id']));
    }
}




$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one($data['table_name'], $where_array);

// echo json_encode($data["mission_grid"]);
// echo json_encode($first_mid);
// echo json_encode($data[$data['table_name']]->p_id);
// exit();


if(!empty($data[$data['table_name']]->ap_id)){
    $ap_id = $data[$data['table_name']]->ap_id;
    // 取得 poi 部門資料
    $poi = $this->Common_model->get_db_join("ap_pois as ap", ['ap_id' => $ap_id] , "admin_group as ag" , "ag.ag_id =ap.d_id " , "INNER")[0];
    $data[$data['table_name']]->d_id = $poi->ag_id;
    $data['poi'] = $poi;
    $data["ar_image"] = $this->Common_model->get_one('department_ar_image', ["dai_id" => $poi->dai_id]);
}else{
    $data[$data['table_name']]->d_id = null;
    $data['poi'] = null;
}



if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("%s不存在，請先設定%s"), $data["item_group_name"], $data["item_group_name"]));
    exit();
}

//局處列表
$data["department_list"] = $this->Common_model->get_db("admin_group", []);
//poi列表
$data["poi_list"] = $this->Common_model->get_db("ap_pois", []);
//ar列表
$data['ar_list'] = $this->Common_model->get_db('department_ar', ['tdar_state' => 'Y', 'tdar_del' => 'N']);

//取得綁定的問題
$data["question"] = $this->Common_model->get_one("question", ['ng_id'=>$input_array["{$data["field_prefix"]}_id"]]);
if(isset($data["question"])){
    switch ($data["question"]->q_style){
        case '是非':
            $data["question"]->detail = $this->Common_model->get_one("question_boolean", ['question_id'=>$data["question"]->question_id]);
            break;
        case '單選':
            $data["question"]->detail = $this->Common_model->get_one("question_select", ['question_id'=>$data["question"]->question_id]);
            break;
        default:
            $data["question"]->detail = null;
    }
}
