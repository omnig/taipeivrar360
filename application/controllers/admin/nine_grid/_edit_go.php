<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = $this->lang->line("編輯");

//傳入值接收
$input_array = array(
    "m_id" => $this->input->post("m_id"),
    "{$data["field_prefix"]}_id" => $this->input->post("{$data["field_prefix"]}_id"),
    "{$data["field_prefix"]}_title" => $this->input->post("{$data["field_prefix"]}_title"),
    // "{$data["field_prefix"]}_trigger" => $this->input->post("{$data["field_prefix"]}_trigger"),
    // "ap_id" => $this->input->post("ap_id"),

);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

$input_array["{$data["field_prefix"]}_describe"] =  $this->input->post("{$data["field_prefix"]}_describe");
$input_array["{$data["field_prefix"]}_notice"] =  $this->input->post("{$data["field_prefix"]}_notice");
$input_array["ap_id"] =  $this->input->post("ap_id");
$input_array["{$data["field_prefix"]}_pass_method"] =  $this->input->post("{$data["field_prefix"]}_pass_method");
$input_array["{$data["field_prefix"]}_trigger_distance"] =  $this->input->post("{$data["field_prefix"]}_trigger_distance");



//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one($data['table_name'], $where_array);

if (!$data[$data['table_name']]) {
    _alert_redirect(sprintf($this->lang->line("這個%s不存在！"), $data["item_name"]),base_url("{$this->controller_name}/mission"));
    exit();
}

$update_array = array(
    "{$data["field_prefix"]}_title" => $input_array["{$data["field_prefix"]}_title"],
    "{$data["field_prefix"]}_describe" => $input_array["{$data["field_prefix"]}_describe"],
    "{$data["field_prefix"]}_notice" => $input_array["{$data["field_prefix"]}_notice"],
    "ap_id" => $input_array["ap_id"],
    "{$data["field_prefix"]}_pass_method" => $input_array["{$data["field_prefix"]}_pass_method"],
    "{$data["field_prefix"]}_trigger_distance" => $input_array["{$data["field_prefix"]}_trigger_distance"],
);

$this->Common_model->update_db($data['table_name'], $update_array, $where_array);

if($input_array["{$data["field_prefix"]}_pass_method"] === 'QA'){
    //處理題目
    $q_style = $this->input->post('q_style');
    $qb_insert_array = [
        'qb_style' => $this->input->post('qb_style'),
        'qb_title' => $this->input->post('qb_title'),
        'qb_image' => $this->input->post('qb_image'),
        'qb_video' => ($this->input->post('qb_video') !== '') ? $this->input->post('qb_video') : null,
        'qb_answer' => $this->input->post('qb_answer'),
        'qb_tip' => $this->input->post('qb_tip'),
    ];
    $qs_insert_array = [
        'qs_style' => $this->input->post('qs_style'),
        'qs_title' => $this->input->post('qs_title'),
        'qs_image' => $this->input->post('qs_image'),
        'qs_video' => ($this->input->post('qs_video') !== '') ? $this->input->post('qs_video') : null,
        'qs_option1' => $this->input->post('qs_option1'),
        'qs_option2' => $this->input->post('qs_option2'),
        'qs_option3' => $this->input->post('qs_option3'),
        'qs_option4' => $this->input->post('qs_option4'),
        'qs_answer' => $this->input->post('qs_answer'),
        'qs_tip' => $this->input->post('qs_tip'),
    ];
    $origin_question = $this->Common_model->get_one("question", ['ng_id'=>$input_array["{$data["field_prefix"]}_id"]]);
    if(!isset($origin_question)){
        //直接新增題目
        $q_insert_array = [
            'q_style'=> $q_style,
            'ng_id'=>$input_array["{$data["field_prefix"]}_id"]
        ];
        $this->Common_model->insert_db('question',$q_insert_array);
        $question_id = $this->Common_model->get_insert_id();
        if($q_style === '是非'){
            $qb_insert_array['question_id'] = $question_id;
            $this->Common_model->insert_db('question_boolean',$qb_insert_array);
        }
        if($q_style === '單選'){
            $qs_insert_array['question_id'] = $question_id;
            $this->Common_model->insert_db('question_select',$qs_insert_array);
        }
    }else{
        if($q_style==='是非'){
            $this->Common_model->delete_db('question_select', ["question_id"=>$origin_question->question_id]);
            //更新question，question_boolean，沒有就新增question_boolean
            if($origin_question->q_style==='是非'){
                $this->Common_model->update_db('question_boolean', $qb_insert_array,["question_id"=>$origin_question->question_id]);
            }else{
                $qb_insert_array['question_id'] = $origin_question->question_id;
                $this->Common_model->insert_db('question_boolean',$qb_insert_array);
                $this->Common_model->update_db('question', ['q_style'=>$q_style],["question_id"=>$origin_question->question_id]);
            }
        }
        if($q_style==='單選'){
            $this->Common_model->delete_db('question_boolean', ["question_id"=>$origin_question->question_id]);
            //更新question，question_select，question_select
            if($origin_question->q_style==='單選'){
                $this->Common_model->update_db('question_select', $qs_insert_array,["question_id"=>$origin_question->question_id]);
            }else{
                $qs_insert_array['question_id'] = $origin_question->question_id;
                $this->Common_model->insert_db('question_select',$qs_insert_array);
                $this->Common_model->update_db('question', ['q_style'=>$q_style],["question_id"=>$origin_question->question_id]);
            }
        }
    }
}

_alert_redirect('更新成功！', base_url("{$this->controller_name}/{$data['table_name']}?m_id={$input_array['m_id']}"));
exit();
