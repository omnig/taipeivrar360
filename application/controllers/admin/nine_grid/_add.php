<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("新增");

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/edit?{$data["field_prefix"]}_id={$this->input->get("{$data["field_prefix"]}_id")}")
    )
);

//傳入值接收
$input_array = array(
    "m_id" => $this->input->get("m_id")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}
$data['m_id'] = $input_array['m_id'];


$where_array = array(
    "m_id" => $input_array["m_id"]
);

$data[$data['table_name']] = $this->Common_model->get_db($data['table_name'], $where_array);

if (!$data[$data['table_name']] ) {
    _alert_redirect(sprintf($this->lang->line("這個%s不存在！"), $data["item_name"]),base_url("{$this->controller_name}/mission"));
    exit();
}


//算出現在有多少個關卡
$data['mission_cnt'] = count($data[$data['table_name']]);



//取得所有建物
$data['ap_building'] = $this->Common_model->get_db('ap_building',$where_array=array());

//取得所有樓層
$data['ap_floors'] = $this->Common_model->get_db('ap_floors',$where_array=array());

// exit();


// if (!$data[$data['table_name']]) {
//     _alert(sprintf($this->lang->line("%s不存在，請先設定%s"), $data["item_group_name"], $data["item_group_name"]));
//     exit();
// }
