<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("編輯");

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->get("{$data["field_prefix"]}_id")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one("topic", $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("%s不存在，請先設定%s"), "主題", "主題"));
    exit();
}

$adm_group =  $this->session->login_admin->adm_group;

$where_array = array(
    'ag_group' => $adm_group
);
$department_list = $this->Common_model->get_db("tp_admin_group", $where_array);


//局處列表
$data["department_list"] = $department_list;

//協辦處列表
$where_array = array(
    'ag_group !=' => $adm_group
);
$co_department_list = $this->Common_model->get_db("tp_admin_group", $where_array);

$data["co_department_list"] = $co_department_list;


//協辦局處ID 陣列
$data["co_organizer_ary"] = explode(",", $data[$data['table_name']] ->t_co_organizer);

$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

//get_key_word
$keywords = $this->Common_model->get_db_join("keyword_topic as kt", $where_array , "keywords as ks" , "kt.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");

foreach ($keywords as $keywords_key => $keywords_value) {
    $data['keywords'][$keywords_key] = $keywords_value->kwd_name;
}


//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['menu_category']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/edit?{$data["field_prefix"]}_id={$this->input->get("{$data["field_prefix"]}_id")}")
    )
);


//poi列表
$data["poi_list"] = $this->Common_model->get_db("ap_pois", ["d_id" => $department_list[0]->ag_id]);


//取得主題類別ID
$this->load->model("Topic_category_model");
$topic_category = $this->Topic_category_model->get_list();
$data["topic_category"] = $topic_category;
