<?php

include_once "__config.php";

// echo json_encode($_REQUEST);
// exit();

//傳入值接收
$input_array = array(
    "tc_id" => $this->input->post("tc_id"),
    "{$data["field_prefix"]}_name" => $this->input->post("{$data["field_prefix"]}_name"),
    // "{$data["field_prefix"]}_describe" => $this->input->post("{$data["field_prefix"]}_describe"),
    "{$data["field_prefix"]}_enabled" => $this->input->post("{$data["field_prefix"]}_enabled"),
    // "ap_ids" => $this->input->post("ap_ids"),
);

//Input 中文敘述
$zh_array = array(
    "tc_id" => "主題類別",
    "{$data["field_prefix"]}_name" => "主題名稱",
    // "{$data["field_prefix"]}_describe" => "主題描述",
    "{$data["field_prefix"]}_enabled" => "主題狀態",
    // "ap_ids" => "poi點位",
);

foreach ($input_array as $index => $value) {
    if (empty($value)) {
        _alert("參數錯誤:(".$zh_array[$index].")不能為空值");
        exit();
    }
}

$input_array["{$data["field_prefix"]}_describe"] = $this->input->post("{$data["field_prefix"]}_describe");
$input_array["ap_ids"] = $this->input->post("ap_ids");

//檢驗字數
if(!valid_string_lenth($input_array["{$data["field_prefix"]}_describe"],100)){
    _alert("參數錯誤:主題描述不能超過100字");
    exit();
}

$adm_group =  $this->session->login_admin->adm_group;

$where_array = array(
    'ag_group' => $adm_group
);
$department = $this->Common_model->get_one("tp_admin_group", $where_array);
$insert_array = array(
    "tc_id" => $input_array["tc_id"],
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "{$data["field_prefix"]}_describe"  => $input_array["{$data["field_prefix"]}_describe" ],
    "{$data["field_prefix"]}_enabled" => $input_array["{$data["field_prefix"]}_enabled"],
    "d_id" => $department->ag_id,
    "ap_ids" => $input_array["ap_ids"],
    // "editor" => $this->session->login_admin->adm_id,
);

//上傳圖片處理
$upload_path = "upload/topic_image/";
$file_type = 'gif|jpg|jpeg|png';
$max_size = 1024;
$resize_width = 264;
$resize_height = 156;
$fit_type = "FIT_OUTER";
$max_width = 1920;
$max_height = 1080;

//上傳圖片
$field_name = "{$data["field_prefix"]}_path";
$upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);

if (!isset($upload_result["upload_data"])) {
    //必須上傳圖片
    _alert($this->lang->line("圖片上傳失敗或沒有上傳圖片"));
    exit();
}
//上傳成功，記錄上傳檔名
$insert_array["t_image"] = $upload_result["upload_data"]["file_name"];

//上傳圖片處理
$upload_path = "upload/topic_image/";
$file_type = 'gif|jpg|jpeg|png';
$max_size = 10000;
$resize_width = 1600;
$resize_height = 900;
$fit_type = "FIT_OUTER";
$max_width = 3200;
$max_height = 1800;

//上傳圖片
$field_name = "{$data["field_prefix"]}_path";
$upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);

if (!isset($upload_result["upload_data"])) {
    //必須上傳圖片
    _alert($this->lang->line("圖片上傳失敗或沒有上傳圖片"));
    exit();
}
//上傳成功，記錄上傳檔名
$insert_array["t_image"] = $upload_result["upload_data"]["file_name"];

//新增Mysql DB Topic Table
$this->Common_model->insert_db("topic", $insert_array);

$topic_id = $this->Common_model->get_insert_id();

$insert_array = array(
    "t_id" => $topic_id,
    "tc_id" => $input_array["tc_id"],
);
$this->Common_model->insert_db("topic_category_relation",$insert_array);

_alert_redirect($this->lang->line("新增成功！"), base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
