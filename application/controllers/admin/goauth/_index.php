<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("登入");


//產生圖形驗證碼
$data['cap'] = $this->captcha_lib->get_captcha();

$key_location = dirname(dirname(dirname(__DIR__))).'/OAuth/client_secret_811181325009-0mop4r7ohh8jskev30pq07riv45ic7cj.json';

$redirect_uri = $this->config->item('server_base_url').'admin/goauth/go';

$client = new Google_Client();
$client->setAccessType('offline');
$client->setApplicationName("Client_Library_Examples");
$client->setAuthConfig($key_location);
$client->setRedirectUri($redirect_uri);
$client->setScopes(array(
            'https://www.googleapis.com/auth/plus.login',
            'https://www.googleapis.com/auth/streetviewpublish',
            'https://www.googleapis.com/auth/youtube',
            'email',
            'profile'
        ));
$objOAuthService = new Google_Service_Oauth2($client);
$authUrl = $client->createAuthUrl();      
header('Location: '.$authUrl);
exit();