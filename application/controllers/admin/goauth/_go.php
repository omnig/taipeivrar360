<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("登入");

// if (isset($_SESSION['access_token'])) {
//     // echo json_encode($this->get_plus_me($_SESSION['access_token']));
//     // echo json_encode($this->get_g_accesstoken($_SESSION['access_token']));
//     // echo print_r($this->check_g_accesstoken($_SESSION['access_token']));
//     // exit();
//     // echo json_encode($_SESSION['access_token']);
//     // echo json_encode($_SESSION['access_token']);

//     //若過期會更新
//     $this->check_g_accesstoken($_SESSION['access_token']);
    

//     //Send Client Request
//     // $client = new Google_Client();
//     // $service = new Google_Service_Oauth2($client);
//     // $client->setAccessToken($_SESSION['access_token']);
//     // $user = $service->userinfo->get();

//     $response = curl_query("https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=".$_SESSION['access_token']['access_token'],'');
    

//     $response = json_decode($response,true);

//     $sa = $this->google_login($response["user_id"],$response["email"]);

//     if (!$sa) {
//         echo "登入失敗,請向管理員通知開通此帳號權限";
//         exit();
//     }else{
//         $this->session->set_userdata("login_sa", $sa);
//         // echo json_encode($_SESSION);
//         // exit();
//         header("Location: " . base_url("{$this->controller_name}/home"));
//         exit();
//     }
    
// }else 

if (isset($_GET['code'])) {

    //這是登出的請小心
    
    $access_token = $this->register_g_accesstoken($_GET['code']);

    //若過期會更新
    $this->check_g_accesstoken($_SESSION['access_token']);
    $response = curl_query("https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=".$_SESSION['access_token']['access_token'],'');

    $response = json_decode($response,true);


    //測試刷新token
    // $refresh_token = $this->refresh_g_accesstoken($_SESSION['access_token']['access_token']);
    // echo json_encode($refresh_token);
    // exit();

    $admin = $this->google_login($response["user_id"],$response["email"]);

    $this->session->set_userdata("login_admin", $admin);
    // echo json_encode($_SESSION);
    // exit();
    header("Location: " . base_url("{$this->controller_name}/home"));
    exit();


}else{
    echo "請檢查網路狀態";
}

exit();


