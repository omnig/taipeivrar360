<?php

include_once "__config.php";

//傳入值接收
$input_array = array(
    "type" => $this->input->post("type")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        exit();
    }
}

if (!$_FILES['file']['name']) {
    exit();
}


if ($_FILES['file']['error']) {

	$error_sting =  htmlentities($_FILES['file']['error'],  ENT_QUOTES,  "utf-8");
    
}

if($error_sting!=""&&$error_sting!=NULL){
	$message = 'Error: ' . $error_sting;
	print ($message);
    exit();
}


//上傳圖片處理
$upload_path = "upload/" . strtolower($input_array["type"]) . "/";
$file_type = 'gif|jpg|jpeg|png';
$max_size = 1024;
$resize_width = 1200;
$resize_height = 900;
$fit_type = "FIT_INNER_NO_ZOOM_IN";
$max_width = 2400;
$max_height = 1800;

//上傳商品圖
$field_name = "file";
$upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);

if (!isset($upload_result["upload_data"])) {
    exit();
}

echo $this->config->item("server_base_url") . "upload/" . strtolower($input_array["type"]) . "/" . $upload_result["upload_data"]["file_name"];
exit();
