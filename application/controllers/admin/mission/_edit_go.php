<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = $this->lang->line("編輯");

//傳入值接收
$input_array = array(
    'rw_id' => $this->input->post('rw_id'),
    "{$data["field_prefix"]}_id" => $this->input->post("{$data["field_prefix"]}_id"),
    "{$data["field_prefix"]}_title" => $this->input->post("{$data["field_prefix"]}_title"),
    "{$data["field_prefix"]}_describe" => $this->input->post("{$data["field_prefix"]}_describe"),
    "{$data["field_prefix"]}_start_time" => $this->input->post("{$data["field_prefix"]}_start_time"),
    "{$data["field_prefix"]}_end_time" => $this->input->post("{$data["field_prefix"]}_end_time"),
    "{$data["field_prefix"]}_enabled" => $this->input->post("{$data["field_prefix"]}_enabled")
);


foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤".$index));
        exit();
    }
}


//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one($data['table_name'], $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), $data["item_name"]));
    exit();
}

$update_array = array(
    'rw_id' => $input_array['rw_id'],
    "{$data["field_prefix"]}_title" => $input_array["{$data["field_prefix"]}_title"],
    "{$data["field_prefix"]}_describe" => $input_array["{$data["field_prefix"]}_describe"],
    "{$data["field_prefix"]}_start_time" => $input_array["{$data["field_prefix"]}_start_time"],
    "{$data["field_prefix"]}_end_time" => $input_array["{$data["field_prefix"]}_end_time"],
    "{$data["field_prefix"]}_enabled" => $input_array["{$data["field_prefix"]}_enabled"]
);


//上傳圖片處理
$upload_path = "upload/mission/";
$file_type = 'gif|jpg|jpeg|png';
$max_size = 2048;
$resize_width = 320;
$resize_height = 200;
$fit_type = "FIT_INNER";
$max_width = 320;
$max_height = 200;

//上傳商品圖
$field_name = "{$data["field_prefix"]}_img";
$upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);

if (isset($upload_result["upload_data"])) {
    //上傳成功，記錄上傳檔名
    $update_array[$field_name] = $upload_result["upload_data"]["file_name"];
} elseif ($upload_result["error"] != "upload_no_file_selected") {
    //錯誤原因若不是因為沒有上傳檔案，就要返回並顯示錯誤訊息
    $upload_error_msg_i18n = $this->lang->line("upload");
    _alert($upload_error_msg_i18n[$upload_result["error"]]);
    exit();
} elseif ($upload_result["error"] == "upload_no_file_selected") {

}

$this->Common_model->update_db($data['table_name'], $update_array, $where_array);

_alert_redirect('更新成功！', base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
