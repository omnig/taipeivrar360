<?php

include_once "__config.php";

$model_name = ucfirst($data['table_name']) . '_model';
$this->load->model($model_name);
$this->load->model('Reward_group_model');


//傳入值接收
$input_array = array(
    "limit" => $this->input->post('length'),
    "skip" => $this->input->post('start'),
    "draw" => intval($this->input->get_post('draw')),
    "columns" => $this->input->post('columns'),
    "order" => $this->input->post('order'),
    "filters" => $this->input->post('filters'),
    "customActionType" => $this->input->post('customActionType'),
    "customActionName" => $this->input->post('customActionName'),
    "id" => $this->input->post('id')
);


//資料分頁設定
$input_array['limit'] = intval($input_array['limit']) < 0 ? 0 : intval($input_array['limit']);
$input_array['skip'] = intval($this->input->get_post('start'));

$adm_group =  $this->session->login_admin->adm_group;
$isEditor =  $this->session->login_admin->adm_is_editor === 'Y';

$where_array = array(
    'ag_group' => $adm_group
);
$department = $this->Common_model->get_one("admin_group", $where_array);

$where_array = array("m.dep_id" => $this->login_admin->ag_id);

//依傳入參數過濾
if ($input_array['filters']) {
    $concat_where_string = "";

    foreach ($input_array['filters'] as $field_name => $value) {
        if ($value == '') {
            continue;
        }

        $where_string = "";

        switch ($field_name) {
            //比對完全相同的值
            case "{$data["field_prefix"]}_order" :
            case "{$data["field_prefix"]}_num" :
            case "rw_remainng" :
            case "{$data["field_prefix"]}_enabled" :
                $where_string = "({$field_name} = '{$value}')";
                break;
            //全文搜尋
            case "{$data["field_prefix"]}_title" :
            case "{$data["field_prefix"]}_describe" :
            case "rw_title":
                $where_string = "({$field_name} LIKE '%{$value}%')";
                break;
        }

        if ($concat_where_string == '') {
            $concat_where_string = $where_string;
        } else {
            $concat_where_string = "$concat_where_string AND $where_string";
        }
    }

    if ($concat_where_string != '') {
        $where_array[] = $concat_where_string;
    }
}

//排序欄位，對映order的排序欄位
$sort_fields = array(
    false, false, "{$data["field_prefix"]}_title", "{$data["field_prefix"]}_describe",false,false,"m_start_time","m_end_time", "rw_title", "rw_remainng", false, false
);

$order_by=array();
$order_by = "";
if (isset($input_array['order'])) {
  foreach ($input_array['order'] as $row) {
      if ($sort_fields[$row['column']]) {
          if ($order_by == '') {
              $order_by = "{$sort_fields[$row['column']]} {$row['dir']}";
              continue;
          }

          $order_by = "{$order},{$sort_fields[$row['column']]} {$sort_fields[$row['dir']]}";
      }
  }
}
//計算資料總數
$iTotalRecords = $this->$model_name->get_list($order_by, '', 0, 0, $where_array, true);


//從資料庫取得本次資料
$ret = $this->$model_name->get_list($order_by, '', $input_array['limit'], $input_array['skip'], $where_array, false);

// echo json_encode($ret );
// exit();

//回傳值
$records = array();
$records["data"] = array();

$status_list = array(
    'Y' => array("success" => $this->lang->line("啟用中")),
    'N' => array("danger" => $this->lang->line("已關閉"))
);

$edit_url = base_url("{$this->controller_name}/{$data['table_name']}/edit");
$del_url = base_url("{$this->controller_name}/{$data['table_name']}/del");
$img_url = base_url("upload/mission/").'/';
$nine_grid_url = base_url("{$this->controller_name}/nine_grid/");

//html表格內容
foreach ($ret as $i => $row) {
    //從資料庫取得獎勵內容
    $where_array = array(
      'rw_id' => $row->rw_id
    );
    $rward = $this->Reward_group_model->get_list('','','','',$where_array,false);
    $reward_title = isset($rward[0]) ? $rward[0]->rw_title : null;
    $reward_img = base_url("upload/reward_group/").'/'.(isset($rward[0]) ? $rward[0]->rw_img : null);
    $reward_remainng = isset($rward[0]) ? $rward[0]->rw_remainng : null;

    $is_enabled = ($row->{"{$data["field_prefix"]}_enabled"} == 'Y') ? 'Y' : 'N';
    $status = $status_list[$is_enabled];
    $id = $row->{"{$data["field_prefix"]}_id"};

    //取得九宮格已處理數
    $where_array = array(
      'm_id' => $id,
    );
    $order_by =array();
    $nine_grid_count = $this->$model_name->get_nine_grid($order_by, '', 0, 0, $where_array, false);


    // 九宮格燈號製作
    $grid_table = "";
    $grid_table .= '<div class="grid_wrapper"> ';
    $grid_table .= '<ul class="grid">';
    foreach ($nine_grid_count as $key => $value) {
      if (is_null($value->ap_id)) {
        $grid_table .= "<li class='no_grid' ;><a href='{$nine_grid_url}?{$data["field_prefix"]}_id={$row->{"{$data["field_prefix"]}_id"}}' ></a></li> ";
      }else{
        $grid_table .= "<li class='has_grid' ;><a href='{$nine_grid_url}?{$data["field_prefix"]}_id={$row->{"{$data["field_prefix"]}_id"}}' ></a></li> ";
      }
    }
    $grid_table .= '</ul>';
    $grid_table .= '</div>';
    //九宮格製作END


    $records["data"] [] = array(
        '<input type="checkbox" name="list_checkbox[]" value="' . $id . '">',
        $i + 1,
        $row->{"{$data["field_prefix"]}_title"},
        $row->{"{$data["field_prefix"]}_describe"},
        '<img src='.$img_url.$row->{"{$data["field_prefix"]}_img"}.' width="100px">',
        $grid_table,
        // $grid_table ."<div class='text-center'><a href='{$nine_grid_url}?{$data["field_prefix"]}_id={$row->{"{$data["field_prefix"]}_id"}}' class='btn btn-xs default blue-stripe btn-operating'><i class='fa fa-pencil'></i> 管理</a></div>",
        ($row->{"{$data["field_prefix"]}_start_time"}) ? date("Y/m/d H:i:s", strtotime($row->{"{$data["field_prefix"]}_start_time"})) : '',
        ($row->{"{$data["field_prefix"]}_end_time"}) ? date("Y/m/d H:i:s", strtotime($row->{"{$data["field_prefix"]}_end_time"})) : '',

        $reward_title.'<br><img src='.$reward_img.' width="100px">',
        $row->rw_code,
        $reward_remainng,
        '<span class="label label-sm label-' . (key($status)) . '">' . (current($status)) . '</span>',
        "<a href='{$edit_url}?{$data["field_prefix"]}_id={$row->{"{$data["field_prefix"]}_id"}}' class='btn btn-xs default blue-stripe btn-operating'><i class='fa fa-pencil'></i> 編輯</a>" .
        "<a class='btn btn-xs default red-stripe btn-operating delete' data='{$row->{"{$data["field_prefix"]}_id"}}'><i class='fa fa-remove'></i> {$this->lang->line("刪除")}</a>",
    );
}

$records["draw"] = $input_array["draw"];
$records["recordsTotal"] = $iTotalRecords;
$records["recordsFiltered"] = $iTotalRecords;

echo json_encode($records);
exit();
