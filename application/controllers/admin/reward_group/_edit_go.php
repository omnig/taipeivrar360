<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = $this->lang->line("編輯");

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->post("{$data["field_prefix"]}_id"),
    "{$data["field_prefix"]}_title" => $this->input->post("{$data["field_prefix"]}_title"),
    "{$data["field_prefix"]}_describe" => $this->input->post("{$data["field_prefix"]}_describe"),
    "{$data["field_prefix"]}_num" => $this->input->post("{$data["field_prefix"]}_num"),
    "{$data["field_prefix"]}_remainng" => $this->input->post("{$data["field_prefix"]}_remainng"),
    "{$data["field_prefix"]}_code" => $this->input->post("{$data["field_prefix"]}_code"),
    // "{$data["field_prefix"]}_start_time" => $this->input->post("{$data["field_prefix"]}_start_time"),
    // "{$data["field_prefix"]}_end_time" => $this->input->post("{$data["field_prefix"]}_end_time"),
    // "{$data["field_prefix"]}_expired_time" => $this->input->post("{$data["field_prefix"]}_expired_time")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}


//判斷獎勵數量不得小於獎勵剩餘
if ($input_array["{$data["field_prefix"]}_num"]<$input_array["{$data["field_prefix"]}_remainng"]) {
    _alert($this->lang->line("參數錯誤，獎勵量不可小於剩餘量"));
    exit();
}


//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one($data['table_name'], $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), $data["item_name"]));
    exit();
}

$update_array = array(
    "{$data["field_prefix"]}_title" => $input_array["{$data["field_prefix"]}_title"],
    "{$data["field_prefix"]}_describe" => $input_array["{$data["field_prefix"]}_describe"],
    "{$data["field_prefix"]}_num" => $input_array["{$data["field_prefix"]}_num"],
    "{$data["field_prefix"]}_remainng" => $input_array["{$data["field_prefix"]}_remainng"],
    "{$data["field_prefix"]}_code" => $input_array["{$data["field_prefix"]}_code"],
    "{$data["field_prefix"]}_start_time" => $this->input->post("{$data["field_prefix"]}_start_time"),
    "{$data["field_prefix"]}_end_time" => $this->input->post("{$data["field_prefix"]}_end_time"),
    "{$data["field_prefix"]}_expired_start_time" => $this->input->post("{$data["field_prefix"]}_expired_start_time"),
    "{$data["field_prefix"]}_expired_end_time" => $this->input->post("{$data["field_prefix"]}_expired_end_time"),
);

//上傳圖片處理
$upload_path = "upload/reward_group/";
$file_type = 'gif|jpg|jpeg|png';
$max_size = 2048;
$resize_width = 320;
$resize_height = 200;
$fit_type = "FIT_INNER";
$max_width = 320;
$max_height = 200;

//上傳商品圖
$field_name = "rw_image";
$upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);


    if (isset($upload_result["upload_data"])) {
        //上傳成功，記錄上傳檔名
        $update_array['rw_img'] = $upload_result["upload_data"]["file_name"];
    } elseif ($upload_result["error"] != "upload_no_file_selected") {
        //錯誤原因若不是因為沒有上傳檔案，就要返回並顯示錯誤訊息
        $upload_error_msg_i18n = $this->lang->line("upload");
        _alert($upload_error_msg_i18n[$upload_result["error"]]);
        exit();
    } else {
        //必須上傳檔案
    }

$this->Common_model->update_db($data['table_name'], $update_array, $where_array);

_alert_redirect('更新成功！', base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
