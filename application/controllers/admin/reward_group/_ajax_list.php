<?php

include_once "__config.php";

$model_name = ucfirst($data['table_name']) . '_model';
$this->load->model($model_name);

// echo json_encode($model_name);
// exit();
//傳入值接收
$input_array = array(
    "limit" => $this->input->post('length'),
    "skip" => $this->input->post('start'),
    "draw" => intval($this->input->get_post('draw')),
    "columns" => $this->input->post('columns'),
    "order" => $this->input->post('order'),
    "filters" => $this->input->post('filters'),
    "customActionType" => $this->input->post('customActionType'),
    "customActionName" => $this->input->post('customActionName'),
    "id" => $this->input->post('id')
);


//資料分頁設定
$input_array['limit'] = intval($input_array['limit']) < 0 ? 0 : intval($input_array['limit']);
$input_array['skip'] = intval($this->input->get_post('start'));

$where_array = array(
    "dep_id" => $this->login_admin->ag_id
);

//依傳入參數過濾
if ($input_array['filters']) {
    $concat_where_string = "";

    foreach ($input_array['filters'] as $field_name => $value) {
        if ($value == '') {
            continue;
        }

        $where_string = "";

        switch ($field_name) {
            //比對完全相同的值
            case "{$data["field_prefix"]}_order" :
            case "{$data["field_prefix"]}_num" :
            case "{$data["field_prefix"]}_remainng" :
                $where_string = "({$field_name} = '{$value}')";
                break;
            //全文搜尋
            case "{$data["field_prefix"]}_title" :
            case "{$data["field_prefix"]}_describe" :
                $where_string = "({$field_name} LIKE '%{$value}%')";
                break;
        }

        if ($concat_where_string == '') {
            $concat_where_string = $where_string;
        } else {
            $concat_where_string = "$concat_where_string AND $where_string";
        }
    }

    if ($concat_where_string != '') {
        $where_array[] = $concat_where_string;
    }
}

//排序欄位，對映order的排序欄位
$sort_fields = array(
    false, false, "{$data["field_prefix"]}_title_zh", false, "{$data["field_prefix"]}_describe", "{$data["field_prefix"]}_num", "{$data["field_prefix"]}_remainng", false
);

$order_by = "";
if (isset($input_array['order'])) {
  foreach ($input_array['order'] as $row) {
      if ($sort_fields[$row['column']]) {
          if ($order_by == '') {
              $order_by = "{$sort_fields[$row['column']]} {$row['dir']}";
              continue;
          }

          $order_by = "{$order},{$sort_fields[$row['column']]} {$sort_fields[$row['dir']]}";
      }
  }
}


//計算資料總數
$iTotalRecords = $this->$model_name->get_list($order_by, '', 0, 0, $where_array, true);


//從資料庫取得本次資料
$ret = $this->$model_name->get_list($order_by, '', $input_array['limit'], $input_array['skip'], $where_array, false);

//回傳值
$records = array();
$records["data"] = array();

$status_list = array(
    'Y' => array("success" => $this->lang->line("啟用中")),
    'N' => array("danger" => $this->lang->line("已關閉"))
);

$edit_url = base_url("{$this->controller_name}/{$data['table_name']}/edit");
$del_url = base_url("{$this->controller_name}/{$data['table_name']}/del");
$img_url = base_url("upload/reward_group/").'/';


//html表格內容
foreach ($ret as $i => $row) {

    $id = $row->{"{$data["field_prefix"]}_id"};

    $records["data"] [] = array(
        '<input type="checkbox" name="list_checkbox[]" value="' . $id . '">',
        $i + 1,
        $row->{"{$data["field_prefix"]}_title"},
        '<img src='.$img_url.$row->{"{$data["field_prefix"]}_img"}.' width="100px">',
        $row->{"{$data["field_prefix"]}_describe"},
        $row->rw_code,
        $row->{"{$data["field_prefix"]}_num"},
        $row->{"{$data["field_prefix"]}_remainng"},
        "<a href='{$edit_url}?{$data["field_prefix"]}_id={$row->{"{$data["field_prefix"]}_id"}}' class='btn btn-xs default blue-stripe btn-operating'><i class='fa fa-pencil'></i> 編輯</a>" .
        "<a class='btn btn-xs default red-stripe btn-operating delete' data='{$row->{"{$data["field_prefix"]}_id"}}'><i class='fa fa-remove'></i> {$this->lang->line("刪除")}</a>",
    );
}

$records["draw"] = $input_array["draw"];
$records["recordsTotal"] = $iTotalRecords;
$records["recordsFiltered"] = $iTotalRecords;

echo json_encode($records);
exit();
