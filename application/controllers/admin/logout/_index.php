<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("登出");

//清空登入session
$this->session->unset_userdata("login_admin");
//清空登入變數
$this->login_admin = null;
//清除目前編輯中店家紀錄
$this->session->unset_userdata("current_store");

//重導至登入頁
header("Location: " . base_url("{$this->controller_name}/login"));
exit();
