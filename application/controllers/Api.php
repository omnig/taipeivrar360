<?php

class Api extends CI_Controller {

    var $site_config;   //網頁一般設定
    var $controller_name = "api";
    var $page;  //目前頁面
    var $start_time;
    var $proxy_server;

    public function __construct() {
        parent::__construct();

        //載入model
        $this->load->model("Api_model");

        //載入helper
        $this->load->helpers('xml_helper');
        $this->load->helper('authorization');
        $this->load->helper('jwt');
        //載入config
        $this->site_config = $this->Common_model->get_one('config');

        //記錄開始時間
        $this->start_time = time();

        header('Access-Control-Allow-Origin: *');
        // $this->proxy_server = "http://210.200.141.175/proxy.php";
    }

    /*
     * 取得查詢url
     * 以此url進行api查詢
     */

    private function get_query_url($url) {
        //return $url;
        $query_url = $this->proxy_server . "?url=" . urlencode($url);
        return $query_url;
    }

    /*
     * 記錄呼叫結果
     * 傳入：
     *      result: 呼叫結果，Y/N
     *      error_message: 當結果為N時，附加錯誤訊息
     * 
     * 若傳入 result = 'N'，則程式在此即顯示錯誤訊息並結束
     */

    private function api_log($result = 'N', $error_message = '', $callback = '') {
        //取得來源IP
        $remote_ip = $this->input->server("HTTP_X_FORWARDED_FOR");
        if (!$remote_ip) {
            $remote_ip = $this->input->server("REMOTE_ADDR");
        }

        $insert_array = array(
            'la_api_name' => $this->page,
            'la_params' => json_encode(array(
                "get" => $this->input->get(),
                "post" => $this->input->post()
            )),
            'la_ip' => $remote_ip,
            'la_access_time' => time() - $this->start_time
        );

        if ($result != 'Y') {
            //呼叫失敗
            $insert_array['la_success'] = 'N';
            $insert_array['la_result'] = $error_message;

            //記錄
            $this->Common_model->insert_db("log_api", $insert_array);

            $ret = array(
                "result" => "false",
                "error_message" => $error_message,
                "data" => array()
            );

            if ($callback !== '') {
                echo "{$callback}(" . json_encode($ret) . ")";
            } else {
                echo json_encode($ret);
            }

            exit();
        }

        //呼叫成功，記錄
        $insert_array['la_success'] = 'Y';
        $insert_array['la_result'] = '';

        $this->Common_model->insert_db("log_api", $insert_array);
    }

    /*
     * 將到站時間轉為剩餘秒數
     * 傳入：
     *      $comeTime: 到站時間，格式為 H:i ，如 13:35
     */

    private function comeTime_to_second($comeTime) {
        $estimate_time = strtotime(date("Y-m-d ") . str_replace(" ", "", $comeTime) . ":00") - time();

        if ($estimate_time < 0) {
            return 0;
        }

        return $estimate_time;
    }

    /* ===============================
     * 記錄log
     * 
     * 傳入參數：
     * $api_name: API(或存取目標)名稱
     * $result: 處理結果
     * $success: 是否成功(Y/N)
     * =============================== */

    private function log($api_name, $result, $success, $input = false) {
        if (!$input) {
            $input = array(
                "get" => $this->input->get(),
                "post" => $this->input->post()
            );
        }

        $insert_array = array(
            "la_ip" => get_remote_addr(),
            "la_api_name" => $api_name,
            "la_params" => json_encode($input),
            "la_result" => $result,
            "la_success" => $success
        );

        $this->Common_model->insert_db("log_api", $insert_array);

        if ($success == 'N') {
            //顯示呼叫結果
            $ret = array(
                "result" => "false",
                "code" => 202,
                "error_message" => $result,
                "data" => array()
            );
            http_response_code(202);
            echo json_encode($ret);

            exit();
        }
    }

    /*
     * 驗證mac
     */

    private function verify_time($timestamp) {
        if (abs($timestamp - time()) > $this->config->item("api_timeout")) { //時間超過限制則視為失敗
            return false;
        }

        return true;
    }

    private function verify_mac($timestamp, $mac) {
        if (abs($timestamp - time()) > $this->config->item("api_timeout")) { //時間超過限制則視為失敗
            return false;
        }

        if (sha1($this->config->item('encrypt_token') . $timestamp) != $mac) {
            return false;
        }

        return true;
    }

    /*
     * 主功能頁面
     */

    public function view($page = 'home', $func = '') {
        if ($func != '') {
            if (file_exists(APPPATH . "/controllers/{$this->controller_name}/{$page}/{$func}.php")) {
                //載入特定功能函式
                include(APPPATH . "/controllers/{$this->controller_name}/{$page}/{$func}.php");
                exit();
            }
        }

        $this->page = $page;

        if (!file_exists(APPPATH . "/controllers/{$this->controller_name}/_{$page}.php")) {
            show_404();
        }
        include(APPPATH . "/controllers/{$this->controller_name}/_{$page}.php");
    }

    /* ==================================
     * 取得語言碼(zh / en)
     * ================================== */

    private function get_locale_code() {
        //若有傳入url參數，更改成傳入的語系
        $lang = strtolower($this->input->get('lang'));

        if (in_array($lang, array('zh', 'en'))) {
            //語系寫入session
            // $this->session->set_userdata(array('i18n' => $lang));

            return $lang;
        }

        //若session有記錄語系，使用session記錄語系
        // $lang = $this->session->i18n;

        if (!$lang) {
            //偵測語系
            $http_accept_language = strtolower($this->input->server('HTTP_ACCEPT_LANGUAGE'));

            //初始值，最後回傳最小者
            $en_index = 9999;
            $zh_index = 9998;

            $tmp = strpos($http_accept_language, 'zh');
            if ($tmp !== false && $tmp < $zh_index) {
                $zh_index = $tmp;
            }

            $tmp = strpos($http_accept_language, 'en');
            if ($tmp !== false && $tmp < $en_index) {
                $en_index = $tmp;
            }

            //將語言id最小者回傳
            $min_index = 10000;
            if ($en_index < $min_index) {
                $min_index = $en_index;
                $lang = 'en';
            }
            if ($zh_index < $min_index) {
                $lang = 'zh';
            }

            //記錄session
            // $this->session->set_userdata(array('i18n' => $lang));
        }

        return $lang;
    }

    /*
     * 密碼加密規則
     */

    private function encrypt_password($input) {
        return sha1($this->config->item('encrypt_token') . $input);
    }

    /*
     * 登入
     */

    private function login($account, $password,$device_id="") {
        $where_array = array(
            "u_email='{$account}'",
            "u_email_confirmed" => 'Y',
            "u_password" => $password,
            "u_enabled" => "Y",
            "u_del" => "N"
        );

        $user = $this->Common_model->get_one("user", $where_array);

        if (!$user) {
            return false;
        }

        //產生登入token
        $user->u_login_token = substr(md5(uniqid("utobonus", true)), 0, 40);

        if ($device_id!="") {
            //更新登入時間及token
            $update_array = array(
                "u_device_id" => $device_id,
                "u_login_token" => $user->u_login_token,
                'u_last_login_timestamp' => date('Y-m-d H:i:s')
            );
        }else{
            //更新登入時間及token
            $update_array = array(
                "u_login_token" => $user->u_login_token,
                'u_last_login_timestamp' => date('Y-m-d H:i:s')
            );
        }

        

        $where_array = array(
            'u_id' => $user->u_id
        );

        $this->Common_model->update_db("user", $update_array, $where_array);

        return $user;
    }

    /*
     * facebook 登入
     */

    private function fb_login($fb_id, $fb_name, $fb_email, $device_id) {

        //檢查FB是否存在
        $where_array = array(
            "u_fb_id" => $fb_id
        );
        $user = $this->Common_model->get_one("user", $where_array);

        //產生登入token
        $login_token = substr(md5(uniqid("utobonus", true)), 0, 40);

        if (!$user) {
            //檢查是否有相同device_id
            $where_array = array(
                'u_name' => 'UNREGISTERED',
                "u_device_id" => $device_id
            );
            $ret = $this->Common_model->get_one('user', $where_array);
            if ($ret) {
                //更新
                $where_array = array(
                    'u_id' => $ret->u_id
                );
                $update_array = array(
                    "u_name" => $fb_name,
                    "u_email" => $fb_email,
                    "u_email_confirmed" => "Y",
                    "u_fb_id" => $fb_id,
                    "u_fb_ts" => date("Y-m-d H:i:s"),
                    'u_login_token_app' => $login_token,
                    "u_device_id" => $device_id
                );
                $this->Common_model->update_db("user", $update_array, $where_array);
            } else {
                //註冊
                $insert_array = array(
                    "u_name" => $fb_name,
                    "u_email" => $fb_email,
                    "u_email_confirmed" => "Y",
                    "u_fb_id" => $fb_id,
                    "u_fb_ts" => date("Y-m-d H:i:s"),
                    'u_login_token_app' => $login_token,
                    "u_device_id" => $device_id
                );
                $this->Common_model->insert_db("user", $insert_array);
                $u_id = $this->Common_model->get_insert_id();
            }
            //取得FB註冊資料
            $where_array = array(
                "u_id" => $u_id
            );
            $user = $this->Common_model->get_one("user", $where_array);
        } else if ($user->u_del == "Y") {
            return false;
        } else {
            //更新登入時間及token
            $update_array = array(
                "u_email_confirmed" => "Y",
                'u_login_token_app' => $login_token,
                'u_device_id' => $device_id,
                'u_last_login_timestamp' => date('Y-m-d H:i:s')
            );
            $where_array = array(
                "u_id" => $user->u_id
            );
            $this->Common_model->update_db("user", $update_array, $where_array);
            $user = $this->Common_model->get_one('user', $where_array);
        }

        return $user;
    }
    

    /*
     * google 登入
     */

    private function google_login($google_id, $google_name, $google_email, $device_id) {

        //檢查是否存在
        $where_array = array(
            "u_google_id" => $google_id,
        );
        $user = $this->Common_model->get_one("user", $where_array);

        //產生登入token
        $login_token = substr(md5(uniqid("utobonus", true)), 0, 40);

        if (!$user) {
            //檢查是否有相同device_id
            $where_array = array(
                'u_name' => 'UNREGISTERED',
                "u_device_id" => $device_id
            );
            $ret = $this->Common_model->get_one('user', $where_array);
            if ($ret) {
                //更新
                $where_array = array(
                    'u_id' => $ret->u_id
                );
                $update_array = array(
                    "u_name" => $fb_name,
                    "u_email" => $fb_email,
                    "u_email_confirmed" => "Y",
                    "u_fb_id" => $fb_id,
                    "u_fb_ts" => date("Y-m-d H:i:s"),
                    'u_login_token_app' => $login_token,
                    "u_device_id" => $device_id
                );
                $this->Common_model->update_db("user", $update_array, $where_array);
            } else {
                //註冊
                $insert_array = array(
                    "u_name" => $google_name,
                    "u_email" => $google_email,
                    "u_email_confirmed" => "Y",
                    "u_google_id" => $google_id,
                    "u_google_ts" => date("Y-m-d H:i:s"),
                    'u_login_token_app' => $login_token,
                    "u_device_id" => $device_id
                );
                $this->Common_model->insert_db("user", $insert_array);
                $u_id = $this->Common_model->get_insert_id();
            }
            //取得google註冊資料
            $where_array = array(
                "u_id" => $u_id
            );
            $user = $this->Common_model->get_one("user", $where_array);
        } else if ($user->u_del == "Y") {
            return false;
        } else {
            //更新登入時間及token
            $update_array = array(
                "u_email_confirmed" => "Y",
                'u_email' => $google_email,
                'u_login_token_app' => $login_token,
                'u_device_id' => $device_id,
                'u_last_login_timestamp' => date('Y-m-d H:i:s')
            );
            $where_array = array(
                "u_id" => $user->u_id
            );
            $this->Common_model->update_db("user", $update_array, $where_array);
            $user = $this->Common_model->get_one('user', $where_array);
        }


        return $user;
    }

    /*
     * Apple 登入
     */

    private function apple_login($user_array, $user_name, $device_id) {

        //檢查是否存在
        $where_array = array(
            "u_apple_id" => $user_array->sub
        );
        $user = $this->Common_model->get_one("user", $where_array);

        //產生登入token
        $login_token = substr(md5(uniqid("utobonus", true)), 0, 40);

        if (!$user) {
            //檢查是否有相同device_id
            $where_array = array(
                'u_name' => 'UNREGISTERED',
                "u_device_id" => $device_id
            );
            $ret = $this->Common_model->get_one('user', $where_array);
            if ($ret) {
                //更新
                $u_id = $ret->u_id;
                $where_array = array(
                    'u_id' => $u_id
                );
                $update_array = array(
                    "u_name" => $user_name,
                    "u_email" => $user_array->email,
                    "u_email_confirmed" => "Y",
                    "u_apple_id" => $user_array->sub,
                    "u_apple_ts" => date("Y-m-d H:i:s"),
                    'u_login_token' => $login_token,
                    "u_device_id" => $device_id
                );
                $this->Common_model->update_db("user", $update_array, $where_array);
            } else {
                //註冊
                $insert_array = array(
                    "u_name" => $user_name,
                    "u_email" => $user_array->email,
                    "u_email_confirmed" => "Y",
                    "u_apple_id" => $user_array->sub,
                    "u_apple_ts" => date("Y-m-d H:i:s"),
                    'u_login_token' => $login_token,
                    "u_device_id" => $device_id
                );
                $this->Common_model->insert_db("user", $insert_array);
                $u_id = $this->Common_model->get_insert_id();
            }
            //重新取得註冊資料
            $where_array = array(
                "u_id" => $u_id
            );
            $user = $this->Common_model->get_one("user", $where_array);
        } else if ($user->u_del == "Y") {
            return false;
        } else {
            //更新登入時間及token
            $update_array = array(
                'u_login_token' => $login_token,
                'u_device_id' => $device_id,
                'u_last_login_timestamp' => date('Y-m-d H:i:s')
            );
            $where_array = array(
                "u_id" => $user->u_id
            );
            $this->Common_model->update_db("user", $update_array, $where_array);
            $user = $this->Common_model->get_one('user', $where_array);
        }

        return $user;
    }

    /*
     * 市民卡登入
     */


    private function city_card_api($rfid_keyout,$birthday){

        // $rfid_keyout = "9122115314770660";
        // $birthday = "20000939";

        $data = array();

        $data['rfid_keyout'] = $rfid_keyout;
        $data['birthday'] = $birthday ;

        $url = $this->config->item('webcard_url');
        $response = curl_query($url, $http_type = 'post',$data );//他是JSON

        $response = json_decode($response,TRUE);

        if (isset($response['entity_count'])) {
            if($response['entity_count']==0){
                //代表驗證失敗
                $response['result'] = FALSE;


                return $response;
            }
        }
        

        if (isset($response["exception"])) {
            echo json_encode($response);
            exit;
        }

        //代表驗證成功
        $response['result'] = TRUE;
        $response['entity_id'] = $response['basdat']['entities'][0]['entity_id'];
        unset($response['basdat']);
        return $response; 


    }

    private function citycard_login($rfid_keyout, $entity_id, $device_id) {

        //檢查是否存在
        $where_array = array(
            "rfid_keyout" => $rfid_keyout,
        );
        $user = $this->Common_model->get_one("user", $where_array);

        //產生登入token
        $login_token = substr(md5(uniqid("utobonus", true)), 0, 40);



        if (!$user) {
            
            //註冊
            $insert_array = array(
                "u_name" => "市民卡登入",
                "u_email" => "N/A",
                "u_email_confirmed" => "Y",
                "rfid_keyout" => $rfid_keyout,
                "entity_id" => $entity_id,
                'u_login_token_app' => $login_token,
                "u_device_id" => $device_id
            );
            $this->Common_model->insert_db("user", $insert_array);

            $u_id = $this->Common_model->get_insert_id();

            //取得市民卡註冊資料
            $where_array = array(
                "u_id" => $u_id
            );
            $user = $this->Common_model->get_one("user", $where_array);

        } else if ($user->u_del == "Y") {
            return false;
        } else {
            //更新登入時間及token
            $update_array = array(
                'u_login_token_app' => $login_token,
                'u_device_id' => $device_id,
                'u_last_login_timestamp' => date('Y-m-d H:i:s')
            );
            $where_array = array(
                "u_id" => $user->u_id
            );
            $this->Common_model->update_db("user", $update_array, $where_array);
            $user = $this->Common_model->get_one('user', $where_array);
        }


        return $user;
    }

    

    private function get_userlocation() {
        //取得使用者目前位置
        $user_location = $this->input->cookie("user_location");
        if ($user_location) {
            $latlon = explode(',', $user_location);
            $lat = $latlon[0];
            $lon = $latlon[1];
            $this->user_latlon = array(
                "lat" => $lat,
                "lon" => $lon
            );
        } else {
            $this->user_latlon = array(
                "lat" => 0,
                "lon" => 0
            );
        }
    }

    private function alert($caption = "", $content = "", $return_uri = "") {

        if ($return_uri == "") {
            $return_uri = $this->return_uri;
            //如果來自alert頁面，則跳轉返回時重導至首頁
            if (strpos($return_uri, base_url("alert")) !== false) {
                $return_uri = base_url("/");
            }
        }

        $alert = array(
            "caption" => $caption,
            "content" => $content,
            "return_uri" => $return_uri
        );

        // $this->session->set_userdata("alert", $alert);

        header("Location: " . base_url("alert"));
        exit();
    }

    private function get_fb_login_url() {
        $fb_config = $this->config->item("facebook");

        $fb = new Facebook\Facebook([
            'app_id' => $fb_config["app_id"],
            'app_secret' => $fb_config["app_secret"],
            'default_graph_version' => 'v2.4',
        ]);

        $helper = $fb->getRedirectLoginHelper();

        $loginUrl = $helper->getLoginUrl($this->config->item("server_base_url") . "fb-callback?return_uri=" . current_full_url(), $fb_config["permissions"]);

        return $loginUrl;
    }

    private function require_login() {
        if (!$this->login_user) {
            $caption = $this->lang->line("請先登入");
            $content = sprintf($this->lang->line("尚未登入或登入逾時，請重新登入。"));
            $redirect_uri = base_url("/");
            $this->alert($caption, $content, $redirect_uri);
            exit();
        }
    }

    private function get_order_number($o_id) {
        return sprintf("{$this->config->item("order_number_prefix")}%08d", $o_id);
    }

    private function get_o_id($order_number) {
        return (int) substr($order_number, 1);
    }

}
