<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Lesson extends \Restserver\Libraries\REST_Controller {


	function __construct()
	{	
		header('Content-Type: application/json');
	    // Construct the parent class
	    parent::__construct();
	    // header('Access-Control-Allow-Origin: http://vrar360.utobonus.com');
	    header("Access-Control-Allow-Methods: GET, POST ,OPTIONS");
	    // // Configure limits on our controller methods
	    // // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
	    // $this->methods['category_list_get']['limit'] = 5; // 500 requests per hour per user/key
	    $this->db = $this->load->database(ENVIRONMENT, TRUE);
	}



	//1.專有名詞清單
	public function list_get(){
        $lesson = array();

	    $this->db->select('*');
	    $this->db->from('tp_lesson');
	    $this->db->where("l_enabled", "Y");
        $this->db->where("l_del", "N");
	    $this->db->order_by('l_id', 'asc');

	    $query = $this->db->get();
        $lesson = $query->result();


	    
		$ret = array(
			"result"=>true,
			"error_message" =>"",
			"data" => $lesson
		);
		$this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK); 
        return;
	}


	


	//2.專有名詞詳細資料

	public function info_post(){
		$lesson = array();
		$id = $this->get('id');

		$this->db->select('*')
		         ->from('tp_lesson_steps')
		         ->where("s_enabled","Y")
		         ->where("s_del", "N")
		         ->where("l_id", $id)
		         ->order_by("s_order","asc");

	    		 
		$query = $this->db->get();

		if ($query) {
		    $lesson = $query->result();
		}


	    if (!$lesson) {
	    	$this->set_response("lesson not found", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
        	return;
	    }else{
	    	foreach ($lesson as $key => $value) {
	    		if ($lesson[$key]->s_image) {
	    			$lesson[$key]->s_image = $this->config->item("server_base_url").$value->s_image;
	    		}
	    		
	    	}
	    }

	    $this->db->select('*');
	    $this->db->from('tp_lesson');
	    $this->db->where("l_id", $id);
	    $this->db->limit(1);
	    $query = $this->db->get();

        $lesson_info = $query->result();

    	

		$ret = array(
			"result"=>true,
			"error_message" =>"",
			"data" => $lesson,
			"lession_info" => $lesson_info[0]
		);
		$this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK);
        return;
	}


}