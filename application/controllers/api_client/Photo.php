<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;



// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Photo extends \Restserver\Libraries\REST_Controller {


    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // header('Access-Control-Allow-Origin: http://vrar360.utobonus.com');
        // header('Access-Control-Allow-Origin: http://vr.ml-codesign.com');
        header("Access-Control-Allow-Methods: GET, POST ,OPTIONS");

        // // Configure limits on our controller methods
        // // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        // $this->methods['users_get']['limit'] = 5; // 500 requests per hour per user/key
        // $this->methods['users_post']['limit'] = 5; // 100 requests per hour per user/key
        $this->methods['post_post']['limit'] = 5;
        $this->load->helper('exif');
        // $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key


    }


    public function post_get(){
        header('Content-Type: application/json');
        //驗證 Token
        //JWT Token
        $headers = $this->input->request_headers();

        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {

            $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);

            //驗證Token 成功
            if ($decodedToken != false) {
                $this->set_response($decodedToken, \Restserver\Libraries\REST_Controller::HTTP_OK);

                $u_id = $decodedToken->u_id;

                //得到資料參數
                $input_array = array(
                    "id" => $this->input->get("id"),
                );
                
                //搜尋是否存在於資料庫
                $where_array = array(
                    'd_id'=>$u_id,
                    'dvp_id'=>$input_array['id'],
                    'is_civil'=>'Y'
                );
                $search_db = $this->Common_model->get_one("department_vr_photo",$where_array);

                $result = array();
                if (count($search_db)>0) {

                    $result['id'] = $search_db->dvp_id;
                    $result['topic'] = tid_return_topic_name($search_db->t_id);
                    

                    if($search_db->Is_url=="N"){
                        $result['vr_photo'] = $this->config->item('server_base_url').$search_db->dvp_img_path;
                    }else{
                        $result['vr_url'] = $search_db->dvp_view_link;
                    }  
                
                    $result['photo_type'] = $search_db->photo_type;
                    $result['name'] = $search_db->dvp_name;
                    $result['capture_timestamp'] = $search_db->dvp_capture_timestamp;                    
                    $result['show_image'] = $this->config->item('server_base_url').$search_db->dvp_rep_img_path;
                    $result['lat'] = $search_db->dvp_lat;
                    $result['lng'] = $search_db->dvp_lng;
                    
                    //關鍵字
                    //環景KeyWord 
                    $where_array = array(
                        "dvp_id" => $search_db->dvp_id
                    );
                    //get_key_word
                    $keywords = $this->Common_model->get_db_join("keyword_department_vr_photo as dvp", $where_array , "keywords as ks" , "dvp.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");
                    $kwd_names = array();
                    foreach ($keywords as $kwd_key => $kwdvalue) {
                        if ($kwdvalue->kwd_name!="") {
                            $kwd_names[$kwd_key] = $kwdvalue->kwd_name;
                        }
                    }

                    $result['keywords'] =$kwd_names;
                    $result['update_api'] = $this->config->item('server_base_url')."api_client/photo/update?id=".$search_db->dvp_id;

                    $this->set_response($result, \Restserver\Libraries\REST_Controller::HTTP_OK); 
                    return;


                }else{
                    # response error
                    $this->set_response("You don't have permission", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
                    return;
                }   
            }
        }
        $this->set_response("Token is Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);


    }
    public function post_post()
    {
        header('Content-Type: application/json');
        //驗證 Token
        //JWT Token
        $headers = $this->input->request_headers();


        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {

            $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);

            //驗證Token 成功
            if ($decodedToken != false) {
                $this->set_response($decodedToken, \Restserver\Libraries\REST_Controller::HTTP_OK);

                $u_id = $decodedToken->u_id;
                // echo json_encode($_REQUEST);
                // exit;

                //得到資料參數
                $input_array = array(
                    "t_id" => $this->input->post("t_id"),
                    "dvp_name" => $this->input->post("dvp_name"),
                    "dvp_capture_timestamp" => $this->input->post("dvp_capture_timestamp"),
                    "dvp_lat" => $this->input->post("dvp_lat"),
                    "dvp_lng" => $this->input->post("dvp_lng"),
                    "photo_type" => $this->input->post("photo_type")
                );


                //檢查參數
                foreach ($input_array as $index => $value) {
                    if ($value === null || $value === '') {
                       $message = [
                                    'status' => FALSE,
                                    'message' => 'INVALID VALUE =>'.$index
                                ];
                        $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                        return;
                    }
                }

                //檢查參數是否在範圍內
                if ($input_array['photo_type']=="pano" || $input_array['photo_type']=="width") {
                    
                }else{
                    $message = [
                                'status' => FALSE,
                                'message' => 'INVALID PHOTO TYPE VALUE'
                            ];
                    $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                    return;
                }

                //檢查是否在主題中
                $where_array = array(
                    't_id' => $input_array['t_id'],
                    't_public_photo' => 'Y'
                );

                $topic_result = $this->Common_model->get_one("topic", $where_array);


                if (!$topic_result) {
                    $message = [
                                    'status' => FALSE,
                                    'message' => 'Topic not allowed'
                                ];
                    $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                    return;   
                }

                //排除檢查項目
                $input_array['photo_url'] = $this->input->post("photo_url");

                //新增至資料庫基本值
                $insert_array = array(
                    't_id' => $input_array['t_id'],
                    "is_civil" => "Y",
                    "d_id" => $u_id,
                    'dvp_name' => $input_array['dvp_name'],
                    'dvp_capture_timestamp' => $input_array['dvp_capture_timestamp'],
                    'photo_type' => $input_array['photo_type'],
                    'dvp_lat' => $input_array['dvp_lat'],
                    'dvp_lng' => $input_array['dvp_lng'],
                    'dvp_state' => 'N',
                );


                //檢查檔案類別與大小
                //vrphoto

                if ($_FILES['show_img']['size']<10000000 && $_FILES['show_img']['type']=="image/jpeg" || $_FILES['show_img']['type']=="image/jpg"|| $_FILES['show_img']['type']=="image/png") {
                    
                }else{
                    $message = [
                                    'status' => FALSE,
                                    'message' => 'show_img File type or size not allowed'
                                ];
                    $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                    return;
                }

                //關鍵字長度檢查
                $tag_ary = $this->input->post("tag");

                if (count($tag_ary)>0 && count($tag_ary)<=5) {
                    foreach ($tag_ary as $tag_key => $tag_value) {
                        //檢查Key 長度
                        $message = [
                            'status' => FALSE,
                            'message' => 'tag標籤名稱太長，最多為10個字元'
                        ];
                        if (!valid_string_lenth($tag_value,10)){
                            $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                            return;
                        }
                    }
                }else{
                    $message = [
                        'status' => FALSE,
                        'message' => 'tag標籤名稱太多，最多為5個'
                    ];
                    $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                    return;

                }

                //處理圖片上傳
                if (isset($_FILES['vrphoto']['name'])) {
                    //處理VR 圖片
                    if ($_FILES['vrphoto']['size']<30000000 && $_FILES['vrphoto']['type']=="image/jpeg" || $_FILES['vrphoto']['type']=="image/jpg") {
                    
                    }else{
                        $message = [
                            'status' => FALSE,
                            'message' => 'vrphoto File type or size not allowed'
                        ];
                        $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                        return;
                    }

                    //環景圖上傳處理
                    $field_name = "vrphoto";
                    $upload_path = "upload/vrphoto/";
                    //相容windows路徑
                    $config['upload_path'] = FCPATH . $upload_path;
                    if (DIRECTORY_SEPARATOR == '\\') {
                        $config['upload_path'] = str_replace('/', '\\', $config['upload_path']);
                    }
                    if (!file_exists($config['upload_path'])) {
                        exec("mkdir " . $config['upload_path']);
                    }
                    $output_dir = $config['upload_path'];
                    $fileName = substr(md5(uniqid(rand())), 3, 12).".jpg";

                    $tmp_name = dirname($_FILES["vrphoto"]["tmp_name"])."/".basename($_FILES["vrphoto"]["tmp_name"]);


                    //上傳實體到資料夾
                    switch (ENVIRONMENT) {
                        case 'development' :
                        case 'test' :
                        // 假如是在Localhost
                        // 假如是在測試機
                            move_uploaded_file($tmp_name,$output_dir.$fileName);
                            break;

                        case 'api' :
                        // 假如是在API Server 
                            move_uploaded_file($tmp_name,$output_dir.$fileName);
                            $sftp = new SFTP('192.168.255.77');      //Prvite IP  For nmnsvs server (伺服器上測試用)
                            if (!$sftp->login('admin', 'admin123$')) {
                                throw new Exception('Login failed');
                            }else{
                                // echo "Login Success"."<br>";
                            }

                            //切換要上傳目錄夾
                            $sftp->chdir("/var/www/vrar360/upload/vrphoto/");


                            //PUT 檔案 P1: 上傳名稱  , P2: Local端檔案位置
                            $sftp->put($fileName, file_get_contents("/var/www/vrar360_api/upload/vrphoto/".$fileName));

                            break;

                        case 'production' :
                        // 判斷假如是在WEB Server
                        // 假如是在API Server 
                            move_uploaded_file($tmp_name,$output_dir.$fileName);
                            $sftp = new SFTP('192.168.255.80');      //Prvite IP  For nmnsvs server (伺服器上測試用)
                            if (!$sftp->login('admin', 'admin123$')) {
                                throw new Exception('Login failed');
                            }else{
                                // echo "Login Success"."<br>";
                            }

                            //切換要上傳目錄夾
                            $sftp->chdir("/var/www/vrar360_api/upload/vrphoto/");

                            //PUT 檔案 P1: 上傳名稱  , P2: Local端檔案位置
                            $sftp->put($fileName, file_get_contents("/var/www/vrar360/upload/vrphoto/".$fileName));

                            break;

                    }
                    



                    //VR 環景照 圖片路徑
                    $filename = $upload_path . $fileName;
                    //讀取EXIF

                    $exif = exif_read_data($filename, 0, true);
                    if(isset($exif['EXIF']['ExifImageLength'])){
                        $insert_array["dvp_height"] = $exif['EXIF']['ExifImageLength'];
                        $insert_array["dvp_width"] = $exif['EXIF']['ExifImageWidth'];
                    }else{
                        $insert_array["dvp_height"] = 0;
                        $insert_array["dvp_width"] = 0;
                    }
                    //新增資料庫函數

                    $insert_array["dvp_type"] = $_FILES["vrphoto"]["type"];
                    $insert_array["dvp_size"] = $_FILES["vrphoto"]["size"];

                    //改寫圖片EXIF 檔 複寫經緯度至photo
                    writ_photo_exif($filename,$input_array["dvp_lat"],$input_array["dvp_lng"]);

                    
                    //上傳至資料庫
                    $insert_array["dvp_backup"] = $filename;
                    $insert_array["dvp_img_path"] = $filename;
                    

                    //上傳影片代表圖片處理
                    $upload_path = "upload/image/";
                    $file_type = 'jpg|jpeg|png';
                    $max_size = 1024;
                    $resize_width = 264;
                    $resize_height = 156;
                    $fit_type = "FIT_OUTER";
                    $max_width = 1920;
                    $max_height = 1080;
                    //上傳圖片
                    $field_name = "show_img";
                    $upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);
                    if (!isset($upload_result["upload_data"])) {
                        //必須上傳圖片
                        _alert($this->lang->line("圖片上傳失敗或沒有上傳圖片"));
                        exit();
                    }
                    //上傳成功，記錄上傳檔名
                    $insert_array['dvp_rep_img_path'] = $upload_path.$upload_result["upload_data"]["file_name"];

                    if($insert_array["dvp_type"] == "width"){
                        $insert_array["dvp_view_link"] = $this->config->item('api_base_url').'kr_pano?image_path='.$this->config->item('api_base_url').$filename;
                    }else{
                        $insert_array["dvp_view_link"] = $this->config->item('api_base_url').'kr_spherical?image_path='.$this->config->item('api_base_url').$filename;
                    }
                    

                    //執行上傳資料庫
                    $this->Common_model->insert_db("department_vr_photo", $insert_array);

                    $dvp_id = $this->Common_model->get_insert_id();
                    //處理關鍵字
                    $tag_ary = $this->input->post("tag");

                    if (count($tag_ary)>0) {
                        foreach ($tag_ary as $tag_key => $tag_value) {

                            //檢查Key 是否存在
                            $where_array =  array('kwd_name' =>  $tag_value);
                            $tag_result = $this->Common_model->get_one("keywords", $where_array);

                            //先清空

                            if ($tag_result) {
                                //有的話Insert key 到relation
                                $insert_array = array(
                                    'dvp_id' => $dvp_id,
                                    'kwd_id' =>$tag_result->kwd_id);
                                $this->Common_model->insert_db("tp_keyword_department_vr_photo", $insert_array);
                            }else{
                                //沒有更新

                                $insert_array = array(
                                    'kwd_name' =>$tag_value);

                                $this->Common_model->insert_db("keywords", $insert_array);

                                $kwd_id = $this->Common_model->get_insert_id();

                                $insert_array = array(
                                    'dvp_id' => $dvp_id,
                                    'kwd_id' =>$kwd_id);
                                $this->Common_model->insert_db("tp_keyword_department_vr_photo", $insert_array);
                            }

                            if($tag_key>=5){
                                break;
                            }
                        }
                    }
                    $where_array =  array('t_id' =>  $input_array['t_id']);
                    $topic_result = $this->Common_model->get_one("topic", $where_array);

                    $response_array = [
                        'file_name' => $input_array['dvp_name'],
                        'topic' => $topic_result->t_name,
                        'file_type'=> $input_array['photo_type'],
                        'file_ext' => $_FILES["vrphoto"]["type"],
                        'file_size(byte)' => $_FILES["vrphoto"]["size"],
                    ];

                    $message = [
                        'status' => TRUE,
                        'message' => '上傳成功',
                        'data' => $response_array
                    ];
                    // CREATED (201) being the HTTP response code
                    $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_CREATED); 
                    return;
                }elseif(isset($input_array['photo_url'])){
                    //處理URL
                    $insert_array['Is_url'] = "Y";

                    $insert_array['dvp_view_link'] = $this->input->post('photo_url');
                    $insert_array['dvp_view_link'] = $this->security->xss_clean($insert_array['dvp_view_link']);

                    //上傳影片代表圖片處理
                    $upload_path = "upload/image/";
                    $file_type = 'jpg|jpeg|png';
                    $max_size = 1024;
                    $resize_width = 264;
                    $resize_height = 156;
                    $fit_type = "FIT_OUTER";
                    $max_width = 1920;
                    $max_height = 1080;
                    //上傳圖片
                    $field_name = "show_img";
                    $upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);
                    if (!isset($upload_result["upload_data"])) {
                        //必須上傳圖片
                        _alert($this->lang->line("圖片上傳失敗或沒有上傳圖片"));
                        exit();
                    }
                    //上傳成功，記錄上傳檔名
                    $insert_array['dvp_rep_img_path'] = $upload_path.$upload_result["upload_data"]["file_name"];
                    

                    //執行上傳資料庫
                    $this->Common_model->insert_db("department_vr_photo", $insert_array);

                    $dvp_id = $this->Common_model->get_insert_id();
                    //處理關鍵字
                    $tag_ary = $this->input->post("tag");

                    if (count($tag_ary)>0) {
                        foreach ($tag_ary as $tag_key => $tag_value) {

                            //檢查Key 是否存在
                            $where_array =  array('kwd_name' =>  $tag_value);
                            $tag_result = $this->Common_model->get_one("keywords", $where_array);

                            if ($tag_result) {
                                //有的話Insert key 到relation
                                $insert_array = array(
                                    'dvp_id' => $dvp_id,
                                    'kwd_id' =>$tag_result->kwd_id);
                                $this->Common_model->insert_db("tp_keyword_department_vr_photo", $insert_array);
                            }else{
                                //沒有更新

                                $insert_array = array(
                                    'kwd_name' =>$tag_value);

                                $this->Common_model->insert_db("keywords", $insert_array);

                                $kwd_id = $this->Common_model->get_insert_id();

                                $insert_array = array(
                                    'dvp_id' => $dvp_id,
                                    'kwd_id' =>$kwd_id);
                                $this->Common_model->insert_db("tp_keyword_department_vr_photo", $insert_array);
                            }

                            if($tag_key>=5){
                                break;
                            }
                        }
                    }
                    

                    $response_array = [
                        'file_name' => $input_array['dvp_name'],
                        'topic' => $topic_result->t_name,
                        'file_type'=> "URL",
                        'file_link' => $input_array['photo_url']
                    ];

                    $message = [
                        'status' => TRUE,
                        'message' => 'upload_success',
                        'data' => $response_array
                    ];
                    // CREATED (201) being the HTTP response code
                    $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_CREATED); 
                    return;
                }else{
                    //檢查Key 長度
                    $message = [
                        'status' => FALSE,
                        'message' => 'don`t hava any intput file'
                    ];

                    $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                    return;


                }


               
                // CREATED (201) being the HTTP response code
                $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_CREATED); 
                return;
            }



        }

        $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
        
    }

    public function del_post(){
        header('Content-Type: application/json');
        //驗證 Token
        //JWT Token
        $headers = $this->input->request_headers();

        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {

            $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);

            //驗證Token 成功
            if ($decodedToken != false) {
                $this->set_response($decodedToken, \Restserver\Libraries\REST_Controller::HTTP_OK);

                $u_id = $decodedToken->u_id;

                //得到資料參數
                $input_array = array(
                    "id" => $this->input->get("id"),
                );


                //搜尋是否存在於資料庫
                $where_array = array(
                    'd_id'=>$u_id,
                    'dvp_id'=>$input_array['id'],
                    'is_civil'=>'Y'
                );
                $search_db = $this->Common_model->get_one("department_vr_photo",$where_array);

                if (count($search_db)>0) {
                    $update_array= array(
                        'dvp_del' => 'Y'
                    );
                    
                    $this->Common_model->update_db("department_vr_photo",$update_array,$where_array);
                    
                    $this->set_response("Success", \Restserver\Libraries\REST_Controller::HTTP_OK); 
                    return;
                }else{
                    # response error
                    // $this->set_response("Fiale", \Restserver\Libraries\REST_Controller::HTTP_OK); 
                    $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
                    return;
                }   
            }
        }
        $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
    }

    public function update_post(){
        header('Content-Type: application/json');
        //驗證 Token
        //JWT Token
        $headers = $this->input->request_headers();

        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {

            $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);

            //驗證Token 成功
            if ($decodedToken != false) {
                $this->set_response($decodedToken, \Restserver\Libraries\REST_Controller::HTTP_OK);

                $u_id = $decodedToken->u_id;

                

                //得到資料參數
                $input_array = array(
                    "id" => $this->input->GET("id"),
                    "dvp_name" => $this->input->post("dvp_name"),
                    "dvp_capture_timestamp" => $this->input->post("dvp_capture_timestamp"),
                    "photo_type" => $this->input->post("photo_type")
                );


                //檢查參數是否在範圍內
                if ($input_array['photo_type']=="pano" || $input_array['photo_type']=="width") {
                    
                }else{
                    $message = [
                                'status' => FALSE,
                                'message' => 'INVALID PHOTO TYPE VALUE'
                            ];
                    $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                    return;
                }

                //搜尋是否存在於資料庫
                $where_array = array(
                    'd_id'=>$u_id,
                    'dvp_id'=>$input_array['id'],
                    'is_civil'=>'Y',
                    'dvp_del'=>'N',
                    'dvp_validation_result !='=>'N',
                    'dvp_validation_result !='=>'n/a'
                );
                $search_db = $this->Common_model->get_one("department_vr_photo",$where_array);

                $result = array();
                if (count($search_db)>0) {
                    //上傳編輯與刪除舊資料
                    

                    if ($input_array["dvp_name"]!="" && $input_array["dvp_name"]!=NULL) {
                        //更新名稱
                        $update_array = array(
                            'dvp_name' => $input_array["dvp_name"],
                            'dvp_validation'=>'N',
                            'dvp_validation_result '=>'n/a',
                            'dvp_state ' => 'N'
                        );
                        $this->Common_model->update_db("department_vr_photo",$update_array,$where_array);
                    }

                    if ($input_array["dvp_capture_timestamp"]!="" && $input_array["dvp_capture_timestamp"]!=NULL) {
                        //更新拍攝日期
                        $update_array = array(
                            'dvp_capture_timestamp' => $input_array["dvp_capture_timestamp"],
                            'dvp_validation'=>'N',
                            'dvp_validation_result '=>'n/a',
                            'dvp_state ' => 'N'
                        );
                        $this->Common_model->update_db("department_vr_photo",$update_array,$where_array);
                    }

                    if ($input_array["photo_type"]!="" && $input_array["photo_type"]!=NULL) {
                        //更新照片類別
                        
                        $update_array = array(
                            'photo_type' => $input_array["photo_type"],
                            'dvp_validation'=>'N',
                            'dvp_validation_result '=>'n/a',
                            'dvp_state ' => 'N'
                        );
                        $this->Common_model->update_db("department_vr_photo",$update_array,$where_array);
                    }

                    //更新關鍵字

                    $tag_ary = $this->input->post("tag");

                    if (count($tag_ary)>0) {
                        //先刪除先前的keyword
                        $where_array= array(
                            'dvp_id' => $input_array['id']
                        );

                        $this->Common_model->delete_db("tp_keyword_department_vr_photo",$where_array);

                        foreach ($tag_ary as $tag_key => $tag_value) {

                            //檢查Key 是否存在
                            $where_array =  array('kwd_name' =>  $tag_value);
                            $tag_result = $this->Common_model->get_one("keywords", $where_array);

                            //先清空

                            if ($tag_result) {
                                //有的話Insert key 到relation
                                $insert_array = array(
                                    'dvp_id' => $input_array['id'],
                                    'kwd_id' =>$tag_result->kwd_id);
                                $this->Common_model->insert_db("tp_keyword_department_vr_photo", $insert_array);
                            }else{
                                //沒有更新

                                $insert_array = array(
                                    'kwd_name' =>$tag_value);

                                $this->Common_model->insert_db("keywords", $insert_array);

                                $kwd_id = $this->Common_model->get_insert_id();

                                $insert_array = array(
                                    'dvp_id' => $input_array['id'],
                                    'kwd_id' =>$kwd_id);
                                $this->Common_model->insert_db("tp_keyword_department_vr_photo", $insert_array);
                            }

                            if($tag_key>=5){
                                break;
                            }
                        }



                    }


                    //更新顯示圖
                    //檢查檔案類別與大小
                    //vrphoto
                    //
                    
                    if (isset($_FILES['show_img'])) {

                        if ($_FILES['show_img']['size']<10000000 && $_FILES['show_img']['type']=="image/jpeg" || $_FILES['show_img']['type']=="image/jpg"|| $_FILES['show_img']['type']=="image/png") {
                            
                        }else{
                            $message = [
                                            'status' => FALSE,
                                            'message' => 'show_img File type or size not allowed'
                                        ];
                            $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                            return;
                        }

                        $old_img_path =  $search_db->dvp_rep_img_path;
                        //上傳影片代表圖片處理
                        $upload_path = "upload/image/";
                        $file_type = 'jpg|jpeg|png';
                        $max_size = 1024;
                        $resize_width = 264;
                        $resize_height = 156;
                        $fit_type = "FIT_OUTER";
                        $max_width = 1920;
                        $max_height = 1080;
                        //上傳圖片
                        $field_name = "show_img";
                        $upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);
                        if (!isset($upload_result["upload_data"])) {
                            //必須上傳圖片
                            _alert($this->lang->line("圖片上傳失敗或沒有上傳圖片"));
                            exit();
                        }
                        $update_array['dvp_rep_img_path'] = $upload_path.$upload_result["upload_data"]["file_name"];
                    }
                    //上傳成功，記錄上傳檔名
                    
                    $update_array['dvp_validation'] = 'N';
                    $update_array['dvp_validation_result'] = 'n/a';
                    $update_array['dvp_state'] = 'N';


                    // $update_array = array(
                    //         'dvp_rep_img_path' => $upload_path.$upload_result["upload_data"]["file_name"],
                    //         'dvp_validation'=>'N',
                    //         'dvp_validation_result '=>'n/a',
                    //         'dvp_state ' => 'N'
                    //     );


                    $where_array = array(
                        'd_id'=>$u_id,
                        'dvp_id'=>$input_array['id'],
                        'is_civil'=>'Y'
                    );
                    //更新
                    $this->Common_model->update_db("department_vr_photo",$update_array,$where_array);

                    //刪除資料庫舊的圖片
                    //刪除舊的圖片
                    if (isset($update_array['dvp_rep_img_path'])) {
                        $this->upload_lib->delete($old_img_path);
                    }
                    //搜尋最新資料
                    $search_db = $this->Common_model->get_one("department_vr_photo",$where_array);

                    //get_tag_array
                    $tags = get_tag_name("keyword_department_vr_photo","dvp_id" ,$search_db->dvp_id);


                    $response_array = [
                        'file_name' => $search_db->dvp_name,
                        'topic' => tid_return_topic_name($search_db->t_id),
                        'show_url' => $this->config->item('server_base_url').$search_db->dvp_rep_img_path,
                        'tags' => $tags,
                        'dvp_capture_timestamp' => $search_db->dvp_capture_timestamp,
                        'photo_type' => $search_db->photo_type
                        // 'file_type'=> "URL",
                        // 'file_link' => $input_array['photo_url']
                    ];

                    $message = [
                        'status' => TRUE,
                        'message' => '上傳成功',
                        'data' => $response_array
                    ];

                    $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_OK); 
                    return;


                }else{
                    # response error
                    $this->set_response("You don't have permission", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
                    return;
                }   
            }
        }
        $this->set_response("Token is Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);


    }




}
