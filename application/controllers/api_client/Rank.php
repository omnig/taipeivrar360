<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Rank extends \Restserver\Libraries\REST_Controller {


	function __construct()
	{	
		header('Content-Type: application/json');
	    // Construct the parent class
	    parent::__construct();
	    // header('Access-Control-Allow-Origin: http://vrar360.utobonus.com');
	    header("Access-Control-Allow-Methods: GET, POST ,OPTIONS");
	    // // Configure limits on our controller methods
	    // // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
	    // $this->methods['category_list_get']['limit'] = 5; // 500 requests per hour per user/key
	    $this->db = $this->load->database(ENVIRONMENT, TRUE);

	    $this->load->model("Share_rank_model");
	    $this->load->model("Creator_rank_model");
	    $this->load->model("Collect_rank_model");
	}



	//1.排行清單
	public function list_get(){
        $rank = array();

	   	$rank["share_rank"] = $this->Share_rank_model->get_union_list("share_rank asc","",3);
	   	$rank["collect_rank"] = $this->Collect_rank_model->get_union_list("collection_rank asc","",3);
	   	$rank["Creator_rank"] = $this->Creator_rank_model->get_union_list("creator_rank asc","",3);

	   	foreach ($rank["share_rank"] as $key => $value) {
	   		$rank["share_rank"][$key]->image = $this->config->item('server_base_url').$rank["share_rank"][$key]->image;
	   	}

	   	foreach ($rank["collect_rank"] as $key => $value) {
	   		$rank["collect_rank"][$key]->image = $this->config->item('server_base_url').$rank["collect_rank"][$key]->image;
	   	}

	    
		$ret = array(
			"result"=>true,
			"error_message" =>"",
			"data" => $rank
		);
		$this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK); 
        return;
	}


	


	//2.用使用者ID拿取使用者的上傳清單

	public function user_vr_post(){
		$user_vr = array();
		$user_id = $this->get('id');


		$vr_mixer = array();
		$vr_photo = $this->getting_vr_photo($user_id);
		$vr_video = $this->getting_vr_video($user_id);


		foreach ($vr_photo as $key => $value) {
			$vr_mixer[]=$value;
		}

		foreach ($vr_video as $key => $value) {
			$vr_mixer[]=$value;
		}
		

		usort($vr_mixer, function ($item1, $item2) {
		    return  $item2['timestamp'] <=> $item1['timestamp'];
		});

		usort($vr_mixer, function ($item1, $item2) {
		    return  $item2['collection_counter'] <=> $item1['collection_counter'];
		});



		$ret = array(
			"result"=>true,
			"error_message" =>"",
			"data" => $vr_mixer
		);
		$this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK);
        return;
	}


	    private function getting_vr_photo($user_id=""){
	    	//搜尋是否存在於資料庫
	    	$where_array = array(
	    	    'd_id'=>$user_id,
	    	    'dvp_state' => 'Y',
	    	    'dvp_del' => 'N'
	    	);
	    	$search_db = $this->Common_model->get_db("department_vr_photo",$where_array);


	    	$result = array();
	    	if ($search_db) {

	    		foreach ($search_db as $key => $value) {

		    	    $result[$key]['id'] = $value->dvp_id;
		    	    $result[$key]['type'] = "vr_photo";

		    	    
		    	    if($value->Is_url=="N"){
		    	        $result[$key]['vr_photo'] = $this->config->item('server_base_url').$value->dvp_img_path;
		    	    }else{
		    	        $result[$key]['vr_url'] = $value->dvp_view_link;
		    	    }  
		    	
		    	    $result[$key]['photo_type'] = $value->photo_type;
		    	    $result[$key]['name'] = $value->dvp_name;
		    	    $result[$key]['capture_time'] = $value->dvp_capture_timestamp;                    
		    	    $result[$key]['timestamp'] = strtotime($value->dvp_capture_timestamp);
		    	    $result[$key]['show_image'] = $this->config->item('server_base_url').$value->dvp_rep_img_path;
		    	    $result[$key]['lat'] = $value->dvp_lat;
		    	    $result[$key]['lng'] = $value->dvp_lng;
		    	    $result[$key]['view_counter'] = (int)$value->dvp_views;
		    	    $result[$key]['collection_counter'] = (int)$value->dvp_collection;
		    	    $result[$key]['share_counter'] = (int)$value->dvp_shares;

		    	    //判斷照片為環景或全景 URL異動
		    	    if ($value->photo_type=="pano") {
		    	    		$result[$key]['url'] =  $value->dvp_view_link;
		    	            $result[$key]['mobile_url'] =  $this->config->item('server_base_url').'/kr_spherical?image_path='.$this->config->item('server_base_url').$value->dvp_backup;
		    	    }else{
		    	            $result[$key]['url'] =  $value->dvp_view_link;
		    	            $result[$key]['mobile_url'] =  $this->config->item('server_base_url').'/kr_pano?image_path='.$this->config->item('server_base_url').$value->dvp_backup;
		    	    }

		    	    //直接吃連結
		    	    if($value->Is_url=="Y"){
		    	        $result[$key]['url'] = $value->dvp_view_link;
		    	        $result[$key]['mobile_url'] = $value->dvp_view_link;
		    	    }


		    	    //判斷是為局處或是民眾  使用者名稱異動
		    	    if ($value->is_civil=="Y") {
		    	        //民眾
		    	        $where_array = array(
		    	            'u_id' => $value->d_id
		    	        );

		    	        $the_user = $this->Common_model->get_one("user",$where_array);

		    	        if($the_user){
		    	            $result[$key]['uploader'] =  $the_user->u_name;
		    	        }else{
		    	            $result[$key]['uploader'] =  "Other";
		    	        }

		    	        
		    	    }else{
		    	        //局處

		    	        $where_array = array(
		    	            'ag_id' => $value->d_id
		    	        );

		    	        $the_department= $this->Common_model->get_one("tp_admin_group",$where_array);

		    	        if($the_department){
		    	            $result[$key]['uploader'] =  $the_department->ag_name;
		    	        }else{
		    	            $result[$key]['uploader'] =  "Other";
		    	        }

		    	    }
		    	}
	    	    

	    	    return $result;


	    	}

	    	return $result;
	    }

	    private function getting_vr_video($user_id=""){


	    	$this->db->select('t.d_id,t.dvv_id,t.dvv_name,t.dvv_rep_img_path,t.dvv_youtube_link,t.dvv_lat,t.dvv_lng,t.dvv_views,t.dvv_collection,t.dvv_capture_timestamp,t.is_civil,t.Is_url,u.u_name as u_name,a.ag_name as a_name');
	    	$this->db->from('tp_department_vr_video as t');
	    	$this->db->join("user as u", "u.u_id = t.d_id AND t.is_civil = 'Y'", "left");
	        $this->db->join("admin_group as a", "a.ag_id = t.d_id AND t.is_civil != 'Y'", "left");

	    	$where_array = array(
	    			't.d_id' => $user_id,
	    	        't.dvv_state' => 'Y',
	    	        't.dvv_del' => 'N'
	    	);
	    	foreach ($where_array as $index => $value) {
	    	    if (is_array($value)) {
	    	        $this->db->where_in($index, $value);
	    	    } elseif (is_numeric($index)) {
	    	        $this->db->where($value);
	    	    } else {
	    	        $this->db->where($index, $value);
	    	    }
	    	}
	    	$this->db->order_by("dvv_validation_timestamp","DESC");
	    	$query = $this->db->get();
	    	if($query){
	    	    $vr_video_list = $query->result();
	    	}else{
	    	    $vr_video_list = NULL;
	    	}

	    	$result = array();

	    	if ($query) {
	    		//整理資料
	    		foreach ($vr_video_list as $key => $value) {

	    		    $result[$key]['id'] =  $value->dvv_id;
	    		    $result[$key]['type'] =  "vr_video";
	    		    $web_link = str_replace("https://www.youtube.com/watch?v=", "", $value->dvv_youtube_link);
	    		    $mobile_link = str_replace("https://www.youtube.com/watch?v=", "", $value->dvv_youtube_link);
	    		    $result[$key]['vr_video'] =  "https://www.youtube.com/watch?v=".$web_link;
	    		    $result[$key]['mobile_vr_video'] =  "vnd.youtube:".$mobile_link;
	    		    $result[$key]['name'] =  $value->dvv_name;
	    		    

	    		    $result[$key]['capture_time'] =  $value->dvv_capture_timestamp;
	    		    $result[$key]['timestamp'] = strtotime($value->dvv_capture_timestamp);
	    		    $result[$key]['show_image'] =  $this->config->item('server_base_url').$value->dvv_rep_img_path;

	    		    $result[$key]['lat'] =  $value->dvv_lat;
	    		    $result[$key]['lng'] =  $value->dvv_lng;

	    		    $result[$key]['view_counter'] = (int)$value->dvv_views;
	    		    $result[$key]['collection_counter'] = (int)$value->dvv_collection;
	    		    $result[$key]['share_counter'] = (int)$value->dvv_shares;


	    		    //判斷是為局處或是民眾  使用者名稱異動
	    		    if ($value->is_civil=="Y") {
	    		        //民眾
	    		            $result[$key]['uploader'] =  $value->u_name;
	    		    }else{
	    		        //局處
	    		            $result[$key]['uploader'] =  $value->a_name;
	    		    }

	    		}
	    	}
	    	
	    	

	    	return $result;
	    }

}