<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Document extends \Restserver\Libraries\REST_Controller {


	function __construct()
	{	
		header('Content-Type: application/json');
	    // Construct the parent class
	    parent::__construct();
	    // header('Access-Control-Allow-Origin: http://vrar360.utobonus.com');
	    header("Access-Control-Allow-Methods: GET, POST ,OPTIONS");
	    // // Configure limits on our controller methods
	    // // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
	    // $this->methods['category_list_get']['limit'] = 5; // 500 requests per hour per user/key
	    $this->db = $this->load->database(ENVIRONMENT, TRUE);
	}



	//1.文章清單
	public function list_get(){
        $document = array();

	    $this->db->select('d.*, COUNT(c.c_id) as num_comments,u.u_name');
	    $this->db->from('tp_document as d');
        $this->db->join('tp_user as u','u.u_id=d.d_uid','left');
	    $this->db->where("d.d_vaild_state", "online");
	    $this->db->where("d.d_del", "N");
        $this->db->order_by('d.d_order', 'asc');
	    $this->db->order_by('d.d_update_time', 'desc');
	    $this->db->join('tp_comment as c', 'c.d_id = d.d_id','LEFT');
	    $this->db->group_by('d.d_id');

	    $query = $this->db->get();
        $document = $query->result();


	    if ($document) {
            foreach ($document as $key => $value) {
                $document[$key]->d_image = $this->config->item("server_base_url").$document[$key]->d_image;
                //返回指定字數
                $document[$key]->d_content = mb_substr($document[$key]->d_content,0,50);

                if ($value->u_name==NULL) {
                    $document[$key]->u_name = "Offical";
                }

                unset($document[$key]->d_comment_num);

                $where_array = array(
                    'd_id' => $document[$key]->d_id, 
                );

                $document[$key]->like_num = $this->Common_model->count_db("tp_user_document",$where_array);
            }
	    }else{
            $document = array();
        }



	    
		$ret = array(
			"result"=>true,
			"error_message" =>"",
			"data" => $document
		);
		$this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK); 
        return;
	}


	//2.文章新增
	public function add_post(){

		//JWT Token
		$headers = $this->input->request_headers();

		//檢驗
		if (!array_key_exists('Authorization', $headers) || empty($headers['Authorization'])) {
			$this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
			return;
		}

		$decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);

		//驗證Token 失敗
        if (!$decodedToken) {
        	$this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
        	return;
        }

        $u_id = $decodedToken->u_id;

        $input_array = array(
        	"title" => $this->post("title"),
        	"type" => $this->post("type"),
        	"content" => $this->post("content"),
        );

        foreach ($input_array as $index => $value) {
            if ($value === null || $value === '') {
                $this->set_response("[".$index ."] params is null.", \Restserver\Libraries\REST_Controller::HTTP_FORBIDDEN);
                return;
            }
        }
        $input_array["type"] = strtolower($input_array["type"]);
        $input_array["url"] =  $this->post("url")? $this->post("url"):"";
        

        // 檢驗類型
        if (!in_array($input_array["type"], array("ar","vr","mr","other"))) {
        	$this->set_response("tpye: [". $input_array["type"] ."]  is invalid.", \Restserver\Libraries\REST_Controller::HTTP_FORBIDDEN);
        	return;
        }

        // 檢驗字數
        if(mb_strlen($input_array["title"])>51){
        	$this->set_response("[ title ] words is too much.", \Restserver\Libraries\REST_Controller::HTTP_FORBIDDEN);
        	return;
        }


        $insert_array =array(
        	"d_title" => $input_array["title"],
        	"d_type" => $input_array["type"],
        	"d_content" => $input_array["content"],
        	"d_url" => $input_array["url"],
        	"is_civil" => "Y",
        	"d_uid" => $u_id
        );

        if (isset($_FILES['image'])) {

            if ($_FILES['image']['size']>1 && $_FILES['image']['size']<10000000 && $_FILES['image']['type']=="image/jpeg" || $_FILES['image']['type']=="image/jpg"|| $_FILES['image']['type']=="image/png") {
                
            }else{
                $message = [
                                'status' => FALSE,
                                'message' => 'image File type or size not allowed'
                            ];
                $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                return;
            }

            //上傳影片代表圖片處理
            $upload_path = "upload/image/";
            $file_type = 'jpg|jpeg|png';
            $max_size = 4096;
            $resize_width = 1024;
            $resize_height = 768;
            $fit_type = "FIT_OUTER";
            $max_width = 1024;
            $max_height = 768;
            //上傳圖片
            $field_name = "image";
            $upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);
            
            $insert_array['d_image'] = $upload_path.$upload_result["upload_data"]["file_name"];
        }else{
        	$this->set_response("no [ image ] upload.", \Restserver\Libraries\REST_Controller::HTTP_FORBIDDEN);
        	return;
        }

        $this->Common_model->insert_db("tp_document",$insert_array);

       	$document = array(
       		"id" => $this->Common_model->get_insert_id()
       	);

        $ret = array(
			"result"=>true,
			"error_message" =>"",
			"data" => $document
		);
		$this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_CREATED);
        return;
	}


	//3.文章詳細資料

	public function info_post(){
		$id = $this->get('id');

		$this->db->select('d.*,u.u_name, COUNT(ty_comment.c_id) as num_comments')
		         ->from('tp_document as d')
                 ->join('tp_user as u','u.u_id=d.d_uid','left')
		         // ->where("d_vaild_state", "online")
		         ->where("d_del", "N")
		         ->where("d.d_id", $id)
		         ->order_by('d.d_update_time', 'desc')
		         ->limit(1);
	    $this->db->join('tp_comment', 'tp_comment.d_id = d.d_id','left')
	    		 ->group_by('d.d_id');
	    		 
		$query = $this->db->get();

		if ($query) {
		    $document = $query->row();
		}else{
			$document = array();
		}


	    if (!$document) {
	    	$this->set_response("Document not found", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
        	return;
	    }
    	$document->d_image = $this->config->item("server_base_url").$document->d_image;

        $where_array = array(
            'd_id' => $id, 
        );

        $document->like_num = $this->Common_model->count_db("tp_user_document",$where_array);

    	//再加入留言

    	$comment = $this->Common_model->get_db_join_by_select(
    		"c.c_id,c.d_id,c.u_id,c.cc_id,c.c_comment,c.c_create_time,u.u_name",
    		"tp_comment as c",
    		array(
    			"c.d_id"=>$id,
    			"c.c_vaild_state"=>"online"),
    		"tp_user as u",
			"u.u_id = c.u_id",
			"left",
    		"c.c_create_time",
    		"desc"
    	);

    	$comment_tmp = array();

    	foreach ($comment as $key => $value) {
    		$comment_tmp[$value->c_id] = $value;
    	}

        foreach ($comment as $key => $value) {
            if ($value->cc_id!=0) {
                $comment_tmp[$value->cc_id]->reply[] = $value;
                unset($comment_tmp[$value->c_id]);
            }
        }

    	$comment_tmp = array_values($comment_tmp);

    	$document->comment = $comment_tmp;

        //JWT Token
        $headers = $this->input->request_headers();

        //檢驗是否登入判斷IS_LIKE
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);
             //有登入
            //驗證Token 成功
            if ($decodedToken != false) {

                $u_id = $decodedToken->u_id;

                $where_array = array(
                    'user_id' => $u_id,
                    'd_id' => $id, 
                );

                $is_like = $this->Common_model->count_db("tp_user_document",$where_array);
                $document->is_like = ($is_like)?"Y":"N";

            }
        }

        //更新+1
        $this->db->set('d_view_num', 'd_view_num+1', FALSE);
        $this->db->where('d_id', $id);
        $this->db->update('tp_document');

		$ret = array(
			"result"=>true,
			"error_message" =>"",
			"data" => $document
		);
		$this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK);
        return;
	}



	//4.文章留言新增

	public function comment_post(){
		$document_id = $this->post("id");
		$comment =  $this->post("comment");

		$input_array = array(
			"id" => $this->post("id"),
			"comment" => $this->post("comment")
		);

		foreach ($input_array as $index => $value) {
            if ($value === null || $value === '') {
                $this->set_response("[".$index ."] params is null.", \Restserver\Libraries\REST_Controller::HTTP_FORBIDDEN);
                return;
            }
        }

		//JWT Token
		$headers = $this->input->request_headers();

		//檢驗
		if (!array_key_exists('Authorization', $headers) || empty($headers['Authorization'])) {
			$this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
			return;
		}

		$decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);

		//驗證Token 失敗
        if (!$decodedToken) {
        	$this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
        	return;
        }

        $u_id = $decodedToken->u_id;


        //驗證發言

        $where_array = array(
            "u_id" => $u_id
        );

        $user = $this->Common_model->get_one("user",$where_array);

        if($user->u_comment=="N"){

            $comment = array(
                "message" => "User is being permission denied to comment document now."
            );

            $ret = array(
                "result"=>false,
                "error_message" =>"User is being permission denied to comment document now.",
                "data" => array()
            );
            $this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }

        $insert_array = array(
        	'd_id' => $input_array["id"],
        	'u_id' => $u_id,
        	'c_comment' =>  $input_array["comment"]
        );


        //  留言回覆
        if ($this->post("c_id")) {
        	$insert_array = array(
        		'd_id' => $input_array["id"],
        		'cc_id' => (int)$this->post("c_id"),
        		'u_id' => $u_id,
        		'c_comment' =>  $input_array["comment"]
        	);
        }

        $this->Common_model->insert_db("tp_comment",$insert_array);

       	$comment = array(
       		"id" => $this->Common_model->get_insert_id()
       	);

        $ret = array(
			"result"=>true,
			"error_message" =>"",
			"data" => $comment
		);
		$this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_CREATED); 
        return;      
	}

    //5.檢舉留言
    public function inform_post(){
        $c_id = $this->post("c_id");


        $input_array = array(
            "c_id" => $this->post("c_id"),
        );

        foreach ($input_array as $index => $value) {
            if ($value === null || $value === '') {
                $this->set_response("[".$index ."] params is null.", \Restserver\Libraries\REST_Controller::HTTP_FORBIDDEN);
                return;
            }
        }



        $where_array = array(
            'c_id' => $input_array["c_id"],
        );

        $document = $this->Common_model->get_one("tp_comment",$where_array);

        //  留言使用者
        if ($document) {
            //禁言模試

            $u_id = $document->u_id;
            $where_array =array(
                "u_id" => $u_id
            );
            $update_array =array(
                "u_comment" => "N"
            );
            $this->Common_model->update_db("tp_user",$update_array,$where_array);


            $where_array = array(
                'c_id' => $input_array["c_id"],
            );

            $update_array = array(
                'c_vaild_state' => 'vaild'
            );
            $this->Common_model->update_db("tp_comment",$update_array,$where_array);



        }

        

        $comment = array(
            "message" => "User is being permission denied to comment document now."
        );

        $ret = array(
            "result"=>true,
            "error_message" =>"",
            "data" => $comment
        );
        $this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_CREATED); 
        return;     
    }


    //6.恢復留言權限
    public function recovery_comment_post(){
        
        //JWT Token
        $headers = $this->input->request_headers();

        //檢驗
        if (!array_key_exists('Authorization', $headers) || empty($headers['Authorization'])) {
            $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }

        $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);

        //驗證Token 失敗
        if (!$decodedToken) {
            $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }

        $u_id = $decodedToken->u_id;

        $where_array =array(
            "u_id" => $u_id
        );
        $update_array =array(
            "u_comment" => "R"
        );
        $this->Common_model->update_db("tp_user",$update_array,$where_array);
        

        $comment = array(
            "message" => "User can comment document now."
        );

        $ret = array(
            "result"=>true,
            "error_message" =>"",
            "data" => $comment
        );
        $this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_CREATED); 
        return;     
    }


    //7.文章編輯
    public function edit_post(){


        $d_id = $this->get('id');

        //JWT Token
        $headers = $this->input->request_headers();

        //檢驗
        if (!array_key_exists('Authorization', $headers) || empty($headers['Authorization'])) {
            $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }

        $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);

        //驗證Token 失敗
        if (!$decodedToken) {
            $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }

        $u_id = $decodedToken->u_id;

        $input_array = array(
            "title" => $this->post("title"),
            "type" => $this->post("type"),
            "content" => $this->post("content")
        );

        foreach ($input_array as $index => $value) {
            if ($value === null || $value === '') {
                $this->set_response("[".$index ."] params is null.", \Restserver\Libraries\REST_Controller::HTTP_FORBIDDEN);
                return;
            }
        }
        $input_array["type"] = strtolower($input_array["type"]);
        $input_array["url"] = $this->post("url")?$this->post("url"):"";

        // 檢驗類型
        if (!in_array($input_array["type"], array("ar","vr","mr","other"))) {
            $this->set_response("tpye: [". $input_array["type"] ."]  is invalid.", \Restserver\Libraries\REST_Controller::HTTP_FORBIDDEN);
            return;
        }

        // 檢驗字數
        if(mb_strlen($input_array["title"])>51){
            $this->set_response("[ title ] words is too much.", \Restserver\Libraries\REST_Controller::HTTP_FORBIDDEN);
            return;
        }

        $where_array = array(
            "d_id" => $d_id,
            "d_uid" => $u_id
        );

        $document_obj = $this->Common_model->get_db("tp_document",$where_array);

        if (!$document_obj) {
            $this->set_response("[ document ] You don't have premission.", \Restserver\Libraries\REST_Controller::HTTP_FORBIDDEN);
            return;
        }

        $update_array =array(
            "d_title" => $input_array["title"],
            "d_type" => $input_array["type"],
            "d_content" => $input_array["content"],
            "d_url" => $input_array["url"],
            "d_vaild_state" => "d_vaild_state"
        );

        // echo json_encode($_FILES);
        // exit;

        if (isset($_FILES['image'])&&trim($_FILES['image'])!="") {

            if ($_FILES['image']['size']>1 && $_FILES['image']['size']<10000000 && $_FILES['image']['type']=="image/jpeg" || $_FILES['image']['type']=="image/jpg"|| $_FILES['image']['type']=="image/png") {
                
            }else{
                $message = [
                                'status' => FALSE,
                                'message' => 'image File type or size not allowed'
                            ];
                $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                return;
            }

            //上傳影片代表圖片處理
            $upload_path = "upload/image/";
            $file_type = 'jpg|jpeg|png';
            $max_size = 4096;
            $resize_width = 1024;
            $resize_height = 768;
            $fit_type = "FIT_OUTER";
            $max_width = 1024;
            $max_height = 768;
            //上傳圖片
            $field_name = "image";
            $upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);
            
            $update_array['d_image'] = $upload_path.$upload_result["upload_data"]["file_name"];
        }else{
            // $this->set_response("no [ image ] upload.", \Restserver\Libraries\REST_Controller::HTTP_FORBIDDEN);
            // return;
        }

        $this->Common_model->update_db("tp_document",$update_array,$where_array);




        $document = array(
            "document" => $this->Common_model->get_db("tp_document",$where_array)
        );

        $ret = array(
            "result"=>true,
            "error_message" =>"",
            "data" => $document
        );
        $this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_CREATED);
        return;
    }

    //8.文章刪除
    public function del_post(){


        $d_id = $this->get('id');

        //JWT Token
        $headers = $this->input->request_headers();

        //檢驗
        if (!array_key_exists('Authorization', $headers) || empty($headers['Authorization'])) {
            $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }

        $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);

        //驗證Token 失敗
        if (!$decodedToken) {
            $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }

        $u_id = $decodedToken->u_id;
        
        $where_array = array(
            "d_id" => $d_id,
            "d_uid" => $u_id
        );

        $document_obj = $this->Common_model->get_db("tp_document",$where_array);

        if (!$document_obj) {
            $this->set_response("[ document ] You don't have premission.", \Restserver\Libraries\REST_Controller::HTTP_FORBIDDEN);
            return;
        }

        $update_array =array(
            "d_del" => "Y",
        );


        $this->Common_model->update_db("tp_document",$update_array,$where_array);

        $document = array(
            "message" => "You delete the document now."
        );

        $ret = array(
            "result"=>true,
            "error_message" =>"",
            "data" => $document
        );
        $this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_CREATED);
        return;
    }

    //9.留言權限查詢
    public function check_comment_post(){
        
        //JWT Token
        $headers = $this->input->request_headers();

        //檢驗
        if (!array_key_exists('Authorization', $headers) || empty($headers['Authorization'])) {
            $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }

        $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);

        //驗證Token 失敗
        if (!$decodedToken) {
            $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }

        $u_id = $decodedToken->u_id;

        $where_array =array(
            "u_id" => $u_id
        );

        $user = $this->Common_model->get_one("tp_user",$where_array);

        $result = new stdClass();
        $result->comment_permission = $user->u_comment;

        $ret = array(
            "result"=>true,
            "error_message" =>"",
            "data" => $result
        );
        $this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_CREATED); 
        return;     
    }


    //10.文章分享
    public function user_share_post(){
        $id = $this->post('id');

        $data = array(
            "id" => $id
        );

        $data["meaage"] ="Share Success.";

        $this->db->set('d_share_num', 'd_share_num+1', FALSE);
        $this->db->where('d_id', $id);
        $this->db->update('tp_document');

        $ret = array(
            "result"=>true,
            "error_message" =>"",
            "data" => $data
        );
        $this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK); 


    }
}