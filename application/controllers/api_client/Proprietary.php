<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Proprietary extends \Restserver\Libraries\REST_Controller {


	function __construct()
	{	
		header('Content-Type: application/json');
	    // Construct the parent class
	    parent::__construct();
	    // header('Access-Control-Allow-Origin: http://vrar360.utobonus.com');
	    header("Access-Control-Allow-Methods: GET, POST ,OPTIONS");
	    // // Configure limits on our controller methods
	    // // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
	    // $this->methods['category_list_get']['limit'] = 5; // 500 requests per hour per user/key
	    $this->db = $this->load->database(ENVIRONMENT, TRUE);
	}



	//1.專有名詞清單
	public function list_get(){
        $proprietary = array();

	    $this->db->select('p_id,p_title,p_describe');
	    $this->db->from('tp_proprietary');
	    $this->db->where("p_status", "Y");
        $this->db->where("p_del", "N");
	    $this->db->order_by('p_id', 'asc');

	    $query = $this->db->get();
        $proprietary = $query->result();


	    if ($proprietary) {
            foreach ($proprietary as $key => $value) {
                //返回指定字數
                $proprietary[$key]->p_describe = mb_substr($proprietary[$key]->p_describe,0,100)."......";
            }
	    }else{
            $proprietary = array();
        }



	    
		$ret = array(
			"result"=>true,
			"error_message" =>"",
			"data" => $proprietary
		);
		$this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK); 
        return;
	}


	


	//2.專有名詞詳細資料

	public function info_post(){
		$id = $this->get('id');

		$this->db->select('*')
		         ->from('tp_proprietary')
		         ->where("p_del", "N")
		         ->where("p_id", $id)
                 ->limit(1);
	    		 
		$query = $this->db->get();

		if ($query) {
		    $proprietary = $query->row();
		}else{
			$proprietary = array();
		}


	    if (!$proprietary) {
	    	$this->set_response("proprietary not found", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
        	return;
	    }
    	

		$ret = array(
			"result"=>true,
			"error_message" =>"",
			"data" => $proprietary
		);
		$this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK);
        return;
	}


}