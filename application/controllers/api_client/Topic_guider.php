<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Topic_guider extends \Restserver\Libraries\REST_Controller {


    function __construct()
    {	
    	header('Content-Type: application/json');
        // Construct the parent class
        parent::__construct();
        // header('Access-Control-Allow-Origin: http://vrar360.utobonus.com');
        header("Access-Control-Allow-Methods: GET, POST ,OPTIONS");
        // // Configure limits on our controller methods
        // // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        // $this->methods['category_list_get']['limit'] = 5; // 500 requests per hour per user/key
        $this->db = $this->load->database(ENVIRONMENT, TRUE);
    }

    //1.主題類別清單
    public function category_list_get(){
        $category = $this->Common_model->get_db("tp_topic_category",array("tc_enabled"=>"Y"),"tc_order");
        $ret = array(
        	"result"=>true,
        	"error_message" =>"",
        	"data" => $category
        );
        $this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK); 
    }


    //2.主題清單
    public function topic_list_post(){


    	$topic_name_filter = $this->post('search_words');
    	$topic_category    = $this->post('category');
    	$topic_category_ary    = explode(",", $topic_category);

    	if ($topic_category=="") {
    		$topic = $this->topic_filter_function($topic_name_filter);
    	}else{
    		$topic = $this->topic_filter_function($topic_name_filter,$topic_category_ary);
    	}
    	

        //JWT Token
        $headers = $this->input->request_headers();

        //檢驗是否登入判斷IS_LIKE
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);
             //有登入
            //驗證Token 成功
            if ($decodedToken != false) {

                $u_id = $decodedToken->u_id;


                foreach ($topic as $key => $value) {

                    $where_array = array(
                        'user_id' => $u_id,
                        't_id' => $value->t_id, 
                    );

                    $is_like = $this->Common_model->count_db("tp_user_topic",$where_array);
                    $topic[$key]->is_like = ($is_like)?"Y":"N";

                }

            }
        }

    	$ret = array(
    		"result"=>true,
    		"error_message" =>"",
    		"data" => $topic
    	);
    	$this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK); 
    }


    //3.取得VR資訊
    public function topic_vr_list_post(){

    	$t_id = $this->post('t_id');

    	$vr_mixer = array();
    	$vr_photo = $this->getting_vr_photo($t_id);
    	$vr_video = $this->getting_vr_video($t_id);


    	foreach ($vr_photo as $key => $value) {
    		$vr_mixer[]=$value;
    	}

    	foreach ($vr_video as $key => $value) {
    		$vr_mixer[]=$value;
    	}
    	

    	usort($vr_mixer, function ($item1, $item2) {
    	    return  $item2['timestamp'] <=> $item1['timestamp'];
    	});

    	usort($vr_mixer, function ($item1, $item2) {
    	    return  $item2['collection_counter'] <=> $item1['collection_counter'];
    	});


    	$ret = array(
    		"result"=>true,
    		"error_message" =>"",
    		"data" => $vr_mixer
    	);
    	$this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK); 

    }


    //4.使用者分享
    public function user_share_post(){
    	$type = $this->post('type');
    	$id = $this->post('id');

    	$data = array(
    		"type" => $type,
    		"id" => $id
    	);

    	
    	$data["meaage"] ="Update Success.";
    	switch ($type) {
    		case 'topic':
    			$this->db->set('t_shares', 't_shares+1', FALSE);
				$this->db->where('t_id', $id);
    			$this->db->update('tp_topic');
    			break;
    		case 'photo':
    			$this->db->set('dvp_shares', 'dvp_shares+1', FALSE);
				$this->db->where('dvp_id', $id);
    			$this->db->update('tp_department_vr_photo');
    			break;
    		case 'video':
    			$this->db->set('dvv_shares', 'dvv_shares+1', FALSE);
				$this->db->where('dvv_id', $id);
    			$this->db->update('tp_department_vr_video ');
    			break;
    		
    		default:
    			$data["meaage"] ="Update Fail(type error).";
    			break;
    	}

    	

    	$ret = array(
    		"result"=>true,
    		"error_message" =>"",
    		"data" => $data
    	);
    	$this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK); 
    }


    private function topic_filter_function($topic_name_filter="",$topic_category=array()){
    	$category = $this->Common_model->get_db("tp_topic_category",array("tc_enabled"=>"Y"),"tc_order");
    	$category_name = array();
    	foreach ($category as $key => $value) {
    		$category_name[$value->tc_id] = $value->tc_title_zh;
    	}



    	$topic = $this->Common_model->get_db("tp_topic",array("t_enabled"=>"Y","t_del"=>"N"),"t_views", 'asc',0, 0, array("t_name"=>$topic_name_filter));

    	foreach ($topic as $key => $value) {
    		$topic[$key]->t_image = $this->config->item("server_base_url")."upload/topic_image/".$topic[$key]->t_image; 
            $topic[$key]->t_bk_image = $this->config->item("server_base_url")."upload/topic_image/".$topic[$key]->t_bk_image; 
    		if($topic[$key]->tc_id==0){
    			$topic[$key]->tc_id = array();

    		}else{
    			$topic[$key]->tc_id = explode(",", $topic[$key]->tc_id);

    			//顯示類別名稱
    			foreach ($topic[$key]->tc_id as $tc_key => $tc_value) {
    				$topic[$key]->tc_name[$tc_value] = $category_name[$tc_value];
    			}

    		}
    		unset($topic[$key]->t_type);
    		unset($topic[$key]->t_topic_link);
    		unset($topic[$key]->t_public_photo);
    		unset($topic[$key]->t_public_video);
    		unset($topic[$key]->t_public_ar);
    		unset($topic[$key]->t_enabled);
    		unset($topic[$key]->t_del);
    		unset($topic[$key]->t_ar_app_link);
    		unset($topic[$key]->t_organizer);
    		unset($topic[$key]->t_co_organizer);
    		unset($topic[$key]->ab_buid);
    		unset($topic[$key]->t_create_timestamp);
    		unset($topic[$key]->t_update_timestamp);

    		$topic[$key]->t_views = (int)$topic[$key]->t_views;
    		$topic[$key]->t_shares = (int)$topic[$key]->t_shares;
    		$topic[$key]->t_likes = $this->Common_model->count_db("user_topic",array("t_id"=>$topic[$key]->t_id));
    	}

    	//有類別塞選
		if($topic_category){
			$topic_filter = array();
			foreach ($topic as $key => $value) {
				foreach ($topic_category as $tc_key => $tc_value) {
					if(in_array($tc_value, $value->tc_id)){

						$topic_filter[] = $topic[$key];
						break;
					}
				}
			}
			$topic = $topic_filter;
		}

    	
    	return $topic;
    }

    private function getting_vr_photo($t_id=""){
    	//搜尋是否存在於資料庫
    	$where_array = array(
    	    't_id'=>$t_id,
    	    'dvp_state' => 'Y',
    	    'dvp_del' => 'N'
    	);
    	$search_db = $this->Common_model->get_db("department_vr_photo",$where_array);


    	$result = array();
    	if ($search_db) {

    		foreach ($search_db as $key => $value) {

	    	    $result[$key]['id'] = $value->dvp_id;
	    	    $result[$key]['type'] = "vr_photo";

	    	    
	    	    if($value->Is_url=="N"){
	    	        $result[$key]['vr_photo'] = $this->config->item('server_base_url').$value->dvp_img_path;
	    	    }else{
	    	        $result[$key]['vr_url'] = $value->dvp_view_link;
	    	    }  
	    	
	    	    $result[$key]['photo_type'] = $value->photo_type;
	    	    $result[$key]['name'] = $value->dvp_name;
	    	    $result[$key]['capture_time'] = $value->dvp_capture_timestamp;                    
	    	    $result[$key]['timestamp'] = strtotime($value->dvp_capture_timestamp);
	    	    $result[$key]['show_image'] = $this->config->item('server_base_url').$value->dvp_rep_img_path;
	    	    $result[$key]['lat'] = $value->dvp_lat;
	    	    $result[$key]['lng'] = $value->dvp_lng;
	    	    $result[$key]['view_counter'] = (int)$value->dvp_views;
	    	    $result[$key]['collection_counter'] = (int)$value->dvp_collection;

	    	    //判斷照片為環景或全景 URL異動
	    	    if ($value->photo_type=="pano") {
	    	    		$result[$key]['url'] =  $value->dvp_view_link;
	    	            $result[$key]['mobile_url'] =  $this->config->item('server_base_url').'/kr_spherical?image_path='.$this->config->item('server_base_url').$value->dvp_backup;
	    	    }else{
	    	            $result[$key]['url'] =  $value->dvp_view_link;
	    	            $result[$key]['mobile_url'] =  $this->config->item('server_base_url').'/kr_pano?image_path='.$this->config->item('server_base_url').$value->dvp_backup;
	    	    }

	    	    //直接吃連結
	    	    if($value->Is_url=="Y"){
	    	        $result[$key]['url'] = $value->dvp_view_link;
	    	        $result[$key]['mobile_url'] = $value->dvp_view_link;
	    	    }


	    	    //判斷是為局處或是民眾  使用者名稱異動
	    	    if ($value->is_civil=="Y") {
	    	        //民眾
	    	        $where_array = array(
	    	            'u_id' => $value->d_id
	    	        );

	    	        $the_user = $this->Common_model->get_one("user",$where_array);

	    	        if($the_user){
	    	            $result[$key]['uploader'] =  $the_user->u_name;
	    	        }else{
	    	            $result[$key]['uploader'] =  "Other";
	    	        }

	    	        
	    	    }else{
	    	        //局處

	    	        $where_array = array(
	    	            'ag_id' => $value->d_id
	    	        );

	    	        $the_department= $this->Common_model->get_one("tp_admin_group",$where_array);

	    	        if($the_department){
	    	            $result[$key]['uploader'] =  $the_department->ag_name;
	    	        }else{
	    	            $result[$key]['uploader'] =  "Other";
	    	        }

	    	    }
	    	}
    	    

    	    return $result;


    	}

    	return $result;
    }

    private function getting_vr_video($t_id=""){


    	$this->db->select('t.d_id,t.dvv_id,t.dvv_name,t.dvv_rep_img_path,t.dvv_youtube_link,t.dvv_lat,t.dvv_lng,t.dvv_views,t.dvv_collection,t.dvv_capture_timestamp,t.is_civil,t.Is_url,u.u_name as u_name,a.ag_name as a_name');
    	$this->db->from('tp_department_vr_video as t');
    	$this->db->join("user as u", "u.u_id = t.d_id AND t.is_civil = 'Y'", "left");
        $this->db->join("admin_group as a", "a.ag_id = t.d_id AND t.is_civil != 'Y'", "left");

    	$where_array = array(
    			't.t_id' => $t_id,
    	        't.dvv_state' => 'Y',
    	        't.dvv_del' => 'N'
    	);
    	foreach ($where_array as $index => $value) {
    	    if (is_array($value)) {
    	        $this->db->where_in($index, $value);
    	    } elseif (is_numeric($index)) {
    	        $this->db->where($value);
    	    } else {
    	        $this->db->where($index, $value);
    	    }
    	}
    	$this->db->order_by("dvv_validation_timestamp","DESC");
    	$query = $this->db->get();
    	if($query){
    	    $vr_video_list = $query->result();
    	}else{
    	    $vr_video_list = NULL;
    	}

    	$result = array();

    	if ($query) {
    		//整理資料
    		foreach ($vr_video_list as $key => $value) {

    		    $result[$key]['id'] =  $value->dvv_id;
    		    $result[$key]['type'] =  "vr_video";
    		    $web_link = str_replace("https://www.youtube.com/watch?v=", "", $value->dvv_youtube_link);
    		    $mobile_link = str_replace("https://www.youtube.com/watch?v=", "", $value->dvv_youtube_link);
    		    $result[$key]['vr_video'] =  "https://www.youtube.com/watch?v=".$web_link;
    		    $result[$key]['mobile_vr_video'] =  "vnd.youtube:".$mobile_link;
    		    $result[$key]['name'] =  $value->dvv_name;
    		    

    		    $result[$key]['capture_time'] =  $value->dvv_capture_timestamp;
    		    $result[$key]['timestamp'] = strtotime($value->dvv_capture_timestamp);
    		    $result[$key]['show_image'] =  $this->config->item('server_base_url').$value->dvv_rep_img_path;

    		    $result[$key]['lat'] =  $value->dvv_lat;
    		    $result[$key]['lng'] =  $value->dvv_lng;

    		    $result[$key]['view_counter'] = (int)$value->dvv_views;
    		    $result[$key]['collection_counter'] = (int)$value->dvv_collection;


    		    //判斷是為局處或是民眾  使用者名稱異動
    		    if ($value->is_civil=="Y") {
    		        //民眾
    		            $result[$key]['uploader'] =  $value->u_name;
    		    }else{
    		        //局處
    		            $result[$key]['uploader'] =  $value->a_name;
    		    }

    		}
    	}
    	
    	

    	return $result;
    }
}