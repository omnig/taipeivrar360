<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Ar extends \Restserver\Libraries\REST_Controller {


    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // header('Access-Control-Allow-Origin: http://vrar360.utobonus.com');
        // header('Access-Control-Allow-Origin: http://vr.ml-codesign.com');
        header("Access-Control-Allow-Methods: GET, POST ,OPTIONS");

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['post_post']['limit'] = 5; // 5 requests per hour per user/key

    }


    public function post_get(){
        header('Content-Type: application/json');
        //驗證 Token
        //JWT Token
        $headers = $this->input->request_headers();

        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {

            $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);

            //驗證Token 成功
            if ($decodedToken != false) {
                $this->set_response($decodedToken, \Restserver\Libraries\REST_Controller::HTTP_OK);

                $u_id = $decodedToken->u_id;

                //得到資料參數
                $input_array = array(
                    "id" => $this->input->get("id"),
                );


                //搜尋是否存在於資料庫
                $where_array = array(
                    'd_id'=>$u_id,
                    'tdar_id'=>$input_array['id'],
                    'is_civil'=>'Y'
                );
                $search_db = $this->Common_model->get_one("department_ar",$where_array);

                $result = array();
                if (count($search_db)>0) {

                    $result['id'] = $search_db->tdar_id;
                    $result['topic'] = tid_return_topic_name($search_db->t_id);
                    
                    $result['ar_photo'] = $this->config->item('server_base_url')."upload_image/".$search_db->tdar_image_path;
                    $result['ar_find_photo'] = $this->config->item('server_base_url')."upload/image/".$search_db->tdar_findpho_path;

                    $result['ar_angle'] = $search_db->tdar_angle;

                    $result['ar_content_type'] = $search_db->tdar_content_type;

                    if ($result['ar_content_type']=="text") {
                        $result['ar_content'] = $search_db->tdar_content_path;

                    }elseif($result['ar_content_type']=="image"){
                        $result['ar_content'] = $this->config->item('server_base_url')."upload/image/".$search_db->tdar_content_path;

                    }elseif($result['ar_content_type']=="video"){
                        $result['ar_content'] = $this->config->item('server_base_url')."upload_video/".$search_db->tdar_content_path;

                    }else{
                        $result['ar_content'] = $this->config->item('server_base_url')."upload_model/".$search_db->tdar_content_path;

                    }

                    

                    $result['name'] = $search_db->tdar_name;

                    $result['capture_timestamp'] = $search_db->tdar_capture_timestamp;                    

                    $result['lat'] = $search_db->tdar_lat;
                    $result['lng'] = $search_db->tdar_lng;
                    
                    //關鍵字
                    //環景KeyWord 
                    $where_array = array(
                        "ar_id" => $search_db->tdar_id
                    );
                    //get_key_word
                    $keywords = $this->Common_model->get_db_join("keyword_department_ar as tdar", $where_array , "keywords as ks" , "tdar.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");
                    $kwd_names = array();
                    foreach ($keywords as $kwd_key => $kwdvalue) {
                        if ($kwdvalue->kwd_name!="") {
                            $kwd_names[$kwd_key] = $kwdvalue->kwd_name;
                        }
                    }

                    $result['keywords'] =$kwd_names;
                    $result['update_api'] = $this->config->item('server_base_url')."api_client/video/update?id=".$search_db->tdar_id;

                    $this->set_response($result, \Restserver\Libraries\REST_Controller::HTTP_OK); 
                    // $this->set_response($search_db, \Restserver\Libraries\REST_Controller::HTTP_OK); 
                    return;


                }else{
                    # response error
                    $this->set_response("You don't have permission", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
                    return;
                }   
            }
        }
        $this->set_response("Token is Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);


    }
    public function post_post()
    {
        header('Content-Type: application/json');
        //驗證 Token
        //JWT Token
        $headers = $this->input->request_headers();

        // echo json_encode($_FILES);
        // exit;

        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {

            $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);

            //驗證Token 成功
            if ($decodedToken != false) {
                $this->set_response($decodedToken, \Restserver\Libraries\REST_Controller::HTTP_OK);

                $u_id = $decodedToken->u_id;

                //得到資料參數
                $input_array = array(
                    "t_id" => $this->input->post("t_id"),
                    "tdar_name" => $this->input->post("tdar_name"),
                    "tdar_capture_timestamp" => $this->input->post("tdar_capture_timestamp"),
                    "tdar_lat" => $this->input->post("tdar_lat"),
                    "tdar_lng" => $this->input->post("tdar_lng"),
                    "content_type" => $this->input->post("content_type")
                );


                //檢查參數
                foreach ($input_array as $index => $value) {
                    if ($value === null || $value === '') {
                       $message = [
                                    'status' => FALSE,
                                    'message' => 'INVALID VALUE =>'.$index
                                ];
                        $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                        return;
                    }
                }
                $input_array['video_url'] = $this->input->post("video_url");

                //新增至資料庫基本值
                $insert_array = array(
                    't_id' => $input_array['t_id'],
                    "is_civil" => "Y",
                    "d_id" => $u_id,
                    'tdar_name' => $input_array['tdar_name'],
                    'tdar_capture_timestamp' => $input_array['tdar_capture_timestamp'],
                    'tdar_lat' => $input_array['tdar_lat'],
                    'tdar_lng' => $input_array['tdar_lng'],
                    'tdar_state' => 'N',
                );


                //檢查檔案類別與大小

                //AR 辨識圖片
                if ($_FILES['ar_photo']['size']<11000000) {
                    
                }else{
                    $message = [
                                    'status' => FALSE,
                                    'message' => 'ar_photo File type or size not allowed (dosn`t use png)s'
                                ];
                    $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                    return;
                }

                if ($_FILES['ar_photo']['type']=="image/jpeg" || $_FILES['ar_photo']['type']=="image/jpg" || $_FILES['ar_photo']['type']=="image/JPEG" || $_FILES['ar_photo']['type']=="image/JPG") {
                    
                }else{
                    $message = [
                                    'status' => FALSE,
                                    'message' => 'ar_photo File type or size not allowed (dosn`t use png)t'.$_FILES['ar_photo']['type']
                                ];
                    $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                    return;
                }

                //AR 尋找圖片
                if ($_FILES['show_img']['size']<11000000) {
                    
                }else{
                    $message = [
                                    'status' => FALSE,
                                    'message' => 'show_img File type or size not allowed'
                                ];
                    $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                    return;
                }

                if ($_FILES['show_img']['type']=="image/jpeg" || $_FILES['show_img']['type']=="image/jpg"|| $_FILES['show_img']['type']=="image/png") {
                    
                }else{
                    $message = [
                                    'status' => FALSE,
                                    'message' => 'show_img File type or size not allowed'
                                ];
                    $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                    return;
                }
                //檢查是否在主題中
                $where_array = array(
                    't_id' => $input_array['t_id'],
                    't_public_ar' => 'Y'
                );

                $topic_result = $this->Common_model->get_one("topic", $where_array);


                if (!$topic_result) {
                    $message = [
                                    'status' => FALSE,
                                    'message' => 'Topic not allowed'
                                ];
                    $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                    return;   
                }

                //關鍵字長度檢查
                $tag_ary = $this->input->post("tag");

                if (count($tag_ary)>0 && count($tag_ary)<=5) {
                    foreach ($tag_ary as $tag_key => $tag_value) {
                        //檢查Key 長度
                        $message = [
                            'status' => FALSE,
                            'message' => 'tag標籤名稱太長，最多為10個字元'
                        ];
                        if (!valid_string_lenth($tag_value,10)){
                            $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                            return;
                        }
                    }
                }else{
                    $message = [
                        'status' => FALSE,
                        'message' => 'tag標籤名稱太多，最多為5個'
                    ];
                    $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                    return;

                }

                /******************************************
                    上傳ＡＲ 呈現內容
                *******************************************/
                //判斷圖/文/影/3D上傳
                $insert_array["tdar_content_type"] = $input_array["content_type"];
                switch ($input_array["content_type"]) {
                    case 'text':
                        # 直接上傳文字
                        
                        if ($this->input->post("content_text")== "" || $this->input->post("content_text") == NULL){
                                $message = [
                                    'status' => FALSE,
                                    'message' => 'Content text cant`t be null'
                                ];
                                $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                                return;

                        }


                        if (!valid_string_lenth($this->input->post("content_text"),50)){
                            $message = [
                                'status' => FALSE,
                                'message' => 'Content text can`t more than 50 characters'
                            ];
                            $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                            return;
                        }
                        $insert_array["tdar_content_type"] = $this->input->post("content_type");
                        $insert_array["tdar_content_path"] = $this->input->post("content_text");
                        break;

                    case 'image':
                        # 上傳圖片路徑
                        //上傳前檢查
                        if ($_FILES['content_file']['size']<11000000 && $_FILES['content_file']['type']=="image/jpeg" || $_FILES['content_file']['type']=="image/jpg"|| $_FILES['content_file']['type']=="image/png") {
                            
                        }else{
                            $message = [
                                'status' => FALSE,
                                'message' => 'content_file File type or size not allowed'
                            ];
                            $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                            return;
                        }

                        // //上傳圖片處理
                        $upload_path = "upload/image/";
                        $file_type = 'gif|jpg|jpeg|png';
                        $max_size = 4096;
                        $resize_width = 264;
                        $resize_height = 156;
                        $fit_type = "FIT_OUTER";
                        $max_width = 1920;
                        $max_height = 1080;

                        //上傳代表圖片
                        $field_name = "content_file";
                        $upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);

                        if (!isset($upload_result["upload_data"])) {
                            //必須上傳圖片
                            echo ($this->lang->line("圖片上傳失敗或沒有上傳圖片"));
                            exit();
                        }
                        //上傳成功，記錄上傳檔名
                        $insert_array["tdar_content_path"] = $upload_result["upload_data"]["file_name"];
                        break;

                    case 'video':

                        //上傳前檢查
                        if ($_FILES['content_file']['size']<31000000 && $_FILES['content_file']['type']=="video/mp4") {
                            
                        }else{
                            $message = [
                                'status' => FALSE,
                                'message' => 'content_file File type or size not allowed'
                            ];
                            $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                            return;
                        }


                        # 上傳影片路徑
                        $upload_path = "upload_video/";
                        $file_type = 'mp4';
                        $max_size = 31000000;
                        $field_name = "content_file";
                        $upload_result = $this->upload_lib->write_from_video($upload_path, $field_name, $file_type, $max_size);


                        switch (ENVIRONMENT) {
                            case 'development' :
                            case 'test' :
                            // 假如是在Localhost
                            // 假如是在測試機
                                break;

                            case 'api' :
                            // 假如是在API Server 
                                $sftp = new SFTP('192.168.255.77');      //Prvite IP  For nmnsvs server (伺服器上測試用)
                                if (!$sftp->login('admin', 'admin123$')) {
                                    throw new Exception('Login failed');
                                }else{
                                    // echo "Login Success"."<br>";
                                }
                                //切換要上傳目錄夾
                                $sftp->chdir("/var/www/vrar360/upload_video/");

                                //PUT 檔案 P1: 上傳名稱  , P2: Local端檔案位置
                                $sftp->put(basename($upload_result["upload_data"]["file_name"]), file_get_contents("/var/www/vrar360_api/upload_video/".basename($upload_result["upload_data"]["file_name"])));



                                break;

                            case 'production' :
                            // 判斷假如是在WEB Server
                            // 假如是在API Server 
                                $sftp = new SFTP('192.168.255.80');      //Prvite IP  For nmnsvs server (伺服器上測試用)
                                if (!$sftp->login('admin', 'admin123$')) {
                                    throw new Exception('Login failed');
                                }else{
                                    // echo "Login Success"."<br>";
                                }

                                //切換要上傳目錄夾
                                $sftp->chdir("/var/www/vrar360_api/upload_video/");

                                //PUT 檔案 P1: 上傳名稱  , P2: Local端檔案位置
                                $sftp->put(basename($upload_result["upload_data"]["file_name"]), file_get_contents("/var/www/vrar360/upload_video/".basename($upload_result["upload_data"]["file_name"])));

                                break;

                        }

                        if (!isset($upload_result["upload_data"])) {
                            //必須上傳影片
                            echo ("影片上傳失敗，或沒有上傳影片");
                            exit();
                        }
                        //上傳成功，記錄上傳檔名
                        $insert_array["tdar_content_path"] = $upload_result["upload_data"]["file_name"];
                        break;

                    case '3d_model':
                        //上傳前檢查
                        if ($_FILES['content_file']['size']<51000000 && preg_replace('/^.*\.([^.]+)$/D', '$1', $_FILES['content_file']['name'])=='fbx') {
                            
                        }else{
                            $message = [
                                'status' => FALSE,
                                'message' => '3D content_file File type or size not allowed'
                            ];
                            $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                            return;
                        }

                        if ($this->input->post("content_degree") == 90 || $this->input->post("content_degree") == 180) {
                            
                        }else{
                            $message = [
                                'status' => FALSE,
                                'message' => 'content_degree is null or not allowed'
                            ];
                            $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                            return;
                        }

                        # 上傳3D 路徑
                        $upload_path = "upload_model/";
                        $file_type = 'fbx';
                        $max_size = 300000;
                        $field_name = "content_file";
                        $upload_result = $this->upload_lib->write_from_model($upload_path, $field_name, $file_type, $max_size);
                        if (!isset($upload_result["upload_data"])) {
                            //必須上傳影片
                            echo ("3D模型上傳失敗，或沒有上傳3D模型");
                            exit();
                        }


                        //上傳成功，記錄上傳檔名
                        $insert_array["tdar_content_path"] = $upload_result["upload_data"]["file_name"];
                        $insert_array["tdar_angle"] = $this->input->post("content_degree");

                        break;

                }

                /******************************************
                上傳ＡＲ 圖片
                ******************************************/
                //{$data["field_prefix"]}_image_path
                // //上傳圖片處理
                $upload_path = "upload_image/";
                $file_type = 'jpg|jpeg|png';
                $max_size = 11000; //10M

                //上傳代表圖片
                $field_name = "ar_photo";

                $upload_result = $this->upload_lib->write_from_image($upload_path, $field_name, $file_type, $max_size);


                if (!isset($upload_result["upload_data"])) {
                    //必須上傳圖片
                    echo ($this->lang->line("圖片上傳失敗或沒有上傳圖片"));
                    exit();
                }
                //上傳成功，記錄上傳檔名
                $insert_array["tdar_image_path"] = $upload_result["upload_data"]["file_name"];
                $insert_array["tdar_size"] = $_FILES['ar_photo']['size'];

                /******************************************
                上傳ＡＲ 尋找圖片
                *******************************************/

                //{$data["field_prefix"]}_findpho_path
                //上傳圖片處理
                $upload_path = "upload/image/";
                $file_type = 'gif|jpg|jpeg|png';
                $max_size = 11000;
                $resize_width = 264;
                $resize_height = 156;
                $fit_type = "FIT_OUTER";
                $max_width = 1920;
                $max_height = 1080;

                //上傳代表圖片
                $field_name = "show_img";
                $upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);


                if (!isset($upload_result["upload_data"])) {
                    //必須上傳圖片
                    echo ($this->lang->line("圖片上傳失敗或沒有上傳圖片"));
                    exit();
                }
                //上傳成功，記錄上傳檔名
                $insert_array["tdar_findpho_path"] = $upload_result["upload_data"]["file_name"];


                //執行上傳資料庫
                    $this->Common_model->insert_db("department_ar", $insert_array);

                    $ar_id = $this->Common_model->get_insert_id();
                    //處理關鍵字
                    $tag_ary = $this->input->post("tag");

                    if (count($tag_ary)>0) {
                        foreach ($tag_ary as $tag_key => $tag_value) {

                            //檢查Key 是否存在
                            $where_array =  array('kwd_name' =>  $tag_value);
                            $tag_result = $this->Common_model->get_one("keywords", $where_array);

                            if ($tag_result) {
                                //有的話Insert key 到relation
                                $insert_array = array(
                                    'ar_id' => $ar_id,
                                    'kwd_id' =>$tag_result->kwd_id);
                                $this->Common_model->insert_db("tp_keyword_department_ar", $insert_array);
                            }else{
                                //沒有更新

                                $insert_array = array(
                                    'kwd_name' =>$tag_value);

                                $this->Common_model->insert_db("keywords", $insert_array);

                                $kwd_id = $this->Common_model->get_insert_id();

                                $insert_array = array(
                                    'ar_id' => $ar_id,
                                    'kwd_id' =>$kwd_id);
                                $this->Common_model->insert_db("tp_keyword_department_ar", $insert_array);
                            }
                        }
                    }

                    $response_array = [
                        'file_name' => $input_array['tdar_name'],
                        'topic' => $topic_result->t_name,
                        'file_type'=> "AR",
                        'file_ext' => preg_replace('/^.*\.([^.]+)$/D', '$1', $_FILES['ar_photo']['name']),
                        'file_size(byte)' => $_FILES["ar_photo"]["size"],
                    ];

                    $message = [
                        'status' => TRUE,
                        'message' => 'upload_success',
                        'data' => $response_array
                    ];
                    // CREATED (201) being the HTTP response code
                    $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_CREATED); 
                    return;
               
                // CREATED (201) being the HTTP response code
                $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_CREATED); 
                return;
            }



        }

        $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
        
    }

    public function del_post(){
        header('Content-Type: application/json');
        //驗證 Token
        //JWT Token
        $headers = $this->input->request_headers();

        // echo json_encode($_FILES);
        // exit;

        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {

            $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);

            //驗證Token 成功
            if ($decodedToken != false) {
                $this->set_response($decodedToken, \Restserver\Libraries\REST_Controller::HTTP_OK);

                $u_id = $decodedToken->u_id;

                //得到資料參數
                $input_array = array(
                    "id" => $this->input->GET("id"),
                );

                //搜尋是否存在於資料庫
                $where_array = array(
                    'd_id'=>$u_id,
                    'tdar_id'=>$input_array['id'],
                    'is_civil'=>'Y'
                );
                $search_db = $this->Common_model->get_one("department_ar",$where_array);

                if (count($search_db)>0) {
                    $update_array= array(
                        'tdar_del' => 'Y'
                    );
                    
                    $this->Common_model->update_db("department_ar",$update_array,$where_array);
                    
                    $this->set_response("Success", \Restserver\Libraries\REST_Controller::HTTP_OK); 
                    return;
                }else{
                    # response error
                    // $this->set_response("Fiale", \Restserver\Libraries\REST_Controller::HTTP_OK); 
                    $this->set_response("Unauthorised(1)", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
                    return;
                } 
                
                
            }



        }

        $this->set_response("Unauthorised(2)", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
    }


    public function update_post(){
        header('Content-Type: application/json');
        //驗證 Token
        //JWT Token
        $headers = $this->input->request_headers();

        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {

            $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);

            //驗證Token 成功
            if ($decodedToken != false) {
                $this->set_response($decodedToken, \Restserver\Libraries\REST_Controller::HTTP_OK);

                $u_id = $decodedToken->u_id;

                

                //得到資料參數
                $input_array = array(
                    "id" => $this->input->GET("id"),
                    "tdar_name" => $this->input->post("tdar_name"),
                );


                //搜尋是否存在於資料庫
                $where_array = array(
                    'd_id'=>$u_id,
                    'tdar_id'=>$input_array['id'],
                    'is_civil'=>'Y',
                    'tdar_del'=>'N',
                    'tdar_characteristic_state !='=>'Y',
                    'tdar_validation_result !='=>'N',
                    'tdar_validation_result !='=>'n/a'
                );
                $search_db = $this->Common_model->get_one("department_ar",$where_array);

                $result = array();
                if (count($search_db)>0) {
                    //上傳編輯與刪除舊資料
                    

                    if ($input_array["tdar_name"]!="" && $input_array["tdar_name"]!=NULL) {
                        //更新名稱
                        $update_array = array(
                            'tdar_name' => $input_array["tdar_name"],
                            'tdar_characteristic_state' => 'N',
                            'tdar_to_og' => 'N',
                            'tdar_validation'=>'N',
                            'tdar_validation_result '=>'n/a',
                            'tdar_state ' => 'N'
                        );
                        $this->Common_model->update_db("department_ar",$update_array,$where_array);
                    }

                    //更新關鍵字

                    $tag_ary = $this->input->post("tag");

                    if (count($tag_ary)>0) {
                        //先刪除先前的keyword
                        $where_array= array(
                            'ar_id' => $input_array['id']
                        );

                        $this->Common_model->delete_db("tp_keyword_department_ar",$where_array);

                        foreach ($tag_ary as $tag_key => $tag_value) {

                            //檢查Key 是否存在
                            $where_array =  array('kwd_name' =>  $tag_value);
                            $tag_result = $this->Common_model->get_one("keywords", $where_array);

                            //先清空

                            if ($tag_result) {
                                //有的話Insert key 到relation
                                $insert_array = array(
                                    'ar_id' => $input_array['id'],
                                    'kwd_id' =>$tag_result->kwd_id);
                                $this->Common_model->insert_db("tp_keyword_department_ar", $insert_array);
                            }else{
                                //沒有更新

                                $insert_array = array(
                                    'kwd_name' =>$tag_value);

                                $this->Common_model->insert_db("keywords", $insert_array);

                                $kwd_id = $this->Common_model->get_insert_id();

                                $insert_array = array(
                                    'ar_id' => $input_array['id'],
                                    'kwd_id' =>$kwd_id);
                                $this->Common_model->insert_db("tp_keyword_department_ar", $insert_array);
                            }

                            if($tag_key>=5){
                                break;
                            }
                        }



                    }


                    //更新顯示圖
                    //檢查檔案類別與大小
                    //vrphoto
                    if(isset($_FILES['show_img'])){
                        if ($_FILES['show_img']['size']<10000000 && $_FILES['show_img']['type']=="image/jpeg" || $_FILES['show_img']['type']=="image/jpg"|| $_FILES['show_img']['type']=="image/png") {
                            $old_img_path =  $search_db->tdar_findpho_path;



                            //上傳影片代表圖片處理
                            $upload_path = "upload/image/";
                            $file_type = 'jpg|jpeg|png';
                            $max_size = 1024;
                            $resize_width = 264;
                            $resize_height = 156;
                            $fit_type = "FIT_OUTER";
                            $max_width = 1920;
                            $max_height = 1080;
                            //上傳圖片
                            $field_name = "show_img";
                            $upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);

                            if (!isset($upload_result["upload_data"])) {
                                //必須上傳圖片
                                _alert($this->lang->line("圖片上傳失敗或沒有上傳圖片"));
                                exit();
                            }
                            //上傳成功，記錄上傳檔名

                            $update_array = array(
                                'tdar_findpho_path' => $upload_result["upload_data"]["file_name"],
                                'tdar_name' => $input_array["tdar_name"],
                                'tdar_characteristic_state' => 'N',
                                'tdar_to_og' => 'N',
                                'tdar_validation'=>'N',
                                'tdar_validation_result '=>'n/a',
                                'tdar_state ' => 'N'
                            );

                            $where_array = array(
                                'd_id'=>$u_id,
                                'tdar_id'=>$input_array['id'],
                                'is_civil'=>'Y'
                            );
                            //更新
                            $this->Common_model->update_db("department_ar",$update_array,$where_array);

                            //刪除舊的圖片
                            if (isset($update_array['tdar_findpho_path'])) {
                                $this->upload_lib->delete("upload/image/".$old_img_path);
                            }
                        }
                    }

                    

                    


                    if(isset($_FILES['ar_photo'])){
                        //AR 辨識圖片
                    if ($_FILES['ar_photo']['size']<11000000) {
                            $old_ar_image = $search_db->tdar_image_path;

                            $upload_path = "upload_image/";
                            $file_type = 'jpg|jpeg|png';
                            $max_size = 11000; //10M

                            //上傳代表圖片
                            $field_name = "ar_photo";
                            $upload_result = $this->upload_lib->write_from_image($upload_path, $field_name, $file_type, $max_size);

                            if (!isset($upload_result["upload_data"])) {
                                //必須上傳圖片
                                echo ($this->lang->line("圖片上傳失敗或沒有上傳圖片"));
                                exit();
                            }
                            //上傳成功，記錄上傳檔名
                            // $update_array["tdar_image_path"] = $upload_result["upload_data"]["file_name"];
                            // $update_array["tdar_size"] = $_FILES['ar_photo']['size'];

                            $update_array = array(
                                'tdar_image_path' => $upload_result["upload_data"]["file_name"],
                                'tdar_size' => $_FILES['ar_photo']['size'],
                                'tdar_name' => $input_array["tdar_name"],
                                'tdar_characteristic_state' => 'N',
                                'tdar_to_og' => 'N',
                                'tdar_validation'=>'N',
                                'tdar_validation_result '=>'n/a',
                                'tdar_state ' => 'N'
                            );

                            $where_array = array(
                                'd_id'=>$u_id,
                                'tdar_id'=>$input_array['id'],
                                'is_civil'=>'Y'
                            );
                            //更新
                            $this->Common_model->update_db("department_ar",$update_array,$where_array);

                            //刪除舊的圖片
                            if (isset($update_array['tdar_image_path'])) {
                                $where_array = array(
                                    'u_filename' => 'upload_image/'.$old_ar_image
                                );
                                $this->Common_model->delete_db('upload_image', $where_array);
                            }
                        }

                        

                        

                    }
                    //刪除資料庫舊的圖片
                    $where_array = array(
                        'd_id'=>$u_id,
                        'tdar_id'=>$input_array['id'],
                        'is_civil'=>'Y'
                    );
                    
                    //搜尋最新資料
                    $search_db = $this->Common_model->get_one("department_ar",$where_array);

                    //get_tag_array
                    $tags = get_tag_name("keyword_department_ar","ar_id" ,$search_db->tdar_id);


                    $response_array = [
                        'file_name' => $search_db->tdar_name,
                        'topic' => tid_return_topic_name($search_db->t_id),
                        'ar_imgae' => $this->config->item('server_base_url').'upload_image/'.$search_db->tdar_image_path,
                        'show_url' => $this->config->item('server_base_url').'upload/image/'.$search_db->tdar_findpho_path,
                        'tags' => $tags,
                        'tdar_capture_timestamp' => $search_db->tdar_capture_timestamp
                    ];

                    $message = [
                        'status' => TRUE,
                        'message' => 'update_success',
                        'data' => $response_array
                    ];

                    $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_OK); 
                    return;


                }else{
                    # response error
                    $this->set_response("You don't have permission", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
                    return;
                }   
            }
        }
        $this->set_response("Token is Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);


    }



}
