<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Video extends \Restserver\Libraries\REST_Controller {


    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // header('Access-Control-Allow-Origin: http://vrar360.utobonus.com');
        // header('Access-Control-Allow-Origin: http://vr.ml-codesign.com');
        header("Access-Control-Allow-Methods: GET, POST ,OPTIONS");


        // // Configure limits on our controller methods
        // // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        // $this->methods['users_get']['limit'] = 5; // 500 requests per hour per user/key
        // $this->methods['users_post']['limit'] = 5; // 100 requests per hour per user/key
        $this->methods['post_post']['limit'] = 5;
        // $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key


    }


    public function post_get(){
        header('Content-Type: application/json');
        //驗證 Token
        //JWT Token
        $headers = $this->input->request_headers();

        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {

            $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);

            //驗證Token 成功
            if ($decodedToken != false) {
                $this->set_response($decodedToken, \Restserver\Libraries\REST_Controller::HTTP_OK);

                $u_id = $decodedToken->u_id;

                //得到資料參數
                $input_array = array(
                    "id" => $this->input->get("id"),
                );


                //搜尋是否存在於資料庫
                $where_array = array(
                    'd_id'=>$u_id,
                    'dvv_id'=>$input_array['id'],
                    'is_civil'=>'Y'
                );
                $search_db = $this->Common_model->get_one("department_vr_video",$where_array);

                $result = array();
                if (count($search_db)>0) {

                    $result['id'] = $search_db->dvv_id;
                    $result['topic'] = tid_return_topic_name($search_db->t_id);
                    

                    if($search_db->Is_url=="N"){
                        $result['vr_video'] = $this->config->item('server_base_url').$search_db->dvv_backup;
                    }else{
                        $result['vr_url'] = $search_db->dvv_youtube_link;
                    }  
                
                    $result['name'] = $search_db->dvv_name;
                    $result['capture_timestamp'] = $search_db->dvv_capture_timestamp;                    
                    $result['show_image'] = $this->config->item('server_base_url').$search_db->dvv_rep_img_path;
                    $result['lat'] = $search_db->dvv_lat;
                    $result['lng'] = $search_db->dvv_lng;
                    
                    //關鍵字
                    //環景KeyWord 
                    $where_array = array(
                        "dvv_id" => $search_db->dvv_id
                    );
                    //get_key_word
                    $keywords = $this->Common_model->get_db_join("keyword_department_vr_video as dvv", $where_array , "keywords as ks" , "dvv.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");
                    $kwd_names = array();
                    foreach ($keywords as $kwd_key => $kwdvalue) {
                        if ($kwdvalue->kwd_name!="") {
                            $kwd_names[$kwd_key] = $kwdvalue->kwd_name;
                        }
                    }

                    $result['keywords'] =$kwd_names;
                    $result['update_api'] = $this->config->item('server_base_url')."api_client/video/update?id=".$search_db->dvv_id;

                    $this->set_response($result, \Restserver\Libraries\REST_Controller::HTTP_OK); 
                    return;


                }else{
                    # response error
                    $this->set_response("You don't have permission", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
                    return;
                }   
            }
        }
        $this->set_response("Token is Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);


    }
    public function post_post()
    {
        header('Content-Type: application/json');
        //驗證 Token
        //JWT Token
        $headers = $this->input->request_headers();

        // echo json_encode($_FILES);
        // exit;

        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {

            $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);

            //驗證Token 成功
            if ($decodedToken != false) {
                $this->set_response($decodedToken, \Restserver\Libraries\REST_Controller::HTTP_OK);

                $u_id = $decodedToken->u_id;

                //得到資料參數
                $input_array = array(
                    "t_id" => $this->input->post("t_id"),
                    "dvv_name" => $this->input->post("dvv_name"),
                    "dvv_capture_timestamp" => $this->input->post("dvv_capture_timestamp"),
                    "dvv_lat" => $this->input->post("dvv_lat"),
                    "dvv_lng" => $this->input->post("dvv_lng")
                );


                //檢查參數
                foreach ($input_array as $index => $value) {
                    if ($value === null || $value === '') {
                       $message = [
                                    'status' => FALSE,
                                    'message' => 'INVALID VALUE =>'.$index
                                ];
                        $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                        return;
                    }
                }
                $input_array['video_url'] = $this->input->post("video_url");

                //新增至資料庫基本值
                $insert_array = array(
                    't_id' => $input_array['t_id'],
                    "is_civil" => "Y",
                    "d_id" => $u_id,
                    'dvv_name' => $input_array['dvv_name'],
                    'dvv_capture_timestamp' => $input_array['dvv_capture_timestamp'],
                    'dvv_lat' => $input_array['dvv_lat'],
                    'dvv_lng' => $input_array['dvv_lng'],
                    'dvv_state' => 'N',
                );


                //檢查檔案類別與大小
                //vrvideo

                if ($_FILES['show_img']['size']<3000000 && $_FILES['show_img']['type']=="image/jpeg" || $_FILES['show_img']['type']=="image/jpg"|| $_FILES['show_img']['type']=="image/png") {
                    
                }else{
                    $message = [
                                    'status' => FALSE,
                                    'message' => 'show_img File type or size not allowed'
                                ];
                    $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                    return;
                }

                //檢查是否在主題中
                $where_array = array(
                    't_id' => $input_array['t_id'],
                    't_public_video' => 'Y'
                );

                $topic_result = $this->Common_model->get_one("topic", $where_array);


                if (!$topic_result) {
                    $message = [
                                    'status' => FALSE,
                                    'message' => 'Topic not allowed'
                                ];
                    $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                    return;   
                }

                //關鍵字長度檢查
                $tag_ary = $this->input->post("tag");

                if (count($tag_ary)>0 && count($tag_ary)<=5) {
                    foreach ($tag_ary as $tag_key => $tag_value) {
                        //檢查Key 長度
                        $message = [
                            'status' => FALSE,
                            'message' => 'tag標籤名稱太長，最多為10個字元'
                        ];
                        if (!valid_string_lenth($tag_value,10)){
                            $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                            return;
                        }
                    }
                }else{
                    $message = [
                        'status' => FALSE,
                        'message' => 'tag標籤名稱太多，最多為5個'
                    ];
                    $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                    return;

                }

                //處理圖片上傳
                if (isset($_FILES['vrvideo']['name'])) {
                    //處理VR 圖片
                    if ($_FILES['vrvideo']['size']<1000000000 && $_FILES['vrvideo']['type']=="video/mp4") {
                    
                    }else{
                        $message = [
                            'status' => FALSE,
                            'message' => '環景影片類型或大小不符合規範:檔案類型:'.$_FILES['vrvideo']['type'].'檔案大小:'.$_FILES['vrvideo']['size']
                        ];
                        $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                        return;
                    }
                    $output_dir = "upload/vrvideo/";
                    $video_name =  $_FILES['vrvideo']['name'];
                    $video_type = $_FILES['vrvideo']['type'];
                    $video_tmp_name = $_FILES['vrvideo']['tmp_name'];
                    $video_error = $_FILES['vrvideo']['error'];
                    $video_size = $_FILES['vrvideo']['size'];


                    //上傳影片至資料夾
                    if(!is_array($_FILES["vrvideo"]["name"])) //single file
                    {
                        $fileName = uniqid().".mp4";

                        $tmp_name = dirname($_FILES["vrvideo"]["tmp_name"])."/".basename($_FILES["vrvideo"]["tmp_name"]);


                        //上傳實體到資料夾
                    switch (ENVIRONMENT) {
                        case 'development' :
                        case 'test' :
                        // 假如是在Localhost
                        // 假如是在測試機
                            move_uploaded_file($tmp_name,$output_dir.$fileName);
                            break;

                        case 'api' :
                        // 假如是在API Server 
                            move_uploaded_file($tmp_name,$output_dir.$fileName);
                            $sftp = new SFTP('192.168.255.77');      //Prvite IP  For nmnsvs server (伺服器上測試用)
                            if (!$sftp->login('admin', 'admin123$')) {
                                throw new Exception('Login failed');
                            }else{
                                // echo "Login Success"."<br>";
                            }

                            //切換要上傳目錄夾
                            $sftp->chdir("/var/www/vrar360/upload/vrvideo/");


                            //PUT 檔案 P1: 上傳名稱  , P2: Local端檔案位置
                            $sftp->put($fileName, file_get_contents("/var/www/vrar360_api/upload/vrvideo/".$fileName));

                            break;

                        case 'production' :
                        // 判斷假如是在WEB Server
                        // 假如是在API Server 
                            move_uploaded_file($tmp_name,$output_dir.$fileName);
                            $sftp = new SFTP('192.168.255.80');      //Prvite IP  For nmnsvs server (伺服器上測試用)
                            if (!$sftp->login('admin', 'admin123$')) {
                                throw new Exception('Login failed');
                            }else{
                                // echo "Login Success"."<br>";
                            }

                            //切換要上傳目錄夾
                            $sftp->chdir("/var/www/vrar360_api/upload/vrvideo/");

                            //PUT 檔案 P1: 上傳名稱  , P2: Local端檔案位置
                            $sftp->put($fileName, file_get_contents("/var/www/vrar360/upload/vrvideo/".$fileName));

                            break;

                    }

                        





                        $insert_array['dvv_type'] = $_FILES['vrvideo']['type'];
                        
                        //新增資料至資料庫
                        $insert_array["dvv_backup"] = $output_dir.$fileName;
                        $insert_array["dvv_size"] = $_FILES['vrvideo']['size'];
                        // 移除檔案指令
                        //@unlink(FCPATH . $filename);
                    }
                    

                    //上傳影片代表圖片處理
                    $upload_path = "upload/image/";
                    $file_type = 'jpg|jpeg|png';
                    $max_size = 1024;
                    $resize_width = 264;
                    $resize_height = 156;
                    $fit_type = "FIT_OUTER";
                    $max_width = 1920;
                    $max_height = 1080;
                    //上傳圖片
                    $field_name = "show_img";
                    $upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);
                    if (!isset($upload_result["upload_data"])) {
                        //必須上傳圖片
                        _alert($this->lang->line("圖片上傳失敗或沒有上傳圖片"));
                        exit();
                    }
                    //上傳成功，記錄上傳檔名
                    $insert_array['dvv_rep_img_path'] = $upload_path.$upload_result["upload_data"]["file_name"];
                    

                    //執行上傳資料庫
                    $this->Common_model->insert_db("department_vr_video", $insert_array);

                    $dvv_id = $this->Common_model->get_insert_id();
                    //處理關鍵字
                    $tag_ary = $this->input->post("tag");

                    if (count($tag_ary)>0) {
                        foreach ($tag_ary as $tag_key => $tag_value) {

                            //檢查Key 是否存在
                            $where_array =  array('kwd_name' =>  $tag_value);
                            $tag_result = $this->Common_model->get_one("keywords", $where_array);

                            if ($tag_result) {
                                //有的話Insert key 到relation
                                $insert_array = array(
                                    'dvv_id' => $dvv_id,
                                    'kwd_id' =>$tag_result->kwd_id);
                                $this->Common_model->insert_db("tp_keyword_department_vr_video", $insert_array);
                            }else{
                                //沒有更新

                                $insert_array = array(
                                    'kwd_name' =>$tag_value);

                                $this->Common_model->insert_db("keywords", $insert_array);

                                $kwd_id = $this->Common_model->get_insert_id();

                                $insert_array = array(
                                    'dvv_id' => $dvv_id,
                                    'kwd_id' =>$kwd_id);
                                $this->Common_model->insert_db("tp_keyword_department_vr_video", $insert_array);
                            }
                        }
                    }

                    $response_array = [
                        'file_name' => $input_array['dvv_name'],
                        'topic' => $topic_result->t_name,
                        'file_type'=> "環景",
                        'file_ext' => $_FILES["vrvideo"]["type"],
                        'file_size(byte)' => $_FILES["vrvideo"]["size"],
                    ];

                    $message = [
                        'status' => TRUE,
                        'message' => '上傳成功',
                        'data' => $response_array
                    ];
                    // CREATED (201) being the HTTP response code
                    $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_CREATED); 
                    return;
                }elseif(isset($input_array['video_url'])){
                    //處理URL
                    $insert_array['Is_url'] = "Y";

                    $insert_array['dvv_youtube_link'] = $this->input->post('video_url');
                    $insert_array['dvv_youtube_link'] = $this->security->xss_clean($insert_array['dvv_youtube_link']);

                    //上傳影片代表圖片處理
                    $upload_path = "upload/image/";
                    $file_type = 'jpg|jpeg|png';
                    $max_size = 1024;
                    $resize_width = 264;
                    $resize_height = 156;
                    $fit_type = "FIT_OUTER";
                    $max_width = 1920;
                    $max_height = 1080;
                    //上傳圖片
                    $field_name = "show_img";
                    $upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);
                    if (!isset($upload_result["upload_data"])) {
                        //必須上傳圖片
                        _alert($this->lang->line("圖片上傳失敗或沒有上傳圖片"));
                        exit();
                    }
                    //上傳成功，記錄上傳檔名
                    $insert_array['dvv_rep_img_path'] = $upload_path.$upload_result["upload_data"]["file_name"];
                    

                    //執行上傳資料庫
                    $this->Common_model->insert_db("department_vr_video", $insert_array);

                    $dvv_id = $this->Common_model->get_insert_id();
                    //處理關鍵字
                    $tag_ary = $this->input->post("tag");

                    if (count($tag_ary)>0) {
                        foreach ($tag_ary as $tag_key => $tag_value) {

                            //檢查Key 是否存在
                            $where_array =  array('kwd_name' =>  $tag_value);
                            $tag_result = $this->Common_model->get_one("keywords", $where_array);

                            if ($tag_result) {
                                //有的話Insert key 到relation
                                $insert_array = array(
                                    'dvv_id' => $dvv_id,
                                    'kwd_id' =>$tag_result->kwd_id);
                                $this->Common_model->insert_db("tp_keyword_department_vr_video", $insert_array);
                            }else{
                                //沒有更新

                                $insert_array = array(
                                    'kwd_name' =>$tag_value);

                                $this->Common_model->insert_db("keywords", $insert_array);

                                $kwd_id = $this->Common_model->get_insert_id();

                                $insert_array = array(
                                    'dvv_id' => $dvv_id,
                                    'kwd_id' =>$kwd_id);
                                $this->Common_model->insert_db("tp_keyword_department_vr_video", $insert_array);
                            }
                        }
                    }
                    
                    $where_array =  array('t_id' =>  $input_array['t_id']);
                    $topic_result = $this->Common_model->get_one("topic", $where_array);

                    $response_array = [
                        'file_name' => $input_array['dvv_name'],
                        'topic' => $topic_result->t_name,
                        'file_type'=> "URL",
                        'file_link' => $input_array['video_url']
                    ];

                    $message = [
                        'status' => TRUE,
                        'message' => 'upload_success',
                        'data' => $response_array
                    ];
                    // CREATED (201) being the HTTP response code
                    $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_CREATED); 
                    return;
                }else{
                    //檢查Key 長度
                    $message = [
                        'status' => FALSE,
                        'message' => 'don`t hava any intput file'
                    ];

                    $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                    return;


                }


               
                // CREATED (201) being the HTTP response code
                $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_CREATED); 
                return;
            }



        }

        $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
        
    }

    public function del_post(){
        header('Content-Type: application/json');
        //驗證 Token
        //JWT Token
        $headers = $this->input->request_headers();

        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {

            $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);

            //驗證Token 成功
            if ($decodedToken != false) {
                $this->set_response($decodedToken, \Restserver\Libraries\REST_Controller::HTTP_OK);

                $u_id = $decodedToken->u_id;

                //得到資料參數
                $input_array = array(
                    "id" => $this->input->get("id"),
                );


                //搜尋是否存在於資料庫
                $where_array = array(
                    'd_id'=>$u_id,
                    'dvv_id'=>$input_array['id'],
                    'is_civil'=>'Y'
                );
                $search_db = $this->Common_model->get_one("department_vr_video",$where_array);

                if (count($search_db)>0) {
                    $update_array= array(
                        'dvv_del' => 'Y'
                    );
                    
                    $this->Common_model->update_db("department_vr_video",$update_array,$where_array);
                    
                    $this->set_response("Success", \Restserver\Libraries\REST_Controller::HTTP_OK); 
                    return;
                }else{
                    # response error
                    // $this->set_response("Fiale", \Restserver\Libraries\REST_Controller::HTTP_OK); 
                    $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
                    return;
                }   
            }
        }
        $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
    }


    public function update_post(){
        header('Content-Type: application/json');
        //驗證 Token
        //JWT Token
        $headers = $this->input->request_headers();

        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {

            $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);

            //驗證Token 成功
            if ($decodedToken != false) {
                $this->set_response($decodedToken, \Restserver\Libraries\REST_Controller::HTTP_OK);

                $u_id = $decodedToken->u_id;

                

                //得到資料參數
                $input_array = array(
                    "id" => $this->input->GET("id"),
                    "dvv_name" => $this->input->post("dvv_name"),
                    "dvv_capture_timestamp" => $this->input->post("dvv_capture_timestamp"),

                );


                //搜尋是否存在於資料庫
                $where_array = array(
                    'd_id'=>$u_id,
                    'dvv_id'=>$input_array['id'],
                    'is_civil'=>'Y',
                    'dvv_del'=>'N',
                    'dvv_validation_result !='=>'N',
                    'dvv_validation_result !='=>'n/a'
                );
                $search_db = $this->Common_model->get_one("department_vr_video",$where_array);

                $result = array();
                if (count($search_db)>0) {
                    //上傳編輯與刪除舊資料
                    

                    if ($input_array["dvv_name"]!="" && $input_array["dvv_name"]!=NULL) {
                        //更新名稱
                        $update_array = array(
                            'dvv_name' => $input_array["dvv_name"],
                            'dvv_validation'=>'N',
                            'dvv_validation_result '=>'n/a',
                            'dvv_state ' => 'N'
                        );
                        $this->Common_model->update_db("department_vr_video",$update_array,$where_array);
                    }

                    if ($input_array["dvv_capture_timestamp"]!="" && $input_array["dvv_capture_timestamp"]!=NULL) {
                        //更新拍攝日期
                        $update_array = array(
                            'dvv_capture_timestamp' => $input_array["dvv_capture_timestamp"],
                            'dvv_validation'=>'N',
                            'dvv_validation_result '=>'n/a',
                            'dvv_state ' => 'N'
                        );
                        $this->Common_model->update_db("department_vr_video",$update_array,$where_array);
                    }

                    if ($input_array["photo_type"]!="" && $input_array["photo_type"]!=NULL) {
                        //更新照片類別
                        $update_array = array(
                            'photo_type' => $input_array["photo_type"],
                            'dvv_validation'=>'N',
                            'dvv_validation_result '=>'n/a',
                            'dvv_state ' => 'N'
                        );
                        $this->Common_model->update_db("department_vr_video",$update_array,$where_array);
                    }

                    //更新關鍵字

                    $tag_ary = $this->input->post("tag");

                    if (count($tag_ary)>0) {
                        //先刪除先前的keyword
                        $where_array= array(
                            'dvv_id' => $input_array['id']
                        );

                        $this->Common_model->delete_db("tp_keyword_department_vr_video",$where_array);

                        foreach ($tag_ary as $tag_key => $tag_value) {

                            //檢查Key 是否存在
                            $where_array =  array('kwd_name' =>  $tag_value);
                            $tag_result = $this->Common_model->get_one("keywords", $where_array);

                            //先清空

                            if ($tag_result) {
                                //有的話Insert key 到relation
                                $insert_array = array(
                                    'dvv_id' => $input_array['id'],
                                    'kwd_id' =>$tag_result->kwd_id);
                                $this->Common_model->insert_db("tp_keyword_department_vr_video", $insert_array);
                            }else{
                                //沒有更新

                                $insert_array = array(
                                    'kwd_name' =>$tag_value);

                                $this->Common_model->insert_db("keywords", $insert_array);

                                $kwd_id = $this->Common_model->get_insert_id();

                                $insert_array = array(
                                    'dvv_id' => $input_array['id'],
                                    'kwd_id' =>$kwd_id);
                                $this->Common_model->insert_db("tp_keyword_department_vr_video", $insert_array);
                            }

                            if($tag_key>=5){
                                break;
                            }
                        }



                    }


                    //更新顯示圖
                    //檢查檔案類別與大小
                    //vrphoto
                    if (isset($_FILES['show_img'])) {
                        if ($_FILES['show_img']['size']<10000000 && $_FILES['show_img']['type']=="image/jpeg" || $_FILES['show_img']['type']=="image/jpg"|| $_FILES['show_img']['type']=="image/png") {
                            
                        }else{
                            $message = [
                                            'status' => FALSE,
                                            'message' => 'show_img File type or size not allowed'
                                        ];
                            $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_NOT_ACCEPTABLE);
                            return;
                        }

                        $old_img_path =  $search_db->dvv_rep_img_path;
                        //上傳影片代表圖片處理
                        $upload_path = "upload/image/";
                        $file_type = 'jpg|jpeg|png';
                        $max_size = 1024;
                        $resize_width = 264;
                        $resize_height = 156;
                        $fit_type = "FIT_OUTER";
                        $max_width = 1920;
                        $max_height = 1080;
                        //上傳圖片
                        $field_name = "show_img";
                        $upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);
                        if (!isset($upload_result["upload_data"])) {
                            //必須上傳圖片
                            _alert($this->lang->line("圖片上傳失敗或沒有上傳圖片"));
                            exit();
                        }
                        //上傳成功，記錄上傳檔名
                        $update_array['dvv_rep_img_path'] = $upload_path.$upload_result["upload_data"]["file_name"];
                    }

                    $update_array['photo_type'] = $input_array["photo_type"];
                    $update_array['dvv_validation'] = 'N';
                    $update_array['dvv_validation_result'] = 'n/a';
                    $update_array['dvv_state'] = 'N';


                    // $update_array = array(
                    //     'dvv_rep_img_path' => $upload_path.$upload_result["upload_data"]["file_name"],
                    //     'photo_type' => $input_array["photo_type"],
                    //     'dvv_validation'=>'N',
                    //     'dvv_validation_result '=>'n/a',
                    //     'dvv_state ' => 'N'
                    // );


                    $where_array = array(
                        'd_id'=>$u_id,
                        'dvv_id'=>$input_array['id'],
                        'is_civil'=>'Y'
                    );
                    //更新
                    $this->Common_model->update_db("department_vr_video",$update_array,$where_array);

                    //刪除資料庫舊的圖片
                    //刪除舊的圖片
                    if (isset($update_array['dvv_rep_img_path'])) {
                        $this->upload_lib->delete($old_img_path);
                    }
                    //搜尋最新資料
                    $search_db = $this->Common_model->get_one("department_vr_video",$where_array);

                    //get_tag_array
                    $tags = get_tag_name("keyword_department_vr_video","dvv_id" ,$search_db->dvv_id);


                    $response_array = [
                        'file_name' => $search_db->dvv_name,
                        'topic' => tid_return_topic_name($search_db->t_id),
                        'show_url' => $this->config->item('server_base_url').$search_db->dvv_rep_img_path,
                        'tags' => $tags,
                        'dvv_capture_timestamp' => $search_db->dvv_capture_timestamp
                        // 'file_type'=> "URL",
                        // 'file_link' => $input_array['photo_url']
                    ];

                    $message = [
                        'status' => TRUE,
                        'message' => '上傳成功',
                        'data' => $response_array
                    ];

                    $this->set_response($message, \Restserver\Libraries\REST_Controller::HTTP_OK); 
                    return;


                }else{
                    # response error
                    $this->set_response("You don't have permission", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
                    return;
                }   
            }
        }
        $this->set_response("Token is Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);


    }



}
