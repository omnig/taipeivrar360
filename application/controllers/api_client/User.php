<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class User extends \Restserver\Libraries\REST_Controller {


    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        header("Access-Control-Allow-Methods: GET,OPTIONS");
        
    }

    //隱藏函數
    public function my_photo_post(){}
    public function my_video_post(){}
    public function my_ar_post(){}
    public function my_medal_post(){}
    public function my_upload_post(){}
    public function my_name_get(){}

    //我的圖片
    public function my_photo_get()
    {   

        //JWT Token
        $headers = $this->input->request_headers();
        
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
             $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);
        //有登入
            //驗證Token 成功
            if ($decodedToken != false) {

                // $order ="";
                // $order = $this->input->get('order');
                // echo json_encode($order);
                // exit;

                $u_id = $decodedToken->u_id;

                $vr_photo_list = array();
                $vr_photo_result = array();

                $where_array = array(
                    'user_id' => $u_id,
                    'dvp_state' => 'Y',
                    'dvp_del' => "N"
                );

                // $vr_photo_list = $this->Common_model->get_db("department_vr_photo", $where_array,"dvp_validation_timestamp","DESC",6);

                $vr_photo_list = $this->Common_model->get_db_join("user_photo as up", $where_array , "department_vr_photo as dvp" , "up.dvp_id = dvp.dvp_id " , "INNER" ,"collection_timestamp" , "ASC");

                // echo json_encode($u_id);
                //資料整理
                foreach ($vr_photo_list as $key => $value) {
                    $vr_photo_result[$key]['id'] =  $value->dvp_id;
                    $vr_photo_result[$key]['name'] =  $value->dvp_name;
                    $vr_photo_result[$key]['photo'] =  $this->config->item('server_base_url').$value->dvp_rep_img_path;

                    //判斷照片為環景或全景 URL異動
                    if ($value->photo_type=="pano") {
                        //環景圖
                        $vr_photo_result[$key]['url'] =  $value->dvp_view_link;
                    }else{
                        //全景圖
                        $vr_photo_result[$key]['url'] =  $this->config->item('server_base_url').'/kr_pano?image_path='.$this->config->item('server_base_url').$value->dvp_backup;
                    }

                    
                    $vr_photo_result[$key]['capture_time'] =  $value->dvp_capture_timestamp;
                    


                    $where_array = array(
                       'dvp_id' =>$value->dvp_id,
                       'user_id'=>$u_id
                    );

                    $vr_photo_user_like = $this->Common_model->get_db("tp_user_photo", $where_array,"","as",0);
                    if($vr_photo_user_like==TRUE){
                        //User 有喜歡
                        $vr_photo_result[$key]['like'] = TRUE;
                    }else{
                        //User 沒有喜歡
                        $vr_photo_result[$key]['like'] = FALSE;
                    }


                    //判斷是為局處或是民眾  使用者名稱異動
                    if ($value->is_civil=="Y") {
                        //民眾
                        $where_array = array(
                            'u_id' => $value->d_id
                        );

                        $the_user = $this->Common_model->get_one("user",$where_array);

                        if($the_user){
                            $vr_photo_result[$key]['uploader'] =  $the_user->u_name;
                        }else{
                            $vr_photo_result[$key]['uploader'] =  "Other";
                        }

                        
                    }else{
                        //局處

                        $where_array = array(
                            'ag_id' => $value->d_id
                        );

                        $the_department= $this->Common_model->get_one("tp_admin_group",$where_array);

                        if($the_department){
                            $vr_photo_result[$key]['uploader'] =  $the_department->ag_name;
                        }else{
                            $vr_photo_result[$key]['uploader'] =  "Other";
                        }

                    }

                    //環景KeyWord 
                    $where_array = array(
                        "dvp_id" => $value->dvp_id
                    );

                    //get_key_word
                    $keywords = $this->Common_model->get_db_join("keyword_department_vr_photo as dvp", $where_array , "keywords as ks" , "dvp.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");
                    $kwd_names = array();
                    foreach ($keywords as $kwd_key => $kwdvalue) {
                        if ($kwdvalue->kwd_name!="") {
                            $kwd_names[$kwd_key] = $kwdvalue->kwd_name;
                        }
                    }
                    $vr_photo_result[$key]['keywords'] =$kwd_names;

                }

                // echo json_encode($vr_photo_result);
                // exit;
                $result = $vr_photo_result;
                
                //顯示呼叫結果
                $ret = array(
                    "result" => "true",
                    "code" => 200,
                    "error_message" => "",
                    "data" => $result,
                    "TotelRecodes" => count($vr_photo_result)
                );

                //沒有結果
                if(count($result)<=0){
                    http_response_code(200);
                    $ret = array(
                        "result" => "true",
                        "code" => 200,
                        "error_message" => "",
                        "data" => "None Data"
                    );
                }


                // CREATED (200) being the HTTP response code
                $this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK); 
                return;
            }else{

                $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }
        }

        $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
        return;

        
    }
    

    //我的影片
    public function my_video_get()
    {
        //JWT Token
        $headers = $this->input->request_headers();
        
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);
             //有登入
            //驗證Token 成功
            if ($decodedToken != false) {

                $u_id = $decodedToken->u_id;

                $vr_video_list = array();
                $vr_video_result = array();

                $where_array = array(
                    'user_id' => $u_id,
                    'dvv_state' => 'Y',
                    'dvv_del' => "N"
                );

                $vr_video_list = $this->Common_model->get_db_join("user_video as uv", $where_array , "department_vr_video as dvv" , "uv.dvv_id = dvv.dvv_id " , "INNER" ,"collection_timestamp" , "ASC");

                // echo json_encode($u_id);
                //資料整理
                foreach ($vr_video_list as $key => $value) {
                    $vr_video_result[$key]['id'] =  $value->dvv_id;
                    $vr_video_result[$key]['name'] =  $value->dvv_name;
                    $vr_video_result[$key]['photo'] =  $this->config->item('server_base_url').$value->dvv_rep_img_path;
                    $web_link = str_replace("https://www.youtube.com/watch?v=", "", $value->dvv_youtube_link);
                    $vr_video_result[$key]['dvv_web_video'] =  "https://www.youtube.com/watch?v=".$web_link;
                    $mobile_link = str_replace("https://www.youtube.com/watch?v=", "", $value->dvv_youtube_link);
                    $vr_video_result[$key]['dvv_mobile_video'] =  "vnd.youtube:".$mobile_link;

                    $vr_video_result[$key]['capture_time'] =  date('d M Y H:i',strtotime($value->dvv_capture_timestamp))." UTC";

                    $vr_video_result[$key]['like'] = NULL;

                    $where_array = array(
                       'dvv_id' =>$value->dvv_id,
                       'user_id'=>$u_id
                    );

                    $vr_video_user_like = $this->Common_model->get_db("tp_user_video", $where_array,"","DESC",0);
                    if($vr_video_user_like==TRUE){
                        //User 有喜歡
                        $vr_video_result[$key]['like'] = TRUE;
                    }else{
                        //User 沒有喜歡
                        $vr_video_result[$key]['like'] = FALSE;
                    }



                    //判斷是為局處或是民眾  使用者名稱異動
                    if ($value->is_civil=="Y") {
                        //民眾
                        $where_array = array(
                            'u_id' => $value->d_id
                        );

                        $the_user = $this->Common_model->get_one("user",$where_array);

                        $vr_video_result[$key]['uploader'] =  $the_user->u_name;
                    }else{
                        //局處

                        $where_array = array(
                            'ag_id' => $value->d_id
                        );

                        $the_department= $this->Common_model->get_one("tp_admin_group",$where_array);

                        if($the_department){
                            $vr_video_result[$key]['uploader'] =  $the_department->ag_name;
                        }else{
                            $vr_video_result[$key]['uploader'] =  "Other";
                        }
                }

                //環景KeyWord 
                $where_array = array(
                    "dvv_id" => $value->dvv_id
                );

                //get_key_word
                $keywords = $this->Common_model->get_db_join("keyword_department_vr_video as dvv", $where_array , "keywords as ks" , "dvv.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");
                $kwd_names = array();
                foreach ($keywords as $kwd_key => $kwdvalue) {
                    if ($kwdvalue->kwd_name!="") {
                        $kwd_names[$kwd_key] = $kwdvalue->kwd_name;
                    }
                }

                $vr_video_result[$key]['keywords'] =$kwd_names;
            }

            // echo json_encode($vr_photo_result);
            // exit;
            $result = $vr_video_result;
            
            //顯示呼叫結果
            $ret = array(
                "result" => "true",
                "code" => 200,
                "error_message" => "",
                "data" => $result,
                "TotelRecodes" => count($vr_video_result)
            );

            //沒有結果
            if(count($result)<=0){
                http_response_code(200);
                $ret = array(
                    "result" => "true",
                    "code" => 200,
                    "error_message" => "",
                    "data" => "None Data"
                );
            }


            // CREATED (200) being the HTTP response code
            $this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK); 
            return;
        }else{

            $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        }

        $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
        return;
    }
    

     //我的影片
    public function my_document_get()
    {
        //JWT Token
        $headers = $this->input->request_headers();
        $document = array();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);
             //有登入
            //驗證Token 成功
            if ($decodedToken != false) {

                $u_id = $decodedToken->u_id;

                $document_list = array();
                $vr_video_result = array();

                $this->db->select('d.*, COUNT(c.c_id) as num_comments,u.u_name');
                $this->db->from('user_document as ud');
                $this->db->join('tp_document as d','d.d_id=ud.d_id','inner');
                $this->db->join('tp_user as u','u.u_id=d.d_uid','left');
                $this->db->where("d.d_vaild_state", "online");
                $this->db->where("d.d_del", "N");
                $this->db->order_by('d.d_update_time', 'desc');
                $this->db->join('tp_comment as c', 'c.d_id = d.d_id','LEFT');
                $this->db->group_by('d.d_id');

                $query = $this->db->get();
                $document = $query->result();

                //資料整理
                if ($document) {
                    foreach ($document as $key => $value) {
                        $document[$key]->d_image = $this->config->item("server_base_url").$document[$key]->d_image;
                        //返回指定字數
                        $document[$key]->d_content = mb_substr($document[$key]->d_content,0,50);

                        if ($value->u_name==NULL) {
                            $document[$key]->u_name = "Offical";
                        }

                        unset($document[$key]->d_comment_num);
                    }
                }else{
                    $document = array();
                }                

            }

            // echo json_encode($vr_photo_result);
            // exit;
            $result = $document;
            
            //顯示呼叫結果
            $ret = array(
                "result" => "true",
                "code" => 200,
                "error_message" => "",
                "data" => $result,
                "TotelRecodes" => count($result)
            );

            //沒有結果
            if(count($result)<=0){
                http_response_code(200);
                $ret = array(
                    "result" => "true",
                    "code" => 200,
                    "error_message" => "",
                    "data" => "None Data"
                );
            }


            // CREATED (200) being the HTTP response code
            $this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK); 
            return;
        }else{

            $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }

        $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
        return;
    }



    //我的AR互動紀錄
    public function my_ar_get()
    {
        //JWT Token
        $headers = $this->input->request_headers();

        $vr_ar_list = array();
        $vr_ar_result = array();
        
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);
             //有登入
            //驗證Token 成功
            if ($decodedToken != false) {

                $u_id = $decodedToken->u_id;

                

                $where_array = array(
                    'user_id' => $u_id,
                    'tdar_state' => 'Y',
                    'tdar_del' => "N"
                );

                $vr_ar_list = $this->Common_model->get_db_join("user_ar as ua", $where_array , "department_ar as da" , "ua.tdar_id = da.tdar_id " , "INNER" ,"collection_timestamp" , "ASC");

                // echo json_encode($u_id);
                //資料整理
                foreach ($vr_ar_list as $key => $value) {
                    $vr_ar_result[$key]['id'] =  $value->tdar_id;
                    $vr_ar_result[$key]['name'] =  $value->tdar_name;
                    $vr_ar_result[$key]['photo'] =  $this->config->item('server_base_url')."upload/image/".$value->tdar_findpho_path;
                    $vr_ar_result[$key]['interactive_time'] = date('d M Y H:i',strtotime($value->collection_timestamp))." UTC";
                }

            }

            $result = $vr_ar_result;
            
            //顯示呼叫結果
            $ret = array(
                "result" => "true",
                "code" => 200,
                "error_message" => "",
                "data" => $result,
                "TotelRecodes" => count($vr_ar_result)
            );

            //沒有結果
            if(count($result)<=0){
                http_response_code(200);
                $ret = array(
                    "result" => "true",
                    "code" => 200,
                    "error_message" => "",
                    "data" => "None Data"
                );
            }


            // CREATED (200) being the HTTP response code
            $this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK); 
            return;
        }else{

            $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        

        $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
        return;
    }
    

    //我的獎章
    public function my_medal_get()
    {
         //JWT Token
        $headers = $this->input->request_headers();

        $counter = array();

        
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);
             //有登入
            //驗證Token 成功

            $counter['VRP'] = 0;
            $counter['VRV'] = 0;
            $counter['AR'] = 0;
            $counter['UVRP'] = 0;
            $counter['UVRV'] = 0;
            $counter['UAR'] = 0;

            if ($decodedToken != false) {

                $u_id = $decodedToken->u_id;


                //VR_Photo_Count
                // 使用者上傳
                $where_array = array(
                    'd_id' => $u_id,
                    'is_civil' => "Y",
                    'dvp_state' => 'Y',
                    'dvp_del' => "N"
                );
                $counter['VRP'] = $this->Common_model->count_db("department_vr_photo",$where_array);
                // 使用者喜愛
                $where_array = array(
                    'user_id' => $u_id
                );
                $counter['UVRP'] = $this->Common_model->count_db("tp_user_photo",$where_array);

                //VR_Video_Count
                $where_array = array(
                    'd_id' => $u_id,
                    'is_civil' => "Y",
                    'dvv_state' => 'Y',
                    'dvv_del' => "N"
                );
                $counter['VRV'] = $this->Common_model->count_db("department_vr_video",$where_array);
                // 使用者喜愛
                $where_array = array(
                    'user_id' => $u_id
                );
                $counter['UVRV'] = $this->Common_model->count_db("tp_user_video",$where_array);

                //AR_Count
                $where_array = array(
                    'd_id' => $u_id,
                    'is_civil' => "Y",
                    'tdar_state' => 'Y',
                    'tdar_del' => "N"
                );
                $counter['AR'] = $this->Common_model->count_db("department_ar",$where_array);
                // 使用者喜愛
                $where_array = array(
                    'user_id' => $u_id
                );
                $counter['UAR'] = $this->Common_model->count_db("tp_user_ar",$where_array);


                

            }

            $result = $counter;

            
            //顯示呼叫結果
            $ret = array(
                "result" => "true",
                "code" => 200,
                "error_message" => "",
                "data" => $result,
                "TotelScore" => (($counter['VRP']+$counter['VRV']+$counter['AR'])*3)+($counter['UVRP']+$counter['UVRV']+$counter['UAR'])
            );

            //沒有結果
            if(count($result)<=0){
                http_response_code(200);
                $ret = array(
                    "result" => "true",
                    "code" => 200,
                    "error_message" => "",
                    "data" => "None Data"
                );
            }


            // CREATED (200) being the HTTP response code
            $this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK); 
            return;
        }else{

            $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        

        $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
        return;
    }
    

    //我的分享
    public function my_upload_get()
    {

        //JWT Token
        $headers = $this->input->request_headers();

        $vr_ar_list = array();
        $vr_ar_result = array();


        //Model 列表  
        $vr_photo_list = array();
        $vr_video_list = array();
        $ar_list = array();
        $document_list = array();

        //整理資料後的回傳結果
        $vr_photo_result = array();
        $vr_video_result = array();
        $ar_result = array();
        $document_result = array();

        //最後整理輸出值
        $after_result = array();
        
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);
             //有登入
            //驗證Token 成功
            if ($decodedToken != false) {

                $u_id = $decodedToken->u_id;



                //VR 360照片
                $where_array = array(
                    'd_id' =>  $u_id ,
                    'is_civil' =>  "Y" ,
                    'dvp_del' => "N"
                );
                $vr_photo_list = $this->Common_model->get_db('department_vr_photo',$where_array);

                

                //資料整理
                foreach ($vr_photo_list as $key => $value) {
                    // $vr_photo_result[$key]['id'] =  $value->dvp_id;

                    $vr_photo_result[$key]['name'] =  $value->dvp_name;

                    $vr_photo_result[$key]['size'] =  is_null($value->dvp_size)?"URL":$value->dvp_size;
                    $vr_photo_result[$key]['photo'] =  $this->config->item('server_base_url').$value->dvp_rep_img_path;
                    $vr_photo_result[$key]['a_type'] =  "VR 360圖片";

                    if ($value->Is_url=="Y") {
                        $vr_photo_result[$key]['b_type'] =  "URL";
                    }else{
                        $vr_photo_result[$key]['b_type'] =  "JPEG";
                    }

                    $vr_photo_result[$key]['upload_time'] = date('d M Y H:i',strtotime($value->dvp_create_timestamp))." UTC";

                    // 狀態判斷
                    if ($value->dvp_state=="Y") {
                        $vr_photo_result[$key]['state'] =  "已發佈";
                        $vr_photo_result[$key]['can_edit'] =  TRUE;
                        $vr_photo_result[$key]['edit_api'] =  $this->config->item('server_base_url')."api_client/photo/post?id=".$value->dvp_id;

                    }elseif($value->dvp_state=="N" && $value->dvp_validation_result =="n/a"){
                        $vr_photo_result[$key]['state'] =  "審核中";
                        $vr_photo_result[$key]['can_edit'] =  FALSE;
                    }elseif($value->dvp_state=="N" && $value->dvp_validation_result =="U"){
                        $vr_photo_result[$key]['state'] =  "請修改";
                        $vr_photo_result[$key]['validation_description'] = trim($value->dvp_validation_description)==""?"未填寫":ltrim($value->dvp_validation_description);
                        $vr_photo_result[$key]['can_edit'] =  TRUE;
                        $vr_photo_result[$key]['edit_api'] =  $this->config->item('server_base_url')."api_client/photo/post?id=".$value->dvp_id;

                    }elseif($value->dvp_state=="N" && $value->dvp_validation_result =="N"){
                        $vr_photo_result[$key]['state'] =  "未通過";
                        $vr_photo_result[$key]['can_edit'] =  FALSE;
                    }else{
                        $vr_photo_result[$key]['state'] =  "待上架";
                        $vr_photo_result[$key]['can_edit'] =  FALSE;
                    }


                    //關鍵字
                    //環景KeyWord 
                    $where_array = array(
                        "dvp_id" => $value->dvp_id
                    );

                    //get_key_word
                    $keywords = $this->Common_model->get_db_join("keyword_department_vr_photo as dvp", $where_array , "keywords as ks" , "dvp.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");
                    $kwd_names = array();
                    foreach ($keywords as $kwd_key => $kwdvalue) {
                        if ($kwdvalue->kwd_name!="") {
                            $kwd_names[$kwd_key] = $kwdvalue->kwd_name;
                        }
                    }

                    $vr_photo_result[$key]['keywords'] =$kwd_names;
                    $vr_photo_result[$key]['del_api'] =$this->config->item('server_base_url')."api_client/photo/del?id=".$value->dvp_id;

                    array_push($after_result, $vr_photo_result[$key]);
                }


                //VR 360影片
                $where_array = array(
                    'd_id' =>  $u_id ,
                    'is_civil' =>  "Y" ,
                    'dvv_del' => "N"
                );

                $vr_video_list = $this->Common_model->get_db('department_vr_video',$where_array);


                //資料整理
                foreach ($vr_video_list as $key => $value) {
                    // $vr_video_result[$key]['id'] =  $value->dvv_id;
                    $vr_video_result[$key]['name'] =  $value->dvv_name;
                    $vr_video_result[$key]['size'] =  is_null($value->dvv_size)?"URL":$value->dvv_size;
                    $vr_video_result[$key]['photo'] =  $this->config->item('server_base_url').$value->dvv_rep_img_path;
                    $vr_video_result[$key]['a_type'] =  "VR 360影片";

                    if ($value->Is_url=="Y") {
                        $vr_video_result[$key]['b_type'] =  "URL";
                    }else{
                        $vr_video_result[$key]['b_type'] =  "MP4";
                    }
                    
                    $vr_video_result[$key]['upload_time'] = date('d M Y H:i',strtotime($value->dvv_create_timestamp))." UTC";

                    // 狀態判斷
                    if ($value->dvv_state=="Y") {
                        $vr_video_result[$key]['state'] =  "已發佈";
                        $vr_video_result[$key]['can_edit'] =  TRUE;
                        $vr_video_result[$key]['edit_api'] =  $this->config->item('server_base_url')."api_client/video/post?id=".$value->dvv_id;
                    }elseif($value->dvv_state=="N" && $value->dvv_validation_result =="n/a"){
                        $vr_video_result[$key]['state'] =  "審核中";
                        $vr_video_result[$key]['can_edit'] =  FALSE;
                    }elseif($value->dvv_state=="N" && $value->dvv_validation_result =="U"){
                        $vr_video_result[$key]['state'] =  "請修改";
                        $vr_video_result[$key]['validation_description'] = trim($value->dvv_validation_description)==""?"未填寫":ltrim($value->dvv_validation_description);
                        $vr_video_result[$key]['can_edit'] =  TRUE;
                        $vr_video_result[$key]['edit_api'] =  $this->config->item('server_base_url')."api_client/video/post?id=".$value->dvv_id;
                    }elseif($value->dvv_state=="N" && $value->dvv_validation_result =="N"){
                        $vr_video_result[$key]['state'] =  "未通過";
                        $vr_video_result[$key]['can_edit'] =  FALSE;
                    }else{
                        $vr_video_result[$key]['state'] =  "待上架";
                        $vr_video_result[$key]['can_edit'] =  FALSE;
                    }


                    //關鍵字
                    //環景KeyWord 
                    $where_array = array(
                        "dvv_id" => $value->dvv_id
                    );

                    //get_key_word
                    $keywords = $this->Common_model->get_db_join("keyword_department_vr_video as dvv", $where_array , "keywords as ks" , "dvv.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");
                    $kwd_names = array();
                    foreach ($keywords as $kwd_key => $kwdvalue) {
                        if ($kwdvalue->kwd_name!="") {
                            $kwd_names[$kwd_key] = $kwdvalue->kwd_name;
                        }
                    }

                    $vr_video_result[$key]['keywords'] =$kwd_names;
                    $vr_video_result[$key]['del_api'] =$this->config->item('server_base_url')."api_client/video/del?id=".$value->dvv_id;
                    array_push($after_result, $vr_video_result[$key]);
                }

                //使用者 AR
                $where_array = array(
                    'd_id' =>  $u_id ,
                    'is_civil' =>  "Y" ,
                    'tdar_del' => "N"
                );

                $ar_list = $this->Common_model->get_db('department_ar',$where_array);


                //資料整理
                foreach ($ar_list as $key => $value) {
                    // $vr_video_result[$key]['id'] =  $value->dvv_id;
                    $ar_result[$key]['name'] =  $value->tdar_name;
                    $ar_result[$key]['size'] =  is_null($value->tdar_size)?"重新上傳":$value->tdar_size;
                    $ar_result[$key]['photo'] =  $this->config->item('server_base_url')."upload/image/".$value->tdar_findpho_path;
                    $ar_result[$key]['a_type'] =  "AR";
                    $ar_result[$key]['upload_time'] = date('d M Y H:i',strtotime($value->tdar_create_timestamp))." UTC";

                    // 狀態判斷
                    if ($value->tdar_state=="Y") {
                        $ar_result[$key]['state'] =  "已發佈";
                        $ar_result[$key]['can_edit'] =  TRUE;
                        $ar_result[$key]['edit_api'] =  $this->config->item('server_base_url')."api_client/ar/post?id=".$value->tdar_id;
                    }elseif($value->tdar_state=="N" && $value->tdar_validation_result =="n/a"){
                        $ar_result[$key]['state'] =  "審核中";
                        $ar_result[$key]['can_edit'] =  FALSE;
                    }elseif($value->tdar_state=="N" && $value->tdar_validation_result =="U"){
                        $ar_result[$key]['state'] =  "請修改";
                        $ar_result[$key]['validation_description'] = trim($value->tdar_validation_description)==""?"未填寫":ltrim($value->tdar_validation_description);
                        $ar_result[$key]['can_edit'] =  TRUE;
                        $ar_result[$key]['edit_api'] =  $this->config->item('server_base_url')."api_client/ar/post?id=".$value->tdar_id;
                    }elseif($value->tdar_state=="N" && $value->tdar_validation_result =="N"){
                        $ar_result[$key]['state'] =  "未通過";
                        $ar_result[$key]['can_edit'] =  FALSE;
                    }else{
                        $ar_result[$key]['state'] =  "待上架";
                        $ar_result[$key]['can_edit'] =  FALSE;
                    }


                    //關鍵字
                    //環景KeyWord 
                    $where_array = array(
                        "ar_id" => $value->tdar_id
                    );

                    //get_key_word
                    $keywords = $this->Common_model->get_db_join("keyword_department_ar as tdar", $where_array , "keywords as ks" , "tdar.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");
                    $kwd_names = array();
                    foreach ($keywords as $kwd_key => $kwdvalue) {
                        if ($kwdvalue->kwd_name!="") {
                            $kwd_names[$kwd_key] = $kwdvalue->kwd_name;
                        }
                    }

                    $ar_result[$key]['keywords'] =$kwd_names;
                    $ar_result[$key]['del_api'] =$this->config->item('server_base_url')."api_client/ar/del?id=".$value->tdar_id;

                    array_push($after_result, $ar_result[$key]);
                }


                $where_array = array(
                    'd_uid' =>  $u_id ,
                    'is_civil' =>  "Y" ,
                    'd_del' => "N"
                );

                $document_list = $this->Common_model->get_db('tp_document',$where_array);


                //資料整理
                foreach ($document_list as $key => $value) {
                    // $vr_video_result[$key]['id'] =  $value->dvv_id;
                    $document_result[$key]['name'] =  $value->d_title;
                    $document_result[$key]['size'] =  "";
                    $document_result[$key]['photo'] =  $this->config->item('server_base_url').$value->d_image;
                    $document_result[$key]['a_type'] =  "Document";
                    $document_result[$key]['upload_time'] = date('d M Y H:i',strtotime($value->d_create_time))." UTC";

                    // 狀態判斷
                    if ($value->d_vaild_state=="online") {
                        $document_result[$key]['state'] =  "已發佈";
                        $document_result[$key]['can_edit'] =  FALSE;
                        $document_result[$key]['edit_api'] =  $this->config->item('server_base_url')."api_client/document/edit/".$value->d_id;
                    }elseif($value->d_vaild_state=="create"){
                        $document_result[$key]['state'] =  "審核中";
                        $document_result[$key]['can_edit'] =  FALSE;
                    }elseif($value->d_vaild_state=="edit"){
                        $document_result[$key]['state'] =  "請修改";
                        $document_result[$key]['validation_description'] = trim($value->d_valid_message)==""?"未填寫":ltrim($value->d_valid_message);
                        $document_result[$key]['can_edit'] =  TRUE;
                        $document_result[$key]['edit_api'] =  $this->config->item('server_base_url')."api_client/document/edit/".$value->d_id;
                    }elseif($value->d_vaild_state=="offline"){
                        $document_result[$key]['state'] =  "未通過";
                        $document_result[$key]['can_edit'] =  FALSE;
                    }else{
                        $document_result[$key]['state'] =  "待上架";
                        $document_result[$key]['can_edit'] =  FALSE;
                    }


                    $document_result[$key]['keywords'][] = $value->d_type;
                    $document_result[$key]['del_api'] =$this->config->item('server_base_url')."api_client/document/del/".$value->d_id;

                    array_push($after_result, $document_result[$key]);
                }

            }

            

            $order =  $this->GET("order");

            if ($order=="desc") {
                usort($after_result, 'time_sort_desc');
            }else{
                usort($after_result, 'time_sort_asc');
            }

            

            $result = $after_result;
            
            //顯示呼叫結果
            $ret = array(
                "result" => "true",
                "code" => 200,
                "error_message" => "",
                "data" => $result,
                "TotelRecodes" => count($after_result)
            );

            //沒有結果
            if(count($result)<=0){
                http_response_code(200);
                $ret = array(
                    "result" => "true",
                    "code" => 200,
                    "error_message" => "",
                    "data" => "None Data"
                );
            }


            // CREATED (200) being the HTTP response code
            $this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK); 
            return;
        }else{

            $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        

        $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
        return;

        

    }

    //我的名字更改

    public function my_name_post()
    {
        //JWT Token
        $headers = $this->input->request_headers();
        $new_name = $this->input->POST('new_name');
        
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);
             //有登入
            //驗證Token 成功
            if ($decodedToken != false) {

                $u_id = $decodedToken->u_id;

                $where_array = array(
                    'u_id' => $u_id
                );

                $user = $this->Common_model->get_one("user",$where_array);


                if (!$user||$new_name==NULL||!isset($new_name)) {
                    $ret = array(
                        "result" => "true",
                        "code" => 200,
                        "error_message" => "",
                        "data" => "INVALID_PARAMETER"
                    );

                    // 回傳結果
                    $this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK); 
                    return;

                }

                $update_array= array(
                    'u_name' => $new_name
                );
                    
                $this->Common_model->update_db("user",$update_array,$where_array);
                $user = $this->Common_model->get_one("user",$where_array);
                unset($user->u_account);
                unset($user->u_fb_id);
                unset($user->u_fb_token);
                unset($user->u_fb_ts);
                unset($user->u_device_id);
                unset($user->u_google_id);
                unset($user->u_firebase_id);
                unset($user->u_google_picture);
                unset($user->u_google_token);
                unset($user->u_google_ts);
                unset($user->u_ty_id);
                unset($user->u_ty_token);
                unset($user->u_ty_ts);
                unset($user->u_login_expire_ts);
                unset($user->u_ip);
                unset($user->u_user_agent);
                unset($user->u_login_token);
                unset($user->u_email_confirmed);
                unset($user->u_create_timestamp);
                unset($user->u_last_login_timestamp);
                unset($user->u_last_logout_timestamp);
                unset($user->u_enabled);
                unset($user->u_del);
            }

            $result = $user;
            
            //顯示呼叫結果
            $ret = array(
                "result" => "true",
                "code" => 200,
                "error_message" => "",
                "data" => $result
            );

            //沒有結果
            if(count($result)<=0){
                http_response_code(200);
                $ret = array(
                    "result" => "true",
                    "code" => 200,
                    "error_message" => "",
                    "data" => "None Data"
                );
            }


            // 回傳結果
            $this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK); 
            return;


        }else{

            $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        

        $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
        return;
    }

    
    //使用者訂閱權限Check
    
    public function check_subscribe_post(){
        //JWT Token
        $headers = $this->input->request_headers();
        //檢驗權限
        if (!array_key_exists('Authorization', $headers) || empty($headers['Authorization'])) {
            $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        //檢驗解碼
        if (!$decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization'])) {
            $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }

        //使用者ID
        $u_id = $decodedToken->u_id;
        $where_array = array(
            "u_id" => $u_id
        );
        $user = $this->Common_model->get_one("tp_user",$where_array);


        //查看狀態
        $user_subscribe = ($user->u_subscribe_confirmed=="N")?"N":"Y";
        $user_mail = $user->u_subscribe_mail;

        $ret = array(
            "result" => true,
            "user_subscribe" => $user_subscribe,
            "user_mail" => $user_mail
        );

        $this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK); 
        return;
    }


    //用者訂閱權限修改

    public function update_subscribe_post(){

        $subscribe = $this->input->post("subscribe");
        $subscribe_mail = $this->input->post("subscribe_mail");

        if ($subscribe && trim($subscribe_mail)=="") {
            $this->set_response("subscribe_mail is null. Please keyin to this field.", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }

        //JWT Token
        $headers = $this->input->request_headers();
        //檢驗權限
        if (!array_key_exists('Authorization', $headers) || empty($headers['Authorization'])) {
            $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        //檢驗解碼
        if (!$decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization'])) {
            $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }

        //使用者ID
        $u_id = $decodedToken->u_id;
        $where_array = array(
            "u_id" => $u_id
        );
        if ($subscribe=="Y") {
            $update_array = array(
                "u_subscribe_confirmed" => "Y",
                "u_subscribe_mail" => $subscribe_mail
            );
        }else{
            $update_array = array(
                "u_subscribe_confirmed" => "N",
            );
        }
        $this->Common_model->update_db("tp_user",$update_array,$where_array);


        // $user = $this->Common_model->get_one("tp_user",$where_array);


        //查看狀態

        $ret = array(
            "result" => true,
            "user_subscribe" => $subscribe
        );

        $this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK); 
        return;
    }


}


