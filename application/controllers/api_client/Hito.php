<?php
defined('BASEPATH') OR exit('No direct script access allowed');


use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';



class Hito extends \Restserver\Libraries\REST_Controller {


    function __construct()
    {	
    	header('Content-Type: application/json');
        // Construct the parent class
        parent::__construct();
        // header('Access-Control-Allow-Origin: http://vrar360.utobonus.com');
        header("Access-Control-Allow-Methods: GET, POST ,OPTIONS");
        // // Configure limits on our controller methods
        // // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        // $this->methods['category_list_get']['limit'] = 5; // 500 requests per hour per user/key
        $this->db = $this->load->database(ENVIRONMENT, TRUE);
    }

    //1.熱門主題清單
    public function topic_rank_get(){

        $topic = $this->topic_filter_function();
    	$ret = array(
    		"result"=>true,
    		"error_message" =>"",
    		"data" => $topic
    	);
    	$this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK); 
    }

    //2.熱門收藏清單
    public function vr_collection_get(){


        $vr_mixer = array();
        $vr_photo = $this->getting_vr_photo("collection");
        $vr_video = $this->getting_vr_video("collection");

        foreach ($vr_photo as $key => $value) {
            $vr_mixer[]=$value;
        }

        foreach ($vr_video as $key => $value) {
            $vr_mixer[]=$value;
        }

        usort($vr_mixer, function ($item1, $item2) {
            return  $item2['collection_counter'] <=> $item1['collection_counter'];
        });


        $vr_mixer = array_slice($vr_mixer,1,9);


        $ret = array(
            "result"=>true,
            "error_message" =>"",
            "data" => $vr_mixer
        );
        $this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK);  
    }

    //3.熱門分享清單
    public function vr_share_get(){


        $vr_mixer = array();
        $vr_photo = $this->getting_vr_photo("share");
        $vr_video = $this->getting_vr_video("share");

        foreach ($vr_photo as $key => $value) {
            $vr_mixer[]=$value;
        }

        foreach ($vr_video as $key => $value) {
            $vr_mixer[]=$value;
        }

        usort($vr_mixer, function ($item1, $item2) {
            return  $item2['shares_counter'] <=> $item1['shares_counter'];
        });


        $vr_mixer = array_slice($vr_mixer,1,9);


        $ret = array(
            "result"=>true,
            "error_message" =>"",
            "data" => $vr_mixer
        );
        $this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK);  
    }

    //3.熱門分享清單
    public function vr_newst_get(){


        $vr_mixer = array();
        $vr_photo = $this->getting_vr_photo("new");
        $vr_video = $this->getting_vr_video("new");

        foreach ($vr_photo as $key => $value) {
            $vr_mixer[]=$value;
        }

        foreach ($vr_video as $key => $value) {
            $vr_mixer[]=$value;
        }

        usort($vr_mixer, function ($item1, $item2) {
            return  $item2['create_time'] <=> $item1['create_time'];
        });


        $vr_mixer = array_slice($vr_mixer,1,9);


        $ret = array(
            "result"=>true,
            "error_message" =>"",
            "data" => $vr_mixer
        );
        $this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK);  
    }


    //4.熱門分享清單 30days
    public function vr_newst_more_get(){


        $vr_mixer = array();
        $vr_photo = $this->getting_vr_photo("more");
        $vr_video = $this->getting_vr_video("more");

        foreach ($vr_photo as $key => $value) {
            $vr_mixer[]=$value;
        }

        foreach ($vr_video as $key => $value) {
            $vr_mixer[]=$value;
        }

        usort($vr_mixer, function ($item1, $item2) {

            return  $item2['create_time'] <=> $item1['create_time'];
        });


        $vr_mixer = array_slice($vr_mixer,1,30);


        $ret = array(
            "result"=>true,
            "error_message" =>"",
            "data" => $vr_mixer
        );
        $this->set_response($ret, \Restserver\Libraries\REST_Controller::HTTP_OK);  
    }



    private function topic_filter_function($topic_name_filter="",$topic_category=array()){

        $department_list = $this->Common_model->get_db("tp_admin_group");

        foreach ($department_list as $key => $value) {
            $data["department_list"][$value->ag_id] = $value->ag_name;
        }

        $category = $this->Common_model->get_db("tp_topic_category",array("tc_enabled"=>"Y"),"tc_order");
        $category_name = array();
        foreach ($category as $key => $value) {
            $category_name[$value->tc_id] = $value->tc_title_zh;
        }

        $this->db->select('*');
        $this->db->from("tp_topic");
        $this->db->where("t_del","N");
        $this->db->order_by("t_order", "asc");
        $this->db->order_by("t_views", "desc");
        $this->db->limit(9);
        $query = $this->db->get();
        $topic = $query->result();
        if ($topic) {
            foreach ($topic as $key => $value) {
                $topic[$key]->t_image = $this->config->item("server_base_url")."upload/topic_image/".$topic[$key]->t_image;
                $topic[$key]->t_bk_image = $this->config->item("server_base_url")."upload/topic_image/".$topic[$key]->t_bk_image; 

                if($topic[$key]->tc_id==0){
                    $topic[$key]->tc_id = array();

                }else{
                    $topic[$key]->tc_id = explode(",", $topic[$key]->tc_id);

                    //顯示類別名稱
                    foreach ($topic[$key]->tc_id as $tc_key => $tc_value) {
                        $topic[$key]->tc_name[$tc_value] = $category_name[$tc_value];
                    }

                }
                unset($topic[$key]->t_type);
                unset($topic[$key]->t_topic_link);
                unset($topic[$key]->t_public_photo);
                unset($topic[$key]->t_public_video);
                unset($topic[$key]->t_public_ar);
                unset($topic[$key]->t_enabled);
                unset($topic[$key]->t_del);
                unset($topic[$key]->t_ar_app_link);
                unset($topic[$key]->t_co_organizer);
                unset($topic[$key]->ab_buid);
                unset($topic[$key]->t_create_timestamp);
                unset($topic[$key]->t_update_timestamp);

                $topic[$key]->t_views = (int)$topic[$key]->t_views;
                $topic[$key]->t_shares = (int)$topic[$key]->t_shares;
                $topic[$key]->t_order = (int)$topic[$key]->t_order;
                $topic[$key]->t_likes = $this->Common_model->count_db("user_topic",array("t_id"=>$topic[$key]->t_id));
                $topic[$key]->t_organizer = $data["department_list"][$topic[$key]->t_organizer];
            }
        }else{
            $topic = array();
        }

        
        return $topic;
    }

    private function getting_vr_photo($order_by="collection"){

        //搜尋是否存在於資料庫
        $where_array = array(
            'dvp_state' => 'Y',
            'dvp_del' => 'N'
        );
        $limit = 30;
        switch ($order_by) {
            case 'collection':
                $order_by ="dvp_collection";
                break;
            case 'share':
                $limit = 30;
                $order_by = "dvp_shares";
                break;
            case 'more':
                $limit = 30;
                $order_by = "dvp_create_timestamp";
                break;
            case 'new':
                $limit = 9;
                $order_by = "dvp_create_timestamp";
                break;
            default:
                $order_by ="dvp_create_timestamp";
                break;
        }

        $search_db = $this->Common_model->get_db("tp_department_vr_photo",$where_array,$order_by,"desc",$limit);


        $result = array();
        if ($search_db) {

            foreach ($search_db as $key => $value) {

                $result[$key]['id'] = $value->dvp_id;
                $result[$key]['type'] = "vr_photo";

                
                if($value->Is_url=="N"){
                    $result[$key]['vr_photo'] = $this->config->item('server_base_url').$value->dvp_img_path;
                }else{
                    $result[$key]['vr_url'] = $value->dvp_view_link;
                }  
            
                $result[$key]['photo_type'] = $value->photo_type;
                $result[$key]['name'] = $value->dvp_name;
                $result[$key]['capture_time'] = $value->dvp_capture_timestamp;                    
                $result[$key]['timestamp'] = strtotime($value->dvp_capture_timestamp);
                $result[$key]['show_image'] = $this->config->item('server_base_url').$value->dvp_rep_img_path;
                $result[$key]['lat'] = $value->dvp_lat;
                $result[$key]['lng'] = $value->dvp_lng;
                $result[$key]['view_counter'] = (int)$value->dvp_views;
                $result[$key]['shares_counter'] = (int)$value->dvp_shares;
                $result[$key]['collection_counter'] = (int)$value->dvp_collection;
                $result[$key]['create_time'] = $value->dvp_create_timestamp;

                //判斷照片為環景或全景 URL異動
                if ($value->photo_type=="pano") {
                        $result[$key]['url'] =  $value->dvp_view_link;
                        $result[$key]['mobile_url'] =  $this->config->item('server_base_url').'/kr_spherical?image_path='.$this->config->item('server_base_url').$value->dvp_backup;
                }else{
                        $result[$key]['url'] =  $value->dvp_view_link;
                        $result[$key]['mobile_url'] =  $this->config->item('server_base_url').'/kr_pano?image_path='.$this->config->item('server_base_url').$value->dvp_backup;
                }

                //直接吃連結
                if($value->Is_url=="Y"){
                    $result[$key]['url'] = $value->dvp_view_link;
                    $result[$key]['mobile_url'] = $value->dvp_view_link;
                }


                //判斷是為局處或是民眾  使用者名稱異動
                if ($value->is_civil=="Y") {
                    //民眾
                    $where_array = array(
                        'u_id' => $value->d_id
                    );

                    $the_user = $this->Common_model->get_one("user",$where_array);

                    if($the_user){
                        $result[$key]['uploader'] =  $the_user->u_name;
                    }else{
                        $result[$key]['uploader'] =  "Other";
                    }

                    
                }else{
                    //局處

                    $where_array = array(
                        'ag_id' => $value->d_id
                    );

                    $the_department= $this->Common_model->get_one("tp_admin_group",$where_array);

                    if($the_department){
                        $result[$key]['uploader'] =  $the_department->ag_name;
                    }else{
                        $result[$key]['uploader'] =  "Other";
                    }

                }
            }
            

            return $result;


        }

        return $result;
    }

    private function getting_vr_video($order_by="collection"){


        $this->db->select('t.d_id,t.dvv_id,t.dvv_name,t.dvv_rep_img_path,t.dvv_youtube_link,t.dvv_lat,t.dvv_lng,t.dvv_shares,t.dvv_views,t.dvv_collection,t.dvv_capture_timestamp,t.is_civil,t.Is_url,t.dvv_create_timestamp,u.u_name as u_name,a.ag_name as a_name');
        $this->db->from('tp_department_vr_video as t');
        $this->db->join("user as u", "u.u_id = t.d_id AND t.is_civil = 'Y'", "left");
        $this->db->join("admin_group as a", "a.ag_id = t.d_id AND t.is_civil != 'Y'", "left");

        $where_array = array(
                't.dvv_state' => 'Y',
                't.dvv_del' => 'N'
        );
        $limit = 9;
        switch ($order_by) {
            case 'collection':
                $order_by ="dvv_collection";
                break;
            case 'share':
                $order_by = "dvv_shares";
                break;
            case 'more':
                $order_by = "dvv_create_timestamp";
                $limit = 30;
                break;
            case 'new':
                $order_by = "dvv_create_timestamp";
                $limit = 9;
                break;
            default:
                $order_by ="dvv_create_timestamp";
                break;
        }

        
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->db->where($value);
            } else {
                $this->db->where($index, $value);
            }
        }
        $this->db->order_by($order_by,"DESC");
        $this->db->limit($limit);
        $query = $this->db->get();
        if($query){
            $vr_video_list = $query->result();
        }else{
            $vr_video_list = NULL;
        }

        $result = array();

        if ($query) {
            //整理資料
            foreach ($vr_video_list as $key => $value) {

                $result[$key]['id'] =  $value->dvv_id;
                $result[$key]['type'] =  "vr_video";
                $web_link = str_replace("https://www.youtube.com/watch?v=", "", $value->dvv_youtube_link);
                $mobile_link = str_replace("https://www.youtube.com/watch?v=", "", $value->dvv_youtube_link);

                $result[$key]['vr_video'] =  "https://www.youtube.com/watch?v=".$web_link;
                $result[$key]['vr_embed_video'] =  "https://www.youtube.com/embed/".$web_link;
                $result[$key]['mobile_vr_video'] =  "vnd.youtube:".$mobile_link;
                $result[$key]['name'] =  $value->dvv_name;
                

                $result[$key]['capture_time'] =  $value->dvv_capture_timestamp;
                $result[$key]['timestamp'] = strtotime($value->dvv_capture_timestamp);
                $result[$key]['show_image'] =  $this->config->item('server_base_url').$value->dvv_rep_img_path;

                $result[$key]['lat'] =  $value->dvv_lat;
                $result[$key]['lng'] =  $value->dvv_lng;

                $result[$key]['view_counter'] = (int)$value->dvv_views;
                $result[$key]['shares_counter'] = (int)$value->dvv_shares;
                $result[$key]['collection_counter'] = (int)$value->dvv_collection;
                $result[$key]['create_time'] = $value->dvv_create_timestamp;

                //判斷是為局處或是民眾  使用者名稱異動
                if ($value->is_civil=="Y") {
                    //民眾
                        $result[$key]['uploader'] =  $value->u_name;
                }else{
                    //局處
                        $result[$key]['uploader'] =  $value->a_name;
                }

            }
        }
        
        

        return $result;
    }
}