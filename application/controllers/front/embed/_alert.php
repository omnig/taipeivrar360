<?php

$data["menu_category"] = "alert";
$data["alert"] = $this->session->alert; //取得alert訊息

//沒有alert訊息，跳轉到首頁
if (!$data["alert"]) {
    header("Location: " . base_url("embed/"));
}
$this->session->unset_userdata("alert");

require_once "module/_main_menu.php";   //主menu
