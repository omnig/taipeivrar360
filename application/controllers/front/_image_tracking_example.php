<?php

$views = ['video_test', 'mindAR_test', 'jigsaw_puzzle_test', 'mindAR_sshm_test','mindAR_test_singleSample'];
if (in_array($func, $views)) {
    echo $this->load->view("{$this->controller_name}/{$func}", $data, true);
    exit();
}
