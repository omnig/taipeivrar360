<?php

$data["menu_category"] = "faq";
$data["meta"]["sub_title"] = $this->lang->line("FAQ");

require_once "module/_main_menu.php";   //主menu

//取得faq_category
$where_array = array(
    "fc_enabled" => 'Y'
);

$data["faq_category"] = $this->Common_model->get_db("faq_category", $where_array, "fc_order", "ASC");

//取得faq
$where_array = array(
    "f_enabled" => 'Y'
);

$faq = $this->Common_model->get_db("faq", $where_array, "f_order", "ASC");

//以fc_id分群
$data["faq"] = array();
foreach ($faq as $row) {
    if (!isset($data["faq"][$row->fc_id])) {
        $data["faq"][$row->fc_id] = array();
    }

    $data["faq"][$row->fc_id][] = $row;
}