<?php

class Migrate extends CI_Controller
{

        public function index()
        {
                $this->load->library('migration');
                $version = 5 ;
                if ($this->migration->version($version) === FALSE)
                {
                        show_error($this->migration->error_string());
                }else{
                	echo "Migrate Success!!".$version ;
                }
                
        }

}