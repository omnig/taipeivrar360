<?php
header('Access-Control-Allow-Origin: *');
/*
 * 取得分類資料
 */

$where_array = array();
$where_like_array = array();

$result = $this->Common_model->get_db("ap_category", $where_array, 'ac_order');


$category = array();
foreach ($result as $row) {
    $category[] = array(
        'title_zh' => $row->ac_title_zh,
        'title_en' => $row->ac_title_en,
        'enabled' => $row->ac_enabled
    );
}

//顯示呼叫結果
$ret = array(
    "result" => "true",
    "error_message" => "",
    "data" => $category
);

echo json_encode($ret);

exit();
