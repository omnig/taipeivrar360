<?php
date_default_timezone_set('Asia/Taipei');

$cpuUnuse = shell_exec("echo $(vmstat 1 2|tail -1|awk '{print $15}')");
$cpuUsage = 100 - (float)$cpuUnuse;

$memUnuse = shell_exec("free | grep Mem | awk '{print 100.0 - ($3/$2 * 100.0)}'");
$memUsage = 100 - (float)$memUnuse;

$d_date = date('Y-m-d');

// CPU 最大使用率
updateData('CPU_USAGE_MAX', $cpuUsage, '>', $d_date);

// CPU 最小使用率
updateData('CPU_USAGE_MIN', $cpuUsage, '<', $d_date);

// MEM 最大使用率
updateData('MEM_USAGE_MAX', $memUsage, '>', $d_date);

// MEM 最小使用率
updateData('MEM_USAGE_MIN', $memUsage, '<', $d_date);





function updateData($item, $number, $compare, $date){
    $CI = & get_instance();

    $record = $CI->Common_model->get_one("dashboard", [
        "d_date" => $date,
        "d_type" => $item
    ]);

    if(empty($record)){
    
        $insert_array = [
            "d_id" => createUUID(),
            "d_date" => $date,
            "d_type" => $item,
            "d_number" => $number
        ];
    
        $CI->Common_model->insert_db("dashboard", $insert_array);
    }else{
        $flag = false;
        switch($compare){
            case '<' :
                $flag = $number < $record->d_number;
                break;
            case '>' :
                $flag = $number > $record->d_number;
                break;
        }

        if($flag){
            $update_array = [
                "d_number" => $number
            ];
    
            $CI->Common_model->update_db("dashboard", $update_array, ['d_id' => $record->d_id]);
        }
    }
}


function createUUID(){
    $CI = & get_instance();
    return $CI->db->query('select uuid() as id')->row()->id;
}
