<?php

//篩選mail.log發送紀錄
$filename = APPPATH . "controllers/cli/data/epapersent_1213.txt";

$fp = fopen($filename, "r");
$id = 0;

while ($line = fgets($fp)) {
    //過濾資料
    $current_year = "2016";
    $current_month = "Dec";
    $current_day = substr($line, strpos($line, $current_month) + 4, 2);
    $current_time = trim(substr($line, strpos($line, $current_month) + 4 + 2, 10));
    $create_date = date_create($current_year . '-' . $current_month . '-' . $current_day . ' ' . $current_time);
    $current_date = date_format($create_date, "Y-m-d H:i:s");
    $email = substr($line, strpos($line, "to=<") + 4, strpos($line, ">,") - strpos($line, "to=<") - 4);
    //echo $current_date . ":" . $email . "<br>";
    //寫入資料庫
    $insert_array = array(
        "ef_email" => $email,
        "ef_send_timestamp" => $current_date
    );
    $this->Common_model->insert_db("email_filter", $insert_array);
    $id++;
}

echo $id . " success!";
exit();
