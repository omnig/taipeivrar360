<?php

/*
 * 此API負責定期清理失效訂單--ATM轉帳
 * 
 * ATM訂單超過7天未付款，訂單取消，通知買家「訂單已取消請勿付款」
 * ATM訂單超過5天未付款，通知買家「訂單尚未付款」
 */

/*
 * 先處理超過7天未付款的訂單
 */

//逾期時限為7天
$expire_time = date("Y-m-d H:i:s", time() - 60 * 60 * 24 * 7);

$where_array = array(
    "o_status_paid" => "N", //未付款
    "o_expire_remove_timestamp IS NULL", //尚未作逾期處理
    "ps_token" => "ATM", //以ATM付款的訂單
    "o_create_timestamp <" => $expire_time  //已逾付款期限
);

$expired_orders = $this->Common_model->get_db_join("order AS o", $where_array, 'pay_settings AS ps', 'o.ps_id=ps.ps_id', 'INNER');

//沒有逾期訂單，結束
if (!$expired_orders) {
    exit();
}

$o_ids = array();
foreach ($expired_orders as $row) {
    $o_ids[] = $row->o_id;
}

//取得所有需返還庫存的order_item
$where_array = array(
    "o_id" => $o_ids
);

$expired_order_items = $this->Common_model->get_db("order_item", $where_array);
//沒有需返還的庫存商品，結束
if (!$expired_order_items) {
    exit();
}

//定義寄信時間為上午8點
$scheduled_send_time = (strtotime(date("Y-m-d 08:00:00")) > time()) ? date("Y-m-d 08:00:00") : date("Y-m-d 08:00:00", strtotime(date("Y-m-d 08:00:00")) + 60 * 60 * 24);

if ($expired_order_items) {
    //準備寫入資料庫，啟動transation確保資料正確
    $this->Common_model->trans_start();

    //將保留數歸還至商品庫存
    foreach ($expired_order_items as $row) {
        $update_array = array(
            "p_inventory" => "`p_inventory`+{$row->oi_qty}",
            "p_pending_count" => "`p_pending_count`-{$row->oi_qty}"
        );

        $where_array = array(
            "p_id" => $row->p_id,
            "p_pending_count >=" => $row->oi_qty    //保留數足夠的才作退還
        );

        $this->Common_model->update_db_field("product", $update_array, $where_array);
    }

    //將訂單標示逾期處理時間
    foreach ($expired_orders as $row) {
        $update_array = array(
            "o_expire_remove_timestamp" => date("Y-m-d H:i:s")
        );

        $where_array = array(
            "o_id" => $row->o_id
        );

        $this->Common_model->update_db("order", $update_array, $where_array);
    }

    //準備寄發通知信，告知買家訂單已逾期
    $insert_array = array();

    foreach ($expired_orders as $row) {
        if (!$row->o_order_number) {
            //沒有訂單編號則不寄發通知信
            continue;
        }

        //載入語系檔
        $this->lang->load("mail", $row->o_buyer_i18n);
        $this->lang->load("front", $row->o_buyer_i18n);

        $data["order"] = $row;  //傳入信件內容

        $insert_array[] = array(
            "m_sender_mail" => $this->site_config->c_service_email,
            "m_sender_name" => $this->lang->line("嬉街客服郵件"),
            "m_receiver_mail" => $row->o_buyer_email,
            "m_receiver_name" => $row->o_buyer_name,
            "m_subject" => sprintf($this->lang->line("#【嬉街商城】訂單已逾期: %s")[$data["order"]->o_buyer_i18n], $data["order"]->o_order_number),
            "m_body" => $this->load->view("mail_content/order_expired", $data, true),
            "m_scheduled_send_time" => $scheduled_send_time
        );
    }

    //批次寫入
    $this->Common_model->insert_batch("mail_queue", $insert_array);

    //transation完成執行寫入
    $this->Common_model->trans_complete();
}

/*
 * 接續處理：
 * ATM訂單超過5天未付款，通知買家「訂單尚未付款」
 */

//逾期時限為5天
$expire_time = date("Y-m-d H:i:s", time() - 60 * 60 * 24 * 5);

$where_array = array(
    "o_order_number IS NOT NULL",
    "o_order_number !=" => '',
    "o_status_paid" => "N", //未付款
    "o_expire_remove_timestamp IS NULL", //尚未作逾期處理
    "ps_token" => "ATM", //以ATM付款的訂單
    "o_create_timestamp <" => $expire_time  //已逾付款期限
);

$expired_orders = $this->Common_model->get_db_join("order AS o", $where_array, 'pay_settings AS ps', 'o.ps_id=ps.ps_id', 'INNER');

//沒有逾期訂單，結束
if (!$expired_orders) {
    exit();
}

//準備寄發通知信，告知買家訂單即將逾期
$insert_array = array();

foreach ($expired_orders as $row) {
    if (!$row->o_order_number) {
        //沒有訂單編號則不寄發通知信
        continue;
    }

    //載入語系檔
    $this->lang->load("mail", $row->o_buyer_i18n);
    $this->lang->load("front", $row->o_buyer_i18n);

    $data["order"] = $row;  //傳入信件內容

    $insert_array[] = array(
        "m_sender_mail" => $this->site_config->c_service_email,
        "m_sender_name" => $this->lang->line("嬉街客服郵件"),
        "m_receiver_mail" => $row->o_buyer_email,
        "m_receiver_name" => $row->o_buyer_name,
        "m_subject" => sprintf($this->lang->line("#【嬉街商城】訂單即將逾期: %s")[$data["order"]->o_buyer_i18n], $data["order"]->o_order_number),
        "m_body" => $this->load->view("mail_content/order_will_expire", $data, true),
        "m_scheduled_send_time" => $scheduled_send_time
    );
}

//批次寫入
$this->Common_model->insert_batch("mail_queue", $insert_array);

exit();
