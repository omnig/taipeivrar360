<?php

/*
 * 此API提供背景呼叫發送email，應與epaper_sender發送時間錯開，避免負載過重
 */

//待發送mail queue
$now = date("Y-m-d H:i:s");

$where_array = array(
    'm_status' => "WAITING",
    'm_scheduled_send_time <=' => $now
);

$order_by = 'm_scheduled_send_time';
$order = 'asc';
$limit = 1000;  //每次發1千封

$mail_queue = $this->Common_model->get_db('mail_queue', $where_array, $order_by, $order, $limit);

if (!$mail_queue) {  //不存在或已發送完畢
    exit();
}

//將信件記錄為處理中
$m_ids = array();

foreach ($mail_queue as $row) {
    if ($row->m_subject != "" && $row->m_receiver_mail != "") {
        $m_ids[] = $row->m_id;
    }
}

//檢查是否有符合寄信條件
if (count($m_ids) == 0) {
    exit();
}

$update_array = array(
    'm_status' => "SENDING"
);

$where_array = array(
    'm_id' => $m_ids
);

$this->Common_model->update_db('mail_queue', $update_array, $where_array);

//逐封寄信
$this->load->library('sendmail_lib');

foreach ($mail_queue as $row) {
    $mail_body = $row->m_body;

    $this->sendmail_lib->set_sender($row->m_sender_mail, $row->m_sender_name);
    $this->sendmail_lib->set_content($row->m_subject, $mail_body);

    //設定副本抄送
    $cc_mails = explode(',', $row->m_cc_mail);
    $cc_names = explode(',', $row->m_cc_name);

    foreach ($cc_mails as $id => $cc_mail) {
        $this->sendmail_lib->set_cc($cc_mail, $cc_names[$id]);
    }

    //設定多個收件人
    $receiver_mails = explode(',', $row->m_receiver_mail);
    $receiver_names = explode(',', $row->m_receiver_name);

    $receivers = array();
    foreach ($receiver_mails as $id => $receiver_mail) {
        $receivers[] = array(
            "receiver_email" => $receiver_mail,
            "receiver_name" => $receiver_names[$id]
        );
    }

    //寄出
    $this->sendmail_lib->send_multi_receiver_mail($receivers);

    //更新此信件為已發送
    $update_array = array(
        'm_send_time' => date("Y-m-d H:i:s"),
        'm_status' => "DONE"
    );

    $where_array = array(
        'm_id' => $row->m_id
    );

    $this->Common_model->update_db('mail_queue', $update_array, $where_array);
}

