<?php

header('Access-Control-Allow-Origin: *');
/*
 * 取得樓層PLAN ID
 */

//傳入值接收
$input_array = array(
    "ab_buid" => $this->input->get("buid"),
    "af_number" => $this->input->get("f")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        //記錄
        $this->log($page, "INVALID VALUE", 'N');
        exit();
    }
}

//取得ab_id
$where_array = array(
    'ab_buid' => $input_array['ab_buid'],
    'ab_enabled' => "Y"
);
$building = $this->Common_model->get_one('ap_building', $where_array);

if (!$building) {
    $this->log($page, "INVALID BUID", 'N');
    exit();
}

$where_array = array(
    'ab_id' => $building->ab_id,
    'af_number' => $input_array['af_number'],
    'af_enabled' => 'Y'
);

$result = $this->Common_model->get_one("ap_floors", $where_array);

$floor = array(
    'floor_name' => $result->af_name,
    'url' => $this->config->item("server_base_url") . 'map/tile/',
    'plan_id' => $result->af_plan_id
);


//顯示呼叫結果
$ret = array(
    "result" => "true",
    "error_message" => "",
    "data" => $floor
);

echo json_encode($ret);

exit();
