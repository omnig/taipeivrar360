<?php

$filename = APPPATH . "controllers/cli/data/mbrs.csv";

$rows = array();

$counter = array(
    "total" => 0,
    "insert" => 0,
    "duplicated" => 0,
    "error" => 0
);

if (($handle = fopen($filename, "r")) !== FALSE) {
    //跳過第一行
    //fgetcsv($handle, 0, ",", '"');

    while (($data = fgetcsv($handle, 0, ",", '"')) !== FALSE) {
        $counter["total"] ++;
        //欄位對映：
        //"mbr_id","user_id","user_email","user_pin","contact_email","login_token","user_name","bonus","redeem_bonus","pref_json","login_expire_ts","curr_month_ref_ts","history_bonus","social_bit","friend_cnt","icode","fb_json","fb_token","push_freq","last_login","viewed","today_viewed","push_token","push_failed","ref_ip","ref_ua","fb_ts","fb_raw","status","smart_token","smart_json","fb_id","phn","aptg_id","aptg_json","device_id"
        $insert_array = array(
            "u_mbr_id" => $data[0], //"mbr_id"
            "u_account" => $data[1], //"user_id"
            "u_email" => $data[2], //"user_email"
            "u_password" => $data[3], //"user_pin"
            //"contact_email"
            "u_login_token" => $data[5], //"login_token"
            "u_name" => $data[6] ? $data[6] : "App#{$data[0]}", //"user_name"
            //"bonus"
            //"redeem_bonus"
            "u_pref_json" => $data[9], //"pref_json"
            //"login_expire_ts"
            //"curr_month_ref_ts"
            //"history_bonus"
            //"social_bit"
            //"friend_cnt"
            //"icode"
            //"fb_json"
            "u_fb_token" => $data[17], //"fb_token"
            "u_push_freq" => $data[18], //"push_freq"
            "u_last_login_timestamp" => $data[19], //"last_login"
            //"viewed"
            //"today_viewed"
            "u_push_token" => $data[22], //"push_token"
            //"push_failed"
            "u_ip" => $data[24], //"ref_ip"
            "u_user_agent" => $data[25], //"ref_ua"
            "u_fb_ts" => $data[26], //"fb_ts"
            //"fb_raw"
            //"u_status" => $data["status"],    //"status"
            //"smart_token"
            //"smart_json"
            "u_fbid" => $data[31], //"fb_id"
            "u_phone" => $data[32], //"phn"
            "u_aptg_id" => $data[33], //"aptg_id"
            "u_aptg_json" => $data[34], //"aptg_json"
            "u_device_id" => $data[35]    //"device_id"
        );

        //檢查是否有重複的帳號或電話
        if ($insert_array["u_account"]) {
            $where_array = array(
                "u_account" => $insert_array["u_account"]
            );
        } else {
            $where_array = array(
                "u_phone" => $insert_array["u_phone"]
            );
        }

        $duplicated_user = $this->Common_model->get_one("user", $where_array);

        if ($duplicated_user) {
            $counter["duplicated"] ++;
            continue;
        }

        //寫入資料庫
        $rows_affected = $this->Common_model->insert_db("user", $insert_array);
        if ($rows_affected > 0) {
            $counter["insert"] ++;
        } else {
            $counter["error"] ++;
        }
    }
    fclose($handle);
}

die(json_encode($counter));
exit();
