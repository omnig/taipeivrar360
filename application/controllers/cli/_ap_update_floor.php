<?php

echo date("Y-m-d H:i:s", time()) . "<br><br>";


$anyplace_api_url = $this->config->item('anyplace_api_url');

$api_name = "mapping/";
$url_path = $anyplace_api_url . $api_name;


//取得所有建物
$where_array = array();
if ($this->input->get("buid")) {
    $where_array = array(
        "ab_buid" => $this->input->get("buid")
    );
}
$building = $this->Common_model->get_db("ap_building", $where_array);

if (!$building) {
    echo "查無建物資料";
    exit();
}

$datas = array();

//API樓層資料
foreach ($building as $row) {

    $url = $url_path . "floor/all";
    $data_query = array(
        "buid" => $row->ab_buid
    );
    $result = curl_query($url, "post", $data_query);
    // echo ($url);
    // exit();
    $result = json_decode($result, true);

    if (isset($result["floors"])) {
        $datas[] = $result["floors"];
    }else{
        $datas[] = array();
    }

    
}

//若沒有資料或讀取資料錯誤，則結束
if (!$datas) {
    _alert("無法取得API資料");
    exit();
}

//刪除既有資料
/*
  $where_array = array();
  $this->Common_model->delete_db("ap_floors", $where_array);
  $this->Common_model->delete_db("ap_pois", $where_array);
 */

$insert_array = array();

//新增FLOORS資料庫
foreach ($datas as $floors) {
    $ab_id = '';
    if($floors){
        foreach ($floors as $row) {

            $db_name = "ap_floors";
            $where_array = array(
                "af_fuid" => $row['fuid']
            );
            $ap_floors = $this->Common_model->get_one($db_name, $where_array);

            //取得對應building ID
            foreach ($building as $b) {
                if ($b->ab_buid == $row['buid']) {
                    $ab_id = $b->ab_id;
                }
            }
            if (!$ab_id) {
                continue;
            }

            //若不存在則新增
            if (!$ap_floors) {

                $insert_array = array(
                    "ab_id" => $ab_id,
                    "af_number" => $row["floor_number"],
                    "af_name" => $row["floor_name"],
                    "af_desc" => $row["description"],
                    "af_fuid" => $row["fuid"],
                    'af_order' => $row["floor_number"],
                    "af_bottom_left_lat" => $row["bottom_left_lat"],
                    "af_bottom_left_lng" => $row["bottom_left_lng"],
                    "af_top_right_lat" => $row["top_right_lat"],
                    "af_top_right_lng" => $row["top_right_lng"],
                    'af_create_timestamp' => date('Y-m-d H:i:s')
                );
                $this->Common_model->insert_db($db_name, $insert_array);
                $f_id = $this->Common_model->get_insert_id();
                echo "insert f_id:" . $f_id . "<br>";
            } else {
                $where_array = array(
                    "af_fuid" => $row['fuid']
                );
                $update_array = array(
                    "af_number" => $row["floor_number"],
                    "af_name" => $row["floor_name"],
                    "af_desc" => $row["description"],
                    "af_fuid" => $row["fuid"],
                    'af_order' => $row["floor_number"],
                    "af_last_update_timestamp" => date("Y-m-d H:i:s")
                );
                if ($row["bottom_left_lat"]) {
                    $update_array["af_bottom_left_lat"] = $row["bottom_left_lat"];
                }
                if ($row["bottom_left_lng"]) {
                    $update_array["af_bottom_left_lng"] = $row["bottom_left_lng"];
                }
                if ($row["top_right_lat"]) {
                    $update_array["af_top_right_lat"] = $row["top_right_lat"];
                }
                if ($row["top_right_lng"]) {
                    $update_array["af_top_right_lng"] = $row["top_right_lng"];
                }
                $this->Common_model->update_db($db_name, $update_array, $where_array);
                echo 'update f_id:' . $ap_floors->af_id . '<br>';
            }
        }
    }
    
}


//取得所有樓層
$where_array = array();
if ($this->input->get("buid")) {
    $where_array = array(
        "ab_buid" => $this->input->get("buid")
    );
}
$floors = $this->Common_model->get_db_join("ap_building as b", $where_array, "ap_floors as f", "b.ab_id=f.ab_id");

if (!$floors) {
    echo "無法取得樓層資料";
    exit();
}

$datas = array();


foreach ($floors as $f) {

    //取得API的POI資料
    $ab_buid = $f->ab_buid;
    $af_number = $f->af_number;
    $ab_id = $f->ab_id;
    $af_id = $f->af_id;
    $url = $url_path . "pois/all_floor";
    $data_query = array(
        "buid" => $ab_buid,
        "floor_number" => (string)$af_number
    );
    $result = curl_query($url, "post", $data_query);
    $result = json_decode($result, true);
    // echo json_encode($result);
    // exit;
    if (isset($result["pois"])) {
        $floor = $result["pois"];
    }else{
        $floor = array();
    }

    //若沒有資料則繼續查
    if (!$floor) {
        echo $f->ab_name . '[' . $f->af_desc . "]: 尚未建立 POI 資料" . "<br>";
        continue;
    }

    //新增或更新POI資料
    foreach ($floor as $row) {

        if ($row["name"] == "Connector") {
            continue;
        }

        $db_name = "ap_pois";
        $where_array = array(
            "ap_puid" => $row['puid']
        );
        $ap_pois = $this->Common_model->get_one($db_name, $where_array);

        //檢查POI分類
        $ac_id = "";
        if ($row["pois_type"]) {
            $where_array = array();
            $where_like_array = array(
                'ac_title_zh' => $row["pois_type"],
                'ac_title_en' => $row["pois_type"]
            );
            $ac_id = $this->Common_model->get_one_field("ap_category", "ac_id", $where_array, '', '', $where_like_array);

            if (!$ac_id) {
                $insert_array = array(
                    "ac_title_en" => $row["pois_type"],
                    "ac_title_zh" => $row["pois_type"]
                );
                $this->Common_model->insert_db("ap_category", $insert_array);
                $ac_id = $this->Common_model->get_insert_id();
            }
        }

        //若不存在則新增
        if (!$ap_pois) {
            $insert_array = array(
                "af_id" => $af_id,
                "ap_puid" => $row["puid"],
                "ap_name" => $row["name"],
                "ap_desc" => $row["description"],
                "ac_id" => $ac_id,
                "ap_lat" => $row["coordinates_lat"],
                "ap_lng" => $row["coordinates_lon"],
                "ap_is_building_entrance" => ($row["is_building_entrance"] == "true") ? "Y" : "N",
                "ap_is_door" => ($row["is_door"] == "true" ) ? "Y" : "N",
                'ap_create_timestamp' => date('Y-m-d H:i:s')
            );
            $this->Common_model->insert_db($db_name, $insert_array);

            $p_id = $this->Common_model->get_insert_id();
            echo 'insert p_id:' . $p_id . "<br>";
        } else {
            $where_array = array(
                "ap_puid" => $row["puid"],
            );
            $update_array = array(
                "ap_name" => $row["name"],
                "ap_desc" => $row["description"],
                "ac_id" => $ac_id,
                "ap_lat" => $row["coordinates_lat"],
                "ap_lng" => $row["coordinates_lon"],
                "ap_is_building_entrance" => ($row["is_building_entrance"] == "true") ? "Y" : "N",
                "ap_is_door" => ($row["is_door"] == "true" ) ? "Y" : "N",
                "ap_last_update_timestamp" => date("Y-m-d H:i:s")
            );
            $this->Common_model->update_db($db_name, $update_array, $where_array);

            // 更新AR 點位經緯度
            update_ar_lat_lon($row["puid"],$row["coordinates_lat"],$row["coordinates_lon"]);

            echo 'update p_id:' . $ap_pois->ap_id . ":" . $row["name"] . " [" . $row["pois_type"] . "]<br>";
        }
    }
}


echo "<br>" . date("Y-m-d H:i:s", time());

function update_ar_lat_lon($puid,$lat,$lon){

    $CI = & get_instance();
    $where_array =array(
        "ap_puid" => $puid
    );
    $ar_pois = $CI->Common_model->get_one("department_ar", $where_array);

    if($ar_poi!= null){

    }

    $update_array = array(
        "tdar_lat" => $lat,
        "tdar_lng" => $lon
    );

    $CI->Common_model->update_db("department_ar", $update_array, $where_array);

}
//$this->log_update($api_name, "Y", $update_rows, $start_time);
