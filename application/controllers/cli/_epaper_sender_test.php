<?php

/*
 * 此API提供背景呼叫發送限定筆數之電子報
 */
echo "Start Time: " . date("Y-m-d H:i:s") . "\n";

//寄發通知信
$this->load->library('sendmail_lib');

//mail內容必須在載入library後
$mail_body = "test";

$this->sendmail_lib->set_sender("service@utobonus.com", "嬉街客服信箱");
$this->sendmail_lib->set_content("[嬉街商城]電子報測試", $mail_body);

//成功寄出epaper的數量
$success_count = 0;

$limit = 50;

//發送email 
for ($i = 0; $i < $limit; $i++) {
    //設定收件人
    $send_name = "christine";
    $send_email = "christine@omniguider.com";
    if (($i % 2) == 1) {
        $send_name = "test";
        $send_email = "test@omniguider.com";
    }
    $this->sendmail_lib->set_receiver($send_email, $send_name);

    //寄發email
    if ($this->sendmail_lib->send_mail()) {
        $success_count++;
        echo "sent: " . date("H:i:s") . " (" . $i . ")" . $send_name . "\n";
    }
}

echo "End Time: " . date("Y-m-d H:i:s") . "\n\n";
echo "success num: " . $success_count;
