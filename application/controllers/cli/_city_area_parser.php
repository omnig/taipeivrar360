<?php

/*
 * 行政區parsing，僅在建立行政區資料時執行一次
 */

$url = "http://download.post.gov.tw/post/download/Zip32_csv_10409_utf8.csv";

$csv = curl_query($url, "GET");

$array = array_map("str_getcsv", explode("\n", $csv));

$city_area = array();
foreach ($array as $row) {
    if (isset($row[1])) {
        if (!isset($city_area[$row[1]])) {
            $city_area[$row[1]] = array();
        }
        $city_area[$row[1]][$row[2]] = substr($row[0], 0, 3);
    }
}

//刪除原有資料
$where_array = array(
    "c_id >" => 0
);
$this->Common_model->delete_db("city", $where_array);
$this->Common_model->delete_db("area", $where_array);

foreach ($city_area as $city_name => $areas) {
    //寫入city資料表
    $insert_array = array(
        "c_name" => $city_name
    );

    $this->Common_model->insert_db("city", $insert_array);
    $c_id = $this->Common_model->get_insert_id();

    foreach ($areas as $area_name => $zip) {
        //寫入area資料表
        $insert_array = array(
            "c_id" => $c_id,
            "a_name" => $area_name,
            "a_zip" => $zip
        );

        $this->Common_model->insert_db("area", $insert_array);
    }
}
