<?php

$data_name = 'tp_VIEWPOINTS';
$data_table = 'viewpoints';


// 從JSON匯入景點
$json = "application/controllers/cli/data/ty_viewpoints.json";
$json_data =  file_get_contents($json);

// echo ($json_data);
// exit;


$insert_rows = 0;
$update_rows = 0;
$counter = array(
    "total" => 0,
    "insert" => 0,
    "duplicated" => 0,
    "error" => 0
);


$start_time = time();
$ret = "Y";
$ret_data = array();

//取得資料
$viewpoints = json_decode($json_data,TRUE);
$viewpoints = $viewpoints["infos"];



//取得DB資料
$db_data = $this->Common_model->get_db($data_table);



//寫入資料
$insert_array = array();
foreach ($viewpoints as $row) {
    //檢查是否已存在DB
    $is_data = check_dataid($db_data, $row["InfoId"]);

    if ($is_data) {
        $where_array = array(
            'v_dataid' => $row["InfoId"]
        );
        $update_array = array(
            'v_name' => $row["Name"],
            'v_desc' => $row["Toldescribe"],
            'a_id' => $row["Zipcode"],
            'v_address' => $row["Add"],
            'v_class' => "1",
            'v_level' => "1",
            'v_web' => (strpos($row["TYWebsite"],"http"))?$row["TYWebsite"]:"http://".$row["TYWebsite"],
            'v_tel' => $row["Tel"],
            'v_image' => "",
            'v_ticketinfo' => $row["Ticketinfo"],
            'v_opentime' => $row["Opentime"],
            'v_travellinginfo' => $row["Parkinginfo"],
            'v_lat' => $row["Py"],
            'v_lng' => $row["Px"],
            'v_remarks' => $row["Remarks"],
            'v_changetime' => $row["Changetime"],
            'v_update_timestamp' => date('Y-m-d H:i:s')
        );
        $this->Common_model->update_db($data_table, $update_array, $where_array);
        $update_rows++;
    } else {
        $insert_array[] = array(
            'v_dataid' => $row["InfoId"],
            'v_name' => $row["Name"],
            'v_desc' => $row["Toldescribe"],
            'a_id' => $row["Zipcode"],
            'v_address' => $row["Add"],
            'v_class' => "1",
            'v_level' => "1",
            'v_web' => (strpos($row["TYWebsite"],"http"))?$row["TYWebsite"]:"http://".$row["TYWebsite"],
            'v_tel' => $row["Tel"],
            'v_image' => "",
            'v_ticketinfo' => $row["Ticketinfo"],
            'v_opentime' => $row["Opentime"],
            'v_travellinginfo' => $row["Parkinginfo"],
            'v_lat' => $row["Py"],
            'v_lng' => $row["Px"],
            'v_remarks' => $row["Remarks"],
            'v_changetime' => $row["Changetime"],
            'v_api_from' => $data_name
        );
    }
}

//批次寫入
if (count($insert_array) > 0) {
    $this->Common_model->batch_insert_db($data_table, $insert_array);
    $insert_rows = count($insert_array);
}
$rows_affected = $update_rows + $insert_rows;


if ($rows_affected > 0) {
    $counter["insert"] = $rows_affected;
} else {
    $counter["error"] = count($ret_data);
}


function check_dataid($data_array, $dataid) {

    $is_exist = false;
    if(count($data_array)>0)
    {
        foreach ($data_array as $row) {
            if ($row->v_dataid == $dataid) {
                $is_exist = true;
                break;
            }
        }
        return $is_exist;
    }
}

echo json_encode($ret_data);

exit();




