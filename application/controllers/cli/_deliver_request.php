<?php

/*
 * 此API負責處理未出貨稽催信件
 * 
 * 付款後第3天未出貨，通知店家管理員
 * 付款後第5天未出貨，通知店家管理員&系統管理後台
 */

/*
 * 先處理付款後第5天未出貨
 */

//逾期時限5天
$expire_time = date("Y-m-d H:i:s", time() - 60 * 60 * 24 * 5);
//逾期6天以上就略過
$expire_time_over = date("Y-m-d H:i:s", time() - 60 * 60 * 24 * 6);

$where_array = array(
    "o_status_paid" => "Y", //已付款
    "os_status_shipped" => "N", //未出貨
    "o_payment_timestamp <" => $expire_time, //已逾出貨期限
    "o_payment_timestamp >" => $expire_time_over //超過6天仍未出貨就不寄
);

$expired_orders = $this->Common_model->get_db_join("order AS o", $where_array, 'order_store AS os', 'o.o_id=os.o_id', 'INNER');

//沒有未出貨，結束
if (!$expired_orders) {
    exit();
}

$s_ids = array();
foreach ($expired_orders as $row) {
    $s_ids[] = $row->s_id;
}

$this->load->model('Order_model');
//取得所有未出貨店家email
$where_array = array(
    "os.s_id" => $s_ids
);
$expired_store_owner = $this->Order_model->get_store_owner_by_order_store($where_array);

//定義寄信時間為上午10點
$scheduled_send_time = date("Y-m-d H:i:s", time());

//準備寫入資料庫，啟動transation確保資料正確
$this->Common_model->trans_start();

//準備寄發通知信，告知賣家訂單未出貨
$insert_array = array();

foreach ($expired_orders as $row) {

    $data["order"] = $row;  //傳入信件內容
    $data["delay"] = "5";

    //取得店家email
    $data["admin"] = "";
    foreach ($expired_store_owner as $eso) {
        if ($row->s_id == $eso->s_id) {
            $data["admin"] = $eso;

            //載入語系檔
            $this->lang->load("mail", $data["admin"]->adm_i18n);
            $this->lang->load("front", $data["admin"]->adm_i18n);

            //寄給店家
            $insert_array[] = array(
                "m_sender_mail" => $this->site_config->c_service_email,
                "m_sender_name" => $this->lang->line("嬉街客服郵件"),
                "m_receiver_mail" => $data["admin"]->adm_email,
                "m_receiver_name" => $data["admin"]->adm_name,
                "m_subject" => sprintf($this->lang->line("#【嬉街商城】尚未出貨通知: %s")[$data["admin"]->adm_i18n], $data["order"]->o_order_number),
                "m_body" => $this->load->view("mail_content/store_deliver_request", $data, true),
                "m_scheduled_send_time" => $scheduled_send_time
            );
        }
    }

    //載入語系檔
    $this->lang->load("mail", "zh");
    $this->lang->load("front", "zh");

    //寄給系統管理員
    $insert_array[] = array(
        "m_sender_mail" => $this->site_config->c_service_email,
        "m_sender_name" => $this->lang->line("嬉街客服郵件"),
        "m_receiver_mail" => $this->site_config->c_sa_email,
        "m_receiver_name" => $this->lang->line("嬉街客服郵件"),
        "m_subject" => sprintf($this->lang->line("#【嬉街商城】尚未出貨通知: %s")["zh"], $data["order"]->o_order_number),
        "m_body" => $this->load->view("mail_content/sa_deliver_request", $data, true),
        "m_scheduled_send_time" => $scheduled_send_time
    );
}

//批次寫入
$this->Common_model->insert_batch("mail_queue", $insert_array);

//transation完成執行寫入
$this->Common_model->trans_complete();


/*
 * 接續處理：
 * 付款後第3天未出貨
 */

//逾期時限為3天
$expire_time = date("Y-m-d H:i:s", time() - 60 * 60 * 24 * 3);
$expire_time_over = date("Y-m-d H:i:s", time() - 60 * 60 * 24 * 4);

$where_array = array(
    "o_status_paid" => "Y", //已付款
    "os_status_shipped" => "N", //未出貨
    "o_payment_timestamp <" => $expire_time, //已逾出貨期限
    "o_payment_timestamp >" => $expire_time_over
);

$expired_orders = $this->Common_model->get_db_join("order AS o", $where_array, 'order_store AS os', 'o.o_id=os.o_id', 'INNER');

//沒有未出貨，結束
if (!$expired_orders) {
    exit();
}

$s_ids = array();
foreach ($expired_orders as $row) {
    $s_ids[] = $row->s_id;
}

//取得所有未出貨店家email
$where_array = array(
    "s_id" => $s_ids
);
$expired_store_owner = $this->Order_model->get_store_owner_by_order_store($where_array);


//準備寄發通知信，告知賣家尚未出貨
$insert_array = array();

foreach ($expired_orders as $row) {

    $data["order"] = $row;  //傳入信件內容
    $data["delay"] = "3";

    //取得店家email
    $data["admin"] = "";
    foreach ($expired_store_owner as $eso) {
        if ($row->s_id == $eso->s_id) {
            $data["admin"] = $eso;

            //載入語系檔
            $this->lang->load("mail", $data["admin"]->adm_i18n);
            $this->lang->load("front", $data["admin"]->adm_i18n);

            $insert_array[] = array(
                "m_sender_mail" => $this->site_config->c_service_email,
                "m_sender_name" => $this->lang->line("嬉街客服郵件"),
                "m_receiver_mail" => $data["admin"]->adm_email,
                "m_receiver_name" => $data["admin"]->adm_name,
                "m_subject" => sprintf($this->lang->line("#【嬉街商城】尚未出貨通知: %s")[$data["admin"]->adm_i18n], $data["order"]->o_order_number),
                "m_body" => $this->load->view("mail_content/store_deliver_request", $data, true),
                "m_scheduled_send_time" => $scheduled_send_time
            );
        }
    }
}

//批次寫入
$this->Common_model->insert_batch("mail_queue", $insert_array);

exit();
