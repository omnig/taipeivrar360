<?php

//初始化，首次建立資料使用
//起始日期：最早一筆store_balance資料

$where_array = array(
    "sb_balance_timestamp IS NOT NULL"
);

$first_record = $this->Common_model->get_one("store_balance", $where_array, "sb_balance_timestamp,sb_id", "ASC");
$limit = 1000;  //防止無窮迴圈

if (!$first_record) {
    exit();
}

$time = strtotime($first_record->sb_balance_timestamp);

while ($time < time() && $limit-- > 0) {
    $date = date("Y-m-d", $time);
    $this->store_balance->init_pay_store_record($date);
    $time += 60 * 60 * 24;  //每天都檢驗
}