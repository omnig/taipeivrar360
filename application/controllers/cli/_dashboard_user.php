<?php
date_default_timezone_set('Asia/Taipei');

$d_date = date("Y-m-d");

/*
 * 統計每日使用人次
 */

$where_array = array(
    "d_date" => $d_date,
    "d_type" => "USE_COUNT"
);

$record = $this->Common_model->get_one("dashboard", $where_array);

$oneHourAgo = date("Y-m-d H:00:00", strtotime("-1 hour"));
$current = date("Y-m-d H:00:00");

// 前一小時記錄次數
$oneHourCount = $this->db->query("select count(*) as count from tp_io_record where (io_start_time >= '$oneHourAgo' and io_start_time < '$current')")->row()->count;

if (empty($record)) {

    $total = $oneHourCount;

    $insert_array = [
        "d_id" => createUUID(),
        "d_date" => $d_date,
        "d_type" => "USE_COUNT",
        "d_number" => $total
    ];

    $this->Common_model->insert_db("dashboard", $insert_array);
}else{

    $total = $record->d_number + $oneHourCount;

    $update_array = [
        "d_number" => $total
    ];

    $this->Common_model->update_db("dashboard", $update_array, ['d_id' => $record->d_id]);
}


/*
 * 統計每日同時上線人數
 */

$where_array = array(
    "d_date" => $d_date,
    "d_type" => "ONLINE_COUNT"
);

$record = $this->Common_model->get_one("dashboard", $where_array);

$oneHourAgo = date("Y-m-d H:00:00", strtotime("-1 hour"));
$current = date("Y-m-d H:00:00");

// 前一小時記錄人數
$oneHourCount = $this->db->query("select count(distinct io_device_id) as count from tp_io_record where (io_start_time >= '$oneHourAgo' and io_start_time < '$current') or (io_start_time >= '$d_date' and io_start_time < '$oneHourAgo' and io_end_time is null)")->row()->count;

if (empty($record)) {

    $total = $oneHourCount;

    $insert_array = [
        "d_id" => createUUID(),
        "d_date" => $d_date,
        "d_type" => "ONLINE_COUNT",
        "d_number" => $total
    ];

    $this->Common_model->insert_db("dashboard", $insert_array);
}else{
    if($record->d_number < $oneHourCount){
        $total = $oneHourCount;

        $update_array = [
            "d_number" => $total
        ];

        $this->Common_model->update_db("dashboard", $update_array, ['d_id' => $record->d_id]);
    }
}


function createUUID(){
    $CI = & get_instance();
    return $CI->db->query('select uuid() as id')->row()->id;
}