<?php

/*
 * 全面更新store_balance process fee
 */

$os_ids = $this->Store_balance_model->get_distinct_os_id();

if (!$os_ids) {
    exit();
}

foreach ($os_ids as $row) {
    $this->store_balance->update_process_fee($row->os_id);
}
