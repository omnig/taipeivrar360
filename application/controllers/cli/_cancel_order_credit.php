<?php

/*
 * 此API負責定期清理失效訂單--信用卡
 * 信用卡付款失效處理毋須通知店家/使用者
 */

//逾期時限為2小時
$expire_time = date("Y-m-d H:i:s", time() - 60 * 60 * 2);

$where_array = array(
    "o_status_paid" => "N", //未付款
    "o_expire_remove_timestamp IS NULL", //尚未作逾期處理
    "ps_token" => "CREDIT", //以信用卡付款的訂單
    "o_create_timestamp <" => $expire_time  //已逾付款期限
);

$expired_orders = $this->Common_model->get_db_join("order AS o", $where_array, 'pay_settings AS ps', 'o.ps_id=ps.ps_id', 'INNER');

//沒有逾期訂單，結束
if (!$expired_orders) {
    exit();
}

$o_ids = array();
foreach ($expired_orders as $row) {
    $o_ids[] = $row->o_id;
}

//取得所有需返還庫存的order_item
$where_array = array(
    "o_id" => $o_ids
);

$expired_order_items = $this->Common_model->get_db("order_item", $where_array);
//沒有需返還的庫存商品，結束
if (!$expired_order_items) {
    exit();
}

//準備寫入資料庫，啟動transation確保資料正確
$this->Common_model->trans_start();

//將保留數歸還至商品庫存
foreach ($expired_order_items as $row) {
    $update_array = array(
        "p_inventory" => "`p_inventory`+{$row->oi_qty}",
        "p_pending_count" => "`p_pending_count`-{$row->oi_qty}"
    );

    $where_array = array(
        "p_id" => $row->p_id,
        "p_pending_count >=" => $row->oi_qty    //保留數足夠的才作退還
    );

    $this->Common_model->update_db_field("product", $update_array, $where_array);
}

//將訂單標示逾期處理時間
foreach ($expired_orders as $row) {
    $update_array = array(
        "o_expire_remove_timestamp" => date("Y-m-d H:i:s")
    );

    $where_array = array(
        "o_id" => $row->o_id
    );

    $this->Common_model->update_db("order", $update_array, $where_array);
}

//transation完成執行寫入
$this->Common_model->trans_complete();

exit();
