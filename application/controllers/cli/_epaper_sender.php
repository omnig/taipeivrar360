<?php

/*
 * 此API提供背景呼叫發送限定筆數之電子報
 */

//取得電子報，並確認目前發送進度
$where_array = array(
    'e_process_index <' => 0,
    'e_reserve_send_time <=' => date("Y-m-d H:i:s")
);
$epaper = $this->Common_model->get_one('epaper', $where_array);



if (!$epaper) {   //不存在或已發送完畢
    exit();
}



//寄發通知信
$this->load->library('sendmail_lib');

//mail內容必須在載入library後
$mail_body = $epaper->e_content;

$this->sendmail_lib->set_sender($epaper->e_sender_email, $epaper->e_sender_name);
$this->sendmail_lib->set_content($epaper->e_title, $mail_body);

$process_index = $epaper->e_process_index;

//成功寄出epaper的數量
$success_count = 0;

//取得傳送email清單
$where_array = array(
    'u_subscribe_confirmed' => 'Y',
    'u_enabled' => 'Y',
    'u_del' => 'N',
    'u_subscribe_mail != ""',
    'u_id >' => $process_index
);

$order_by = 'u_id';
$order = 'DESC';
$limit = 50;

$users = $this->Common_model->get_db('user', $where_array, $order_by, $order, $limit);

$where_array = array(
    'e_id' => $epaper->e_id
);

if (!$users) {
    //沒有user，表示已發送完畢，記錄並結束
    $update_array = array(
        'e_process_index' => 0
    );

    $this->Common_model->update_db('epaper', $update_array, $where_array);

    exit();
}

//更新process_index
foreach ($users as $row) {
    $process_index = $row->u_id;  //移動 process_index
}

$update_array = array(
    'e_process_index' => $process_index
);

$this->Common_model->update_db('epaper', $update_array, $where_array);

//對user發送email    
foreach ($users as $i => $row) {
    //設定收件人
    $this->sendmail_lib->set_receiver($row->u_subscribe_mail, $row->u_name);
    
    //寄發email
    if ($this->sendmail_lib->send_mail()) {
        $success_count++;
    }
}

//更新目前已發送的進度
$update_array = array(
    'e_send_count' => $epaper->e_send_count + $success_count
);

$this->Common_model->update_db('epaper', $update_array, $where_array);
