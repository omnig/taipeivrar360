<?php

/*
 * 此API為首次建立store_balance初始資料用
 * 不允許重複執行
 */

//檢查store_balance是否為空，為空才可以執行
$store_balance = $this->Common_model->get_one("store_balance");
if ($store_balance) {
    echo "store_balance is NOT empty!";
    exit();
}

/*
 * 1. 訂單付款-加減帳：此時balance_timestamp為空值
 */
$where_array = array(
    "o_status_paid" => 'Y'
);
$order = $this->Common_model->get_db("order", $where_array);

if ($order) {
    foreach ($order as $row) {
        $this->store_balance->new_paid_order($row->o_id);
    }
}

/*
 * 2. 主訂單出貨：此訂單balance_timestamp為空值者，定義計帳時間為此時間點
 */
$where_array = array(
    "o_status_paid" => 'Y',
    "o_status_shipped" => 'Y'
);
$order = $this->Common_model->get_db("order", $where_array);

if ($order) {
    foreach ($order as $row) {
        //以出貨時間為計帳時間
        $this->store_balance->commit_balance_timestamp($row->o_id, $row->o_delivery_timestamp);
    }
}

/*
 * 3. 退貨-減帳：記錄退貨時間點
 */
$where_array = array(
    "oi_qty <" => 0
);
$order_item = $this->Common_model->get_db("order_item", $where_array, "oi_id", "ASC");

if ($order_item) {
    foreach ($order_item as $row) {
        //以出貨時間為計帳時間
        $this->store_balance->new_refund_item($row->oi_id);
    }
}

/*
 * 4. 退貨至全部退光-退運費：減帳
 */
$where_array = array(
    "`os_item_amount` = `os_refund_amount`"
);
$order_store = $this->Common_model->get_db("order_store", $where_array);

if ($order_store) {
    foreach ($order_store as $row) {
        $this->store_balance->cancel_shipment($row->os_id);
    }
}

exit();
