<?php

require_once APPPATH . 'third_party/facebook-php-sdk/src/Facebook/autoload.php';
// include_once APPPATH . 'third_party/google/Google_Client.php';
// include_once APPPATH . 'third_party/google/contrib/Google_Oauth2Service.php';

class Front extends CI_Controller {

    var $site_config;   //網頁一般設定
    var $login_user;    //登入使用者
    var $login_faild = 0; //記錄登入失敗次數
    var $controller_name = "front";
    var $return_uri;    //返回來源網址
    var $page;  //目前頁面
    var $fb_login_url;
    var $user_latlon;   //使用者目前位置

    public function __construct() {
        parent::__construct();

        //載入網頁前端model
        $this->load->model("front_model");

        //載入config
        $this->site_config = $this->Common_model->get_one('config');

        //登入使用者
        $this->login_user = $this->session->login_user;

        //識別語系代碼
        $this->get_locale_code();

        //載入語系檔
        $this->lang->load($this->controller_name, $this->session->i18n);

        //載入Library
        $this->load->library('user_agent');

        //取得來源網址
        $this->return_uri = $this->agent->referrer();

        //沒有來源uri，或是來源不在本站，則預設重導至本站首頁
        if (!$this->return_uri || strpos($this->return_uri, base_url()) === false) {
            $this->return_uri = base_url("/");
        }

        //取得使用者目前位置
        $this->get_userlocation();

        //如果session記錄了register_data，但已過久(5分鐘)，就清掉
        $register_data = $this->session->register_data;
        if (isset($register_data["timestamp"]) && $register_data["timestamp"] < time() - 300) {
            $this->session->unset_userdata("register_data");
        }

        //透過連結登入
        if ($this->input->get("user_id") && $this->input->get("user_token")) {

            $user = $this->login_by_url($this->input->get("user_id"), $this->input->get("user_token"));

            if ($user) {
                $this->login_user = $user;
                $this->session->set_userdata("login_user", $user);
            }
        }
    }

    /*
     * 主功能頁面
     */

    public function view($page = '', $func = '') {

        if ($page == '') {
            $page = $this->site_config->c_default_page;
            $func = $this->site_config->c_default_page_func;
        }

        if ($func != '') {
            if (file_exists(APPPATH . "/controllers/{$this->controller_name}/{$page}/{$func}.php")) {
                //載入特定功能函式
                include(APPPATH . "/controllers/{$this->controller_name}/{$page}/{$func}.php");
                exit();
            }
        }

        //檢查sp頁時間是否結束
        if (strtolower($page) == "sp") {
            $where_array = array(
                "tp_id" => $func,
                "tp_end_timestamp >=" => date("Y-m-d")
            );
        }

        $this->page = $page;

        $data["meta"]["title"] = $this->lang->line("site_name");
        $data["meta"]["sub_title"] = "";
        $data["meta"]["keyword"] = $this->site_config->c_meta_keyword;
        $data["meta"]["description"] = $this->site_config->c_meta_description;
        $data["meta"]["author"] = "";

        if (file_exists(APPPATH . "/controllers/{$this->controller_name}/_{$page}.php")) {
            //載入controller
            include(APPPATH . "/controllers/{$this->controller_name}/_{$page}.php");
        } elseif (!file_exists(APPPATH . "/views/{$this->controller_name}/{$page}.php")) {
            //controller和view都不存在，show 404
            $page = '404';
            $func = '404';
            include(APPPATH . "/controllers/{$this->controller_name}/{$page}/_{$func}.php");
            //show_404();
        }

        //取得FB登入url，必須在controller載入後取得
        if (!$this->login_user) {
            $this->fb_login_url = $this->get_fb_login_url();
        }

        if ($this->agent->is_mobile() && file_exists(APPPATH . "/views/{$this->controller_name}/mobile/{$page}.php")) {
            $this->load->view("{$this->controller_name}/mobile/{$page}", $data);
        } else {
            $this->load->view("{$this->controller_name}/{$page}", $data);
        }
    }

    //設定 login faild 次數
    private function set_login_faild_times($login_faild) {
        $this->session->set_userdata("login_faild", $login_faild);
    }

    private function get_login_faild_times() {
        return $this->session->userdata('login_faild');
    }

    //設定store id
    private function set_store_id($s_id) {
        $this->session->set_userdata("store_id", $s_id);
    }

    private function get_store_id() {
        return $this->session->userdata('store_id');
    }

    /*
     * store嵌入頁
     */

    public function embed($page = 'home', $func = '') {
        if ($func != '') {
            if (file_exists(APPPATH . "/controllers/{$this->controller_name}/embed/{$page}/{$func}.php")) {
                //載入特定功能函式
                include(APPPATH . "/controllers/{$this->controller_name}/embed/{$page}/{$func}.php");
                exit();
            }
        }

        $this->page = $page;

        $data["meta"]["title"] = $this->lang->line("site_name");
        $data["meta"]["sub_title"] = "";
        $data["meta"]["keyword"] = $this->site_config->c_meta_keyword;
        $data["meta"]["description"] = $this->site_config->c_meta_description;
        $data["meta"]["author"] = "";

        if (file_exists(APPPATH . "/controllers/{$this->controller_name}/embed/_{$page}.php")) {
            //載入controller
            include(APPPATH . "/controllers/{$this->controller_name}/embed/_{$page}.php");
        } elseif (!file_exists(APPPATH . "/views/{$this->controller_name}/embed/{$page}.php")) {
            //controller和view都不存在，show 404
            show_404();
        }

        //取得FB登入url，必須在controller載入後取得
        if (!$this->login_user) {
            $this->fb_login_url = $this->get_fb_login_url();
        }

        $this->load->view("{$this->controller_name}/embed/{$page}", $data);
    }

    /* ==================================
     * 取得語言碼(zh / en)
     * ================================== */

    private function get_locale_code() {
        //若有傳入url參數，更改成傳入的語系
        $lang = strtolower($this->input->get('lang'));

        if (in_array($lang, array('zh', 'en'))) {
            //語系寫入session
            $this->session->set_userdata(array('i18n' => $lang));

            return $lang;
        }

        //若session有記錄語系，使用session記錄語系
        $lang = $this->session->i18n;

        if (!$lang) {
            //偵測語系
            $http_accept_language = strtolower($this->input->server('HTTP_ACCEPT_LANGUAGE'));

            //初始值，最後回傳最小者
            $en_index = 9999;
            $zh_index = 9998;

            $tmp = strpos($http_accept_language, 'zh');
            if ($tmp !== false && $tmp < $zh_index) {
                $zh_index = $tmp;
            }

            $tmp = strpos($http_accept_language, 'en');
            if ($tmp !== false && $tmp < $en_index) {
                $en_index = $tmp;
            }

            //將語言id最小者回傳
            $min_index = 10000;
            if ($en_index < $min_index) {
                $min_index = $en_index;
                $lang = 'en';
            }
            if ($zh_index < $min_index) {
                $lang = 'zh';
            }

            //記錄session
            $this->session->set_userdata(array('i18n' => $lang));
        }

        return $lang;
    }

    /*
     * 密碼加密規則
     */

    private function encrypt_password($input) {
        return sha1($this->config->item('encrypt_token') . $input);
    }

    /*
     * 登入
     */

    private function login($account, $password) {
        $encryped_password = $this->encrypt_password($password);

        $where_array = array(
            "u_account" => $account,
            "u_password" => $encryped_password,
            "u_del" => "N"
        );

        $user = $this->Common_model->get_one("user", $where_array);

        if (!$user) {
            //確認是否為App端註冊、以手機為帳號的
            $where_array = array(
                "u_phone" => $account,
                "u_password" => $encryped_password,
                "u_del" => "N"
            );

            $user = $this->Common_model->get_one("user", $where_array);

            if (!$user) {
                return false;
            }
        }

        if ($user->u_email_confirmed != "Y") {
            $caption = $this->lang->line("Email認證尚未完成");
            $content = sprintf($this->lang->line("請開啟您註冊時填寫的Email信箱，點擊其中的認證連結，以啟用您的帳號。"), base_url("set_email") . "?u_id={$user->u_id}", base_url("set_email") . "?u_id={$user->u_id}", base_url());
            $this->alert($caption, $content);
            exit();
        }

        if ($user->u_enabled != 'Y') {
            return false;
        }

        //更新登入時間
        $update_array = array(
            'u_last_login_timestamp' => date('Y-m-d H:i:s')
        );

        $where_array = array(
            'u_id' => $user->u_id
        );

        $this->Common_model->update_db("user", $update_array, $where_array);

        return $user;
    }

    /*
     * 登入
     */

    private function fb_login($fb_id, $fb_token) {
        $where_array = array(
            "u_fbid" => $fb_id,
            "u_del" => "N"
        );

        $user = $this->Common_model->get_one("user", $where_array);

        if (!$user) {
            return false;
        }

        //更新登入時間
        $update_array = array(
            'u_fb_token' => $fb_token,
            'u_enabled' => 'Y',
            'u_last_login_timestamp' => date('Y-m-d H:i:s')
        );

        $where_array = array(
            'u_id' => $user->u_id
        );

        $this->Common_model->update_db("user", $update_array, $where_array);

        return $user;
    }

    private function get_userlocation() {
        //取得使用者目前位置
        $user_location = $this->input->cookie("user_location");
        if ($user_location) {
            $latlon = explode(', ', $user_location);
            $lat = $latlon[0];
            $lon = $latlon[1];
            $this->user_latlon = array(
                "lat" => $lat,
                "lon" => $lon
            );
        } else {
            $this->user_latlon = array(
                "lat" => 0,
                "lon" => 0
            );
        }
    }

    private function alert($caption = "", $content = "", $return_uri = "", $reload_opener = false, $returnbutton = true) {

        if ($return_uri == "" || $return_uri == "embed") {
            $return_uri = $this->return_uri;
            //如果來自alert頁面，則跳轉返回時重導至首頁
            if (strpos($return_uri, base_url("alert")) !== false) {
                $return_uri = base_url("/");
            }
        }

        $alert = array(
            "caption" => $caption,
            "content" => $content,
            "return_uri" => $return_uri,
            "return" => $returnbutton
        );

        $this->session->set_userdata("alert", $alert);

        $query = $reload_opener ? "?close_refresh_opener=1" : '';

        //如果來自store頁面
        $embed = (strpos($return_uri, "embed") == "") ? "" : "embed/";
        header("Location: " . base_url($embed . "alert") . $query);
        exit();
    }

    private function get_fb_login_url() {
        $fb_config = $this->config->item("facebook");

        $fb = new Facebook\Facebook([
            'app_id' => $fb_config["app_id"],
            'app_secret' => $fb_config["app_secret"],
            'default_graph_version' => 'v2.4',
        ]);

        $helper = $fb->getRedirectLoginHelper();

        $fb_callback_url = $this->config->item("server_base_url") . "fb-callback?return_uri=" . current_full_url();
        $loginUrl = $helper->getLoginUrl($fb_callback_url, $fb_config["permissions"]);

        return $loginUrl;
    }


    private function require_login() {
        if (!$this->login_user) {
            $caption = $this->lang->line("請先登入");
            $content = sprintf($this->lang->line("尚未登入或登入逾時，請重新登入。"));
            $redirect_uri = base_url("/");
            $this->alert($caption, $content, $redirect_uri);
            exit();
        }
    }


}
