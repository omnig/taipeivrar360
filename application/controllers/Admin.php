<?php

class Admin extends CI_Controller {

    var $site_config;   //網頁一般設定
    var $login_admin;    //登入使用者
    var $menu_list; //menu清單
    var $menu_allow_all;  //允許任何人存取的menu清單
    var $menu;  //實際有權限、可存取的menu
    var $menu_category; //目前的menu分類
    var $menu_category_list; //menu分類清單
    var $controller_name = "admin";
    var $theme = 'darkblue';    //css theme名稱
    var $owner_store;   //登入者擁有管理權的商店清單
    var $owner_s_id;    //登入者擁有的商店 s_id 清單

    public function __construct() {
        parent::__construct();

        //載入config
        $this->site_config = $this->Common_model->get_one('config');

        //登入管理員
        $this->login_admin = $this->session->login_admin;

        //menu清單
        $this->menu_list = $this->config->item("admin_menu_list");
        $this->menu_allow_all = $this->config->item("admin_menu_allow_all");
        $this->menu = $this->get_valid_menu($this->menu_list, isset($this->login_admin->adm_group) ? $this->login_admin->adm_group : '');   //記錄此帳號有權限可存取的menu
        $this->menu_category = $this->get_menu_category();


        // echo json_encode($this->login_admin->adm_group);
        // exit();
        //識別語系代碼
        $this->get_locale_code();

        //記錄管理員使用語系
        if ($this->login_admin && $this->session->i18n != $this->login_admin->adm_i18n) {
            $update_array = array(
                "adm_i18n" => $this->session->i18n
            );

            $where_array = array(
                "adm_id" => $this->login_admin->adm_id
            );

            $this->Common_model->update_db("admin", $update_array, $where_array);

            $this->login_admin->adm_i18n = $this->session->i18n;
        }

        //記錄管理員的合法主題
        if(!isset($this->login_admin->topic)){

            $topic_array = array();

            @$adm_id = adname_to_groupid($this->login_admin->adm_group);
            if (isset($adm_id)) {
                $this->login_admin->ag_id = @$adm_id;
                $where_array = array(
                    "t_organizer" => $adm_id
                );
                $topic_list = $this->Common_model->get_field("topic","t_name as name",$where_array);

                foreach ($topic_list as $key => $value) {
                    $topic_array[$key] = $value->name;
                }
                @$this->login_admin->topics  = $topic_array ; 
            }
            


            
        }

        //載入語系檔
        $this->lang->load($this->controller_name, $this->session->i18n);
        $this->lang->load("upload", $this->session->i18n);


        //google_oauth伺服器驗證JSON檔
        $this->key_location = dirname(__DIR__).'/OAuth/client_secret_811181325009-0mop4r7ohh8jskev30pq07riv45ic7cj.json';

        if (isset($_SESSION['access_token'])) {
            $this->check_g_accesstoken($_SESSION['access_token']);
        }
    }

    /*
     * 主功能頁面
     */

    public function view($page = 'home', $func = 'index') {

        $this->check_login_status($page);

        $data["meta"]["title"] = $this->site_config->c_site_name;
        $data["meta"]["author"] = "";

        if (file_exists(APPPATH . "/controllers/{$this->controller_name}/{$page}/_{$func}.php")) {
            //載入controller
            include(APPPATH . "/controllers/{$this->controller_name}/{$page}/_{$func}.php");
        } elseif (!file_exists(APPPATH . "/views/{$this->controller_name}/{$page}.php")) {
            //controller和view都不存在，show 404
            show_404();
        }

        $this->load->view("{$this->controller_name}/{$page}/{$func}", $data);
    }

    /*
     * 驗證具權限的menu
     */

    private function get_valid_menu($all_menu, $adm_group) {
        //取得已被授權的menu
        $where_array = array(
            'ag_group' => $adm_group
        );
        $admin_auth = $this->Common_model->get_db("admin_auth", $where_array);
        if($adm_group){
            $isEditor =  $this->session->login_admin->adm_is_editor === "Y";
        }



        //將有權限的menu整理成array
        $auth_menu = array();
        if ($admin_auth) {
            foreach ($admin_auth as $row) {
                $auth_menu[] = $row->aa_menu_category;
            }
        }



        $valid_menu = $all_menu;



        foreach ($valid_menu as $main_key => $main_menu) {

            if($adm_group && $main_key === 'admin' && $isEditor){
                unset($valid_menu[$main_key]);
                continue;
            }

            if (isset($main_menu["heading"])) {
                //標題列
                continue;
            }

            if (!isset($main_menu["sub_menu"])) {
                //沒有子menu，則此為menu選項
                if (!in_array($main_key, $auth_menu)) {
                    unset($valid_menu[$main_key]);
                }

                continue;
            }

            //有子menu，檢查子menu項目
            foreach ($main_menu["sub_menu"] as $sub_key => $sub_menu) {
                if (!in_array($sub_key, $auth_menu)) {
                    unset($valid_menu[$main_key]["sub_menu"][$sub_key]);
                }
            }

            //若子menu全部都被移除了，就連main menu一併刪除
            if (count($valid_menu[$main_key]["sub_menu"]) == 0) {
                unset($valid_menu[$main_key]);
            }
        }



        //排除沒有子項目的heading
        $last_heading_key = false;
        $this->menu_category_list = array();
        foreach ($valid_menu as $main_key => $main_menu) {
            if (!isset($main_menu["heading"])) {
                //非標題列
                $last_heading_key = false;
                continue;
            }

            // echo json_encode($valid_menu);
            // exit();

            $this->menu_category_list[$valid_menu[$main_key]["category"]] = true;

            //標題列
            if ($last_heading_key) {
                //有前一個標題，表示標題接標題，表示前一個標題是不需要的
                unset($this->menu_category_list[$valid_menu[$last_heading_key]["category"]]);
                unset($valid_menu[$last_heading_key]);
                $last_heading_key = false;
                continue;
            }

            $last_heading_key = $main_key;
        }
        // echo json_encode($this->menu_category_list);
        // exit();

        

        if ($last_heading_key) {
            //檢查結束後，仍有前一個標題，表示結尾是標題欄，要移除
            unset($this->menu_category_list[@$valid_menu[$last_heading_key]["category"]]);
            unset($valid_menu[$last_heading_key]);
        }

        return $valid_menu;
    }

    /* ==================================
     * 取得menu_category
     * ================================== */

    private function get_menu_category() {
        $first_menu_category = false;
        $current_menu_category = $this->session->current_menu_category;
        $input_menu_category = $this->input->get("menu_category");

        foreach ($this->menu as $row) {
            if (!isset($row["category"]) || !$row["category"]) {
                continue;
            }

            if (!$first_menu_category) {
                $first_menu_category = $row["category"];
            }

            //有傳入值，使用傳入值
            if ($row["category"] == $input_menu_category) {
                $this->session->current_menu_category = $input_menu_category;
                return $input_menu_category;
            }

            //無傳入值，使用原值
            if (!$input_menu_category && $row["category"] == $current_menu_category) {
                $this->session->current_menu_category = $current_menu_category;
                return $current_menu_category;
            }

            //無傳入值也無原值，回傳第1個
            if (!$current_menu_category && !$input_menu_category) {
                $this->session->current_menu_category = $row["category"];
                return $row["category"];
            }
        }

        return $first_menu_category;
    }

    /* ==================================
     * 取得語言碼(zh / en)
     * ================================== */

    private function get_locale_code() {
        //若有傳入url參數，更改成傳入的語系
        $lang = strtolower($this->input->get('lang'));

        if (in_array($lang, array('zh', 'en'))) {
            //語系寫入session
            $this->session->set_userdata(array('i18n' => $lang));

            return $lang;
        }

        //若session有記錄語系，使用session記錄語系
        $lang = $this->session->i18n;

        if (!$lang) {
            //偵測語系
            $http_accept_language = strtolower($this->input->server('HTTP_ACCEPT_LANGUAGE'));

            //初始值，最後回傳最小者
            $en_index = 9999;
            $zh_index = 9998;

            $tmp = strpos($http_accept_language, 'zh');
            if ($tmp !== false && $tmp < $zh_index) {
                $zh_index = $tmp;
            }

            $tmp = strpos($http_accept_language, 'en');
            if ($tmp !== false && $tmp < $en_index) {
                $en_index = $tmp;
            }

            //將語言id最小者回傳
            $min_index = 10000;
            if ($en_index < $min_index) {
                $min_index = $en_index;
                $lang = 'en';
            }
            if ($zh_index < $min_index) {
                $lang = 'zh';
            }

            //記錄session
            $this->session->set_userdata(array('i18n' => $lang));
        }

        return $lang;
    }

    /*
     * 取得base_url路徑
     */

    private function get_base_url($url) {
        return base_url("{$this->controller_name}/{$url}");
    }

    /*
     * 驗證登入狀態
     * 進入網頁先檢查項目
     */

    private function check_login_status($page) {

        //限制IP
        $ip = $_SERVER['HTTP_HOST'];
        //鴻圖測試站server
        $ip = explode(',', $ip);
        $ip = $ip[0];

        $client_ip = get_client_ip();
        $ip_array = explode(".", $client_ip);

        // echo json_encode($ip_array);
        // exit;

        // if (!($ip_array[0] == "::1" || $ip_array[0] == "192" || $ip_array[0] == "39" || $ip_array[0] == "60")) {
        //     _alert_redirect('很抱歉，您的位置無法登入系統，請洽系統管理員。\\n' . $ip, 'http://www.nmns.edu.tw/');
        // }

        // if (!(!isset($ip_array[1]) || $ip_array[1] == "168" || $ip_array[1] == "9" || $ip_array[1] == "172" || $ip_array[1] == "244")) {
        //     _alert_redirect('很抱歉，您的位置無法登入系統，請洽系統管理員。\\n' . $ip_array[1], 'http://www.nmns.edu.tw/');
        // }

        $allow_ip = array(
            'web_l4' => '163.29.37.138',
            'api_l4' => '163.29.37.132',
            'uat_l4' => '163.29.37.177',
            'server_uat_addr' => 'ar-test.gov.taipei',
            'server_web_addr' => 'ar-web.gov.taipei',
            'server_web_addr_temp' => 'ar-web-taipei.omniguider.com',
            'server_api_addr' => 'ar-api.gov.taipei',
            'server_api_addr_temp' => 'ar-api-taipei.omniguider.com',
            'test_server' => 'ar-taipei-test.omniguider.com',
        );
        
        if (!array_search($ip, $allow_ip)) {
            _alert_redirect('很抱歉，您的位置無法登入系統，請洽系統管理員。\\n' . $ip, 'https://login.gov.taipei/');
        }

        //不須登入的網頁
        if (in_array($page, array('login','login2', 'login_go', 'logout','goauth'))) {
            return;
        }

        if (!$this->login_admin) {
            header("Location: " . $this->get_base_url('login2'));
            exit();
        }

        return;
    }

    /*
     * 驗證網頁存取權限
     */

    private function menu_auth($menu_category) {

        //取得後台定義的許可清單
        if ($menu_category == '') {
            //未設定menu分類
            _alert($this->lang->line("存取被拒！"));
            exit();
        }

        //menu屬於全部允許的清單，則可以存取
        if (in_array($menu_category, $this->menu_allow_all)) {
            return true;
        }

        //未登入
        if (!isset($this->login_admin->adm_group)) {
            _alert($this->lang->line("存取被拒！"));
            exit();
        }

        //取得存取權限
        if(in_array($menu_category, ['ap_pois', 'ap_floors'])){
            $where_array = array(
                'ag_group' => $this->login_admin->adm_group,
                'aa_menu_category' => 'ap_building'
            );
        }else{
            $where_array = array(
                'ag_group' => $this->login_admin->adm_group,
                'aa_menu_category' => $menu_category
            );
        }

        $admin_auth = $this->Common_model->get_db("admin_auth", $where_array);

        //沒有存取權限，失敗
        if (!$admin_auth) {
            _alert($this->lang->line("存取被拒！"));
            exit();
        }

        return true;
    }

    /*
     * 密碼加密規則
     */

    private function encrypt_password($input) {
        return sha1($this->config->item('encrypt_token') . $input);
    }

    /*
     * 登入
     */

    private function login($account, $password, $menu_cateogry = '') {
        if ($menu_cateogry == 'sso') {
            $encryped_password = $password;
        } else {
            $encryped_password = $this->encrypt_password($password);
        }
        // $encryped_password = $this->encrypt_password($password);

        $where_array = array(
            "adm_account" => $account,
            "adm_password" => $encryped_password,
            "adm_enabled" => "Y"
        );

        $admin = $this->Common_model->get_one("admin", $where_array);

        if (!$admin) {
            return false;
        }

        //更新登入時間
        $update_array = array(
            'adm_last_login_timestamp' => date('Y-m-d H:i:s')
        );

        $where_array = array(
            'adm_id' => $admin->adm_id
        );

        $this->Common_model->update_db("admin", $update_array, $where_array);

        return $admin;
    }



    /*
     * Google登入
     */

    private function google_login($user_id,$email_account) {
        // $encryped_password = $this->encrypt_password($password);

        $where_array = array(
            "google_id" => $user_id,
            "adm_account" => $email_account,
            "adm_enabled" => "Y"
        );

        $admin = $this->Common_model->get_one("admin", $where_array);
        //如果id 與帳號 與權限 沒符合
        if (!$admin) {
        
            $where_array = array(
                "adm_account" => $email_account,
                "google_id" => $user_id
            );

            //檢查是否為新增使用者
            $admin_data = $this->Common_model->get_one("admin", $where_array);

            //有資料代表old使用者
            if($admin_data){
                echo "申請通過，請向管理員確認權限開通<br>Mail:".$where_array['adm_account'];
                exit();
                // return false;
            }else{

                $where_array = array(
                    "google_id" => $user_id,
                    "adm_account" => $email_account
                );

                $duplicated = $this->Common_model->get_one("admin", $where_array);
                if ($duplicated) {
                    echo "已申請過囉，請向管理員確認權限開通<br>Mail:".$where_array['adm_account'];
                    exit();
                }

                $insert_array = array(
                    "adm_name" => $email_account,
                    "google_id" => $user_id,
                    "adm_account" => $email_account,
                    "adm_email" => $email_account,
                    "adm_group" => "New",
                    "adm_enabled" => "N"
                );

                $this->Common_model->insert_db("admin", $insert_array);
                    echo "申請通過，請向管理員確認權限開通<br>Mail:".$where_array['adm_account'];
                    exit();
            }


            
        }


        //更新登入時間
        $update_array = array(
            'adm_last_login_timestamp' => date('Y-m-d H:i:s')
        );

        $where_array = array(
            'adm_id' => $admin->adm_id
        );

        $this->Common_model->update_db("admin", $update_array, $where_array);

        return $admin;
    }

    /*
     * 市府單一登入
     */

    private function taoyuan_login($user_id,$email_account) {
        $where_array = array(
            "google_id" => $user_id,
            "sa_account" => $email_account,
            "sa_enabled" => "Y"
        );

        $sa = $this->Common_model->get_one("sa", $where_array);
        //如果id 與帳號 與權限 沒符合
        if (!$sa) {
        
            $where_array = array(
                "sa_account" => $email_account,
                "google_id" => $user_id
            );

            //檢查是否為新增使用者
            $sa_data = $this->Common_model->get_one("sa", $where_array);

            //有資料代表old使用者
            if($sa_data){
                echo "申請通過，請向管理員確認權限開通<br>Mail:".$where_array['adm_account']; 
                exit();
                // return false;
            }else{

                $where_array = array(
                    "google_id" => $user_id,
                    "sa_account" => $email_account
                );

                $duplicated = $this->Common_model->get_one("sa", $where_array);
                if ($duplicated) {
                    echo "已申請過囉，請向管理員確認權限開通<br>Mail:".$where_array['adm_account'];
                    exit();
                }

                $insert_array = array(
                    "sa_name" => $email_account,
                    "google_id" => $user_id,
                    "sa_account" => $email_account,
                    "sa_group" => "New",
                    "sa_enabled" => "N",
                    "sa_password" => ""
                );

                $this->Common_model->insert_db("sa", $insert_array);
                    echo "申請通過，請向管理員確認權限開通<br>Mail:".$where_array['adm_account'];
                    exit();
            }


            
        }


        //更新登入時間
        $update_array = array(
            'sa_last_login_timestamp' => date('Y-m-d H:i:s')
        );

        $where_array = array(
            'sa_id' => $sa->sa_id
        );

        $this->Common_model->update_db("sa", $update_array, $where_array);

        return $sa;
    }
    

    private function google_checking(){
        // echo json_encode($_SESSION);
        // exit();
        
        if(isset($_SESSION['access_token'])){

            //Don`t need to do anything 
            if($_SESSION['access_token']['created']+3000<time()){
                unset($_SESSION['access_token']);
            }
        }else{
            $_SESSION['REDIRECT_QUERY_STRING'] = $_SERVER["REDIRECT_QUERY_STRING"];
            header("Location: " . $this->get_base_url('goauth'));
            exit();
                
        }

    }
    
    private function get_order_number($o_id) {
        return sprintf("{$this->config->item("order_number_prefix")}%08d", $o_id);
    }

    /**
     * Google_access_token 註冊
     * @param string $_GET['code']
     * @return array access token
     * @return ->['access_token'],['token_type'],['expires_in'],['created']
     */

    private function register_g_accesstoken($get_code){
        $redirect_url = $this->config->item('server_base_url').'admin/goauth/go';
        $client = new Google_Client();
        $service = new Google_Service_Oauth2($client);

        $client->setApplicationName("vrar360");
        $client->setAuthConfig($this->key_location);
        $client->setIncludeGrantedScopes(true);
        $client->setAccessType('offline');
        // $client->addScope(Google_Service_Plus::PLUS_ME);
        $httpClient = $client->authorize();
        $client->setScopes(array(
            'https://www.googleapis.com/auth/streetviewpublish',
            'https://www.googleapis.com/auth/youtube',
            'https://www.googleapis.com/auth/content',
            'https://www.googleapis.com/auth/userinfo.profile',
            'Google_Service_Oauth2::USERINFO_PROFILE',
            'Google_Service_Oauth2::USERINFO_EMAIL'
            ));
        
        $client->setRedirectUri($redirect_url);
        $token =  $client->authenticate($get_code);
        $_SESSION['access_token'] = $client->getAccessToken();
        // $client->setAccessToken($token );
        // echo json_encode($_SESSION);
        // exit();
        // $user = $service->userinfo->get(); 
        // $response = $httpClient->get('https://www.googleapis.com/plus/v1/people/me');
        // echo var_dump($response);
        // echo $_SESSION['access_token'];
        // echo json_encode($response);
        // exit();
        // echo json_encode($user);
        // exit();
        return $_SESSION['access_token'];
    }

    /**
     * Google_access_token 更新
     * @param string access token
     * @return array access token
     * @return ->['access_token'],['token_type'],['expires_in'],['created']
     */

    private function refresh_g_accesstoken($access_token){
        $redirect_url = $this->config->item('server_base_url').'admin/goauth/go';
        $client = new Google_Client();
        $client->setApplicationName("vrar360");
        $client->setAuthConfig($this->key_location);
        $client->setIncludeGrantedScopes(true);
        $client->setAccessType('offline');
        $client->setScopes(array(
            'https://www.googleapis.com/auth/plus.login',
            // 'https://www.googleapis.com/auth/streetviewpublish',
            'https://www.googleapis.com/auth/youtube',
            'email',
            'profile'
            ));
        $client->setRedirectUri($redirect_url);
        $response_data= $client->refreshToken($access_token);
        $_SESSION['access_token'] = $response_data['refresh_token'];

        return $_SESSION['access_token'];
    }

    /**
     * Google_access_token 取得
     * @param string access token
     * @return array access token
     * @return ->['access_token'],['token_type'],['expires_in'],['created']
     */

    private function get_g_accesstoken($access_token){
        $redirect_url = $this->config->item('server_base_url').'admin/goauth/go';
        $client = new Google_Client();
        $client->setApplicationName("vrar360");
        $client->setAuthConfig($this->key_location);
        $client->setIncludeGrantedScopes(true);
        $client->setAccessType('offline');
        $client->addScope("email");
        $client->addScope("profile");
        $client->setRedirectUri($redirect_url);
        $client->setAccessToken($access_token);
        $_SESSION['access_token'] = $client->getAccessToken();
        return $_SESSION['access_token'];
    }

    /**
     * Google_access_token 檢查
     * @param string access token
     * @return bool Returns True if the access_token is expired.
     */

    private function check_g_accesstoken($access_token){
        
        $response = curl_query("https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=".$access_token['access_token'],'');
        // exit();
        $response = json_decode($response,true);


        //超過session時間
        if(!isset($response['expires_in'])){
            //清空登入session
            $this->session->unset_userdata("login_admin");
            //清空登入變數
            $this->login_admin = null;

            unset($_SESSION['access_token']);
            //重導至登入頁
            _alert("超過登入時間，請重新登入");
            header("Location: " . $this->get_base_url('login'));
            exit();
        }

    
        $this->session->set_userdata(array('expires_in' => $response['expires_in']));

        if ($response['expires_in']<300){
            $this->refresh_g_accesstoken($access_token);
        }

        // $SESSION['access_token'] = 

        return $response;
    }


    /**
     * Google_access_token 丟棄
     * @param null
     * @return null
     */
    private function log_out_g_accesstoken(){
        unset($_SESSION['access_token']);
    }

    /**
     * Google_user_data 取得
     * @param array access token
     * @return response /user/me
     */
    private function get_plus_me($access_token){
        $redirect_url = $this->config->item('server_base_url').'admin/goauth/go';
        $client = new Google_Client();
        $client->setApplicationName("vrar360");
        $client->setAuthConfig($this->key_location);
        $client->setIncludeGrantedScopes(true);
        $client->setAccessType('offline');
        $client->addScope("email");
        $client->addScope("profile");
        $client->setRedirectUri($redirect_url);
        $client->setAccessToken($access_token);
        
        //Send Client Request
        $service = new Google_Service_Oauth2($client);
        $user = $service->userinfo->get(); 

        echo json_encode($user);
        exit();
    }

    /*
     * 員工愛上網權限登入
     */

    private function sso_login($userobj) {
        $encryped_password = $this->encrypt_password($userobj->userPrincipalName);

        $where_array = array(
            "adm_account" => $userobj->userPrincipalName,
            "adm_enabled" => "Y"
        );
        $admin = $this->Common_model->get_one("admin", $where_array);

        //檢查機關是否存在
        $orgName = str_replace('OU=', '', $userobj->orgName);
        $orgName = $orgName ? $orgName : $userobj->depName;
        $org_id = substr($userobj->orgID, 0, 10);
        $where_array = array();
        $where_like_array = array(
            'd_title' => $orgName,
            'd_org_id' => $org_id
        );
        $department = $this->Common_model->get_db('department', $where_array, '', '', 0, 0, $where_like_array);
        if (!$department) {
            //不存在，則新增
            $insert_array = array(
                'd_title' => $orgName,
                'd_org_id' => $org_id
            );
            $this->Common_model->insert_db('department', $insert_array);
            $d_id = $this->Common_model->get_insert_id();
        } else {
            $d_id = $department[0]->d_id;
            $orgName = $department[0]->d_title;
        }

        //檢查管理者是否存在
        if (!$admin) {
            //不存在，則自動新增
            $insert_array = array(
                'adm_account' => $userobj->userPrincipalName,
                'adm_password' => $encryped_password,
                'adm_name' => $userobj->givenName,
                'adm_email' => $userobj->userPrincipalName,
                'd_id' => $d_id,
                'adm_orgname' => $orgName,
                'adm_depname' => $userobj->depName,
                "adm_enabled" => 'N'
            );
            $this->Common_model->insert_db('admin', $insert_array);
            $adm_id = $this->Common_model->get_insert_id();

            $where_array = array(
                'adm_id' => $adm_id
            );
            $admin = $this->Common_model->get_one('admin', $where_array);

            return $admin;
        }

        // 更新department
        $department_db_data = $this->Common_model->get_one("department", array("d_id" => $d_id));
        if (!$department_db_data->d_org_id) {
            $update_array = array(
                "d_org_id" => $org_id
            );

            $this->Common_model->update_db("department", $update_array, array("d_id" => $d_id));
        }

        //更新登入時間
        $update_array = array(
            'd_id' => $d_id,
            'adm_last_login_timestamp' => date('Y-m-d H:i:s')
        );

        $where_array = array(
            'adm_id' => $admin->adm_id
        );
        $this->Common_model->update_db("admin", $update_array, $where_array);

        return $admin;
    }

    /*
     * 記錄管理者操作log
     */

    private function adm_log($table, $item_name, $act, $table_id, $act_title) {

        $insert_array = array(
            "al_ip" => get_remote_addr(),
            'adm_id' => $this->login_admin->adm_id ? $this->login_admin->adm_id : $table_id,
            'al_table' => $table,
            "al_table_id" => $table_id,
            "al_act_item" => $item_name,
            'al_act_title' => $act_title,
            'al_act' => $act
        );
        $this->Common_model->insert_db("adm_log", $insert_array);
    }

}
