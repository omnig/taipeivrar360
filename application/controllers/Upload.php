<?php

class Upload extends CI_Controller {

    public function __construct() {
        parent::__construct();
        // $this->output->set_header('Access-Control-Allow-Origin: *');
        //建立cache
        if ($this->config->item("cache_time") > 0) {
            $this->output->cache($this->config->item("cache_time"));
        }
    }

    /*
     * 讀取檔案
     */

    public function read($path = '', $filename = false) {
        $data["upload_file"] = $this->upload_lib->read("upload/{$path}/{$filename}");

        $this->load->view("upload/read", $data);
    }

}
