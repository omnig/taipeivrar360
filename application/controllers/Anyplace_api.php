<?php

class Anyplace_api extends CI_Controller {

    var $site_config;   //網頁一般設定
    var $controller_name = "anyplace_api";
    var $page;  //目前頁面
    var $bucket;
    public function __construct() {
        parent::__construct();


        $bucketName = "anyplace";

        // // Establish username and password for bucket-access
        $authenticator = new \Couchbase\PasswordAuthenticator();
        $authenticator->username('anyplace')->password('123456');

        // // Connect to Couchbase Server
        $cluster = new CouchbaseCluster("couchbase://192.168.56.1");
        // $cluster = new CouchbaseCluster("http://210.200.141.134:8091");
        // $cluster = new CouchbaseCluster('http://127.0.0.1:8091');
        // // Authenticate, then open bucket
        $cluster->authenticate($authenticator);
        $this->bucket = $cluster->openBucket($bucketName);

    }

    /* ===============================
     * 記錄log
     * 
     * 傳入參數：
     * $api_name: API(或存取目標)名稱
     * $result: 處理結果
     * $success: 是否成功(Y/N)
     * =============================== */

    private function log($api_name, $result, $success, $input = false) {
        if (!$input) {
            $input = array(
                "get" => $this->input->get(),
                "post" => $this->input->post()
            );
        }

        $insert_array = array(
            "la_ip" => get_remote_addr(),
            "la_api_name" => $api_name,
            "la_params" => json_encode($input),
            "la_result" => $result,
            "la_success" => $success
        );

        $this->Common_model->insert_db("log_locapi", $insert_array);

        if ($success == 'N') {
            //顯示呼叫結果
            $ret = array(
                "result" => "false",
                "error_message" => $result,
                "data" => array()
            );

            echo json_encode($ret);

            exit();
        }
    }

    /*
     * 主功能頁面
     */

    public function view($page = 'home', $func = '') {

        //載入語系檔
        // $this->lang->load("front", $this->session->i18n);

        if ($func != '') {
            if (file_exists(APPPATH . "/controllers/{$this->controller_name}/{$page}/{$func}.php")) {
                //載入特定功能函式
                include(APPPATH . "/controllers/{$this->controller_name}/{$page}/{$func}.php");
                exit();
            }
        }

        $this->page = $page;

        if (!file_exists(APPPATH . "/controllers/{$this->controller_name}/_{$page}.php")) {
            show_404();
        }
        include(APPPATH . "/controllers/{$this->controller_name}/_{$page}.php");
    }

    public function mapping($page = 'home', $func = '') {

        
        $this->page = $page;

        if (!file_exists(APPPATH . "/controllers/{$this->controller_name}/mapping/{$page}/_{$func}.php")) {
            show_404();
        }
        include(APPPATH . "/controllers/{$this->controller_name}/mapping/{$page}/_{$func}.php");
    }

    public function navigation($page = 'home', $func = '') {

        
        $this->page = $page;

        if (!file_exists(APPPATH . "/controllers/{$this->controller_name}/navigation/{$page}/_{$func}.php")) {
            show_404();
        }
        include(APPPATH . "/controllers/{$this->controller_name}/navigation/{$page}/_{$func}.php");
    }

    public function check_param($check_array = array(),$check_name = array()){

        foreach ($check_name as $key => $value) {

            if(!array_key_exists($value,$check_array)){

                $ret = array(
                    "error_messages"=>array("Missing or Invalid parameter:: [".$value."]"),
                    "status"=>"error",
                    "message"=>"Missing or Invalid parameter:: [".$value."]",
                    "status_code"=>400
                );

                echo json_encode($ret);
                exit;
            }
        }
    }

    public function verifyOwnerId($authToken =""){
        $ret = "111360824860124697116";
        switch ($authToken) {
            case 'ya29.GluNBNSt2Q7kA39wJl9EaLWthvTv_HNbam2nzHZ0SeLMjPr-_6BtYCxrDWHvg5tK88-o79PFwUSRwODeux4CLbD4eCKAfMxBrQWymDH4mYtSOl1ax9K1V7-v-2wu':
                $ret = "111360824860124697116"; 
                break;
            
            case 'a01.GluNBNSt2Q':
                $ret = "103753786025816092985"; 
                break;

            case 'a02.GluNBNSt2Q':
                $ret = "108571787535378763743"; 
                break;

            case 'a03.GluNBNSt2Q':
                $ret = "105982338244176461817"; 
                break;

            case 'a04.GluNBNSt2Q':
                $ret = "111549172365571754504"; 
                break;

            default:
                $ret = "111360824860124697116";
                break;
        }

        return $ret;
    }

    public function appendToOwnerId($ownerId =""){  
        return (string)$ownerId."_google";
    }

    public function requirePropertiesInJson($json_params_content=""){

        if($json_params_content==""||$json_params_content==NULL){

            $ret = array(
                "error_messages"=>array("requirePropertiesInJson"),
                "status"=>"error",
                "message"=>"requirePropertiesInJson",
                "status_code"=>400
            );

            echo json_encode($ret);
            exit;
        }
        
    }

}
