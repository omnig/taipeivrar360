<?php
//以使用者位置及方圓幾公里內拿AR導引點位資訊
header('Content-Type: application/json');
$input_array = array(
    "user_lat" => $this->input->GET('user_lat'),
    "user_lng" => $this->input->GET('user_lng')
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}
$input_array["radius"] = $this->input->GET('radius')?$this->input->GET('radius'):1;

//判斷主題沒有被關閉
//判斷主題沒有被刪除
$where_array = array(
	't_enabled' => 'Y',
	't_del' => 'N'
);

 
//找尋附近AR_主題
$ar_result = $this->Common_model->get_table_point_distance("tp_topic","t_name","t_lat","t_lng",$where_array,$input_array['user_lat'],$input_array['user_lng'],$input_array['radius']);

foreach ($ar_result as $key => $value) {
	$ar_result[$key]->t_lat = (double)$ar_result[$key]->t_lat;
	$ar_result[$key]->t_lng = (double)$ar_result[$key]->t_lng;
	@$ar_result[$key]->distance = (double)$ar_result[$key]->distance;
}

if (count($ar_result)==0) {
	
	http_response_code(204);
		$ret = array(
		    "result" => "false",
		    "code" => 204,
		    "error_message" => "No Data Here",
		    "data" => array()
		);
		echo json_encode($ret);
		exit();
}


//顯示AR結果
$ret = array(
    "result" => "true",
    "code" => 200,
    "error_message" => "",
    "data" => $ar_result 
);

echo json_encode($ret);
exit();