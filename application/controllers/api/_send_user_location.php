<?php

//傳入值接收
$input_array = array(
    // "timestamp" => $this->input->post("timestamp"),
    // "mac" => $this->input->post("mac"),
    "data" => $this->input->post("jsondata"),
    "device_id" => $this->input->post("device_id")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}


//驗證mac
// if (!$this->verify_mac($input_array["timestamp"], $input_array["mac"])) {
//     $this->log($page, "ACCESS_DENY", 'N');
// }

//檢查使用者是否存在
// if ($input_array["device_id"]) {
//     $where_array = array(
//         'u_device_id' => $input_array['device_id']
//     );
//     $user = $this->Common_model->get_one('user', $where_array);
//     if (!$user) {
//         $this->log($page, "INVALID_USER", 'N');
//     }
// }

//取得PLAN ID列表
$where_array = array(
    'af_enabled' => 'Y'
);
$floors = $this->Common_model->get_db('ap_floors', $where_array);


$insert_array = array();
$input_array["data"] = str_replace("&amp;quot;",'"',$input_array["data"]);
$data_array = json_decode($input_array["data"], true);

//若接收空字串，則回傳錯誤
if (count($data_array) == 0) {
    $this->log($page, "INVALID DATA:46 ".$input_array["data"], 'N');
}

$where_array = array();
$one_data_array = array();

foreach ($data_array as $row) {
    if (!isset($row["lat"]) || !isset($row["lng"]) || !isset($row["date"]) ) {
        $this->log($page, "ERROR DATA", 'N');
    }
    if (!$row["lat"] || !$row["lng"] || !$row["date"] ) {
        $this->log($page, "INVALID DATA:57", 'N');
    }

    if ($user) {
        $one_data_array = array(
            "ul_device_id" => $input_array["device_id"],
            "ul_lat" => $row["lat"],
            "ul_lng" => $row["lng"],
            "ul_timestamp" => $row["date"],
            'u_id' => $user->u_id
        );
        $insert_array[] = $one_data_array;
    } else {
        $one_data_array = array(
            "ul_device_id" => $input_array["device_id"],
            "ul_lat" => $row["lat"],
            "ul_lng" => $row["lng"],
            "ul_timestamp" => $row["date"]
        );
        $insert_array[] = $one_data_array;
    }
}

//批次寫入
$rows = $this->Common_model->batch_insert_db("user_location", $insert_array);

//最後位置寫入
$where_array = array(
    "ul_device_id" => $input_array["device_id"]
);
$row = $this->Common_model->get_one('user_last_location', $where_array);

if ($row) {
    $this->Common_model->update_db('user_last_location', $one_data_array, $where_array);
} else {
    $this->Common_model->insert_db('user_last_location', $one_data_array);
}

if ($rows == 0) {
    $result = "true";
    $error_msg = "";
    $msg = "NO DATA INSERTED";
} else if ($rows > 0) {
    $result = "true";
    $error_msg = "";
    $msg = $rows . " ROWS INSERTED";
} else {
    $result = "false";
    $error_msg = "INSERT FAILD";
    $msg = "";
}


$group_info = array();
$data = array();

//取得群組資料
$this->load->model("Group_model");

//取得device_id的所屬群組
$where_array = array(
    "gr.gr_device_id" => $input_array["device_id"],
    "g.g_end_timestamp >=" => date("Y-m-d H:i:s", time()),
    "g.g_enabled" => "Y",
    "g.g_del" => "N",
    "gr.gr_del" => "N"
);
$group_ids = $this->Common_model->get_db_join("group_relation as gr", $where_array, "group as g", "gr.g_id=g.g_id");

//取得群組所有成員
$info = array();
if ($group_ids) {
    $g_ids = "";
    foreach ($group_ids as $key => $row) {
        if ($key != 0) {
            $g_ids = $g_ids . ",";
        }
        $g_ids = $g_ids . $row->g_id;
    }

    $members = $this->Group_model->get_group_user_location($g_ids, $group_ids[0]->g_timestamp);


    //排序：0-admin->1-myself->2-other users...
    $idx = 2;
    foreach ($group_ids as $group) {
        foreach ($members as $row) {
            if ($group->g_id == $row->g_id) {
                $info = array(
                    "id" => (int) $group->g_id,
                    "key" => $group->g_key,
                    "title" => ($group->g_title) ? $group->g_title : "",
                    'remind_time' => $group->g_remind_timestamp ? $group->g_remind_timestamp : '',
                    'collection_time' => $group->g_collection_timestamp ? $group->g_collection_timestamp : '',
                    'p_type' => $group->p_type ? $group->p_type : '',
                    'p_id' => $group->p_id ? $group->p_id : '',
                    "end_timestamp" => $group->g_end_timestamp,
                );


                $info = array($info);
                if ($row->gr_role == '0') {
                    $data_idx = 0;
                } else if ($row->ul_device_id == $input_array['device_id']) {
                    $data_idx = 1;
                } else {
                    $data_idx = $idx;
                    $idx++;
                }
                $data[$data_idx] = array(
                    "id" => (int) $row->g_id,
                    "name" => $row->gr_name,
                    "tel" => $row->gr_tel ? $row->gr_tel : '',
                    "device_id" => $row->ul_device_id,
                    "role" => ($row->gr_role == "0") ? "admin" : "user",
                    "lat" => ($row->ul_lat) ? (float) $row->ul_lat : "",
                    "lng" => ($row->ul_lng) ? (float) $row->ul_lng : "",
                    "number" => (int) $row->af_number,
                    "update_timestamp" => ($row->ul_timestamp) ? $row->ul_timestamp : ""
                );
            }
        }
        ksort($data);
        $data = array_values($data);

        $group_info[] = array(
            "info" => $info,
            "members" => $data
        );
        $data = array();
        $idx = 2;
    }
}


//顯示呼叫結果
$ret = array(
    "result" => $result,
    "error_message" => $error_msg,
    "msg" => $msg,
    "data" => $group_info
);

echo json_encode($ret);

if (count($group_info) > 0) {
    $this->log($page, json_encode($ret), 'Y');
}

exit();
