<?php

//傳入值接收
$input_array = array(
    "timestamp" => $this->input->post("timestamp"),
    "mac" => $this->input->post("mac"),
    "key" => $this->input->post("key"),
    "u_device_id" => $this->input->post("user_device_id"),
    "adm_device_id" => $this->input->post("admin_device_id")
);


foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}


//驗證mac
if (!$this->verify_mac($input_array["timestamp"], $input_array["mac"])) {
    $this->log($page, "ACCESS_DENY", 'N');
}

$input_array["user_u_id"] = $this->input->post("user_u_id");
$input_array["adm_u_id"] = $this->input->post("admin_u_id");

//檢查key是否存在，以及是否有管理者權限
$where_array = array(
    "g_key" => $input_array["key"],
    "g_device_id" => $input_array["adm_device_id"]
);
$group = $this->Common_model->get_one("group", $where_array);

if (!$group) {
    $this->log($page, "INVALID KEY or PERMISSION DENIED", 'N');
    exit();
}

//紀錄刪除使用者
$update_array = array(
    "gr_del" => "Y"
);
$where_array = array(
    "g_id" => $group->g_id,
    "gr_device_id" => $input_array["u_device_id"]
);
$row = $this->Common_model->update_db("group_relation", $update_array, $where_array);

if ($row == 0) {
    $result = "false";
    $error_msg = "NO USER KICKED";
    $msg = "";
} else if ($row > 0) {
    $result = "true";
    $error_msg = "";
    $msg = "KICK SUCCESS";
} else {
    $result = "false";
    $error_msg = "KICK FAILD";
    $msg = "";
}

$data = array();

//顯示呼叫結果
$ret = array(
    "result" => $result,
    "error_message" => $error_msg,
    "msg" => $msg,
    "data" => $data
);

echo json_encode($ret);

$this->log($page, json_encode($ret), 'Y');

exit();
