<?php

//傳入值接收
$input_array = array(
    'rfid_keyout' => $this->input->GET('rfid_keyout'),
    'birthday' => $this->input->GET('birthday')
);

//必須傳入
foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER=>".$index, 'N');
    }
}

$result = $this->city_card_api($input_array["rfid_keyout"], $input_array["birthday"]);


if(!$result['result']){
    $this->log($page, "LOGIN_FAIL", 'N');
}else{
    $user = $this->citycard_login($input_array["rfid_keyout"], $input_array["birthday"], "");
}
 

//沒有取得登入使用者資料，登入失敗
if (!$user) {
    $this->log($page, "LOGIN_FAIL", 'N');
}

//準備回傳結果
$data = array(
    "u_id" => $user->u_id,
);

//顯示呼叫結果
$ret = array(
    "result" => "true",
    "error_message" => "",
    "data" => $data
);

$input_array['format'] = $this->input->post('format') ? strtolower($this->input->post('format')) : 'json';

if ($input_array['format'] == 'xml') {
    $xml = array_to_xml($ret, false);

    header('Content-Type: application/xml');
    echo $xml;
} else {
    echo json_encode($ret);
}

$this->log($page, json_encode($ret), 'Y');

exit();
