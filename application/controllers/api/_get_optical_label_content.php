<?php
header('Content-Type: application/json');
//以Image ID 拿到 AR Pattern 對應資料

$input_array = array(
    "identify_code" => $this->input->GET('identify_code')
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}


$this->readonly_db = $this->load->database('read_only', TRUE);
$this->readonly_db->select('ap.*, tdar.*');
$this->readonly_db->from('optical_label_poi_relation AS olpr');
$this->readonly_db->join('optical_label AS ol', 'olpr.ol_id=ol.ol_id and ol.ol_del=\'N\' and ol.ol_enabled=\'Y\'');
$this->readonly_db->join('ap_pois AS ap', 'olpr.ap_id=ap.ap_id and ap.ap_enabled=\'Y\'');
$this->readonly_db->join('department_ar AS tdar', 'ap.tdar_id=tdar.tdar_id and tdar.tdar_del=\'N\'');
$this->readonly_db->where('ol.ol_identify_number', $input_array["identify_code"]);
// $this->readonly_db->where('ol.ol_del', 'N');
// $this->readonly_db->where('ol.ol_enabled', 'Y');
$this->readonly_db->limit(1);
$query = $this->readonly_db->get();

if ($query) {
    $row = $query->row();
    $result = [
        "text" => $row->ap_ar_text,
        "url" => $row->ap_ar_url,
        "heigh" => $row->ap_ar_heigh,
        "content_type" => $row->tdar_content_type,
        // "content" => $this->config->item("server_base_url")."/upload/image/".$row->tdar_content_path,
        "cover_identify_image" => $row->ap_cover_identify_image,
        "size" => number_format((float)$row->ap_ar_vsize/100,2)
    ];
    switch ($row->tdar_content_type) {
        case 'text':
            $result["content"] = $row->tdar_content_path;
    
            //產生文字圖片
            $resulr_url = $this->config->item('server_base_url')."/api/get_text_image?text=".$row->tdar_content_path;
            //$resulr_url = curl_query($url);
            $result["url_image"] = $resulr_url;
    
            break;
        case 'image':
            $result["content"] = $this->config->item('server_base_url')."/upload/image/".$row->tdar_content_path;		
            break;
        case 'youtube':
            $result["content"] = $row->tdar_content_path;
            break;
        case '3d_model':
            $result["content"] = $this->config->item('server_base_url')."/upload_model/".$row->tdar_content_path;
            $result["animation_name"] = $row->tdar_animation_name;
            $result["ios"] = $this->config->item('server_base_url')."/upload_model/".$row->tdar_content_path;
            $result["android"] = $this->config->item('server_base_url')."/upload_model/".$row->tdar_content_path;
    
            break;
        case 'video':
            $result["content"] = $this->config->item('server_base_url')."/upload_video/".$row->tdar_content_path;
            $result["isTransparent"] = $row->tdar_is_transparent;
            break;
    }
} else {
    $result = [];
}

$ret = array(
    "result" => "true",
    "code" => 200,
    "error_message" => "",
    "data" => (object)$result
);
$this->log($page, json_encode($ret), 'Y');
echo json_encode($ret);
exit();