<?php

//傳入值接收
$input_array = array(
    "timestamp" => $this->input->post("timestamp"),
    "mac" => $this->input->post("mac"),
    'b_mac' => $this->input->post("beacon_mac"),
    "voltage" => $this->input->post("voltage")
);


foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}

//驗證mac
if (!$this->verify_mac($input_array["timestamp"], $input_array["mac"])) {
    $this->log($page, "ACCESS_DENY", 'N');
}


//取得beacon資料
$mac = str_replace(":", "", $input_array["b_mac"]);
$where_array = array(
    "b_mac like '%$mac%'"
);
$beacon = $this->Common_model->get_one("beacon", $where_array);

if (!$beacon) {
    $this->log($page, "INVALID_BEACON", 'N');
    exit();
}


//更新beacon電壓&次數
$where_array = array(
    "b_id" => $beacon->b_id
);
$new_voltage_count = (int) $this->Common_model->get_one_field("beacon", "b_voltage_count", $where_array) + 1;

$update_array = array(
    "b_voltage" => $input_array["voltage"],
    'b_voltage_count' => $new_voltage_count,
    "b_voltage_timestamp" => date("Y-m-d H:i:s")
);
$this->Common_model->update_db("beacon", $update_array, $where_array);


//顯示呼叫結果
$ret = array(
    "result" => "true",
    "error_message" => ""
);

echo json_encode($ret);

$this->log($page, json_encode($ret), 'Y');

exit();
