<?php
header('Content-Type: application/json');
//得到Slider頁面
//載入Model
$this->load->model("Home_slide_model");


$where_array= array(
	'hs_state' => 'Y'
);

$slider_list = $this->Home_slide_model->get_list('','',25,0,$where_array);
// echo json_encode($slider_list);
// exit;
$result = array();

foreach ($slider_list as $key => $value) {
	$result[$key]['image'] =  $this->config->item('server_base_url')."upload/home_slide/".$value->hs_path;
	$result[$key]['url'] =  $value->hs_link;
	$result[$key]['order'] = $value->hs_order;
}




//顯示所有結果
$ret = array(
    "result" => "true",
    "code" => 200,
    "error_message" => "",
    "data" => $result 
);

echo json_encode($ret);
exit();


