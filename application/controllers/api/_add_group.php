<?php

//傳入值接收
$input_array = array(
    // "timestamp" => $this->input->post("timestamp"),
    // "mac" => $this->input->post("mac"),
    "title" => $this->input->post("title"),
    "name" => $this->input->post("name"),
    "device_id" => $this->input->post("device_id"),
    "hour" => $this->input->post("hour")
);


foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}

//驗證mac
// if (!$this->verify_mac($input_array["timestamp"], $input_array["mac"])) {
//     $this->log($page, "ACCESS_DENY", 'N');
// }


$input_array["login_token"] = $this->input->post("login_token");
$input_array["p_type"] = $this->input->post("p_type") ? $this->input->post("p_type") : '';
$input_array["p_id"] = $this->input->post("p_id") ? $this->input->post("p_id") : '';
$input_array["remind_time"] = $this->input->post("remind_time") ? $this->input->post("remind_time") : '';
$input_array["collection_time"] = $this->input->post("collection_time") ? $this->input->post("collection_time") : '';
$input_array["tel"] = $this->input->post("tel") ? $this->input->post("tel") : '';
$timestamp = date("Y-m-d H:i:s", strtotime("+{$input_array["hour"]} hours"));


//檢查使用者是否存在
$user = '';
if ($input_array["login_token"]) {
    $where_array = array(
        'u_device_id' => $input_array['device_id']
    );
    $user = $this->Common_model->get_one('user', $where_array);
    if (!$user) {
        $this->log($page, "INVALID USER", 'N');
    }
}


//產生一組KEY
$key = sprintf("%04d", rand(0, 9999));

//查詢KEY是否存在
$where_array = array(
    "g_key" => $key,
    "g_timestamp >=" => $timestamp,
    "g_enabled" => "Y",
    "g_del" => "N"
);
$group = $this->Common_model->get_one("group", $where_array);

//若KEY仍啟用，則重新產製
while ($group) {
    $key = sprintf("%04d", rand(0, 9999));
    $where_array = array(
        "g_key" => $key,
        "g_timestamp >=" => $timestamp,
        "g_enabled" => "Y",
        "g_del" => "N"
    );
    $group = $this->Common_model->get_one("group", $where_array);
}

//檢查管理者開群組次數
$where_array = array(
    'gr_device_id' => $input_array["device_id"],
    'gr_role' => '0',
    "g_timestamp >=" => $timestamp
);
if ($user) {
    $where_array['gr.u_id'] = $user->u_id;
}
$count = count($this->Common_model->get_db_join('group_relation as gr', $where_array, 'group as g', 'gr.g_id=g.g_id'));
if ($count >= 10) {
    $this->log($page, "ADD_LIMIT", 'N');
}

//新增group
$insert_array = array(
    "g_title" => $input_array["title"],
    "g_key" => $key,
    "g_device_id" => $input_array["device_id"],
    "g_timestamp" => date("Y-m-d H:i:s", $input_array["timestamp"]),
    "g_end_timestamp" => $timestamp
);

if ($input_array["remind_time"]) {
    $insert_array['g_remind_timestamp'] = $input_array["remind_time"];
}
if ($input_array["collection_time"]) {
    $insert_array['g_collection_timestamp'] = $input_array["collection_time"];
}
if ($input_array["p_id"]) {
    $insert_array['p_type'] = $input_array["p_type"];
    $insert_array['p_id'] = $input_array["p_id"];
}

$this->Common_model->insert_db("group", $insert_array);
$g_id = $this->Common_model->get_insert_id();

//新增管理者
$insert_array = array(
    "g_id" => $g_id,
    "gr_device_id" => $input_array["device_id"],
    "gr_name" => $input_array["name"],
    "gr_role" => "0",
    "gr_timestamp" => date("Y-m-d H:i:s", $input_array["timestamp"])
);
if ($input_array['tel']) {
    $insert_array['gr_tel'] = $input_array['tel'];
}
if ($user) {
    $insert_array["u_id"] = $user->u_id;
}

$row = $this->Common_model->insert_db("group_relation", $insert_array);

if ($row > 0) {
    $result = 'true';
    $msg = 'INSERT SUCCESS';
} else {
    $result = 'false';
    $msg = 'INSERT FAILED';
}

$data = array(
    "key" => $key
);

//顯示呼叫結果
$ret = array(
    "result" => $result,
    "error_message" => "",
    "msg" => $msg,
    "data" => array($data)
);

echo json_encode($ret);


$this->log($page, json_encode($ret), 'Y');

exit();
