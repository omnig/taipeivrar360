<?php


$input_array = array(
    "dvp_id" => $this->input->GET('id')
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}

$input_array['device'] = $this->input->GET('device');

isset($input_array['device'])? $mobile = TRUE:FALSE;

$where_array = array(
	'dvp_id' => $input_array['dvp_id'],
	'dvp_state' => 'Y',
	'dvp_del' => 'N'
);

$vr_photo_list = $this->Common_model->get_db("department_vr_photo", $where_array,"dvp_create_timestamp","DESC",1);

if (!$vr_photo_list) {
	header("Location: ".$this->config->item('front_base_url'));
	exit();
}else{
	//有資料
	//瀏覽數加一，並轉址
	$update_array = array(
		'dvp_views' => $vr_photo_list[0]->dvp_views+1
	);


	$this->Common_model->update_db("department_vr_photo", $update_array, $where_array);



	
	if($mobile){
		//如果是環景且上傳到google street view
		if ($vr_photo_list[0]->dvp_backup!="N"&&$vr_photo_list[0]->photo_type=="pano") {

			$redirect_url = "http://vrar360.dev.utobonus.com/kr_spherical?image_path=";
			$redirect_url .=$this->config->item('server_base_url').$vr_photo_list[0]->dvp_backup;
			header("Location: " . $redirect_url);
			exit();
		}
		// 假如是長的圖片，用kr_Pano
		if ($vr_photo_list[0]->dvp_backup!="N"&&$vr_photo_list[0]->photo_type=="width") {

			$redirect_url = "http://vrar360.dev.utobonus.com/kr_pano?image_path=";
			$redirect_url .=$this->config->item('server_base_url').$vr_photo_list[0]->dvp_backup;
			header("Location: " . $redirect_url);
			exit();

		}


		//如果是連結
		if ($vr_photo_list[0]->dvp_backu=="N") {

			header("Location: " . $vr_photo_list[0]->dvp_view_link);
			exit();

		}


	}
	

	


	header("Location: " . $vr_photo_list[0]->dvp_view_link);
	exit();
}
	


// echo json_encode($vr_photo_list);
echo json_encode("none");
exit();


