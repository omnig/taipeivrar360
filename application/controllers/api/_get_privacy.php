<?php
header('Content-Type: application/json');


//token
$data["token"] = "privacy";

//讀取資料庫
$where_array = array(
    "si_token" => $data["token"]
);

$data = $this->Common_model->get_one("static_info", $where_array);


$response_data['name'] = $data->si_name;
// $response_data['content'] = ($data->si_content_zh);

// $data->si_content_zh = str_replace("</div>", "/n", $data->si_content_zh);
// $data->si_content_zh = str_replace("\t", "", $data->si_content_zh);
// $data->si_content_zh = str_replace("&nbsp;", "", $data->si_content_zh);
// $response_data['content'] = strip_tags($data->si_content_zh);
// $response_data['content']= str_replace("</div>", "/n", $data->si_content_zh);

$response_data['content'] = ($data->si_content_zh);

//顯示呼叫結果
$ret = array(
    "result" => "true",
    "code" => 200,
    "error_message" => "",
    "data" => $response_data
);

//沒有結果
if(count($response_data)<=0){
	http_response_code(400);
	$ret = array(
	    "result" => "false",
	    "code" => 400,
	    "error_message" => "None Data",
	    "data" => array()
	);
}

echo json_encode($ret);
exit();

