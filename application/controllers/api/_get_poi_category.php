<?php
header('Content-Type: application/json');

$result = $this->Common_model->get_db("ap_category", ["ac_enabled" => "Y"], "ac_order", 'asc');

$response_data = array();

foreach ($result as $result_key => $result_value) {
    $img = '';
    if (isset(json_decode($result_value->ac_image)->s100)) {
        $img = base_url("upload/ap_category") . "/" . json_decode($result_value->ac_image)->s100;
    }
	$response_data[$result_key]=array(
		"id" => $result_value->ac_id,
		"title_zh" => $result_value->ac_title_zh,
        "title_en" => $result_value->ac_title_en,
        "image" => $img
	);

}




//沒有結果
if(count($result)>0){
	//顯示呼叫結果
	$ret = array(
		"result" => "true",
		"code" => 200,
		"error_message" => "",
		"data" => $response_data
	);
}else{
	http_response_code(400);
	$ret = array(
	    "result" => "false",
	    "code" => 400,
	    "error_message" => "None Data",
	    "data" => array()
	);
}

echo json_encode($ret);
exit();

