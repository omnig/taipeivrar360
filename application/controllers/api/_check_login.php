<?php

//傳入值接收
$input_array = array(
    "timestamp" => $this->input->post("timestamp"),
    "mac" => $this->input->post("mac"),
    "device_id" => $this->input->post("device_id")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}

$input_array['login_token'] = $this->input->post("login_token");
$input_array['firebase_id'] = $this->input->post('reg_id');
$input_array['push'] = $this->input->post('push') ? strtoupper($this->input->post('push')) : 'Y';

//驗證mac
if (!$this->verify_mac($input_array["timestamp"], $input_array["mac"])) {
    $this->log($page, "ACCESS_DENY", 'N');
}

//取得使用者資料
$where_array = array(
    "u_device_id" => $input_array["device_id"]
);
$users = $this->Common_model->get_db("user", $where_array);


//產生登入token
$login_token = substr(md5(uniqid("utobonus", true)), 0, 40);


if (!$users) {
    //未註冊的user
    $login_token = '';
    $insert_array = array(
        'u_name' => 'UNREGISTERED',
        "u_login_token" => $login_token,
        'u_device_id' => $input_array["device_id"]
    );
    if ($input_array['firebase_id']) {
        $insert_array['u_firebase_id'] = $input_array['firebase_id'];
    }
    $this->Common_model->insert_db('user', $insert_array);
    $u_id = $this->Common_model->get_insert_id();
} else {
    $user = '';
    if ($input_array['login_token']) {
        //檢查login_token
        foreach ($users as $row) {
            if ($row->u_login_token == $input_array['login_token']) {
                $user = $row;
            }
        }
        if ($user) {
            //更新user資料
            $where_array = array(
                'u_id' => $user->u_id
            );
            $update_array = array(
                "u_login_token" => $login_token,
            );
            if ($input_array['firebase_id']) {
                $update_array['u_firebase_id'] = $input_array['firebase_id'];
            }
            $this->Common_model->update_db("user", $update_array, $where_array);
            $u_id = $user->u_id;
        }
    }

    if (!$user) {
        //未登入 或 login_token錯誤，清空login_token
        $login_token = '';
        $update_array = array(
            "u_login_token" => $login_token,
        );
        if ($input_array['firebase_id']) {
            $update_array['u_firebase_id'] = $input_array['firebase_id'];
        }
        $this->Common_model->update_db('user', $update_array, $where_array);
        $u_id = $users[0]->u_id;
        
        //login_token錯誤
        if ($input_array['login_token']) {
            $this->log($page, "INVALID_USER", 'N');
        }
    }
}

$where_array = array(
    'u_id' => $u_id
);
$user = $this->Common_model->get_one('user', $where_array);


//準備回傳結果
$data = array(
    "login_token" => $login_token,
    'reg_id' => $user ? ($user->u_firebase_id ? $user->u_firebase_id : '') : ''
);

//顯示呼叫結果
$ret = array(
    "result" => "true",
    "error_message" => "",
    "data" => array($data)
);

$input_array['format'] = $this->input->post('format') ? strtolower($this->input->post('format')) : 'json';

if ($input_array['format'] == 'xml') {
    $xml = array_to_xml($ret, false);

    header('Content-Type: application/xml');
    echo $xml;
} else {
    echo json_encode($ret);
}

$this->log($page, json_encode($ret), 'Y');

exit();
