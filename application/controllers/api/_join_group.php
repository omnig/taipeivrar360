<?php

//傳入值接收
$input_array = array(
    // "timestamp" => $this->input->post("timestamp"),
    // "mac" => $this->input->post("mac"),
    "name" => $this->input->post("name"),
    "key" => $this->input->post("key"),
    "device_id" => $this->input->post("device_id")
);


foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}


//驗證mac
// if (!$this->verify_mac($input_array["timestamp"], $input_array["mac"])) {
//     $this->log($page, "ACCESS_DENY", 'N');
// }

$input_array["login_token"] = $this->input->post("login_token");
$input_array["tel"] = $this->input->post("tel") ? $this->input->post("tel") : '';

//檢查使用者是否存在
if ($input_array["login_token"]) {
    $where_array = array(
        'u_login_token' => $input_array['login_token'],
        'u_device_id' => $input_array['device_id']
    );
    $user = $this->Common_model->get_one('user', $where_array);
    if (!$user) {
        $this->log($page, "INVALID USER", 'N');
    }
}

//查詢group是否存在
$where_array = array(
    "g_key" => $input_array["key"],
    "g_end_timestamp >=" => date("Y-m-d H:i:s",time()),
    "g_enabled" => 'Y',
    "g_del" => 'N'
);
$group = $this->Common_model->get_one("group", $where_array);

if (!$group) {
    $this->log($page, "INVALID GROUP", 'N');
    exit();
}

//檢查是否重覆加入
$where_array = array(
    "g_id" => (int) $group->g_id,
    "gr_device_id" => $input_array["device_id"],
    'gr_del' => 'N'
);
$member = $this->Common_model->get_one("group_relation", $where_array);

if ($member) {
    $this->log($page, "DUPLICATE DEVICE_ID", 'N');
    exit();
}

//檢查名稱是否重覆
if ($input_array["name"]) {
    $where_array = array(
        "g_id" => (int) $group->g_id,
        "gr_name" => $input_array["name"],
        'gr_del' => 'N'
    );
    $member = $this->Common_model->get_one("group_relation", $where_array);

    if ($member) {
        $this->log($page, "DUPLICATE NAME", 'N');
        exit();
    }
}


//新增member
$insert_array = array(
    "g_id" => $group->g_id,
    "gr_device_id" => $input_array["device_id"],
    "gr_name" => $input_array["name"],
    "gr_role" => "1",
    "gr_timestamp" => date("Y-m-d H:i:s", $input_array["timestamp"]),
);
if ($input_array['tel']) {
    $insert_array['gr_tel'] = $input_array['tel'];
}
if ($user) {
    $insert_array["u_id"] = $user->u_id;
}
$row = $this->Common_model->insert_db("group_relation", $insert_array);

if ($row > 0) {
    $result = 'true';
    $msg = 'INSERT SUCCESS';
} else {
    $result = 'false';
    $msg = 'INSERT FAILED';
}

//顯示呼叫結果
$ret = array(
    "result" => $result,
    "error_message" => "",
    "message" => $msg
);

echo json_encode($ret);

$this->log($page, json_encode($ret), 'Y');

exit();
