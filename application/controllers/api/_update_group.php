<?php

//傳入值接收
$input_array = array(
    // "timestamp" => $this->input->post("timestamp"),
    // "mac" => $this->input->post("mac"),
    "key" => $this->input->post("key"),
    "enabled" => $this->input->post("enabled"),
    "device_id" => $this->input->post("device_id")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}


//驗證mac
// if (!$this->verify_mac($input_array["timestamp"], $input_array["mac"])) {
//     $this->log($page, "ACCESS_DENY", 'N');
// }

$input_array["login_token"] = $this->input->post("login_token");
$input_array["name"] = $this->input->post("name");
$input_array["tel"] = $this->input->post("tel");
//限管理者權限更新
$input_array["title"] = $this->input->post("title");
$input_array["p_type"] = $this->input->post("p_type") ? $this->input->post("p_type") : '';
$input_array["p_id"] = $this->input->post("p_id") ? $this->input->post("p_id") : '';
$input_array["remind_time"] = $this->input->post("remind_time") ? $this->input->post("remind_time") : '';
$input_array["collection_time"] = $this->input->post("collection_time") ? $this->input->post("collection_time") : '';
$input_array["del"] = $this->input->post("del");
$input_array["hour"] = $this->input->post("hour");
$timestamp = date("Y-m-d H:i:s", strtotime("+{$input_array["hour"]} hours"));


//檢查使用者是否存在
if ($input_array["login_token"]) {
    $where_array = array(
        'u_device_id' => $input_array['device_id']
    );
    $user = $this->Common_model->get_one('user', $where_array);
    if (!$user) {
        $this->log($page, "INVALID_USER", 'N');
    }
}

//檢查key是否存在，以及是否有權限
$where_array = array(
    "g_key" => $input_array["key"],
    "gr_device_id" => $input_array["device_id"]
);
$group = $this->Common_model->get_db_join("group as g", $where_array, 'group_relation as gr', 'g.g_id=gr.g_id', 'inner');

if (!$group) {
    $this->log($page, "INVALID KEY or PERMISSION DENIED", 'N');
}
$group = $group[0];
//die(json_encode($group));
if ($group->g_del == "Y") {
    $this->log($page, "ALREADY DELETED", 'N');
} else if ($group->g_del == 'C') {
    $this->log($page, "INVALID KEY", 'N');
}


//更新使用者資訊
$update_array = array();
if ($input_array["name"]) {
    $update_array['gr_name'] = $input_array["name"];
}
if ($input_array["tel"]) {
    $update_array['gr_tel'] = $input_array["tel"];
}
if (count($update_array)) {
    $where_array = array(
        "g_id" => $group->g_id,
        "gr_device_id" => $input_array["device_id"]
    );
    $rows = $this->Common_model->update_db('group_relation', $update_array, $where_array);
}

//更新group狀態（限管理者權限）
$update_array = array();
if ($group->gr_role == '0') {
    $update_array["g_enabled"] = (strtoupper($input_array["enabled"]) == "N") ? "N" : "Y";

    if ($input_array["title"]) {
        $update_array["g_title"] = $input_array["title"];
    }
    if ($input_array["remind_time"]) {
        $update_array['g_remind_timestamp'] = (strtolower($input_array["remind_time"]) != 'false' ) ? $input_array["remind_time"] : '';
    }
    if ($input_array["collection_time"]) {
        $update_array["g_collection_timestamp"] = (strtolower($input_array["collection_time"]) != 'false') ? $input_array["collection_time"] : '';
    }
    if ($input_array["p_id"]) {
        $update_array["p_type"] = (strtolower($input_array["p_type"]) != 'false') ? $input_array["p_type"] : '';
        $update_array["p_id"] = (strtolower($input_array["p_id"]) != 'false') ? $input_array["p_id"] : '';
    }

    if ($input_array["hour"]) {
        $update_array["g_end_timestamp"] = $timestamp;
    }
    if ($input_array["del"]) {
        $update_array["g_del"] = "Y";
    }
    $where_array = array(
        "g_id" => $group->g_id
    );
    $rows = $rows + $this->Common_model->update_db("group", $update_array, $where_array);
}


//顯示呼叫結果
$ret = array(
    "result" => "true",
    "error_message" => 'UPDATE SUCCESS',
    "msg" => ''
);

echo json_encode($ret);


$this->log($page, json_encode($ret), 'Y');

exit();
