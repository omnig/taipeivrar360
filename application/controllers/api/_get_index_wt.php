<?php
header('Content-Type: application/json');
//以使用者位置及方圓幾公里內拿AR導引點位資訊
//載入Model
$this->load->model("Index_map_model");


$input_array = array(
    "type" => $this->input->GET('type'),
);

$input_array["lat"] = $this->input->GET('lat')?$this->input->GET('lat'):25.005249;
$input_array["lon"] = $this->input->GET('lon')?$this->input->GET('lon'):121.048247;
$input_array["nrPois"] = $this->input->GET('nrPois')?$this->input->GET('nrPois'):30;
$input_array["poi_id"] = $this->input->GET('poi_id')?$this->input->GET('poi_id'):"";

switch ($input_array['type']) {
	case 'topic':
		$result = get_topic($input_array["lat"],$input_array["lon"],$input_array["nrPois"],$input_array["poi_id"]);
		break;

	case 'ar':
		$result = get_ar($input_array["lat"],$input_array["lon"],$input_array["nrPois"],$input_array["poi_id"]);
		break;

	case 'poi':
		$result = get_poi($input_array["lat"],$input_array["lon"],$input_array["nrPois"],$input_array["poi_id"]);
		break;

	case 'view':
		$result = get_view($input_array["lat"],$input_array["lon"],$input_array["nrPois"],$input_array["poi_id"]);
		break;
	
	default:
		$result = get_topic($input_array["lat"],$input_array["lon"],$input_array["nrPois"],$input_array["poi_id"]);
		break;
}


//顯示所有結果
// $ret = array(
//     "result" => "true",
//     "code" => 200,
//     "error_message" => "",
//     "data" => $result 
// );

echo json_encode($result);
exit();



function get_topic($lat,$lon,$nrPois,$poi_id){
	$CI = & get_instance();
	//找尋附近主題
	//判斷主題沒有被關閉
	//判斷主題沒有被刪除
	$where_array = array(
		't_enabled' => 'Y',
		't_del' => 'N'
	);
	$topic_result = $CI ->Index_map_model->get_topic_list_latlong("distance","",$nrPois,0,$where_array,false,$lat,$lon);

	// echo json_encode($topic_result);
	// exit;

	//做資料整理
	foreach ($topic_result as $key => $value) {
		@$topic[$key]->id = (string)$value->t_id;
		@$topic[$key]->longitude = (string)$value->t_lng;
		@$topic[$key]->latitude = (string)$value->t_lat;
		@$topic[$key]->description = $value->t_describe;
		@$topic[$key]->name = $value->t_name;
		@$topic[$key]->is_slected = $poi_id == (string)$value->t_id ?TRUE:FALSE;
	}

	//輸出結果
	$result = array();
	if(isset($topic)){
		$result = $topic;
	}
	return $result;
}

function get_view($lat,$lon,$nrPois,$poi_id){
	$CI = & get_instance();
	//找尋附近主題
	//判斷主題沒有被關閉
	//判斷主題沒有被刪除
	$where_array = array(
		'v_enabled' => 'Y',
		'v_del' => 'N'
	);
	$topic_result = $CI ->Index_map_model->get_view_list_latlong("distance","",$nrPois,0,$where_array,false,$lat,$lon);

	// echo json_encode($topic_result);
	// exit;

	//做資料整理
	foreach ($topic_result as $key => $value) {
		@$topic[$key]->id = (string)$value->v_id;
		@$topic[$key]->name = $value->v_name;
		@$topic[$key]->description = $value->v_desc;
		@$topic[$key]->img = $value->v_image;
		@$topic[$key]->latitude = (double)$value->v_lat;
		@$topic[$key]->longitude = (double)$value->v_lng;
		@$topic[$key]->tel = $value->v_tel;
		@$topic[$key]->address = $value->v_address;
		@$topic[$key]->ticketinfo = $value->v_ticketinfo;
		@$topic[$key]->opentime = $value->v_opentime;
		@$topic[$key]->travellinginfo = $value->v_travellinginfo;
		@$topic[$key]->remarks = $value->v_remarks;
		@$topic[$key]->recommand = $value->v_recommand;
	}

	//輸出結果
	$result = array();
	if(isset($topic)){
		$result = $topic;
	}
	return $result;
}

function get_ar($lat,$lon,$nrPois,$poi_id){
	$CI = & get_instance();
	//找尋附近AR
	//判斷AR沒有被關閉
	//判斷主題沒有被刪除
	$where_array = array(
		'tdar_state' => 'Y',
		'tdar_del' => 'N',
		'tdar_to_og' => 'Y'
	);
	$ar_result = $CI->Index_map_model->get_department_ar_list_latlong("distance","",$nrPois,0,$where_array,false,$lat,$lon);

	// echo json_encode($ar_result);
	// exit;

	//做資料整理
	foreach ($ar_result as $key => $value) {

		@$ar[$key]->id = (string)$value->tdar_id;	
		@$ar[$key]->longitude = (string)$value->tdar_lng;	
		@$ar[$key]->latitude = (string)$value->tdar_lat;
		@$ar[$key]->description = $value->tdar_description;
		@$ar[$key]->name = $value->tdar_name;
		@$ar[$key]->is_slected = $poi_id == (string)$value->tdar_id ?TRUE:FALSE;
	}

	//輸出結果
	$result = array();
	if(isset($ar)){
		$result = $ar;
	}

	return $result;
}

function get_poi($lat,$lon,$nrPois,$poi_id){
	$CI = & get_instance();
	//判斷設施有沒有啟用
	//判斷設施不是門口
	$where_array = array(
		'ap_enabled' => 'Y',
		'ap_is_door' => 'N',
		'ac_title_en !=' => 'ar point'
	);

	//找尋附近設施
	$poi_result = $CI->Index_map_model->get_poi_list_latlong("distance","",$nrPois,0,$where_array,false,$lat,$lon);
	// echo json_encode($poi_result);
	// exit;
	//做資料整理
	foreach ($poi_result as $key => $value) {

		@$poi[$key]->id = (string)$value->ap_id;
		@$poi[$key]->longitude = (string)$value->ap_lng;
		@$poi[$key]->latitude = (string)$value->ap_lat;
		@$poi[$key]->description = "";
		@$poi[$key]->name = $value->ap_name;
		@$poi[$key]->is_slected = $poi_id == (string)$value->ap_id ?TRUE:FALSE;

	}

	//輸出結果
	$result = array();
	if(isset($poi)){
		$result = $poi;
	}
	return $result;
}