<?php


$input_array = array(
    "dvv_id" => $this->input->GET('id')
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}

$where_array = array(
	'dvv_id' => $input_array['dvv_id'],
	'dvv_state' => 'Y',
	'dvv_del' => 'N'
);

$vr_video_list = $this->Common_model->get_db("department_vr_video", $where_array,"dvv_create_timestamp","DESC",1);

if (!$vr_video_list) {
	header("Location: ".$this->config->item('front_base_url'));
	exit();
}else{
	//有資料
	//瀏覽數加一，並轉址
	$update_array = array(
		'dvv_views' => $vr_video_list[0]->dvv_views+1
	);
	$this->Common_model->update_db("department_vr_video", $update_array, $where_array);
	$link = $vr_video_list[0]->dvv_youtube_link;


	// if (strpos($link, 'http') === 0) {
	// 	header("Location: " . $vr_video_list[0]->dvv_youtube_link);
	// 	exit();
	// }

	if ($this->input->GET('device')=="m") {

        $mobile_link = str_replace("https://www.youtube.com/watch?v=", "", $vr_video_list[0]->dvv_youtube_link);
		header("Location: vnd.youtube:" . $mobile_link);
		exit();
	}else{
		// header("Location: https://www.youtube.com/watch?v=" . $vr_video_list[0]->dvv_youtube_link);
		// /embed
		$matches=array();
		preg_match('/(?:youtu\.be\/|youtube\.com(?:\/embed\/|\/v\/|\/watch\?v=|\/user\/\S+|\/ytscreeningroom\?v=))([\w\-]{10,12})\b/', $vr_video_list[0]->dvv_youtube_link, $matches , PREG_OFFSET_CAPTURE);
		if ($matches &&$matches[1]) {
			$vr_video_list[0]->dvv_youtube_link = $matches[1][0];
		}

		header("Location: https://www.youtube.com/embed/" . $vr_video_list[0]->dvv_youtube_link);
		exit();
	}

	
}
	


// echo json_encode($vr_photo_list);
echo json_encode("none");
exit();


