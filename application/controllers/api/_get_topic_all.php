<?php
header('Content-Type: application/json');

//局處列表
$where_array = array();

$department_list = $this->Common_model->get_db("tp_admin_group", $where_array);

foreach ($department_list as $key => $value) {
	$data["department_list"][$value->ag_id] = $value->ag_name;
}


//資料表名稱
$data["table_name"] = "topic_manage";

$model_name = ucfirst($data['table_name']) . '_model';
$this->load->model($model_name);


$order_by = 'ag_order ASC';

$input_array['limit'] = 0;

$input_array['skip'] = 0;

$where_array = array(
	'ag_enabled' => 'Y'
);

//從資料庫取得本次資料
$result = $this->$model_name->get_list($order_by, '', $input_array['limit'], $input_array['skip'], $where_array, false);
$response_data = array();

foreach ($result as $result_key => $result_value) {

	$result_value->t_organizer = $data["department_list"][$result_value->t_organizer];
	$result_value->t_co_organizer =  explode(',',$result_value->t_co_organizer);

	// echo json_encode($data["department_list"]);
	// exit;

	// echo count($result_value->t_co_organizer);
	// exit;
	
	foreach ($result_value->t_co_organizer as $co_key => $co_value) {
			

			$selectindex = $result_value->t_co_organizer[$co_key];

			if ($selectindex) {
				$result_value->t_co_organizer[$co_key] =  $data["department_list"][$selectindex];
			}else{
				$result_value->t_co_organizer = array();
			}
			

	}

	$response_data[$result_key]=array(
		"id" =>$result_value->t_id,
		"t_iframe_link" => $result_value->t_iframe_link,//上方Iframe
		"t_views" => $result_value->t_views, //瀏覽次數
		"t_ar_app_link" => "https://vr-manage.tycg.gov.tw/?app=".$result_value->t_id, //AR導覽
		"t_topic_link" => $result_value->t_topic_link, //前往主題活動官網頁
		"t_name" => $result_value->t_name, //主題名稱
		"t_organizer" => $result_value->t_organizer, //主辦局處
		"t_co_organizer"=> $result_value->t_co_organizer, //協辦局處
		"t_describe" => $result_value->t_describe, //主辦說明
		"t_lat" =>$result_value->t_lat,
		"t_lng" =>$result_value->t_lng,
	);
}


//顯示呼叫結果
$ret = array(
    "result" => "true",
    "code" => 200,
    "error_message" => "",
    "data" => $response_data
);

echo json_encode($ret);
exit();

