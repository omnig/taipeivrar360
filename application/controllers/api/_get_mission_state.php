<?php
header('Content-Type: application/json');
/*
-新增記錄使用者完第一關關卡資訊
-取得目前使用者，在遊戲第幾關
-並回傳導引路徑
--------------------------
example API : 
http://nmp.utobonus.com/api/get_mission_state?m_id=4&u_id=3&lat=25.080636526318713&lng=121.56444892287254

輸入參數
key1 "m_id" ->  任務ID
value1 (int) 

key2 "u_id" ->  使用者ID
value2 (int) 

key3 "lat" ->  使用者目前精度
value3 (string)   25.080636526318713

key4 "lng" ->  使用者目前緯度
value4 (string)   121.56444892287254

key5 "f" ->  使用者目前樓層  (若沒有設定則預設為1樓)
value5 (int)   2


回傳內容

導至POI 路徑

*/

//傳入值接收
$input_array = array(
  "m_id" => $this->input->get('m_id'),
  "u_id" => $this->input->get('u_id'),
  "lat" => $this->input->get("lat"),
  "lng" => $this->input->get("lng")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}

//使用者所在樓層
$floor_number = ($this->input->get("f")) ? $this->input->get("f") : '1';
$input_array["grid"] = $this->input->get('grid');
//查驗使用者經緯度
if(isset($input_array["lat"])):
  if (!is_numeric($input_array["lat"]) || !is_numeric($input_array["lng"])) {
      $this->log($page, "INVALID LOCATION", 'N');
      $ret = array(
          "result" => "false",
          "error_message" => "INVALID LOCATION"
      );
      echo json_encode($ret);
      exit();
  }
endif;


//取得任務列表資訊
$where_array = array(
    "m_id" => $input_array['m_id'],
    "p_id >" => "0"
);
$mission = $this->Common_model->get_db("nine_grid", $where_array);
$max_mission = count($mission);


//彙整使用者資訊
$where_array =array(
  'm_id' => $input_array["m_id"],
  'u_id' => $input_array["u_id"]
);
$this->load->model("Mission_model");
$user = $this->Mission_model->get_mission_user("", "", 0, 0, $where_array);

if (count($user)==0) {
  //代表沒有資訊-要新增
  $insert_array = array(
      "u_id" => $input_array["u_id"],
      "m_id" => $input_array["m_id"],
      "m_id_max" => $max_mission,
      "grid_now" => 0,
      "Is_finish" => "None",
      "rws_enabled" => "None"
  );

  $row = $this->Common_model->insert_db("mission_user", $insert_array);
  //取得POI
  $poi = $mission[0]->p_id;
  $result =  return_route_xt($poi,$floor_number,$input_array["lat"],$input_array["lng"],$page);
}else{
  //代表有資訊-要回傳現在關卡位置
  //現在關卡位置
  $now_grid = $user[0]->grid_now;
  //取得POI
  $poi = $mission[$now_grid]->p_id;
  $result =  return_route_xt($poi,$floor_number,$input_array["lat"],$input_array["lng"],$page);
}




//顯示呼叫結果
$ret = array(
    "result" => "true",
    "error_message" => "",
    "data" => $result
);

$input_array['format'] = $this->input->post('format') ? strtolower($this->input->post('format')) : 'json';

if ($input_array['format'] == 'xml') {
    $xml = array_to_xml($ret, false);

    header('Content-Type: application/xml');
    echo $xml;
} else {
    echo json_encode($ret);
}

$this->log($page, '', 'Y');

exit();



function return_route_xt($p_id,$floor_number,$lat,$lng,$page){

  $ci = & get_instance();
  //從資料庫取得相關參數
  $where_array = array(
      "ap_id" => $p_id
  );
  $poi = $ci->Common_model->get_one("ap_pois", $where_array);
  if (!$poi) {
      // $ci->log($page, "INVALID VALUE", 'N');
      $ret = array(
          "result" => "false",
          "error_message" => "INVALID VALUE"
      );

      echo json_encode($ret);
      exit();
  }

  $puid = $poi->ap_puid;

  //查詢
  $query_info = array(
      "pois_to" => $puid,
      "floor_number" => $floor_number,
      "coordinates_lat" => $lat,
      "coordinates_lon" => $lng
  );

  //載入helper
  $ci->load->helper('anyplace_helper');
  //回傳POI資訊
  $result = get_navi_result('route_xy', $query_info);

  return $result;
}