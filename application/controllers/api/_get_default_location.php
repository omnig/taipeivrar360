<?php

header('Content-Type: application/json');


$site_config = $this->site_config;
$result['app_lat'] = (double)$site_config->c_app_lat;
$result['app_lng'] = (double)$site_config->c_app_lng;
$result['web_lat'] = (double)$site_config->c_web_lat;
$result['web_lng'] = (double)$site_config->c_web_lng;

//顯示所有結果
$ret = array(
    "result" => "true",
    "code" => 200,
    "error_message" => "",
    "data" => $result 
);

echo json_encode($ret);
exit();