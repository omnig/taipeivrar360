<?php

header('Content-Type: application/json');


$site_config = $this->site_config;
$result['index_description'] = $site_config->c_index_text;


//顯示所有結果
$ret = array(
    "result" => "true",
    "code" => 200,
    "error_message" => "",
    "data" => $result 
);

echo json_encode($ret);
exit();