<?php

//傳入值接收
$input_array = array(
    "timestamp" => $this->input->post("timestamp"),
    "mac" => $this->input->post("mac"),
    "ip" => $this->input->post("ip"),
    "user_agent" => $this->input->post("user_agent")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}

$input_array["account"] = $this->input->post("account");
$input_array["password"] = $this->input->post("password");
$input_array["fb_id"] = $this->input->post("fb_id");
$input_array["fb_token"] = $this->input->post("fb_token");
$input_array["device_id"] = $this->input->post("device_id");


//驗證mac
if (!$this->verify_mac($input_array["timestamp"], $input_array["mac"])) {
    $this->log($page, "ACCESS_DENY", 'N');
}

//必須傳入帳號密碼，或是fb_id
if (!($input_array["account"] && $input_array["password"]) && !($input_array["fb_id"] && $input_array["fb_token"])) {
    $this->log($page, "INVALID_PARAMETER", 'N');
}

if ($input_array["account"] && $input_array["password"]) {
    //驗證帳號密碼
    $user = $this->login($input_array["account"], $input_array["password"], $input_array["ip"], $input_array["user_agent"],$input_array["device_id"]);
} else {
    //驗證fb_id
    $user = $this->fb_login($input_array["fb_id"], $input_array["fb_token"], $input_array["ip"], $input_array["user_agent"],$input_array["device_id"]);
}

//沒有取得登入使用者資料，登入失敗
if (!$user) {
    $this->log($page, "LOGIN_FAIL", 'N');
}

//準備回傳結果
$data = array(
    "mbr_id" => $user->u_mbr_id,
    "user_id" => $user->u_account,
    "user_email" => $user->u_email,
    "login_token" => $user->u_login_token,
    "user_name" => $user->u_name,
    "bonus" => $user->u_bonus,
    "redeem_bonus" => $user->u_redeem_bonus,
    "history_bonus" => $user->u_history_bonus,
    "pref_json" => $user->u_pref_json,
    "last_login" => $user->u_last_login_timestamp,
    "ref_ip" => $user->u_ip,
    "ref_ua" => $user->u_user_agent,
    "fb_ts" => $user->u_fb_ts ? $user->u_fb_ts : '',
    "status" => "255",
    "fb_id" => $user->u_fbid,
    "phn" => $user->u_phone,
    "aptg_id" => $user->u_aptg_id,
    "aptg_json" => $user->u_aptg_json,
    "device_id" => $user->u_device_id
);

//顯示呼叫結果
$ret = array(
    "result" => "true",
    "error_message" => "",
    "data" => $data
);

echo json_encode($ret);

$this->log($page, json_encode($ret), 'Y');

exit();
