<?php
header('Content-Type: application/json');

$result = $this->Common_model->get_db("topic_category", ["tc_enabled" => "Y"], "tc_order", 'asc');

$response_data = array();

foreach ($result as $result_key => $result_value) {

	$response_data[$result_key]=array(
		"id" => $result_value->tc_id,
		"title" => $result_value->tc_title_zh
	);

}




//沒有結果
if(count($result)>0){
	//顯示呼叫結果
	$ret = array(
		"result" => "true",
		"code" => 200,
		"error_message" => "",
		"data" => $response_data
	);
}else{
	http_response_code(400);
	$ret = array(
	    "result" => "false",
	    "code" => 400,
	    "error_message" => "None Data",
	    "data" => array()
	);
}

echo json_encode($ret);
exit();

