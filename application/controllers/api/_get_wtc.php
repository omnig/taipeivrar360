<?php
header('Content-Type: application/json');
//WTC 列表
$where_array = array();

$wtc_result = $this->Common_model->get_db("upload_og_ar", $where_array);

$wtc_list = array();

foreach ($wtc_result as $key => $value) {
	$wtc_list[$key] = $this->config->item('server_base_url').$value->u_filename;
}

//顯示呼叫結果
$ret = array(
    "result" => "true",
    "code" => 200,
    "error_message" => "",
    "data" => array(
    	"nums" => count($wtc_list),
    	"wtc" => $wtc_list,
    )
);
$this->log($page, json_encode($ret), 'Y');
echo json_encode($ret);
exit();