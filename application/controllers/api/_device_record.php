<?php
date_default_timezone_set('Asia/Taipei');
header('Content-Type: application/json');

//傳入值接收
$input_array = [
    "io_type" => $this->input->post('type'), // 1: 進入點， 2: 結束點
    "io_device_id" => $this->input->post('device_id'),
    "io_pf" => $this->input->post('platform')
];

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}

$datetime = date("Y-m-d H:i:s");

if($input_array['io_type'] === '1'){

    $this->db->where('io_device_id', $input_array['io_device_id']);
    $this->db->where('io_pf', $input_array['io_pf']);
    $this->db->where('io_end_time is null');
    $this->db->set('io_end_time', $datetime);
    $this->db->update('tp_io_record');

    $uuid = $this->db->query('select uuid() as id')->row()->id;
    // 新增
    $insert = [
        'io_id' => $uuid,
        'io_start_time' => $datetime,
        'io_device_id' => $input_array['io_device_id'],
        'io_pf' => $input_array['io_pf'],
    ];
    $this->db->insert('tp_io_record', $insert);
}else{
    $this->db->select('*');
    $this->db->from('tp_io_record');

    $this->db->where('io_device_id', $input_array['io_device_id']);
    $this->db->where('io_pf', $input_array['io_pf']);
    
    $this->db->order_by('io_start_time', 'desc');
    $this->db->limit(1);
    $record = $this->db->get()->row();
    if(is_null($record)){
        $this->log($page, "SOMETHING WRONG", 'N');
    }
    
    $this->db->where('io_id', $record->io_id);
    $this->db->set('io_end_time', $datetime);
    $this->db->update('tp_io_record');
}

// 清除多餘的記錄
$this->db->where('io_start_time <= \''.date('Y-m-d 00:00:00', strtotime("-1 month")).'\'');
$this->db->delete('tp_io_record');

//顯示呼叫結果
$ret = array(
    "result" => "true",
    "error_message" => "",
    "data" => (object)[]
);

$this->log($page, json_encode($ret), 'Y');

echo json_encode($ret);

exit();