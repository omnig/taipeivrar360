<?php
header('Content-Type: application/json');


//傳入值接收
$input_array = array(
  "u_id" => $this->input->get('u_id'),
  "type" => $this->input->get('type'),
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}

$input_array["dep"] = $this->input->get('dep')!=""?$this->input->get('dep'):"%";


if($input_array["dep"]!=""){
  //取得任務列表
  $where_array = array(
    "m.dep_id" => $input_array["dep"],
    'm_enabled' => "Y",
    'm_start_time <' => date("Y-m-d H:i:s"),
    'm_end_time >' => date("Y-m-d H:i:s")
  );
}else{
  //取得任務列表
  $where_array = array(
    'm_enabled' => "Y",
    'm_start_time <' => date("Y-m-d H:i:s"),
    'm_end_time >' => date("Y-m-d H:i:s")
  );
}


$this->load->model("Mission_model");
$mission = $this->Mission_model->get_list("", "", 0, 0, $where_array);

$where_array =array(
  'u_id' => $input_array["u_id"]
);
$user = $this->Mission_model->get_mission_user("", "", 0, 0, $where_array);

//載入使用者與任務資訊
foreach ($mission as $key => $value) {
  if (count($user)>0) {
    foreach ($user as $user_key => $user_value) {
      if ($user_value->m_id==$value->m_id) {
        $mission[$key]->grid_now = $user_value->grid_now;
        $mission[$key]->Is_finish = $user_value->Is_finish=="Done"?TRUE:FALSE;
        $mission[$key]->rws_enabled = $user_value->rws_enabled;
        break;
      }else{
        $mission[$key]->grid_now = 0;
        $mission[$key]->Is_finish = FALSE;
        $mission[$key]->rws_enabled = "None";
      }
    }
  }else{
        $mission[$key]->grid_now = 0;
        $mission[$key]->Is_finish = FALSE;
        $mission[$key]->rws_enabled = "None";
  }
  
}

///這邊判斷回傳資訊
if($input_array["type"] == "mission"){
  foreach ($mission as $key => $value) {
    unset($mission[$key]->rw_id);
    unset($mission[$key]->rw_title);
    unset($mission[$key]->rw_img);
    unset($mission[$key]->rw_describe);
    unset($mission[$key]->rw_num);
    unset($mission[$key]->rw_remainng);
  }
}elseif($input_array["type"] == "reward"){
  foreach ($mission as $key => $value) {
    unset($mission[$key]->m_describe);
    unset($mission[$key]->m_start_time);
    unset($mission[$key]->m_end_time);
    unset($mission[$key]->m_enabled);
  }
}


foreach ($mission as $key => $value) {
  isset($value->m_img)?$value->m_img = $this->config->item('image_base_url').'upload/mission/'.$value->m_img:"";
  isset($value->rw_img)?$value->rw_img = $this->config->item('image_base_url').'upload/reward_group/'.$value->rw_img:"";
}

//顯示呼叫結果
$ret = array(
    "result" => "true",
    "error_message" => "",
    "data" => $mission
);


$input_array['format'] = $this->input->get('format') ? strtolower($this->input->get('format')) : 'json';

if ($input_array['format'] == 'xml') {
    $xml = array_to_xml($ret, false);

    header('Content-Type: application/xml');
    echo $xml;
} else {
    echo json_encode($ret);
}

$this->log($page, '', 'Y');
exit();
