<?php

header('Content-Type: application/json');
/*
-取得任務獎勵

*mission_user_需更新rws_enabled
*reward獎勵領取 需更新 減1
m_id
u_id

--rw_id
--rs_id

--------------------------
example API : 
http://nmp.utobonus.com/api/get_mission_reward?m_id=4&u_id=3
輸入參數
key1 "m_id" ->  任務ID
value1 (int) 

回傳內容

已領取狀態

成功-Success
失敗-Mission not complete or Is receive.

 */
//echo pureTest();
//die();
//傳入值接收
$input_array = array(
    "m_id" => $this->input->get('m_id'),
    "u_id" => $this->input->get('u_id'),
    "device_id" => $this->input->get('device_id')
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}
//echo json_encode($input_array);die();
//彙整使用者資訊
$where_array = array(
    'm_id' => $input_array["m_id"],
    'u_id' => $input_array["u_id"]
);
$this->load->model("Mission_model");
$user = $this->Mission_model->get_mission_user("", "", 0, 0, $where_array);


$mission_info = $this->Common_model->get_one("mission", array("m_id" => $input_array["m_id"]));
if (!isset($mission_info) || !is_numeric($input_array["m_id"])) {
    $ret = array(
        "result" => "false",
        "error_message" => "INVALID VALUE : No mission info."
    );

    echo json_encode($ret);
    exit();
}
$mission_reward_group_id = $mission_info->rw_id;

if (count($user) <= 0) {
    $ret = array(
        "result" => "false",
        "error_message" => "INVALID VALUE : No user info."
    );

    echo json_encode($ret);
    exit();
}

if ($user[0]->is_finish !== "Done" || $user[0]->rws_enabled !== "None") {
    $ret = array(
        "result" => "false",
        "error_message" => "INVALID VALUE : Mission has not complete or had already received."
    );

    echo json_encode($ret);
    exit();
}

//echo test($input_array["u_id"], $input_array["device_id"], $mission_reward_group_id);
//die();

$result = countdown_reward($input_array["u_id"], $input_array["device_id"], $mission_reward_group_id);

if ($result['result']) {
    //扣序號成功
    $result = "Success";
} else {
    $ret = array(
        "result" => "false",
        "error_message" => "INVALID VALUE : (" . $result['error_code'] . ")" . $result['reason']
    );

    echo json_encode($ret, JSON_UNESCAPED_UNICODE);
    exit();
}


//mission_user_需更新rws_enabled
$where_array = array(
    'm_id' => $input_array["m_id"],
    'u_id' => $input_array["u_id"]
);

$update_array = array(
    "rws_enabled" => "Done"
);

$this->Common_model->update_db("mission_user", $update_array, $where_array);


//顯示呼叫結果
$ret = array(
    "result" => "true",
    "error_message" => "",
    "data" => $result
);

echo json_encode($ret);

$this->log($page, '', 'Y');

exit();


//reward獎勵領取 需更新 減1
function countdown_reward($u_id, $device_id, $rw_id)
{
    $ci = &get_instance();
    try {
        $ci->Common_model->trans_start();
        //從reward 群組資料表更新最新獎勵剩餘數量
        $where_array = array(
            "rw_id" => $rw_id
        );

        $rw = $ci->Common_model->get_one_by_lock("reward_group", $where_array);
//        $rw = $ci->Common_model->get_one("reward_group", $where_array);
        if (
            (!is_null($rw->rw_end_time) && date_format(date_create($rw->rw_end_time), "Y-m-d") < date('Y-m-d')) ||
            (!is_null($rw->rw_start_time) && date_format(date_create($rw->rw_start_time), "Y-m-d") > date('Y-m-d'))
        ) {
            $ci->Common_model->trans_rollback();
            // 超過對獎時間
            return ["result" => false, "error_code" => '1', "reason" => "Not in reward period."];
        }

        if ($rw->rw_remainng - 1 < 0) {
            $ci->Common_model->trans_rollback();
            return ["result" => false, "error_code" => '2', "reason" => "Reward not enough."];
        }

        $rw_code = $rw->rw_code;
        if (is_null($rw_code)) {
            $ci->Common_model->trans_rollback();
            return ["result" => false, "error_code" => '-1', "reason" => "System Error."];
        }

        if (in_array(ENVIRONMENT, ['production', 'api', 'uat'])) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $ci->config->item("reward_url"));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER,
                        [
                            'Content-Type: application/json;charset=utf-8'
                        ]
            );
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            if (ENVIRONMENT === 'uat') {
                curl_setopt($ch, CURLOPT_PROXY, 'http://172.25.165.200:8080');
            }
            $body = new stdClass();
            $body->userId = $u_id;
            $body->serverToken = $ci->config->item("reward_server_token");
            $body->couponId = $rw_code;
            $timestamp = time();
            $dt = new DateTime("now", new DateTimeZone("Asia/Taipei")); //first argument "must" be a string
            $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
            $body->scheduleSendTime = $dt->format('Y-m-d h:i:s');
            $expired_start_date = is_null($rw->rw_expired_start_time) ? '' : date_format(
                date_create($rw->rw_expired_start_time),
                'Y-m-d'
            );
            $expired_end_date = is_null($rw->rw_expired_end_time) ? '' : date_format(
                date_create($rw->rw_expired_end_time),
                'Y-m-d'
            );
            $body->content = "您好，您已收到一張票券：{$rw->rw_title}，使用期限：{$expired_start_date} ~ {$expired_end_date}，詳細訊息請至票夾確認";

            $body->userReceiveCount = "1";

            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
            $result_string = curl_exec($ch);
            $result = json_decode($result_string);
            curl_close($ch);
            if (!isset($result->code) || $result->code !== 0) {
                $ci->db->trans_rollback();
                return ["result" => false, "error_code" => (string)$result->code, "reason" => $result->message];
            }
        }

        //領取獎勵
        //從reward資料表取出一筆，並更新
        $where_array = array(
            "rw_id" => $rw_id,
            "rs_enabled" => "N"
        );
        $rs = $ci->Common_model->get_one_by_lock("reward", $where_array);

        //被領取的序號
        $rs_id = $rs->rs_id;
        // $rs_serial = $rs->rs_serial;
        //更新
        $where_array = array(
            'rs_id' => $rs_id,
            "rs_enabled" => "N"
        );
        $update_array = array(
            "rs_enabled" => "Y",
            "rs_uid" => $u_id,
            "rs_device_id" => $device_id,
            "rs_timestamp" => date("Y-m-d H:i:s"),
        );
        $ci->Common_model->update_db("reward", $update_array, $where_array);

        //更新
        $where_array = array(
            'rw_id' => $rw_id
        );
        $update_array = array(
            "rw_remainng" => $rw->rw_remainng - 1
        );
        $ci->Common_model->update_db("reward_group", $update_array, $where_array);

        $ci->Common_model->trans_complete();

        if ($ci->db->trans_status() === false) {
            return ["result" => false, "error_code" => '', "reason" => 'query commit error'];
        }

        return ["result" => true, "error_code" => null, "reason" => null];
    } catch (throwable $error) {
        $ci->Common_model->trans_rollback();

        return ["result" => false, "error_code" => '', "reason" => $error];
    }
}

function lock($rw_id)
{
    $ci = &get_instance();

    while (true) {
        $rw_count = $ci->Common_model->update_db(
            "reward_group",
            [
                "rw_lock" => 1
            ],
            [
                "rw_id" => $rw_id,
                "rw_lock" => 0,
            ]
        );
        if ($rw_count !== 0) {
            break;
        }
        sleep(1);
    }
    return true;
}

function unlock($rw_id)
{
    $ci = &get_instance();

    $ci->Common_model->update_db("reward_group", ["rw_lock" => 0], ["rw_id" => $rw_id]);
    return true;
}

function test($u_id, $device_id, $rw_id)
{
    $ci = &get_instance();

    //從reward 群組資料表更新最新獎勵剩餘數量
    $where_array = array(
        "rw_id" => $rw_id
    );
    // $rw = $ci->Common_model->get_one("reward_group",$where_array);

    $rw = $ci->Common_model->get_one("reward_group", $where_array);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $ci->config->item("reward_url"));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER,
                [
                    'Content-Type: application/json;charset=utf-8'
                ]
    );
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    if (ENVIRONMENT === 'uat') {
        curl_setopt($ch, CURLOPT_PROXY, 'http://172.25.165.200:8080');
    }

    $body = new stdClass();
    $body->userId = $u_id;
    $body->serverToken = $ci->config->item("reward_server_token");
    $body->couponId = $rw->rw_code;
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone("UTC")); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    $body->scheduleSendTime = $dt->format('Y-m-d\TH:i:s\Z');
    $expired_start_date = is_null($rw->rw_expired_start_time) ? '' : date_format(
        date_create($rw->rw_expired_start_time),
        'Y-m-d'
    );
    $expired_end_date = is_null($rw->rw_expired_end_time) ? '' : date_format(
        date_create($rw->rw_expired_end_time),
        'Y-m-d'
    );
    $body->content = "您好，您已收到一張票券：{$rw->rw_title}，使用期限：{$expired_start_date} ~ {$expired_end_date}，詳細訊息請至票夾確認";

    $body->userReceiveCount = "1";

    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
    $result_string = curl_exec($ch);
    $result = json_decode($result_string);
    curl_close($ch);
    return $result_string;
}

function pureTest()
{
    $ci = &get_instance();
    echo ENVIRONMENT . '  ' . $ci->config->item("reward_url") . '  ';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $ci->config->item("reward_url"));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER,
                [
                    'Content-Type: application/json;charset=utf-8'
                ]
    );
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    if (ENVIRONMENT === 'uat') {
        curl_setopt($ch, CURLOPT_PROXY, 'http://172.25.165.200:8080');
    }

    $result_string = curl_exec($ch);
    $result = json_decode($result_string);
    curl_close($ch);
    return $result_string;
}
