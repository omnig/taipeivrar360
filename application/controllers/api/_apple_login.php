<?php

//傳入值接收
$input_array = array(
    "timestamp" => $this->input->post("timestamp"),
    "mac" => $this->input->post("mac"),
    "device_id" => $this->input->post("device_id"),
    'identity_token' => $this->input->post('identity_token'),
    'name' => $this->input->post('name')
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}

//驗證mac
if (!$this->verify_mac($input_array["timestamp"], $input_array["mac"])) {
    $this->log($page, "ACCESS_DENY", 'N');
}


$firebaseJWT = new \Firebase\JWT\JWT;
$firebaseJWK = new \Firebase\JWT\JWK;


//取得驗證參數
$iss = $this->config->item('apple')['iss'];
$team_id = $this->config->item('apple')['team_id'];
$client_id = $this->config->item('apple')['client_id'];
$key_id = $this->config->item('apple')['key_id'];
$privateKey = $this->config->item('apple')['private_key'];
$auth_keys_url = $this->config->item('apple')['auth_keys_url'];
$auth_token_url = $this->config->item('apple')['auth_token_url'];


$tks = explode('.', $input_array['identity_token']);
list($headb64, $bodyb64, $cryptob64) = $tks;


//取得Apple Auth Keys
$ret = curl_query($auth_keys_url, 'get');
$apple_public_keys = json_decode($ret, true);


if (!$apple_public_keys) {
  $this->log($page, "INVALID_PUBLIC_KEYS", 'N');
}

$keys = $firebaseJWK->parseKeySet($apple_public_keys);
$keys_array = array();
foreach ($keys as $kid => $key) {
    $keys_array[$kid] = openssl_pkey_get_details($key)['key'];
}

//檢查identity_token對應Apple Auth Key
$header = json_decode($firebaseJWT->urlsafeB64Decode($headb64));
$publicKey = $keys_array[$header->kid];
if (!$publicKey) {
    $this->log($page, "INVALID_AUTH_KEY", 'N');
}

try {
    $user_array = $firebaseJWT->decode($input_array['identity_token'], $publicKey, array('RS256'));
} catch (Exception $e) {
    $this->log($page, $e->getMessage(), 'N');
}

//檢查參數
if ($user_array->iss != $iss) {
    $this->log($page, "INVALID_ISS", 'N');
}
if ($user_array->aud != $client_id) {
    $this->log($page, "INVALID_CLIENT_ID", 'N');
}

/**暫時測試參數***/
// $user_array = array(
//   "iss" => "https://appleid.apple.com",
//   "aud" => "com.omniguider.AliShan20191212",
//   "exp" => 1601432457,
//   "iat" => 1601346057,
//   "sub" => "000966.5ac46a5e7d0f4e76930a2b5b3172f7a2.0738",
//   "c_hash" => "U8_aumdpv8uu9NxrU3yZIw",
//   "email" => "8tersj6uf8@privaterelay.appleid.com",
//   "email_verified" => "true",
//   "is_private_email" => "true",
//   "auth_time" => 1601346057,
//   "nonce_supported" => true
// );
// $user_array = (object)$user_array;

if ($user_array) {
    $user = $this->apple_login($user_array, $input_array['name'], $input_array["device_id"]);
}

//沒有取得登入使用者資料，登入失敗
if (!$user) {
    $this->log($page, "LOGIN_FAIL", 'N');
}


//準備回傳結果
$data = array(
    "u_id" => $user->u_id,
    "user_email" => $user->u_email,
    "login_token" => $user->u_login_token,
    "user_name" => $user->u_name,
    "last_login" => $user->u_last_login_timestamp ? $user->u_last_login_timestamp : '',
    'last_logout' => $user->u_last_logout_timestamp ? $user->u_last_logout_timestamp : '',
    "apple_ts" => $user->u_apple_ts ? $user->u_apple_ts : '',
    "apple_id" => $user->u_apple_id ? $user->u_apple_id : '',
);

//顯示呼叫結果
$ret = array(
    "result" => "true",
    "error_message" => "",
    "data" => $data
);


echo json_encode($ret);

$this->log($page, json_encode($ret), 'Y');
exit();
