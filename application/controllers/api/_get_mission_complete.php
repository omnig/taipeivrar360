<?php
header('Content-Type: application/json');
/*
-記錄使用者完成關卡
-並回傳導引路徑
--------------------------
example API : 
http://nmp.utobonus.com/api/get_mission_complete?m_id=4&u_id=3&lat=25.080636526318713&lng=121.56444892287254&f=1

輸入參數
key1 "m_id" ->  任務ID
value1 (int) 

key2 "u_id" ->  使用者ID
value2 (int) 

key3 "lat" ->  使用者目前精度
value3 (string)   25.080636526318713

key4 "lng" ->  使用者目前緯度
value4 (string)   121.56444892287254

key5 "f" ->  使用者目前樓層  (若沒有設定則預設為1樓)
value5 (int)   2


回傳內容

若未完成關卡 => 回傳導引POI 路徑
若完成關卡 => 回傳Compete.

*/

//傳入值接收
$input_array = array(
  "m_id" => $this->input->get('m_id'),
  "ng_id" => $this->input->get('ng_id'),
  "u_id" => $this->input->get('u_id')
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}


//取得任務列表資訊
$where_array = array(
    "m_id" => $input_array['m_id'],
    "ap_id is not null"
);
$mission = $this->Common_model->get_db("nine_grid", $where_array);
$max_mission = count($mission);
$ng_ids = array_column($mission, 'ng_id');

$missionData = $this->Common_model->get_one("mission", ["m_id" => $input_array['m_id']]);
if(!isset($missionData->m_min_pass_count) || $missionData===false){
    $ret = array(
        "result" => "false",
        "error_message" => "Can't find mission.m_min_pass_count, m_id=".$input_array['m_id']
    );
    echo json_encode($ret);
    exit();
}
$minPassCount = $missionData->m_min_pass_count;

if ($input_array["u_id"] === '0') {
  $this->log($page, "INVALID USER", 'N');
  $ret = array(
      "result" => "false",
      "error_message" => "INVALID USER"
  );
  echo json_encode($ret);
  exit();
}

//彙整使用者資訊
$where_array =array(
  'm_id' => $input_array["m_id"],
  'u_id' => $input_array["u_id"]
);
$this->load->model("Mission_model");
$user = $this->Mission_model->get_mission_user("", "", 0, 0, $where_array);
if (count($user)==0) {
  //代表沒有資訊-要新增
  $insert_array = array(
      "u_id" => $input_array["u_id"],
      "m_id" => $input_array["m_id"],
      "m_id_max" => $max_mission,
      "grid_now" => 0,
      "is_finish" => "None",
      "rws_enabled" => "None"
  );

  $row = $this->Common_model->insert_db("mission_user", $insert_array);
  $user = $this->Mission_model->get_mission_user("", "", 0, 0, $where_array);
}

// 已通過關卡
$pass_grids = empty($user[0]->pass_grids)?[]:explode(',', $user[0]->pass_grids);

// 檢查該任務是否有該關卡

// $this->load->model("Nine_grid_model");
if(!in_array($input_array['ng_id'], $ng_ids)){
  // 無此關卡
  $this->log($page, "ERROR_INPUT", 'N');
  $ret = array(
      "result" => "false",
      "error_message" => "ERROR_INPUT"
  );
  echo json_encode($ret);
  exit();
}

if(in_array($input_array["ng_id"], $pass_grids)){
  // 重覆通關
  // $this->log($page, "ERROR_INPUT", 'N');
  $ret = array(
      "result" => "false",
      "error_message" => "ERROR_INPUT!",
      "data" => (object)[]
  );
  echo json_encode($ret);
  exit();
}

// 新增通關內容
array_push($pass_grids, $input_array['ng_id']);
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


// ==============================
$now_grid = $user[0]->grid_now;
$new_grid = $now_grid+1;

if(count($pass_grids) >= $minPassCount){
  //如果完成大於最大值(10>9)
  //更新資料表並回傳成功訊息
  $where_array = array(
      'm_id' => $input_array["m_id"],
      "u_id" => $input_array["u_id"]
  );
  $update_array =array(
      "grid_now" => count($pass_grids),
      "is_finish" =>"Done",
      "pass_grids" => implode(",", $pass_grids)
  );
  $this->Common_model->update_db("mission_user", $update_array, $where_array);
  $result = array(
    "status" =>"success",
    "status_code" =>200,
    "mission_status"=>"Complete",
    "message" =>"Mission Complete",
  );
}else{
  //更新資料表
  $where_array = array(
      'm_id' => $input_array["m_id"],
      "u_id" => $input_array["u_id"]
  );
  $update_array =array(
      "grid_now" => $new_grid,
      "is_finish" =>"Ing",
      "rws_enabled" => "None",
      "pass_grids" => implode(",", $pass_grids)
  );
  $this->Common_model->update_db("mission_user", $update_array, $where_array);
  $result = array(
    "status" =>"success",
    "status_code" =>200,
    "mission_status"=>"Uncomplete",
    "message" =>"Mission uncomplete",
  );
}

//顯示呼叫結果
$ret = array(
    "result" => "true",
    "error_message" => "",
    "data" => $result
);
echo json_encode($ret);

$this->log($page, '', 'Y');

exit();
