<?php
header('Content-Type: application/json');
//以使用者位置及方圓幾公里內拿AR導引點位資訊
//載入Model
$this->load->model("Index_map_model");
$this->load->model('Ap_building_model');

$input_array = array(
    "type" => $this->input->GET('type')
);

$input_array["t_id"]= $this->input->GET('t_id')?$this->input->GET('t_id'):-1;

switch ($input_array['type']) {
	case 'topic':
		$result = get_topic($this->config);
		break;
	case 'topicGroup':
		$result = get_topic_group($this->config);
		break;

	// case 'ar':
	// 	$result = get_ar($input_array["t_id"]);
	// 	break;

	case 'poi':
		$input_array = array(
			"user_lat" => $this->input->GET('user_lat')??null,
			"user_lng" => $this->input->GET('user_lng')??null,
			"radius" => $this->input->GET('radius')??100,
		);
		$result = get_poi($this->config, $input_array["user_lat"], $input_array["user_lng"], $input_array["radius"]);
		break;

	// case 'view':
	// 	$result = get_view();
	// 	break;
	
	default:
		$result['topic'] = get_topic($this->config);
		// $result['ar'] = get_ar($input_array["t_id"]);
		$result['poi'] = get_poi($this->config);
		// $result['view'] = get_view();
		break;
}


//顯示所有結果
$ret = array(
    "result" => "true",
    "code" => 200,
    "error_message" => "",
    "data" => $result 
);

echo json_encode($ret);
exit();


function get_topic_group($config){
	$CI = & get_instance();
	$topic_group_list = $CI->Index_map_model->get_topic_group_list("","",0,0,[
		'tg_enabled' => 'Y',
		'tg_del' => 'N'
	]);
	$topic_list = $CI->Index_map_model->get_topic_list("","",0,0,[
		't_enabled' => 'Y',
		't_del' => 'N'
	]);
	$poi_list = $CI->Index_map_model->get_poi_list("", "", 0, 0, [
		'ap_enabled' => 'Y'
	]);

	//做資料整理
	foreach ($topic_list as $key => $topic) {
		$ap_ids = explode(',', $topic->ap_ids);
		$pois = array_filter($poi_list, function($ap)use($ap_ids){
			return in_array($ap->ap_id, $ap_ids);
		});
		usort($pois, function($last, $current)use($ap_ids){
			$current_index = array_search($current->ap_id, $ap_ids);
			$last_index = array_search($last->ap_id, $ap_ids);
			return $last_index > $current_index ? 1 : -1;
		});
		$pois = tidyPOI(array_values($pois)); // 移除filter的index
		$topic_result[$key] = [
			"id" => $topic->t_id,
			"name" => $topic->t_name,
			"description" => $topic->t_describe,
			"image" => $CI ->config->item('server_base_url')."/upload/topic_image/".$topic->t_image,
			"category" => [
				"id" => $topic->tc_id,
				"title" => $topic->tc_title_zh,
			],
			"pois" => $pois,
		];
	}
	
	foreach($topic_group_list as  $key => $topic_group){
		$t_ids = explode(',', $topic_group->t_ids);
		$topics = array_filter($topic_result, function($topic)use($t_ids){
			return in_array($topic["id"], $t_ids);
		});
		usort($topics, function($last, $current)use($t_ids){
			$current_index = array_search($current["id"], $t_ids);
			$last_index = array_search($last["id"], $t_ids);
			return $last_index > $current_index ? 1 : -1;
		});
		$topics = array_values($topics); // 移除filter的index
		$topic_group_result[$key] = [
			"id" => $topic_group->tg_id,
			"name" => $topic_group->tg_name,
			"description" => $topic_group->tg_describe,
			"image" => $CI ->config->item('server_base_url')."/upload/topic_group_image/".$topic_group->tg_image,
			"topics" => $topics,
		];
	}

	//輸出結果
	return isset($topic_group_result) ? $topic_group_result : [];
}

function get_topic($config){
	$CI = & get_instance();
	//找尋附近主題
	//判斷主題沒有被關閉
	//判斷主題沒有被刪除
	$where_array = array(
		't_enabled' => 'Y',
		't_del' => 'N'
	);
	$topic_list = $CI->Index_map_model->get_topic_list("","",0,0,$where_array);
	$poi_list = $CI->Index_map_model->get_poi_list("", "", 0, 0, ['ap_enabled' => 'Y']);

	//做資料整理
	foreach ($topic_list as $key => $topic) {
		$ap_ids = explode(',', $topic->ap_ids);
		$pois = array_filter($poi_list, function($ap)use($ap_ids){
			return in_array($ap->ap_id, $ap_ids);
		});
		usort($pois, function($last, $current)use($ap_ids){
			$current_index = array_search($current->ap_id, $ap_ids);
			$last_index = array_search($last->ap_id, $ap_ids);
			return $last_index > $current_index ? 1 : -1;
		});
		$pois = tidyPOI(array_values($pois)); // 移除filter的index
		$topic_result[$key] = [
			"id" => $topic->t_id,
			"name" => $topic->t_name,
			"description" => $topic->t_describe,
			"image" => $CI ->config->item('server_base_url')."/upload/topic_image/".$topic->t_image,
			"category" => [
				"id" => $topic->tc_id,
				"title" => $topic->tc_title_zh,
			],
			"pois" => $pois,
		];
	}

	//輸出結果
	return isset($topic_result) ? $topic_result : [];
}

function get_poi($config, $user_lat = null, $user_lng = null, $radius = null){
	$CI = & get_instance();
	//判斷設施有沒有啟用
	//判斷設施不是門口
	$where_array = array(
		// 'ap_enabled' => 'Y',
		// 'ac_title_en !=' => 'ar point'
		'ap_enabled' => 'Y',
	);

	$poi_result = $CI->Index_map_model->get_poi_list("", "", 0, 0, $where_array, $user_lat, $user_lng, $radius);

	//輸出結果
	return tidyPOI($poi_result);
}

function tidyPOI($poi_list){
	$CI = & get_instance();
	$poi_result = [];
	//做資料整理
	foreach ($poi_list as $key => $row) {
		$poi_result[$key] = array(
			"id" => $row->ap_id,
			"name" => $row->ap_name,
			"desc" => $row->ap_desc,
			// "locid" => ($row->ap_locid) ? $row->ap_locid : "",
			"enabled" => $row->ap_enabled,
			"lat" => $row->ap_lat,
			"lng" => $row->ap_lng,
			"hyperlink_text" => $row->ap_hyperlink_text,
			"hyperlink_url" => $row->ap_hyperlink_url,
			"video_hyperlink" => $row->ap_video_hyperlink,
			"audio_path" => is_null($row->ap_audio_path) ? "" : $CI->config->item("server_base_url")."/upload/audio/".$row->ap_audio_path,
			"image" => $CI->config->item("server_base_url")."/upload_image/".$row->ap_image_path,
			"category" => [
				"id" => $row->ac_id,
				"title" => $row->ac_title_zh,
				"image" => empty($row->ac_image) ? null : $CI->config->item("server_base_url") . "/upload/ap_category/" . json_decode($row->ac_image)->s100,
			],
			"ar_trigger" => [
				"active_method" => $row->ap_ar_active_method,
				"distance" => $row->ap_ar_distance,
				"identify_image_path" => $CI->config->item("server_base_url")."/upload_image/".$row->dai_image_path,
			],
			"ar" => [
				"text" => $row->ap_ar_text,
				"url" => $row->ap_ar_url,
				"heigh" => $row->ap_ar_heigh,
				"content_type" => $row->tdar_content_type,
				// "content" => $CI->config->item("server_base_url")."/upload/image/".$row->tdar_content_path,
				"interactive_text" => $row->tdar_interactive_text,
				"interactive_url" => $row->tdar_interactive_url,
				"cover_identify_image" => $row->ap_cover_identify_image,
				"size" => number_format((float)$row->ap_ar_vsize/100,2)
			]
		);
		if(is_null($row->b_id)){
			$poi_result[$key]["beacon"] = null;
		}else{
			$poi_result[$key]["beacon"] = [
				"id" => $row->b_id,
				"description" => $row->b_description,
				"major" => $row->b_major,
				"minor" => $row->b_minor,
                "hwid" => $row->b_hwid,
				"mac" => $row->b_mac,
				"lat" => $row->b_lat,
				"lng" => $row->b_lng,
				"range" => $row->b_range,
			];
		}
		if(is_null($row->ol_id)){
			$poi_result[$key]["optical_label"] = null;
		}else{
			$poi_result[$key]["optical_label"] = [
				"id" => $row->ol_id,
				"description" => $row->ol_description,
				"identify_code" => $row->ol_identify_number,
				"range" => $row->ol_range,
			];
		}
		switch ($row->tdar_content_type) {
			case 'text':
				$poi_result[$key]["ar"]["content"] = $row->tdar_content_path;
		
				//產生文字圖片
				$resulr_url = $CI->config->item('server_base_url')."/api/get_text_image?text=".$row->tdar_content_path;
				//$resulr_url = curl_query($url);
				$poi_result[$key]["ar"]["url_image"] = $resulr_url;
		
				break;
			case 'image':
				$poi_result[$key]["ar"]["content"] = $CI->config->item('server_base_url')."/upload/image/".$row->tdar_content_path;		
				break;
			case 'youtube':
				$poi_result[$key]["ar"]["content"] = $row->tdar_content_path;
				break;
			case '3d_model':
				$poi_result[$key]["ar"]["content"] = $CI->config->item('server_base_url')."/upload_model/".$row->tdar_content_path;
				$poi_result[$key]["ar"]["animation_name"] = $row->tdar_animation_name;
				$poi_result[$key]["ar"]["ios"] = $CI->config->item('server_base_url')."/upload_model/".$row->tdar_content_path;
				$poi_result[$key]["ar"]["android"] = $CI->config->item('server_base_url')."/upload_model/".$row->tdar_content_wt3;
		
				break;
			case 'video':
				$poi_result[$key]["ar"]["content"] = $CI->config->item('server_base_url')."/upload_video/".$row->tdar_content_path;
				$poi_result[$key]["ar"]["isTransparent"] = $row->tdar_is_transparent;
				break;
		}
	}
	return $poi_result;
}