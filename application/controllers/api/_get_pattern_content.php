<?php
header('Content-Type: application/json');
//以Image ID 拿到 AR Pattern 對應資料

$input_array = array(
    "pattern_id" => $this->input->GET('pattern_id')
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}
 
$input_array['user_id'] = $this->input->GET('user_id')?$this->input->GET('user_id'):"";

//局處AR列表
$where_array = array(
    'tdar_del' => "N"
);
$where_like_array = array(
	'tdar_image_path' => $input_array["pattern_id"]
	);

//先找局處
$ar_result = $this->Common_model->get_one_with_like("tp_department_ar", $where_array,"tdar_id","asc",$where_like_array);


if (!$ar_result) {
	//在找名眾
    $obj = new stdClass();
	// http_response_code(204);
	$ret = array(
	    "result" => "false",
	    "code" => 204,
	    "error_message" => "No Data Here",
	    "data" => $obj
	);
	echo json_encode($ret);
	exit();

}

//移除不必要資訊
// unset($ar_result->tdar_id);
unset($ar_result->t_id);
unset($ar_result->d_id);
unset($ar_result->tdar_characteristic);
unset($ar_result->tdar_characteristic_state);
unset($ar_result->tdar_to_og);
unset($ar_result->tdar_create_timestamp);
unset($ar_result->tdar_collection);
unset($ar_result->tdar_state);
unset($ar_result->tdar_del);
unset($ar_result->tdar_lat);
unset($ar_result->tdar_lng);
unset($ar_result->tdar_description);
unset($ar_result->tdar_findpho_path);

$ar_result->tdar_view_size = $ar_result->tdar_vsize;
unset($ar_result->tdar_vsize);

$ar_object = $ar_result;
$ar_object->name = $ar_result->tdar_name;
$ar_object->image_path = $this->config->item('server_base_url')."upload_image/".$ar_object->tdar_image_path;
$ar_object->content_type = $ar_result->tdar_content_type;


$ar_object->angle = ($ar_result->tdar_angle>91)?(string)($ar_result->tdar_angle+90):(string)($ar_result->tdar_angle-90);
$ar_object->ext = preg_replace('/^.*\.([^.]+)$/D', '$1', $ar_result->tdar_content_path);



switch ($ar_result->tdar_content_type) {
	case 'text':
		$ar_object->content = $ar_object->tdar_content_path;

		//產生文字圖片
		//$url = "http://api.img4me.com/?text=".$ar_object->tdar_content_path."&font=arial&fcolor=000000&size=30&bcolor=FFFFFF&type=png";
        $resulr_url = $this->config->item('server_base_url')."/api/get_text_image?text=".$ar_object->tdar_content_path;
		//$resulr_url = curl_query($url);
		$ar_object->url_image = $resulr_url;

		break;
	case 'image':
		$ar_object->content = $this->config->item('server_base_url')."upload/image/".$ar_object->tdar_content_path;		
		break;
	case 'youtube':
		$ar_object->content = $ar_object->tdar_content_path;
		break;
	case '3d_model':
		$ar_object->content = $this->config->item('server_base_url')."upload_model/".$ar_object->tdar_content_path;
		$ar_object->ios = $this->config->item('server_base_url')."upload_model/".$ar_object->tdar_content_path;
		$ar_object->android = $this->config->item('server_base_url').$ar_object->tdar_content_wt3;

		break;
	case 'video':
		$ar_object->content = $this->config->item('server_base_url')."upload_video/".$ar_object->tdar_content_path;
		break;
}


unset($ar_result->tdar_image_path);
unset($ar_result->tdar_content_path);
unset($ar_result->tdar_content_type);
unset($ar_result->tdar_angle);
unset($ar_result->tdar_name);


//記錄使用者AR ID
if($input_array['user_id']!=""&&$input_array['user_id']!=NULL){
	user_like_ar($ar_result->tdar_id,$input_array['user_id']);
}

//顯示呼叫結果
$ret = array(
    "result" => "true",
    "code" => 200,
    "error_message" => "",
    "data" => $ar_object 
);
$this->log($page, json_encode($ret), 'Y');
echo json_encode($ret);
exit();



function user_like_ar($tdar_id,$u_id){
	//資料庫檢查是否存在
	$CI = & get_instance();
    $where_array = array(
        'user_id' => $u_id,
        'tdar_id' => $tdar_id
    );
    $db_result =  $CI->Common_model->get_one("user_ar", $where_array);

    //假如有不做任何動作
    //假如沒有Insert

    if ($db_result) {
       
        
    }else{

        $where_array = array(
            'user_id' => $u_id,
            'tdar_id' => $tdar_id
        );
        $db_result =  $CI->Common_model->insert_db("user_ar", $where_array);

        if ($db_result) {
            $message = [
                'status' => TRUE,
                'user_id' => $u_id,
                'tdar_id' => $tdar_id,
                'message' => 'update_success'
            ];
        }else{
            $message = [
                'status' => FALSE,
                'user_id' => $u_id,
                'tdar_id' => $tdar_id,
                'message' => 'update_fail'
            ];
        }

    }
}