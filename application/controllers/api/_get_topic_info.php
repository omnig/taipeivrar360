<?php
header('Content-Type: application/json');
$this->load->model("Index_map_model");       
$input_array = array(
    "topic_id" => $this->input->GET('topic_id')
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}

//局處列表
$where_array = array();

$department_list = $this->Common_model->get_db("tp_admin_group", $where_array);

foreach ($department_list as $key => $value) {
	$data["department_list"][$value->ag_id] = $value->ag_name;
}

//資料表名稱
$data["table_name"] = "topic_manage";

$model_name = ucfirst($data['table_name']) . '_model';
$this->load->model($model_name);


$order_by = 'ag_order ASC';

$input_array['limit'] = 0;

$input_array['skip'] = 0;

$where_array = array(
	't_id'=>$input_array['topic_id'],
	'ag_enabled' => 'Y'
);

//從資料庫取得本次資料
$result = $this->$model_name->get_list($order_by, '', $input_array['limit'], $input_array['skip'], $where_array, false);
$response_data = array();


//取得AR數量
$where_array = array(
	'tda.ap_puid !=' => NULL,
	'tdar_state' => 'Y',
	'tdar_del' => 'N',
	'tdar_to_og' => 'Y',
	't_id' => $input_array['topic_id']
);
$ar_result = $this->Index_map_model->get_department_ar_list("","",0,0,$where_array,true);


//取得景點數量
$where_array = array(
	'ap_enabled' => 'Y',
	'ac_title_en !=' => 'ar point',
	't_id' => $input_array['topic_id']
);
$poi_result = $this->Index_map_model->get_poi_list_with_topic("","",0,0,$where_array,true);

// echo json_encode($result);
// exit;

foreach ($result as $result_key => $result_value) {


	$response_data[$result_key]=array(
		"public_photo" => $result_value->t_public_photo, //
		"public_video" => $result_value->t_public_video, //
		"public_ar" => $result_value->t_public_ar,   //
		"public_ar_count" => $ar_result,   //
		"public_poi_count" => $poi_result,   //
		"t_lat" => $result_value->t_lat, //經度
		"t_lng" => $result_value->t_lng, //緯度
	);

}


//顯示呼叫結果
$ret = array(
    "result" => "true",
    "code" => 200,
    "error_message" => "",
    "data" => $response_data
);

//沒有結果
if(count($result)<=0){
	http_response_code(400);
	$ret = array(
	    "result" => "false",
	    "code" => 400,
	    "error_message" => "None Data",
	    "data" => array()
	);
}

echo json_encode($ret);
exit();

