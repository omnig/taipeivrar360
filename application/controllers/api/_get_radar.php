<?php

//傳入值接收
$input_array = array(
    "lat" => round($this->input->post("user_lat"), 7),
    "lng" => round($this->input->post("user_lng"), 7),
);

foreach ($input_array as $index => $value) {
    if (empty($value)) {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}

$input_array["g_id"] = $this->input->post("id");


//取得radar資料
$where_array = array(
    "r_enabled" => "Y",
    "r_del" => "N"
);
if ($input_array["g_id"]) {
    $where_array["g_id"] = $input_array["g_id"];
}

//找出離使用者最近的radar
$this->load->model("Radar_model");
$radar = $this->Radar_model->get_db_by_range("r_distance", "", 1, 0, $where_array, $input_array["lat"], $input_array["lng"]);

if (!$radar) {
    $this->log($page, "INVALID RADAR", 'N');
    exit();
}


$radar_data = array();
foreach ($radar as $radarkey => $radar) {

    $radar_data[$radarkey] = array(
        "id" => (int) $radar->r_id,
        "title" => $radar->r_title,
        "lat" => (float) $radar->r_lat,
        "lng" => (float) $radar->r_lng,
        "range" => (float) $radar->r_range,
        "radius" => (float) $radar->r_radius,
    );
}

//顯示呼叫結果
$ret = [
    "result" => "true",
    "code" => 200,
    "error_message" => "",
    "data" => [
        "radar" => $radar_data
    ]
];


echo json_encode($ret);

$this->log($page, json_encode($ret), 'Y');

exit();


