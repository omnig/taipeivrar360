<?php

//傳入值接收
$input_array = array(
    "timestamp" => $this->input->post("timestamp"),
    "mac" => $this->input->post("mac"),
    'login_token' => $this->input->post("login_token"),
    "device_id" => $this->input->post("device_id")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}

//驗證mac
if (!$this->verify_mac($input_array["timestamp"], $input_array["mac"])) {
    $this->log($page, "ACCESS_DENY", 'N');
}

//取得使用者資料
$where_array = array(
    "u_device_id" => $input_array["device_id"]
);

$user = $this->Common_model->get_one("user", $where_array);

if (!$user) {
    $this->log($page, "INVALID_DEVICE_ID", 'N');
}

//更新資料
$update_array = array(
    "u_login_token" => "",
    'u_last_logout_timestamp' => date('Y-m-d H:i:s', $input_array["timestamp"])
);

$this->Common_model->update_db("user", $update_array, $where_array);

//準備回傳結果
$data = array();

//顯示呼叫結果
$ret = array(
    "result" => "true",
    "error_message" => "",
    "data" => $data
);

$input_array['format'] = $this->input->post('format') ? strtolower($this->input->post('format')) : 'json';

if ($input_array['format'] == 'xml') {
    $xml = array_to_xml($ret, false);

    header('Content-Type: application/xml');
    echo $xml;
} else {
    echo json_encode($ret);
}

$this->log($page, json_encode($ret), 'Y');

exit();
