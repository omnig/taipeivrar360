<?php

//傳入值接收
$input_array = array(
    "timestamp" => $this->input->post("timestamp"),
    "mac" => $this->input->post("mac"),
    "device_id" => $this->input->post("device_id"),
    'parameter' => $this->input->post('parameter')
);


foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}

//驗證mac
if (!$this->verify_mac($input_array["timestamp"], $input_array["mac"])) {
    $this->log($page, "ACCESS_DENY", 'N');
}

$input_array['parameter'] = trim(htmlspecialchars_decode($input_array['parameter']));
$input_array['parameter'] = str_replace('&quot;','"',$input_array['parameter']);
$params = json_decode($input_array['parameter'], true);

foreach ($params as $row) {
    $input_array["fb_id"] = $row["id"];
    $input_array["fb_name"] = $row['name'];
    $input_array["fb_email"] = $row['email'];
}

//必須傳入帳號密碼，或是fb_id
if (!($input_array["fb_id"])) {
    $this->log($page, "INVALID_PARAMETER", 'N');
}

if ($input_array["fb_id"]) {
    //驗證fb_id
    $user = $this->fb_login($input_array["fb_id"], $input_array["fb_name"], $input_array["fb_email"], $input_array["device_id"]);
}

//沒有取得登入使用者資料，登入失敗
if (!$user) {
    $this->log($page, "LOGIN_FAIL", 'N');
}

//準備回傳結果
$data = array(
    "u_id" => $user->u_id,
    "user_email" => $user->u_email,
    "login_token" => $user->u_login_token_app,
    "user_name" => $user->u_name,
    "last_login" => $user->u_last_login_timestamp ? $user->u_last_login_timestamp : '',
    'last_logout' =>$user->u_last_logout_timestamp ? $user->u_last_logout_timestamp : '',
    "fb_ts" => $user->u_fb_ts ? $user->u_fb_ts : '',
    "fb_id" => $user->u_fb_id ? $user->u_fb_id : ''
);

//顯示呼叫結果
$ret = array(
    "result" => "true",
    "error_message" => "",
    "data" => $data
);

$input_array['format'] = $this->input->post('format') ? strtolower($this->input->post('format')) : 'json';

if ($input_array['format'] == 'xml') {
    $xml = array_to_xml($ret, false);

    header('Content-Type: application/xml');
    echo $xml;
} else {
    echo json_encode($ret);
}

$this->log($page, json_encode($ret), 'Y');

exit();
