<?php
header('Content-Type: application/json');
//以使用者位置及方圓幾公里內拿AR導引點位資訊
//載入Model
$this->load->model("Index_search_model");


$input_array = array(
    "type" => $this->input->GET('type'),
    "keyword" => $this->input->GET('keyword')
);

//檢查要送進資料庫的查詢是否超過字數
if(!valid_string_lenth($input_array['keyword'],10)){
	
    $this->log($page, "INVALID_PARAMETER", 'N');
    echo json_encode($ret);
    exit();
}


switch ($input_array['type']) {
	case 'topic':
		$keyword = ($input_array['keyword'])?$input_array['keyword']:"";
		$result = get_topic($keyword );
		break;

	case 'ar':
		$keyword = ($input_array['keyword'])?$input_array['keyword']:"";
		$result = get_ar($keyword );
		break;

	case 'poi':
		$keyword = ($input_array['keyword'])?$input_array['keyword']:"";
		$result = get_poi($keyword );
		break;
	
	default:
		$keyword = ($input_array['keyword'])?$input_array['keyword']:"";
		$result['topic'] = get_topic($keyword);
		$result['ar'] = get_ar($keyword);
		$result['poi'] = get_poi($keyword);
		$result['view'] = get_viewpoint($keyword);
		break;
}


//顯示所有結果
$ret = array(
    "result" => "true",
    "code" => 200,
    "error_message" => "",
    "data" => $result 
);

echo json_encode($ret);
exit();



function get_topic($keyword=""){
	$CI = & get_instance();
	//找尋附近主題
	//判斷主題沒有被關閉
	//判斷主題沒有被刪除
	$where_array = array(
		't_enabled' => 'Y',
		't_del' => 'N'
	);
	$topic_result = $CI ->Index_search_model->get_topic_list("",$keyword,0,0,$where_array);

	// echo json_encode($topic_result);
	// exit;

	//做資料整理
	foreach ($topic_result as $key => $value) {
		//加入網頁連結
		// $topic_result[$key]->web = $CI ->config->item("server_base_url")."#";
	}

	//做資料整理
	foreach ($topic_result as $key => $value) {
		@$topic[$key]->id = (int)$value->t_id;
		@$topic[$key]->name = $value->t_name;
		@$topic[$key]->description = $value->t_describe;
		@$topic[$key]->img = $CI ->config->item('server_base_url')."upload/topic_image/".$value->t_image;
		@$topic[$key]->lat = (double)$value->t_lat;
		@$topic[$key]->lng = (double)$value->t_lng;
		@$topic[$key]->web = $CI ->config->item("server_base_url")."#";

		$where_array = array(
	    "t_id" => @$topic[$key]->id
		);
		$keywords_ary = array();
		$keywords = $CI ->Common_model->get_db_join("keyword_topic as kt", $where_array , "keywords as ks" , "kt.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");

		foreach ($keywords as $keywords_key => $keywords_value) {

			if($keywords_value->kwd_name!=NULL && $keywords_value->kwd_name!="" ){
			    $keywords_ary[$keywords_key] = $keywords_value->kwd_name;
			}
		}

		@$topic[$key]->keywords = $keywords_ary;
	}

	

	//輸出結果
	$result = array();
	if(isset($topic)){
		$result = $topic;
	}
	return $result;
}

function get_ar($keyword=""){
	$CI = & get_instance();
	//找尋附近AR
	//判斷AR沒有被關閉
	//判斷主題沒有被刪除
	$where_array = array(
		'tdar_state' => 'Y',
		'tdar_del' => 'N',
		'tdar_to_og' => 'Y'
	);
	$ar_result = $CI->Index_search_model->get_department_ar_list("",$keyword,0,0,$where_array);

	//做資料整理
	foreach ($ar_result as $key => $value) {
		if (isset($value->ab_id)) {
			@$ar[$key]->ab_id = (int)$value->ab_id;
			@$ar[$key]->inside =TRUE;
		}else{
			@$ar[$key]->ab_id = NULL;
			@$ar[$key]->inside =FALSE;
		}
		@$ar[$key]->id = (int)$value->tdar_id;
		@$ar[$key]->name = $value->tdar_name;
		@$ar[$key]->img = $CI->config->item("server_base_url")."upload/image/".$value->tdar_findpho_path;
		@$ar[$key]->description = $value->tdar_description;
		@$ar[$key]->lat = (double)$value->tdar_lat;
		@$ar[$key]->lng = (double)$value->tdar_lng;

		$where_array = array(
	    "ar_id" => @$ar[$key]->id
		);

		$keywords_ary = array();

		$keywords = $CI ->Common_model->get_db_join("keyword_department_ar as kda", $where_array , "keywords as ks" , "kda.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");


		foreach ($keywords as $keywords_key => $keywords_value) {

			if($keywords_value->kwd_name!=NULL && $keywords_value->kwd_name!="" ){
			    $keywords_ary[$keywords_key] = $keywords_value->kwd_name;
			}
		}

		@$ar[$key]->keywords = $keywords_ary;
	}

	//輸出結果
	$result = array();
	if(isset($ar)){
		$result = $ar;
	}

	return $result;
	// return $ar_result;
}

function get_poi($keyword){
	$CI = & get_instance();
	//判斷設施有沒有啟用
	//判斷設施不是門口
	$where_array = array(
		'ap_enabled' => 'Y',
		'ap_is_door' => 'N',
		'ac_title_en !=' => 'ar point'
	);

	//找尋附近設施
	$poi_result = $CI->Index_search_model->get_poi_list("",$keyword,0,0,$where_array);
	//做資料整理
	foreach ($poi_result as $key => $value) {
		@$poi[$key]->id = (int)$value->ap_id;
		@$poi[$key]->ab_id = (int)$value->ab_id;
		@$poi[$key]->name = $value->ap_name;
		@$poi[$key]->lat = (double)$value->ap_lat;
		@$poi[$key]->lng = (double)$value->ap_lng;
		@$poi[$key]->title = $value->ac_title_zh;
		//更改圖片 json_decode 

		if (isset($value->ac_image)) {
			$image_ary = json_decode($value->ac_image,TRUE);
			@$poi[$key]->img = $CI->config->item("server_base_url")."upload/ap_category/".$image_ary['s100'];
		}else{
			@$poi[$key]->img = "";

		}
	}

	//輸出結果
	$result = array();
	if(isset($poi)){
		$result = $poi;
	}
	return $result;
}

function get_viewpoint($keyword){
	$CI = & get_instance();
	//判斷設施有沒有啟用
	//判斷設施不是門口
	$where_array = array(
		'v_enabled' => 'Y',
		'v_del' => 'N',
	);

	//找尋附近設施
	$poi_result = $CI->Index_search_model->get_viewpoint_list("",$keyword,0,0,$where_array);
	//做資料整理
	foreach ($poi_result as $key => $value) {
		@$poi[$key]->id = (int)$value->v_id;
		@$poi[$key]->ab_id = "";
		@$poi[$key]->name = $value->v_name;
		@$poi[$key]->lat = (double)$value->v_lat;
		@$poi[$key]->lng = (double)$value->v_lng;
		@$poi[$key]->title = $value->v_name;
		//更改圖片 json_decode 

		if (isset($value->ac_image)) {
			$image_ary = json_decode($value->ac_image,TRUE);
			@$poi[$key]->img = $CI->config->item("server_base_url")."upload/ap_category/".$image_ary['s100'];
		}else{
			@$poi[$key]->img = $value->v_image;

		}
	}

	//輸出結果
	$result = array();
	if(isset($poi)){
		$result = $poi;
	}
	return $result;
}
