<?php

//傳入值接收
$input_array = array(
    // "timestamp" => $this->input->post("timestamp"),
    // "mac" => $this->input->post("mac"),
    // "device_id" => $this->input->post("device_id"),
    "lat" => round($this->input->post("user_lat"), 7),
    "lng" => round($this->input->post("user_lng"), 7),
);

foreach ($input_array as $index => $value) {
    if (empty($value)) {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}

// //驗證mac
// if (!$this->verify_mac($input_array["timestamp"], $input_array["mac"])) {
//     $this->log($page, "ACCESS_DENY", 'N');
// }

$input_array["g_id"] = $this->input->post("id");


//取得geofence資料
$where_array = array(
    "g_enabled" => "Y",
    "g_del" => "N"
);
if ($input_array["g_id"]) {
    $where_array["g_id"] = $input_array["g_id"];
}

//找出離使用者最近的geofence
$this->load->model("Geofence_model");
$geofence = $this->Geofence_model->get_db_by_range("g_distance", "", 0, 0, $where_array, $input_array["lat"], $input_array["lng"]);

if (!$geofence) {
    $this->log($page, "INVALID GEOFENCE", 'N');
    exit();
}


// //取得Geofence推播資訊
// $where_array = array(
//     "gt_start_timestamp <=" => date("Y-m-d H:i:s"),
//     "gt_end_timestamp >=" => date("Y-m-d H:i:s")
// );
// $this->load->model("Geofence_trigger_model");
// $geofence_trigger = $this->Geofence_trigger_model->get_relation("", "", 0, 0, $where_array);

// echo json_encode($geofence_trigger);
// exit;

/*
  //寫入已領取之Geofence推播資訊
  if ($geofence_trigger) {
  $insert_array = array();
  foreach ($geofence_trigger as $key => $row) {
  $insert_array[] = array(
  "gt_id" => $row->gt_id,
  "gr_device_id" => $input_array["device_id"],
  'u_id' => $user->u_id,
  'gr_timestamp' => date('Y-m-d H:i:s')
  );
  }
  $this->Common_model->batch_insert_db("geofence_record", $insert_array);
  }
 */

$this->load->model("Index_map_model");

$geofence_data = array();
foreach ($geofence as $geokey => $geo) {

    $geofence_data[$geokey] = array(
        "id" => (int) $geo->g_id,
        "title" => $geo->g_title,
        "lat" => (float) $geo->g_lat,
        "lng" => (float) $geo->g_lng,
        "range" => (float) $geo->g_range,
        "poi_notify_enabled" => $geo->g_poi_notify_enabled,
        "ar_trigger" =>[
            "active_method" => $geo->g_ar_active_method,
            "distance" => $geo->g_ar_distance,
            "identify_image_path" => $this->config->item("server_base_url")."/upload_image/".$geo->dai_image_path,
        ],
        "ar" => [
            "text" => $geo->g_ar_text,
            "url" => $geo->g_ar_url,
            "heigh" => $geo->g_ar_heigh,
            "content_type" => $geo->tdar_content_type,
            "interactive_text" => $geo->tdar_interactive_text,
            "interactive_url" => $geo->tdar_interactive_url,
            "cover_identify_image" => $geo->g_cover_identify_image,
            "size" => number_format((float)$geo->g_ar_vsize/100,2)
        ]
    );
    switch ($geo->tdar_content_type) {
        case 'text':
            $geofence_data[$geokey]->content = $geo->tdar_content_path;
    
            //產生文字圖片
            $resulr_url = $this->config->item('server_base_url')."/api/get_text_image?text=".$geo->tdar_content_path;
            //$resulr_url = curl_query($url);
            $geofence_data[$geokey]["ar"]["url_image"] = $resulr_url;
    
            break;
        case 'image':
            $geofence_data[$geokey]["ar"]["content"] = $this->config->item('server_base_url')."/upload/image/".$geo->tdar_content_path;		
            break;
        case 'youtube':
            $geofence_data[$geokey]["ar"]["content"] = $geo->tdar_content_path;
            break;
        case '3d_model':
            $geofence_data[$geokey]["ar"]["content"] = $this->config->item('server_base_url')."/upload_model/".$geo->tdar_content_path;
            $geofence_data[$geokey]["ar"]["ios"] = $this->config->item('server_base_url')."/upload_model/".$geo->tdar_content_path;
            $geofence_data[$geokey]["ar"]["android"] = $this->config->item('server_base_url').$geo->tdar_content_wt3;
            $geofence_data[$geokey]["ar"]["animation_name"] = $geo->tdar_animation_name;
    
            break;
        case 'video':
            $geofence_data[$geokey]["ar"]["content"] = $this->config->item('server_base_url')."/upload_video/".$geo->tdar_content_path;
            break;
    }
    if($geofence_data[$geokey]["poi_notify_enabled"] === "N"){
        $geofence_data[$geokey]["poi"] = null;
    }else{
        $pois = $this->Index_map_model->get_poi_list("", "", 0, 0, ['ap.ap_id' => $geo->ap_id], null, null, 0);
        if(count($pois) === 0){
            $geofence_data[$geokey]["poi_notify_enabled"] = 'N';
            $geofence_data[$geokey]["poi"] = null;
        }else{
            $poi = $pois[0];
            $geofence_data[$geokey]["poi"] = [
                "id" => $poi->ap_id,
                "name" => $poi->ap_name,
                "desc" => $poi->ap_desc,
                "enabled" => $poi->ap_enabled,
                "lat" => $poi->ap_lat,
                "lng" => $poi->ap_lng,
                "hyperlink_text" => $poi->ap_hyperlink_text,
                "hyperlink_url" => $poi->ap_hyperlink_url,
                "video_hyperlink" => $poi->ap_video_hyperlink,
                "audio_path" => is_null($poi->ap_audio_path) ? "" : $this->config->item("server_base_url")."/upload/audio/".$poi->ap_audio_path,
                "image" => $this->config->item("server_base_url")."/upload_image/".$poi->ap_image_path,
                "category" => [
                    "id" => $poi->ac_id,
                    "title" => $poi->ac_title_zh,
                    "image" => empty($poi->ac_image) ? null : $this->config->item("server_base_url") . "/upload/ap_category/" . json_decode($poi->ac_image)->s100,
                ],
                "ar_trigger" => [
                    "active_method" => $poi->ap_ar_active_method,
                    "distance" => $poi->ap_ar_distance,
                    "identify_image_path" => $this->config->item("server_base_url")."/upload_image/".$poi->dai_image_path,
                ],
                "ar" => [
                    "text" => $poi->ap_ar_text,
                    "url" => $poi->ap_ar_url,
                    "heigh" => $poi->ap_ar_heigh,
                    "content_type" => $poi->tdar_content_type,
                    "interactive_text" => $poi->tdar_interactive_text,
                    "interactive_url" => $poi->tdar_interactive_url,
                    "cover_identify_image" => $poi->ap_cover_identify_image,
                    "size" => number_format((float)$poi->ap_ar_vsize/100,2)
                ]
            ];
            if(is_null($poi->b_id)){
                $geofence_data[$geokey]["poi"]["beacon"] = null;
            }else{
                $geofence_data[$geokey]["poi"]["beacon"] = [
                    "id" => $poi->b_id,
                    "description" => $poi->b_description,
                    "major" => $poi->b_major,
                    "minor" => $poi->b_minor,
                    "hwid" => $poi->b_hwid,
                    "mac" => $poi->b_mac,
                    "lat" => $poi->b_lat,
                    "lng" => $poi->b_lng,
                    "range" => $poi->b_range,
                ];
            }
            if(is_null($poi->ol_id)){
                $geofence_data[$geokey]["poi"]["optical_label"] = null;
            }else{
                $geofence_data[$geokey]["poi"]["optical_label"] = [
                    "id" => $poi->ol_id,
                    "description" => $poi->ol_description,
                    "identify_code" => $poi->ol_identify_number,
                    "range" => $poi->ol_range,
                ];
            }
            switch ($poi->tdar_content_type) {
                case 'text':
                    $geofence_data[$geokey]["poi"]["ar"]["content"] = $poi->tdar_content_path;
            
                    //產生文字圖片
                    $resulr_url = $this->config->item('server_base_url')."/api/get_text_image?text=".$poi->tdar_content_path;
                    //$resulr_url = curl_query($url);
                    $geofence_data[$geokey]["poi"]["ar"]["url_image"] = $resulr_url;
            
                    break;
                case 'image':
                    $geofence_data[$geokey]["poi"]["ar"]["content"] = $this->config->item('server_base_url')."/upload/image/".$poi->tdar_content_path;		
                    break;
                case 'youtube':
                    $geofence_data[$geokey]["poi"]["ar"]["content"] = $poi->tdar_content_path;
                    break;
                case '3d_model':
                    $geofence_data[$geokey]["poi"]["ar"]["content"] = $this->config->item('server_base_url')."/upload_model/".$poi->tdar_content_path;
                    $geofence_data[$geokey]["poi"]["ar"]["animation_name"] = $poi->tdar_animation_name;
                    $geofence_data[$geokey]["poi"]["ar"]["ios"] = $this->config->item('server_base_url')."/upload_model/".$poi->tdar_content_path;
                    $geofence_data[$geokey]["poi"]["ar"]["android"] = $this->config->item('server_base_url')."/upload_model/".$poi->tdar_content_wt3;
            
                    break;
                case 'video':
                    $geofence_data[$geokey]["poi"]["ar"]["content"] = $this->config->item('server_base_url')."/upload_video/".$poi->tdar_content_path;
                    $geofence_data[$geokey]["poi"]["ar"]["isTransparent"] = $poi->tdar_is_transparent;
                    break;
            }
        }
    }
}

//顯示呼叫結果
$ret = [
    "result" => "true",
    "code" => 200,
    "error_message" => "",
    "data" => [
        "geofence" => $geofence_data
    ]
];


echo json_encode($ret);

$this->log($page, json_encode($ret), 'Y');

exit();


