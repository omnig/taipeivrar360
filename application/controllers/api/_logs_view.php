<?php
//檢查傳入值start
//處理傳入值
$input_result = $this->input->post("result");
$input_keyword = $this->input->post("keyword");

if ($input_result != 'Y' && $input_result != 'N') {
    die();
}

if ($input_result == 'Y') {
    $caption = "API成功紀錄";
} else {
    $caption = "API失敗紀錄";
}

$where_string = "la_success = '{$input_result}'";
if ($input_keyword) {
    $keyword_array = explode(' ', $input_keyword);
    foreach ($keyword_array as $keyword) {
        $where_string.=" AND (la_api_name LIKE '%{$keyword}%' OR la_params LIKE '%{$keyword}%' OR la_result LIKE '%{$keyword}%')";
    }
}

$rows = $this->Common_model->get_db_by_where_string("log_api", $where_string, 200, "la_id", "desc");

?>

<table class="account_list" width="100%" border="0" align="center" cellpadding="6" cellspacing="0" style="font-size: 10px;">
    <tr>
        <td colspan="4">
            <div align="center"><?= $caption ?></div>
        </td>
    </tr>
    <tr>
        <td bgcolor="#F6F6F6" width="8%"><div align="center">id</div></td>
        <td bgcolor="#F6F6F6" width="10%"><div align="center">API</div></td>
        <td bgcolor="#F6F6F6"><div align="center">參數</div></td>
        <td bgcolor="#F6F6F6" width="30%"><div align="center">結果</div></td>
        <td bgcolor="#F6F6F6" width="10%"><div align="center">IP</div></td>
        <td bgcolor="#F6F6F6" width="8%"><div align="center">時間</div></td>
    </tr>
    <?php foreach ($rows as $id => $row) : ?>
        <tr style="background-color: <?=($id%2==1)?'#fff':"#e1f5d1" ?>" onmouseover="this.style.backgroundColor = '#ccc'" onmouseout="this.style.backgroundColor = '<?=($id%2==1)?'#fff':"#e1f5d1" ?>'">
            <td><div align="center"><?= $row->la_id ?></div></td>
            <td><div align="left"><?= $row->la_api_name ?></div></td>
            <td><div align="left" style="word-break: break-all"><?= htmlspecialchars($row->la_params) ?></div></td>
            <td><div align="left" style="word-break: break-all"><?= htmlspecialchars($row->la_result) ?></div></td>
            <td><div align="center"><?= $row->la_ip ?></div></td>
            <td><div align="center"><?= $row->la_timestamp ?></div></td>
        </tr>
    <?php endforeach; ?>
</table>