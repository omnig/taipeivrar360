<?php
//檢查傳入值start
//處理傳入值
$input_array = array(
    "origin_string" => $this->input->post('origin_string'),
);
foreach ($input_array as $index => $value) {
    if ($value === false || $value === '') {
        $this->api_log("false", "ERR_INVALID_PARAMETER");
    }
}

//檢查傳入值end
//產生新密碼start
//加密
$encoded_origin_string = $this->encrypt_password($input_array["origin_string"]);
?>
<table>
    <tr>
        <td>
            原始明碼：
        </td>
        <td>
            <?= $input_array["origin_string"]; ?>
        </td>
    </tr>
    <tr>
        <td>
            加密字串：
        </td>
        <td>
            <?= $encoded_origin_string; ?>
        </td>
    </tr>
</table>