<?php

//傳入值接收
$input_array = array(
  "text" => $this->input->get('text'),
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}

$image = dirname(dirname(dirname(__DIR__)))."/white.png";

$image = new \NMC\ImageWithText\Image($image);

$text = $input_array['text'];
$text_len = strlen($text);


$text1 = new \NMC\ImageWithText\Text($text, 20, 50);

$text1->align = 'left';
$text1->color = '000000';
$text1->font = dirname(dirname(dirname(__DIR__))).'/NotoSansCJKtc-Medium.otf';
$text1->lineHeight = 36;
$text1->size = 24;
$text1->startX = 380 -$text_len*2;
$text1->startY = 70;
$image->addText($text1);


$time_name = time();


recusive_file_output($image,$time_name,0,2);


//判斷檔案是否存在
function check_file_content($file_path = ""){

	if(!file_exists($file_path)){
		throw new Exception("Mike : Value must be exists");
	}

}

//判斷遞迴限制
function check_recusive_count($counter=0,$max_count=0){
	if($counter>=$max_count){
		throw new Exception("Mike : counter must be lower max_count");
	}
}

//遞迴檢驗輸出圖檔
function recusive_file_output($image,$time_name="",$counter=0,$max_count=0){

	$image->render(dirname(dirname(dirname(__DIR__))) . '/upload/destination'.$time_name.'.png');
	$file_path = dirname(dirname(dirname(__DIR__)))."/upload/destination".$time_name.".png";
	try{

		check_file_content($file_path);
		

		$imageData = base64_encode(file_get_contents($file_path));
		header("Content-type: image/png");
		readfile($file_path);
		unlink(dirname(dirname(dirname(__DIR__)))."/upload/destination".$time_name.".png");
		exit;

	}catch(Exception $e){
		
		$counter++;
		try{
			check_recusive_count($counter,$max_count);
		}catch(Exception $e){
			echo "counter->".$counter."maxcounter->".$max_count;
			// http_response_code(201);
			// exit;
		}
		// usleep(1000000);
		recusive_file_output($image,$time_name,$counter,$max_count);
	}


}



