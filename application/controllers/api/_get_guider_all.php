<?php
//以使用者位置及方圓幾公里內拿AR導引點位資訊
header('Content-Type: application/json');
$input_array = array(
    "user_lat" => $this->input->GET('user_lat'),
    "user_lng" => $this->input->GET('user_lng')
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}
$input_array["radius"] = $this->input->GET('radius')?$this->input->GET('radius'):1;

$topic_result=array();
$ar_result=array();
$poi_result=array();

$topic_result = get_topic($input_array['user_lat'],$input_array['user_lng'],$input_array["radius"]);
$ar_result = get_ar($input_array['user_lat'],$input_array['user_lng'],$input_array["radius"]);
$poi_result = get_poi($input_array['user_lat'],$input_array['user_lng'],$input_array["radius"]);
$view_result = get_view($input_array['user_lat'],$input_array['user_lng'],$input_array["radius"]);

$result["topic"] = $topic_result;
$result["ar"] = $ar_result;
$result["poi"] = $poi_result;
$result["view"] = $view_result;

//顯示AR結果
$ret = array(
    "result" => "true",
    "code" => 200,
    "error_message" => "",
    "data" => $result 
);

echo json_encode($ret);
exit();


function get_topic($user_lat,$user_lng,$radius){

	$CI = & get_instance();
	$result = array();
	//判斷主題沒有被關閉
	//判斷主題沒有被刪除
	$where_array = array(
		't_enabled' => 'Y',
		't_del' => 'N'
	);

	 
	//找尋附近AR_主題
	$topic_result = $CI->Common_model->get_table_point_distance("tp_topic","t_name","t_lat","t_lng",$where_array,$user_lat,$user_lng,$radius);

	foreach ($topic_result as $key => $value) {
		$result[$key] = new stdClass();
		$result[$key]->name = (string)$topic_result[$key]->t_name;
		$result[$key]->lat = (double)$topic_result[$key]->t_lat;
		$result[$key]->lng = (double)$topic_result[$key]->t_lng;
		@$result[$key]->distance = (double)$topic_result[$key]->distance;
	}

	return $result;
}

function get_ar($user_lat,$user_lng,$radius){

	$CI = & get_instance();
	$result = array();
	//判斷主題沒有被關閉
	//判斷主題沒有被刪除
	$where_array = array(
		'tdar_state' => 'Y',
		'tdar_del' => 'N'
	);

	//找尋附近AR_主題
	$ar_result = $CI->Common_model->get_table_point_distance("tp_department_ar","tdar_name","tdar_lat","tdar_lng",$where_array,$user_lat,$user_lng,$radius);

	foreach ($ar_result as $key => $value) {
		$result[$key] = new stdClass();
		$result[$key]->name = (string)$ar_result[$key]->tdar_name;
		$result[$key]->lat = (double)$ar_result[$key]->tdar_lat;
		$result[$key]->lng = (double)$ar_result[$key]->tdar_lng;
		@$result[$key]->distance = (double)$ar_result[$key]->distance;
	}


	return $result;
}


function get_poi($user_lat,$user_lng,$radius){

	$CI = & get_instance();
	$result = array();
	//判斷主題沒有被關閉
	//判斷主題沒有被刪除
	$where_array = array(
		'ap_enabled' => 'Y'
	);

	//找尋附近AR_主題
	$poi_result = $CI->Common_model->get_poi_distance("tp_ap_pois","ap_name","ap_lat","ap_lng",$where_array,$user_lat,$user_lng,$radius,"");

	foreach ($poi_result as $key => $value) {
		$result[$key] = new stdClass();
		$result[$key]->name = (string)$poi_result[$key]->ap_name;
		$result[$key]->lat = (double)$poi_result[$key]->ap_lat;
		$result[$key]->lng = (double)$poi_result[$key]->ap_lng;
		$ac_image = json_decode($poi_result[$key]->ac_image);
		$result[$key]->image = $CI->config->item('server_base_url')."upload/ap_category/".@$ac_image->s100;

		@$result[$key]->distance = (double)$poi_result[$key]->distance;
	}


	return $result;
}

function get_view($user_lat,$user_lng,$radius){

	$CI = & get_instance();
	$result = array();
	//判斷景點沒有被關閉
	//判斷景點沒有被刪除
	$where_array = array(
		'v_enabled' => 'Y',
		'v_del' => 'N'
	);

	 
	//找尋附近AR_主題
	$topic_result = $CI->Common_model->get_view_distance("tp_viewpoints","v_name","v_lat","v_lng",$where_array,$user_lat,$user_lng,$radius,"");

	foreach ($topic_result as $key => $value) {
		$result[$key] = new stdClass();
		$result[$key]->name = (string)$topic_result[$key]->v_name;
		$result[$key]->lat = (double)$topic_result[$key]->v_lat;
		$result[$key]->lng = (double)$topic_result[$key]->v_lng;
		$result[$key]->recommand = $topic_result[$key]->v_recommand;
		@$result[$key]->distance = (double)$topic_result[$key]->distance;
	}

	return $result;
}