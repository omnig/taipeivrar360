<?php
header('Content-Type: application/json');

//局處列表
$where_array = array(
	'ag_enabled' => "Y"
);

$department_list = $this->Common_model->get_db("tp_admin_group", $where_array,'ag_order');

foreach ($department_list as $key => $value) {
	$data["department_list"][$value->ag_id] = $value->ag_name;
}


// echo json_encode($department_list);
// exit;


foreach ($department_list as $result_key => $result_value) {


	$response_data[$result_key]=array(
		"id" => $result_value->ag_id,//ID
		"name" => $result_value->ag_name, //名稱
		"order" => $result_value->ag_order, //排序
	);
}


//顯示呼叫結果
$ret = array(
    "result" => "true",
    "code" => 200,
    "error_message" => "",
    "data" => $response_data
);

echo json_encode($ret);
exit();

