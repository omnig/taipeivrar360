<?php
header('Content-Type: application/json');

//局處列表
$where_array = array();

$department_list = $this->Common_model->get_db("tp_admin_group", $where_array);

foreach ($department_list as $key => $value) {
	$data["department_list"][$value->ag_id] = $value->ag_name;
}


//資料表名稱
$data["table_name"] = "topic_manage";

$model_name = ucfirst($data['table_name']) . '_model';
$this->load->model($model_name);


$order_by = 'field(t_id,117,50,48,49,44,47)';

$input_array['limit'] = 0;

$input_array['skip'] = 0;

$where_array = array(
	't_id' => array(117,50,48,49,44,47),
	't_enabled' => 'Y',
	't_del' => 'N'
);

//從資料庫取得本次資料
$result = $this->$model_name->get_list($order_by, '', $input_array['limit'], $input_array['skip'], $where_array, false);
$response_data = array();


// echo json_encode($result);
// exit;

foreach ($result as $result_key => $result_value) {

	$result_value->t_organizer = $data["department_list"][$result_value->t_organizer];
	$result_value->t_co_organizer =  explode(',',$result_value->t_co_organizer);

	
	foreach ($result_value->t_co_organizer as $co_key => $co_value) {
		$result_value->t_co_organizer[$co_key] =  $data["department_list"][$result_value->t_co_organizer[$co_key]];
	}

	$response_data[$result_key]=array(
		"t_id" =>$result_value->t_id, //主題ID
		"t_name" => $result_value->t_name, //主題名稱
		"t_image" => $this->config->item('server_base_url')."upload/topic_image/".$result_value->t_image, //主題圖片
		"t_create_timestamp" =>  date('d M Y H:i',strtotime($result_value->t_create_timestamp))." UTC"
		
	);
}


//顯示呼叫結果
$ret = array(
    "result" => "true",
    "code" => 200,
    "error_message" => "",
    "data" => $response_data
);

echo json_encode($ret);
exit();

