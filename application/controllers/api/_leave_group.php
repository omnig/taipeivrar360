<?php

//傳入值接收
$input_array = array(
    "timestamp" => $this->input->post("timestamp"),
    "mac" => $this->input->post("mac"),
    "key" => $this->input->post("key"),
    "device_id" => $this->input->post("device_id")
);


foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}


//驗證mac
if (!$this->verify_mac($input_array["timestamp"], $input_array["mac"])) {
    $this->log($page, "ACCESS_DENY", 'N');
}

$input_array["login_token"] = $this->input->post("login_token");

//檢查使用者是否存在
$user = '';
if ($input_array["login_token"]) {
    $where_array = array(
        'u_login_token' => $input_array['login_token'],
        'u_device_id' => $input_array['device_id']
    );
    $user = $this->Common_model->get_one('user', $where_array);
    if (!$user) {
        $this->log($page, "INVALID_USER", 'N');
    }
}


//檢查key及裝置是否存在
$where_array = array(
    "g_key" => $input_array["key"],
    "gr_device_id" => $input_array["device_id"]
);
if ($user) {
    $where_array['gr.u_id'] = $user->u_id;
}
$group = $this->Common_model->get_db_join("group as g", $where_array, 'group_relation as gr', 'g.g_id=gr.g_id');

if (!$group) {
    $this->log($page, "INVALID KEY or DEVICE_ID", 'N');
    exit();
}

//紀錄刪除使用者
$update_array = array(
    "gr_del" => "Y"
);
$where_array = array(
    "g_id" => $group[0]->g_id,
    "gr_device_id" => $input_array["device_id"]
);
if ($user) {
    $where_array['u_id'] = $user->u_id;
}
$row = $this->Common_model->update_db("group_relation", $update_array, $where_array);

if ($row == 0) {
    $result = "false";
    $error_msg = "ALREADY LEFT";
    $msg = "";
} else if ($row > 0) {
    $result = "true";
    $error_msg = "";
    $msg = "SUCCESS";
} else {
    $result = "false";
    $error_msg = "FAILD";
    $msg = "";
}

$data = array();

//顯示呼叫結果
$ret = array(
    "result" => $result,
    "error_message" => $error_msg,
    "msg" => $msg,
    "data" => $data
);

echo json_encode($ret);


$this->log($page, json_encode($ret), 'Y');

exit();
