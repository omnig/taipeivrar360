<?php

header('Content-Type: application/json');
/*
-回傳任務內所有關卡狀態

--------------------------
example API : 
http://nmp.utobonus.com/api/get_nine_grid?m_id=5&u_id=4

輸入參數
key1 "m_id" ->  任務ID
value1 (int) 

key2 "u_id" ->  使用者ID
value2 (int) 


回傳內容

"grid_now": 0,   (0代表未開始玩,1代表已開始完第一關)
"is_finish": "None", (None 代表未完成 , Done 代表已完成)
"nine_grid" array(
  "ng_title": "關卡名稱1",   (關卡名稱)
  "p_id": "140",  (POIID)
  "ng_trigger": "Near_trigger", (觸發模式)
  "ng_order": "1"  (關卡排序)
)


*/


//傳入值接收
$input_array = array(
    "m_id" => $this->input->get('m_id'),
    "u_id" => $this->input->get('u_id')
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}

//取得任務列表
$where_array = array(
    'm_id' => $input_array['m_id'],
    'm_enabled' => "Y"
);
$this->load->model("Mission_model");
$mission = $this->Mission_model->get_list("", "", 0, 0, $where_array);


$where_array = array(
    'u_id' => $input_array["u_id"]
);
$user = $this->Mission_model->get_mission_user("", "", 0, 0, $where_array);


//載入使用者與任務資訊
foreach ($mission as $key => $value) {
    if (count($user) > 0) {
        foreach ($user as $user_key => $user_value) {
            if ($user_value->m_id == $value->m_id) {
                $mission[$key]->grid_now = (int)$user_value->grid_now;
                $mission[$key]->pass_grids = explode(",", $user_value->pass_grids);
                $mission[$key]->is_finish = $user_value->is_finish == "Done" ? true : false;
                $mission[$key]->rws_enabled = $user_value->rws_enabled;
                break;
            } else {
                $mission[$key]->grid_now = 1;
                $mission[$key]->pass_grids = [];
                $mission[$key]->is_finish = false;
                $mission[$key]->rws_enabled = "None";
            }
        }
    } else {
        $mission[$key]->grid_now = 1;
        $mission[$key]->pass_grids = [];
        $mission[$key]->is_finish = false;
        $mission[$key]->rws_enabled = "None";
    }
}


foreach ($mission as $key => $value) {
    unset($mission[$key]->m_img);
    unset($mission[$key]->m_id);
    unset($mission[$key]->m_start_time);
    unset($mission[$key]->m_enabled);
    unset($mission[$key]->rw_id);
    unset($mission[$key]->rw_img);
    unset($mission[$key]->rw_describe);
    unset($mission[$key]->rw_num);
}


//取得任務列表
$where_array = array(
    "m_id" => $input_array['m_id'],
    "ng.ap_id is not null"
);

$this->load->model("Nine_grid_model");
$nine_grid = $this->Nine_grid_model->get_list("ng_order", "", 0, 0, $where_array);
$nine_list = [];
$grid_now = (int)$mission[0]->grid_now;
$pass_grids = $mission[0]->pass_grids;

$this->load->model("Question_model");

$CI = &get_instance();
foreach ($nine_grid as $key => $value) {
    if (empty($value->ap_id)) {
        continue;
    } else {
        $poi = array(
            "id" => $value->ap_id,
            "name" => $value->ap_name,
            "desc" => $value->ap_desc,
            "locid" => ($value->ap_locid) ? $value->ap_locid : "",
            "enabled" => $value->ap_enabled,
            "lat" => $value->ap_lat,
            "lng" => $value->ap_lng,
            "hyperlink_text" => $value->ap_hyperlink_text,
            "hyperlink_url" => $value->ap_hyperlink_url,
            "video_hyperlink" => $value->ap_video_hyperlink,
            "audio_path" =>  is_null($value->ap_audio_path) ? "" : $CI->config->item("server_base_url") . "/upload/audio/" . $value->ap_audio_path,
            "image" => $CI->config->item("server_base_url") . "/upload_image/" . $value->ap_image_path,
            "category" => [
                "id" => $value->ac_id,
                "title" => $value->ac_title_zh,
                "image" => empty($value->ac_image) ? null : $CI->config->item(
                        "server_base_url"
                    ) . "/upload/ap_category/" . json_decode($value->ac_image)->s100,
            ],
            "ar_trigger" => [
                "active_method" => $value->ap_ar_active_method,
                "distance" => $value->ap_ar_distance,
                "identify_image_path" => $CI->config->item(
                        "server_base_url"
                    ) . "/upload_image/" . $value->dai_image_path,
            ],
            "ar" => [
                "text" => $value->ap_ar_text,
                "url" => $value->ap_ar_url,
                "heigh" => $value->ap_ar_heigh,
                "content_type" => $value->tdar_content_type,
                // "content" => $CI->config->item("server_base_url")."/upload/image/".$value->tdar_content_path,
                "interactive_text" => $value->tdar_interactive_text,
                "interactive_url" => $value->tdar_interactive_url,
                "cover_identify_image" => $value->ap_cover_identify_image,
                "size" => number_format((float)$value->ap_ar_vsize/100,2)
            ]
        );
		if(is_null($value->b_id)){
			$poi["beacon"] = null;
		}else{
			$poi["beacon"] = [
				"id" => $value->b_id,
				"description" => $value->b_description,
				"major" => $value->b_major,
				"minor" => $value->b_minor,
                "hwid" => $value->b_hwid,
				"mac" => $value->b_mac,
				"lat" => $value->b_lat,
				"lng" => $value->b_lng,
				"range" => $value->b_range,
			];
		}
        switch ($value->tdar_content_type) {
            case 'text':
                $poi["ar"]["content"] = $value->tdar_content_path;

                //產生文字圖片
                $resulr_url = $CI->config->item(
                        'server_base_url'
                    ) . "/api/get_text_image?text=" . $value->tdar_content_path;
                //$resulr_url = curl_query($url);
                $poi["ar"]["url_image"] = $resulr_url;

                break;
            case 'image':
                $poi["ar"]["content"] = $CI->config->item(
                        'server_base_url'
                    ) . "/upload/image/" . $value->tdar_content_path;
                break;
            case 'youtube':
                $poi["ar"]["content"] = $value->tdar_content_path;
                break;
            case '3d_model':
                $poi["ar"]["content"] = $CI->config->item(
                        'server_base_url'
                    ) . "/upload_model/" . $value->tdar_content_path;
                $poi["ar"]["ios"] = $CI->config->item('server_base_url') . "/upload_model/" . $value->tdar_content_path;
                $poi["ar"]["android"] = $CI->config->item('server_base_url') . $value->tdar_content_wt3;

                break;
            case 'video':
                $poi["ar"]["content"] = $CI->config->item(
                        'server_base_url'
                    ) . "/upload_video/" . $value->tdar_content_path;
                $poi["ar"]["isTransparent"] = $value->tdar_is_transparent;
                break;
        }

        $question = null;
        $question_list = $this->Question_model->get_list("q_modify_timestamp DESC", "", 0, 0, ['ng_id' => $value->ng_id]);
        if(count($question_list)>0){
            if($question_list[0]->q_style==='是非'){
                $question = [
                    'style'=>$question_list[0]->q_style,
                    'type'=>$question_list[0]->qb_style,
                    'title'=>$question_list[0]->qb_title,
                    'image'=>$question_list[0]->qb_image,
                    'video'=>$question_list[0]->qb_video,
                    'answer'=>$question_list[0]->qb_answer,
                    'tip'=>$question_list[0]->qb_tip,
                ];
            }
            if($question_list[0]->q_style==='單選'){
                $question = [
                    'style'=>$question_list[0]->q_style,
                    'type'=>$question_list[0]->qs_style,
                    'title'=>$question_list[0]->qs_title,
                    'image'=>$question_list[0]->qs_image,
                    'video'=>$question_list[0]->qs_video,
                    'option1'=>$question_list[0]->qs_option1,
                    'option2'=>$question_list[0]->qs_option2,
                    'option3'=>$question_list[0]->qs_option3,
                    'option4'=>$question_list[0]->qs_option4,
                    'answer'=>$question_list[0]->qs_answer,
                    'tip'=>$question_list[0]->qs_tip,
                ];
            }
        }

        $nine_list[$key] = [
            "id" => $value->ng_id,
            "title" => $value->ng_title,
            "description" => $value->ng_describe,
            "notice" => $value->ng_notice,
            'trigger_distance' => $value->ng_trigger_distance,
            'pass_method' => $value->ng_pass_method,
            'is_beacon_first' => ($value->ng_is_beacon_first==='1'),
            "poi" => $poi,
            'question'=>$question,
            "is_complete" => $mission[0]->is_finish === "Done" ? true : (in_array(
                $value->ng_id,
                $pass_grids
            ) ? true : false),

        ];
    }
    // // =========================================================================
    // $value->ar_imag = ($value->tdar_image_path)?$this->config->item("server_base_url")."/upload_image/".$value->tdar_image_path:"";
    // $value->tdar_image_path = str_replace(".jpg", "", $value->tdar_image_path);

    // //新增AR ID
    // $value->ar_id = $value->tdar_id;

    // if (empty($value->ap_id)) {
    //   $value->ng_title = null;
    //   $value->ar_id = null;
    //   $value->ap_id = null;
    //   $value->ng_trigger = null;
    //   $value->ng_order = null;
    //   $value->tdar_image_path=null;
    // }
    // unset($value->ng_id);
    // unset($value->m_id);
    // unset($value->ng_img);

    // $value->ng_complete = ($key+1 < $grid_now)?TRUE:FALSE;

    // if ($mission[0]->is_finish=="Done") {
    //   $value->ng_complete = TRUE;
    // }

}


// 獲取九宮格的AR圖形

if($mission[0]->is_finish && $mission[0]->rws_enabled === 'None'){
    if($mission[0]->rw_end_time < date('Y-m-d 00:00:00')){
        $mission[0]->rws_enabled = 'Expired';
    }
}

//回傳欄位
$result = [
    "grid_now" => (int)$mission[0]->grid_now,
    "is_finish" => $mission[0]->is_finish,
    "nine_grid" => $nine_list,
    "mission_title" => $mission[0]->m_title,
    "mission_describe" => $mission[0]->m_describe,
    "mission_end_time" => $mission[0]->m_end_time,
    "rws_enabled" => $mission[0]->rws_enabled,
    "reward_name" => $mission[0]->rw_title,
    "reward_remainng" => $mission[0]->rw_remainng,
];

//顯示呼叫結果
$ret = array(
    "result" => "true",
    "error_message" => "",
    "data" => $result
);

$input_array['format'] = $this->input->post('format') ? strtolower($this->input->post('format')) : 'json';

if ($input_array['format'] == 'xml') {
    $xml = array_to_xml($ret, false);

    header('Content-Type: application/xml');
    echo $xml;
} else {
    echo json_encode($ret);
}

$this->log($page, '', 'Y');

exit();
