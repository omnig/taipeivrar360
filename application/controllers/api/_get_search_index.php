<?php
header('Content-Type: application/json');
//載入Model
$this->load->model("Index_search_model");

$input_array = array(
    "keyword" => $this->input->GET('k'),
    "order" =>$this->input->GET('order'),  //hot or new
    "department_id" => $this->input->GET('d')
);

//檢查要送進資料庫的查詢是否超過字數
if(!valid_string_lenth($input_array['keyword'],10)){
	
    $this->log($page, "INVALID_PARAMETER", 'N');
    echo json_encode($ret);
    exit();
}
//資料總筆數

$result['recordsTotal'] = 0;

if($this->input->GET('type')==""||$this->input->GET('type')==NULL){
	$input_array["type"] = "ALL";
}else{
	$input_array["type"] = $this->input->GET('type');
}



if (isset($input_array['dapartment_id'])&&$input_array['dapartment_id']!=NULL && isset($input_array['keyword'])&&$input_array['dapartment_id']!=NULL) {
// echo json_encode("dk");
// exit;
//先判斷局處是否存在
//先判斷關鍵字是否存在

//判斷是否增加排序條件
$order = $input_array['order']=="hot"?"hot":"new";

$keyword = ($input_array['keyword'])?$input_array['keyword']:"";
$department_id = ($input_array['department_id'])?$input_array['department_id']:"";




if($input_array["type"]=="ALL"){
	$result['topic'] = get_topic($keyword,$order,$department_id);
	$result['vr_photo'] = get_vr_photo($keyword,$order,$department_id);
	$result['vr_video'] = get_vr_video($keyword,$order,$department_id);
	$result['recordsTotal'] = count($result['topic']) + count($result['vr_photo']) + count($result['vr_video']);
}elseif($input_array["type"]=="topic"){
	$result = get_topic($keyword,$order,$department_id);
}elseif($input_array["type"]=="vr_photo"){
	$result = get_vr_photo($keyword,$order,$department_id);
}elseif($input_array["type"]=="vr_video"){
	$result = get_vr_video($keyword,$order,$department_id);
}


}elseif(isset($input_array['department_id'])&&$input_array['department_id']!=NULL){
//先判斷局處是否存在，如果沒有顯示全部
// echo json_encode("d");
// exit;
//判斷是否增加排序條件
$order = $input_array['order']=="hot"?"hot":"new";
$department_id = ($input_array['department_id'])?$input_array['department_id']:"";

$keyword = ($input_array['keyword'])?$input_array['keyword']:"";


if($input_array["type"]=="ALL"){
	$result['topic'] = get_topic($keyword,$order,$department_id);
	$result['vr_photo'] = get_vr_photo($keyword,$order,$department_id);
	$result['vr_video'] = get_vr_video($keyword,$order,$department_id);
	$result['recordsTotal'] = count($result['topic']) + count($result['vr_photo']) + count($result['vr_video']);
}elseif($input_array["type"]=="topic"){
	$result = get_topic($keyword,$order,$department_id);
}elseif($input_array["type"]=="vr_photo"){
	$result = get_vr_photo($keyword,$order,$department_id);
}elseif($input_array["type"]=="vr_video"){
	$result = get_vr_video($keyword,$order,$department_id);
}



}elseif(isset($input_array['keyword'])&&$input_array['keyword']!=NULL){
//先判斷關鍵字是否存在
// echo json_encode("k");
// exit;
//判斷是否增加排序條件
$order = $input_array['order']=="hot"?"hot":"new";

$keyword = ($input_array['keyword'])?$input_array['keyword']:"";
$department_id = "";


if($input_array["type"]=="ALL"){
		$result['topic'] = get_topic($keyword,$order,$department_id);
		$result['vr_photo'] = get_vr_photo($keyword,$order,$department_id);
		$result['vr_video'] = get_vr_video($keyword,$order,$department_id);
		$result['recordsTotal'] = count($result['topic']) + count($result['vr_photo']) + count($result['vr_video']);
	}elseif($input_array["type"]=="topic"){
		$result = get_topic($keyword,$order,$department_id);
	}elseif($input_array["type"]=="vr_photo"){
		$result = get_vr_photo($keyword,$order,$department_id);
	}elseif($input_array["type"]=="vr_video"){
		$result = get_vr_video($keyword,$order,$department_id);
	}

}elseif(isset($input_array['type'])&&$input_array['type']!=NULL){
//先判斷關鍵字是否存在
// echo json_encode("k");
// exit;
//判斷是否增加排序條件
$order = $input_array['order']=="hot"?"hot":"new";

$keyword = ($input_array['keyword'])?$input_array['keyword']:"";
$department_id = "";


if($input_array["type"]=="ALL"){
		$result['topic'] = get_topic($keyword,$order,$department_id);
		$result['vr_photo'] = get_vr_photo($keyword,$order,$department_id);
		$result['vr_video'] = get_vr_video($keyword,$order,$department_id);
		$result['recordsTotal'] = count($result['topic']) + count($result['vr_photo']) + count($result['vr_video']);
	}elseif($input_array["type"]=="topic"){
		$result = get_topic($keyword,$order,$department_id);
	}elseif($input_array["type"]=="vr_photo"){
		$result = get_vr_photo($keyword,$order,$department_id);
	}elseif($input_array["type"]=="vr_video"){
		$result = get_vr_video($keyword,$order,$department_id);
	}

}



else{
//顯示預設全部	
// echo json_encode("else");
// exit;
$result['topic'] = get_topic("","","");
$result['vr_photo'] = get_vr_photo("","","");
$result['vr_video'] = get_vr_video("","","");
$result['recordsTotal'] = count($result['topic']) + count($result['vr_photo']) + count($result['vr_video']);

}


//顯示所有結果
$ret = array(
    "result" => "true",
    "code" => 200,
    "error_message" => "",
    "data" => $result 
);

echo json_encode($ret);
exit();


function get_topic($keyword="",$order="hot",$department_id){
	$CI = & get_instance();
	//找尋附近主題
	//判斷主題沒有被關閉
	//判斷主題沒有被刪除

	//排序規則
	$order= ($order=="hot")?"t_views":"t_update_timestamp";
	
	//如果有加入局處搜尋條件
	if ($department_id!="" || $department_id!=NULL) {
		$where_array = array(
			't_organizer' => $department_id,
			't_enabled' => 'Y',
			't_del' => 'N'
		);
	}else{
		$where_array = array(
			't_enabled' => 'Y',
			't_del' => 'N'
		);
	}

	
	$topic_result = $CI ->Index_search_model->get_topic_list($order,$keyword,0,0,$where_array);


	//做資料整理
	foreach ($topic_result as $key => $value) {
		@$topic[$key]->id = $value->t_id;
		@$topic[$key]->name = $value->t_name;
		@$topic[$key]->description = $value->t_describe;
		@$topic[$key]->img = $CI ->config->item('server_base_url')."upload/topic_image/".$value->t_image;
		@$topic[$key]->lat = (double)$value->t_lat;
		@$topic[$key]->lng = (double)$value->t_lng;
		@$topic[$key]->web = $CI ->config->item("server_base_url")."#";
		@$topic[$key]->upload_date = $value->t_update_timestamp;
		@$topic[$key]->view = $value->t_views;

		$where_array = array(
	    "t_id" => @$topic[$key]->id
		);
		$keywords_ary = array();
		$keywords = $CI ->Common_model->get_db_join("keyword_topic as kt", $where_array , "keywords as ks" , "kt.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");

		foreach ($keywords as $keywords_key => $keywords_value) {

			if($keywords_value->kwd_name!=NULL && $keywords_value->kwd_name!="" ){
			    $keywords_ary[$keywords_key] = $keywords_value->kwd_name;
			}
		}

		@$topic[$key]->keywords = $keywords_ary;
	}

	

	//輸出結果
	$result = array();
	if(isset($topic)){
		$result = $topic;
	}
	return $result;
}


function get_vr_photo($keyword="",$order="hot",$department_id){
	$CI = & get_instance();
	//找尋附近主題
	//判斷主題沒有被關閉
	//判斷主題沒有被刪除

	//排序規則
	$order= ($order=="hot")?"dvp_collection":"dvp_create_timestamp";
	
	//如果有加入局處搜尋條件
	if ($department_id!="" || $department_id!=NULL) {
		$where_array = array(
			't_organizer' => $department_id,
			'dvp_state' => 'Y',
			'dvp_del' => 'N'
		);
	}else{
		$where_array = array(
			'dvp_state' => 'Y',
			'dvp_del' => 'N'
		);
	}
	
	$vr_photo_result = $CI ->Index_search_model->get_vr_photo_list($order,$keyword,0,0,$where_array);



	//做資料整理
	foreach ($vr_photo_result as $key => $value) {
		@$vr_photo[$key]->id = $value->dvp_id;
		@$vr_photo[$key]->name = $value->dvp_name;
		@$vr_photo[$key]->img = $CI ->config->item('server_base_url').$value->dvp_rep_img_path;
		@$vr_photo[$key]->show_image = $CI->config->item('server_base_url').'api/vp_redirect?id='.$value->dvp_id;
		@$vr_photo[$key]->show_image_mobile = $CI->config->item('server_base_url').'api/vp_redirect?id='.$value->dvp_id.'&device=m';
		@$vr_photo[$key]->lat = (double)$value->dvp_lat;
		@$vr_photo[$key]->lng = (double)$value->dvp_lng;
		@$vr_photo[$key]->upload_date = $value->dvp_create_timestamp;
		@$vr_photo[$key]->view = $value->dvp_collection;

		$where_array = array(
		    "dvp_id" => @$vr_photo[$key]->id
		);
		$keywords_ary = array();
		$keywords = $CI ->Common_model->get_db_join("keyword_department_vr_photo as kvp", $where_array , "keywords as ks" , "kvp.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");

		foreach ($keywords as $keywords_key => $keywords_value) {

			if($keywords_value->kwd_name!=NULL && $keywords_value->kwd_name!="" ){
			    $keywords_ary[$keywords_key] = $keywords_value->kwd_name;
			}
		}

		@$vr_photo[$key]->keywords = $keywords_ary;
	}

	

	//輸出結果
	$result = array();
	if(isset($vr_photo)){
		$result = $vr_photo;
	}
	return $result;
}

function get_vr_video($keyword="",$order="hot",$department_id){
	$CI = & get_instance();
	//找尋附近主題
	//判斷主題沒有被關閉
	//判斷主題沒有被刪除

	//排序規則
	$order= ($order=="hot")?"dvv_collection":"dvv_create_timestamp";
	
	//如果有加入局處搜尋條件
	if ($department_id!="" || $department_id!=NULL) {
		$where_array = array(
			't_organizer' => $department_id,
			'dvv_state' => 'Y',
			'dvv_del' => 'N'
		);
	}else{
		$where_array = array(
			'dvv_state' => 'Y',
			'dvv_del' => 'N'
		);
	}
	$vr_video_result = $CI ->Index_search_model->get_vr_video_list($order,$keyword,0,0,$where_array);

	// echo json_encode($vr_photo_result);
	// exit;

	//做資料整理
	// foreach ($topic_result as $key => $value) {
	// 	//加入網頁連結
	// 	// $topic_result[$key]->web = $CI ->config->item("server_base_url")."#";
	// }

	//做資料整理
	foreach ($vr_video_result as $key => $value) {
		@$vr_video[$key]->id = $value->dvv_id;
		@$vr_video[$key]->name = $value->dvv_name;
		@$vr_video[$key]->img = $CI ->config->item('server_base_url').$value->dvv_rep_img_path;
		@$vr_video[$key]->show_video = $CI->config->item('server_base_url').'api/vv_redirect?id='.$value->dvv_id;
		@$vr_video[$key]->show_video_mobile = $CI->config->item('server_base_url').'api/vv_redirect?id='.$value->dvv_id.'&device=m';
		@$vr_video[$key]->lat = (double)$value->dvv_lat;
		@$vr_video[$key]->lng = (double)$value->dvv_lng;
		@$vr_video[$key]->upload_date = $value->dvv_create_timestamp;
		@$vr_video[$key]->view = $value->dvv_collection;

		$where_array = array(
		    "dvv_id" => @$vr_video[$key]->id
		);
		$keywords_ary = array();
		$keywords = $CI ->Common_model->get_db_join("keyword_department_vr_video as kvp", $where_array , "keywords as ks" , "kvp.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");

		foreach ($keywords as $keywords_key => $keywords_value) {

			if($keywords_value->kwd_name!=NULL && $keywords_value->kwd_name!="" ){
			    $keywords_ary[$keywords_key] = $keywords_value->kwd_name;
			}
		}

		@$vr_video[$key]->keywords = $keywords_ary;
	}

	

	//輸出結果
	$result = array();
	if(isset($vr_video)){
		$result = $vr_video;
	}
	return $result;
}
