<?php
header('Content-Type: application/json; charset=utf-8');

//傳入值接收
$input_array = array(
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}

$update_array = "";

// 上傳圖片
if (isset($_FILES['user_image']) AND @$_FILES['user_image']["name"]!="" ) {

    if ($_FILES['user_image']['size']<50000000 && $_FILES['user_image']['type']=="image/jpeg" || $_FILES['user_image']['type']=="image/jpg"|| $_FILES['user_image']['type']=="image/png") {
    }else{
        $this->log($page, "user_image file type or size not allowed", 'N');
    }

    //上傳影片代表圖片處理
    $upload_path = "upload/image/";
    $file_type = 'jpg|jpeg|png';
    $max_size = 5120;
    $resize_width = 8000;
    $resize_height = 8000;
    $fit_type = "FIT_INNER_NO_ZOOM_IN";
    $max_width = 3264;
    $max_height = 2448;
    //上傳圖片
    $field_name = "user_image";
    $upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);
    if (!isset($upload_result["upload_data"])) {
        //必須上傳圖片
        $this->log($page, "圖片上傳失敗或沒有上傳圖片", 'N');
        exit();
    }
    $update_array = $this->config->item("server_base_url").$upload_path.$upload_result["upload_data"]["file_name"];


}else{
    $this->log($page, "圖片上傳失敗或沒有上傳圖片", 'N');
    exit();
}

$ret = array(
    "result" => "true",
    "error_message" => "",
    "data" => $update_array
);

$this->log($page, json_encode($ret), 'Y');

echo json_encode($ret);
exit;