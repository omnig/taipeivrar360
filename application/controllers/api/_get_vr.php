<?php
header('Content-Type: application/json');




//VR Photo

$vr_photo_list = array();
$vr_photo_result = array();

$where_array = array(
	'dvp_state' => 'Y',
	'dvp_del' => 'N'
);

$vr_photo_list = $this->Common_model->get_db("department_vr_photo", $where_array,"dvp_create_timestamp","DESC",6);

//資料整理
foreach ($vr_photo_list as $key => $value) {
	$vr_photo_result[$key]['id'] =  $value->dvp_id;
	$vr_photo_result[$key]['name'] =  $value->dvp_name;
	$vr_photo_result[$key]['photo'] =  $this->config->item('server_base_url').$value->dvp_rep_img_path;

	//判斷照片為環景或全景 URL異動
	if ($value->photo_type=="pano") {
		//環景圖
		$vr_photo_result[$key]['url'] =  $value->dvp_view_link;
	}else{
		//全景圖
		$vr_photo_result[$key]['url'] =  $this->config->item('server_base_url').'/kr_pano?image_path='.$this->config->item('server_base_url').$value->dvp_backup;
	}

	
	if($value->Is_url=="Y"){
        $vr_photo_result[$key]['url'] = $value->dvp_view_link;
        $vr_photo_result[$key]['mobile_url'] = $value->dvp_view_link;
    }

	$vr_photo_result[$key]['capture_time'] =  $value->dvp_capture_timestamp;

	//判斷是為局處或是民眾  使用者名稱異動
	if ($value->is_civil=="Y") {
		//民眾
		$where_array = array(
			'u_id' => $value->d_id
		);

		$the_user = $this->Common_model->get_one("user",$where_array);

		if($the_user){
			$vr_photo_result[$key]['uploader'] =  $the_user->u_name;
		}else{
			$vr_photo_result[$key]['uploader'] =  "Other";
		}

		
	}else{
		//局處

		$where_array = array(
			'ag_id' => $value->d_id
		);

		$the_department= $this->Common_model->get_one("tp_admin_group",$where_array);

		if($the_department){
			$vr_photo_result[$key]['uploader'] =  $the_department->ag_name;
		}else{
			$vr_photo_result[$key]['uploader'] =  "Other";
		}

	}

	//環景KeyWord 
		$where_array = array(
    "dvp_id" => $value->dvp_id
	);

	//get_key_word
	$keywords = $this->Common_model->get_db_join("keyword_department_vr_photo as dvp", $where_array , "keywords as ks" , "dvp.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");
	$kwd_names = array();
	foreach ($keywords as $kwd_key => $kwdvalue) {
		if ($kwdvalue->kwd_name!="") {
			$kwd_names[$kwd_key] = $kwdvalue->kwd_name;
		}
	}
	$vr_photo_result[$key]['keywords'] =$kwd_names;

}



//VR Video

$vr_video_list = array();
$vr_video_result = array();

$where_array = array(
	'dvv_state' => 'Y',
	'dvv_del' => 'N'
);

$vr_video_list = $this->Common_model->get_db("tp_department_vr_video", $where_array,"dvv_create_timestamp","DESC",6);

//資料整理
foreach ($vr_video_list as $key => $value) {
	$vr_video_result[$key]['id'] =  $value->dvv_id;
	$vr_video_result[$key]['name'] =  $value->dvv_name;
	$vr_video_result[$key]['photo'] =  $this->config->item('server_base_url').$value->dvv_rep_img_path;
	$web_link = str_replace("https://www.youtube.com/watch?v=", "", $value->dvv_youtube_link);
    $vr_video_result[$key]['dvv_web_video'] =  "https://www.youtube.com/embed/".$web_link;
    $mobile_link = str_replace("https://www.youtube.com/watch?v=", "", $value->dvv_youtube_link);
    $vr_video_result[$key]['dvv_mobile_video'] =  "vnd.youtube:".$mobile_link;
	

	
	$vr_video_result[$key]['capture_time'] = date('d M Y H:i',strtotime($value->dvv_capture_timestamp))." UTC";

	//判斷是為局處或是民眾  使用者名稱異動
	if ($value->is_civil=="Y") {
		//民眾
		$where_array = array(
			'u_id' => $value->d_id
		);

		$the_user = $this->Common_model->get_one("user",$where_array);

		$vr_video_result[$key]['uploader'] =  $the_user->u_name;
	}else{
		//局處

		$where_array = array(
			'ag_id' => $value->d_id
		);

		$the_department= $this->Common_model->get_one("tp_admin_group",$where_array);

		if($the_department){
			$vr_video_result[$key]['uploader'] =  $the_department->ag_name;
		}else{
			$vr_video_result[$key]['uploader'] =  "Other";
		}
		

	}

	//環景KeyWord 
	$where_array = array(
	    "dvv_id" => $value->dvv_id
	);

	//get_key_word
	$keywords = $this->Common_model->get_db_join("keyword_department_vr_video as dvv", $where_array , "keywords as ks" , "dvv.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");
	$kwd_names = array();
	foreach ($keywords as $kwd_key => $kwdvalue) {
		if ($kwdvalue->kwd_name!="") {
			$kwd_names[$kwd_key] = $kwdvalue->kwd_name;
		}
	}
	$vr_video_result[$key]['keywords'] =$kwd_names;

}

$result = array(
	"vr_photo" => $vr_photo_result,
	"vr_video" => $vr_video_result
);

//顯示呼叫結果
$ret = array(
    "result" => "true",
    "code" => 200,
    "error_message" => "",
    "data" => $result
);

//沒有結果
if(count($result)<=0){
	http_response_code(400);
	$ret = array(
	    "result" => "false",
	    "code" => 400,
	    "error_message" => "None Data",
	    "data" => array()
	);
}

echo json_encode($ret);
exit();