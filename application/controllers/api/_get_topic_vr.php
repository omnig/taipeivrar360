<?php
header('Content-Type: application/json');

$input_array = array(
    "topic_id" => $this->input->GET('topic_id')
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}



$where_array = array(
	'dvp.t_id'=>$input_array['topic_id'],
	'dvp_state' => 'Y',
	'dvp_del' => 'N'
);


$vr_photo =array();
$vr_video =array();

$this->load->model("Department_vr_photo_model");

$vr_photo_list = $this->Department_vr_photo_model->get_list('', '', 6, 0, $where_array);


foreach ($vr_photo_list as $p_key => $p_value) {
	@$vr_photo[$p_key]->dvp_id = $p_value->dvp_id;
	@$vr_photo[$p_key]->dvp_name = $p_value->dvp_name;
	@$vr_photo[$p_key]->dvp_user_name = $p_value->u_name;

	//局處顯示不一樣
	if ($p_value->is_civil=="N") {
		@$vr_photo[$p_key]->dvp_user_name = $p_value->ag_name;
	}
	
	@$vr_photo[$p_key]->dvp_image = $this->config->item('server_base_url').$p_value->dvp_rep_img_path;
	@$vr_photo[$p_key]->dvp_show_image = $this->config->item('server_base_url').'api/vp_redirect?id='.$p_value->dvp_id;
	@$vr_photo[$p_key]->dvp_date = $p_value->dvp_create_timestamp;

	$where_array = array(
	    "dvp_id" => $p_value->dvp_id
	);

	//get_key_word
	$data['keywords'] = array();
	$keywords = $this->Common_model->get_db_join("keyword_department_vr_photo as dvp", $where_array , "keywords as ks" , "dvp.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");

	foreach ($keywords as $keywords_key => $keywords_value) {
		if ($keywords_value->kwd_name!=""||$keywords_value->kwd_name!=NULL) {
			$data['keywords'][$keywords_key] = $keywords_value->kwd_name;
		}
	}
	@$vr_photo[$p_key]->keywords = $data['keywords'];
}

$this->load->model("Department_vr_video_model");

$where_array = array(
	'dvv.t_id'=>$input_array['topic_id'],
	'dvv_state' => 'Y',
	'dvv_del' => 'N'
);

$vr_video_list = $this->Department_vr_video_model->get_list('', '', 6, 0, $where_array);

// echo json_encode($vr_video_list);
// exit;

foreach ($vr_video_list as $v_key => $v_value) {
	@$vr_video[$v_key]->dvv_id = $v_value->dvv_id;
	@$vr_video[$v_key]->dvv_name = $v_value->dvv_name;
	@$vr_video[$v_key]->dvv_user_name = $v_value->u_name;

	//局處顯示不一樣
	if ($v_value->is_civil=="N") {
		@$vr_video[$v_key]->dvv_user_name = $v_value->ag_name;
	}
	
	@$vr_video[$v_key]->dvv_image = $this->config->item('server_base_url').$v_value->dvv_rep_img_path;
	@$vr_video[$v_key]->dvp_web_video = $this->config->item('server_base_url').'api/vv_redirect?id='.$v_value->dvv_id;
	@$vr_video[$v_key]->dvv_mobile_video = $this->config->item('server_base_url').'api/vv_redirect?id='.$v_value->dvv_id.'&device=m';
	@$vr_video[$v_key]->dvp_date = $v_value->dvp_create_timestamp;

	$where_array = array(
	    "dvv_id" => $v_value->dvv_id
	);

	//get_key_word
	$data['keywords'] = array();
	$keywords = $this->Common_model->get_db_join("keyword_department_vr_video as dvv", $where_array , "keywords as ks" , "dvv.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");

	foreach ($keywords as $keywords_key => $keywords_value) {
		if ($keywords_value->kwd_name!=""||$keywords_value->kwd_name!=NULL) {
			$data['keywords'][$keywords_key] = $keywords_value->kwd_name;
		}
	}
	@$vr_video[$v_key]->keywords = $data['keywords'];
}



$result = array(
	"vr_photo" => $vr_photo,
	"vr_video" => $vr_video
);

//顯示呼叫結果
$ret = array(
    "result" => "true",
    "code" => 200,
    "error_message" => "",
    "data" => $result
);

//沒有結果
if(count($vr_photo)==0&&count($vr_video)==0){
	http_response_code(400);
	$ret = array(
	    "result" => "false",
	    "code" => 400,
	    "error_message" => "None Data",
	    "data" => array()
	);
}

echo json_encode($ret);
exit();

