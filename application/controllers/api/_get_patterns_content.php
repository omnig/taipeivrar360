<?php
header('Content-Type: application/json');
//以Image ID 拿到 AR Pattern 對應資料

$input_array = array(
    "pattern_id" => $this->input->GET('pattern_id')
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}
 
$input_array['user_id'] = $this->input->GET('user_id')?$this->input->GET('user_id'):"";

//局處AR列表
// $where_array = array(
//     'tdar_del' => "N"
// );
// $where_like_array = array(
// 	'tdar_image_path' => $input_array["pattern_id"]
// 	);

//先找局處
// $ar_result = $this->Common_model->get_mulit_ar("tp_department_ar", $where_array,"tdar_id","asc",$where_like_array);
$this->load->model("Department_ar_model");
$ar_result = $this->Department_ar_model->get_mulit_ar( $input_array["pattern_id"]);




if (!$ar_result) {
	//在找名眾

	// http_response_code(204);
	$ret = array(
	    "result" => "false",
	    "code" => 204,
	    "error_message" => "No Data Here",
	    "data" => array()
	);
	echo json_encode($ret);
	exit();
}


foreach ($ar_result as $key => $value) {
    $ar_object[$key] = (object) [
        "id" => $ar_result[$key]->tdar_id,
        "name" => $ar_result[$key]->tdar_name,
        "content_type" => $ar_result[$key]->tdar_content_type,
        "heigh" => $ar_result[$key]->default_ar_heigh,
        "interactive_text" => $ar_result[$key]->default_ar_text,
        "interactive_url" => $ar_result[$key]->default_ar_url,
        "cover_identify_image" => $ar_result[$key]->default_cover_identify_image,
        "size" => number_format((float)$ar_result[$key]->default_ar_vsize/100,2),
        "isTransparent" => $ar_result[$key]->tdar_is_transparent,
        "ext" => preg_replace('/^.*\.([^.]+)$/D', '$1', $ar_result[$key]->tdar_content_path),
    ];

    switch ($ar_result[$key]->tdar_content_type) {
        case 'text':
            $ar_object[$key]->content = $ar_result[$key]->tdar_content_path;

            //產生文字圖片
            //$url = "http://api.img4me.com/?text=".$ar_object[$key]->tdar_content_path."&font=arial&fcolor=000000&size=30&bcolor=FFFFFF&type=png";
            $resulr_url = $this->config->item('server_base_url')."/api/get_text_image?text=".$ar_result[$key]->tdar_content_path;
            //$resulr_url = curl_query($url);
            $ar_object[$key]->url_image = $resulr_url;

            break;
        case 'image':
            $ar_object[$key]->content = $this->config->item('server_base_url')."/upload/image/".$ar_result[$key]->tdar_content_path;     
            break;
        case 'youtube':
            $ar_object[$key]->content = $ar_result[$key]->tdar_content_path;
            break;
        case '3d_model':
            $ar_object[$key]->content = $this->config->item('server_base_url')."/upload_model/".$ar_result[$key]->tdar_content_path;
            $ar_object[$key]->animation_name = $ar_result[$key]->tdar_animation_name;
            break;
        case 'video':
            $ar_object[$key]->content = $this->config->item('server_base_url')."/upload_video/".$ar_result[$key]->tdar_content_path;
            break;
    }
}

//顯示呼叫結果
$ret = array(
    "result" => "true",
    "code" => 200,
    "error_message" => "",
    "data" => array_values($ar_object)
);
$this->log($page, json_encode($ret), 'Y');
echo json_encode($ret);
exit();
