<?php

/*
 * 取得室內導航路線
 */

//傳入值接收
$input_array = array(
    "a_id" => $this->input->get("a"),
    "b_id" => $this->input->get("b")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID VALUE", 'N');
        $ret = array(
            "result" => "false",
            "error_message" => "INVALID VALUE"
        );

        echo json_encode($ret);
        exit();
    }
}

$input_array["callback"] = $this->input->get("callback");


//從資料庫取得puid
$where_array = array(
    "ap_id" => $input_array["a_id"]
);
$a_puid = $this->Common_model->get_one_field("ap_pois", "ap_puid", $where_array);

if (!$a_puid) {
    $this->log($page, "INVALID ID", 'N');
    $ret = array(
        "result" => "false",
        "error_message" => "INVALID ID"
    );

    echo json_encode($ret);
    exit();
}

$where_array = array(
    "ap_id" => $input_array["b_id"]
);
$b_puid = $this->Common_model->get_one_field("ap_pois", "ap_puid", $where_array);

if (!$b_puid) {
    $this->log($page, "INVALID ID", 'N');
    $ret = array(
        "result" => "false",
        "error_message" => "INVALID ID"
    );

    echo json_encode($ret);
    exit();
}


//查詢
$query_info = array(
    "pois_from" => $a_puid,
    "pois_to" => $b_puid
);

//載入helper
$this->load->helper('anyplace_helper');
$result = get_navi_result('route', $query_info);


if (isset($result["pois"])) {
    $result = $result["pois"];
    $ret = array(
        "result" => "true",
        "error_message" => "",
        "data" => $result
    );
} else if ($result["status"] == "error") {
    $ret = array(
        "result" => "false",
        "error_message" => $result["message"]
    );
}


if ($input_array["callback"]) {
    echo "{$input_array["callback"]}(" . json_encode($ret) . ")";
} else {
    echo json_encode($ret);
}

$this->log($page, "", 'Y');
exit();
