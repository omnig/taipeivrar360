<?php

/*
 * 取得建物樓層
 */

//傳入值接收
$input_array = array();

$input_array["callback"] = $this->input->get("callback");

// $ap_building = $this->Common_model->get_db("ap_building", []);
$this->load->model('Ap_pois_model');
$ap_pois = $this->Ap_pois_model->get_list($order = '', $keyword = '', $limit = 0, $skip = 0, []);

if (!$ap_pois) {
    //記錄
    $this->log($page, "NOT FOUND", 'N');
    $ret = array(
        "result" => "false",
        "error_message" => "INVALID BUILDING"
    );
    if ($input_array["callback"]) {
        echo "{$input_array["callback"]}(" . json_encode($ret) . ")";
    } else {
        echo json_encode($ret);
    }
    exit();
}

$building = array();

foreach ($ap_pois as $key => $row) {
    $building[$key] = array(
        "id" => $row->ap_id,
        "name" => $row->ap_name,
        "desc" => $row->ap_desc,
        "locid" => ($row->ap_locid) ? $row->ap_locid : "",
        "enabled" => $row->ap_enabled,
        "lat" => $row->ap_lat,
        "lng" => $row->ap_lng,
        "hyperlink_text" => $row->ap_hyperlink_text,
        "hyperlink_url" => $row->ap_hyperlink_url,
        "video_hyperlink" => $row->ap_video_hyperlink,
        "audio_path" => $row->ap_audio_path,
        "ar_trigger" => [
            "active_method" => $row->ap_ar_active_method,
            "distance" => $row->ap_ar_distance,
            "identify_image_path" => $this->config->item('server_base_url')."/upload_image/".$row->dai_image_path,
        ],
        "ar" => [
            "text" => $row->ap_ar_text,
            "url" => $row->ap_ar_url,
            "heigh" => $row->ap_ar_heigh,
            "content_type" => $row->tdar_content_type,
            // "content" => $row->tdar_content_path,
            "interactive_text" => $row->tdar_interactive_text,
            "interactive_url" => $row->tdar_interactive_url
        ]
    );
    switch ($row->tdar_content_type) {
        case 'text':
            $building[$key]["ar"]["content"] = $row->tdar_content_path;
    
            //產生文字圖片
            $resulr_url = $this->config->item('server_base_url')."/api/get_text_image?text=".$row->tdar_content_path;
            //$resulr_url = curl_query($url);
            $building[$key]["ar"]["url_image"] = $resulr_url;
    
            break;
        case 'image':
            $building[$key]["ar"]["content"] = $this->config->item('server_base_url')."/upload/image/".$row->tdar_content_path;		
            break;
        case 'youtube':
            $building[$key]["ar"]["content"] = $row->tdar_content_path;
            break;
        case '3d_model':
            $building[$key]["ar"]["content"] = $this->config->item('server_base_url')."/upload_model/".$row->tdar_content_path;
            $building[$key]["ar"]["ios"] = $this->config->item('server_base_url')."/upload_model/".$row->tdar_content_path;
            $building[$key]["ar"]["android"] = $this->config->item('server_base_url').$row->tdar_content_wt3;
    
            break;
        case 'video':
            $building[$key]["ar"]["content"] = $this->config->item('server_base_url')."/upload_video/".$row->tdar_content_path;
            break;
    }
}


$ret = array(
    "result" => "true",
    "error_message" => "",
    "data" => $building
);

if ($input_array["callback"]) {
    echo "{$input_array["callback"]}(" . json_encode($ret) . ")";
} else {
    echo json_encode($ret);
}

$this->log($page, json_encode($ret), 'Y');
exit();
