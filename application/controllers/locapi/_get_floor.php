<?php

/*
 * 取得建物樓層
 */

//傳入值接收
$input_array = array(
    "ab_id" => $this->input->get("b")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        //記錄
        $this->log($page, "INVALID VALUE", 'N');
        exit();
    }
}

$input_array["callback"] = $this->input->get("callback");


$where_array = array(
    'ab_id' => $input_array["ab_id"],
    "af_enabled" => "Y"
);

$ap_floors = $this->Common_model->get_db("ap_floors", $where_array, "af_order");

if (!$ap_floors) {
    //記錄
    $this->log($page, "FLOOR NOT FOUND", 'N');
    $ret = array(
        "result" => "false",
        "error_message" => "FLOOR NOT FOUND"
    );

    if ($input_array["callback"]) {
        echo "{$input_array["callback"]}(" . json_encode($ret) . ")";
    } else {
        echo json_encode($ret);
    }
    exit();
}

$af_ids = array();
foreach ($ap_floors as $row) {
    $af_ids[] = $row->af_id;
}
$af_ids = array_unique($af_ids);

$where_array = array(
    'ap.af_id' => $af_ids,
    "ap_enabled" => "Y"
);
//$ap_pois = $this->Common_model->get_db_join("ap_pois as ap", $where_array, 'ap_category as ac', 'ap.ac_id=ac.ac_id');

$this->load->model('Ap_pois_model');
$ap_pois = $this->Ap_pois_model->get_list($order = '', $keyword = '', $limit = 0, $skip = 0, $where_array);

// $ap_pois = $this->Common_model->get_db_join("ap_pois as ap", $where_array, 'tp_department_ar as tdar', 'ap.ap_puid=tdar.ap_puid',"INNER");

if (count($ap_pois) === 0) {
    //記錄
    $this->log($page, "POI NOT FOUND", 'N');
    $ret = array(
        "result" => "false",
        "error_message" => "POI NOT FOUND"
    );

    if ($input_array["callback"]) {
        echo "{$input_array["callback"]}(" . json_encode($ret) . ")";
    } else {
        echo json_encode($ret);
    }
    exit();
}


//取得有連結POI的展品
$where_array = array(
    'e_enabled' => 'Y',
    'e_del' => 'N'
);
$this->load->model('exh_model');
$exhibits = $this->exh_model->get_exh_relation('', '', 0, 0, $where_array);


$floors = array();

foreach ($ap_floors as $key => $row) {
    $pois = array();
    $idx = 0;
    foreach ($ap_pois as $p_key => $poi) {
        //檢視是否有展品
        $is_exhibit = false;
        $poi_exhibit = array();
        foreach ($exhibits as $e_key => $e) {
            if ($e->ap_id == $poi->ap_id) {
                $is_exhibit = true;
                $poi_exhibit[] = array(
                    'e_id' => (int) $e->e_id,
                    'title_zh' => $e->e_title_zh,
                    'content_zh' => $e->e_content_zh ? $e->e_content_zh : '',
                    'title_en' => $e->e_title_en ? $e->e_title_en : '',
                    'content_en' => $e->e_content_en ? $e->e_content_en : '',
                    'title_jp' => $e->e_title_jp ? $e->e_title_jp : '',
                    'content_jp' => $e->e_content_jp ? $e->e_content_jp : '',
                    'image' => ($e->e_image) ? $this->config->item("image_base_url") . "upload/exh/" . $e->e_image : '',
                    'audio' => ($e->e_audio) ? $this->config->item('vs_base_url') . $e->e_audio : '',
                    'tour_time' => (int) $e->e_tour_time
                );
            }
        }
        if (($row->af_id == $poi->af_id) && $poi->ap_enabled == "Y") {
            $pois[$idx] = array(
//                "is_entrance" => $poi->ap_is_building_entrance,
//                "is_door" => $poi->ap_is_door,
//                'is_show' => $poi->ac_is_show,
//                "is_exhibit" => $is_exhibit,
//                "exhibit" => $poi_exhibit,
                "id" => $poi->ap_id,
                "name" => $poi->ap_name,
                "desc" => $poi->ap_desc,
                "locid" => ($poi->ap_locid) ? $poi->ap_locid : "",
                "enabled" => $poi->ap_enabled,
                "lat" => $poi->ap_lat,
                "lng" => $poi->ap_lng,
                "hyperlink_text" => $poi->ap_hyperlink_text,
                "hyperlink_url" => $poi->ap_hyperlink_url,
                "video_hyperlink" => $poi->ap_video_hyperlink,
                "audio_path" => $poi->ap_audio_path,
                "ar_trigger" => [
                    "active_method" => $poi->ap_ar_active_method,
                    "distance" => $poi->ap_ar_distance,
                    "identify_image_path" => $this->config->item('server_base_url')."/upload_image/".$poi->dai_image_path,
                ],
                "ar" => [
                    "text" => $poi->ap_ar_text,
                    "url" => $poi->ap_ar_url,
                    "heigh" => $poi->ap_ar_heigh,
                    "content_type" => $poi->tdar_content_type,
                    // "content" => $row->tdar_content_path,
                    "interactive_text" => $poi->tdar_interactive_text,
                    "interactive_url" => $poi->tdar_interactive_url,
                    "cover_identify_image" => $poi->ap_cover_identify_image,
                    "size" => number_format((float)$poi->ap_ar_vsize/100,2)
                ]
            );
            if(is_null($poi->b_id)){
                $pois[$idx]["beacon"] = null;
            }else{
                $pois[$idx]["beacon"] = [
                    "id" => $poi->b_id,
                    "description" => $poi->b_description,
                    "major" => $poi->b_major,
                    "minor" => $poi->b_minor,
                    "hwid" => $poi->b_hwid,
                    "mac" => $poi->b_mac,
                    "lat" => $poi->b_lat,
                    "lng" => $poi->b_lng,
                    "range" => $poi->b_range,
                ];
            }
            switch ($poi->tdar_content_type) {
                case 'text':
                    $pois[$idx]["ar"]["content"] = $poi->tdar_content_path;

                    //產生文字圖片
                    $resulr_url = $this->config->item('server_base_url')."/api/get_text_image?text=".$poi->tdar_content_path;
                    //$resulr_url = curl_query($url);
                    $pois[$idx]["ar"]["url_image"] = $resulr_url;

                    break;
                case 'image':
                    $pois[$idx]["ar"]["content"] = $this->config->item('server_base_url')."/upload/image/".$poi->tdar_content_path;
                    break;
                case 'youtube':
                    $pois[$idx]["ar"]["content"] = $poi->tdar_content_path;
                    break;
                case '3d_model':
                    $pois[$idx]["ar"]["content"] = $this->config->item('server_base_url')."/upload_model/".$poi->tdar_content_path;
                    $pois[$idx]["ar"]["ios"] = $this->config->item('server_base_url')."/upload_model/".$poi->tdar_content_path;
                    $pois[$idx]["ar"]["android"] = $this->config->item('server_base_url').$poi->tdar_content_wt3;

                    break;
                case 'video':
                    $pois[$idx]["ar"]["content"] = $this->config->item('server_base_url')."/upload_video/".$poi->tdar_content_path;
                    $pois[$idx]["ar"]["isTransparent"] = $poi->tdar_is_transparent;
                    break;
            }
            $idx++;
        }
    }
    if ($row->af_enabled == "Y") {
        $floors[$key] = array(
            "id" => (int) $row->af_id,
            "number" => $row->af_number,
            "name" => $row->af_name,
            "desc" => $row->af_desc,
            "order" => $row->af_order,
            "lat" => ($row->af_bottom_left_lat + $row->af_top_right_lat) / 2,
            "lng" => ($row->af_bottom_left_lng + $row->af_top_right_lng) / 2,
            "bl_lat" => (float) $row->af_bottom_left_lat,
            "bl_lng" => (float) $row->af_bottom_left_lng,
            "tr_lat" => (float) $row->af_top_right_lat,
            "tr_lng" => (float) $row->af_top_right_lng,
            "plan_id" => ($row->af_plan_id) ? $row->af_plan_id : "",
            "is_map" => $row->af_map,
            "pois" => $pois
        );
    }
}



$ret = array(
    "result" => "true",
    "error_message" => "",
    "data" => $floors
);

if ($input_array["callback"]) {
    echo "{$input_array["callback"]}(" . json_encode($ret) . ")";
} else {
    echo json_encode($ret);
}

$this->log($page, "", 'Y');
exit();
