<?php
header('Content-Type: application/json');
$this->load->model("Mission_model");

//流程
//1.判斷使用者目前在此任務關卡第幾關
//2.導引至未完成下一關關卡
//3.取得導引POI 
//4.取得此POI 的 Building
//5.照後面的判斷


// 1.判斷建築物的中心點(1樓平面圖的左上經緯與右下經緯/2)
// 2.判斷點中心半徑(中心點與右下經緯的距離)
// 3.使用者與中心點距離計算
// 4.若距離在半徑內 則代表在建物內, 反之則建物外

// 5.使用者若在建築物外，尋找最近出口  (搜尋所有出口  找出與使用者距離最短的)
// 6.找出最近出口座標，返回座標路徑  出口->目的
// 7.使用者若在建築物內直接Call Anyplace 位置->目的

// echo json_encode("123");
// exit();


$input_array = array(
    "m_id" => $this->input->get("m_id"),   //任務ID
    "u_id" => $this->input->get("u_id"),   //任務ID
    // "ab_id" => $this->input->get("b"),	   //建物ID
    "lat" => $this->input->get("lat"), //使用者經度
    "lng" => $this->input->get("lng"), //使用者緯度
    // "b_lat" => $this->input->get("b_lat"), //目標物經度
    // "b_lng" => $this->input->get("b_lng")  //目標物緯度
);


foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}


//查驗使用者經緯度
if(isset($input_array["lat"])):
  if (!is_numeric($input_array["lat"]) || !is_numeric($input_array["lng"])) {
      $this->log($page, "INVALID LOCATION", 'N');
      $ret = array(
          "result" => "false",
          "error_message" => "INVALID LOCATION"
      );
      echo json_encode($ret);
      exit();
  }
endif;



//判斷使用者目前在此任務關卡第幾關
//取得任務列表資訊
$where_array = array(
    "m_id" => $input_array['m_id'],
    "p_id >" => "0"
);
// $mission = $this->Common_model->get_db("nine_grid", $where_array);
$mission = $this->Mission_model->get_mission_ar("ng_order", "", 0, 0, $where_array);
$max_mission = count($mission);


if (count($mission)<=0) {
    $this->log($page, "INVALID MISSION", 'N');
      $ret = array(
          "result" => "false",
          "error_message" => "INVALID MISSION"
      );
      echo json_encode($ret);
      exit();
}



// echo json_encode($mission);
// exit();



//彙整使用者資訊
$where_array =array(
  'm_id' => $input_array["m_id"],
  'u_id' => $input_array["u_id"]
);

$user = $this->Mission_model->get_mission_user("", "", 0, 0, $where_array);

if (count($user)==0) {
  //代表沒有資訊-要新增
  $insert_array = array(
      "u_id" => $input_array["u_id"],
      "m_id" => $input_array["m_id"],
      "m_id_max" => $max_mission,
      "grid_now" => 1,
      "Is_finish" => "None",
      "rws_enabled" => "None"
  );

  $row = $this->Common_model->insert_db("mission_user", $insert_array);
  //取得POI
  $poi =(int)$mission[0]->p_id;

  $ar_info= array(
      "now_grid" => $now_grid+1,
      "building_id" => $mission[0]->ab_id,
      "id" => $mission[0]->tdar_id, 
      "name" => $mission[0]->tdar_name, 
      "img" => $this->config->item("server_base_url")."upload/image/".$mission[0]->tdar_findpho_path,
      "pattern_image" => $this->config->item("server_base_url")."upload_image/".$mission[0]->tdar_image_path,
      "description" => $mission[0]->tdar_description,
      "lat" => $mission[0]->tdar_lat,
      "lng" => $mission[0]->tdar_lng

  );
  // $result =  return_route_xt2($poi,$floor_number,$input_array["lat"],$input_array["lng"],$page);
  $result =  return_route_xt2($mission[0]->ab_id,$input_array["lat"],$input_array["lng"],$mission[0]->ap_lat,$mission[0]->ap_lng,$ar_info);
}else{
  //代表有資訊-要回傳現在關卡位置
  //現在關卡位置
  $now_grid = (int)$user[0]->grid_now;
  $now_grid = $now_grid-1;
  if ($user[0]->Is_finish=="Done") {
    $ret = array(
          "result" => "false",
          "error_message" => "MISSION IS COMPLETE"
      );
      echo json_encode($ret);
      exit();
  }

  //取得POI
  $poi = (int)$mission[$now_grid]->p_id;

  $ar_info= array(
      "now_grid" => $now_grid+1,
       "building_id" => $mission[$now_grid]->ab_id,
      "id" => $mission[$now_grid]->tdar_id, 
      "name" => $mission[$now_grid]->tdar_name, 
      "img" => $this->config->item("server_base_url")."upload/image/".$mission[$now_grid]->tdar_findpho_path,
      "pattern_image" => $this->config->item("server_base_url")."upload_image/".$mission[$now_grid]->tdar_image_path,
      "description" => $mission[$now_grid]->tdar_description,
      "lat" => $mission[$now_grid]->tdar_lat,
      "lng" => $mission[$now_grid]->tdar_lng

  );


  $result =  return_route_xt2($mission[$now_grid]->ab_id,$input_array["lat"],$input_array["lng"],$mission[$now_grid]->ap_lat,$mission[$now_grid]->ap_lng,$ar_info);
}


echo json_encode($result);
exit();





function rad($d, $pi = 3.14) {
 return $d * $pi / 180.0;
}
 
function getDistance($lat1, $lng1, $lat2, $lng2) {
 $r = 6378.137; // 地球半徑
 $radLat1 = rad($lat1);
 $radLat2 = rad($lat2);
 $lat = $radLat1 - $radLat2;
 $long = rad($lng1) - rad($lng2);
 
 $s = 2 * asin(sqrt(pow(sin($lat/2), 2) + cos($radLat1)*cos($radLat2)*pow(sin($long/2), 2)));
 $s = $s * $r;
 return round($s * 10000) / 10000;
 // return $s;
}




function return_route_xt2($building_id,$user_lat,$user_lng,$ar_lat,$ar_lng,$ar_info){


  $ci = & get_instance();
  //從資料庫取得相關參數
    $return_ary = array();
    $return_ary['inhourse'] = "false";
    $where_array = array(
        'af.ab_id' =>$building_id,
        'af_number' => 1
    );

    $ap_building_floor = $ci->Common_model->get_db_join("ap_floors as af", $where_array , "ap_building as ab" , "af.ab_id = ab.ab_id " , "INNER" ,"" , "ASC");



    $lacation1_lat = $ap_building_floor[0]->af_bottom_left_lat;
    $lacation1_lng = $ap_building_floor[0]->af_bottom_left_lng;

    $lacation2_lat = $ap_building_floor[0]->af_top_right_lat;
    $lacation2_lng = $ap_building_floor[0]->af_top_right_lng;

    $center_lat = ($ap_building_floor[0]->af_bottom_left_lat+$ap_building_floor[0]->af_top_right_lat)/2;
    $center_lng = ($ap_building_floor[0]->af_bottom_left_lng+$ap_building_floor[0]->af_top_right_lng)/2;

    $radius = getDistance($lacation1_lat, $lacation1_lng, $center_lat, $center_lng );
    $user_distance = getDistance($user_lat, $user_lng, $center_lat, $center_lng );


    if ($radius>=$user_distance) {
        $return_ary['inhourse'] = "true";
        $return_ary['ab_id'] = $ap_building_floor[0]->ab_id;
        $return_ary['af_fuid'] = $ap_building_floor[0]->af_fuid;
    }



    //取得最近的入口
    $ci->load->model("Api_model");

    $where_array = array(
        "ap_is_building_entrance" => "Y",
        "ab_id" => $building_id
    );
    $nearby_entrance = $ci->Api_model->search_entrance($user_lat, $user_lng, $where_array, 1, 1);



    if (!$nearby_entrance) {
        $ret = array(
            "result" => "false",
            "error_message" => "Entrance NO RESULT"
        );

        echo json_encode($ret);
        exit();
    }


    $where_array = array(
    // "ap_is_building_entrance" => "N",
        "ab_id" => $building_id
    );
    $nearby_poi = $ci->Api_model->search_entrance($ar_lat, $ar_lng, $where_array, 1, 1);



    if (!$nearby_poi) {
        $ci->log($page, "NO RESULT", 'N');
        $ret = array(
            "result" => "false",
            "error_message" => " POI NO RESULT"
        );

        echo json_encode($ret);
        exit();
    }

    //使用者所在樓層
    $floor_number = ($ci->input->get("f")) ? $ci->input->get("f") : '1';

    //查詢 判斷使用者是在室內或在室外
    if ($return_ary['inhourse']=="true") {
        //室內
        $query_info = array(
            "pois_to" => $nearby_poi[0]->ap_puid,
            "floor_number" => $floor_number,
            "coordinates_lat" => $user_lat, //使用者位置
            "coordinates_lon" => $user_lng  //使用者位置
        );
    }else{
        //室外
        $query_info = array(
            "pois_to" => $nearby_poi[0]->ap_puid,
            "floor_number" => $floor_number,
            "coordinates_lat" => $nearby_entrance[0]->ap_lat, //最近出口位置
            "coordinates_lon" => $nearby_entrance[0]->ap_lng  //最近出口位置
        );
    }



    //載入helper
    $ci->load->helper('anyplace_helper');
    $result = get_navi_result('route_xy', $query_info);

    //  // echo json_encode($nearby_poi);
     // echo json_encode($result);
    // exit();

    if (isset($result["pois"])) {
        $result = $result["pois"];
        $return_ary['result']  ="true";
        $return_ary['error_message']  = "";
        $return_ary['data'] = $result;
        $return_ary['ar_info'] = $ar_info;

    } else if ($result["status"] == "error") {
        $return_ary['result']  = "false";
        $return_ary['error_message']  = "路徑未建立";
        $return_ary['data'] = $result["message"];
    }else{
        $return_ary['result']  = "false";
        $return_ary['error_message']  = "點位路徑導引有問題";
        $return_ary['data'] = "error";
        $return_ary['info'] = $result;
    }

    // echo json_encode($return_ary);
    // exit();

    return $return_ary;
}

