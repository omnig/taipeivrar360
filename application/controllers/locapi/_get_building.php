<?php

/*
 * 取得建物樓層
 */

//傳入值接收
$input_array = array();

$input_array["callback"] = $this->input->get("callback");

$where_array = array(
    '(ab_enabled = "Y")'
);
$ap_building = $this->Common_model->get_db("ap_building as ab", $where_array, 'ab_create_timestamp');

if (!$ap_building) {
    //記錄
    $this->log($page, "NOT FOUND", 'N');
    $ret = array(
        "result" => "false",
        "error_message" => "INVALID BUILDING"
    );
    if ($input_array["callback"]) {
        echo "{$input_array["callback"]}(" . json_encode($ret) . ")";
    } else {
        echo json_encode($ret);
    }
    exit();
}

$building = array();

foreach ($ap_building as $row) {
    $building[] = array(
        "ab_id" => (int) $row->ab_id,
        "c_id" => $row->c_id,
        "ab_buid" => $row->ab_buid,
        "ab_desc" => $row->ab_desc,
        "ab_lat" => $row->ab_lat,
        "ab_lng" => $row->ab_lng,
        "ab_locid" => $row->ab_locid,
        "ab_enabled" => $row->ab_enabled,
        "ab_create_timestamp" => $row->ab_create_timestamp,
    );
}


$ret = array(
    "result" => "true",
    "error_message" => "",
    "data" => $building
);

if ($input_array["callback"]) {
    echo "{$input_array["callback"]}(" . json_encode($ret) . ")";
} else {
    echo json_encode($ret);
}

$this->log($page, "", 'Y');

exit();
