<?php

/*
 * 從室內位置至最近出口
 */

//傳入值接收
$input_array = array(
    "ab_id" => $this->input->get("b"),
    "a_lat" => $this->input->get("a_lat"),
    "a_lng" => $this->input->get("a_lng"),
    "b_lat" => $this->input->get("b_lat"),
    "b_lng" => $this->input->get("b_lng")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID VALUE", 'N');
        $ret = array(
            "result" => "false",
            "error_message" => "INVALID VALUE"
        );

        echo json_encode($ret);
        exit();
    }
}

if (!is_numeric($input_array["a_lat"]) || !is_numeric($input_array["a_lng"]) || !is_numeric($input_array["b_lat"]) || !is_numeric($input_array["b_lng"])) {
    $this->log($page, "INVALID VALUE", 'N');
    $ret = array(
        "result" => "false",
        "error_message" => "INVALID VALUE"
    );

    echo json_encode($ret);
    exit();
}

$input_array["callback"] = $this->input->get("callback");


//取得最近的入口
$this->load->model("Api_model");

$where_array = array(
    "ap_is_building_entrance" => "Y",
    "ab_id" => $input_array["ab_id"]
);
$nearby_entrance = $this->Api_model->search_entrance($input_array["b_lat"], $input_array["b_lng"], $where_array, 1, 1);

if (!$nearby_entrance) {
    $this->log($page, "NO RESULT", 'N');
    $ret = array(
        "result" => "false",
        "error_message" => "NO RESULT"
    );

    echo json_encode($ret);
    exit();
}


//使用者所在樓層
$floor_number = ($this->input->get("f")) ? $this->input->get("f") : '1';

//查詢
$query_info = array(
    "pois_to" => $nearby_entrance[0]->ap_puid,
    "floor_number" => $floor_number,
    "coordinates_lat" => $input_array["a_lat"],
    "coordinates_lon" => $input_array["a_lng"]
);

//載入helper
$this->load->helper('anyplace_helper');
$result = get_navi_result('route_xy', $query_info);

if (isset($result["pois"])) {
    $result = $result["pois"];
    $ret = array(
        "result" => "true",
        "error_message" => "",
        "data" => $result
    );
} else if ($result["status"] == "error") {
    $ret = array(
        "result" => "false",
        "error_message" => $result["message"]
    );
}


if ($input_array["callback"]) {
    echo "{$input_array["callback"]}(" . json_encode($ret) . ")";
} else {
    echo json_encode($ret);
}

$this->log($page, "", 'Y');
exit();
