<?php
header('Content-Type: application/json');


//流程
// 1.判斷建築物的中心點(1樓平面圖的左上經緯與右下經緯/2)
// 2.判斷點中心半徑(中心點與右下經緯的距離)
// 3.使用者與中心點距離計算
// 4.若距離在半徑內 則代表在建物內, 反之則建物外

// 5.若在建築物外，尋找最近出口  (搜尋所有出口  找出與使用者距離最短的)
// 6.找出最近出口座標，返回座標路徑  出口->目的
// 7.若在建築物內直接Call Anyplace 位置->目的


$input_array = array(
    "ab_id" => $this->input->get("b"),	   //建物ID
    "a_lat" => $this->input->get("a_lat"), //使用者經度
    "a_lng" => $this->input->get("a_lng"), //使用者緯度
    "b_lat" => $this->input->get("b_lat"), //目標物經度
    "b_lng" => $this->input->get("b_lng")  //目標物緯度
);



$return_ary = array();
$return_ary['inhourse'] = "false";
$where_array = array(
	'af.ab_id' =>$input_array['ab_id'],
	'af_number' => 1
);

$ap_building_floor = $this->Common_model->get_db_join("ap_floors as af", $where_array , "ap_building as ab" , "af.ab_id = ab.ab_id " , "INNER" ,"" , "ASC");

// $type = $this->Common_model->get_db('ap_category', array());

// echo json_encode($ap_building_floor);
// exit;

$lacation1_lat = $ap_building_floor[0]->af_bottom_left_lat;
$lacation1_lng = $ap_building_floor[0]->af_bottom_left_lng;

$lacation2_lat = $ap_building_floor[0]->af_top_right_lat;
$lacation2_lng = $ap_building_floor[0]->af_top_right_lng;

$center_lat = ($ap_building_floor[0]->af_bottom_left_lat+$ap_building_floor[0]->af_top_right_lat)/2;
$center_lng = ($ap_building_floor[0]->af_bottom_left_lng+$ap_building_floor[0]->af_top_right_lng)/2;


// echo json_encode($ap_building_floor);
// echo $center_lat;
// echo $center_lng;
// exit;

//ouside test
// $user_lat = 25.08109;
// $user_lng = 121.56508;

$user_lat = $input_array['a_lat'];
$user_lng = $input_array['a_lng'];

//inside test
// $user_lat = 25.08066;
// $user_lng = 121.56469;

$radius = getDistance($lacation1_lat, $lacation1_lng, $center_lat, $center_lng );
$user_distance = getDistance($user_lat, $user_lng, $center_lat, $center_lng );


if ($radius>=$user_distance) {
	$return_ary['inhourse'] = "true";
	$return_ary['ab_id'] = $ap_building_floor[0]->ab_id;
	$return_ary['af_fuid'] = $ap_building_floor[0]->af_fuid;
}




//取得最近的入口
$this->load->model("Api_model");

$where_array = array(
    "ap_is_building_entrance" => "Y",
    "ab_id" => $input_array["ab_id"]
);
$nearby_entrance = $this->Api_model->search_entrance($input_array["a_lat"], $input_array["a_lng"], $where_array, 1, 1);

if (!$nearby_entrance) {
    $this->log($page, "NO RESULT", 'N');
    $ret = array(
        "result" => "false",
        "error_message" => "Entrance NO RESULT"
    );

    echo json_encode($ret);
    exit();
}





$where_array = array(
    // "ap_is_building_entrance" => "N",
    "ab_id" => $input_array["ab_id"]
);
$nearby_poi = $this->Api_model->search_entrance($input_array["b_lat"], $input_array["b_lng"], $where_array, 1, 1);

if (!$nearby_poi) {
    $this->log($page, "NO RESULT", 'N');
    $ret = array(
        "result" => "false",
        "error_message" => " POI NO RESULT"
    );

    echo json_encode($ret);
    exit();
}


//使用者所在樓層
$floor_number = ($this->input->get("f")) ? $this->input->get("f") : '1';

//查詢 判斷使用者是在室內或在室外
if ($return_ary['inhourse']=="true") {
	//室內
	$query_info = array(
	    "pois_to" => $nearby_poi[0]->ap_puid,
	    "floor_number" => $floor_number,
	    "coordinates_lat" => $input_array["a_lat"], //使用者位置
	    "coordinates_lon" => $input_array["a_lng"]  //使用者位置
	);
}else{
	//室外
	$query_info = array(
	    "pois_to" => $nearby_poi[0]->ap_puid,
	    "floor_number" => $floor_number,
	    "coordinates_lat" => $nearby_entrance[0]->ap_lat, //最近出口位置
	    "coordinates_lon" => $nearby_entrance[0]->ap_lng  //最近出口位置
	);
}


//載入helper
$this->load->helper('anyplace_helper');
$result = get_navi_result('route_xy', $query_info);

if ($result["pois"]) {
    $result = $result["pois"];
    $return_ary['result']  ="true";
    $return_ary['error_message']  = "";
    $return_ary['data'] = $result;

} else if ($result["status"] == "error") {
    $return_ary['result']  = "true";
    $return_ary['error_message']  = "";
    $return_ary['data'] = array( 
        array(
            "buid" => "",
            "floor_number" => "1",
            "puid" => "",
            "pois_type" => "",
            "lat" => $input_array["b_lat"],
            "lon" => $input_array["b_lng"]
        )
        
    );
}else{
    $return_ary['result']  = "true";
    $return_ary['error_message']  = "";
    $return_ary['data'] = array( 
        array(
            "buid" => "",
            "floor_number" => "1",
            "puid" => "",
            "pois_type" => "",
            "lat" => $input_array["b_lat"],
            "lon" => $input_array["b_lng"]
        )
        
    );
}



// echo $radius."<br>";
// echo $user_distance;
// exit;

echo json_encode($return_ary);
exit;


function rad($d, $pi = 3.14) {
 return $d * $pi / 180.0;
}
 
function getDistance($lat1, $lng1, $lat2, $lng2) {
 $r = 6378.137; // 地球半徑
 $radLat1 = rad($lat1);
 $radLat2 = rad($lat2);
 $lat = $radLat1 - $radLat2;
 $long = rad($lng1) - rad($lng2);
 
 $s = 2 * asin(sqrt(pow(sin($lat/2), 2) + cos($radLat1)*cos($radLat2)*pow(sin($long/2), 2)));
 $s = $s * $r;
 return round($s * 10000) / 10000;
 // return $s;
}

