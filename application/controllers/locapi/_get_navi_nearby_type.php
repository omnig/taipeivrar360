<?php

/*
 * 從室內位置至最近出口
 */

//傳入值接收
$input_array = array(
    "ab_id" => $this->input->get("b"),
    "af_number" => $this->input->get("f"),
    "lat" => $this->input->get("lat"),
    "lng" => $this->input->get("lng"),
    "type" => strtolower($this->input->get("type"))
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID VALUE", 'N');
        $ret = array(
            "result" => "false",
            "error_message" => "INVALID VALUE"
        );

        echo json_encode($ret);
        exit();
    }
}

if (!is_numeric($input_array["lat"]) || !is_numeric($input_array["lng"])) {
    $this->log($page, "INVALID VALUE", 'N');
    $ret = array(
        "result" => "false",
        "error_message" => "INVALID VALUE"
    );

    echo json_encode($ret);
    exit();
}

$input_array["callback"] = $this->input->get("callback");


//載入model
$this->load->model("Api_model");

//取得最近的type
$type = strtolower($input_array["type"]);
$where_array = array(
    "f.ab_id" => $input_array["ab_id"],
    "f.af_number" => $input_array["af_number"],
    "ac_title_en like " => "%" . $input_array["type"] . "%"
);
$nearby_type = $this->Api_model->search_nearby_type($input_array["lat"], $input_array["lng"], $where_array, 0, 1, "af_number");

if (!$nearby_type) {
    $this->log($page, "NO RESULT", 'N');
    $ret = array(
        "result" => "false",
        "error_message" => "NO RESULT"
    );

    echo json_encode($ret);
    exit();
} else {
    foreach ($nearby_type as $row) {
        if ($row->af_number == $input_array["af_number"]) {
            $floor_number = $input_array["af_number"];
            $puid = $row->ap_puid;
            break;
        }
    }
}

if (!$puid) {
    //或找其它樓層(待確認)

    $this->log($page, "NO RESULT", 'N');
    $ret = array(
        "result" => "false",
        "error_message" => "NO RESULT"
    );

    echo json_encode($ret);
    exit();
}


$anyplace_api_url = $this->config->item('anyplace_api_url');
$anyplace_key = $this->config->item("anyplace_key");

//API名稱
$api_name = "navigation/route_xy";

//查詢
$data = array(
    "access_token" => $anyplace_key,
    "pois_to" => $puid,
    "floor_number" => $floor_number,
    "coordinates_lat" => $input_array["lat"],
    "coordinates_lon" => $input_array["lng"]
);


//目標API url
$url = $anyplace_api_url . $api_name;

$result = curl_query($url, "post", $data);
$result = json_decode($result, true);

if (isset($result["pois"])) {
    $result = $result["pois"];
    $ret = array(
        "result" => "true",
        "error_message" => "",
        "data" => $result
    );
} else if ($result["status"] == "error") {
    $ret = array(
        "result" => "false",
        "error_message" => $result["message"]
    );
}


if ($input_array["callback"]) {
    echo "{$input_array["callback"]}(" . json_encode($ret) . ")";
} else {
    echo json_encode($ret);
}

$this->log($page, "", 'Y');
exit();
