<?php

/*
 * 從使用者位置至室內POI路線
 */

//傳入值接收
$input_array = array(
    "p_id" => $this->input->get("p"),
    "lat" => $this->input->get("lat"),
    "lng" => $this->input->get("lng")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID VALUE", 'N');
        $ret = array(
            "result" => "false",
            "error_message" => "INVALID VALUE"
        );

        echo json_encode($ret);
        exit();
    }
}


if (!is_numeric($input_array["lat"]) || !is_numeric($input_array["lng"])) {
    $this->log($page, "INVALID LOCATION", 'N');
    $ret = array(
        "result" => "false",
        "error_message" => "INVALID LOCATION"
    );

    echo json_encode($ret);
    exit();
}

$input_array["callback"] = $this->input->get("callback");


//從資料庫取得相關參數
$where_array = array(
    "ap_id" => $input_array["p_id"]
);
$poi = $this->Common_model->get_one("ap_pois", $where_array);
if (!$poi) {
    $this->log($page, "INVALID VALUE", 'N');
    $ret = array(
        "result" => "false",
        "error_message" => "INVALID VALUE"
    );

    echo json_encode($ret);
    exit();
}

$puid = $poi->ap_puid;


//使用者所在樓層
$floor_number = ($this->input->get("f")) ? $this->input->get("f") : '1';

//查詢
$query_info = array(
    "pois_to" => $puid,
    "floor_number" => $floor_number,
    "coordinates_lat" => $input_array["lat"],
    "coordinates_lon" => $input_array["lng"]
);

//載入helper
$this->load->helper('anyplace_helper');
$result = get_navi_result('route_xy', $query_info);


if (isset($result["pois"])) {
    $result = $result["pois"];
    $ret = array(
        "result" => "true",
        "error_message" => "",
        "data" => $result
    );
} else if ($result["status"] == "error") {
    $ret = array(
        "result" => "false",
        "error_message" => $result["message"]
    );
}


if ($input_array["callback"]) {
    echo "{$input_array["callback"]}(" . json_encode($ret) . ")";
} else {
    echo json_encode($ret);
}

$this->log($page, "", 'Y');
exit();
