<?php
header('Content-Type: application/json');
// header('Access-Control-Allow-Origin: *');
/*
 * 取得樓層PLAN ID
 */

//傳入值接收
$input_array = array(
    "af_number" => '1'
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        //記錄
        $this->log($page, "INVALID VALUE", 'N');
        exit();
    }
}

//取得ab_id
$where_array = array(
    'ab_enabled' => "Y"
);
$building = $this->Common_model->get_db('ap_building', $where_array);

if (!$building) {
    $this->log($page, "INVALID BUID", 'N');
    exit();
}

// echo json_encode($building);
// exit;
$i=0;
$floor =array();
foreach ($building as $b_key => $b_value) {
    $where_array = array(
        'ab_id' => $b_value->ab_id,
        'af_number' => '1',
        'af_enabled' => 'Y',
        'af_plan_id !=' => NULL
    );

    $result = $this->Common_model->get_one("ap_floors", $where_array);

    // echo json_encode($result);
    // exit;
    if ($result) {
        $floor[$i] = array(
            'floor_name' => $result->af_name,
            'url' => $this->config->item("server_base_url") . 'map/tile/',
            'plan_id' => $result->af_plan_id,
            'bl_lat' => $result->af_bottom_left_lat,
            'bl_lng' => $result->af_bottom_left_lng,
            'tr_lat' => $result->af_top_right_lat,
            'tr_lng' => $result->af_top_right_lng
        );
        $i++;
    }
    
}




//顯示呼叫結果
$ret = array(
    "result" => "true",
    "error_message" => "",
    "data" => $floor
);

echo json_encode($ret);

exit();
