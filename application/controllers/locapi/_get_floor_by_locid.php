<?php

/*
 * 取得建物樓層
 */

//傳入值接收
$input_array = array(
    "ab_locid" => $this->input->get("b")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        //記錄
        $this->log($page, "INVALID VALUE", 'N');
        exit();
    }
}

$input_array["callback"] = $this->input->get("callback");

//取得對應建物ID
$where_array = array(
    'ab_locid' => $input_array["ab_locid"],
    "ab_enabled" => "Y"
);
$ap_building = $this->Common_model->get_one("ap_building", $where_array);

if (!$ap_building) {
    //記錄
    $this->log($page, "BUILDING NOT FOUND", 'N');
    $ret = array(
        "result" => "false",
        "error_message" => "BUILDING NOT FOUND"
    );

    if ($input_array["callback"]) {
        echo "{$input_array["callback"]}(" . json_encode($ret) . ")";
    } else {
        echo json_encode($ret);
    }
    exit();
}

$input_array["ab_id"] = $ap_building->ab_id;

$where_array = array(
    'ab_id' => $input_array["ab_id"],
    "af_enabled" => "Y"
);

$ap_floors = $this->Common_model->get_db("ap_floors", $where_array, "af_order");

if (!$ap_floors) {
    //記錄
    $this->log($page, "FLOOR NOT FOUND", 'N');
    $ret = array(
        "result" => "false",
        "error_message" => "FLOOR NOT FOUND"
    );

    if ($input_array["callback"]) {
        echo "{$input_array["callback"]}(" . json_encode($ret) . ")";
    } else {
        echo json_encode($ret);
    }
    exit();
}

$af_ids = array();
foreach ($ap_floors as $row) {
    $af_ids[] = $row->af_id;
}
$af_ids = array_unique($af_ids);

$where_array = array(
    'af_id' => $af_ids,
    "ap_enabled" => "Y"
);
$ap_pois = $this->Common_model->get_db_join("ap_pois as ap", $where_array, 'ap_category as ac', 'ap.ac_id=ac.ac_id');

if (!$ap_pois) {
    //記錄
    $this->log($page, "POI NOT FOUND", 'N');
    $ret = array(
        "result" => "false",
        "error_message" => "POI NOT FOUND"
    );

    if ($input_array["callback"]) {
        echo "{$input_array["callback"]}(" . json_encode($ret) . ")";
    } else {
        echo json_encode($ret);
    }
    exit();
}


//取得有連結POI的展品
$where_array = array(
    'e_enabled' => 'Y',
    'e_del' => 'N'
);
$this->load->model('exh_model');
$exhibits = $this->exh_model->get_exh_relation('', '', 0, 0, $where_array);


$floors = array();

foreach ($ap_floors as $key => $row) {
    $pois = array();
    $idx = 0;
    foreach ($ap_pois as $p_key => $poi) {
        //檢視是否有展品
        $is_exhibit = false;
        $poi_exhibit = array();
        foreach ($exhibits as $e_key => $e) {
            if ($e->ap_id == $poi->ap_id) {
                $is_exhibit = true;
                $poi_exhibit[] = array(
                    'e_id' => (int) $e->e_id,
                    'title_zh' => $e->e_title_zh,
                    'content_zh' => $e->e_content_zh ? $e->e_content_zh : '',
                    'title_en' => $e->e_title_en ? $e->e_title_en : '',
                    'content_en' => $e->e_content_en ? $e->e_content_en : '',
                    'title_jp' => $e->e_title_jp ? $e->e_title_jp : '',
                    'content_jp' => $e->e_content_jp ? $e->e_content_jp : '',
                    'image' => ($e->e_image) ? $this->config->item("image_base_url") . "upload/exh/" . $e->e_image : '',
                    'audio' => ($e->e_audio) ? $this->config->item('vs_base_url') . $e->e_audio : '',
                    'tour_time' => (int) $e->e_tour_time
                );
            }
        }
        if (($row->af_id == $poi->af_id) && $poi->ap_enabled == "Y") {
            $pois[$idx] = array(
                "id" => (int) $poi->ap_id,
                "name" => $poi->ap_name,
                "desc" => $poi->ap_desc,
                "type" => $poi->ac_title_en ? $poi->ac_title_en : "",
                "type_zh" => $poi->ac_title_zh ? $poi->ac_title_zh : "",
                "lat" => (float) $poi->ap_lat,
                "lng" => (float) $poi->ap_lng,
                "is_entrance" => $poi->ap_is_building_entrance,
                "is_door" => $poi->ap_is_door,
                'is_show' => $poi->ac_is_show,
                "is_exhibit" => $is_exhibit,
                "exhibit" => $poi_exhibit
            );
            $idx++;
        }
    }

    if ($row->af_enabled == "Y") {
        $floors[$key] = array(
            "id" => (int) $row->af_id,
            "number" => $row->af_number,
            "name" => $row->af_name,
            "desc" => $row->af_desc,
            "order" => $row->af_order,
            "lat" => ($row->af_bottom_left_lat + $row->af_top_right_lat) / 2,
            "lng" => ($row->af_bottom_left_lng + $row->af_top_right_lng) / 2,
            "bl_lat" => (float) $row->af_bottom_left_lat,
            "bl_lng" => (float) $row->af_bottom_left_lng,
            "tr_lat" => (float) $row->af_top_right_lat,
            "tr_lng" => (float) $row->af_top_right_lng,
            "plan_id" => ($row->af_plan_id) ? $row->af_plan_id : "",
            "is_map" => $row->af_map,
            "pois" => $pois
        );
    }
}



$ret = array(
    "result" => "true",
    "error_message" => "",
    "data" => $floors
);

if ($input_array["callback"]) {
    echo "{$input_array["callback"]}(" . json_encode($ret) . ")";
} else {
    echo json_encode($ret);
}

$this->log($page, "", 'Y');
exit();
