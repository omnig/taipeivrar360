<?php

class Upload_video extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //建立cache
        if ($this->config->item("cache_time") > 0) {
            $this->output->cache($this->config->item("cache_time"));
        }

    }

    /*
     * 讀取檔案
     */

    public function read( $filename = false) {
        $data["upload_file"] = $this->upload_lib->read_video("upload_video/{$filename}");
        

        $this->load->view("upload/read", $data);
    }

}
