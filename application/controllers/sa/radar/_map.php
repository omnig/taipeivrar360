<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line($data["menu_name"]);

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    )
);

$where_array = [
    'r_del' => 'N'
];

$model_name = "Radar_model";
$this->load->model($model_name);
$data["radar"] = $this->$model_name->get_list('', '', 0, 0, $where_array, false);

