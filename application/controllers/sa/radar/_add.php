<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("新增");

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/add")
    )
);

//取得園區
$data['config'] = $this->Common_model->get_db('config');
$data["default_lat"] = $this->site_config->c_app_lat;
$data["default_lng"] = $this->site_config->c_app_lng;

$data['category_list'] = $this->Common_model->get_db('ap_category', ['ac_enabled' => 'Y']);
$data['ar_list'] = $this->Common_model->get_db('department_ar', ['tdar_state' => 'Y', 'tdar_del' => 'N']);
$data["department_list"] = $this->Common_model->get_db("tp_admin_group", []);
