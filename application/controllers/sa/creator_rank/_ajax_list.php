<?php

include_once "__config.php";

$model_name = ucfirst($data['table_name']) . '_model';
$this->load->model($model_name);

//傳入值接收
$input_array = array(
    "limit" => $this->input->post('length'),
    "skip" => $this->input->post('start'),
    "draw" => intval($this->input->get_post('draw')),
    "columns" => $this->input->post('columns'),
    "order" => $this->input->post('order'),
    "filters" => $this->input->post('filters'),
    "customActionType" => $this->input->post('customActionType'),
    "customActionName" => $this->input->post('customActionName'),
    "id" => $this->input->post('id')
);


//多筆資料管理
if ($input_array["customActionType"] == "group_action" && $input_array['id']) {
    $where_array = array(
        "{$data["field_prefix"]}_id" => $input_array['id']
    );

    switch ($input_array['customActionName']) {
        case 'enable' : //啟用
            $update_array = array(
                "{$data["field_prefix"]}_enabled" => 'Y'
            );
            $this->Common_model->update_db($data["table_name"], $update_array, $where_array);
            break;
        case 'disable' :    //停用
            $update_array = array(
                "{$data["field_prefix"]}_enabled" => 'N'
            );
            $this->Common_model->update_db($data["table_name"], $update_array, $where_array);
            break;
        case 'del' :    //刪除
            $this->Common_model->delete_db($data["table_name"], $where_array);
            break;
    }

    $records["customActionStatus"] = "OK";
    $records["customActionMessage"] = "變更完成!";
}

//資料分頁設定
$input_array['limit'] = intval($input_array['limit']) < 0 ? 0 : intval($input_array['limit']);
$input_array['skip'] = intval($this->input->get_post('start'));

$where_array = array(

);

//依傳入參數過濾
if ($input_array['filters']) {
    $concat_where_string = "";

    foreach ($input_array['filters'] as $field_name => $value) {
        if ($value == '') {
            continue;
        }

        $where_string = "";

        switch ($field_name) {
            //比對完全相同的值
            case "shares_sum" :
            case "collections_sum":
            case "creator_rank":
            case "upload_sum":
            case "views_sum":
            case "type":
                $where_string = "{$field_name} = '{$value}'";
                break;
            //全文搜尋
            case "u_id" :
            case "name" :
            case "user_name":
                $where_string = "{$field_name} LIKE '%{$value}%'";
                break;

            //其它特殊搜尋遇濾方式
            case 'first_from' :
                $where_string = "firsr_create_timestamp >= '{$value} 00:00:00'";
                break;
            case 'last_from' :
                $where_string = "last_create_timestamp >= '{$value} 00:00:00'";
                break;
            case 'first_to' :
                $where_string = "firsr_create_timestamp <= '{$value} 23:59:59'";
                break;
            case 'last_to' :
                $where_string = "last_create_timestamp >= '{$value} 00:00:00'";
                break;
        }

        if ($concat_where_string == '') {
            $concat_where_string = $where_string;
        } else {
            $concat_where_string = "$concat_where_string AND $where_string";
        }
    }

    if ($concat_where_string != '') {
        $where_array[] = $concat_where_string;
    }
}

//排序欄位，對映order的排序欄位
$sort_fields = array(
    false,
    false,
    "user_name",
    "firsr_create_timestamp",
    "last_create_timestamp",
    "shares_sum",
    "collections_sum",
    "views_sum",
    "upload_sum",
    "creator_rank",
    false
);

$order_by = '';
if (isset($input_array['order'])) {
    foreach ($input_array['order'] as $row) {
        if ($sort_fields[$row['column']]) {


            if ($order_by == '') {

                $order_by = "{$sort_fields[$row['column']]} {$row['dir']}";
                continue;
            }

            $order_by = "{$order_by},{$sort_fields[$row['column']]} {$row['dir']}";
        }
    }
}

//計算資料總數
$iTotalRecords = count($this->$model_name->get_union_list($order_by,'',$input_array['limit'], $input_array['skip'], $where_array, false));


//從資料庫取得本次資料
$ret = $this->$model_name->get_union_list($order_by, '', $input_array['limit'], $input_array['skip'], $where_array, false);



//回傳值
$records = array();
$records["data"] = array();



$edit_url = base_url("{$this->controller_name}/{$data['table_name']}/edit");
$del_url = base_url("{$this->controller_name}/{$data['table_name']}/del");

//html表格內容
foreach ($ret as $i => $row) {

    $id = $row->u_id;

    $img_url = $this->config->item("server_base_url");

    $records["data"] [] = array(
        '<input type="checkbox" name="list_checkbox[]" value="' . $id . '">',
        $i + 1,
        $row->user_name,
        $row->firsr_create_timestamp,
        $row->last_create_timestamp,
        $row->shares_sum,
        $row->collections_sum,
        $row->views_sum,
        $row->upload_sum,
        $row->creator_rank,
        "<a href='{$edit_url}?id=$id' class='btn btn-xs default blue-stripe btn-operating'><i class='fa fa-pencil'></i> 編輯排名</a>"
    );
}

$records["draw"] = $input_array["draw"];
$records["recordsTotal"] = $iTotalRecords;
$records["recordsFiltered"] = $iTotalRecords;

echo json_encode($records);
exit();
