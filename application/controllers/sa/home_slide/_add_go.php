<?php

include_once "__config.php";

// echo json_encode($_FILES);
// exit();

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = "新增";

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_order" => $this->input->post("{$data["field_prefix"]}_order"),
    "{$data["field_prefix"]}_enabled" => $this->input->post("{$data["field_prefix"]}_enabled")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

$input_array["{$data["field_prefix"]}_name"] = $this->input->post("{$data["field_prefix"]}_name");
$input_array["{$data["field_prefix"]}_link"] = $this->input->post("{$data["field_prefix"]}_link");

$insert_array = array(
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "{$data["field_prefix"]}_link" => $input_array["{$data["field_prefix"]}_link"],
    "{$data["field_prefix"]}_order" => $input_array["{$data["field_prefix"]}_order"],
    "{$data["field_prefix"]}_state" => $input_array["{$data["field_prefix"]}_enabled"]
);

//上傳圖片處理
$upload_path = "upload/home_slide/";
$file_type = 'gif|jpg|jpeg|png';
$max_size = 2048;
$resize_width = 1920;
$resize_height = 830;
$fit_type = "FIT_OUTER";
$max_width = 1920;
$max_height = 830;

//上傳圖片
$field_name = "{$data["field_prefix"]}_path";
$upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);

if (!isset($upload_result["upload_data"])) {
    //必須上傳圖片
    _alert($this->lang->line("圖片上傳失敗或沒有上傳圖片"));
    exit();
}

//上傳成功，記錄上傳檔名
$insert_array[$field_name] = $upload_result["upload_data"]["file_name"];

$this->Common_model->insert_db($data['table_name'], $insert_array);

_alert_redirect($this->lang->line("新增成功！"), base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
