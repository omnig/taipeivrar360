<?php

include_once "__config.php";

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->post("{$data["field_prefix"]}_id"),
    "{$data["field_prefix"]}_order" => $this->input->post("{$data["field_prefix"]}_order"),
    "{$data["field_prefix"]}_state" => $this->input->post("{$data["field_prefix"]}_state")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

$input_array["{$data["field_prefix"]}_name"] = $this->input->post("{$data["field_prefix"]}_name");
$input_array["{$data["field_prefix"]}_link"] = $this->input->post("{$data["field_prefix"]}_link");

//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one($data['table_name'], $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), $data["item_name"]));
    exit();
}

$update_array = array(
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "{$data["field_prefix"]}_link" => $input_array["{$data["field_prefix"]}_link"],
    "{$data["field_prefix"]}_order" => $input_array["{$data["field_prefix"]}_order"],
    "{$data["field_prefix"]}_state" => $input_array["{$data["field_prefix"]}_state"]
);

//上傳圖片處理
$upload_path = "upload/home_slide/";
$file_type = 'gif|jpg|jpeg|png';
$max_size = 2048;
$resize_width = 1920;
$resize_height = 830;
$fit_type = "FIT_OUTER";
$max_width = 1920;
$max_height = 830;

//上傳圖片
$field_name = "{$data["field_prefix"]}_path";
$upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);

if (isset($upload_result["upload_data"])) {
    //上傳成功，記錄上傳檔名
    $update_array[$field_name] = $upload_result["upload_data"]["file_name"];
} elseif ($upload_result["error"] != "upload_no_file_selected") {
    //錯誤原因若不是因為沒有上傳檔案，就要返回並顯示錯誤訊息
    $upload_error_msg_i18n = $this->lang->line("upload");
    _alert($upload_error_msg_i18n[$upload_result["error"]]);
    exit();
}

$this->Common_model->update_db($data['table_name'], $update_array, $where_array);

//刪除舊的圖片
if (isset($update_array[$field_name])) {
    $this->upload_lib->delete("upload/home_slide/" . $data[$data['table_name']]->$field_name);
}

_alert_redirect('更新成功！', base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
