<?php

include_once "__config.php";
include_once "__ga_sdk.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line($data["menu_name"]);

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    )
);


$data["ga_result"] = $ga_result;
$data["document_num"] = $this->Statistics_model->get_document_num();
$data["vrvideo_upload_num"] = $this->Statistics_model->get_vrvideo_upload_num();
$data["document_share_num"] = $this->Statistics_model->get_document_share_num();
$data["vrphoto_upload_num"] = $this->Statistics_model->get_vrphoto_upload_num();
$data["document_read_num"] = $this->Statistics_model->get_document_read_num();
$data["vr_share_num"] = $this->Statistics_model->get_vr_share_num();
$data["comments_num"] = $this->Statistics_model->get_comments_num();
$data["member_num"] = $this->Statistics_model->get_member_num();
$data["proprietary_num"] = $this->Statistics_model->get_proprietary_num();
