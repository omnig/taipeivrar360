<?php

include_once "__config.php";


//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = "編輯";

//傳入值接收
$input_array = array(
    "id" => $this->input->post("id"),
    "type" => $this->input->post("type"),
    "collection_rank" => $this->input->post("collection_rank"),
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

switch ($input_array["type"]) {
    case 'photo':
        $data['table_name'] = "tp_department_vr_photo";
        $data["field_prefix"] = "dvp";
        break;
    case 'video':
        $data['table_name'] = "tp_department_vr_video";
        $data["field_prefix"] = "dvv";
        break;
    default:
        $data['table_name'] = "tp_department_vr_photo";
        $data["field_prefix"] = "dvp";
        break;
}


//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["id"]
);

$data[$data['table_name']] = $this->Common_model->get_one($data['table_name'], $where_array);



if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), $data["item_name"]));
    exit();
}

//準備更新資料庫
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["id"]
);

$update_array = array(
    "collection_rank" => $input_array["collection_rank"],
);


$this->Common_model->update_db($data['table_name'], $update_array, $where_array);


_alert_redirect('更新成功！', base_url("{$this->controller_name}/{$data['menu_category']}"));
exit();
