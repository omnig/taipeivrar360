<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("編輯");

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/edit?{$data["field_prefix"]}_id={$this->input->get("{$data["field_prefix"]}_id")}")
    )
);

//傳入值接收
$input_array = array(
    "id" => $this->input->get("id"),
    "type" => $this->input->get("type"),
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}


switch ($input_array["type"]) {
    case 'photo':
        $data['table_name'] = "tp_department_vr_photo";
        $data["field_prefix"] = "dvp";
        break;
    case 'video':
        $data['table_name'] = "tp_department_vr_video";
        $data["field_prefix"] = "dvv";
        break;
    default:
        $data['table_name'] = "tp_department_vr_photo";
        $data["field_prefix"] = "dvp";
        break;
}

$where_array = array(
    "{$data["field_prefix"]}_id" =>$input_array["id"]
);

$data["vr_object"] = $this->Common_model->get_one($data['table_name'], $where_array);

$data["vr_object"]->type = $input_array["type"];


// echo json_encode($data["vr_object"] );
// exit;

if (!$data["vr_object"]) {
    _alert(sprintf($this->lang->line("%s不存在，請先設定%s"), $data["item_group_name"], $data["item_group_name"]));
    exit();
}
