<?php

include_once "__config.php";


//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("新增");

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['menu_category']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/add")
    )
);



$where_array = array();
$department_list = $this->Common_model->get_db("tp_admin_group", $where_array);
$data["department_list"] = $department_list;



$where_array = array(
        't_enabled'=>'Y',
        't_del'=>'N'
    );
$topic_list = $this->Common_model->get_db("tp_topic", $where_array);
$data["topic_list"] = $topic_list;

// $this->load->helper('youtube_helper');
// $access_token = $this->session->access_token['access_token'];
// $youtube_link = youtube_client_upload($access_token,$video_path,$video_title,$video_description)
// exit();
