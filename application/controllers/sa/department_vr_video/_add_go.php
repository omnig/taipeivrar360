<?php

include_once "__config.php";
use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;
ini_set("memory_limit","2048M");
set_time_limit(0);
$output_dir = "upload/vrvideo/";
// echo json_encode($_REQUEST);
// exit();

// echo json_encode($_FILES);
// exit();

//圖片上傳檔案檢查
if (!isset($_FILES["{$data["field_prefix"]}_rep_img_path"])||$_FILES["{$data["field_prefix"]}_rep_img_path"]["name"]=="") {
    //必須上傳圖片
   echo ("顯示圖片上傳失敗或沒有上傳圖片(1)");
    exit();
}


//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_name" => $this->input->post("{$data["field_prefix"]}_name"),
    "{$data["field_prefix"]}_lat" => $this->input->post("{$data["field_prefix"]}_lat"),
    "{$data["field_prefix"]}_lng" => $this->input->post("{$data["field_prefix"]}_lng"),
    "t_id" => $this->input->post("t_id"),
    "d_id" => $this->input->post("d_id")
);

//Input 中文敘述
$zh_array = array(
    "{$data["field_prefix"]}_name" => "VR360環景影片名稱",
    "{$data["field_prefix"]}_lat" => "經度",
    "{$data["field_prefix"]}_lng" => "緯度",
    "t_id" => "主題選擇",
    "d_id" => "上傳局處"
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        echo ("參數錯誤:(".$zh_array[$index].")不能為空值");
        exit();
    }
}

$input_array["{$data["field_prefix"]}_description"] = $this->input->post("{$data["field_prefix"]}_description");


$insert_array = array(
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "{$data["field_prefix"]}_description"  => $input_array["{$data["field_prefix"]}_description"],
    "{$data["field_prefix"]}_lat" => $input_array["{$data["field_prefix"]}_lat"],
    "{$data["field_prefix"]}_lng" => $input_array["{$data["field_prefix"]}_lng"],
    "t_id" => $input_array["t_id"],
    "d_id" => $input_array["d_id"],
);

//上傳圖片處理
$upload_path = "upload/image/";
$file_type = 'gif|jpg|jpeg|png';
$max_size = 1024;
$resize_width = 264;
$resize_height = 156;
$fit_type = "FIT_OUTER";
$max_width = 1920;
$max_height = 1080;

//上傳代表圖片
$field_name = "{$data["field_prefix"]}_rep_img_path";
$upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);


if (!isset($upload_result["upload_data"])) {
    //必須上傳圖片
    echo ($this->lang->line("圖片上傳失敗或沒有上傳圖片(2)"));
    exit();
}
//上傳成功，記錄上傳檔名
$insert_array["{$data["field_prefix"]}_rep_img_path"] = "upload/image/".$upload_result["upload_data"]["file_name"];

$input_array["upload_url"] = $this->input->post("upload_url");

if (isset($input_array["upload_url"])&&$input_array["upload_url"]!=NULL) {
    //處理URL上傳
    //URL 判斷
    $insert_array['dvv_youtube_link'] = $this->input->post('upload_url');
    $insert_array['dvv_youtube_link'] = $this->security->xss_clean($insert_array['dvv_youtube_link']);

    if (strpos($this->input->post('upload_url'), 'http') !== false) {

    }else{
        $insert_array['dvv_youtube_link'] = "http://".$insert_array['dvv_youtube_link'];
    }
    
}else{
    //處理上傳影片
    if (isset($_FILES['dvv_video'])) {

        $video_name =  $_FILES['dvv_video']['name'];
        $video_type = $_FILES['dvv_video']['type'];
        $video_tmp_name = $_FILES['dvv_video']['tmp_name'];
        $video_error = $_FILES['dvv_video']['error'];
        $video_size = $_FILES['dvv_video']['size'];

        //判斷是否為MP4格式
        if ($_FILES['dvv_video']['type']!="video/mp4") {
            echo json_encode("The upload file is not mp4 type.");
            exit();
        }

        //判斷影片大小是否超過限制
        $files_max_size = 300000000; //300M
        if ($_FILES['dvv_video']['size'] > $files_max_size ) {
            echo json_encode("The upload files size is over 300M .");
            exit();
        }

        //上傳影片至資料夾
        if(!is_array($_FILES["dvv_video"]["name"])) //single file
        {
            $fileName = uniqid().".mp4";

            $tmp_name = dirname($_FILES["dvv_video"]["tmp_name"])."/".basename($_FILES["dvv_video"]["tmp_name"]);


            //上傳實體到資料夾
            switch (ENVIRONMENT) {
                case 'development' :
                case 'test' :
                // 假如是在Localhost
                // 假如是在測試機
                    move_uploaded_file($tmp_name,$output_dir.$fileName);
                    break;

                case 'api' :
                // 假如是在API Server 
                    move_uploaded_file($tmp_name,$output_dir.$fileName);
                    $sftp = new SFTP('192.168.255.77');      //Prvite IP  For nmnsvs server (伺服器上測試用)
                    if (!$sftp->login('admin', 'admin123$')) {
                        throw new Exception('Login failed');
                    }else{
                        // echo "Login Success"."<br>";
                    }

                    //切換要上傳目錄夾
                    $sftp->chdir("/var/www/vrar360/upload/vrvideo/");


                    //PUT 檔案 P1: 上傳名稱  , P2: Local端檔案位置
                    $sftp->put($fileName, file_get_contents("/var/www/vrar360_api/upload/vrvideo/".$fileName));

                    break;

                case 'production' :
                // 判斷假如是在WEB Server
                // 假如是在API Server 
                    move_uploaded_file($tmp_name,$output_dir.$fileName);
                    $sftp = new SFTP('192.168.255.80');      //Prvite IP  For nmnsvs server (伺服器上測試用)
                    if (!$sftp->login('admin', 'admin123$')) {
                        throw new Exception('Login failed');
                    }else{
                        // echo "Login Success"."<br>";
                    }

                    //切換要上傳目錄夾
                    $sftp->chdir("/var/www/vrar360_api/upload/vrvideo/");

                    //PUT 檔案 P1: 上傳名稱  , P2: Local端檔案位置
                    $sftp->put($fileName, file_get_contents("/var/www/vrar360/upload/vrvideo/".$fileName));

                    break;

            }

            $ret[]= $fileName;

            $insert_array['dvv_type'] = $_FILES['dvv_video']['type'];

            //上傳至Youtube
            $this->load->helper('youtube_helper');
            $access_token = $this->session->access_token['access_token'];

            if (!$access_token) {
                echo ("請使用Google帳號登入,或Token超過時間，請重新登入");
                exit();
            }

            $youtube_link = youtube_client_upload($access_token,$output_dir.$fileName,$input_array["{$data["field_prefix"]}_name"] ,$input_array["{$data["field_prefix"]}_description"]);
            
            //新增資料至資料庫
            $insert_array["{$data["field_prefix"]}_backup"] = $output_dir.$fileName;
            $insert_array["{$data["field_prefix"]}_youtube_link"] = $youtube_link;
            $insert_array["{$data["field_prefix"]}_size"] = $_FILES['dvv_video']['size'];

            // 移除檔案指令
            //@unlink(FCPATH . $filename);

            
        }
    }else{
        echo json_encode("Not receive the file'");
        exit();
    }
}


$this->Common_model->insert_db("department_vr_video", $insert_array);
// @unlink($filename);

$dvv_id = $this->Common_model->get_insert_id();

//處理關鍵字
$tag_ary = $this->input->post("{$data["field_prefix"]}_tag");

foreach ($tag_ary as $tag_key => $tag_value) {
    //檢查Key 是否存在
    $where_array =  array('kwd_name' =>  $tag_value);
    $tag_result = $this->Common_model->get_one("keywords", $where_array);

    if ($tag_result) {
        //有的話Insert key 到relation
        $insert_array = array(
            'dvv_id' => $dvv_id,
            'kwd_id' =>$tag_result->kwd_id);
        $this->Common_model->insert_db("tp_keyword_department_vr_video", $insert_array);
    }else{
        //沒有更新

        $insert_array = array(
            'kwd_name' =>$tag_value);

        $this->Common_model->insert_db("keywords", $insert_array);

        $kwd_id = $this->Common_model->get_insert_id();

        $insert_array = array(
            'dvv_id' => $dvv_id,
            'kwd_id' =>$kwd_id);
        $this->Common_model->insert_db("tp_keyword_department_vr_video", $insert_array);
    }
    

}

_alert_redirect($this->lang->line("新增成功！"), base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
