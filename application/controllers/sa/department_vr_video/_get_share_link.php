<?php
//Get Google StreetView link
//資料表名稱
$data['menu_category'] = "department_vr_photo";
$data["table_name"] = $data['menu_category'];

//資料表欄位前置詞
$data["field_prefix"] = "dvp";


//傳入值接收
$input_array = array(
	"dvp_id" =>$this->input->get("dvp_id"),
    "photo_id" => $this->input->get("photo_id")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}



//載入helper
$this->load->helper('google_publish_helper');
//得到目前管理員access_token Session
$access_token = $this->session->access_token['access_token'];



$share_link = get_photo_id($access_token,$input_array["photo_id"]);


//更新資料庫
$where_array = array(
	'dvp_id' => $input_array['dvp_id']
);  


$update_array = array(
    "{$data["field_prefix"]}_view_link" => $share_link
);



$this->Common_model->update_db($data["table_name"], $update_array, $where_array);


echo "<script nonce=\"cm1vaw==\">alert('修改成功');</script>";
echo "<script nonce=\"cm1vaw==\">window.close();</script>";
exit();

?>