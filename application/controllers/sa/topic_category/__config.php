<?php

//menu_category用來驗證存取權限
$data['menu_category'] = "topic_category";
$data["menu_name"] = '主題類別管理';
$data['main_menu_category'] = "topic_category";

//資料表名稱
$data["table_name"] = $data['menu_category'];

//資料表欄位前置詞
$data["field_prefix"] = "tc";

//資料名稱
$data["item_name"] = $this->lang->line("分類");

//分類
$data["item_group"] = "";
$data["item_group_name"] = "";

//分類前置詞
$data["item_group_prefix"] = "";

