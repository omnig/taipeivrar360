<?php

include_once "__config.php";

$model_name = ucfirst($data['table_name']) . '_model';
$this->load->model($model_name);


//傳入值接收
$input_array = array(
    "limit" => $this->input->post('length'),
    "skip" => $this->input->post('start'),
    "draw" => intval($this->input->get_post('draw')),
    "columns" => $this->input->post('columns'),
    "order" => $this->input->post('order'),
    "filters" => $this->input->post('filters'),
    "customActionType" => $this->input->post('customActionType'),
    "customActionName" => $this->input->post('customActionName'),
    "id" => $this->input->post('id')
);

//多筆資料管理
if ($input_array["customActionType"] == "group_action" && $input_array['id']) {
    $where_array = array(
        "{$data["field_prefix"]}_id" => $input_array['id']
    );

    switch ($input_array['customActionName']) {
        case 'enable' : //啟用
            $update_array = array(
                "{$data["field_prefix"]}_characteristic_state" => 'Y'
            );
            $this->Common_model->update_db($data["table_name"], $update_array, $where_array);
            break;
        case 'low' : //辨識度過低
            $update_array = array(
                "{$data["field_prefix"]}_validation_result" => 'N',
                "{$data["field_prefix"]}_validation_description" => '上傳圖片無法辨識',
                "{$data["field_prefix"]}_characteristic_state" => 'L'
            );
            $this->Common_model->update_db($data["table_name"], $update_array, $where_array);
            break;
    }

    $records["customActionStatus"] = "OK";
    $records["customActionMessage"] = "變更完成!";
}

//資料分頁設定
$input_array['limit'] = intval($input_array['limit']) < 0 ? 0 : intval($input_array['limit']);
$input_array['skip'] = intval($this->input->get_post('start'));

$where_array = array(
    "{$data["field_prefix"]}_to_og" => 'Y',
    "{$data["field_prefix"]}_characteristic_state NOT" => array('Y','L')
    );

//依傳入參數過濾
if ($input_array['filters']) {
    $concat_where_string = "";

    foreach ($input_array['filters'] as $field_name => $value) {
        if ($value == '') {
            continue;
        }

        $where_string = "";

        switch ($field_name) {
            //比對完全相同的值
            case "d_id":
            case "t_id":
            $where_string = "({$field_name} = '{$value}')";
                break;

            //全文搜尋
            case "{$data["field_prefix"]}_name" :
            case "{$data["field_prefix"]}_enabled" :
                $where_string = "({$field_name} LIKE '%{$value}%')";
                break;

            //其它特殊搜尋遇濾方式
            case 'create_from' :
                $where_string = "({$data["field_prefix"]}_create_timestamp >= '{$value} 00:00:00')";
                break;
            case 'create_to' :
                $where_string = "({$data["field_prefix"]}_create_timestamp <= '{$value} 23:59:59')";
                break;
        }

        if ($concat_where_string == '') {
            $concat_where_string = $where_string;
        } else {
            $concat_where_string = "$concat_where_string AND $where_string";
        }
    }

    if ($concat_where_string != '') {
        $where_array[] = $concat_where_string;
    }
}

//排序欄位，對映order的排序欄位
$sort_fields = array(
    false, false, false,"tdar_image_path", "{$data["field_prefix"]}_name", "{$data["field_prefix"]}_description",  "tdar_content_type", "{$data["field_prefix"]}_create_timestamp","{$data["field_prefix"]}_state", false 
);
$order_by = "";
$order = '{$data["field_prefix"]}_create_timestamp ASC';

if (isset($input_array['order'] )) {
    foreach ($input_array['order'] as $row) {
        if ($sort_fields[$row['column']]) {
            if ($order_by == '') {
                $order_by = "{$sort_fields[$row['column']]} {$row['dir']}";
                continue;
            }

            $order_by = "{$order},{$sort_fields[$row['column']]} {$sort_fields[$row['dir']]}";
        }
    }
}


//計算資料總數
$iTotalRecords = $this->$model_name->get_list($order_by, '', 0, 0, $where_array, true);

//從資料庫取得本次資料
$ret = $this->$model_name->get_list($order_by, '', $input_array['limit'], $input_array['skip'], $where_array, false);

// echo json_encode($ret);
// exit();
//回傳值
$records = array();
$records["data"] = array();

$status_list = array(
    'Y' => array("success" => $this->lang->line("啟用中")),
    'N' => array("danger" => $this->lang->line("已關閉"))
);


$identification = base_url("{$this->controller_name}/department_ar/identification");
$edit_url = base_url("{$this->controller_name}/department_ar/edit");
$del_url = base_url("{$this->controller_name}/department_ar/del");

// echo json_encode($ret);
// exit;

//html表格內容
foreach ($ret as $i => $row) {
    $is_enabled = ($row->{"{$data["field_prefix"]}_state"} == 'Y') ? 'Y' : 'N';
    $status = $status_list[$is_enabled];
    

    $pattern_state = ($row->{"{$data["field_prefix"]}_characteristic_state"} == 'Y') ? 'Y' : 'N';
    
    
    if ($pattern_state=='N') {
        //代表未處理,顯示通知button
        $pattern_link = '<span class="label label-sm label-danger">尚未建立特徵值</span>';

    }else{
        //代表已處理,有特徵值
        $pattern_link = '<span class="label label-sm label-success">已建立特徵值</span>';
    }


    $content_type = $row->{"{$data["field_prefix"]}_content_type"};

    if ($content_type =="3d_model") {

        $model_url = $this->config->item('server_base_url')."upload_model/".$row->{"{$data["field_prefix"]}_content_path"};

        $content_type = $row->{"{$data["field_prefix"]}_content_type"}."<br><a href='".$model_url."' target='_blank' class='btn btn-xs default yellow-stripe btn-operating'><i class='fa fa-arrow-down'></i> 3D檔案下載</a>";

        $upload_url = base_url("{$this->controller_name}/og_ar_list");

        $content_type .= " <a href='".$upload_url."/add_wt3?id=".$row->{"{$data["field_prefix"]}_id"}."' target='_blank' class='btn btn-xs default green-stripe btn-operating'><i class='fa fa-arrow-up'></i> WT3檔案上傳</a>";

        if (!is_null($row->{"{$data["field_prefix"]}_content_wt3"})) {
           $content_type = $row->{"{$data["field_prefix"]}_content_type"}.'<br><span class="label label-sm label-success">已製作WT3檔</span>';
        }

    }

    $id = $row->{"{$data["field_prefix"]}_id"};
    $img_url = base_url("upload_image/" . $row->{"{$data["field_prefix"]}_image_path"});
    $records["data"] [] = array(
        '<input type="checkbox" name="list_checkbox[]" value="' . $id . '">',
        $i + 1,
        "<img class='img-responsive' src='{$img_url}' />",
        $row->{"{$data["field_prefix"]}_image_path"},
        $row->{"{$data["field_prefix"]}_name"},
        $row->{"{$data["field_prefix"]}_description"},
        $content_type,
        ($row->{"{$data["field_prefix"]}_create_timestamp"}) ? date("Y/m/d", strtotime($row->{"{$data["field_prefix"]}_create_timestamp"})) : '',
        '<span class="label label-sm label-' . (key($status)) . '">' . (current($status)) . '</span>',
        $pattern_link
        
    );
}

$records["draw"] = $input_array["draw"];
$records["recordsTotal"] = $iTotalRecords;
$records["recordsFiltered"] = $iTotalRecords;

echo json_encode($records);
exit();
