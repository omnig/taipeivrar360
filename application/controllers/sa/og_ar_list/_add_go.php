<?php
include_once "__config.php";
ini_set("memory_limit","2048M");
set_time_limit(0);
// echo json_encode($_REQUEST);
// exit();

// echo json_encode($_FILES);
// exit();


//application/octet-stream

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_name" => $this->input->post("{$data["field_prefix"]}_name"),
    "{$data["field_prefix"]}_lat" => $this->input->post("{$data["field_prefix"]}_lat"),
    "{$data["field_prefix"]}_lng" => $this->input->post("{$data["field_prefix"]}_lng"),
    "t_id" => $this->input->post("t_id"),
    "d_id" => $this->input->post("d_id"),
    "{$data["field_prefix"]}_state" => $this->input->post("{$data["field_prefix"]}_state"),
    "{$data["field_prefix"]}_content_type" => $this->input->post("{$data["field_prefix"]}_content_type")
);

//Input 中文敘述
$zh_array = array(
    "{$data["field_prefix"]}_name" => "AR點位名稱",
    "{$data["field_prefix"]}_lat" => "經度",
    "{$data["field_prefix"]}_lng" => "緯度",
    "t_id" => "主題選擇",
    "d_id" => "上傳局處",
    "{$data["field_prefix"]}_state" => "AR點位狀態",
    "{$data["field_prefix"]}_content_type" => "AR呈現類別",
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert("參數錯誤:(".$zh_array[$index].")不能為空值");
        exit();
    }
}

$input_array["{$data["field_prefix"]}_description"] = $this->input->post("{$data["field_prefix"]}_description");


$insert_array = array(
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "{$data["field_prefix"]}_description"  => $input_array["{$data["field_prefix"]}_description"],
    "{$data["field_prefix"]}_lat" => $input_array["{$data["field_prefix"]}_lat"],
    "{$data["field_prefix"]}_lng" => $input_array["{$data["field_prefix"]}_lng"],
    "t_id" => $input_array["t_id"],
    "d_id" => $input_array["d_id"],
    "{$data["field_prefix"]}_state" => $input_array["{$data["field_prefix"]}_state"],
    "{$data["field_prefix"]}_content_type" => $input_array["{$data["field_prefix"]}_content_type"]
);

/******************************************
上傳ＡＲ 圖片
*******************************************/
//{$data["field_prefix"]}_image_path
// //上傳圖片處理
$upload_path = "upload_image/";
$file_type = 'jpg|jpeg|png';
$max_size = 30480; //30M

//上傳代表圖片
$field_name = "{$data["field_prefix"]}_image_path";
$upload_result = $this->upload_lib->write_from_image($upload_path, $field_name, $file_type, $max_size);

if (!isset($upload_result["upload_data"])) {
    //必須上傳圖片
    _alert($this->lang->line("圖片上傳失敗或沒有上傳圖片"));
    exit();
}
//上傳成功，記錄上傳檔名
$insert_array["{$data["field_prefix"]}_image_path"] = $upload_result["upload_data"]["file_name"];


/******************************************
上傳ＡＲ 尋找圖片
*******************************************/

//{$data["field_prefix"]}_findpho_path
// //上傳圖片處理
$upload_path = "upload/image/";
$file_type = 'gif|jpg|jpeg|png';
$max_size = 4096;
$resize_width = 264;
$resize_height = 156;
$fit_type = "FIT_OUTER";
$max_width = 1920;
$max_height = 1080;

//上傳代表圖片
$field_name = "{$data["field_prefix"]}_findpho_path";
$upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);


if (!isset($upload_result["upload_data"])) {
    //必須上傳圖片
    _alert($this->lang->line("圖片上傳失敗或沒有上傳圖片"));
    exit();
}
//上傳成功，記錄上傳檔名
$insert_array["{$data["field_prefix"]}_findpho_path"] = $upload_result["upload_data"]["file_name"];




/******************************************
上傳ＡＲ 呈現內容
*******************************************/


//判斷圖/文/影/3D上傳
switch ($input_array["{$data["field_prefix"]}_content_type"]) {
    case 'text':
        # 直接上傳文字
        $input_array["{$data["field_prefix"]}_content_path"] = $this->input->post("{$data["field_prefix"]}_content_path");
        break;

    case 'image':
        # 上傳圖片路徑
        // //上傳圖片處理
        $upload_path = "upload_image/";
        $file_type = 'gif|jpg|jpeg|png';
        $max_size = 4096;
        $resize_width = 1920;
        $resize_height = 1080;
        $fit_type = "FIT_OUTER";
        $max_width = 1920;
        $max_height = 1080;

        //上傳代表圖片
        $field_name = "{$data["field_prefix"]}_content_image";
        $upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);


        if (!isset($upload_result["upload_data"])) {
            //必須上傳圖片
            _alert($this->lang->line("圖片上傳失敗或沒有上傳圖片"));
            exit();
        }
        //上傳成功，記錄上傳檔名
        $insert_array["{$data["field_prefix"]}_content_path"] = $upload_result["upload_data"]["file_name"];
        break;

    case 'video':
        # 上傳影片路徑
        $upload_path = "upload_video/";
        $file_type = 'mp4';
        $max_size = 300000;
        $field_name = "{$data["field_prefix"]}_content_video";
        $upload_result = $this->upload_lib->write_from_video($upload_path, $field_name, $file_type, $max_size);
        if (!isset($upload_result["upload_data"])) {
            //必須上傳影片
            _alert("影片上傳失敗，或沒有上傳影片");
            exit();
        }
        //上傳成功，記錄上傳檔名
        $insert_array["{$data["field_prefix"]}_content_path"] = $upload_result["upload_data"]["file_name"];
        break;

    case '3d_model':
        # 上傳3D 路徑
        $upload_path = "upload_model/";
        $file_type = 'obj|deb|fbx';
        $max_size = 300000;
        $field_name = "{$data["field_prefix"]}_content_model";
        $upload_result = $this->upload_lib->write_from_model($upload_path, $field_name, $file_type, $max_size);
        if (!isset($upload_result["upload_data"])) {
            //必須上傳影片
            _alert("3D模型上傳失敗，或沒有上傳3D模型");
            exit();
        }
        //上傳成功，記錄上傳檔名
        $insert_array["{$data["field_prefix"]}_content_path"] = $upload_result["upload_data"]["file_name"];
        $input_array["{$data["field_prefix"]}_angle"] = $this->input->post("{$data["field_prefix"]}_content_degree");

        break;

}


$this->Common_model->insert_db("department_ar", $insert_array);
// @unlink($filename);

_alert_redirect($this->lang->line("新增成功！"), base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
