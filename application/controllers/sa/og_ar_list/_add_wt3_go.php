<?php

include_once "__config.php";
ini_set("memory_limit","2048M");
set_time_limit(0);


$data['table_name'] = "og_ar_list";

$input_array = array(
    "tdar_id" => $this->input->post("ar_id")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
       _alert_redirect("參數錯誤", base_url("{$this->controller_name}/{$data['table_name']}"));
    }
}


$ext = preg_replace('/^.*\.([^.]+)$/D', '$1', $_FILES['tdar_upload']['name']);


if ($ext!="wt3") {
	_alert_redirect("上傳檔案類型錯誤", base_url("{$this->controller_name}/{$data['table_name']}"));
}



//上傳Model
$upload_result = $this->upload_lib->write_from_model('upload_model/','tdar_upload');


//儲存上傳檔名與路徑
$update_array=array(
	'tdar_content_path' => $upload_result["upload_data"]["file_name"],
	'tdar_content_wt3' => "upload_model/".$upload_result["upload_data"]["file_name"]
);

$where_array=array(
	'tdar_id' => $input_array['tdar_id']
);


$this->Common_model->update_db("department_ar", $update_array, $where_array);



_alert_redirect($this->lang->line("新增成功！"), base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
