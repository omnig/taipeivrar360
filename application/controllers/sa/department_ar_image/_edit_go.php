<?php

include_once "__config.php";
ini_set("memory_limit","2048M");
set_time_limit(0);
use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;
// echo json_encode($_FILES);
// exit();
// echo json_encode($_REQUEST);
// exit();

// 修改內容權限
$sa_group = $_SESSION["login_sa"]->sa_group;
if(in_array($sa_group, ['ENGINEER', 'WEB_DEV', 'ADMIN'])){
    $auth = 'developer';
}else{
    $auth = 'user';
}

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->post("{$data["field_prefix"]}_id"),
    "{$data["field_prefix"]}_name" => $this->input->post("{$data["field_prefix"]}_name"),
    "{$data["field_prefix"]}_state" => $this->input->post("{$data["field_prefix"]}_state"),
);

//Input 中文敘述
$zh_array = array(
    "{$data["field_prefix"]}_name" => "辨識圖名稱",
    "{$data["field_prefix"]}_state" => "狀態",
);

if ($auth === 'developer') {
    $input_array["{$data["field_prefix"]}_star"] = $this->input->post("{$data["field_prefix"]}_star");
    $input_array["{$data["field_prefix"]}_characteristic_state"] = $this->input->post("{$data["field_prefix"]}_characteristic_state");
    $zh_array["{$data["field_prefix"]}_star"] = '辨識圖星等';
    $zh_array["{$data["field_prefix"]}_characteristic_state"] = '辨識圖狀況';
}


foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        echo ("參數錯誤:(".$zh_array[$index].")不能為空值");
        exit();
    }
}

$input_array["{$data["field_prefix"]}_description"] = $this->input->post("{$data["field_prefix"]}_description");

//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one("department_ar_image", $where_array);

if (!$data[$data['table_name']]) {
    echo (sprintf($this->lang->line("這個%s不存在！"), "辨識圖"));
    exit();
}

$update_array = array(
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "{$data["field_prefix"]}_description"  => $input_array["{$data["field_prefix"]}_description"],
    "{$data["field_prefix"]}_state"  => $input_array["{$data["field_prefix"]}_state"],
    "default_tdar_id" => $this->input->post("default_tdar_id"),
    "default_ar_text" => $this->input->post("default_ar_text"),
    "default_ar_url" => $this->input->post("default_ar_url"),
    "default_ar_heigh" => $this->input->post("default_ar_heigh"),
    "default_ar_vsize" => $this->input->post("default_ar_vsize"),
    "default_cover_identify_image" => $this->input->post("default_cover_identify_image"),
);

if ($auth === 'developer') {
    $update_array["{$data["field_prefix"]}_star"] = $input_array["{$data["field_prefix"]}_star"];
    $update_array["{$data["field_prefix"]}_characteristic_state"] = $input_array["{$data["field_prefix"]}_characteristic_state"];
}

//圖片上傳檔案檢查
if($data[$data['table_name']]->{"{$data["field_prefix"]}_characteristic_state"} === 'Y' && (isset($_FILES["{$data["field_prefix"]}_image_path"]) || !empty($_FILES["{$data["field_prefix"]}_image_path"]["name"]))){
    echo (sprintf($this->lang->line("已完成鑑定，不可更換圖片")));
    exit();
}
if(in_array($data[$data['table_name']]->{"{$data["field_prefix"]}_characteristic_state"}, ['N', 'F'])) {
    if (isset($_FILES["{$data["field_prefix"]}_image_path"]) && !empty($_FILES["{$data["field_prefix"]}_image_path"]["name"])) {
        $upload_path = "upload_image/";
        $file_type = 'jpg|jpeg|png';
        $max_size = 30480; //30M

        //上傳代表圖片
        $field_name = "{$data["field_prefix"]}_image_path";
        $upload_result = $this->upload_lib->write_from_image($upload_path, $field_name, $file_type, $max_size);

        if(isset($this->session->userdata["ar_data"])){
            $update_array["{$data["field_prefix"]}_image_path"] = $this->session->userdata["ar_data"]->tdar_image_path;
        }else if (!isset($upload_result["upload_data"])) {
            //必須上傳圖片
            echo ($this->lang->line("圖片上傳失敗或沒有上傳圖片"));
            exit();
        }else{
            //上傳成功，記錄上傳檔名
            $update_array["{$data["field_prefix"]}_image_path"] = $upload_result["upload_data"]["file_name"];
        }
    }
}

//更新AR
$this->Common_model->update_db("department_ar_image", $update_array, $where_array);

_alert_redirect('更新成功！', base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
