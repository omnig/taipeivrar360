<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = $this->lang->line("編輯");

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->post("{$data["field_prefix"]}_id"),
    "d_id" => $this->input->post("d_id"),
    "{$data["field_prefix"]}_title" => $this->input->post("{$data["field_prefix"]}_title"),
    "{$data["field_prefix"]}_lat" => $this->input->post("{$data["field_prefix"]}_lat"),
    "{$data["field_prefix"]}_lng" => $this->input->post("{$data["field_prefix"]}_lng"),
    "{$data["field_prefix"]}_range" => $this->input->post("{$data["field_prefix"]}_range"),
    "{$data["field_prefix"]}_mail_enabled" => $this->input->post("{$data["field_prefix"]}_mail_enabled"),
    "{$data["field_prefix"]}_enabled" => $this->input->post("{$data["field_prefix"]}_enabled"),
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one($data['table_name'], $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), $data["item_name"]));
    exit();
}

$update_array = array(
    "{$data["field_prefix"]}_title" => $input_array["{$data["field_prefix"]}_title"],
    "d_id" => $input_array["d_id"],
    "{$data["field_prefix"]}_range" => $input_array["{$data["field_prefix"]}_range"],
    "{$data["field_prefix"]}_lat" => $input_array["{$data["field_prefix"]}_lat"],
    "{$data["field_prefix"]}_lng" => $input_array["{$data["field_prefix"]}_lng"],
    "{$data["field_prefix"]}_enabled" => $input_array["{$data["field_prefix"]}_enabled"],
    "{$data["field_prefix"]}_mail_enabled" => $input_array["{$data["field_prefix"]}_mail_enabled"],
    "tdar_id" => $this->input->post("tdar_id"),
    "{$data["field_prefix"]}_ar_active_method" => $this->input->post("{$data["field_prefix"]}_ar_active_method"),
    "dai_id" => $this->input->post("dai_id"),
    "{$data["field_prefix"]}_ar_text" => $this->input->post("{$data["field_prefix"]}_ar_text"),
    "{$data["field_prefix"]}_ar_url" => $this->input->post("{$data["field_prefix"]}_ar_url"),
    "{$data["field_prefix"]}_ar_distance" => $this->input->post("{$data["field_prefix"]}_ar_distance"),
    "{$data["field_prefix"]}_ar_heigh" => $this->input->post("{$data["field_prefix"]}_ar_heigh"),
    "{$data["field_prefix"]}_cover_identify_image" => $this->input->post("{$data["field_prefix"]}_cover_identify_image"),
    "{$data["field_prefix"]}_ar_vsize" => $this->input->post("{$data["field_prefix"]}_ar_vsize"),
    "ap_id" => $this->input->post("ap_id"),
    "{$data["field_prefix"]}_poi_notify_enabled" => $this->input->post("{$data["field_prefix"]}_poi_notify_enabled"),
);

$this->Common_model->update_db($data['table_name'], $update_array, $where_array);


if ($input_array["{$data["field_prefix"]}_mail_enabled"] == 'Y') {
    //寄發通知信
    $this->load->library('sendmail_lib');

    $data_array = $update_array;
    $data_array['id'] = $input_array["{$data["field_prefix"]}_id"];
    $data_array['title'] = $data["menu_name"];
    $data_array['act_title'] = '編輯';
    $data_array['sa_account'] = $this->login_sa->sa_account;

    //準備寄發通知信，告知系統管理者
    $mail_title = "【後台】" . $data_array['act_title'] . $data_array['title'] . "通知信_" . date('Y-m-d');
    $mail_body = $this->load->view("mail_content/sa_lbs_notice", $data_array, true);

    $this->sendmail_lib->set_sender($this->config->item("og_pm_mail"), "VRAR360管理後台系統通知");
    $this->sendmail_lib->set_content($mail_title, $mail_body);

    $this->sendmail_lib->set_receiver($this->config->item("og_pm_mail"), '系統管理員');
    $this->sendmail_lib->send_mail();
}



_alert_redirect('更新成功！', base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
