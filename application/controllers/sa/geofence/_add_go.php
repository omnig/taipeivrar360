<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = "新增";

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_title" => $this->input->post("{$data["field_prefix"]}_title"),
    "{$data["field_prefix"]}_lat" => $this->input->post("{$data["field_prefix"]}_lat"),
    "{$data["field_prefix"]}_lng" => $this->input->post("{$data["field_prefix"]}_lng"),
    "{$data["field_prefix"]}_range" => $this->input->post("{$data["field_prefix"]}_range"),
    "{$data["field_prefix"]}_mail_enabled" => $this->input->post("{$data["field_prefix"]}_mail_enabled"),
    "{$data["field_prefix"]}_enabled" => $this->input->post("{$data["field_prefix"]}_enabled"),
    "d_id" => $this->input->post("d_id")
);

foreach ($input_array as $index => $value) {
    if (empty($value)) {
        var_dump($index);
        die;
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

//準備新增資料庫
$insert_array = array(
    "{$data["field_prefix"]}_title" => $input_array["{$data["field_prefix"]}_title"],
    "d_id" => $input_array["d_id"],
    "{$data["field_prefix"]}_range" => $input_array["{$data["field_prefix"]}_range"],
    "{$data["field_prefix"]}_lat" => $input_array["{$data["field_prefix"]}_lat"],
    "{$data["field_prefix"]}_lng" => $input_array["{$data["field_prefix"]}_lng"],
    "{$data["field_prefix"]}_mail_enabled" => $input_array["{$data["field_prefix"]}_mail_enabled"],
    "{$data["field_prefix"]}_enabled" => $input_array["{$data["field_prefix"]}_enabled"],
    "tdar_id" => $this->input->post("tdar_id"),
    "{$data["field_prefix"]}_ar_active_method" => $this->input->post("{$data["field_prefix"]}_ar_active_method"),
    "dai_id" => $this->input->post("dai_id"),
    "{$data["field_prefix"]}_ar_text" => $this->input->post("{$data["field_prefix"]}_ar_text"),
    "{$data["field_prefix"]}_ar_url" => $this->input->post("{$data["field_prefix"]}_ar_url"),
    "{$data["field_prefix"]}_ar_distance" => $this->input->post("{$data["field_prefix"]}_ar_distance"),
    "{$data["field_prefix"]}_ar_heigh" => $this->input->post("{$data["field_prefix"]}_ar_heigh"),
    "{$data["field_prefix"]}_ar_vsize" => $this->input->post("{$data["field_prefix"]}_ar_vsize"),
    "ap_id" => $this->input->post("ap_id"),
    "{$data["field_prefix"]}_poi_notify_enabled" => $this->input->post("{$data["field_prefix"]}_poi_notify_enabled"),
    "{$data["field_prefix"]}_create_timestamp" => date('Y-m-d H:i:s'),
);

if($insert_array["{$data["field_prefix"]}_ar_active_method"] === '2'){
    $insert_array["{$data["field_prefix"]}_cover_identify_image"] = $this->input->post("{$data["field_prefix"]}_cover_identify_image");
}

$this->Common_model->insert_db("{$data['table_name']}", $insert_array);


if ($input_array["{$data["field_prefix"]}_mail_enabled"] == 'Y') {
    //寄發通知信
    $this->load->library('sendmail_lib');

    $data_array = $insert_array;
    $data_array['id'] = $id;
    $data_array['title'] = $data["menu_name"];
    $data_array['act_title'] = '新增';
    $data_array['sa_account'] = $this->login_sa->sa_account;

    //準備寄發通知信，告知系統管理者
    $mail_title = "【後台】" . $data_array['act_title'] . $data_array['title'] . "通知信_" . date('Y-m-d');
    $mail_body = $this->load->view("mail_content/sa_lbs_notice", $data_array, true);

    $this->sendmail_lib->set_sender($this->config->item("og_pm_mail"), "VRAR360管理後台系統通知");
    $this->sendmail_lib->set_content($mail_title, $mail_body);

    $this->sendmail_lib->set_receiver($this->config->item("og_pm_mail"), '系統管理員');
    $this->sendmail_lib->send_mail();
}


_alert_redirect($this->lang->line("新增成功！"), base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
