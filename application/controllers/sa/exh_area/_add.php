<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("新增");

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/add")
    )
);

//取得樓層資訊
$where_array = array();
$data['building'] = $this->Common_model->get_db('ap_building');
$data['floors'] = $this->Common_model->get_db('ap_floors', $where_array, 'af_number');

//取得展廳列表
$where_array = array(
    'eh_del' => 'N'
);
$data['exh_hall'] = $this->Common_model->get_db('exh_hall', $where_array);
