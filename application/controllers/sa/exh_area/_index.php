<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line($data["menu_name"]);

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    )
);

//樓層
$where_array = array(
    'af_enabled' => 'Y'
);
$data['af_floors'] = $this->Common_model->get_db('ap_floors', $where_array);

//展廳
$where_array = array(
    'eh_del' => 'N'
);
$data['exh_hall'] = $this->Common_model->get_db('exh_hall', $where_array);
