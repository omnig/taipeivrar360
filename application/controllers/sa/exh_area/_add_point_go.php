<?php

include_once "__config.php";

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->get("{$data["field_prefix"]}_id"),
    "{$data["item_group_prefix"]}_title_zh" => $this->input->get("zh"),
    "{$data["item_group_prefix"]}_enabled" => $this->input->get("enabled")
);


foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

$input_array["{$data["item_group_prefix"]}_title_en"] = $this->input->get("en");
$input_array["{$data["item_group_prefix"]}_title_jp"] = $this->input->get("jp");

//新增資料庫
$insert_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"],
    "{$data["item_group_prefix"]}_title_zh" => $input_array["{$data["item_group_prefix"]}_title_zh"],
    "{$data["item_group_prefix"]}_title_en" => $input_array["{$data["item_group_prefix"]}_title_en"],
    "{$data["item_group_prefix"]}_title_jp" => $input_array["{$data["item_group_prefix"]}_title_jp"],
    "{$data["item_group_prefix"]}_enabled" => $input_array["{$data["item_group_prefix"]}_enabled"]
);

$this->Common_model->insert_db($data['item_group'], $insert_array);


_alert_redirect('新增成功！', base_url("{$this->controller_name}/{$data['table_name']}/edit?") . 'ea_id=' . $input_array["{$data["field_prefix"]}_id"]);
exit();
