<?php

//menu_category用來驗證存取權限
$data['menu_folder'] = 'exh_hall';
$data['menu_category'] = "exh_area";
$data["menu_name"] = '展區管理';

//資料表名稱
$data["table_name"] = $data['menu_category'];

//資料表欄位前置詞
$data["field_prefix"] = "ea";

//資料名稱
$data["item_name"] = '展區';

//分類
$data["item_group"] = "exh_point";
$data["item_group_name"] = "區域";

//分類前置詞
$data["item_group_prefix"] = "ep";

//驗證權限
$this->menu_auth($data['menu_folder']);
