<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("編輯");

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/edit?{$data["field_prefix"]}_id={$this->input->get("{$data["field_prefix"]}_id")}")
    )
);

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->get("{$data["field_prefix"]}_id")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one($data['table_name'], $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("%s不存在，請先設定%s"), $data["item_group_name"], $data["item_group_name"]));
    exit();
}

$data['relation'] = $this->Common_model->get_db("{$data['table_name']}_relation", $where_array);

//取得展廳列表
$where_array = array(
    'eh_del' => 'N'
);
$data['exh_hall'] = $this->Common_model->get_db('exh_hall', $where_array);

//取得區域列表
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"],
    'ep_del' => 'N'
);
$data['exh_point'] = $this->Common_model->get_db('exh_point', $where_array);

//取得樓層資訊
$where_array = array();
$data['building'] = $this->Common_model->get_db('ap_building');
$data['floors'] = $this->Common_model->get_db('ap_floors', $where_array, 'af_number');

