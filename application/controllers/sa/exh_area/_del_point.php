<?php

include_once "__config.php";

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->get("{$data["field_prefix"]}_id"),
    "{$data["item_group_prefix"]}_id" => $this->input->get("{$data["item_group_prefix"]}_id")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"],
    "{$data["item_group_prefix"]}_id" => $input_array["{$data["item_group_prefix"]}_id"]
);

$data[$data['item_group']] = $this->Common_model->get_one($data['item_group'], $where_array);

if (!$data[$data['item_group']]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), $data["item_group_name"]));
    exit();
}

//紀錄刪除
$update_array = array(
    "{$data["item_group_prefix"]}_del" => 'Y'
);

$this->Common_model->update_db($data['item_group'], $update_array, $where_array);


_alert_redirect('刪除成功！', base_url("{$this->controller_name}/{$data['table_name']}/edit?ea_id=") . $input_array["{$data["field_prefix"]}_id"]);
exit();
