<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = "新增";

//傳入值接收
$input_array = array(
    'eh_id' => $this->input->post('eh_id'),
    "{$data["field_prefix"]}_title_zh" => $this->input->post("{$data["field_prefix"]}_title_zh"),
    "{$data["field_prefix"]}_content_zh" => $this->input->post("{$data["field_prefix"]}_content_zh"),
    'f_ids' => $this->input->post('f_ids'),
    "{$data["field_prefix"]}_enabled" => $this->input->post("{$data["field_prefix"]}_enabled")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

$input_array["{$data["field_prefix"]}_title_en"] = $this->input->post("{$data["field_prefix"]}_title_en");
$input_array["{$data["field_prefix"]}_content_en"] = $this->input->post("{$data["field_prefix"]}_content_en");
$input_array["{$data["field_prefix"]}_title_jp"] = $this->input->post("{$data["field_prefix"]}_title_jp");
$input_array["{$data["field_prefix"]}_content_jp"] = $this->input->post("{$data["field_prefix"]}_content_jp");


//檢查同建物是否重覆選取
$ab_id = array();
if ($input_array['f_ids']) {
    $where_array = array(
        'af_id' => $input_array['f_ids']
    );
    $floors = $this->Common_model->get_db('ap_floors', $where_array);
    foreach ($floors as $f) {
        $ab_id[] = $f->ab_id;
    }
    if (count($input_array['f_ids']) != count(array_unique($ab_id))) {
        _alert('相同建物不可複選樓層');
        exit();
    }
}

//準備新增資料庫
$insert_array = array(
    'eh_id' => $input_array['eh_id'],
    "{$data["field_prefix"]}_title_zh" => $input_array["{$data["field_prefix"]}_title_zh"],
    "{$data["field_prefix"]}_content_zh" => $input_array["{$data["field_prefix"]}_content_zh"],
    "{$data["field_prefix"]}_title_en" => $input_array["{$data["field_prefix"]}_title_en"],
    "{$data["field_prefix"]}_content_en" => $input_array["{$data["field_prefix"]}_content_en"],
    "{$data["field_prefix"]}_title_jp" => $input_array["{$data["field_prefix"]}_title_jp"],
    "{$data["field_prefix"]}_content_jp" => $input_array["{$data["field_prefix"]}_content_jp"],
    "{$data["field_prefix"]}_enabled" => $input_array["{$data["field_prefix"]}_enabled"]
);


//上傳圖片處理
$upload_path = "upload/{$data['menu_category']}/";
$file_type = 'gif|jpg|jpeg|png';
$max_size = 1024;
$resize_width = 1200;
$resize_height = 900;
$fit_type = "FIT_INNER";
$max_width = 2400;
$max_height = 1800;

//上傳商品圖
$field_name = "{$data["field_prefix"]}_image";
$upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);

if (isset($upload_result["upload_data"])) {
    //上傳成功，記錄上傳檔名
    $insert_array[$field_name] = $upload_result["upload_data"]["file_name"];
} elseif ($upload_result["error"] != "upload_no_file_selected") {
    //錯誤原因若不是因為沒有上傳檔案，就要返回並顯示錯誤訊息
    $upload_error_msg_i18n = $this->lang->line("upload");
    _alert($upload_error_msg_i18n[$upload_result["error"]]);
    exit();
} else {
    
}

$this->Common_model->insert_db($data['table_name'], $insert_array);
$id = $this->Common_model->get_insert_id();

//新增關聯性
foreach ($input_array['f_ids'] as $idx => $f_id) {
    $insert_array = array(
        'eh_id' => $id,
        'af_id' => $f_id
    );
    $this->Common_model->insert_db("{$data['table_name']}_relation", $insert_array);
}


_alert_redirect("新增成功！", base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
