<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = "新增";

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_title" => $this->input->post("{$data["field_prefix"]}_title"),
    "{$data["field_prefix"]}_type" => $this->input->post("{$data["field_prefix"]}_type"),
    "{$data["field_prefix"]}_content" => $this->input->post("{$data["field_prefix"]}_content"),
    "{$data["field_prefix"]}_url" => $this->input->post("{$data["field_prefix"]}_url"),
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

//準備新增資料庫
$insert_array = array(
    "{$data["field_prefix"]}_title" => $input_array["{$data["field_prefix"]}_title"],
    "{$data["field_prefix"]}_type" => $input_array["{$data["field_prefix"]}_type"],
    "{$data["field_prefix"]}_content" => $input_array["{$data["field_prefix"]}_content"],
    "{$data["field_prefix"]}_url" => $input_array["{$data["field_prefix"]}_url"],
    "is_civil" => "N",
    "d_vaild_state" => "online",
    "{$data["field_prefix"]}_uid" => 0 //管理者新增
);



//上傳圖片處理
$upload_path = "upload/image/";
$file_type = 'gif|jpg|jpeg|png';
$max_size = 4096;
$resize_width = 1024;
$resize_height = 768;
$fit_type = "FIT_OUTER";
$max_width = 1024;
$max_height = 768;

//上傳商品圖
$field_name = "{$data["field_prefix"]}_image";
$upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);

if (isset($upload_result["upload_data"])) {
    //上傳成功，記錄上傳檔名
    $insert_array[$field_name] = "upload/image/".$upload_result["upload_data"]["file_name"];
} elseif ($upload_result["error"] != "upload_no_file_selected") {
    //錯誤原因若不是因為沒有上傳檔案，就要返回並顯示錯誤訊息
    $upload_error_msg_i18n = $this->lang->line("upload");
    _alert($upload_error_msg_i18n[$upload_result["error"]]);
    exit();
} else {
    
}


$this->Common_model->insert_db($data['table_name'], $insert_array);


_alert_redirect($this->lang->line("新增成功！"), base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
