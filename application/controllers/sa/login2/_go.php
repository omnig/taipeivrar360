<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("登入");

$input_array = array(
    "account" => $this->input->post("account"),
    "password" => $this->input->post('password'),
    "captcha_id" => $this->input->post('captcha_id'),
    "captcha_word" => $this->input->post('captcha_word')
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

//驗證圖形碼
if (!$this->captcha_lib->verify_captcha($input_array["captcha_id"], $input_array["captcha_word"])) {
    _alert($this->lang->line("驗證失敗！"));
    exit();
}


$sa = $this->login($input_array["account"], $input_array["password"]);

if (!$sa) {
    _alert($this->lang->line("登入失敗"));
    exit();
}

$this->session->set_userdata("login_sa", $sa);


// echo json_encode($_SESSION);
// exit();
header("Location: " . base_url("{$this->controller_name}/home"));
exit();
