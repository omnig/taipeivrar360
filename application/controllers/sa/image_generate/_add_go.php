<?php

include_once "__config.php";



//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = "新增";

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_height" => $this->input->post("{$data["field_prefix"]}_height"),
    "{$data["field_prefix"]}_width" => $this->input->post("{$data["field_prefix"]}_width"),
    "{$data["field_prefix"]}_type" => $this->input->post("{$data["field_prefix"]}_type"),
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

$input_array["{$data["field_prefix"]}_title"] = $this->input->post("{$data["field_prefix"]}_title");


//準備新增資料庫
$insert_array = array(
    "{$data["field_prefix"]}_title" => $input_array["{$data["field_prefix"]}_title"],
    "{$data["field_prefix"]}_height" => $input_array["{$data["field_prefix"]}_height"],
    "{$data["field_prefix"]}_width" => $input_array["{$data["field_prefix"]}_width"],
    "{$data["field_prefix"]}_type" => $input_array["{$data["field_prefix"]}_type"],
);



//上傳圖片處理
$upload_path = "upload/image_genrate/";
$file_type = 'gif|jpg|jpeg|png';
$max_size = 10240;
$resize_width = $input_array["{$data["field_prefix"]}_width"];
$resize_height = $input_array["{$data["field_prefix"]}_height"];
$fit_type = $input_array["{$data["field_prefix"]}_type"];
$max_width = 4800;
$max_height = 3600;

//上傳商品圖
$field_name = "{$data["field_prefix"]}_link";
$upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);

if (isset($upload_result["upload_data"])) {
    //上傳成功，記錄上傳檔名
    $insert_array[$field_name] = $this->config->item("server_base_url")."upload/image_genrate/".$upload_result["upload_data"]["file_name"];
} elseif ($upload_result["error"] != "upload_no_file_selected") {
    //錯誤原因若不是因為沒有上傳檔案，就要返回並顯示錯誤訊息
    $upload_error_msg_i18n = $this->lang->line("upload");
    _alert($upload_error_msg_i18n[$upload_result["error"]]);
    exit();
} else {
    
}


$this->Common_model->insert_db($data['table_name'], $insert_array);


_alert_redirect($this->lang->line("新增成功！"), base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
