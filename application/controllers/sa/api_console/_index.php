<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = "API管理平台";

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    )
);

$data[$data['table_name']] = $this->site_config;


//API清單 - 取得資料
$data["topic_api"] = [
    [
        "id" => "get_topic_category",
        "name" => "取得主題類別列表"
    ],
    [
        "id" => "get_topic_group",
        "name" => "取得主題群組列表"
    ],
    [
        "id" => "get_topic",
        "name" => "取得主題列表"
    ],
];
$data["poi_api"] = [
    [
        "id" => "get_poi",
        "name" => "取得POI列表"
    ],
];
$data["geofence_api"] = [
    [
        "id" => "get_geofence",
        "name" => "取得LBS列表"
    ],
];
$data["mission_api"] = [
    [
        "id" => "get_mission",
        "name" => "取得任務列表"
    ],
    [
        "id" => "get_reward",
        "name" => "取得獎勵列表"
    ],
    [
        "id" => "get_nine_grid",
        "name" => "取得關卡狀態"
    ],
    [
        "id" => "get_mission_complete",
        "name" => "關卡通關"
    ],
    [
        "id" => "get_mission_reward",
        "name" => "領獎"
    ],
];
$data["pattern_api"] = [
    [
        "id" => "get_patterns_content",
        "name" => "取得ar列表"
    ],
];
$data["radar_api"] = [
    [
        "id" => "get_radar",
        "name" => "取得radar列表"
    ],
];
