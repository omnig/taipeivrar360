<?php

include_once "__config.php";


//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = "編輯";

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->post("{$data["field_prefix"]}_id"),
    "{$data["field_prefix"]}_title" => $this->input->post("{$data["field_prefix"]}_title"),
    "{$data["field_prefix"]}_describe" => $this->input->post("{$data["field_prefix"]}_describe"),
    "{$data["field_prefix"]}_from" => $this->input->post("{$data["field_prefix"]}_from"),
    "{$data["field_prefix"]}_status" => $this->input->post("{$data["field_prefix"]}_status")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}



//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one($data['table_name'], $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), $data["item_name"]));
    exit();
}

//準備更新資料庫
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$update_array = array(
    "{$data["field_prefix"]}_title" => $input_array["{$data["field_prefix"]}_title"],
    "{$data["field_prefix"]}_describe" => $input_array["{$data["field_prefix"]}_describe"],
    "{$data["field_prefix"]}_from" => $input_array["{$data["field_prefix"]}_from"],
    "{$data["field_prefix"]}_status" => $input_array["{$data["field_prefix"]}_status"],
);


$this->Common_model->update_db($data['table_name'], $update_array, $where_array);


_alert_redirect('修改成功！', base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
