<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = "新增";

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_title" => $this->input->post("{$data["field_prefix"]}_title"),
    "{$data["field_prefix"]}_describe" => $this->input->post("{$data["field_prefix"]}_describe"),
    "{$data["field_prefix"]}_from" => $this->input->post("{$data["field_prefix"]}_from"),
    "{$data["field_prefix"]}_status" => $this->input->post("{$data["field_prefix"]}_status"),
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

//準備新增資料庫
$insert_array = array(
    "{$data["field_prefix"]}_title" => $input_array["{$data["field_prefix"]}_title"],
    "{$data["field_prefix"]}_describe" => $input_array["{$data["field_prefix"]}_describe"],
    "{$data["field_prefix"]}_from" => $input_array["{$data["field_prefix"]}_from"],
    "{$data["field_prefix"]}_status" => $input_array["{$data["field_prefix"]}_status"],
);



$this->Common_model->insert_db($data['table_name'], $insert_array);


_alert_redirect($this->lang->line("新增成功！"), base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
