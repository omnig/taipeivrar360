<?php

//menu_category用來驗證存取權限
$data['menu_category'] = "mission";
$data["menu_name"] = $this->lang->line("任務管理");

//資料表名稱
$data["table_name"] = $data['menu_category'];

//資料表欄位前置詞
$data["field_prefix"] = "m";

/*
//資料名稱
$data["item_name"] = $this->lang->line("景點");

//分類
$data["item_group"] = "nearby_category";
$data["item_group_name"] = "景點分類";

//分類前置詞
$data["item_group_prefix"] = "nc";
*/

//驗證權限
$this->menu_auth($data['menu_category']);
