<?php
date_default_timezone_set('Asia/Taipei');
header('Content-type: text/html; charset=utf-8');

$data["menu_name"] = $this->lang->line("登入");


//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line($data["menu_name"]);

// 最最新版電子驗證系統
if (!isset($_COOKIE['PUBLIC_APP_USER_SSO_TOKEN'])) {
    _alert("無法登入");
    exit();
}

$this->load->library('taipei_sso_lib');
// 登入系統帳號，取得系統TOKEN
$appAuthRes = $this->taipei_sso_lib->systemAuthentication();
if(!$appAuthRes['success']){
    _alert("系統錯誤, ".$appAuthRes['msg']);
    exit();
}

$appAuthRes = $appAuthRes['result'];

$publicAppSSOToken = $appAuthRes['PUBLIC_APP_SSO_TOKEN'];
$privilegedAppSSOToken = $appAuthRes['PRIVILEGED_APP_SSO_TOKEN'];
$privateAppSSOToken = $appAuthRes['PRIVATE_APP_SSO_TOKEN'];

$userSSOToken = $_COOKIE['PUBLIC_APP_USER_SSO_TOKEN'];

// 驗證人員TOKEN
$userAuthRes = $this->taipei_sso_lib->userAuthentication($privilegedAppSSOToken, $userSSOToken);

if(!$userAuthRes['success']){
    _alert("驗證錯誤, ".$userAuthRes['msg']);
    exit();
}

$userAuthRes = $userAuthRes['result'];

$userNodeUUID = $userAuthRes['APP_USER_NODE_UUID'];
$companyUUID = $userAuthRes['APP_COMPANY_UUID'];

// 個人資料
$userInfo = $this->taipei_sso_lib->userInfo($privilegedAppSSOToken, $userSSOToken, $companyUUID, $userNodeUUID);

if(!$userInfo['success']){
    _alert("人員資料錯誤, ".$userInfo['msg']);
    exit();
}

$userInfo = $userInfo['result']['APP_USER_BASIC_PROFILE'];
// {
//     "APP_DEPT_NODE_UUID": "bfd3b12b-4784-430c-a2ba-bd1115bd2df2",
//     "APP_USER_LOGIN_ID": “dominic.yishinglee@gmail.com",
//     “APP_USER_CHT_NAME”: “李宜興",
//     "APP_USER_STATUS": "1"
// }

// // 部門資訊
// $deptNodeUUID = $userInfo['APP_DEPT_NODE_UUID'];
// // $deptNodeUUID = 'a12a70ad-631a-48df-a11d-02717702d378';
// // $companyUUID = 'c595a5b7-7f48-4d5d-b40a-a7096b5a223d';
// $deptInfo = $this->taipei_sso_lib->deptInfo($privilegedAppSSOToken, $companyUUID, $deptNodeUUID);

// if(!$deptInfo['success']){
//     _alert("人員資料錯誤, ".$deptInfo['msg']);
//     exit();
// }

// $deptInfo = $deptInfo['result']['APP_DEPT_BASIC_PROFILE'];

$local_userinfo = new UserInfoAuthorization();
$local_userobj = new User();
$local_userobj->setProperty("givenName", $userInfo['APP_USER_CHT_NAME']);
$local_userobj->setProperty("userPrincipalName", $userInfo['APP_USER_LOGIN_ID']);
// $local_userobj->setProperty("orgID", $deptInfo['APP_DEPT_CODE_NO']);
// $local_userobj->setProperty("depID", $deptInfo['APP_DEPT_CODE_NO']);
// $local_userobj->setProperty("orgName", $deptInfo['APP_DEPT_NAME']);
// $local_userobj->setProperty("depName", $deptInfo['APP_DEPT_NAME']);

// $local_userobj->setProperty("givenName", 'Angel Hsiang');
// $local_userobj->setProperty("userPrincipalName", "Angel");

$local_userinfo->setProperty("user", $local_userobj);
$local_userinfo->setProperty("result", "true");


$userobj = $local_userinfo->user;
//登入驗證
$sa = $this->sso_login($userobj);

if (!$sa) {
    //檢查登入局處管理
    $adm = $this->admin_sso_login($userobj);
    if (!$adm) {
        _alert_redirect("無系統功能權限，請洽系統管理員\\r數位創新中心-張先生 #51826", "https://login.gov.taipei/");
    }
    $params = '?account=' . $adm->adm_account . '&password=' . $adm->adm_password;
    $adm_redirect_url = $this->config->item('server_base_url') . '/admin/login' . $params;
    header("Location: " . $adm_redirect_url);
    exit();
}

$this->session->set_userdata("login_sa", $sa);

//紀錄log
$this->sa_log($page, $data["menu_name"], 'SSO_LOGIN', $sa->sa_id, '');

header("Location: " . base_url("{$this->controller_name}/home"));
exit();



class User {

    public $givenName;
    public $userPrincipalName;

    public function setProperty($propname, $propvalue) {
        switch ($propname) {
            case "givenName":
                $this->givenName = $propvalue;
                break;
            case "userPrincipalName":
                $this->userPrincipalName = $propvalue;
                break;
        }
    }

}

class UserInfoAuthorization {

    public $result;
    public $apid;
    public $ip;
    public $description;
    public $user;

    public function setProperty($propname, $propvalue) {
        switch ($propname) {
            case "result":
                $this->result = $propvalue;
                break;
            case "apid":
                $this->apid = $propvalue;
                break;
            case "ip":
                $this->ip = $propvalue;
                break;
            case "description":
                $this->description = $propvalue;
                break;
            case "user":
                $this->user = $propvalue;
                break;
        }
    }

}