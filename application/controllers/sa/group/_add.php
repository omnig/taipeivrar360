<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("新增");

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/add")
    )
);


$where_array = array(
    "b_enabled" => "Y"
);
//取得beacon清單
$data["beacon"] = $this->Common_model->get_db_join_by_select('b.*,p.p_id,p.p_enabled', 'beacon as b', $where_array, 'product as p', 'b.b_id=p.b_id and p.p_del="N"', 'LEFT', 'b_id', 'asc', 'b_id');
