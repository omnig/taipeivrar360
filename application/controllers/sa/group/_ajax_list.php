<?php

include_once "__config.php";

$model_name = ucfirst($data['table_name']) . '_model';
$this->load->model($model_name);

//傳入值接收
$input_array = array(
    "limit" => $this->input->post('length'),
    "skip" => $this->input->post('start'),
    "draw" => intval($this->input->get_post('draw')),
    "columns" => $this->input->post('columns'),
    "order" => $this->input->post('order'),
    "filters" => $this->input->post('filters'),
    "customActionType" => $this->input->post('customActionType'),
    "customActionName" => $this->input->post('customActionName'),
    "id" => $this->input->post('id')
);

//資料分頁設定
$input_array['limit'] = intval($input_array['limit']) < 0 ? 0 : intval($input_array['limit']);
$input_array['skip'] = intval($this->input->get_post('start'));

$where_array = array();

//依傳入參數過濾
if ($input_array['filters']) {
    $concat_where_string = "";

    foreach ($input_array['filters'] as $field_name => $value) {
        if ($value == '') {
            continue;
        }

        $where_string = "";

        switch ($field_name) {
            //比對完全相同的值
            case "{$data["field_prefix"]}_enabled" :
                $where_string = "({$field_name} = '{$value}')";
                break;
            //全文搜尋
            case "u_id":
            case "{$data["field_prefix"]}_key" :
            case "{$data["field_prefix"]}_title" :
            case "{$data["field_prefix"]}_device_id" :
                $where_string = "({$field_name} LIKE '%{$value}%')";
                break;

            //其它特殊搜尋遇濾方式
            case 'create_from' :
                $where_string = "({$data["field_prefix"]}_timestamp >= '{$value} 00:00:00')";
                break;
            case 'create_to' :
                $where_string = "({$data["field_prefix"]}_timestamp <= '{$value} 23:59:59')";
                break;
            case 'end_from' :
                $where_string = "({$data["field_prefix"]}_end_timestamp >= '{$value} 00:00:00')";
                break;
            case 'end_to' :
                $where_string = "({$data["field_prefix"]}_end_timestamp <= '{$value} 23:59:59')";
                break;
        }

        if ($concat_where_string == '') {
            $concat_where_string = $where_string;
        } else {
            $concat_where_string = "$concat_where_string AND $where_string";
        }
    }

    if ($concat_where_string != '') {
        $where_array[] = $concat_where_string;
    }
}

//排序欄位，對映order的排序欄位
$sort_fields = array(
    false,
    "{$data["field_prefix"]}_key",
    "{$data["field_prefix"]}_title",
    "{$data["field_prefix"]}_device_id",
    'count',
    "{$data["field_prefix"]}_timestamp",
    "{$data["field_prefix"]}_end_timestamp",
    "{$data["field_prefix"]}_enabled",
    false
);

$order_by = '';
foreach ($input_array['order'] as $row) {
    if ($sort_fields[$row['column']]) {
        if ($order_by == '') {
            $order_by = "{$sort_fields[$row['column']]} {$row['dir']}";
            continue;
        }

        $order_by = "{$order},{$sort_fields[$row['column']]} {$sort_fields[$row['dir']]}";
    }
}

//檢查時間已超過之群組，並更新狀態
$where_like_array = array(
    'g_end_timestamp <=' => date('Y-m-d H:i:s')
);
$update_array = array(
    'g_enabled' => 'C'
);
$this->Common_model->update_db('group', $update_array, $where_like_array);


//計算資料總數
$iTotalRecords = $this->$model_name->get_list($order_by, '', 0, 0, $where_array, true);

//從資料庫取得本次資料
$ret = $this->$model_name->get_list($order_by, '', $input_array['limit'], $input_array['skip'], $where_array, false);

$where_array = array(
        //"gr_del" => "N"
);
$group_relation = $this->Common_model->get_db("group_relation", $where_array);

//回傳值
$records = array();
$records["data"] = array();

$status_list = array(
    'Y' => array("success" => $this->lang->line("啟用中")),
    'N' => array("danger" => $this->lang->line("已關閉")),
    'C' => array("default" => '已結束')
);

$edit_url = base_url("{$this->controller_name}/{$data['table_name']}/edit");
$del_url = base_url("{$this->controller_name}/{$data['table_name']}/del");

//html表格內容
foreach ($ret as $i => $row) {
    $status = $status_list[$row->{"{$data["field_prefix"]}_enabled"}];
    $id = $row->{"{$data["field_prefix"]}_id"};
    $members = "'[ 加入時間 : 名稱 (裝置ID) ]" . "\\n";
    foreach ($group_relation as $gr) {
        if ($row->g_id == $gr->g_id) {
            $members = $members . $gr->gr_timestamp . " : " . $gr->gr_name . " (" . $gr->gr_device_id . ")";
            $members = $members . (($gr->gr_del == "Y") ? " [已離開]" : "");
            $members = $members . "\\n";
        }
    }
    $members = $members . "'";
    $records["data"] [] = array(
        $i + 1,
        $row->{"{$data["field_prefix"]}_key"},
        $row->{"{$data["field_prefix"]}_title"},
        $row->{"{$data["field_prefix"]}_device_id"},
        '<a href="javascript:alert(' . $members . ')" data-html="true" data-toggle="tooltip" data-placement="top" title="檢視成員" >' . $row->count . '</a>',
        ($row->{"{$data["field_prefix"]}_timestamp"}) ? date("Y/m/d H:i:s", strtotime($row->{"{$data["field_prefix"]}_timestamp"})) : '',
        ($row->{"{$data["field_prefix"]}_end_timestamp"}) ? date("Y/m/d H:i:s", strtotime($row->{"{$data["field_prefix"]}_end_timestamp"})) : '',
        '<span class="label label-sm label-' . (key($status)) . '">' . (current($status)) . '</span>',
        ""
    );
}

$records["draw"] = $input_array["draw"];
$records["recordsTotal"] = $iTotalRecords;
$records["recordsFiltered"] = $iTotalRecords;

echo json_encode($records);
exit();
