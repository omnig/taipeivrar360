<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = "新增";

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_title" => $this->input->post("{$data["field_prefix"]}_title"),
    "{$data["field_prefix"]}_type" => $this->input->post("{$data["field_prefix"]}_type"),
    "{$data["field_prefix"]}_enabled" => $this->input->post("{$data["field_prefix"]}_enabled"),
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

//準備新增資料庫
$insert_array = array(
    "{$data["field_prefix"]}_title" => $input_array["{$data["field_prefix"]}_title"],
    "{$data["field_prefix"]}_type" => $input_array["{$data["field_prefix"]}_type"],
    "{$data["field_prefix"]}_enabled" => $input_array["{$data["field_prefix"]}_enabled"],
);



$this->Common_model->insert_db($data['table_name'], $insert_array);


_alert_redirect($this->lang->line("新增成功！"), base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
