<?php

$where_array = array(
    "s_address !=" => '',
    "s_address IS NOT NULL",
    "(s_lat IS NULL OR s_lat='0' OR s_lon IS NULL OR s_lon='0')"
);

$store = $this->Common_model->get_db("store", $where_array);

$output_html = "";
if (!$store) {
    echo "沒有需處理的店家";
    exit();
}

foreach ($store as $row) {
    //取得經緯度
    $latlon = address_to_latlon($row->s_address);
    if ($latlon["lat"] != 0 && $latlon["lon"] != 0) {
        $update_array = array(
            "s_lat" => $latlon["lat"],
            "s_lon" => $latlon["lon"]
        );
        $where_array = array(
            "s_id" => $row->s_id
        );
        $this->Common_model->update_db("store", $update_array, $where_array);
    }

    $output_html .= "「{$row->s_address}」 = {$latlon["lat"]},{$latlon["lon"]}<br>";
}


echo $output_html;
exit();
