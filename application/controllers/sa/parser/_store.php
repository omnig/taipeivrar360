<?php

//API url
$url = "http://ur-dev.hxcld.com/acc_shop/json";

$parms = array(
    "page" => 1,
    "rp" => 100000,
    "qtype" => "c1",
    "kw" => "kw_word=&status=1"
);
//curl取得資料
$store = json_decode(curl_query($url, "post", $parms), true);

$row_data = $store["rows"];

//資料欄位對映
$field_map = array(
    "c1" => "s_name_zh",
    "c31" => "s_trade_circle",
    "c32" => "s_category",
    "c33" => "s_tel",
    "c34" => "s_address",
    "c35" => "s_open_time"
);

//寫入資料庫
foreach ($row_data as $row) {
    $insert_array = array();

    foreach ($field_map as $key => $field) {
        $insert_array[$field] = $row["cell"][$key];
    }

    //讀取圖片檔，c13:160x160 icon圖
    //c14:大圖
    if ($row["cell"]["c13"]) {
        preg_match('/ *src *= *["\']?([^"\']*)/i', $row["cell"]["c13"], $img_url);
        if (isset($img_url[0])) {
            $pos = strpos($img_url[0], '"') ? strpos($img_url[0], '"') : strpos($img_url[0], "'");
            $url = mb_substr($img_url[0], $pos + 1);

            //產生隨機filename
            $icon_filename = substr(md5(uniqid(rand())), 3, 12);
            $filename = "upload/store/{$icon_filename}";

            //圖檔寫入資料庫
            if ($this->upload_lib->write_from_url($url, $filename)) {
                //填入資料庫欄位
                $insert_array["s_icon_filename"] = $icon_filename;
            }
        }
    }
    if ($row["cell"]["c14"]) {
        preg_match('/ *src *= *["\']?([^"\']*)/i', $row["cell"]["c14"], $img_url);
        if (isset($img_url[0])) {
            $pos = strpos($img_url[0], '"') ? strpos($img_url[0], '"') : strpos($img_url[0], "'");
            $url = mb_substr($img_url[0], $pos + 1);

            //產生隨機filename
            $logo_filename = substr(md5(uniqid(rand())), 3, 12);
            $filename = "upload/store/{$logo_filename}";

            //圖檔寫入資料庫
            if ($this->upload_lib->write_from_url($url, $filename)) {
                //填入資料庫欄位
                $insert_array["s_logo_filename"] = $logo_filename;
            }
        }
    }

    $this->Common_model->insert_db("store", $insert_array);
    $store_id = $this->Common_model->get_insert_id();

    //寫入擁有者
    $owners = json_decode($row["cell"]["c7"], true);

    foreach ($owners as $one_owner) {
        if ($one_owner == "") {
            continue;
        }

        $insert_array = array(
            "s_id" => $store_id,
            "so_owner" => $one_owner
        );

        $this->Common_model->insert_db("store_owner", $insert_array);
    }
}

die(json_encode($row_data));
exit();
