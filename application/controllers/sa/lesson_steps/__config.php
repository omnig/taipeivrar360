<?php

//menu_category用來驗證存取權限
$data['menu_category'] = "lesson_steps";
$data["menu_name"] = $this->lang->line("課程管理");

//資料表名稱
$data["table_name"] = $data['menu_category'];

//資料表欄位前置詞
$data["field_prefix"] = "s";

//資料名稱
$data["item_name"] = $this->lang->line("課程");

//分類
$data["item_group"] = "";
$data["item_group_name"] = "";

//分類前置詞
$data["item_group_prefix"] = "";

//驗證權限
// $this->menu_auth($data['menu_category']);


$data["l_id"] = $this->input->get("l_id");
$where_array = array(
	"l_id" => $data["l_id"]
);
$lesson  = $this->Common_model->get_one("lesson",$where_array);
if (!$lesson) {
	 _alert(sprintf($this->lang->line("%s不存在，請先設定%s"), "課程", "課程"));
    exit();
}