<?php

include_once "__config.php";

$model_name = ucfirst($data['table_name']) . '_model';
$this->load->model($model_name);

//傳入值接收
$input_array = array(
    "limit" => $this->input->post('length'),
    "skip" => $this->input->post('start'),
    "draw" => intval($this->input->get_post('draw')),
    "columns" => $this->input->post('columns'),
    "order" => $this->input->post('order'),
    "filters" => $this->input->post('filters'),
    "customActionType" => $this->input->post('customActionType'),
    "customActionName" => $this->input->post('customActionName'),
    "id" => $this->input->post('id')
);

//多筆資料管理
if ($input_array["customActionType"] == "group_action" && $input_array['id']) {
    $where_array = array(
        "{$data["field_prefix"]}_id" => $input_array['id']
    );

    switch ($input_array['customActionName']) {
        case 'enable' : //啟用
            $act = 'UPDATE';
            $act_name = '啟用';
            $update_array = array(
                "{$data["field_prefix"]}_enabled" => 'Y'
            );
            $this->Common_model->update_db($data["table_name"], $update_array, $where_array);
            break;
        case 'disable' :    //停用
            $act = 'UPDATE';
            $act_name = '關閉';
            $update_array = array(
                "{$data["field_prefix"]}_enabled" => 'N'
            );
            $this->Common_model->update_db($data["table_name"], $update_array, $where_array);
            break;
        case 'del' :    //刪除
            $act = 'UPDATE';
            $act_name = '刪除';
            $update_array = array(
                "{$data["field_prefix"]}_del" => 'Y'
            );
            $this->Common_model->update_db($data["table_name"], $update_array, $where_array);
            break;
    }

    //紀錄log
    $log_data = $this->Common_model->get_db($data["table_name"], $where_array);
    $content_array = array();
    foreach ($log_data as $row) {
        $content_array[] = '財產編號:' . $row->{"{$data["field_prefix"]}_property_number"} . ' 辨識碼:' . $row->{"{$data["field_prefix"]}_identify_number"};
    }
    $this->sa_log($page, $data["menu_name"], $act, implode(',', $input_array['id']), "批次{$act_name}: " . implode('、', $content_array));

    $records["customActionStatus"] = "OK";
    $records["customActionMessage"] = "變更完成!";
}

//資料分頁設定
$input_array['limit'] = intval($input_array['limit']) < 0 ? 0 : intval($input_array['limit']);
$input_array['skip'] = intval($this->input->get_post('start'));

$where_array = array(
);

//依傳入參數過濾
if ($input_array['filters']) {
    $concat_where_string = "";

    foreach ($input_array['filters'] as $field_name => $value) {
        if ($value == '') {
            continue;
        }

        $where_string = "";

        switch ($field_name) {

            case "{$data["field_prefix"]}_enabled" :
                $where_string = "({$field_name} = '{$value}')";
                break;
            case "{$data["field_prefix"]}_description" :
            case "{$data["field_prefix"]}_range" :
            case "{$data["field_prefix"]}_property_number" :
            case "{$data["field_prefix"]}_identify_number" :
                $where_string = "({$field_name} LIKE '%{$value}%')";
                break;
        }

        if ($concat_where_string == '') {
            $concat_where_string = $where_string;
        } else {
            $concat_where_string = "$concat_where_string AND $where_string";
        }
    }

    if ($concat_where_string != '') {
        $where_array[] = $concat_where_string;
    }
}

//排序欄位，對映order的排序欄位
$sort_fields = array(
    false, false, "{$data["field_prefix"]}_description", "{$data["field_prefix"]}_property_number", "{$data["field_prefix"]}_identify_number", "{$data["field_prefix"]}_range", "", "{$data["field_prefix"]}_enabled", false
);

$order_by = '';
foreach ($input_array['order'] as $row) {
    if ($sort_fields[$row['column']]) {
        if ($order_by == '') {
            $order_by = "{$sort_fields[$row['column']]} {$row['dir']}";
            continue;
        }

        $order_by = "{$order},{$sort_fields[$row['column']]} {$sort_fields[$row['dir']]}";
    }
}

//計算資料總數
$iTotalRecords = $this->$model_name->get_list($order_by, '', 0, 0, $where_array, true);

//從資料庫取得本次資料
$ret = $this->$model_name->get_list($order_by, '', $input_array['limit'], $input_array['skip'], $where_array, false);

//回傳值
$records = array();
$records["data"] = array();

$status_list = array(
    'Y' => array("success" => $this->lang->line("啟用中")),
    'N' => array("danger" => $this->lang->line("已關閉"))
);

$edit_url = base_url("{$this->controller_name}/{$data['table_name']}/edit");
$del_url = base_url("{$this->controller_name}/{$data['table_name']}/del");

//html表格內容
foreach ($ret as $i => $row) {
    $id = $row->{"{$data["field_prefix"]}_id"};
    
    $is_enabled = ($row->{"{$data["field_prefix"]}_enabled"} == 'Y') ? 'Y' : 'N';

    if ($is_enabled==='N') {
        //代表未處理,顯示啟用button
        $status = '<span class="label label-sm label-default">已關閉</span>';
    }else{
        //代表已處理,顯示啟用中
        $status = '<span class="label label-sm label-success">啟用中</span>';
    }

    $records["data"] [] = array(
        '<input type="checkbox" name="list_checkbox[]" value="' . $id . '">',
        $i + 1,
        $row->{"{$data["field_prefix"]}_description"},
        $row->{"{$data["field_prefix"]}_property_number"},
        $row->{"{$data["field_prefix"]}_identify_number"},
        $row->{"{$data["field_prefix"]}_range"},
        ($row->{"{$data["field_prefix"]}_image"}) ? "<img width='100%' src='" . base_url("upload/optical_label") . "/" . $row->{"{$data["field_prefix"]}_image"} . "' >" : "",
        $status,
        "<a href='{$edit_url}?{$data["field_prefix"]}_id={$row->{"{$data["field_prefix"]}_id"}}' class='btn btn-xs default blue-stripe btn-operating'><i class='fa fa-pencil'></i> {$this->lang->line("編輯")}</a>" .
        "<a class='btn btn-xs default red-stripe btn-operating delete' data='{$row->{"{$data["field_prefix"]}_id"}}'><i class='fa fa-remove'></i> {$this->lang->line("刪除")}</a>",
    );
}

$records["draw"] = $input_array["draw"];
$records["recordsTotal"] = $iTotalRecords;
$records["recordsFiltered"] = $iTotalRecords;

echo json_encode($records);
exit();
