<?php
include_once "__config.php";
//傳入值接收
$input_array = array(
    "tdar_id" =>  $this->input->GET('tdar_id')
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        // _alert("參數錯誤,尋找不到AR點位");
        echo ("參數錯誤,尋找不到AR點位");
        exit();
    }
}



$where_array = array(
    'tdar_id' => $input_array['tdar_id'],
);

// $result = $this->Common_model->get_one("tp_department_ar", $where_array);

$result_topic = $this->Common_model->get_db_join("tp_department_ar as tda", $where_array , "topic as t" , "tda.t_id = t.t_id " , "INNER" ,"" , "ASC");


if ($result_topic[0]->ab_buid==""||$result_topic[0]->ab_buid==NULL) {
	_alert("主題未設定平面圖，請通知管理員進行後製作業設定");
	exit();
}
// echo json_encode($result_topic);
// exit;

$where_array = array(
	'ab_buid' => $result_topic[0]->ab_buid,
	'af_number' => 1
);

$result_floor =  $this->Common_model->get_db_join("ap_floors as af", $where_array , "ap_building as ab" , "af.ab_id = ab.ab_id " , "INNER" ,"" , "ASC");


if ($result_floor[0]->af_id==""||$result_floor[0]->af_id==NULL) {
	_alert("主題未設定平面圖，請通知管理員進行後製作業設定");
    exit();
}


//新增Anyplace DB
$query_info = array(
    'buid' => $result_floor[0]->ab_buid,
    'floor_name' => $result_floor[0]->af_name,
    'floor_number' => $result_floor[0]->af_number,
    'name' => $result_topic[0]->tdar_name,
    'description' => "-",
    'pois_type' => "ar point",
    'is_door' => 'false',
    'is_building_entrance' => 'false',
    'coordinates_lat' => $result_topic[0]->tdar_lat,
    'coordinates_lon' => $result_topic[0]->tdar_lng,
    'is_published' => 'false'
);


//載入helper
$this->load->helper('anyplace_helper');
$result = get_mapping_result('add', 'pois', $query_info);

if ($result['status_code'] == 200) {
    $update_array['ap_puid'] = $result['puid'];
} else if ($result['status_code'] !== 200) {
    _alert('新增失敗 [' . $result['status_code'] . ':' . $result['message'] . ']。\n\n請再試一次，或洽系統管理員。');
    exit();
}


$where_array = array(
    'tdar_id' => $input_array['tdar_id'],
);

$this->Common_model->update_db("department_ar", $update_array, $where_array);


$mail_body="AR點位資訊如下:<br>";

$mail_body.= "====================AR建物名稱為=====================<br>";
$mail_body.= "[".$result_floor[0]->ab_name."]<br>";
$mail_body.= "====================AR點位名稱為=====================<br>";
$mail_body.= "[".$result_topic[0]->tdar_name."]<br>";
$mail_body.= "====================AR經度為=====================<br>";
$mail_body.= "[".$result_topic[0]->tdar_lat."]<br>";
$mail_body.= "====================AR緯度為=====================<br>";
$mail_body.= "[".$result_topic[0]->tdar_lng."]<br>";
$mail_body.= "====================================================<br>";
$mail_body.= "<br>";
$mail_body.= "====================此為局處上傳AR點位================<br>";
$mail_body.= "====================請至圖資管理->路徑管理建立路徑連結=================<br>";
$mail_body.= $this->config->item('server_base_url')."sa/ap_route <br>";
$mail_body.= "====================並記得按更新按鈕唷================================<br>";
$mail_body.= "<br>";
$mail_body.= "<br>";
$mail_body.= "<br>";
$mail_body.= "<br>";
$mail_body.= "<br>";
$mail_body.= "<br>";



//
//寄發通知信
$this->load->library('sendmail_lib');

//mail內容必須在載入library後
$this->sendmail_lib->set_sender("no-reply@omniguider.com", "[桃園發信機制]");
$this->sendmail_lib->set_content("[桃園VRAR360] AR 點位路徑接點通知", $mail_body);



//發送email 
// $send_name = $this->config->item('og_arrd_name');
// $send_email = $this->config->item('og_arrd_mail');
// $this->sendmail_lib->set_receiver($send_email, $send_name);

// //給CC
// $send_name2 = $this->config->item('og_arrd_name2');
// $send_email2 = $this->config->item('og_arrd_mail2');
// $this->sendmail_lib->set_cc($send_email2, $send_name2);
// //
// $send_pm_name = $this->config->item('og_pm_name');
// $send_pm_email = $this->config->item('og_pm_mail');
// $this->sendmail_lib->set_cc($send_pm_name, $send_pm_email);

$receivers =  array(
    // array(
    //     'receiver_email' =>  $this->config->item('og_arrd_mail'),
    //     'receiver_name'  =>  $this->config->item('og_arrd_name'),
    // ),
    // array(
    //     'receiver_email' =>  $this->config->item('og_arrd_mail2'),
    //     'receiver_name'  =>  $this->config->item('og_arrd_name2'),
    // ),
    array(
        'receiver_email' =>  $this->config->item('og_arrd_mail1'),
        'receiver_name'  =>  $this->config->item('og_arrd_name1'),
    ),
    array(
        'receiver_email' =>  $this->config->item('og_pm_mail'),
        'receiver_name'  =>  $this->config->item('og_pm_name'),
    )
);


//寄發email
if ($this->sendmail_lib->send_multi_receiver_mail($receivers)) {
   //寄發成功 
   //更新鑑定狀態
	$update_array = array(
	    "{$data["field_prefix"]}_to_og" => 'Y'
	);
	$this->Common_model->update_db($data["table_name"], $update_array, $where_array);

	_alert_redirect('更新成功,已寄信通知管理員進行後製作業建立導引路徑！', base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
}else{
	_alert_redirect("imca.vrar寄信有問題，請開啟低安全性權線，另洽工程師", base_url("{$this->controller_name}/{$data["table_name"]}"));
	exit();
}




