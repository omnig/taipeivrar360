<?php
include_once "__config.php";
ini_set("memory_limit","2048M");
set_time_limit(0);
use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;
// echo json_encode($_REQUEST);
// exit();

// echo json_encode($_FILES);
// exit();


//application/octet-stream

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_name" => $this->input->post("{$data["field_prefix"]}_name"),
    "d_id" => $this->input->post("d_id"),
    "{$data["field_prefix"]}_content_type" => $this->input->post("{$data["field_prefix"]}_content_type"),
);

switch ($input_array["{$data["field_prefix"]}_content_type"]) {
    case 'text':
        // 不須區分存取區域
        $input_array['share_other'] = 'N';
        break;
    default:
        $input_array['share_other'] = $this->input->post("share_other");
        break;
}

//Input 中文敘述
$zh_array = array(
    "{$data["field_prefix"]}_name" => "AR名稱",
    "d_id" => "所屬局處",
    "{$data["field_prefix"]}_content_type" => "AR呈現類別",
    "share_other" => "是否於平台分享共用",
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
//        echo ("參數錯誤:(".$zh_array[$index].")不能為空值");
        echo json_encode(
            array(
                'msg' => ("參數錯誤:(".$zh_array[$index].")不能為空值"),
                'csrf_token' => $this->security->get_csrf_hash()
            )
        );
        exit();
    }
}


$input_array["{$data["field_prefix"]}_description"] = $this->input->post("{$data["field_prefix"]}_description");
$input_array["{$data["field_prefix"]}_is_transparent"] = $this->input->post("{$data["field_prefix"]}_is_transparent")??'N';

$insert_array = array(
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "{$data["field_prefix"]}_description"  => $input_array["{$data["field_prefix"]}_description"],
    "d_id" => $input_array["d_id"],
    "{$data["field_prefix"]}_content_type" => $input_array["{$data["field_prefix"]}_content_type"],
    "{$data["field_prefix"]}_validation" => "Y",
    "{$data["field_prefix"]}_state" => "N",
    "share_other" => $input_array["share_other"]
);

/******************************************
上傳ＡＲ 呈現內容
*******************************************/


//判斷圖/文/影/3D上傳
switch ("{$input_array["{$data["field_prefix"]}_content_type"]}") {
    case 'text':
        # 直接上傳文字
        $insert_array["{$data["field_prefix"]}_content_path"] = $this->input->post("{$data["field_prefix"]}_content_path");
        if($this->input->post("{$data["field_prefix"]}_content_path")===""){
            echo json_encode(
                array(
                    'msg' => '請填寫文字',
                    'csrf_token' => $this->security->get_csrf_hash()
                )
            );
            exit();
        }
        if (mb_strlen($insert_array["{$data["field_prefix"]}_content_path"], "utf-8")>21) {
            echo json_encode(
                array(
                    'msg' => '文字超過20個字',
                    'csrf_token' => $this->security->get_csrf_hash()
                )
            );
            exit();
        }
        break;

    case 'image':
        # 上傳圖片路徑
        // //上傳圖片處理
        $upload_path = "upload/image/";
        $file_type = 'gif|jpg|jpeg|png';
        $max_size = 4096;
        $resize_width = 264;
        $resize_height = 156;
        $fit_type = "FIT_OUTER";
        $max_width = 1920;
        $max_height = 1080;

        //上傳代表圖片
        $field_name = "{$data["field_prefix"]}_content_image";
        $upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);


        if (!isset($upload_result["upload_data"])) {
            //必須上傳圖片
//            echo ($this->lang->line("圖片上傳失敗或沒有上傳圖片"));
            echo json_encode(
                array(
                    'msg' => ($this->lang->line("圖片上傳失敗或沒有上傳圖片")),
                    'csrf_token' => $this->security->get_csrf_hash()
                )
            );
            exit();
        }
        //上傳成功，記錄上傳檔名
        $insert_array["{$data["field_prefix"]}_content_path"] = $upload_result["upload_data"]["file_name"];
        break;

    case 'video':
        # 上傳影片路徑
        $upload_path = "upload_video/";
        $file_type = 'mp4';
        $max_size = 300000;
        $field_name = "{$data["field_prefix"]}_content_video";
        $upload_result = $this->upload_lib->write_from_video($upload_path, $field_name, $file_type, $max_size);

        if (!isset($upload_result["upload_data"])) {
            //必須上傳影片
//            echo ("影片上傳失敗，或沒有上傳影片");
            echo json_encode(
                array(
                    'msg' => "影片上傳失敗，或沒有上傳影片",
                    'csrf_token' => $this->security->get_csrf_hash()
                )
            );
            exit();
        }
        //上傳成功，記錄上傳檔名
        $insert_array["{$data["field_prefix"]}_content_path"] = $upload_result["upload_data"]["file_name"];
        $insert_array["{$data["field_prefix"]}_is_transparent"] = $input_array["{$data["field_prefix"]}_is_transparent"];
        break;

    case 'youtube':
        # 上傳Youtube ID
        
        //記錄上傳ID
        $insert_array["{$data["field_prefix"]}_content_path"] = $this->input->post("{$data["field_prefix"]}_video_id");
        break;

    case '3d_model':
        if($this->input->post("{$data["field_prefix"]}_animation_name")===""){
            echo json_encode(
                array(
                    'msg' => '請填寫動畫名稱',
                    'csrf_token' => $this->security->get_csrf_hash()
                )
            );
            exit();
        }
        # 上傳3D 路徑
        $upload_path = "upload_model/";
        $file_type = 'obj|deb|fbx';
        $max_size = 300000;
        $field_name = "{$data["field_prefix"]}_content_model";
        $upload_result = $this->upload_lib->write_from_model($upload_path, $field_name, $file_type, $max_size);
        if (!isset($upload_result["upload_data"])) {
            //必須上傳影片
//            echo ("3D模型上傳失敗，或沒有上傳3D模型");
            echo json_encode(
                array(
                    'msg' => '3D模型上傳失敗，或沒有上傳3D模型',
                    'csrf_token' => $this->security->get_csrf_hash()
                )
            );
            exit();
        }
        //上傳成功，記錄上傳檔名
        $insert_array["{$data["field_prefix"]}_content_path"] = $upload_result["upload_data"]["file_name"];
        $insert_array["{$data["field_prefix"]}_angle"] = $this->input->post("{$data["field_prefix"]}_content_degree");
        $insert_array["{$data["field_prefix"]}_animation_name"] = $this->input->post("{$data["field_prefix"]}_animation_name");

        break;

    // case 'image/Y':
    //     # code...
    //     $insert_array["{$data["field_prefix"]}_content_type"] = "image";
    //     $insert_array["{$data["field_prefix"]}_content_path"] = $this->input->post("share_image");
    //     if ($insert_array["{$data["field_prefix"]}_content_path"]==NULL || $insert_array["{$data["field_prefix"]}_content_path"] == "") {
    //         echo ("圖片尚未選取");
    //         exit();
    //     }
    //     break;


    // case 'video/Y':
    //     # code...
    //     $insert_array["{$data["field_prefix"]}_content_type"] = "video";
    //     $insert_array["{$data["field_prefix"]}_content_path"] = $this->input->post("share_video");
    //     if ($insert_array["{$data["field_prefix"]}_content_path"]==NULL || $insert_array["{$data["field_prefix"]}_content_path"] == "") {
    //         echo json_encode($insert_array);
    //         die;
    //         echo ("影片尚未選取");
    //         exit();
    //     }
    //     break;

    // case '3d_model/Y':
    //     # code...
    //     $insert_array["{$data["field_prefix"]}_content_type"] = "3d_model";
    //     $insert_array["{$data["field_prefix"]}_content_path"] = $this->input->post("share_3d");


    //     if ($insert_array["{$data["field_prefix"]}_content_path"]==NULL || $insert_array["{$data["field_prefix"]}_content_path"] == "") {
    //         echo ("3D模型尚未選取");
    //         exit();
    //     }

    //     $where_array = array(
    //         'tdar_id' => $insert_array["{$data["field_prefix"]}_content_path"]
    //     );
    //     $old_ar = $this->Common_model->get_one("department_ar",$where_array);

    //     $insert_array["{$data["field_prefix"]}_content_path"] = $old_ar->tdar_content_path;
    //     $insert_array["{$data["field_prefix"]}_size"] = $old_ar->tdar_size;
    //     $insert_array["{$data["field_prefix"]}_angle"] = $old_ar->tdar_angle;
    //     $insert_array["{$data["field_prefix"]}_content_wt3"] = $old_ar->tdar_content_wt3;

    //     // echo json_encode($insert_array["{$data["field_prefix"]}_content_path"]);
    //     // exit;

    //     break;
}

$this->Common_model->insert_db("department_ar", $insert_array);
// @unlink($filename);

$ar_id = $this->Common_model->get_insert_id();

unset($this->session->userdata["ar_id"]);
unset($this->session->userdata["ar_data"]);

_alert_redirect($this->lang->line("新增成功！"), base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
