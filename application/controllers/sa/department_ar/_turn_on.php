<?php
include_once "__config.php";
//傳入值接收
$input_array = array(
    "tdar_id" =>  $this->input->GET('tdar_id')
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        // _alert("參數錯誤,尋找不到AR點位");
        echo ("參數錯誤,尋找不到AR點位");
        exit();
    }
}



$where_array = array(
    'tdar_id' => $input_array['tdar_id'],
);

$result_topic = $this->Common_model->get_db_join("tp_department_ar as tda", $where_array , "topic as t" , "tda.t_id = t.t_id " , "INNER" ,"" , "ASC");
// echo json_encode($_REQUEST);
// exit;

if ($result_topic[0]->ab_buid==""||$result_topic[0]->ab_buid==NULL) {
	_alert("主題未設定平面圖，請通知管理員進行後製作業設定");
	exit();
}



if ($result_topic[0]->tdar_characteristic_state=="N") {
    _alert("尚未鑑定AR圖，請通知管理員進行後製作業鑑定上傳辨識檔");
    exit();
}

// echo json_encode($result_topic);
// exit;

$where_array = array(
	'ab_buid' => $result_topic[0]->ab_buid,
	'af_number' => 1
);



$result_floor =  $this->Common_model->get_db_join("ap_floors as af", $where_array , "ap_building as ab" , "af.ab_id = ab.ab_id " , "INNER" ,"" , "ASC");


if ($result_floor[0]->af_id==""||$result_floor[0]->af_id==NULL) {
	_alert("主題未設定平面圖，請通知管理員進行後製作業設定");
    exit();
}




$where_array = array(
    'tdar_id' => $input_array['tdar_id'],
);

$update_array = array(
    'tdar_state' => 'Y'
);

$this->Common_model->update_db("department_ar", $update_array, $where_array);


_alert_redirect('更新成功！', base_url("{$this->controller_name}/{$data['table_name']}"));
exit();




