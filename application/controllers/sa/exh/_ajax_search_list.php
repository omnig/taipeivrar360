<?php

//傳入值接收
$input_array = array(
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $ret = array(
            "result" => false,
            "error_message" => ""
        );

        echo json_encode($ret);
        exit();
    }
}

//取得傳入值
$data['ep_id'] = $this->input->get('ep_id');
$data["limit"] = $this->input->get("limit");

//記入session
$this->session->set_userdata("order_ep_id", $data['ep_id']);

//取得商品清單
$where_array = array(
    'ep_id' => $data['ep_id'],
    'e_del' => 'N'
);

if (!($data["limit"])) {
    $data["limit"] = 100;
}

$oneline_exhibits = $this->Common_model->get_db('exh', $where_array, 'e_order');

//整理回傳展品欄位
$exhibits = array();
foreach ($oneline_exhibits as $row) {
    $exhibits[] = array(
        'id' => $row->e_id,
        'name' => $row->e_title_zh,
        'order' => $row->e_order,
        'enabled' => $row->e_enabled
    );
}

$ret = array(
    "result" => true,
    "data" => array(
        "exhibits" => $exhibits
    )
);

echo json_encode($ret);
exit();
