<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("新增");

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/add")
    )
);


//取得展區區域
$this->load->model('Exh_area_model');        
$data['exh_area'] = $this->Exh_area_model->get_list();
$data['exh_point'] = $this->Exh_area_model->get_area_point();

//載入POI分類
$where_array = array(
    'ac_enabled' => 'Y'
);
$data["category"] = $this->Common_model->get_db("ap_category", $where_array, 'ac_order');
