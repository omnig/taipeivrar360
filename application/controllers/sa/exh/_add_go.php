<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = "新增";

//傳入值接收
$input_array = array(
    'ep_id' => $this->input->post('ep_id'),
    "{$data["field_prefix"]}_title_zh" => $this->input->post("{$data["field_prefix"]}_title_zh"),
    "{$data["field_prefix"]}_content_zh" => $this->input->post("{$data["field_prefix"]}_content_zh"),
    "{$data["field_prefix"]}_enabled" => $this->input->post("{$data["field_prefix"]}_enabled")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

$input_array["{$data["field_prefix"]}_title_en"] = $this->input->post("{$data["field_prefix"]}_title_en");
$input_array["{$data["field_prefix"]}_content_en"] = $this->input->post("{$data["field_prefix"]}_content_en");
$input_array["{$data["field_prefix"]}_title_jp"] = $this->input->post("{$data["field_prefix"]}_title_jp");
$input_array["{$data["field_prefix"]}_content_jp"] = $this->input->post("{$data["field_prefix"]}_content_jp");
$input_array["{$data["field_prefix"]}_tour_time"] = $this->input->post("{$data["field_prefix"]}_tour_time");
$input_array["{$data["field_prefix"]}_audio"] = $this->input->post("{$data["field_prefix"]}_audio");


//準備新增資料庫
$insert_array = array(
    'ep_id' => $input_array['ep_id'],
    "{$data["field_prefix"]}_title_zh" => $input_array["{$data["field_prefix"]}_title_zh"],
    "{$data["field_prefix"]}_content_zh" => $input_array["{$data["field_prefix"]}_content_zh"],
    "{$data["field_prefix"]}_title_en" => $input_array["{$data["field_prefix"]}_title_en"],
    "{$data["field_prefix"]}_content_en" => $input_array["{$data["field_prefix"]}_content_en"],
    "{$data["field_prefix"]}_title_jp" => $input_array["{$data["field_prefix"]}_title_jp"],
    "{$data["field_prefix"]}_content_jp" => $input_array["{$data["field_prefix"]}_content_jp"],
    "{$data["field_prefix"]}_tour_time" => $input_array["{$data["field_prefix"]}_tour_time"],
    "{$data["field_prefix"]}_audio" => $input_array["{$data["field_prefix"]}_audio"],
    "{$data["field_prefix"]}_enabled" => $input_array["{$data["field_prefix"]}_enabled"]
);


//上傳圖片處理
$upload_path = "upload/exh/";
$file_type = 'gif|jpg|jpeg|png';
$max_size = 1024;
$resize_width = 1200;
$resize_height = 900;
$fit_type = "FIT_INNER";
$max_width = 2400;
$max_height = 1800;

//上傳商品圖
$field_name = "{$data["field_prefix"]}_image";
$upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);

if (isset($upload_result["upload_data"])) {
    //上傳成功，記錄上傳檔名
    $insert_array[$field_name] = $upload_result["upload_data"]["file_name"];
} elseif ($upload_result["error"] != "upload_no_file_selected") {
    //錯誤原因若不是因為沒有上傳檔案，就要返回並顯示錯誤訊息
    $upload_error_msg_i18n = $this->lang->line("upload");
    _alert($upload_error_msg_i18n[$upload_result["error"]]);
    exit();
} else {
    
}

//上傳檔案處理
$upload_path = "upload/audio/";
$file_type = 'mp3';
$max_size = 8192;

$field_name = "{$data["field_prefix"]}_audio";
$upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size);

if (isset($upload_result["upload_data"])) {
    //上傳成功，記錄上傳檔名
    $insert_array[$field_name] = $upload_result["upload_data"]["file_name"];
} elseif ($upload_result["error"] != "upload_no_file_selected") {
    //錯誤原因若不是因為沒有上傳檔案，就要返回並顯示錯誤訊息
    $upload_error_msg_i18n = $this->lang->line("upload");
    _alert($upload_error_msg_i18n[$upload_result["error"]]);
    exit();
}

$this->Common_model->insert_db($data['table_name'], $insert_array);
$e_id = $this->Common_model->get_insert_id();


//新增POI資訊
if ($this->input->post("ac_id")) {
    $input_array = array(
        'ep_id' => $this->input->post('ep_id'),
        "ap_name" => $this->input->post("{$data["field_prefix"]}_title_zh"),
        "ap_desc" => $this->input->post("{$data["field_prefix"]}_title_zh"),
        "ac_id" => $this->input->post("ac_id"),
        "ap_lat" => $this->input->post("{$data["field_prefix"]}_lat"),
        "ap_lng" => $this->input->post("{$data["field_prefix"]}_lng"),
        "ap_is_building_entrance" => $this->input->post("{$data["field_prefix"]}_is_building_entrance"),
        "ap_is_door" => $this->input->post("{$data["field_prefix"]}_is_building_entrance"),
        "ap_enabled" => $this->input->post("{$data["field_prefix"]}_enabled")
    );

    foreach ($input_array as $index => $value) {
        if ($value === null || $value === '') {
            _alert("POI參數錯誤");
            exit();
        }
    }

    //取得關聯參數
    $where_array = array(
        'ac_id' => $input_array['ac_id']
    );
    $type = $this->Common_model->get_one_field('ap_category', 'ac_title_en', $where_array);

    $this->load->model('exh_model');
    $where_array = array(
        'ep_id' => $input_array['ep_id']
    );
    $relation = $this->exh_model->get_relation('', '', 0, 0, $where_array);

    foreach ($relation as $row) {
        $insert_array = array(
            'af_id' => $row->af_id,
            "ap_name" => $input_array["ap_name"],
            "ap_desc" => $input_array["ap_desc"],
            'ac_id' => $input_array['ac_id'],
            "ap_lat" => $input_array["ap_lat"],
            "ap_lng" => $input_array["ap_lng"],
            "ap_is_building_entrance" => $input_array["ap_is_building_entrance"],
            "ap_is_door" => $input_array["ap_is_door"],
            "ap_enabled" => $input_array["ap_enabled"],
            "ap_create_timestamp" => date('Y-m-d H:i:s')
        );

        //新增Anyplace DB
        $query_info = array(
            'buid' => $row->ab_buid,
            'floor_name' => $row->af_name,
            'floor_number' => $row->af_number,
            'name' => $input_array["ap_name"],
            'description' => $input_array["ap_desc"],
            'pois_type' => $type,
            'is_door' => ($input_array["ap_is_door"] == 'Y') ? 'true' : 'false',
            'is_building_entrance' => ($input_array["ap_is_building_entrance"] == 'Y') ? 'true' : 'false',
            'coordinates_lat' => $input_array["ap_lat"],
            'coordinates_lon' => $input_array["ap_lng"],
            'is_published' => 'false'
        );

        //載入helper
        $this->load->helper('anyplace_helper');
        $result = get_mapping_result('add', 'pois', $query_info);

        if ($result['status_code'] == 200) {
            $insert_array['ap_puid'] = $result['puid'];
        } else if ($result['status_code'] !== 200) {
            _alert_redirect('POI新增失敗 [' . $result['status_code'] . ':' . $result['message'] . ']。\n\n請再試一次，或洽系統管理員。', base_url("{$this->controller_name}/{$data['table_name']}"));
            exit();
        }

        $this->Common_model->insert_db('ap_pois', $insert_array);
        $ap_id = $this->Common_model->get_insert_id();

        //新增展品關聯性
        $insert_array = array(
            'e_id' => $e_id,
            'ap_id' => $ap_id
        );
        $this->Common_model->insert_db('exh_relation', $insert_array);
    }
}


_alert_redirect($this->lang->line("新增成功！"), base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
