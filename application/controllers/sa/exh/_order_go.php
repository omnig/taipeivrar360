<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = "新增";

//傳入值接收
$input_array = array(
    'e_order' => $this->input->post('e_order[]')
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

foreach ($input_array['e_order'] as $idx => $row) {
    $update_array = array(
        "e_order" => $idx
    );
    $where_array = array(
        "e_id" => $row
    );
    $this->Common_model->update_db('exh', $update_array, $where_array);
}


_alert_redirect($this->lang->line("更新成功！"), base_url("{$this->controller_name}/{$data['table_name']}/order"));
exit();
