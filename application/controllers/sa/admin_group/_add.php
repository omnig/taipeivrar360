<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("新增");

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/add")
    )
);

//整理menu權限checkbox清單
$data['checkbox_menu_list'] = array();
foreach ($this->admin_menu_list as $main_key => $menu_item) {
    if (isset($menu_item["sub_menu"])) {  //有子menu  
        foreach ($menu_item["sub_menu"] as $sub_key => $sub_menu) {   //展開子menu  
            $data['checkbox_menu_list'][] = array(
                'title' => "{$this->lang->line($menu_item["title"])} / {$this->lang->line($sub_menu["title"])}",
                'table_name' => $sub_key
            );
        }

        continue;
    }

    //沒有子menu 
    if (!isset($menu_item["title"])) {
        continue;  //分隔項目，跳過 
    }

    $data['checkbox_menu_list'][] = array(
        'title' => "{$this->lang->line($menu_item["title"])}",
        'table_name' => $main_key
    );
}
