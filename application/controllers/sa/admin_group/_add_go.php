<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = "新增";


//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_group" => $this->input->post("{$data["field_prefix"]}_group"),
    "{$data["field_prefix"]}_order" => $this->input->post("{$data["field_prefix"]}_order"),
    "{$data["field_prefix"]}_name" => $this->input->post("{$data["field_prefix"]}_name")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

$input_array ["{$data["field_prefix"]}_phone"] = $this->input->post("{$data["field_prefix"]}_phone");
$input_array ["{$data["field_prefix"]}_website"] = $this->input->post("{$data["field_prefix"]}_website");

// echo json_encode($input_array);
// exit();
//確認有沒有重複的資料
$where_array = array(
    "{$data["field_prefix"]}_group" => $input_array["{$data["field_prefix"]}_group"]
);

$duplicated = $this->Common_model->get_one($data['table_name'], $where_array);

if ($duplicated) {
    _alert("{$this->lang->line("重複的")}{$data["item_name"]}");
    exit();
}

$insert_array = array(
    "{$data["field_prefix"]}_group" => $input_array["{$data["field_prefix"]}_group"],
    "{$data["field_prefix"]}_order" => $input_array["{$data["field_prefix"]}_order"],
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "{$data["field_prefix"]}_phone" => $input_array["{$data["field_prefix"]}_phone"],
    "{$data["field_prefix"]}_website" => $input_array["{$data["field_prefix"]}_website"]
);

$this->Common_model->insert_db($data['table_name'], $insert_array);

//刪除menu授權(如果有舊的)
$where_array = array(
    "{$data["field_prefix"]}_group" => $input_array["{$data["field_prefix"]}_group"]
);
$this->Common_model->delete_db("admin_auth", $where_array);

//新增權限設定
$input_array["selected_menu"] = $this->input->post("selected_menu");
if ($input_array["selected_menu"]) {
    foreach ($input_array["selected_menu"] as $table_name => $on) {
        $insert_array = array(
            "{$data["field_prefix"]}_group" => $input_array["{$data["field_prefix"]}_group"],
            'aa_menu_category' => $table_name
        );

        $this->Common_model->insert_db("admin_auth", $insert_array);
    }
}

_alert_redirect($this->lang->line("新增成功！"), base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
