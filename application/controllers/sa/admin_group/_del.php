<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = $this->lang->line("刪除");

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->get("{$data["field_prefix"]}_id")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data["menu_category"]] = $this->Common_model->get_one($data["menu_category"], $where_array);

if (!$data[$data["menu_category"]]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), $data["item_name"]));
    exit();
}

$this->Common_model->delete_db($data["menu_category"], $where_array);

//刪除menu授權(如果有舊的)
$where_array = array(
    "ag_group" => $data[$data["menu_category"]]->{"{$data["field_prefix"]}_group"}
);
$this->Common_model->delete_db("admin_auth", $where_array);

_alert_redirect($this->lang->line('刪除成功！'), base_url("{$this->controller_name}/{$data["menu_category"]}"));
exit();
