<?php

//menu_category用來驗證存取權限
$data['menu_category'] = "faq";
$data["menu_name"] = $this->lang->line("FAQ");

//資料表名稱
$data["table_name"] = $data['menu_category'];

//資料表欄位前置詞
$data["field_prefix"] = "f";

//資料名稱
$data["item_name"] = $this->lang->line("FAQ");

//分類
$data["item_group"] = "faq_category";
$data["item_group_name"] = $this->lang->line("FAQ分類");

//分類前置詞
$data["item_group_prefix"] = "fc";

//驗證權限
$this->menu_auth($data['menu_category']);
