<?php

include_once "__config.php";

$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->post("{$data["field_prefix"]}_id"),
    "tc_id" => $this->input->post("tc_id"),
    "{$data["field_prefix"]}_name" => $this->input->post("{$data["field_prefix"]}_name"),
    "d_id" => $this->input->post("d_id"),
    "{$data["field_prefix"]}_enabled" => $this->input->post("{$data["field_prefix"]}_enabled"),
);

//Input 中文敘述
$zh_array = array(
    "{$data["field_prefix"]}_id" => "ID",
    "tc_id" => "主題類別",
    "{$data["field_prefix"]}_name" => "主題名稱",
    "{$data["field_prefix"]}_enabled" => "主題狀態",
    "d_id" => "所屬局處"
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert("參數錯誤:(".$zh_array[$index].")不能為空值");
        exit();
    }
}

$input_array["{$data["field_prefix"]}_describe"] = $this->input->post("{$data["field_prefix"]}_describe");
$input_array["ap_ids"] = $this->input->post("ap_ids");

//檢驗字數
if(!valid_string_lenth($input_array["{$data["field_prefix"]}_describe"],100)){
    _alert("參數錯誤:主題描述不能超過100字");
    exit();
}

//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one("topic", $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), "主題"));
    exit();
}

$this->Common_model->update_db("topic_category_relation",["tc_id" => $input_array["tc_id"]], ["t_id" => $input_array["{$data["field_prefix"]}_id"]]);

$update_array = array(
    "tc_id" => $input_array["tc_id"],
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "{$data["field_prefix"]}_describe"  => $input_array["{$data["field_prefix"]}_describe" ],
    "{$data["field_prefix"]}_enabled" => $input_array["{$data["field_prefix"]}_enabled"],
    "d_id" => $input_array["d_id"],
    "ap_ids" => $input_array["ap_ids"],
);

if (isset($_FILES["{$data["field_prefix"]}_path"]) && !empty($_FILES["{$data["field_prefix"]}_path"]["name"])){
    //上傳圖片處理
    $upload_path = "upload/topic_image/";
    $file_type = 'gif|jpg|jpeg|png';
    $max_size = 1024;
    $resize_width = 264;
    $resize_height = 156;
    $fit_type = "FIT_OUTER";
    $max_width = 1920;
    $max_height = 1080;

    //上傳圖片
    $field_name = "{$data["field_prefix"]}_path";
    $upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);

    if (isset($upload_result["upload_data"])) {
        //上傳成功，記錄上傳檔名
        $update_array["t_image"] = $upload_result["upload_data"]["file_name"];
    } elseif ($upload_result["error"] != "upload_no_file_selected") {
        //錯誤原因若不是因為沒有上傳檔案，就要返回並顯示錯誤訊息
        $upload_error_msg_i18n = $this->lang->line("upload");
        _alert($upload_error_msg_i18n[$upload_result["error"]]);
        exit();
    }

    //刪除舊的圖片
    if (isset($update_array[$field_name])) {
        $this->upload_lib->delete("upload/topic_image/" . $data[$data['table_name']]->$field_name);
    }
}
$this->Common_model->update_db("topic", $update_array, $where_array);



_alert_redirect('更新成功！', base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
