<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("編輯");

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->get("{$data["field_prefix"]}_id")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one("topic", $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("%s不存在，請先設定%s"), "主題", "主題"));
    exit();
}

//get_key_word
$keywords = $this->Common_model->get_db_join("keyword_topic as kt", $where_array , "keywords as ks" , "kt.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");

foreach ($keywords as $keywords_key => $keywords_value) {
    $data['keywords'][$keywords_key] = $keywords_value->kwd_name;
}


$where_array = array();
$department_list = $this->Common_model->get_db("tp_admin_group", $where_array);
//局處列表
$data["department_list"] = $department_list;


//poi列表
$data["poi_list"] = $this->Common_model->get_db("ap_pois", []);




//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['menu_category']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/edit?{$data["field_prefix"]}_id={$this->input->get("{$data["field_prefix"]}_id")}")
    )
);


//取得主題類別ID
$this->load->model("Topic_category_model");
$topic_category = $this->Topic_category_model->get_list();
$data["topic_category"] = $topic_category;


// $topic_tc_ids = $data[$data['table_name']]->tc_id;
// $topic_tc_id_ary =  explode(",", $topic_tc_ids);

$topic_tc_id_ary = $this->Common_model->get_field("topic_category_relation", "tc_id", array("t_id"=>$data[$data['table_name']]->t_id));

// echo json_encode($topic_tc_id_ary);
// exit;

foreach ($data["topic_category"] as $key => $value) {

    $data["topic_category"][$key]->tc_select = FALSE;

    foreach ($topic_tc_id_ary as $tta_key => $tta_value) {

        if($value->tc_id==$tta_value->tc_id){
            $data["topic_category"][$key]->tc_select = TRUE;
            continue;
        }
    }
}

// echo json_encode($topic_tc_id_ary);
// echo json_encode($data["topic_category"]);
// exit();
