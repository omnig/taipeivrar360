<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = $this->lang->line("編輯");

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_service_email" => $this->input->post("{$data["field_prefix"]}_service_email"),
    "{$data["field_prefix"]}_sa_email" => $this->input->post("{$data["field_prefix"]}_sa_email"),
    "{$data["field_prefix"]}_meta_keyword" => $this->input->post("{$data["field_prefix"]}_meta_keyword"),
    "{$data["field_prefix"]}_meta_description" => $this->input->post("{$data["field_prefix"]}_meta_description"),
    "{$data["field_prefix"]}_trigger_distance" => $this->input->post("{$data["field_prefix"]}_trigger_distance"),
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤") . $index);
        exit();
    }
}

$input_array["{$data["field_prefix"]}_counter"] = $this->input->post("{$data["field_prefix"]}_counter");
$input_array["{$data["field_prefix"]}_ios_app_url"] = $this->input->post("{$data["field_prefix"]}_ios_app_url");
$input_array["{$data["field_prefix"]}_android_app_url"] = $this->input->post("{$data["field_prefix"]}_android_app_url");
$input_array["{$data["field_prefix"]}_app_lat"] = $this->input->post("{$data["field_prefix"]}_app_lat");
$input_array["{$data["field_prefix"]}_app_lng"] = $this->input->post("{$data["field_prefix"]}_app_lng");
$input_array["{$data["field_prefix"]}_web_lat"] = $this->input->post("{$data["field_prefix"]}_web_lat");
$input_array["{$data["field_prefix"]}_web_lng"] = $this->input->post("{$data["field_prefix"]}_web_lng");
$input_array["{$data["field_prefix"]}_index_text"] = $this->input->post("{$data["field_prefix"]}_index_text");

//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => 1
);

$data[$data['table_name']] = $this->Common_model->get_one($data['table_name'], $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), $data["item_name"]));
    exit();
}

$update_array = array(
    "{$data["field_prefix"]}_service_email" => $input_array["{$data["field_prefix"]}_service_email"],
    "{$data["field_prefix"]}_sa_email" => $input_array["{$data["field_prefix"]}_sa_email"],
    "{$data["field_prefix"]}_meta_keyword" => $input_array["{$data["field_prefix"]}_meta_keyword"],
    "{$data["field_prefix"]}_meta_description" => $input_array["{$data["field_prefix"]}_meta_description"],
    "{$data["field_prefix"]}_counter" => $input_array["{$data["field_prefix"]}_counter"],
    "{$data["field_prefix"]}_ios_app_url" => $input_array["{$data["field_prefix"]}_ios_app_url"],
    "{$data["field_prefix"]}_android_app_url" => $input_array["{$data["field_prefix"]}_android_app_url"],
    "{$data["field_prefix"]}_app_lat" => $input_array["{$data["field_prefix"]}_app_lat"],
    "{$data["field_prefix"]}_app_lng" => $input_array["{$data["field_prefix"]}_app_lng"],
    "{$data["field_prefix"]}_web_lat" => $input_array["{$data["field_prefix"]}_web_lat"],
    "{$data["field_prefix"]}_web_lng" => $input_array["{$data["field_prefix"]}_web_lng"],
    "{$data["field_prefix"]}_index_text" => $input_array["{$data["field_prefix"]}_index_text"],
    "{$data["field_prefix"]}_trigger_distance" => $input_array["{$data["field_prefix"]}_trigger_distance"],
);

$this->Common_model->update_db($data['table_name'], $update_array, $where_array);

_alert_redirect('更新成功！', base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
