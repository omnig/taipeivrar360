<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("登入");

if (isset($_GET['code'])) {

    //這是登出的請小心
    
    $access_token = $this->register_g_accesstoken($_GET['code']);

    //若過期會更新
    $this->check_g_accesstoken($access_token);
    // echo json_encode($access_token);
    // exit();
    $response = curl_query("https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=".$_SESSION['access_token']['access_token'],'');
    $response = json_decode($response,true);
    $_SESSION['access_token'] = $access_token;
    //測試刷新token
    // $refresh_token = $this->refresh_g_accesstoken($_SESSION['access_token']['access_token']);
    // echo json_encode($refresh_token);
    // exit();

    if(isset($_SESSION['login_sa'])&&isset($_SESSION['REDIRECT_QUERY_STRING'])){

        $redirect_url =  $_SESSION['REDIRECT_QUERY_STRING'];
        header("Location: " . base_url($redirect_url));
        exit();

    }else{
        $sa = $this->google_login($response["user_id"],$response["email"]);

        $this->session->set_userdata("login_sa", $sa);
        // echo json_encode($_SESSION);
        // exit();
        header("Location: " . base_url("{$this->controller_name}/home"));
        exit();
    }

    


}else{
    echo "請檢查網路狀態";
}

exit();


