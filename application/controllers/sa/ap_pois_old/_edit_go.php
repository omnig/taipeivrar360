<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = $this->lang->line("編輯");

//傳入值接收
$input_array = array(
    'ab_id' => $this->input->post('ab_id'),
    'af_id' => $this->input->post('af_id'),
    "{$data["field_prefix"]}_id" => $this->input->post("{$data["field_prefix"]}_id"),
    "{$data["field_prefix"]}_puid" => $this->input->post("{$data["field_prefix"]}_puid"),
    "{$data["field_prefix"]}_name" => $this->input->post("{$data["field_prefix"]}_name"),
    "{$data["field_prefix"]}_desc" => $this->input->post("{$data["field_prefix"]}_desc"),
    "ac_id" => $this->input->post("ac_id"),
    "{$data["field_prefix"]}_lat" => $this->input->post("{$data["field_prefix"]}_lat"),
    "{$data["field_prefix"]}_lng" => $this->input->post("{$data["field_prefix"]}_lng"),
    "{$data["field_prefix"]}_is_building_entrance" => $this->input->post("{$data["field_prefix"]}_is_building_entrance"),
    "{$data["field_prefix"]}_is_door" => $this->input->post("{$data["field_prefix"]}_is_building_entrance"),
    "{$data["field_prefix"]}_enabled" => $this->input->post("{$data["field_prefix"]}_enabled")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

$input_array["e_ids"] = $this->input->post("e_ids[]");
$input_array["{$data["field_prefix"]}_uid"] = $this->input->post("{$data["field_prefix"]}_uid");

//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one($data['table_name'], $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), $data["item_name"]));
    exit();
}

$update_array = array(
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "{$data["field_prefix"]}_desc" => $input_array["{$data["field_prefix"]}_desc"],
    "ac_id" => $input_array["ac_id"],
    "{$data["field_prefix"]}_lat" => $input_array["{$data["field_prefix"]}_lat"],
    "{$data["field_prefix"]}_lng" => $input_array["{$data["field_prefix"]}_lng"],
    "{$data["field_prefix"]}_is_building_entrance" => $input_array["{$data["field_prefix"]}_is_building_entrance"],
    "{$data["field_prefix"]}_is_door" => $input_array["{$data["field_prefix"]}_is_door"],
    "{$data["field_prefix"]}_enabled" => $input_array["{$data["field_prefix"]}_enabled"]
);


//取得相關資料
$where_array = array(
    'af_id' => $input_array['af_id']
);
$row = $this->Common_model->get_db_join('ap_floors as af', $where_array, 'ap_building as ab', 'ab.ab_id=af.ab_id', 'inner');
$row = $row[0];

$where_array = array(
    'ac_id' => $input_array['ac_id']
);
$type = $this->Common_model->get_one_field('ap_category', 'ac_title_en', $where_array);

//更新Anyplace DB
$query_info = array(
    'puid' => $input_array["{$data["field_prefix"]}_puid"],
    'buid' => $row->ab_buid,
    'floor_name' => $row->af_name,
    'floor_number' => $row->af_number,
    'name' => $input_array["{$data["field_prefix"]}_name"],
    'description' => $input_array["{$data["field_prefix"]}_desc"],
    'pois_type' => $type,
    'is_door' => ($input_array["{$data["field_prefix"]}_is_door"] == 'Y') ? 'true' : 'false',
    'is_building_entrance' => ($input_array["{$data["field_prefix"]}_is_building_entrance"] == 'Y') ? 'true' : 'false',
    'coordinates_lat' => $input_array["{$data["field_prefix"]}_lat"],
    'coordinates_lon' => $input_array["{$data["field_prefix"]}_lng"]
);

//載入helper
$this->load->helper('anyplace_helper');
$result = get_mapping_result('update', 'pois', $query_info);

if ($result['status_code'] != 200) {
    _alert('更新失敗 [' . $result['status_code'] . ':' . $result['message'] . ']。\n\n請再試一次，或洽系統管理員。');
    exit();
}

$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);
$this->Common_model->update_db($data['table_name'], $update_array, $where_array);

// 更新點位假如為ar_point
if($type == "ar point"){
    $where_array = array(
        'ap_puid' => $input_array["{$data["field_prefix"]}_puid"]
    );
    $update_array = array(
        'tdar_lat' => $input_array["{$data["field_prefix"]}_lat"],
        'tdar_lng' => $input_array["{$data["field_prefix"]}_lng"],
    );
    $this->Common_model->update_db("department_ar", $update_array, $where_array);
}

//刪除舊展品關聯
$where_array = array(
    'ap_id' => $input_array["{$data["field_prefix"]}_id"]
);
$this->Common_model->delete_db('exh_relation', $where_array);

//新增展品關聯性
if ($input_array['e_ids']) {
    foreach ($input_array['e_ids'] as $row) {
        $insert_array = array(
            'e_id' => $row,
            'ap_id' => $input_array["{$data["field_prefix"]}_id"]
        );
        $this->Common_model->insert_db('exh_relation', $insert_array);
    }
}

_alert_redirect('更新成功！', base_url("{$this->controller_name}/{$data['table_name']}") . '?ab_id=' . $input_array['ab_id'] . "&af_id=" . $input_array["af_id"]);
exit();
