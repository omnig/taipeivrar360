<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("編輯") . 'POI';

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['main_menu_category']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/edit?af_id={$this->input->get("af_id")}&{$data["field_prefix"]}_id={$this->input->get("{$data["field_prefix"]}_id")}")
    )
);

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->get("{$data["field_prefix"]}_id")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one($data['table_name'], $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("%s不存在，請先設定%s"), $data["item_group_name"], $data["item_group_name"]));
    exit();
}

//載入分類
$where_array = array();
$data["category"] = $this->Common_model->get_db("ap_category", $where_array, 'ac_order');

//載入展品
$this->load->model('exh_model');
$where_array = array(
    'ab.ab_id' => $this->input->get('ab_id'),
    'af.af_id' => $this->input->get('af_id')
);
$data["exhibits"] = $this->exh_model->get_exh_relation('ep_order', '', 0, 0, $where_array);

$where_array = array();
$data['exhibits_relation'] = $this->Common_model->get_db('exh_relation', $where_array);

//載入樓層地圖資訊
$where_array = array(
    'af_id' => $this->input->get('af_id')
);
$data['ap_floor'] = $this->Common_model->get_one('ap_floors', $where_array);