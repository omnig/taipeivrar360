<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("新增") . 'POI';

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/add")
    )
);


//載入分類
$where_array = array(
    'ac_enabled' => 'Y'
);
$data["category"] = $this->Common_model->get_db("ap_category", $where_array, 'ac_order');


//載入樓層地圖資訊
$where_array = array(
    'af_id' => $this->input->get('af_id')
);
$data['ap_floor'] = $this->Common_model->get_one('ap_floors', $where_array);
$data['ab_id'] = $this->input->get('ab_id');
$data['af_id'] = $this->input->get('af_id');

$data['floor_lat'] = ($data['ap_floor']->af_bottom_left_lat + $data['ap_floor']->af_top_right_lat)/2;
$data['floor_lng'] = ($data['ap_floor']->af_bottom_left_lng + $data['ap_floor']->af_top_right_lng)/2;

// echo json_encode($data['ap_floor']);
// exit();