<?php

//menu_category用來驗證存取權限
$data['menu_category'] = "ap_pois";
$data["menu_name"] = $this->lang->line("設施POI管理");
$data['main_menu_category'] = "ap_building";

//資料表名稱
$data["table_name"] = $data['menu_category'];

//資料表欄位前置詞
$data["field_prefix"] = "ap";

//資料名稱
$data["item_name"] = "POI";

//分類
$data["item_group"] = "";
$data["item_group_name"] = "";

//分類前置詞
$data["item_group_prefix"] = "";

//驗證權限
$this->menu_auth($data['main_menu_category']);
