<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = "新增";

//
//傳入值接收
$input_array = array(
    'ab_id' => $this->input->post('ab_id'),
    'af_id' => $this->input->post('af_id'),
    "{$data["field_prefix"]}_name" => $this->input->post("{$data["field_prefix"]}_name"),
    "{$data["field_prefix"]}_desc" => $this->input->post("{$data["field_prefix"]}_desc"),
    "ac_id" => $this->input->post("ac_id"),
    "{$data["field_prefix"]}_lat" => $this->input->post("{$data["field_prefix"]}_lat"),
    "{$data["field_prefix"]}_lng" => $this->input->post("{$data["field_prefix"]}_lng"),
    "{$data["field_prefix"]}_is_building_entrance" => $this->input->post("{$data["field_prefix"]}_is_building_entrance"),
    "{$data["field_prefix"]}_is_door" => $this->input->post("{$data["field_prefix"]}_is_building_entrance"),
    "{$data["field_prefix"]}_enabled" => $this->input->post("{$data["field_prefix"]}_enabled")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($index.$this->lang->line("參數錯誤"));
        exit();
    }
}

$input_array['e_ids'] = $this->input->post('e_ids[]');


$insert_array = array(
    'af_id' => $input_array['af_id'],
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "{$data["field_prefix"]}_desc" => $input_array["{$data["field_prefix"]}_desc"],
    'ac_id' => $input_array['ac_id'],
    "{$data["field_prefix"]}_lat" => $input_array["{$data["field_prefix"]}_lat"],
    "{$data["field_prefix"]}_lng" => $input_array["{$data["field_prefix"]}_lng"],
    "{$data["field_prefix"]}_is_building_entrance" => $input_array["{$data["field_prefix"]}_is_building_entrance"],
    "{$data["field_prefix"]}_is_door" => $input_array["{$data["field_prefix"]}_is_door"],
    "{$data["field_prefix"]}_enabled" => $input_array["{$data["field_prefix"]}_enabled"],
    "{$data["field_prefix"]}_create_timestamp" => date('Y-m-d H:i:s')
);

//取得相關資料
$where_array = array(
    'af_id' => $input_array['af_id']
);
$row = $this->Common_model->get_db_join('ap_floors as af', $where_array, 'ap_building as ab', 'ab.ab_id=af.ab_id', 'inner');
$row = $row[0];

$where_array = array(
    'ac_id' => $input_array['ac_id']
);
$type = $this->Common_model->get_one_field('ap_category', 'ac_title_en', $where_array);

//新增Anyplace DB
$query_info = array(
    'buid' => $row->ab_buid,
    'floor_name' => $row->af_name,
    'floor_number' => $row->af_number,
    'name' => $input_array["{$data["field_prefix"]}_name"],
    'description' => $input_array["{$data["field_prefix"]}_desc"],
    'pois_type' => $type,
    'is_door' => ($input_array["{$data["field_prefix"]}_is_door"] == 'Y') ? 'true' : 'false',
    'is_building_entrance' => ($input_array["{$data["field_prefix"]}_is_building_entrance"] == 'Y') ? 'true' : 'false',
    'coordinates_lat' => $input_array["{$data["field_prefix"]}_lat"],
    'coordinates_lon' => $input_array["{$data["field_prefix"]}_lng"],
    'is_published' => 'false'
);

//載入helper
$this->load->helper('anyplace_helper');
$result = get_mapping_result('add', 'pois', $query_info);

if ($result['status_code'] == 200) {
    $insert_array['ap_puid'] = $result['puid'];
} else if ($result['status_code'] !== 200) {
    _alert('新增失敗 [' . $result['status_code'] . ':' . $result['message'] . ']。\n\n請再試一次，或洽系統管理員。');
    exit();
}

$this->Common_model->insert_db($data['table_name'], $insert_array);
$ap_id = $this->Common_model->get_insert_id();

//新增展品關聯性
if ($input_array['e_ids']) {
    $where_array = array(
        'e_id' => $input_array['e_ids']
    );
    $er_id = $this->Common_model->get_db('exh_relation', $where_array);

    if ($er_id) {
        _alert('展品位置已存在，請重新選擇');
        exit();
    } else {
        foreach ($input_array['e_ids'] as $e_id) {
            $insert_array = array(
                'e_id' => $e_id,
                'ap_id' => $ap_id
            );
            $this->Common_model->insert_db('exh_relation', $insert_array);
        }
    }
}

_alert_redirect($this->lang->line("新增成功！"), base_url("{$this->controller_name}/{$data['table_name']}") . '?ab_id=' . $input_array['ab_id'] . '&af_id=' . $input_array['af_id']);
exit();
