<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("新增");

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_password" => $this->input->post("{$data["field_prefix"]}_password"),
    "{$data["field_prefix"]}_name" => $this->input->post("{$data["field_prefix"]}_name"),
    "{$data["field_prefix"]}_email" => $this->input->post("{$data["field_prefix"]}_account"),
    "{$data["field_prefix"]}_account" => $this->input->post("{$data["field_prefix"]}_account"),
    "{$data["field_prefix"]}_enabled" => $this->input->post("{$data["field_prefix"]}_enabled"),
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

$input_array["{$data["field_prefix"]}_phone"] = $this->input->post("{$data["field_prefix"]}_phone");

//驗證帳號規則
$this->load->helper('email');
if (!valid_email($input_array["{$data["field_prefix"]}_account"])) {
    _alert($this->lang->line("Email格式錯誤"));
    exit();
}

//驗證密碼複雜度
$match = '/^[\d\w]{6,16}$/';
if (!preg_match($match, $input_array["{$data["field_prefix"]}_password"])) {
    _alert($this->lang->line("密碼須為6~16碼的英數混合文字"));
    exit();
}

//驗證傳入email格式
$this->load->helper('email');
if (!valid_email($input_array["{$data["field_prefix"]}_email"])) {
    _alert($this->lang->line("Email格式錯誤"));
    exit();
}

//確認有沒有重複的帳號
$where_array = array(
    "{$data["field_prefix"]}_account" => $input_array["{$data["field_prefix"]}_account"]
);

$duplicated = $this->Common_model->get_one($data['table_name'], $where_array);
if ($duplicated) {
    _alert("{$this->lang->line("重複的")}{$data["item_name"]}");
    exit();
}

//確認有沒有重複的Email
$where_array = array(
    "{$data["field_prefix"]}_email" => $input_array["{$data["field_prefix"]}_email"]
);

$duplicated = $this->Common_model->get_one($data['table_name'], $where_array);
if ($duplicated) {
    _alert("{$this->lang->line("重複的")}Email");
    exit();
}

$insert_array = array(
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "{$data["field_prefix"]}_account" => $input_array["{$data["field_prefix"]}_account"],
    "{$data["field_prefix"]}_email" => $input_array["{$data["field_prefix"]}_email"],
    "{$data["field_prefix"]}_enabled" => $input_array["{$data["field_prefix"]}_enabled"],
    "{$data["field_prefix"]}_password" => $this->encrypt_password($input_array["{$data["field_prefix"]}_password"]),
    "{$data["field_prefix"]}_phone" => $input_array["{$data["field_prefix"]}_phone"],
    "{$data["field_prefix"]}_email_confirmed" => 'N'
);

$this->Common_model->insert_db($data['table_name'], $insert_array, $where_array);

$insert_id = $this->Common_model->get_insert_id();

//取得user資料
$where_array = array(
    "u_id" => $insert_id
);

$user = $this->Common_model->get_one("user", $where_array);

if (!$user) {
    $caption = "資料庫錯誤";
    $content = $this->lang->line("資料庫寫入失敗，請聯繫系統管理員。");
    $this->alert($caption, $content);
    exit();
}


_alert_redirect($this->lang->line("新增成功！"), base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
