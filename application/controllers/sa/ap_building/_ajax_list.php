<?php

include_once "__config.php";

$model_name = ucfirst($data['table_name']) . '_model';
$this->load->model($model_name);

//傳入值接收
$input_array = array(
    "limit" => $this->input->post('length'),
    "skip" => $this->input->post('start'),
    "draw" => intval($this->input->get_post('draw')),
    "columns" => $this->input->post('columns'),
    "order" => $this->input->post('order'),
    "filters" => $this->input->post('filters'),
    "customActionType" => $this->input->post('customActionType'),
    "customActionName" => $this->input->post('customActionName'),
    "id" => $this->input->post('id')
);

//多筆資料管理
if ($input_array["customActionType"] == "group_action" && $input_array['id']) {
    $where_array = array(
        "{$data["field_prefix"]}_id" => $input_array['id']
    );

    switch ($input_array['customActionName']) {
        case 'enable' : //啟用
            $update_array = array(
                "{$data["field_prefix"]}_enabled" => 'Y'
            );
            $this->Common_model->update_db($data["table_name"], $update_array, $where_array);
            break;
        case 'disable' :    //停用
            $update_array = array(
                "{$data["field_prefix"]}_enabled" => 'N'
            );
            $this->Common_model->update_db($data["table_name"], $update_array, $where_array);
            break;
        case 'del' :    //刪除
            $this->Common_model->delete_db($data["table_name"], $where_array);
            $this->Common_model->delete_db("ap_floors", $where_array);
            break;
    }

    $records["customActionStatus"] = "OK";
    $records["customActionMessage"] = "變更完成!";
}

//資料分頁設定
$input_array['limit'] = intval($input_array['limit']) < 0 ? 0 : intval($input_array['limit']);
$input_array['skip'] = intval($this->input->get_post('start'));

$where_array = array(
//    'c.c_id' => ($this->session->current_config) ? $this->session->current_config->c_id : $this->owner_c_id
//    'c.c_id' => '1'
);

//依傳入參數過濾
if ($input_array['filters']) {
    $concat_where_string = "";

    foreach ($input_array['filters'] as $field_name => $value) {
        if ($value == '') {
            continue;
        }

        $where_string = "";

        switch ($field_name) {
            //比對完全相同的值
            case "{$data["field_prefix"]}_order" :
                $where_string = "({$field_name} = '{$value}')";
                break;
            //全文搜尋
            case "{$data["field_prefix"]}_name" :
            case "{$data["field_prefix"]}_desc" :
            case "{$data["field_prefix"]}_enabled" :
                $where_string = "({$field_name} LIKE '%{$value}%')";
                break;

            //其它特殊搜尋遇濾方式
            case 'create_from' :
                $where_string = "({$data["field_prefix"]}_create_timestamp >= '{$value} 00:00:00')";
                break;
            case 'create_to' :
                $where_string = "({$data["field_prefix"]}_create_timestamp <= '{$value} 23:59:59')";
                break;
        }

        if ($concat_where_string == '') {
            $concat_where_string = $where_string;
        } else {
            $concat_where_string = "$concat_where_string AND $where_string";
        }
    }

    if ($concat_where_string != '') {
        $where_array[] = $concat_where_string;
    }
}

//排序欄位，對映order的排序欄位
$sort_fields = array(
    false, false, "{$data["field_prefix"]}_zone", "{$data["field_prefix"]}_name", "{$data["field_prefix"]}_desc", false, "{$data["field_prefix"]}_enabled", false
);

$order_by = '';
foreach ($input_array['order'] as $row) {
    if ($sort_fields[$row['column']]) {
        if ($order_by == '') {
            $order_by = "{$sort_fields[$row['column']]} {$row['dir']}";
            continue;
        }

        $order_by = "{$order},{$sort_fields[$row['column']]} {$sort_fields[$row['dir']]}";
    }
}

//計算資料總數
$iTotalRecords = $this->$model_name->get_list($order_by, '', 0, 0, $where_array, true);

//從資料庫取得本次資料
$ret = $this->$model_name->get_list($order_by, '', $input_array['limit'], $input_array['skip'], $where_array, false);

//回傳值
$records = array();
$records["data"] = array();

$status_list = array(
    'Y' => array("success" => $this->lang->line("啟用中")),
    'N' => array("danger" => $this->lang->line("已關閉"))
);

$edit_url = base_url("{$this->controller_name}/{$data['table_name']}/edit");
$del_url = base_url("{$this->controller_name}/{$data['table_name']}/del");

//html表格內容
foreach ($ret as $i => $row) {

    $af_array = implode('<br>', (explode(',', $row->af_name)));

    $id = $row->{"{$data["field_prefix"]}_id"};

    $ap_floors_url = base_url("{$this->controller_name}/ap_floors") . "?{$data["field_prefix"]}_id={$row->{"{$data["field_prefix"]}_id"}}";
    $update_floor_api = base_url("cli/ap_update_floor") . "?buid={$row->{"{$data["field_prefix"]}_buid"}}";
    $get_floors_api = base_url("locapi/get_floor") . "?b={$row->{"{$data["field_prefix"]}_id"}}";

    $records["data"] [] = array(
        '<input type="checkbox" name="list_checkbox[]" value="' . $id . '">',
        $i + 1,
        $row->{"{$data["field_prefix"]}_buid"},
        $row->{"{$data["field_prefix"]}_name"},
        $row->{"{$data["field_prefix"]}_desc"},
        $row->{"{$data["field_prefix"]}_lat"}.'<br>'.$row->{"{$data["field_prefix"]}_lng"},
        $af_array,
        "<a href='{$ap_floors_url}' class='btn btn-xs default blue-stripe btn-operating'><i class='fa fa-sitemap'></i> {$this->lang->line("管理")}{$this->lang->line("樓層資訊")}</a>",
        ""
            //."<a href='{$update_floor_api}' class='btn btn-xs default red-stripe btn-operating' target='_blank' ><i class=''></i> {$this->lang->line("更新")}</a>",
            //"<a href='{$get_floors_api}' target='_blank' class='btn btn-xs default green-stripe btn-operating'><i class=''></i>API</a>",
    );

    //Couchbase 資料更新判斷
    //建立帳號資料
//    $this->$model_name->set_couchabse_account_data($row->c_zone, array("name" => $row->c_zone));
//
//    // 更新建物資料
//    $this->$model_name->set_couchabse_building_data($row->c_zone, array("name" => $row->c_zone, "lat" => $row->c_lat, "lng" => $row->c_lng));
}

$records["draw"] = $input_array["draw"];
$records["recordsTotal"] = $iTotalRecords;
$records["recordsFiltered"] = $iTotalRecords;

echo json_encode($records);
exit();
