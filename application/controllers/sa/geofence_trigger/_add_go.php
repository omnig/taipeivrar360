<?php
// echo json_encode($_POST);
// exit;

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = "新增";

//傳入值接收
$input_array = array(
    "g_id" => $this->input->post("g_id"),
    "{$data["field_prefix"]}_title" => $this->input->post("{$data["field_prefix"]}_title"),
    "{$data["field_prefix"]}_description" => $this->input->post("{$data["field_prefix"]}_description"),
    "{$data["field_prefix"]}_start_timestamp" => $this->input->post("{$data["field_prefix"]}_start_timestamp"),
    "{$data["field_prefix"]}_end_timestamp" => $this->input->post("{$data["field_prefix"]}_end_timestamp"),
    "{$data["field_prefix"]}_target" => $this->input->post("{$data["field_prefix"]}_target"),
    "{$data["field_prefix"]}_topic" => $this->input->post("{$data["field_prefix"]}_topic"),
    "{$data["field_prefix"]}_go_point_type" => $this->input->post("{$data["field_prefix"]}_go_point_type"),
    "{$data["field_prefix"]}_point_selector" => $this->input->post("{$data["field_prefix"]}_point_selector")?$this->input->post("{$data["field_prefix"]}_point_selector"):$this->input->post("{$data["field_prefix"]}_topic"),
    "{$data["field_prefix"]}_lat" => $this->input->post("{$data["field_prefix"]}_lat"),
    "{$data["field_prefix"]}_lng" => $this->input->post("{$data["field_prefix"]}_lng"),
    "{$data["field_prefix"]}_timelimit_push" => $this->input->post("{$data["field_prefix"]}_timelimit_push"),
    "{$data["field_prefix"]}_mail_enabled" => $this->input->post("{$data["field_prefix"]}_mail_enabled"),
    "{$data["field_prefix"]}_enabled" => $this->input->post("{$data["field_prefix"]}_enabled")
);

// echo json_encode($input_array);
// exit;

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

//輸入日期限制
if (strtotime($input_array["{$data["field_prefix"]}_start_timestamp"]) - (strtotime(date('Y-m-d') . '+1 day')) < 0) {
    //_alert('限制輸入隔天之後的日期');
    //exit();
}
if ((strtotime($input_array["{$data["field_prefix"]}_start_timestamp"]) - strtotime($input_array["{$data["field_prefix"]}_end_timestamp"])) >= 0) {
    _alert('開始日期不得等於或小於結束日期');
    exit();
}

$input_array["{$data["field_prefix"]}_title_en"] = $this->input->post("{$data["field_prefix"]}_title_en");
$input_array["{$data["field_prefix"]}_title_jp"] = $this->input->post("{$data["field_prefix"]}_title_jp");
$input_array["{$data["field_prefix"]}_description_en"] = $this->input->post("{$data["field_prefix"]}_description_en");
$input_array["{$data["field_prefix"]}_description_jp"] = $this->input->post("{$data["field_prefix"]}_description_jp");


//準備新增資料庫
$insert_array = array(
    "{$data["field_prefix"]}_title" => $input_array["{$data["field_prefix"]}_title"],
    "{$data["field_prefix"]}_title_en" => $input_array["{$data["field_prefix"]}_title_en"],
    "{$data["field_prefix"]}_title_jp" => $input_array["{$data["field_prefix"]}_title_jp"],
    "{$data["field_prefix"]}_description" => $input_array["{$data["field_prefix"]}_description"],
    "{$data["field_prefix"]}_description_en" => $input_array["{$data["field_prefix"]}_description_en"],
    "{$data["field_prefix"]}_description_jp" => $input_array["{$data["field_prefix"]}_description_jp"],
    "{$data["field_prefix"]}_start_timestamp" => $input_array["{$data["field_prefix"]}_start_timestamp"],
    "{$data["field_prefix"]}_end_timestamp" => $input_array["{$data["field_prefix"]}_end_timestamp"],
    "{$data["field_prefix"]}_target" => $input_array["{$data["field_prefix"]}_target"],

    "{$data["field_prefix"]}_topic" => $input_array["{$data["field_prefix"]}_topic"],
    "{$data["field_prefix"]}_go_point_type" => $input_array["{$data["field_prefix"]}_go_point_type"],
    "{$data["field_prefix"]}_go_point_id" => $input_array["{$data["field_prefix"]}_point_selector"],
    "{$data["field_prefix"]}_lat" => $input_array["{$data["field_prefix"]}_lat"],
    "{$data["field_prefix"]}_lng" => $input_array["{$data["field_prefix"]}_lng"],

    "{$data["field_prefix"]}_timelimit_push" => $input_array["{$data["field_prefix"]}_timelimit_push"],
    "{$data["field_prefix"]}_mail_enabled" => $input_array["{$data["field_prefix"]}_mail_enabled"],
    "{$data["field_prefix"]}_enabled" => $input_array["{$data["field_prefix"]}_enabled"]
);

//上傳圖片處理
$upload_path = "upload/geofence/";
$file_type = 'gif|jpg|jpeg|png';
$max_size = 1024;
$resize_width = 1200;
$resize_height = 900;
$fit_type = "FIT_INNER";
$max_width = 2400;
$max_height = 1800;

//上傳商品圖
$field_name = "{$data["field_prefix"]}_image";
$upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);

if (isset($upload_result["upload_data"])) {
    //上傳成功，記錄上傳檔名
    $insert_array[$field_name] = $upload_result["upload_data"]["file_name"];
} elseif ($upload_result["error"] != "upload_no_file_selected") {
    //錯誤原因若不是因為沒有上傳檔案，就要返回並顯示錯誤訊息
    $upload_error_msg_i18n = $this->lang->line("upload");
    _alert($upload_error_msg_i18n[$upload_result["error"]]);
    exit();
} else {
    
}

$this->Common_model->insert_db($data['table_name'], $insert_array);

$gt_id = $this->Common_model->get_insert_id();


if ($input_array["{$data["field_prefix"]}_mail_enabled"] == 'Y') {
    //寄發通知信
    $this->load->library('sendmail_lib');

    $data_array = $insert_array;
    $data_array['id'] = $gt_id;
    $data_array['title'] = $data["menu_name"];
    $data_array['act_title'] = '新增';
    $data_array['sa_account'] = $this->login_sa->sa_account;
    $data_array['g_lat'] = $this->input->post('g_lat');
    $data_array['g_lng'] = $this->input->post('g_lng');
    $data_array['g_range'] = $this->input->post('g_range');

    //準備寄發通知信，告知系統管理者
    $mail_title = "【後台】" . $data_array['act_title'] . $data_array['title'] . "通知信_" . date('Y-m-d');
    $mail_body = $this->load->view("mail_content/sa_lbs_trigger_notice", $data_array, true);

    $this->sendmail_lib->set_sender($this->config->item("og_pm_mail"), "VRAR360管理後台系統通知");
    $this->sendmail_lib->set_content($mail_title, $mail_body);

    $this->sendmail_lib->set_receiver($this->config->item("og_pm_mail"), '系統管理員');
    $this->sendmail_lib->send_mail();
}


//新增geofence關連表
$insert_array = array(
    "g_id" => $input_array["g_id"],
    "gt_id" => $gt_id
);
$this->Common_model->insert_db("geofence_relation", $insert_array);
$id = $this->Common_model->get_insert_id();



_alert_redirect($this->lang->line("新增成功！"), base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
