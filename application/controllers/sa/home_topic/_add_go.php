<?php

include_once "__config.php";

// echo json_encode($_REQUEST);
// exit();

//傳入值接收
$input_array = array(
    "t_id" => $this->input->post("t_id"),
    "t_order" => $this->input->post("t_order")
);



foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert("參數錯誤:不能為空值");
        exit();
    }
}


//判斷是否超過六個

$count_topic = $this->Common_model->count_db("home_topic");

if ($count_topic>=6) {
   _alert_redirect("超過首頁主題數，請用編輯修改首頁主題", base_url("{$this->controller_name}/{$data['table_name']}"));
   exit();
}

// echo json_encode($count_topic);
// exit();

$insert_array = array(
    "t_id" => $input_array["t_id"],
    "ht_order"  => $input_array["t_order" ]
);


$this->Common_model->insert_db("home_topic", $insert_array);

$topic_id = $this->Common_model->get_insert_id();



_alert_redirect($this->lang->line("新增成功！"), base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
