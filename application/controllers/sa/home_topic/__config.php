<?php

//menu_category用來驗證存取權限
$data['menu_category'] = "home_topic";
$data["menu_name"] = "首頁主題管理";

//資料表名稱
$data["table_name"] = "home_topic";

//資料表欄位前置詞
$data["field_prefix"] = "t";

//資料名稱
$data["item_name"] = "主題";

//分類
$data["item_group"] = "";
$data["item_group_name"] = "";

//分類前置詞
$data["item_group_prefix"] = "";

//驗證權限
$this->menu_auth($data['menu_category']);
