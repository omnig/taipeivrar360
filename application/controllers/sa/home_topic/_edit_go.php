<?php

include_once "__config.php";


// echo json_encode($_REQUEST);
// exit();

//傳入值接收
$input_array = array(
    "ht_id" => $this->input->post("ht_id"),
    "t_id" => $this->input->post("t_id"),
    "t_order" => $this->input->post("t_order")
);


foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert("參數錯誤:不能為空值");
        exit();
    }
}



//取得此資料，確定資料存在
$where_array = array(
    "ht_id" => $input_array["ht_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one("home_topic", $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), "主題"));
    exit();
}


$update_array = array(
   "t_id" => $input_array["t_id"],
   "ht_order"  => $input_array["t_order" ]
);


$this->Common_model->update_db("home_topic", $update_array, $where_array);





//處理關鍵字
$topic_id = $input_array["{$data["field_prefix"]}_id"];
$tag_ary = $this->input->post("{$data["field_prefix"]}_tag");


_alert_redirect('更新成功！', base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
