<?php

include_once "__config.php";

//每個後台頁面都要定義 member_category，用於使用者權限驗證 資料表為menu_auth

header('Content-type: application/force-download');
header('Content-Disposition: attachment; filename="' . date('YmdHis') . '_dbbackup.sql.gz"');

passthru("mysqldump --user={$this->db->username} --host={$this->db->hostname} --password={$this->db->password} {$this->db->database} | gzip");

exit();
