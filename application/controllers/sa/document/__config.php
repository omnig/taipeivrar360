<?php

//menu_category用來驗證存取權限
$data['menu_category'] = "document";
$data["menu_name"] = $this->lang->line("文章管理");

//資料表名稱
$data["table_name"] = $data['menu_category'];

//資料表欄位前置詞
$data["field_prefix"] = "d";

//資料名稱
$data["item_name"] = $this->lang->line("文章");

//分類
$data["item_group"] = "";
$data["item_group_name"] = "";

//分類前置詞
$data["item_group_prefix"] = "";

//驗證權限
$this->menu_auth($data['menu_category']);




function add_to_epaper($d_id="",$title="",$content=""){

	$ci = & get_instance();
	//先檢查文商是否有更新
	
	$where_array = array(
		"d_id" => $d_id,
		"d_subscribe" => "Y"
	);
	$document = $ci->Common_model->get_one("document",$where_array);


	if (!$document) {

		$update_array = array(
			"d_subscribe" => "Y"
		);

		$where_array = array(
			"d_id" => $d_id,
		);

		 $ci->Common_model->update_db("document",$update_array,$where_array);

		//加入epaper table
		$insert_array = array(
		    "e_sender_name" => "桃園VR360",
		    "e_sender_email" => "imca.vrar@gmail.com",
		    "e_title" => $title,
		    "e_content" => $content,
		    "e_email_confirmed" => "Y",
		    "e_reserve_send_time" => date("Y-m-d H:i:s")
		);

		$ci->Common_model->insert_db("epaper", $insert_array);
		return TRUE;

	}else{

		return TRUE;
	}

	return FALSE;

}