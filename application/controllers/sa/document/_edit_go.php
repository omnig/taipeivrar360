<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = "編輯";

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->post("{$data["field_prefix"]}_id"),
    "{$data["field_prefix"]}_title" => $this->input->post("{$data["field_prefix"]}_title"),
    "{$data["field_prefix"]}_type" => $this->input->post("{$data["field_prefix"]}_type"),
    "{$data["field_prefix"]}_content" => $this->input->post("{$data["field_prefix"]}_content"),
    "{$data["field_prefix"]}_vaild_state" => $this->input->post("{$data["field_prefix"]}_vaild_state"),
    "{$data["field_prefix"]}_order" => $this->input->post("{$data["field_prefix"]}_order"),
    
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}


$input_array["{$data["field_prefix"]}_url"] =   $this->input->post("{$data["field_prefix"]}_url");
$input_array["{$data["field_prefix"]}_valid_message"] =   $this->input->post("{$data["field_prefix"]}_valid_message");



//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one($data['table_name'], $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), $data["item_name"]));
    exit();
}

//準備更新資料庫
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$update_array = array(
    "{$data["field_prefix"]}_title" => $input_array["{$data["field_prefix"]}_title"],
    "{$data["field_prefix"]}_type" => $input_array["{$data["field_prefix"]}_type"],
    "{$data["field_prefix"]}_content" => $input_array["{$data["field_prefix"]}_content"],
    "{$data["field_prefix"]}_url" => $input_array["{$data["field_prefix"]}_url"],
    "{$data["field_prefix"]}_vaild_state" => $input_array["{$data["field_prefix"]}_vaild_state"],
    "{$data["field_prefix"]}_order" => $input_array["{$data["field_prefix"]}_order"],
    "{$data["field_prefix"]}_valid_message" => $input_array["{$data["field_prefix"]}_valid_message"],
);

//上傳圖片處理
$upload_path = "upload/image/";
$file_type = 'gif|jpg|jpeg|png';
$max_size = 1024;
$resize_width = 200;
$resize_height = 150;
$fit_type = "FIT_INNER";
$max_width = 2400;
$max_height = 1800;

//上傳商品圖
$field_name = "{$data["field_prefix"]}_image";
$upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);

if (isset($upload_result["upload_data"])) {
    //上傳成功，記錄上傳檔名
    $update_array[$field_name] = "upload/image/".$upload_result["upload_data"]["file_name"];
} elseif ($upload_result["error"] != "upload_no_file_selected") {
    //錯誤原因若不是因為沒有上傳檔案，就要返回並顯示錯誤訊息
    $upload_error_msg_i18n = $this->lang->line("upload");
    _alert($upload_error_msg_i18n[$upload_result["error"]]);
    exit();
}

$this->Common_model->update_db($data['table_name'], $update_array, $where_array);


//mail內容必須在載入library後
$input_array["content"] = $input_array["{$data["field_prefix"]}_content"]. "<a href='".$this->config->item("front_base_url")."article/main/".$input_array["{$data["field_prefix"]}_id"]."'>觀看更多..</a>";

$mail_body = $this->load->view("mail_content/document_content", $input_array, true);

//寄電子報控制
if ($input_array["{$data["field_prefix"]}_vaild_state"]=="online") {
    add_to_epaper(
        $input_array["{$data["field_prefix"]}_id"],
        "[桃園VR360-新文章]".$input_array["{$data["field_prefix"]}_title"],
        $mail_body
    );
}


_alert_redirect('更新成功！', base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
