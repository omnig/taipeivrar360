<?php

//menu_category用來驗證存取權限
$data['menu_category'] = "dashboard/order";
$data["menu_name"] = $this->lang->line("紅利");

include_once "__config.php";

$this->load->library('export_csv_lib');    //載入csv library
//傳入值接收
$input_array = array(
    "begin_date" => $this->input->get("begin_date"),
    "end_date" => $this->input->get("end_date")
);

if (!$input_array["begin_date"] || !$input_array["end_date"]) {
    $data["begin_date"] = date("Y-m-d", time() - 86400 * 30);
    $data["end_date"] = date("Y-m-d");
} else {
    $data["begin_date"] = $input_array["begin_date"];
    $data["end_date"] = $input_array["end_date"];
}

//紅利交易紀錄

$where_array = array(
    "bt_timestamp >=" => $data["begin_date"] . " 00:00:00",
    "bt_timestamp <" => $data["end_date"] . " 00:00:00"
);

$ret = $this->Common_model->get_db_join("bonus_transation AS bt", $where_array, "user AS u", "bt.u_id=u.u_id", "INNER", "bt_timestamp", "ASC");

$csv_content_array = array();
foreach ($ret as $row) {
    $csv_content_array[] = array(
        $row->bt_timestamp, $row->u_id, $row->u_name, $row->bt_itemid, $row->bt_transid, $row->bt_goodid, $row->bt_title, $row->bt_point, $row->bt_qty, $row->bt_total, $row->bt_expiration
    );
}

//匯出csv
//匯出檔名
$csv_filename = 'bonus_transation' . date('YmdHis') . '.csv';

//匯出csv內容
$csv_title = $this->lang->line("site_name") . "_紅利交易紀錄\n"
        . "時間,使用者ID,姓名,獲得點數交易ID,消耗點數交易ID,兌換商品ID,交易內容,點數,數量,小計,點數使用期限\n";

$this->export_csv_lib->set_filename($csv_filename);
$this->export_csv_lib->set_title($csv_title);
$this->export_csv_lib->set_content_array($csv_content_array);
$this->export_csv_lib->execute();

exit();
