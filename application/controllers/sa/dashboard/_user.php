<?php

//menu_category用來驗證存取權限
$data['menu_category'] = "dashboard/user";
$data["menu_name"] = $this->lang->line("會員");

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $data["menu_name"];

//麵包屑
$data["bread"] = array(
    array(
        "title" => $this->lang->line("成效報表") . " - " . $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['menu_category']}")
    )
);

//傳入值接收
$input_array = array(
    "begin_date" => $this->input->get("begin_date"),
    "end_date" => $this->input->get("end_date")
);

if (!$input_array["begin_date"] || !$input_array["end_date"]) {
    $data["begin_date"] = date("Y-m-d", time() - 86400 * 30);
    $data["end_date"] = date("Y-m-d");
} else {
    $data["begin_date"] = $input_array["begin_date"];
    $data["end_date"] = $input_array["end_date"];
}

//會員圖表
$data["chart_user"] = array(
    "title" => $this->lang->line("新會員"), //圖表標題
    "title_color" => "red", //標題欄顏色
    "mode" => "time", //x軸資料型態
    "data" => array()   //要繪圖的資料
);

//新會員
$ret = $this->dashboard_model->get_list($data["begin_date"], $data["end_date"], "NEW_USER");

$points = array();
foreach ($ret as $row) {
    $points[] = array(
        (int) strtotime($row->d_date) * 1000, (int) $row->d_number
    );
}

$data["chart_user"]["data"][] = array(
    "data" => $points,
    "label" => $this->lang->line("新會員"),
    "lines" => array(
        "lineWidth" => 1
    ),
    "shadowSize" => 0
);

//會員累計圖表
$data["chart_user_sum"] = array(
    "title" => $this->lang->line("累計會員"), //圖表標題
    "title_color" => "blue", //標題欄顏色
    "mode" => "time", //x軸資料型態
    "data" => array()   //要繪圖的資料
);

//累計會員
$ret = $this->dashboard_model->get_statistics($data["begin_date"], $data["end_date"], "NEW_USER");

$points = array();
foreach ($ret as $row) {
    $points[] = array(
        (int) strtotime($row->d_date) * 1000, (int) $row->d_number
    );
}

$data["chart_user_sum"]["data"][] = array(
    "data" => $points,
    "label" => $this->lang->line("會員"),
    "lines" => array(
        "lineWidth" => 1
    ),
    "shadowSize" => 0
);
