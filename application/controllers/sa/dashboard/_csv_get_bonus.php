<?php

//menu_category用來驗證存取權限
$data['menu_category'] = "dashboard/order";
$data["menu_name"] = $this->lang->line("紅利");

include_once "__config.php";

$this->load->library('export_csv_lib');    //載入csv library

//傳入值接收
$input_array = array(
    "begin_date" => $this->input->get("begin_date"),
    "end_date" => $this->input->get("end_date")
);

if (!$input_array["begin_date"] || !$input_array["end_date"]) {
    $data["begin_date"] = date("Y-m-d", time() - 86400 * 30);
    $data["end_date"] = date("Y-m-d");
} else {
    $data["begin_date"] = $input_array["begin_date"];
    $data["end_date"] = $input_array["end_date"];
}

//獲得紅利
$ret = $this->dashboard_model->get_list($data["begin_date"], $data["end_date"], "GET_BONUS");

$csv_content_array = array();
foreach ($ret as $row) {
    $csv_content_array[] = array(
        $row->d_date, $row->d_number
    );
}

//匯出csv
//匯出檔名
$csv_filename = 'get_bonus_' . date('YmdHis') . '.csv';

//匯出csv內容
$csv_title = $this->lang->line("site_name") . "_獲得紅利統計\n"
        . "日期,點數\n";

$this->export_csv_lib->set_filename($csv_filename);
$this->export_csv_lib->set_title($csv_title);
$this->export_csv_lib->set_content_array($csv_content_array);
$this->export_csv_lib->execute();

exit();
