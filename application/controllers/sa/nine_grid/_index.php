<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line($data["menu_name"]);
$model_name = ucfirst($data['table_name']) . '_model';
$this->load->model($model_name);

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['main_menu_category']}")
    )
);


//傳入值接收
$input_array = array(
    "m_id" => $this->input->get("m_id")
);


foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

$data['mid'] = $input_array['m_id'];

$where_array = array(
    "ng.m_id" => $input_array['m_id']
);
$order_by ='ng_id';
//載入基本資訊
$data['nine_grid_list'] = $this->$model_name->get_view_list($order_by, '', 0, 0, $where_array);

// echo json_encode($data['nine_grid_list']);
// exit();
