<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = $this->lang->line("刪除");

//傳入值接收
$input_array = array(
    "m_id" => $this->input->GET("m_id"),
    "{$data["field_prefix"]}_id" => $this->input->GET("ng_id")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

//取得此資料，確定資料存在
$where_array = array(
	"m_id" => $input_array['m_id'],
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data["table_name"]] = $this->Common_model->get_one($data["table_name"], $where_array);

if (!$data[$data["table_name"]]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), $data["item_name"]));
    exit();
}

//刪除
$this->Common_model->delete_db($data["table_name"], $where_array);

_alert_redirect($this->lang->line('刪除成功！'), base_url("{$this->controller_name}/{$data["table_name"]}?m_id={$input_array["m_id"]}"));
exit();
