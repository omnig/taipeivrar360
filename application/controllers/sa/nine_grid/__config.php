<?php

//menu_category用來驗證存取權限
$data['menu_category'] = "nine_grid";
$data["menu_name"] = '九宮格管理';
$data['main_menu_category'] = "mission";

//資料表名稱
$data["table_name"] = $data['menu_category'];

//資料表欄位前置詞
$data["field_prefix"] = "ng";


//觸發事件陣列
// $data['trigger'] = array("Near_trigger"=>"靠近POI","AR"=>"AR");
$data['trigger'] = array("poi"=>"靠近POI", "AR"=>"AR導引");
/*
//資料名稱
$data["item_name"] = $this->lang->line("分類");

//分類
$data["item_group"] = "";
$data["item_group_name"] = "";

//分類前置詞
$data["item_group_prefix"] = "";
*/

//取得後臺介面題庫選擇需要的設定
$data["pass_method"] = [
    '到點闖關' => 'touchdown',
    'AR闖關' => 'AR',
    '問答闖關' => 'QA',
];

//取得後臺介面題庫選擇需要的設定
$data["q_style"] = [
    '是非' => 'qb',
    '單選' => 'qs',
];
