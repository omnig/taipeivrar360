<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("編輯");

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->post("{$data["field_prefix"]}_id"),
    "{$data["field_prefix"]}_name" => $this->input->post("{$data["field_prefix"]}_name"),
    "{$data["field_prefix"]}_email" => $this->input->post("{$data["field_prefix"]}_email"),
    "{$data["field_prefix"]}_group" => $this->input->post("{$data["field_prefix"]}_group"),
    "{$data["field_prefix"]}_account" => $this->input->post("{$data["field_prefix"]}_account"),
    "{$data["field_prefix"]}_enabled" => $this->input->post("{$data["field_prefix"]}_enabled")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($index.$this->lang->line("參數錯誤"));
        exit();
    }
}

$input_array["{$data["field_prefix"]}_password"] = $this->input->post("{$data["field_prefix"]}_password");

//確認有沒有重複的資料
// $where_array = array(
//     "{$data["field_prefix"]}_id !=" => $input_array["{$data["field_prefix"]}_id"]
// );

// $duplicated = $this->Common_model->get_one($data['table_name'], $where_array);
// if ($duplicated) {
//     _alert("{$this->lang->line("重複的")}{$data["item_name"]}");
//     exit();
// }


//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one($data['table_name'], $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), $data["item_name"]));
    exit();
}


$update_array = array(
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "{$data["field_prefix"]}_account" => $input_array["{$data["field_prefix"]}_account"],
    "{$data["field_prefix"]}_group" => $input_array["{$data["field_prefix"]}_group"],
    "{$data["field_prefix"]}_email" => $input_array["{$data["field_prefix"]}_email"],
    "{$data["field_prefix"]}_enabled" => $input_array["{$data["field_prefix"]}_enabled"]
);

if ($input_array["{$data["field_prefix"]}_password"]) {
    $update_array["{$data["field_prefix"]}_password"] = $this->encrypt_password($input_array["{$data["field_prefix"]}_password"]);
}

$this->Common_model->update_db($data['table_name'], $update_array, $where_array);

_alert_redirect($this->lang->line('更新成功！'), base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
