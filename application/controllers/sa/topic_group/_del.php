<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = $this->lang->line("刪除");

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->get("{$data["field_prefix"]}_id")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);


$data["table_name"] = 'topic_group';
$data[$data["table_name"]] = $this->Common_model->get_one($data["table_name"], $where_array);

if (!$data[$data["table_name"]]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), $data["item_name"]));
    exit();
}

//軟刪除
// $this->Common_model->delete_db($data["table_name"], $where_array);
$update_array = array(
    "{$data["field_prefix"]}_del" => 'Y'
);
$this->Common_model->update_db($data["table_name"], $update_array, $where_array);


// //刪除Andyplace DB
// $query_info = array(
//     'buid' => $ab_buid
// );

// //載入helper
// $this->load->helper('anyplace_helper');
// $result = get_mapping_result('delete', 'building', $query_info);

// if ($result['status_code'] != 200) {
//     _alert('刪除失敗 [' . $result['status_code'] . ':' . $result['message'] . ']。\n\n請再試一次，或洽系統管理員。');
//     exit();
// }



//刪除圖片
// $filename = "upload/home_slide/" . $data[$data["table_name"]]->{"{$data["field_prefix"]}_image_filename"};
// $this->upload_lib->delete($filename);
$data["table_name"] = 'topic_group';
_alert_redirect($this->lang->line('刪除成功！'), base_url("{$this->controller_name}/{$data["table_name"]}"));
exit();
