<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = "活動紀錄";

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/visited")
    )
);

//取得使用者參觀歷程
$this->load->model('user_model');
$where_array = array(
    'u.u_id' => $this->input->get('u_id')
);
$data['visited'] = $this->user_model->get_visited('r_timestamp desc', '', 0, 0, $where_array);

$data['user_name'] = $this->Common_model->get_one_field('user as u', 'u_name', $where_array);


//取得使用者書籤收藏
$data['favorite'] = $this->user_model->get_favorite('f_timestamp desc', '', 0, 0, $where_array);
