<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = $this->lang->line("編輯");

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->post("{$data["field_prefix"]}_id"),
    "{$data["field_prefix"]}_uuid" => $this->input->post("{$data["field_prefix"]}_uuid"),
    "{$data["field_prefix"]}_major" => $this->input->post("{$data["field_prefix"]}_major"),
    "{$data["field_prefix"]}_minor" => $this->input->post("{$data["field_prefix"]}_minor"),
    "{$data["field_prefix"]}_mac" => $this->input->post("{$data["field_prefix"]}_mac"),
    "{$data["field_prefix"]}_range" => $this->input->post("{$data["field_prefix"]}_range"),
    "{$data["field_prefix"]}_enabled" => $this->input->post("{$data["field_prefix"]}_enabled")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

$input_array["remove_image"] = $this->input->post("remove_image");

//檢查beacon mac是否重覆
$replace = array(":", "：");
$mac = str_replace($replace, "", $input_array["{$data["field_prefix"]}_mac"]);
$mac_str = mb_substr($mac, strlen($mac) - 6, 6, 'utf8');
$where_array = array(
    "{$data["field_prefix"]}_id !=" => $input_array["{$data["field_prefix"]}_id"],
    "{$data["field_prefix"]}_mac like '%$mac_str%'"
);
$row = $this->Common_model->get_one($data['table_name'], $where_array);
if (!is_null($row)) {
    $mac_msg = '\\n\\n＊＊＊請注意：MAC已重覆＊＊＊';
    _alert('MAC重覆！');
    exit();
}


$input_array["{$data["field_prefix"]}_hwid"] = $this->input->post("{$data["field_prefix"]}_hwid");
$input_array["{$data["field_prefix"]}_description"] = $this->input->post("{$data["field_prefix"]}_description");
$input_array['af_id'] = 1;
$input_array["{$data["field_prefix"]}_lat"] = $this->input->post("{$data["field_prefix"]}_lat");
$input_array["{$data["field_prefix"]}_lng"] = $this->input->post("{$data["field_prefix"]}_lng");
$input_array["{$data["field_prefix"]}_set_timestamp"] = $this->input->post("{$data["field_prefix"]}_set_timestamp");
$input_array["{$data["field_prefix"]}_end_timestamp"] = $this->input->post("{$data["field_prefix"]}_end_timestamp");

//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one($data['table_name'], $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), $data["item_name"]));
    exit();
}

$update_array = array(
    "{$data["field_prefix"]}_description" => $input_array["{$data["field_prefix"]}_description"],
    "{$data["field_prefix"]}_uuid" => $input_array["{$data["field_prefix"]}_uuid"],
    "{$data["field_prefix"]}_major" => $input_array["{$data["field_prefix"]}_major"],
    "{$data["field_prefix"]}_minor" => $input_array["{$data["field_prefix"]}_minor"],
    "{$data["field_prefix"]}_mac" => $mac,
    "{$data["field_prefix"]}_range" => $input_array["{$data["field_prefix"]}_range"],
    "{$data["field_prefix"]}_enabled" => $input_array["{$data["field_prefix"]}_enabled"]
);

if ($input_array["{$data["field_prefix"]}_hwid"]) {
    $update_array["{$data["field_prefix"]}_hwid"] = $input_array["{$data["field_prefix"]}_hwid"];
}
if ($input_array["{$data["field_prefix"]}_set_timestamp"]) {
    $update_array["{$data["field_prefix"]}_set_timestamp"] = $input_array["{$data["field_prefix"]}_set_timestamp"];
}
if ($input_array["{$data["field_prefix"]}_end_timestamp"]) {
    $update_array["{$data["field_prefix"]}_end_timestamp"] = $input_array["{$data["field_prefix"]}_end_timestamp"];
}
if ($input_array['af_id'] > 0) {
    $update_array["af_id"] = $input_array["af_id"];
}
if ($input_array["{$data["field_prefix"]}_lat"] != null && $input_array["{$data["field_prefix"]}_lng"] != null) {
    $update_array["{$data["field_prefix"]}_lat"] = $input_array["{$data["field_prefix"]}_lat"];
    $update_array["{$data["field_prefix"]}_lng"] = $input_array["{$data["field_prefix"]}_lng"];
}

//上傳圖片處理
$upload_path = "upload/beacon/";
$file_type = 'gif|jpg|jpeg|png';
$max_size = 1024;
$resize_width = 1200;
$resize_height = 900;
$fit_type = "FIT_INNER";
$max_width = 2400;
$max_height = 1800;

$field_name = "{$data["field_prefix"]}_image";
if (isset($input_array["remove_image"]) && isset($input_array["remove_image"][$field_name])) {
    $update_array[$field_name] = NULL;
} else {
    $upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);

    if (isset($upload_result["upload_data"])) {
        //上傳成功，記錄上傳檔名
        $update_array[$field_name] = $upload_result["upload_data"]["file_name"];
    } elseif ($upload_result["error"] != "upload_no_file_selected") {
        //錯誤原因若不是因為沒有上傳檔案，就要返回並顯示錯誤訊息
        $upload_error_msg_i18n = $this->lang->line("upload");
        _alert($upload_error_msg_i18n[$upload_result["error"]]);
        exit();
    }
}

$this->Common_model->update_db($data['table_name'], $update_array, $where_array);

//紀錄log
$this->sa_log($page, $data["menu_name"], 'UPDATE', $input_array["{$data["field_prefix"]}_id"], 'MAJOR:' . $input_array["{$data["field_prefix"]}_major"] . ' MINOR:' . $input_array["{$data["field_prefix"]}_minor"]);


_alert_redirect('更新成功！', base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
