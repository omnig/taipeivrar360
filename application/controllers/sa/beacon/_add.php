<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("新增");

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/add")
    )
);

// 取得樓層資料
$where_array = array(
    "ab.ab_enabled" => "Y",
    "af.af_enabled" => "Y"
);
$data['ap_floors'] = $this->Common_model->get_db_join("ap_building as ab", $where_array, "ap_floors as af", "ab.ab_id= af.ab_id", "left", 'af.ab_id,af_order');
