<?php

include_once "__config.php";

$model_name = ucfirst($data['table_name']) . '_model';
$this->load->model($model_name);

//傳入值接收
$input_array = array(
    "limit" => $this->input->post('length'),
    "skip" => $this->input->post('start'),
    "draw" => intval($this->input->get_post('draw')),
    "columns" => $this->input->post('columns'),
    "order" => $this->input->post('order'),
    "filters" => $this->input->post('filters'),
    "customActionType" => $this->input->post('customActionType'),
    "customActionName" => $this->input->post('customActionName'),
    "id" => $this->input->post('id')
);

//多筆資料管理
if ($input_array["customActionType"] == "group_action" && $input_array['id']) {
    $where_array = array(
        "{$data["field_prefix"]}_id" => $input_array['id']
    );

    switch ($input_array['customActionName']) {
        case 'enable' : //啟用
            $act = 'UPDATE';
            $act_name = '啟用';
            $update_array = array(
                "{$data["field_prefix"]}_enabled" => 'Y'
            );
            $this->Common_model->update_db($data["table_name"], $update_array, $where_array);
            break;
        case 'disable' :    //停用
            $act = 'UPDATE';
            $act_name = '關閉';
            $update_array = array(
                "{$data["field_prefix"]}_enabled" => 'N'
            );
            $this->Common_model->update_db($data["table_name"], $update_array, $where_array);
            break;
        case 'del' :    //刪除
            $act = 'DELETE';
            $act_name = '刪除';
            //刪除圖片
            $data[$data["table_name"]] = $this->Common_model->get_db($data["table_name"], $where_array);

            if (!$data[$data["table_name"]]) {
                break;
            }

            $this->Common_model->delete_db($data["table_name"], $where_array);

            foreach ($data[$data["table_name"]] as $row) {
                if ($row->{"{$data["field_prefix"]}_image"}) {
                    $filename = "upload/{$data["table_name"]}/" . $row->{"{$data["field_prefix"]}_image"};
                    $this->upload_lib->delete($filename);
                }
            }

            break;
    }

    //紀錄log
    $log_data = $this->Common_model->get_db($data["table_name"], $where_array);
    $content_array = array();
    foreach ($log_data as $row) {
        $content_array[] = 'MAJOR:' . $row->b_major . ' MINOR:' . $row->b_minor;
    }
    $this->sa_log($page, $data["menu_name"], $act, implode(',', $input_array['id']), "批次{$act_name}: " . implode('、', $content_array));

    $records["customActionStatus"] = "OK";
    $records["customActionMessage"] = "變更完成!";
}

//資料分頁設定
$input_array['limit'] = intval($input_array['limit']) < 0 ? 0 : intval($input_array['limit']);
$input_array['skip'] = intval($this->input->get_post('start'));

$where_array = array(
);

//依傳入參數過濾
if ($input_array['filters']) {
    $concat_where_string = "";

    foreach ($input_array['filters'] as $field_name => $value) {
        if ($value == '') {
            continue;
        }

        $where_string = "";

        switch ($field_name) {
            //比對完全相同的值
            case "{$data["field_prefix"]}_minor" :
                $where_string = "({$field_name} = '{$value}')";
                break;
            //全文搜尋
            case "{$data["field_prefix"]}_mac" :
                $replace = array(":", "：");
                $mac = str_replace($replace, "", $value);
                $where_string = "({$field_name} LIKE '%{$mac}%')";
                break;

            case "{$data["field_prefix"]}_major" :
            case "{$data["field_prefix"]}_description" :
            case "af_desc" :
            case "{$data["field_prefix"]}_uuid" :
            case "{$data["field_prefix"]}_hwid" :
            case "{$data["field_prefix"]}_voltage" :
            case "{$data["field_prefix"]}_range" :
                $where_string = "({$field_name} LIKE '%{$value}%')";
                break;

            //其它特殊搜尋遇濾方式
            case 'voltage_from' :
                $where_string = "({$data["field_prefix"]}_set_timestamp >= '{$value} 00:00:00')";
                break;
            case 'voltage_to' :
                $where_string = "({$data["field_prefix"]}_set_timestamp <= '{$value} 23:59:59')";
                break;
            case 'set_from' :
                $where_string = "({$data["field_prefix"]}_set_timestamp >= '{$value} 00:00:00')";
                break;
            case 'set_to' :
                $where_string = "({$data["field_prefix"]}_set_timestamp <= '{$value} 23:59:59')";
                break;
            case 'end_from' :
                $where_string = "({$data["field_prefix"]}_end_timestamp >= '{$value} 00:00:00')";
                break;
            case 'end_to' :
                $where_string = "({$data["field_prefix"]}_end_timestamp <= '{$value} 23:59:59')";
                break;
            case 'call_from' :
                $where_string = "({$data["field_prefix"]}_call_timestamp >= '{$value} 00:00:00')";
                break;
            case 'call_to' :
                $where_string = "({$data["field_prefix"]}_call_timestamp <= '{$value} 23:59:59')";
                break;
        }

        if ($concat_where_string == '') {
            $concat_where_string = $where_string;
        } else {
            $concat_where_string = "$concat_where_string AND $where_string";
        }
    }

    if ($concat_where_string != '') {
        $where_array[] = $concat_where_string;
    }
}

//排序欄位，對映order的排序欄位
$sort_fields = array(
    false, false, "{$data["field_prefix"]}_description", "af_desc", "{$data["field_prefix"]}_major", "{$data["field_prefix"]}_minor", "{$data["field_prefix"]}_hwid", "{$data["field_prefix"]}_mac", "{$data["field_prefix"]}_voltage", "{$data["field_prefix"]}_voltage_timestamp", "{$data["field_prefix"]}_voltage_count", "{$data["field_prefix"]}_range", "", "{$data["field_prefix"]}_set_timestamp", false
);

$order_by = '';
foreach ($input_array['order'] as $row) {
    if ($sort_fields[$row['column']]) {
        if ($order_by == '') {
            $order_by = "{$sort_fields[$row['column']]} {$row['dir']}";
            continue;
        }

        $order_by = "{$order},{$sort_fields[$row['column']]} {$sort_fields[$row['dir']]}";
    }
}

//計算資料總數
$iTotalRecords = $this->$model_name->get_list($order_by, '', 0, 0, $where_array, true);

//從資料庫取得本次資料
$ret = $this->$model_name->get_list($order_by, '', $input_array['limit'], $input_array['skip'], $where_array, false);

//回傳值
$records = array();
$records["data"] = array();

$status_list = array(
    'Y' => array("success" => $this->lang->line("啟用中")),
    'N' => array("danger" => $this->lang->line("已關閉"))
);

$edit_url = base_url("{$this->controller_name}/{$data['table_name']}/edit");
$del_url = base_url("{$this->controller_name}/{$data['table_name']}/del");

//html表格內容
foreach ($ret as $i => $row) {
    $id = $row->{"{$data["field_prefix"]}_id"};
    //呈現電量(%)
    $voltage = "";
    $vol_timestamp = "";
    if ($row->{"{$data["field_prefix"]}_voltage"}) {
        $voltage = floatval($row->{"{$data["field_prefix"]}_voltage"}) . "v";
        if (floatval($row->{"{$data["field_prefix"]}_voltage"}) >= 2.3) {
            $voltage = "<div class='circle' style='background-color:#00cc00;width:25px;height:25px;'>" . $voltage . "</div>";
        } else if (floatval($row->{"{$data["field_prefix"]}_voltage"}) <= 2) {
            $voltage = "<div class='circle' style='background-color:red;width:25px;height:25px;'>" . $voltage . "</div>";
        } else {
            $voltage = "<div class='circle' style='background-color:yellow;width:25px;height:25px;'>" . $voltage . "</div>";
        }
    }
    if ($row->{"{$data["field_prefix"]}_voltage_timestamp"}) {
        $vol_timestamp = date("Y/m/d H:i:s", strtotime($row->{"{$data["field_prefix"]}_voltage_timestamp"}));
    }

    //呈現mac
    $mac = "";
    if ($row->{"{$data["field_prefix"]}_mac"}) {
        $mac_array = str_split($row->{"{$data["field_prefix"]}_mac"}, 2);
        $mac = implode(":", $mac_array);
    }
    $records["data"] [] = array(
        '<input type="checkbox" name="list_checkbox[]" value="' . $id . '">',
        $i + 1,
        $row->{"{$data["field_prefix"]}_description"},
        ($row->ab_name) ? $row->ab_name . "[" . $row->af_name . "]" : "",
        $row->{"{$data["field_prefix"]}_major"},
        $row->{"{$data["field_prefix"]}_minor"},
        $row->{"{$data["field_prefix"]}_hwid"},
        $mac,
        $voltage,
        $vol_timestamp,
        $row->b_voltage_count,
        $row->{"{$data["field_prefix"]}_range"},
        ($row->{"{$data["field_prefix"]}_image"}) ? "<img width='100%' src='" . base_url("upload/beacon") . "/" . $row->{"{$data["field_prefix"]}_image"} . "' >" : "",
        ($row->{"{$data["field_prefix"]}_set_timestamp"}) ? date("Y/m/d", strtotime($row->{"{$data["field_prefix"]}_set_timestamp"})) : '',
        "<a href='{$edit_url}?{$data["field_prefix"]}_id={$row->{"{$data["field_prefix"]}_id"}}' class='btn btn-xs default blue-stripe btn-operating'><i class='fa fa-pencil'></i> {$this->lang->line("編輯")}</a>" .
        "<a class='btn btn-xs default red-stripe btn-operating delete' data='{$row->{"{$data["field_prefix"]}_id"}}'><i class='fa fa-remove'></i> {$this->lang->line("刪除")}</a>",
    );
}

$records["draw"] = $input_array["draw"];
$records["recordsTotal"] = $iTotalRecords;
$records["recordsFiltered"] = $iTotalRecords;

echo json_encode($records);
exit();
