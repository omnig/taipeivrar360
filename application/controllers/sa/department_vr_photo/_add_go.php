<?php

include_once "__config.php";
use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;
// echo json_encode($_REQUEST);
// echo $_REQUEST;
// echo var_dump($_FILES);
// exit();

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_name" => $this->input->post("{$data["field_prefix"]}_name"),
    "{$data["field_prefix"]}_lat" => $this->input->post("{$data["field_prefix"]}_lat"),
    "{$data["field_prefix"]}_lng" => $this->input->post("{$data["field_prefix"]}_lng"),
    "t_id" => $this->input->post("t_id"),
    "d_id" => $this->input->post("d_id")
);

//Input 中文敘述
$zh_array = array(
    "{$data["field_prefix"]}_name" => "VR360環景圖名稱",
    "{$data["field_prefix"]}_lat" => "經度",
    "{$data["field_prefix"]}_lng" => "緯度",
    "t_id" => "主題選擇",
    "d_id" => "上傳局處"
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
       echo ("參數錯誤:(".$zh_array[$index].")不能為空值");
        exit();
    }
}

//圖片上傳檔案檢查
if (!isset($_FILES["{$data["field_prefix"]}_rep_img_path"])||$_FILES["{$data["field_prefix"]}_rep_img_path"]["name"]=="") {
    //必須上傳圖片
   echo ("顯示圖片上傳失敗或沒有上傳圖片");
    exit();
}




if ($this->input->post("upload_url")==""&&$_FILES["{$data["field_prefix"]}_img_path"]["name"]=="") {
    //必須上傳圖片
    echo ("沒有上傳VR圖片或VR URL");
    exit();
}




$input_array["{$data["field_prefix"]}_description"] = $this->input->post("{$data["field_prefix"]}_description");
$input_array["upload_url"] = $this->input->post("upload_url");
$input_array["photo_type"] = $this->input->post("photo_type");


$insert_array = array(
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "{$data["field_prefix"]}_description"  => $input_array["{$data["field_prefix"]}_description"],
    "{$data["field_prefix"]}_lat" => $input_array["{$data["field_prefix"]}_lat"],
    "{$data["field_prefix"]}_lng" => $input_array["{$data["field_prefix"]}_lng"],
    "t_id" => $input_array["t_id"],
    "d_id" => $input_array["d_id"],
);

//上傳圖片處理
$upload_path = "upload/image/";
$file_type = 'gif|jpg|jpeg|png';
$max_size = 1024;
$resize_width = 264;
$resize_height = 156;
$fit_type = "FIT_OUTER";
$max_width = 1920;
$max_height = 1080;

//上傳代表圖片
$field_name = "{$data["field_prefix"]}_rep_img_path";
$upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);


if (!isset($upload_result["upload_data"])) {
    //必須上傳圖片
    echo ($this->lang->line("圖片上傳失敗或沒有上傳圖片"));
    exit();
}
//上傳成功，記錄上傳檔名
$insert_array["{$data["field_prefix"]}_rep_img_path"] = "upload/image/".$upload_result["upload_data"]["file_name"];




if (isset($input_array["upload_url"])&&$input_array["upload_url"]!=NULL) {
    /*****************************************************************************
    *URL上傳處理----------START
    ******************************************************************************/

    //URL 判斷
    $insert_array['dvp_view_link'] = $this->input->post('upload_url');
    $insert_array['dvp_view_link'] = $this->security->xss_clean($insert_array['dvp_view_link']);
    $insert_array['Is_url'] = "Y";
    
    if (strpos($this->input->post('upload_url'), 'http') !== false) {

    }else{
        $insert_array['dvp_view_link'] = "http://".$insert_array['dvp_view_link'];
    }


}else{
    if ($input_array["photo_type"]=="pano") {
        /*****************************************************************************
        *360環景圖上傳處理----------START
        ******************************************************************************/

        //載入helper
        // $this->load->helper('google_publish_helper');
        // $this->load->helper('exif');
        // //得到目前管理員access_token Session
        // $access_token = $this->session->access_token['access_token'];

        // //向GoogleSTView 拿上傳網址(google_publish_helper)
        // $upload_url = get_upload_url($access_token);


        // $field_name = "{$data["field_prefix"]}_img_path";

        // $upload_path = "upload/vrphoto/";
        // //相容windows路徑
        // $config['upload_path'] = FCPATH . $upload_path;
        // if (DIRECTORY_SEPARATOR == '\\') {
        //     $config['upload_path'] = str_replace('/', '\\', $config['upload_path']);
        // }

        // if (!file_exists($config['upload_path'])) {
        //     exec("mkdir " . $config['upload_path']);
        // }

        // $output_dir = $config['upload_path'];
        // $fileName = substr(md5(uniqid(rand())), 3, 12).".jpg";

        // $tmp_name = dirname($_FILES["{$data["field_prefix"]}_img_path"]["tmp_name"])."/".basename($_FILES["{$data["field_prefix"]}_img_path"]["tmp_name"]);

        // // move_uploaded_file($tmp_name,$output_dir.$fileName);
        // //上傳實體到資料夾
        // switch (ENVIRONMENT) {
        //     case 'development' :
        //     case 'test' :
        //     // 假如是在Localhost
        //     // 假如是在測試機
        //         move_uploaded_file($tmp_name,$output_dir.$fileName);
        //         break;

        //     case 'api' :
        //     // 假如是在API Server 
        //         move_uploaded_file($tmp_name,$output_dir.$fileName);
        //         $sftp = new SFTP('192.168.255.77');      //Prvite IP  For nmnsvs server (伺服器上測試用)
        //         if (!$sftp->login('admin', 'admin123$')) {
        //             throw new Exception('Login failed');
        //         }else{
        //             // echo "Login Success"."<br>";
        //         }

        //         //切換要上傳目錄夾
        //         $sftp->chdir("/var/www/vrar360/upload/vrphoto/");


        //         //PUT 檔案 P1: 上傳名稱  , P2: Local端檔案位置
        //         $sftp->put($fileName, file_get_contents("/var/www/vrar360_api/upload/vrphoto/".$fileName));

        //         break;

        //     case 'production' :
        //     // 判斷假如是在WEB Server
        //     // 假如是在API Server 
        //         move_uploaded_file($tmp_name,$output_dir.$fileName);
        //         $sftp = new SFTP('192.168.255.80');      //Prvite IP  For nmnsvs server (伺服器上測試用)
        //         if (!$sftp->login('admin', 'admin123$')) {
        //             throw new Exception('Login failed');
        //         }else{
        //             // echo "Login Success"."<br>";
        //         }

        //         //切換要上傳目錄夾
        //         $sftp->chdir("/var/www/vrar360_api/upload/vrphoto/");

        //         //PUT 檔案 P1: 上傳名稱  , P2: Local端檔案位置
        //         $sftp->put($fileName, file_get_contents("/var/www/vrar360/upload/vrphoto/".$fileName));

        //         break;

        // }

        // //VR 環景照 圖片路徑
        // $filename = $upload_path . $fileName;

        // //讀取EXIF
        // $exif = exif_read_data($filename, 0, true);

        // if(isset($exif['EXIF']['ExifImageLength'])){
        //     $insert_array["{$data["field_prefix"]}_height"] = $exif['EXIF']['ExifImageLength'];
        //     $insert_array["{$data["field_prefix"]}_width"] = $exif['EXIF']['ExifImageWidth'];
        // }else{
        //     $insert_array["{$data["field_prefix"]}_height"] = 0;
        //     $insert_array["{$data["field_prefix"]}_width"] = 0;
        // }
        

        // //判斷是否為正常Google環景照片
        
        // if ($insert_array["{$data["field_prefix"]}_height"]*2!=$insert_array["{$data["field_prefix"]}_width"]) {
        //     echo ($this->lang->line("此環景照片不符合GoogleStreetView上傳規範:長寬比需2:1"));
        //     exit();
        // }

        // //新增資料庫參數
        // $insert_array["{$data["field_prefix"]}_type"] = $_FILES["{$data["field_prefix"]}_img_path"]["type"];
        // $insert_array["{$data["field_prefix"]}_size"] = $_FILES["{$data["field_prefix"]}_img_path"]["size"];

    
        // //複寫經緯度至photo
        // writ_photo_exif($filename,$input_array["{$data["field_prefix"]}_lat"],$input_array["{$data["field_prefix"]}_lng"]);

        // //上傳至Google StreetView
        // $cmd = exec('curl --request POST \--url "'. addslashes($upload_url) .'" \--upload-file "'.$filename.'" \--header "Authorization: Bearer '. addslashes($access_token) .'" ');

        // //上傳Meta Data(google_publish_helper)
        // $photoID  = upload_meta_data($upload_url,$access_token,$input_array["{$data["field_prefix"]}_lat"],$input_array["{$data["field_prefix"]}_lng"]);
        //環景圖上傳處理
        $field_name = "{$data["field_prefix"]}_img_path";
        $upload_path = "upload/vrphoto/";
        //相容windows路徑
        $config['upload_path'] = FCPATH . $upload_path;
        if (DIRECTORY_SEPARATOR == '\\') {
            $config['upload_path'] = str_replace('/', '\\', $config['upload_path']);
        }
        if (!file_exists($config['upload_path'])) {
            exec("mkdir " . $config['upload_path']);
        }
        $output_dir = $config['upload_path'];
        $fileName = substr(md5(uniqid(rand())), 3, 12).".jpg";

        $tmp_name = dirname($_FILES["{$data["field_prefix"]}_img_path"]["tmp_name"])."/".basename($_FILES["{$data["field_prefix"]}_img_path"]["tmp_name"]);

        // move_uploaded_file($tmp_name ,$output_dir.$fileName);
        // //上傳實體到資料夾
        switch (ENVIRONMENT) {
            case 'development' :
            case 'test' :
            // 假如是在Localhost
            // 假如是在測試機
                move_uploaded_file($tmp_name,$output_dir.$fileName);
                break;

            case 'api' :
            // 假如是在API Server 
                move_uploaded_file($tmp_name,$output_dir.$fileName);
                $sftp = new SFTP('192.168.255.77');      //Prvite IP  For nmnsvs server (伺服器上測試用)
                if (!$sftp->login('admin', 'admin123$')) {
                    throw new Exception('Login failed');
                }else{
                    // echo "Login Success"."<br>";
                }

                //切換要上傳目錄夾
                $sftp->chdir("/var/www/vrar360/upload/vrphoto/");


                //PUT 檔案 P1: 上傳名稱  , P2: Local端檔案位置
                $sftp->put($fileName, file_get_contents("/var/www/vrar360_api/upload/vrphoto/".$fileName));

                break;

            case 'production' :
            // 判斷假如是在WEB Server
            // 假如是在API Server 
                move_uploaded_file($tmp_name,$output_dir.$fileName);
                $sftp = new SFTP('192.168.255.80');      //Prvite IP  For nmnsvs server (伺服器上測試用)
                if (!$sftp->login('admin', 'admin123$')) {
                    throw new Exception('Login failed');
                }else{
                    // echo "Login Success"."<br>";
                }

                //切換要上傳目錄夾
                $sftp->chdir("/var/www/vrar360_api/upload/vrphoto/");

                //PUT 檔案 P1: 上傳名稱  , P2: Local端檔案位置
                $sftp->put($fileName, file_get_contents("/var/www/vrar360/upload/vrphoto/".$fileName));

                break;

        }

        //VR 環景照 圖片路徑
        $filename = $upload_path . $fileName;
        //讀取EXIF

        $exif = exif_read_data($filename, 0, true);
        if(isset($exif['EXIF']['ExifImageLength'])){
            $insert_array["dvp_height"] = $exif['EXIF']['ExifImageLength'];
            $insert_array["dvp_width"] = $exif['EXIF']['ExifImageWidth'];
        }else{
            $insert_array["dvp_height"] = 0;
            $insert_array["dvp_width"] = 0;
        }
        //新增資料庫函數

        $insert_array["dvp_type"] = $_FILES["{$data["field_prefix"]}_img_path"]["type"];
        $insert_array["dvp_size"] = $_FILES["{$data["field_prefix"]}_img_path"]["size"];
        //改寫圖片EXIF 檔 複寫經緯度至photo

        
        $insert_array["photo_type"] = "pano";
        $insert_array["{$data["field_prefix"]}_photoID"] = $photoID;
        $insert_array["{$data["field_prefix"]}_view_link"] = $this->config->item('api_base_url').'kr_spherical?image_path='.$this->config->item('api_base_url').$filename;
        $insert_array["{$data["field_prefix"]}_backup"] = $filename;
        $insert_array["{$data["field_prefix"]}_img_path"] = $filename;
        //拿到Google StreetView Share ID //(待解決)
        // get_photo_id($access_token,$photoID);


        /*****************************************************************************
        *360環景圖上傳處理----------END
        ******************************************************************************/
    }else{
        /*****************************************************************************
        *180全景圖上傳處理----------START
        ******************************************************************************/

        //環景圖上傳處理
        $field_name = "{$data["field_prefix"]}_img_path";
        $upload_path = "upload/vrphoto/";
        //相容windows路徑
        $config['upload_path'] = FCPATH . $upload_path;
        if (DIRECTORY_SEPARATOR == '\\') {
            $config['upload_path'] = str_replace('/', '\\', $config['upload_path']);
        }
        if (!file_exists($config['upload_path'])) {
            exec("mkdir " . $config['upload_path']);
        }
        $output_dir = $config['upload_path'];
        $fileName = substr(md5(uniqid(rand())), 3, 12).".jpg";

        $tmp_name = dirname($_FILES["{$data["field_prefix"]}_img_path"]["tmp_name"])."/".basename($_FILES["{$data["field_prefix"]}_img_path"]["tmp_name"]);

        // move_uploaded_file($tmp_name ,$output_dir.$fileName);
        // //上傳實體到資料夾
        switch (ENVIRONMENT) {
            case 'development' :
            case 'test' :
            // 假如是在Localhost
            // 假如是在測試機
                move_uploaded_file($tmp_name,$output_dir.$fileName);
                break;

            case 'api' :
            // 假如是在API Server 
                move_uploaded_file($tmp_name,$output_dir.$fileName);
                $sftp = new SFTP('192.168.255.77');      //Prvite IP  For nmnsvs server (伺服器上測試用)
                if (!$sftp->login('admin', 'admin123$')) {
                    throw new Exception('Login failed');
                }else{
                    // echo "Login Success"."<br>";
                }

                //切換要上傳目錄夾
                $sftp->chdir("/var/www/vrar360/upload/vrphoto/");


                //PUT 檔案 P1: 上傳名稱  , P2: Local端檔案位置
                $sftp->put($fileName, file_get_contents("/var/www/vrar360_api/upload/vrphoto/".$fileName));

                break;

            case 'production' :
            // 判斷假如是在WEB Server
            // 假如是在API Server 
                move_uploaded_file($tmp_name,$output_dir.$fileName);
                $sftp = new SFTP('192.168.255.80');      //Prvite IP  For nmnsvs server (伺服器上測試用)
                if (!$sftp->login('admin', 'admin123$')) {
                    throw new Exception('Login failed');
                }else{
                    // echo "Login Success"."<br>";
                }

                //切換要上傳目錄夾
                $sftp->chdir("/var/www/vrar360_api/upload/vrphoto/");

                //PUT 檔案 P1: 上傳名稱  , P2: Local端檔案位置
                $sftp->put($fileName, file_get_contents("/var/www/vrar360/upload/vrphoto/".$fileName));

                break;

        }

        //VR 環景照 圖片路徑
        $filename = $upload_path . $fileName;
        //讀取EXIF

        $exif = exif_read_data($filename, 0, true);
        if(isset($exif['EXIF']['ExifImageLength'])){
            $insert_array["dvp_height"] = $exif['EXIF']['ExifImageLength'];
            $insert_array["dvp_width"] = $exif['EXIF']['ExifImageWidth'];
        }else{
            $insert_array["dvp_height"] = 0;
            $insert_array["dvp_width"] = 0;
        }
        //新增資料庫函數

        $insert_array["dvp_type"] = $_FILES["{$data["field_prefix"]}_img_path"]["type"];
        $insert_array["dvp_size"] = $_FILES["{$data["field_prefix"]}_img_path"]["size"];
        //改寫圖片EXIF 檔 複寫經緯度至photo

        //上傳至資料庫
        $insert_array["photo_type"] = "width";
        $insert_array["dvp_backup"] = $filename;
        $insert_array["dvp_img_path"] = $filename;
        $insert_array["{$data["field_prefix"]}_view_link"] = $this->config->item('api_base_url').'kr_pano?image_path='.$this->config->item('api_base_url').$filename;


        /*****************************************************************************
        *180全景圖上傳處理----------END
        ******************************************************************************/
    }

}





$this->Common_model->insert_db("department_vr_photo", $insert_array);
// @unlink($filename);

$dvp_id = $this->Common_model->get_insert_id();

//處理關鍵字
$tag_ary = $this->input->post("{$data["field_prefix"]}_tag");

foreach ($tag_ary as $tag_key => $tag_value) {
    //檢查Key 是否存在
    $where_array =  array('kwd_name' =>  $tag_value);
    $tag_result = $this->Common_model->get_one("keywords", $where_array);

    if ($tag_result) {
        //有的話Insert key 到relation
        $insert_array = array(
            'dvp_id' => $dvp_id,
            'kwd_id' =>$tag_result->kwd_id);
        $this->Common_model->insert_db("tp_keyword_department_vr_photo", $insert_array);
    }else{
        //沒有更新

        $insert_array = array(
            'kwd_name' =>$tag_value);

        $this->Common_model->insert_db("keywords", $insert_array);

        $kwd_id = $this->Common_model->get_insert_id();

        $insert_array = array(
            'dvp_id' => $dvp_id,
            'kwd_id' =>$kwd_id);
        $this->Common_model->insert_db("tp_keyword_department_vr_photo", $insert_array);
    }
    

}

_alert_redirect($this->lang->line("新增成功！"), base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
