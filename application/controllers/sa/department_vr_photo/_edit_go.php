<?php

include_once "__config.php";
 

$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->post("{$data["field_prefix"]}_id"),
    "t_id" => $this->input->post("t_id"),
    "d_id" => $this->input->post("d_id"),
    "{$data["field_prefix"]}_name" => $this->input->post("{$data["field_prefix"]}_name"),
    "{$data["field_prefix"]}_lat" => $this->input->post("{$data["field_prefix"]}_lat"),
    "{$data["field_prefix"]}_lng" => $this->input->post("{$data["field_prefix"]}_lng"),
    "{$data["field_prefix"]}_state" => $this->input->post("{$data["field_prefix"]}_state")
);

//Input 中文敘述
$zh_array = array(
    "{$data["field_prefix"]}_id" => "環景圖片ID",
    "t_id" => "主題ID",
    "d_id" => "局處ID",
    "{$data["field_prefix"]}_name" => "環景圖片名稱",
    "{$data["field_prefix"]}_lat" =>  "環景經度",
    "{$data["field_prefix"]}_lng" => "環景緯度",
    "{$data["field_prefix"]}_state" => "環景狀態"
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        echo ("參數錯誤:(".$zh_array[$index].")不能為空值");
        exit();
    }
}
$input_array["{$data["field_prefix"]}_description"] = $this->input->post("{$data["field_prefix"]}_description");

//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one("department_vr_photo", $where_array);

if (!$data[$data['table_name']]) {
    echo (sprintf($this->lang->line("這個%s不存在！"), "環景圖片"));
    exit();
}


$update_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"],
    "t_id" => $input_array["t_id"],
    "d_id" => $input_array["d_id"],
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "{$data["field_prefix"]}_description" => $input_array["{$data["field_prefix"]}_description"],
    "{$data["field_prefix"]}_lat" => $input_array["{$data["field_prefix"]}_lat"],
    "{$data["field_prefix"]}_lng" => $input_array["{$data["field_prefix"]}_lng"],
    "{$data["field_prefix"]}_state" => $input_array["{$data["field_prefix"]}_state"]
);

//上傳圖片處理
$upload_path = "upload/image/";
$file_type = 'gif|jpg|jpeg|png';
$max_size = 1024;
$resize_width = 264;
$resize_height = 156;
$fit_type = "FIT_OUTER";
$max_width = 1920;
$max_height = 1080;

//上傳圖片
$field_name = "{$data["field_prefix"]}_rep_img_path";
$upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type, $max_width, $max_height);

if (isset($upload_result["upload_data"])) {
    //上傳成功，記錄上傳檔名
    $update_array[$field_name] = "upload/image/".$upload_result["upload_data"]["file_name"];
} elseif ($upload_result["error"] != "upload_no_file_selected") {
    //錯誤原因若不是因為沒有上傳檔案，就要返回並顯示錯誤訊息
    $upload_error_msg_i18n = $this->lang->line("upload");
    echo ($upload_error_msg_i18n[$upload_result["error"]]);
    exit();
}

$this->Common_model->update_db("department_vr_photo", $update_array, $where_array);

//刪除舊的圖片
if (isset($update_array[$field_name])) {
    $this->upload_lib->delete($data[$data['table_name']]->$field_name);
}


//處理關鍵字
$dvp_id = $input_array["{$data["field_prefix"]}_id"];
$tag_ary = $this->input->post("{$data["field_prefix"]}_tag");

//先移除舊的keyword
$where_array =array(
    'dvp_id' => $dvp_id 
);

$this->Common_model->delete_db('keyword_department_vr_photo',$where_array);

//再新增
foreach ($tag_ary as $tag_key => $tag_value) {
    //檢查Key 是否存在
    $where_array =  array('kwd_name' =>  $tag_value);
    $tag_result = $this->Common_model->get_one("keywords", $where_array);

    if ($tag_result) {
        //有的話Insert key 到relation
        $insert_array = array(
            'dvp_id' => $dvp_id,
            'kwd_id' =>$tag_result->kwd_id);
        $this->Common_model->insert_db("keyword_department_vr_photo", $insert_array);
    }else{
        //沒有更新

        $insert_array = array(
            'kwd_name' =>$tag_value);

        $this->Common_model->insert_db("keywords", $insert_array);

        $kwd_id = $this->Common_model->get_insert_id();

        $insert_array = array(
            'dvp_id' => $dvp_id,
            'kwd_id' =>$kwd_id);
        $this->Common_model->insert_db("keyword_department_vr_photo", $insert_array);
    }
    

}

_alert_redirect('更新成功！', base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
