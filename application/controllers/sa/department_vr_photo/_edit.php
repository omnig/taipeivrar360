<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("編輯");

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->get("{$data["field_prefix"]}_id")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one("{$data["table_name"]}", $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("%s不存在，請先設定%s"), "環景照片", "環景照片"));
    exit();
}

$where_array = array(
    "dvp_id" => $input_array["{$data["field_prefix"]}_id"]
);

//get_key_word
$keywords = $this->Common_model->get_db_join("keyword_department_vr_photo as dvp", $where_array , "keywords as ks" , "dvp.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");

foreach ($keywords as $keywords_key => $keywords_value) {
    $data['keywords'][$keywords_key] = $keywords_value->kwd_name;
}


//局處列表
$where_array = array();
$department_list = $this->Common_model->get_db("tp_admin_group", $where_array);
$data["department_list"] = $department_list;
//主題列表
$where_array = array(
        't_enabled'=>'Y',
        't_del'=>'N'
    );
$topic_list = $this->Common_model->get_db("tp_topic", $where_array);
$data["topic_list"] = $topic_list;



//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['menu_category']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/edit?{$data["field_prefix"]}_id={$this->input->get("{$data["field_prefix"]}_id")}")
    )
);
