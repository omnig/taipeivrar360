<?php

//資料表名稱
$data["table_name"] = "static_info";

//token
$data["token"] = "privacy";

//menu_category用來驗證存取權限
$data['menu_category'] = "{$data["table_name"]}/{$data["token"]}";
$data["menu_name"] = $this->lang->line("隱私權政策");

//資料表欄位前置詞
$data["field_prefix"] = "si";

//資料名稱
$data["item_name"] = $this->lang->line("資料");

//驗證權限
$this->menu_auth($data['menu_category']);

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = "編輯";

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_token" => $this->input->post("{$data["field_prefix"]}_token"),
    "{$data["field_prefix"]}_name" => $this->input->post("{$data["field_prefix"]}_name"),
    "{$data["field_prefix"]}_content_zh" => $this->input->post("{$data["field_prefix"]}_content_zh")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert("參數錯誤！");
        exit();
    }
}

//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_token" => $input_array["{$data["field_prefix"]}_token"]
);

$data[$data['table_name']] = $this->Common_model->get_one($data['table_name'], $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), $data["item_name"]));
    exit();
}

$update_array = array(
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "{$data["field_prefix"]}_content_zh" => $input_array["{$data["field_prefix"]}_content_zh"]
);

$this->Common_model->update_db($data['table_name'], $update_array, $where_array);

_alert_redirect('更新成功！', base_url("{$this->controller_name}/{$data['menu_category']}"));
exit();
