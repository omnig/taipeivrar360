<?php

//資料表名稱
$data["table_name"] = "static_info";

//token
$data["token"] = "privacy";

//menu_category用來驗證存取權限
$data['menu_category'] = "{$data["table_name"]}/{$data["token"]}";
$data["menu_name"] = $this->lang->line("隱私權政策");

//資料表欄位前置詞
$data["field_prefix"] = "si";

//資料名稱
$data["item_name"] = $this->lang->line("資料");

//驗證權限
$this->menu_auth($data['menu_category']);

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line($data["menu_name"]);

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url($data['menu_category'])
    )
);

//讀取資料庫
$where_array = array(
    "{$data["field_prefix"]}_token" => $data["token"]
);

$data[$data['table_name']] = $this->Common_model->get_one($data['table_name'], $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("%s不存在，請先設定%s"), $data["item_group_name"], $data["item_group_name"]));
    exit();
}
