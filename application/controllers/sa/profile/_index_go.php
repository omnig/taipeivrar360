<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("編輯");

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_name" => $this->input->post("{$data["field_prefix"]}_name"),
    "{$data["field_prefix"]}_account" => $this->input->post("{$data["field_prefix"]}_account")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

$input_array["{$data["field_prefix"]}_password"] = $this->input->post("{$data["field_prefix"]}_password");

//確認有沒有重複的資料
$where_array = array(
    "{$data["field_prefix"]}_account" => $input_array["{$data["field_prefix"]}_account"],
    "{$data["field_prefix"]}_id !=" => $this->login_sa->sa_id
);

$duplicated = $this->Common_model->get_one($data['table_name'], $where_array);
if ($duplicated) {
    _alert("{$this->lang->line("重複的")}{$data["item_name"]}");
    exit();
}

//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $this->login_sa->sa_id
);

$data[$data['table_name']] = $this->Common_model->get_one($data['table_name'], $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), $data["item_name"]));
    exit();
}

$update_array = array(
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "{$data["field_prefix"]}_account" => $input_array["{$data["field_prefix"]}_account"]
);

if ($input_array["{$data["field_prefix"]}_password"]) {
    $update_array["{$data["field_prefix"]}_password"] = $this->encrypt_password($input_array["{$data["field_prefix"]}_password"]);
}

$this->Common_model->update_db($data['table_name'], $update_array, $where_array);

//更新登入session資料
foreach ($update_array as $key => $val) {
    $this->login_sa->{$key} = $val;
}

$this->session->set_userdata("login_sa", $this->login_sa);

_alert_redirect($this->lang->line('更新成功！'), base_url("{$this->controller_name}/profile"));
exit();
