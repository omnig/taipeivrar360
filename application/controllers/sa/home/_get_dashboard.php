<?php

include_once "__config.php";

date_default_timezone_set('Asia/Taipei');

header('Content-Type: application/json');

$model_name = ucfirst($data['table_name']) . '_model';
$this->load->model($model_name);


//傳入值接收
$input_array = array(
    "start_date" => $this->input->get('start_date'),
    "end_date" => $this->input->get('end_date'),
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        $this->log($page, "INVALID_PARAMETER", 'N');
    }
}

$result = $this->$model_name->get_statistics($input_array['start_date'], $input_array['end_date']);

echo json_encode($result);

exit();