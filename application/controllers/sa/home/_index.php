<?php

include_once "__config.php";
date_default_timezone_set('Asia/Taipei');

$model_name = ucfirst($data['table_name']) . '_model';
$this->load->model($model_name);

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line($data["menu_name"]);

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url($data['menu_category'])
    )
);

$data["report"] = $this->$model_name->get_statistics(date('Y-m-d'), date('Y-m-d'));
$data["report_week"] = $this->$model_name->get_statistics(date('Y-m-d', strtotime('-1 week')), date('Y-m-d'));
$data["report_month"] = $this->$model_name->get_statistics(date('Y-m-d', strtotime('-1 month')), date('Y-m-d'));

// echo json_encode($this->menu);
// exit();