<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line($data["menu_name"]);

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    )
);


//token
$data["token"] = "home_vr360";

//資料表欄位前置詞
$data["field_prefix"] = "si";


//讀取資料庫
$where_array = array(
    "{$data["field_prefix"]}_token" => $data["token"]
);

$data['home_vr360'] = $this->Common_model->get_one("static_info", $where_array);

// echo json_encode($data['home_vr360']->si_content_zh );
// exit;
