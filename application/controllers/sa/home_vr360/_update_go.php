<?php

include_once "__config.php";

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_content_zh" => $this->input->post("{$data["field_prefix"]}_content_zh"),
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert("參數錯誤！");
        exit();
    }
}

$where_array = array(
    "{$data["field_prefix"]}_token" => "home_vr360"
);


$update_array = array(
    "{$data["field_prefix"]}_content_zh" => $input_array["{$data["field_prefix"]}_content_zh"]
);

$this->Common_model->update_db("static_info", $update_array, $where_array);


_alert_redirect('更新成功！', base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
