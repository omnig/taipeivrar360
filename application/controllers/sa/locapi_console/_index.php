<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = "API管理平台";

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    )
);

$data[$data['table_name']] = $this->site_config;

//地圖API清單 - 取得資料
$data["api_list"] = array(
    array(
        "id" => "get_building",
        "name" => "取得所有建物"
    ),
    array(
        "id" => "get_floor",
        "name" => "取得建物的所有樓層及POI"
    )
);

