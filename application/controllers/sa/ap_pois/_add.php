<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = '新增POI';

//取得資料
if($this->input->get('af_id')!==null){
    $where_array = array(
        'af_id' => $this->input->get('af_id')
    );
    $data['floor'] = $this->Common_model->get_one('ap_floors as af', $where_array);

    if (!isset($data['floor'])) {
        _alert('樓層不存在');
        exit();
    }
}

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/add")
    )
);
$data["default_lat"] = $this->site_config->c_app_lat;
$data["default_lng"] = $this->site_config->c_app_lng;

$data['category_list'] = $this->Common_model->get_db('ap_category', ['ac_enabled' => 'Y']);
$data['ar_list'] = $this->Common_model->get_db('department_ar', ['tdar_state' => 'Y', 'tdar_del' => 'N']);
$data["department_list"] = $this->Common_model->get_db("tp_admin_group", []);

$model_name = 'Beacon_model';
$this->load->model($model_name);
$data['beacon_list'] = $this->Beacon_model->get_unbinded_beacons();

$model_name = 'Optical_label_model';
$this->load->model($model_name);
$data['optical_label_list'] = $this->Optical_label_model->get_unbinded_optical_label();
