<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = "新增";

//傳入值接收
$input_array = array(
    "d_id" => $this->input->post("d_id"),
    "{$data["field_prefix"]}_name" => $this->input->post("{$data["field_prefix"]}_name"),
    "{$data["field_prefix"]}_desc" => $this->input->post("{$data["field_prefix"]}_desc"),
    "{$data["field_prefix"]}_lat" => $this->input->post("{$data["field_prefix"]}_lat"),
    "{$data["field_prefix"]}_lng" => $this->input->post("{$data["field_prefix"]}_lng"),
    "{$data["field_prefix"]}_enabled" => $this->input->post("{$data["field_prefix"]}_enabled"),
    "ac_id" => $this->input->post("ac_id"),
    "af_id" => ($this->input->post("af_id") !== null) ? $this->input->post("af_id") : 1, //沒af_id的話直接讓POI歸屬室外建物
);

foreach ($input_array as $index => $value) {
    if (empty($value)) {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}
if (!isset($_FILES["{$data["field_prefix"]}_image_path"]) || empty($_FILES["{$data["field_prefix"]}_image_path"]["name"])){
    _alert($this->lang->line("參數錯誤"));
    exit();
}

$insert_array = array(
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "d_id" => $input_array["d_id"],
    "{$data["field_prefix"]}_desc" => $input_array["{$data["field_prefix"]}_desc"],
    "{$data["field_prefix"]}_lat" => $input_array["{$data["field_prefix"]}_lat"],
    "{$data["field_prefix"]}_lng" => $input_array["{$data["field_prefix"]}_lng"],
    "{$data["field_prefix"]}_enabled" => $input_array["{$data["field_prefix"]}_enabled"],
    "ac_id" => $input_array["ac_id"],
    "af_id" => $input_array["af_id"],
    "{$data["field_prefix"]}_hyperlink_text" => $this->input->post("{$data["field_prefix"]}_hyperlink_text"),
    "{$data["field_prefix"]}_hyperlink_url" => $this->input->post("{$data["field_prefix"]}_hyperlink_url"),
    "{$data["field_prefix"]}_video_hyperlink" => $this->input->post("{$data["field_prefix"]}_video_hyperlink"),
    "tdar_id" => $this->input->post("tdar_id"),
    "{$data["field_prefix"]}_ar_active_method" => $this->input->post("{$data["field_prefix"]}_ar_active_method"),
    "dai_id" => $this->input->post("dai_id"),
    "{$data["field_prefix"]}_ar_text" => $this->input->post("{$data["field_prefix"]}_ar_text"),
    "{$data["field_prefix"]}_ar_url" => $this->input->post("{$data["field_prefix"]}_ar_url"),
    "{$data["field_prefix"]}_ar_distance" => $this->input->post("{$data["field_prefix"]}_ar_distance"),
    "{$data["field_prefix"]}_ar_heigh" => $this->input->post("{$data["field_prefix"]}_ar_heigh"),
    "{$data["field_prefix"]}_ar_vsize" => $this->input->post("{$data["field_prefix"]}_ar_vsize"),
    "{$data["field_prefix"]}_create_timestamp" => date('Y-m-d H:i:s')
);

if($insert_array["{$data["field_prefix"]}_ar_active_method"] === '2'){
    $insert_array["{$data["field_prefix"]}_cover_identify_image"] = $this->input->post("{$data["field_prefix"]}_cover_identify_image");
}

//{$data["field_prefix"]}_image_path
// //上傳圖片處理

$upload_path = "upload_image/";
$file_type = 'jpg|jpeg|png';
$max_size = 10240;
$resize_width = 1920;
$resize_height = 1080;
$fit_type = "FIT_OUTER";

//上傳代表圖片
$field_name = "{$data["field_prefix"]}_image_path";
$upload_result = $this->upload_lib->write_from_image($upload_path, $field_name, $file_type, $max_size, $resize_width, $resize_height, $fit_type);

if (!isset($upload_result["upload_data"])) {
    //必須上傳圖片
    echo ($this->lang->line("圖片上傳失敗或沒有上傳圖片"));
    exit();
}else{
    //上傳成功，記錄上傳檔名
    $insert_array["{$data["field_prefix"]}_image_path"] = $upload_result["upload_data"]["file_name"];
}


// //上傳語音檔處理
if (isset($_FILES["{$data["field_prefix"]}_audio_path"]) && !empty($_FILES["{$data["field_prefix"]}_audio_path"]["name"])) {
    //{$data["field_prefix"]}_audio_path
    // //上傳語音檔處理
    $upload_path = "upload/audio";
    $file_type = 'mp3';
    $max_size = 8192;
    
    //上傳語音檔
    $field_name = "{$data["field_prefix"]}_audio_path";
    $upload_result = $this->upload_lib->write_from_upload($upload_path, $field_name, $file_type, $max_size);
    if (!isset($upload_result["upload_data"])) {
        //必須上傳影音檔
        echo ($this->lang->line("影音檔上傳失敗"));
        exit();
    }else{
        //上傳成功，記錄上傳檔名
        $insert_array["{$data["field_prefix"]}_audio_path"] = $upload_result["upload_data"]["file_name"];
    }
}else{
    $insert_array["{$data["field_prefix"]}_audio_path"] = null;
}

// //新增Anyplace DB，後續要改成poi的邏輯而不是building的
// $query_info = array(
//     'is_published' => 'false',
//     'name' => $input_array["{$data["field_prefix"]}_name"],
//     'description' => $input_array["{$data["field_prefix"]}_desc"],
//     'url' => '-',
//     'address' => '-',
//     'coordinates_lat' => $input_array["{$data["field_prefix"]}_lat"],
//     'coordinates_lon' => $input_array["{$data["field_prefix"]}_lng"]
// );

// //載入helper
// $this->load->helper('anyplace_helper');
// $result = get_mapping_result('add', 'building', $query_info);

// if ($result['status_code'] == 200) {
//     $insert_array["{$data["field_prefix"]}_buid"] = $result['buid'];
// } else {
//     _alert('新增失敗 [' . $result['status_code'] . ':' . $result['message'] . ']。\n\n請再試一次，或洽系統管理員。');
//     exit();
// }

//新增Mysql DB Building Table
$this->Common_model->trans_start();
$this->Common_model->insert_db($data['table_name'], $insert_array);

$poi_id = $this->Common_model->get_insert_id();
// $this->Common_model->delete_db('beacon_poi_relation', ['ap_id' => $poi_id]);
if(!empty($this->input->post("b_id"))){
    $this->Common_model->insert_db('beacon_poi_relation', ['ap_id' => $poi_id, 'b_id' => $this->input->post("b_id")]);
}
if(!empty($this->input->post("ol_id"))){
    $this->Common_model->insert_db('optical_label_poi_relation', ['ap_id' => $poi_id, 'ol_id' => $this->input->post("ol_id")]);
}
$this->Common_model->trans_complete();



if($this->input->post("af_id") !== null){
    _alert_redirect($this->lang->line("新增成功！"), base_url("{$this->controller_name}/{$data['table_name']}?af_id={$this->input->post('af_id')}"));
    exit();
}
_alert_redirect($this->lang->line("新增成功！"), base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
