<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("編輯") . 'POI';

//取得資料
if($this->input->get('af_id')!==null){
    $where_array = array(
        'af_id' => $this->input->get('af_id')
    );
    $data['floor'] = $this->Common_model->get_one('ap_floors as af', $where_array);

    if (!isset($data['floor'])) {
        _alert('樓層不存在');
        exit();
    }
}

//麵包屑
$data["bread"] = array(
    array(
        "title" => $data["menu_name"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}")
    ),
    array(
        "title" => $data["small_title"],
        "url" => base_url("{$this->controller_name}/{$data['table_name']}/edit?{$data["field_prefix"]}_id={$this->input->get("{$data["field_prefix"]}_id")}")
    )
);

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->get("{$data["field_prefix"]}_id")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤"));
        exit();
    }
}

$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one($data['table_name'], $where_array);
$data[$data['table_name']]->b_id = null;
if($beacon = $this->Common_model->get_one('beacon_poi_relation', $where_array)){
    $data[$data['table_name']]->b_id = $beacon->b_id;
}
$data[$data['table_name']]->ol_id = null;
if($beacon = $this->Common_model->get_one('optical_label_poi_relation', $where_array)){
    $data[$data['table_name']]->ol_id = $beacon->ol_id;
}

if(!is_null($data[$data['table_name']]->dai_id)){
    $data["ar_image"] = $this->Common_model->get_one('department_ar_image', ["dai_id" => $data[$data['table_name']]->dai_id]);
}else{
    $data["ar_image"] = null;
}

$data['category_list'] = $this->Common_model->get_db('ap_category', ['ac_enabled' => 'Y']);
$data['ar_list'] = $this->Common_model->get_db('department_ar', ['tdar_state' => 'Y', 'tdar_del' => 'N']);
$data["department_list"] = $this->Common_model->get_db("tp_admin_group", []);

$data["default_lat"] = $this->site_config->c_app_lat;
$data["default_lng"] = $this->site_config->c_app_lng;

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("%s不存在，請先設定%s"), $data["item_group_name"], $data["item_group_name"]));
    exit();
}

$model_name = 'Beacon_model';
$this->load->model($model_name);
$data['beacon_list'] = $this->Beacon_model->get_unbinded_beacons();

$model_name = 'Optical_label_model';
$this->load->model($model_name);
$data['optical_label_list'] = $this->Optical_label_model->get_unbinded_optical_label();
