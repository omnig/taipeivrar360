<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->lang->line("site_name")} | {$data["menu_name"]}";
$data["big_title"] = $this->lang->line("site_name");
$data["small_title"] = $this->lang->line("登出");

//清空登入session
$this->session->unset_userdata("login_sa");
$this->session->unset_userdata("access_token");
//清空登入變數
$this->login_sa = null;

//重導至登入頁
header("Location: " . base_url("{$this->controller_name}/login"));
exit();
