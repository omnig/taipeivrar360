<?php
include_once "__config.php";
$this->load->helper("point");
// $this->load->helper("google");
$model_name = ucfirst($data['table_name']) . '_model';
$this->load->model($model_name);

// $ret = $this->$model_name->get_point_data(array(1174,10,19,39,54,67,68,70,82,83,85,118,120,127,131));
// $ret = google_place_content_by_id();
// echo json_encode($ret);
// exit;


//下載API 內容
$xml_string = curl_query("https://khh.travel/spotxml.aspx","GET");
$xml_object = simplexml_load_string($xml_string);
$view_points = $xml_object->Infos[0];
$view_points = $view_points->Info;


$full_data_array = array();
$where_array = array();

foreach ($view_points as $v_key => $v_value) {

	$where_array = array(
		"{$data["field_prefix"]}_dataid" => (string)$v_value["Id"]
	);

	$full_data_array = array(
	    'a_id' => fetch_area_id_from_address((string)$v_value["Add"]),
	    "{$data["field_prefix"]}_dataid" => (string)$v_value["Id"],
	    "{$data["field_prefix"]}_name" => (string)$v_value["Name"],
	    "{$data["field_prefix"]}_desc" => (string)$v_value["Toldescribe"],
	    "{$data["field_prefix"]}_tel" => (string)$v_value["Tel"],
	    "{$data["field_prefix"]}_web" => (string)$v_value["Website"],
	    "{$data["field_prefix"]}_image" => (string)$v_value["Picture1"],
	    "{$data["field_prefix"]}_address" => (string)$v_value["Add"],
	    "{$data["field_prefix"]}_class" => (string)$v_value["Class1"],
	    "{$data["field_prefix"]}_level" => (string)$v_value["Level"],
	    "{$data["field_prefix"]}_ticketinfo" => (string)$v_value["Ticketinfo"],
	    "{$data["field_prefix"]}_opentime" => (string)$v_value["Opentime"],
	    "{$data["field_prefix"]}_travellinginfo" => (string)$v_value["Travellinginfo"],
	    "{$data["field_prefix"]}_remarks" => (string)$v_value["Remarks"],
	    "{$data["field_prefix"]}_lat" => (string)$v_value["Py"],
	    "{$data["field_prefix"]}_lng" => (string)$v_value["Px"],
	    "{$data["field_prefix"]}_api_from" => 'API',
	    "{$data["field_prefix"]}_changetime" => date('Y-m-d H:i:s'),
	    "{$data["field_prefix"]}_update_timestamp" => date('Y-m-d H:i:s')
	);


	// 排除地址無法判斷區域問題
	if(fetch_area_id_from_address((string)$v_value["Add"])!=0){
		//排除類別為宗廟
		if((string)$v_value["Class1"]!="04"){
			$ret = $this->Common_model->update_or_insert($data['table_name'],$full_data_array, $where_array,FALSE);
			// 取得ID加入總表Table
			$insert_id = $this->Common_model->get_insert_id();
			if($insert_id!=NULL){
				$this->$model_name->insert_point_data("v",$insert_id);
			}
		}
		
	}

}



_alert_redirect("更新成功", base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
