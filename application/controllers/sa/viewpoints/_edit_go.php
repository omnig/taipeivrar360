<?php

include_once "__config.php";

//自動組成前端基本資訊
$data["meta"]["title"] = "{$this->site_config->c_site_name} | {$data["menu_name"]}";
$data["big_title"] = $this->site_config->c_site_name;
$data["small_title"] = "編輯";

//傳入值接收
$input_array = array(
    "{$data["field_prefix"]}_id" => $this->input->post("{$data["field_prefix"]}_id"),
    "{$data["field_prefix"]}_name" => $this->input->post("{$data["field_prefix"]}_name"),
    "{$data["field_prefix"]}_lat" => $this->input->post("{$data["field_prefix"]}_lat"),
    "{$data["field_prefix"]}_lng" => $this->input->post("{$data["field_prefix"]}_lng"),
    "{$data["field_prefix"]}_enabled" => $this->input->post("{$data["field_prefix"]}_enabled"),
    "{$data["field_prefix"]}_recommand" => $this->input->post("{$data["field_prefix"]}_recommand")
);

foreach ($input_array as $index => $value) {
    if ($value === null || $value === '') {
        _alert($this->lang->line("參數錯誤").$index);
        exit();
    }
}

$input_array["{$data["field_prefix"]}_desc"] = $this->input->post("{$data["field_prefix"]}_desc");
$input_array["{$data["field_prefix"]}_desc_en"] = $this->input->post("{$data["field_prefix"]}_desc_en");
$input_array["{$data["field_prefix"]}_tel"] = $this->input->post("{$data["field_prefix"]}_tel");
$input_array["{$data["field_prefix"]}_web"] = $this->input->post("{$data["field_prefix"]}_web");
$input_array["{$data["field_prefix"]}_image"] = $this->input->post("{$data["field_prefix"]}_image");
$input_array["{$data["field_prefix"]}_address"] = $this->input->post("{$data["field_prefix"]}_address");
$input_array["{$data["field_prefix"]}_ticketinfo"] = $this->input->post("{$data["field_prefix"]}_ticketinfo");
$input_array["{$data["field_prefix"]}_opentime"] = $this->input->post("{$data["field_prefix"]}_opentime");
$input_array["{$data["field_prefix"]}_travellinginfo"] = $this->input->post("{$data["field_prefix"]}_travellinginfo");
$input_array["{$data["field_prefix"]}_remarks"] = $this->input->post("{$data["field_prefix"]}_remarks");



//取得此資料，確定資料存在
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$data[$data['table_name']] = $this->Common_model->get_one($data['table_name'], $where_array);

if (!$data[$data['table_name']]) {
    _alert(sprintf($this->lang->line("這個%s不存在！"), $data["item_name"]));
    exit();
}

//準備更新資料庫
$where_array = array(
    "{$data["field_prefix"]}_id" => $input_array["{$data["field_prefix"]}_id"]
);

$update_array = array(
    "{$data["field_prefix"]}_name" => $input_array["{$data["field_prefix"]}_name"],
    "{$data["field_prefix"]}_desc" => $input_array["{$data["field_prefix"]}_desc"],
    "{$data["field_prefix"]}_name_en" => "",
    "{$data["field_prefix"]}_desc_en" => $input_array["{$data["field_prefix"]}_desc_en"],
    "{$data["field_prefix"]}_tel" => $input_array["{$data["field_prefix"]}_tel"],
    "{$data["field_prefix"]}_web" => $input_array["{$data["field_prefix"]}_web"],
    "{$data["field_prefix"]}_image" => $input_array["{$data["field_prefix"]}_image"],
    "{$data["field_prefix"]}_address" => $input_array["{$data["field_prefix"]}_address"],
    "{$data["field_prefix"]}_ticketinfo" => $input_array["{$data["field_prefix"]}_ticketinfo"],
    "{$data["field_prefix"]}_opentime" => $input_array["{$data["field_prefix"]}_opentime"],
    "{$data["field_prefix"]}_travellinginfo" => $input_array["{$data["field_prefix"]}_travellinginfo"],
    "{$data["field_prefix"]}_remarks" => $input_array["{$data["field_prefix"]}_remarks"],
    "{$data["field_prefix"]}_lat" => $input_array["{$data["field_prefix"]}_lat"],
    "{$data["field_prefix"]}_lng" => $input_array["{$data["field_prefix"]}_lng"],
    "{$data["field_prefix"]}_enabled" => $input_array["{$data["field_prefix"]}_enabled"],
    "{$data["field_prefix"]}_recommand" => $input_array["{$data["field_prefix"]}_recommand"],
    "{$data["field_prefix"]}_update_timestamp" => date('Y-m-d H:i:s')
);



$this->Common_model->update_db($data['table_name'], $update_array, $where_array);

$e_id = $input_array["{$data["field_prefix"]}_id"];
$e_name = $input_array["{$data["field_prefix"]}_name"];




_alert_redirect('更新成功！', base_url("{$this->controller_name}/{$data['table_name']}"));
exit();
