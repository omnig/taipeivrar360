<?php
include_once "__config.php";
$this->load->helper("point");
$model_name = ucfirst($data['table_name']) . '_model';
$this->load->model($model_name);

// 使用遞迴自動加載頁數撈取資料
function recusive_load_data($url="",$page=1,$data_collection=array()){

	$json_string = curl_query($url.$page,"GET");
	$json_array = json_decode($json_string,TRUE);
	$json_page = $json_array["pagesInfo"][0]["_Pages"];
	foreach ($json_array["ResultList"] as $key => $value) {
		array_push($data_collection, $value);
	}
	if($page==$json_page){
		return ($data_collection);
	}else{
		$page+=1;
		$data_collection = recusive_load_data($url,$page,$data_collection);
		return ($data_collection);
	}
}


//下載API 內容
$view_points = recusive_load_data("https://theme.khh.travel/gmap/JasonResult.aspx?lang=2&GetType=1&layer1=12&cityCode=10017&page=");


$full_data_array = array();
$where_array = array();


foreach ($view_points as $v_key => $v_value) {

	if((string)$v_value["name"]!=""){
		$v_value["id"] = str_pad($v_value["id"],4,'0',STR_PAD_LEFT);
		$v_value["id"] = "C1_397000000A_00".$v_value["id"];
		$where_array = array(
			"{$data["field_prefix"]}_dataid" => (string)$v_value["id"]
		);

		$full_data_array = array(
		    "{$data["field_prefix"]}_name_en" => (string)$v_value["name"],
		    "{$data["field_prefix"]}_desc_en" => "",
		);

		// 排除地址無法判斷區域問題
		if(fetch_area_id_from_address((string)$v_value["address"])!=0){
			$ret = $this->Common_model->update_db($data['table_name'],$full_data_array, $where_array);

			// 取得ID加入總表Table
			// $insert_id = $this->Common_model->get_insert_id();
			// if($insert_id!=NULL){
			// 	$this->$model_name->insert_point_data("s",$insert_id);
			// }
		}
	}
	

}

_alert_redirect("更新成功", base_url("{$this->controller_name}/{$data['table_name']}"));
exit();


