<?php

include_once "__config.php";

$model_name = ucfirst($data['table_name']) . '_model';
$this->load->model($model_name);


//傳入值接收
$input_array = array(
    "limit" => $this->input->post('length'),
    "skip" => $this->input->post('start'),
    "draw" => intval($this->input->get_post('draw')),
    "columns" => $this->input->post('columns'),
    "order" => $this->input->post('order'),
    "filters" => $this->input->post('filters'),
    "customActionType" => $this->input->post('customActionType'),
    "customActionName" => $this->input->post('customActionName'),
    "id" => $this->input->post('id')
);

//資料分頁設定
$input_array['limit'] = intval($input_array['limit']) < 0 ? 0 : intval($input_array['limit']);
$input_array['skip'] = intval($this->input->get_post('start'));

$where_array = array(
    'dai_state' =>'Y',
    'dai_characteristic_state' => 'Y',
    'dai_del'=>'N');

//依傳入參數過濾
if ($input_array['filters']) {
    $concat_where_string = "";

    foreach ($input_array['filters'] as $field_name => $value) {
        if ($value == '') {
            continue;
        }

        $where_string = "";

        switch ($field_name) {
            //比對完全相同的值
            case "d_id":
            case "dar.t_id":
            case "{$data["field_prefix"]}_star" :
            $where_string = "({$field_name} = '{$value}')";
                break;

            //全文搜尋
            case "{$data["field_prefix"]}_name" :
            case "{$data["field_prefix"]}_state" :
            case "{$data["field_prefix"]}_description" :
            case "{$data["field_prefix"]}_enabled" :
                $where_string = "({$field_name} LIKE '%{$value}%')";
                break;

            //其它特殊搜尋遇濾方式
            case 'create_from' :
                $where_string = "({$data["field_prefix"]}_create_timestamp >= '{$value} 00:00:00')";
                break;
            case 'create_to' :
                $where_string = "({$data["field_prefix"]}_create_timestamp <= '{$value} 23:59:59')";
                break;
            case "t_id":
                $where_string = "(t.t_id = '{$value}')";
                break;
        }

        if ($concat_where_string == '') {
            $concat_where_string = $where_string;
        } else {
            $concat_where_string = "$concat_where_string AND $where_string";
        }
    }

    if ($concat_where_string != '') {
        $where_array[] = $concat_where_string;
    }
}

//排序欄位，對映order的排序欄位
$sort_fields = array(
    false,
    false ,false, false,"{$data["field_prefix"]}_star", "{$data["field_prefix"]}_name", "{$data["field_prefix"]}_description",  "{$data["field_prefix"]}_content_type","d_id", "{$data["field_prefix"]}_create_timestamp","{$data["field_prefix"]}_state","{$data["field_prefix"]}_characteristic_state","{$data["field_prefix"]}_to_og", false ,false
);

$order_by = "{$data["field_prefix"]}_create_timestamp DESC";
if (isset($input_array['order'] )) {
    foreach ($input_array['order'] as $row) {
        if ($sort_fields[$row['column']]) {
            if ($order_by == '') {
                $order_by = "{$sort_fields[$row['column']]} {$row['dir']}";
                continue;
            }

            $order_by = "{$sort_fields[$row['column']]} {$row['dir']}";
        }
    }
}


//計算資料總數
$iTotalRecords = $this->$model_name->get_list($order_by, '', 0, 0, $where_array, true);

//從資料庫取得本次資料
$ret = $this->$model_name->get_list($order_by, '', $input_array['limit'], $input_array['skip'], $where_array, false);

// echo json_encode($ret);
// exit();
//回傳值
$records = array();
$records["data"] = array();

$select_ar_url = base_url("{$this->controller_name}/ar_image_list/select");

//html表格內容
foreach ($ret as $i => $row) {

    $id = $row->{"{$data["field_prefix"]}_id"};
    $img_url = base_url("upload_image/" . $row->{"{$data["field_prefix"]}_image_path"});
    $img_id = basename($img_url);

    // 星等加入
    $star_string = "";
    $star_true = '<i class="fa fa-star" style="color: #efe422;font-size: 20px;"></i>';
    $star_false = '<i class="fa fa-star-o" style="color: #efe422;font-size: 20px;"></i>';

    for ($star_index=0; $star_index < 3; $star_index++) { 
        if ($star_index < $row->{"{$data["field_prefix"]}_star"}) {
            $star_string .=$star_true;
        }else{
            $star_string .=$star_false;
        }
    }
    $image_code = preg_replace("/\.(jpg|jpeg|png)/", "", $row->{"{$data["field_prefix"]}_image_path"});

    $records["data"] [] = array(
        $i + 1,
        $image_code,
        $row->{"{$data["field_prefix"]}_name"},
        "<img class='img-responsive' id='{$img_id}' src='{$img_url}' style='cursor: pointer;' onclick='closeimg(\"{$img_id}\")' />",
        "<a href='{$select_ar_url}?dai_id={$row->{"{$data["field_prefix"]}_id"}}' class='btn btn-xs default green-stripe btn-operating'><i class='fa fa-check-square'></i> 選擇 </a>"
    );
}

$records["draw"] = $input_array["draw"];
$records["recordsTotal"] = $iTotalRecords;
$records["recordsFiltered"] = $iTotalRecords;

echo json_encode($records);
exit();
