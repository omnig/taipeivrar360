<?php

class Cli extends CI_Controller {

    var $site_config;   //網頁一般設定
    var $controller_name = "cli";
    var $page;  //目前頁面
    var $proxy_server;

    public function __construct() {
        parent::__construct();
        if (!is_cli()) {
            // die("ACCESS DENY!");
        }
        
        date_default_timezone_set('Asia/Taipei');
        
        //載入config
        $this->site_config = $this->Common_model->get_one('config');
    }

    /*
     * 取得查詢url
     * 以此url進行api查詢
     */

    private function get_query_url($url) {
        return $url;
    }

    /*
     * 主功能頁面
     */

    public function view($page = '') {
        $this->page = $page;

        if (!file_exists(APPPATH . "/controllers/{$this->controller_name}/_{$page}.php")) {
            show_404();
        }

        //載入controller
        include(APPPATH . "/controllers/{$this->controller_name}/_{$page}.php");
    }

    private function get_order_number($o_id) {
        return sprintf("{$this->config->item("order_number_prefix")}%08d", $o_id);
    }


}
