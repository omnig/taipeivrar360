<?php

class Locapi extends CI_Controller {

    var $site_config;   //網頁一般設定
    var $controller_name = "locapi";
    var $page;  //目前頁面
    var $i18n;

    public function __construct() {
        parent::__construct();

        //載入config
        $this->site_config = $this->Common_model->get_one('config');

        //載入前台model
        $this->load->model("front_model");

        //載入Library
        $this->load->library('user_agent');

        //定義使用者目前位置
        $this->user_latlon = array(
            "lat" => 0,
            "lon" => 0
        );
    }

    /* ===============================
     * 記錄log
     * 
     * 傳入參數：
     * $api_name: API(或存取目標)名稱
     * $result: 處理結果
     * $success: 是否成功(Y/N)
     * =============================== */

    private function log($api_name, $result, $success, $input = false) {
        if (!$input) {
            $input = array(
                "get" => $this->input->get(),
                "post" => $this->input->post()
            );
        }

        $insert_array = array(
            "la_ip" => get_remote_addr(),
            "la_api_name" => $api_name,
            "la_params" => json_encode($input),
            "la_result" => $result,
            "la_success" => $success
        );

        $this->Common_model->insert_db("log_locapi", $insert_array);

        if ($success == 'N') {
            //顯示呼叫結果
            $ret = array(
                "result" => "false",
                "error_message" => $result,
                "data" => array()
            );

            echo json_encode($ret);

            exit();
        }
    }

    /*
     * 驗證mac
     */

    private function verify_mac($timestamp, $mac) {
        if (abs($timestamp - time()) > $this->config->item("api_timeout")) { //時間超過限制則視為失敗
            return false;
        }

        if (sha1($this->config->item('encrypt_token') . $timestamp) != $mac) {
            return false;
        }

        return true;
    }

    /*
     * 主功能頁面
     */

    public function view($page = 'home', $func = '') {
        //識別語系代碼
        $this->i18n = $this->get_locale_code();

        //載入語系檔
        $this->lang->load("front", $this->session->i18n);

        if ($func != '') {
            if (file_exists(APPPATH . "/controllers/{$this->controller_name}/{$page}/{$func}.php")) {
                //載入特定功能函式
                include(APPPATH . "/controllers/{$this->controller_name}/{$page}/{$func}.php");
                exit();
            }
        }

        $this->page = $page;

        if (!file_exists(APPPATH . "/controllers/{$this->controller_name}/_{$page}.php")) {
            show_404();
        }
        include(APPPATH . "/controllers/{$this->controller_name}/_{$page}.php");
    }

    /* ==================================
     * 取得語言碼(zh / en)
     * ================================== */

    private function get_locale_code() {
        //若有傳入url參數，更改成傳入的語系
        $lang = strtolower($this->input->post('lang'));

        if (in_array($lang, array('zh', 'en'))) {
            //語系寫入session
            $this->session->set_userdata(array('i18n' => $lang));

            return $lang;
        }

        //若session有記錄語系，使用session記錄語系
        $lang = $this->session->i18n;

        if (!$lang) {
            //偵測語系
            $http_accept_language = strtolower($this->input->server('HTTP_ACCEPT_LANGUAGE'));

            //初始值，最後回傳最小者
            $en_index = 9999;
            $zh_index = 9998;

            $tmp = strpos($http_accept_language, 'zh');
            if ($tmp !== false && $tmp < $zh_index) {
                $zh_index = $tmp;
            }

            $tmp = strpos($http_accept_language, 'en');
            if ($tmp !== false && $tmp < $en_index) {
                $en_index = $tmp;
            }

            //將語言id最小者回傳
            $min_index = 10000;
            if ($en_index < $min_index) {
                $min_index = $en_index;
                $lang = 'en';
            }
            if ($zh_index < $min_index) {
                $lang = 'zh';
            }

            //記錄session
            $this->session->set_userdata(array('i18n' => $lang));
        }

        return $lang;
    }

    /*
     * 密碼加密規則
     */

    private function encrypt_password($input) {
        return sha1($this->config->item('encrypt_token') . $input);
    }

    /*
     * 登入
     */

    private function login($account, $password) {
        $where_array = array(
            "(u_account='{$account}' OR (u_email='{$account}' AND u_email_confirmed='Y') OR u_phone='{$account}')",
            "u_password" => $password,
            'u_email_confirmed' => 'Y',
            "u_enabled" => "Y",
            "u_del" => "N"
        );

        $user = $this->Common_model->get_one("user", $where_array);

        if (!$user) {
            return false;
        }

        //產生登入token
        $user->u_login_token = substr(md5(uniqid("utobonus", true)), 0, 40);

        //更新登入時間及token
        $update_array = array(
            "u_login_token" => $user->u_login_token,
            'u_last_login_timestamp' => date('Y-m-d H:i:s')
        );

        $where_array = array(
            'u_id' => $user->u_id
        );

        $this->Common_model->update_db("user", $update_array, $where_array);

        return $user;
    }

    /*
     * facebook 登入
     */

    private function fb_login($fb_id, $fb_token, $ip, $user_agent) {
        $url = "https://graph.facebook.com/{$fb_id}?access_token={$fb_token}";
        $result = json_decode(curl_query($url, "get"));

        if (!isset($result->name)) {
            return false;
        }

        $where_array = array(
            "u_fbid" => $fb_id,
            "u_del" => "N"
        );

        $user = $this->Common_model->get_one("user", $where_array);

        if (!$user) {
            return false;
        }

        //產生登入token
        $user->u_login_token = substr(md5(uniqid("utobonus", true)), 0, 40);

        //更新登入時間及token
        $update_array = array(
            'u_login_token' => $user->u_login_token,
            'u_fb_token' => $fb_token,
            'u_last_login_timestamp' => date('Y-m-d H:i:s'),
            "u_ip" => $ip,
            "u_user_agent" => $user_agent
        );

        $where_array = array(
            'u_id' => $user->u_id
        );

        $this->Common_model->update_db("user", $update_array, $where_array);

        return $user;
    }

    private function get_userlocation() {
        //取得使用者目前位置
        $user_location = $this->input->cookie("user_location");
        if ($user_location) {
            $latlon = explode(',', $user_location);
            $lat = $latlon[0];
            $lon = $latlon[1];
            $this->user_latlon = array(
                "lat" => $lat,
                "lon" => $lon
            );
        } else {
            $this->user_latlon = array(
                "lat" => 0,
                "lon" => 0
            );
        }
    }

    private function alert($caption = "", $content = "", $return_uri = "") {

        if ($return_uri == "") {
            $return_uri = $this->return_uri;
            //如果來自alert頁面，則跳轉返回時重導至首頁
            if (strpos($return_uri, base_url("alert")) !== false) {
                $return_uri = base_url("/");
            }
        }

        $alert = array(
            "caption" => $caption,
            "content" => $content,
            "return_uri" => $return_uri
        );

        $this->session->set_userdata("alert", $alert);

        header("Location: " . base_url("alert"));
        exit();
    }

    private function get_fb_login_url() {
        $fb_config = $this->config->item("facebook");

        $fb = new Facebook\Facebook([
            'app_id' => $fb_config["app_id"],
            'app_secret' => $fb_config["app_secret"],
            'default_graph_version' => 'v2.4',
        ]);

        $helper = $fb->getRedirectLoginHelper();

        $loginUrl = $helper->getLoginUrl($this->config->item("server_base_url") . "fb-callback?return_uri=" . current_full_url(), $fb_config["permissions"]);

        return $loginUrl;
    }

    private function require_login() {
        if (!$this->login_user) {
            $caption = $this->lang->line("請先登入");
            $content = sprintf($this->lang->line("尚未登入或登入逾時，請重新登入。"));
            $redirect_uri = base_url("/");
            $this->alert($caption, $content, $redirect_uri);
            exit();
        }
    }

    private function get_order_number($o_id) {
        return sprintf("{$this->config->item("order_number_prefix")}%08d", $o_id);
    }

    private function get_o_id($order_number) {
        return (int) substr($order_number, 1);
    }

}
