<?php

class Sa extends CI_Controller {

    var $site_config;   //網頁一般設定
    var $login_sa;    //登入使用者
    var $menu_list; //menu清單
    var $menu_allow_all;  //允許任何人存取的menu清單
    var $admin_menu_list; //menu清單
    var $admin_menu_allow_all;  //允許任何人存取的menu清單
    var $menu;  //實際有權限、可存取的menu
    var $controller_name = "sa";
    var $theme = 'blue';
    var $key_location = 'oauth_path';

    public function __construct() {
        parent::__construct();

        //載入config
        $this->site_config = $this->Common_model->get_one('config');

        //登入管理員
        $this->login_sa = $this->session->userdata('login_sa');

        //menu清單
        $this->menu_list = $this->config->item("menu_list");

        $this->menu_allow_all = $this->config->item("menu_allow_all");

        $this->menu = $this->get_valid_menu($this->menu_list, isset($this->login_sa->sa_group) ? $this->login_sa->sa_group : '');   //記錄此帳號有權限可存取的menu
        
        // $sa_auth = $this->Common_model->get_db("tp_sa_auth");
        // echo json_encode($this->menu);
        // exit();
        //admin menu
        $this->admin_menu_list = $this->config->item("admin_menu_list");
        
        //識別語系代碼
        $this->get_locale_code();

        //載入語系檔
        $this->lang->load($this->controller_name, "zh");
        $this->lang->load("upload",  "zh");

        //google_oauth伺服器驗證JSON檔
        $this->key_location = dirname(__DIR__).'/OAuth/client_secret_811181325009-0mop4r7ohh8jskev30pq07riv45ic7cj.json';

        if (isset($_SESSION['access_token'])) {
            $this->check_g_accesstoken($_SESSION['access_token']);
        }


        //效能分析開啟
        //$this->output->enable_profiler(TRUE);
        //效能分析關閉
         $this->output->enable_profiler(FALSE);

    }

    /*
     * 主功能頁面
     */

    public function view($page = 'home', $func = 'index') {

        $this->check_login_status($page);

        @$data["meta"]["title"] = $this->site_config->c_site_name;
        @$data["meta"]["keyword"] = $this->site_config->c_meta_keyword;
        @$data["meta"]["description"] = $this->site_config->c_meta_description;
        @$data["meta"]["author"] = "";

        if (file_exists(APPPATH . "/controllers/{$this->controller_name}/{$page}/_{$func}.php")) {
            //載入controller
            include(APPPATH . "/controllers/{$this->controller_name}/{$page}/_{$func}.php");
        } elseif (!file_exists(APPPATH . "/views/{$this->controller_name}/{$page}.php")) {
            //controller和view都不存在，show 404
            $page = '404';
            $func = 'index';
            include(APPPATH . "/controllers/{$this->controller_name}/{$page}/_{$func}.php");
            //show_404();
        }

        $this->load->view("{$this->controller_name}/{$page}/{$func}", $data);
    }

    /*
     * 驗證具權限的menu
     */

    private function get_valid_menu($all_menu, $sa_group) {
        //取得已被授權的menu
        $where_array = array(
            'sag_group' => $sa_group
        );
        $sa_auth = $this->Common_model->get_db("sa_auth", $where_array);

        //將有權限的menu整理成array
        $auth_menu = array();
        if ($sa_auth) {
            foreach ($sa_auth as $row) {
                $auth_menu[] = $row->saa_menu_category;
            }
        }

        $valid_menu = $all_menu;

        foreach ($valid_menu as $main_key => $main_menu) {
            if (isset($main_menu["heading"])) {
                //標題列
                continue;
            }

            if (!isset($main_menu["sub_menu"])) {
                //沒有子menu，則此為menu選項
                if (!in_array($main_key, $auth_menu)) {
                    unset($valid_menu[$main_key]);
                }

                continue;
            }

            //有子menu，檢查子menu項目
            foreach ($main_menu["sub_menu"] as $sub_key => $sub_menu) {
                if (!in_array($sub_key, $auth_menu)) {
                    unset($valid_menu[$main_key]["sub_menu"][$sub_key]);
                }
            }

            //若子menu全部都被移除了，就連main menu一併刪除
            if (count($valid_menu[$main_key]["sub_menu"]) == 0) {
                unset($valid_menu[$main_key]);
            }
        }

        //排除沒有子項目的heading
        $last_heading_key = false;
        foreach ($valid_menu as $main_key => $main_menu) {
            if (!isset($main_menu["heading"])) {
                //非標題列
                $last_heading_key = false;
                continue;
            }

            //標題列
            if ($last_heading_key) {
                //有前一個標題，表示標題接標題，表示前一個標題是不需要的
                unset($valid_menu[$last_heading_key]);
                $last_heading_key = false;
                continue;
            }

            $last_heading_key = $main_key;
        }

        if ($last_heading_key) {
            //檢查結束後，仍有前一個標題，表示結尾是標題欄，要移除
            unset($valid_menu[$last_heading_key]);
        }

        return $valid_menu;
    }

    /* ==================================
     * 取得語言碼(zh / en)
     * ================================== */

    private function get_locale_code() {
        //若有傳入url參數，更改成傳入的語系
        $lang = strtolower($this->input->get('lang'));

        if (in_array($lang, array('zh'))) {
            //語系寫入session
            $this->session->set_userdata(array('i18n' => $lang));

            return $lang;
        }

        //若session有記錄語系，使用session記錄語系
        $lang = $this->session->i18n;

        if (!$lang) {
            //偵測語系
            // $http_accept_language = strtolower($this->input->server('HTTP_ACCEPT_LANGUAGE'));

            //初始值，最後回傳最小者
            // $en_index = 9999;
            // $zh_index = 9998;

            // $tmp = strpos($http_accept_language, 'zh');
            // if ($tmp !== false && $tmp < $zh_index) {
            //     $zh_index = $tmp;
            // }

            // $tmp = strpos($http_accept_language, 'en');
            // if ($tmp !== false && $tmp < $en_index) {
            //     $en_index = $tmp;
            // }

            //將語言id最小者回傳
            // $min_index = 10000;
            // if ($en_index < $min_index) {
            //     $min_index = $en_index;
            //     $lang = 'en';
            // }
            // if ($zh_index < $min_index) {
            //     $lang = 'zh';
            // }


            //指定lang
            $lang = 'zh';
            //記錄session
            $this->session->set_userdata(array('i18n' => $lang));
        }

        return $lang;
    }

    /*
     * 取得base_url路徑
     */

    private function get_base_url($url) {
        return base_url("{$this->controller_name}/{$url}");
    }

    /*
     * 驗證登入狀態
     * 進入網頁先檢查項目
     */

    private function check_login_status($page) {

        //限制IP
        $ip = $_SERVER['HTTP_HOST'];
        //鴻圖測試站server
        $ip = explode(',', $ip);
        $ip = $ip[0];

        $client_ip = get_client_ip();
        $ip_array = explode(".", $client_ip);

        // echo json_encode($ip_array);
        // exit;

        // if (!($ip_array[0] == "::1" || $ip_array[0] == "192" || $ip_array[0] == "39" || $ip_array[0] == "60")) {
        //     _alert_redirect('很抱歉，您的位置無法登入系統，請洽系統管理員。\\n' . $ip, 'http://www.nmns.edu.tw/');
        // }

        // if (!(!isset($ip_array[1]) || $ip_array[1] == "168" || $ip_array[1] == "9" || $ip_array[1] == "172" || $ip_array[1] == "244")) {
        //     _alert_redirect('很抱歉，您的位置無法登入系統，請洽系統管理員。\\n' . $ip_array[1], 'http://www.nmns.edu.tw/');
        // }

        if(!in_array(ENVIRONMENT, ['test', 'development'])){
            $allow_ip = array(
                'web_l4' => '163.29.37.138',
                'api_l4' => '163.29.37.132',
                'uat_l4' => '163.29.37.177',
                'server_uat_addr' => 'ar-test.gov.taipei',
                'server_web_addr' => 'ar-web.gov.taipei',
                'server_web_addr_temp' => 'ar-web-taipei.omniguider.com',
                'server_api_addr' => 'ar-api.gov.taipei',
                'server_api_addr_temp' => 'ar-api-taipei.omniguider.com',
                'test_server' => 'ar-taipei-test.omniguider.com',
            );
            
            if (!array_search($ip, $allow_ip)) {
                _alert_redirect('很抱歉，您的位置無法登入系統，請洽系統管理員。\\n' . $ip, 'https://login.gov.taipei/');
            }
        }

        //不須登入的網頁
        if (in_array($page, array('login','login2','login_go', 'logout' ,'goauth','3d_model','login_ad'))) {
            return;
        }

        if (!$this->login_sa) {
            header("Location: " . $this->get_base_url('login'));
            exit();
        }

        return;
    }

    /*
     * 驗證網頁存取權限
     */

    private function menu_auth($menu_category) {

        //取得後台定義的許可清單
        if ($menu_category == '') {
            //未設定menu分類
            _alert($this->lang->line("存取被拒！"));
            exit();
        }

        //menu屬於全部允許的清單，則可以存取
        if (in_array($menu_category, $this->menu_allow_all)) {
            return true;
        }

        //未登入
        if (!isset($this->login_sa->sa_group)) {
            _alert($this->lang->line("存取被拒！"));
            exit();
        }

        //取得存取權限
        if(in_array($menu_category, ['ap_pois', 'ap_floors'])){
            $where_array = array(
                'sag_group' => $this->login_sa->sa_group,
                'saa_menu_category' => 'ap_building'
            );
        }else{
            $where_array = array(
                'sag_group' => $this->login_sa->sa_group,
                'saa_menu_category' => $menu_category
            );
        }

        $sa_auth = $this->Common_model->get_db("sa_auth", $where_array);

        //沒有存取權限，失敗
        if (!$sa_auth) {
            _alert($this->lang->line("存取被拒！"));
            exit();
        }

        return true;
    }

    /*
     * 密碼加密規則
     */

    private function encrypt_password($input) {
        return sha1($this->config->item('encrypt_token') . $input);
    }

    /*
     * 登入
     */

    private function login($account, $password) {
        $encryped_password = $this->encrypt_password($password);

        $where_array = array(
            "sa_account" => $account,
            "sa_password" => $encryped_password,
            "sa_enabled" => "Y"
        );

        $sa = $this->Common_model->get_one("sa", $where_array);

        if (!$sa) {
            return false;
        }
        
        // 檢查群組
        $sa_group = $this->Common_model->get_one("sa_group", [
            "sag_group" => $sa->sa_group,
            "sag_enabled" => 'Y'
        ]);

        if (!$sa_group) {
            return false;
        }

        //更新登入時間
        $update_array = array(
            'sa_last_login_timestamp' => date('Y-m-d H:i:s')
        );

        $where_array = array(
            'sa_id' => $sa->sa_id
        );

        $this->Common_model->update_db("sa", $update_array, $where_array);

        // $this->google_checking();

        return $sa;
    }
    /*
     * Google登入
     */

    private function google_login($user_id,$email_account) {
        $where_array = array(
            "google_id" => $user_id,
            "sa_account" => $email_account,
            "sa_enabled" => "Y"
        );

        $sa = $this->Common_model->get_one("sa", $where_array);
        //如果id 與帳號 與權限 沒符合
        if (!$sa) {
        
            $where_array = array(
                "sa_account" => $email_account,
                "google_id" => $user_id
            );

            //檢查是否為新增使用者
            $sa_data = $this->Common_model->get_one("sa", $where_array);

            //有資料代表old使用者
            if($sa_data){
                echo "申請通過，請向管理員確認權限開通<br>Mail:".$where_array['adm_account'];
                exit();
                // return false;
            }else{

                $where_array = array(
                    "google_id" => $user_id,
                    "sa_account" => $email_account
                );

                $duplicated = $this->Common_model->get_one("sa", $where_array);
                if ($duplicated) {
                    echo "已申請過囉，請向管理員確認權限開通<br>Mail:".$where_array['adm_account'];
                    exit();
                }

                $insert_array = array(
                    "sa_name" => $email_account,
                    "google_id" => $user_id,
                    "sa_account" => $email_account,
                    "sa_group" => "New",
                    "sa_enabled" => "N",
                    "sa_password" => ""
                );

                $this->Common_model->insert_db("sa", $insert_array);
                    echo "申請通過，請向管理員確認權限開通<br>Mail:".$where_array['adm_account'];
                    exit();
            }


            
        }


        //更新登入時間
        $update_array = array(
            'sa_last_login_timestamp' => date('Y-m-d H:i:s')
        );

        $where_array = array(
            'sa_id' => $sa->sa_id
        );

        $this->Common_model->update_db("sa", $update_array, $where_array);

        return $sa;
    }

    /*
     * 市府單一登入
     */

    private function taoyuan_login($user_id,$email_account) {
        $where_array = array(
            "google_id" => $user_id,
            "sa_account" => $email_account,
            "sa_enabled" => "Y"
        );

        $sa = $this->Common_model->get_one("sa", $where_array);
        //如果id 與帳號 與權限 沒符合
        if (!$sa) {
        
            $where_array = array(
                "sa_account" => $email_account,
                "google_id" => $user_id
            );

            //檢查是否為新增使用者
            $sa_data = $this->Common_model->get_one("sa", $where_array);

            //有資料代表old使用者
            if($sa_data){
                echo "申請通過，請向管理員確認權限開通";
                exit();
                // return false;
            }else{

                $where_array = array(
                    "google_id" => $user_id,
                    "sa_account" => $email_account
                );

                $duplicated = $this->Common_model->get_one("sa", $where_array);
                if ($duplicated) {
                    echo "已申請過囉，請向管理員確認權限開通<br>Mail:".$where_array['adm_account'];
                    exit();
                }

                $insert_array = array(
                    "sa_name" => $email_account,
                    "google_id" => $user_id,
                    "sa_account" => $email_account,
                    "sa_group" => "New",
                    "sa_enabled" => "N",
                    "sa_password" => ""
                );

                $this->Common_model->insert_db("sa", $insert_array);
                    echo "申請通過，請向管理員確認權限開通<br>Mail:".$where_array['adm_account'];
                    exit();
            }


            
        }


        //更新登入時間
        $update_array = array(
            'sa_last_login_timestamp' => date('Y-m-d H:i:s')
        );

        $where_array = array(
            'sa_id' => $sa->sa_id
        );

        $this->Common_model->update_db("sa", $update_array, $where_array);

        return $sa;
    }
    

    private function google_checking(){
        // echo json_encode($_SESSION['access_token']['created']);
        // echo json_encode(date('Y-m-d H:i:s',$_SESSION['access_token']['created']+38));
        // echo "<br>";
        // echo json_encode(time());
        // echo json_encode(date('Y-m-d H:i:s',time()));

        // echo json_encode($_SESSION);
        // exit();
        
        if(isset($_SESSION['access_token'])){
            //Don`t need to do anything 
            if($_SESSION['access_token']['created']+3000<time()){
                unset($_SESSION['access_token']);
            }

        }else{
            $_SESSION['REDIRECT_QUERY_STRING'] = $_SERVER["REDIRECT_QUERY_STRING"];
            header("Location: " . $this->get_base_url('goauth'));
            exit();
                
        }

    }


    private function get_order_number($o_id) {
        return sprintf("{$this->config->item("order_number_prefix")}%08d", $o_id);
    }

    /**
     * Google_access_token 註冊
     * @param string $_GET['code']
     * @return array access token
     * @return ->['access_token'],['token_type'],['expires_in'],['created']
     */

    private function register_g_accesstoken($get_code){
        $redirect_url = $this->config->item('server_base_url').'sa/goauth/go';
        $client = new Google_Client();
        $service = new Google_Service_Oauth2($client);

        $client->setApplicationName("vrar360");
        $client->setAuthConfig($this->key_location);
        $client->setIncludeGrantedScopes(true);
        $client->setAccessType('offline');
        $httpClient = $client->authorize();
        $client->setScopes(array(
            'https://www.googleapis.com/auth/streetviewpublish',
            // 'https://www.googleapis.com/auth/youtube',
            'https://www.googleapis.com/auth/content',
            'https://www.googleapis.com/auth/userinfo.profile',
            'Google_Service_Oauth2::USERINFO_PROFILE',
            'Google_Service_Oauth2::USERINFO_EMAIL'
            ));
        
        $client->setRedirectUri($redirect_url);
        $token =  $client->authenticate($get_code);
        $_SESSION['access_token'] = $client->getAccessToken();
        return $_SESSION['access_token'];
    }

    /**
     * Google_access_token 更新
     * @param string access token
     * @return array access token
     * @return ->['access_token'],['token_type'],['expires_in'],['created']
     */

    private function  refresh_g_accesstoken($access_token){
        $redirect_url = $this->config->item('server_base_url').'sa/goauth/go';
        $client = new Google_Client();
        $client->setApplicationName("vrar360");
        $client->setAuthConfig($this->key_location);
        $client->setIncludeGrantedScopes(true);
        $client->setAccessType('offline');
        $client->setScopes(array(
            'https://www.googleapis.com/auth/plus.login',
            'https://www.googleapis.com/auth/streetviewpublish',
            // 'https://www.googleapis.com/auth/youtube',
            'email',
            'profile'
            ));
        $client->setRedirectUri($redirect_url);
        $response_data= $client->refreshToken($access_token);
        if ($response_data) {
            $_SESSION['access_token'] = $response_data['refresh_token'];
        }else{
            $_SESSION['access_token'] = "";
        }
        

        return $_SESSION['access_token'];
    }

    /**
     * Google_access_token 取得
     * @param string access token
     * @return array access token
     * @return ->['access_token'],['token_type'],['expires_in'],['created']
     */

    private function get_g_accesstoken($access_token){
        $redirect_url = $this->config->item('server_base_url').'sa/goauth/go';
        $client = new Google_Client();
        $client->setApplicationName("vrar360");
        $client->setAuthConfig($this->key_location);
        $client->setIncludeGrantedScopes(true);
        $client->setAccessType('offline');
        $client->setScopes(array(
            'https://www.googleapis.com/auth/streetviewpublish',
            // 'https://www.googleapis.com/auth/youtube',
            'email',
            'profile'
            ));
        // $client->addScope("email");
        // $client->addScope("profile");
        $client->setRedirectUri($redirect_url);
        $client->setAccessToken($access_token);
        $_SESSION['access_token'] = $client->getAccessToken();
        return $_SESSION['access_token'];
    }

    /**
     * Google_access_token 檢查
     * @param string access token
     * @return bool Returns True if the access_token is expired.
     */

    private function check_g_accesstoken($access_token){
        
        $response = curl_query("https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=".$access_token['access_token'],'');
        $response = json_decode($response,true);


        //超過session時間
        if(!isset($response['expires_in'])){
            //清空登入session
            $this->session->unset_userdata("login_sa");
            //清空登入變數
            $this->login_sa = null;

            unset($_SESSION['access_token']);

            //重導至登入頁
            // _alert("超過登入時間，請重新登入");
            // header("Location: " . $this->get_base_url('login'));
            // exit();
        }

    
        $this->session->set_userdata(array('expires_in' => $response['expires_in']));

        if ($response['expires_in']<300){
            $this->refresh_g_accesstoken($access_token);
        }

        return $response;
    }


    /**
     * Google_access_token 丟棄
     * @param null
     * @return null
     */
    private function log_out_g_accesstoken(){
        unset($_SESSION['access_token']);
    }

    /**
     * Google_user_data 取得
     * @param array access token
     * @return response /user/me
     */
    private function get_plus_me($access_token){
        $redirect_url = $this->config->item('server_base_url').'sa/goauth/go';
        $client = new Google_Client();
        $client->setApplicationName("vrar360");
        $client->setAuthConfig($this->key_location);
        $client->setIncludeGrantedScopes(true);
        $client->setAccessType('offline');
        $client->addScope("email");
        $client->addScope("profile");
        $client->addScope('https://www.googleapis.com/auth/streetviewpublish');
        // $client->addScope('https://www.googleapis.com/auth/youtube');
        $client->setRedirectUri($redirect_url);
        $client->setAccessToken($access_token);
        
        //Send Client Request
        $service = new Google_Service_Oauth2($client);
        $user = $service->userinfo->get(); 

        echo json_encode($user);
        exit();
    }

    /*
     * 員工愛上網權限登入
     */

    private function sso_login($userobj) {
        // $encryped_password = $this->encrypt_password($userobj->userPrincipalName);

        $where_array = array(
            "sa_account" => $userobj->userPrincipalName,
            // "sa_password" => $encryped_password,
            "sa_enabled" => "Y"
        );
        $sa = $this->Common_model->get_one("sa", $where_array);

        //檢查管理者是否存在
        if (!$sa) {
            //不存在，則不允許登入
            return false;
        }

        //更新登入時間
        $update_array = array(
            'sa_last_login_timestamp' => date('Y-m-d H:i:s')
        );
        $where_array = array(
            'sa_id' => $sa->sa_id
        );
        $this->Common_model->update_db("sa", $update_array, $where_array);

        return $sa;
    }

    private function admin_sso_login($userobj) {
        $encryped_password = $this->encrypt_password($userobj->userPrincipalName);

        $where_array = array(
            "adm_account" => $userobj->userPrincipalName,
            "adm_enabled" => "Y"
        );
        $admin = $this->Common_model->get_one("admin", $where_array);

        //檢查管理者是否存在
        if (!$admin) {
            return false;
        }

        //更新登入時間
        $update_array = array(
            'adm_last_login_timestamp' => date('Y-m-d H:i:s')
        );

        $where_array = array(
            'adm_id' => $admin->adm_id
        );
        $this->Common_model->update_db("admin", $update_array, $where_array);

        return $admin;
    }

    

    /*
     * 記錄管理者操作log
     */

    private function sa_log($table, $item_name, $act, $table_id, $act_title) {

        $insert_array = array(
            "sl_ip" => get_remote_addr(),
            'sa_id' => $this->login_sa->sa_id ? $this->login_sa->sa_id : $table_id,
            'sl_table' => $table,
            "sl_table_id" => $table_id,
            "sl_act_item" => $item_name,
            'sl_act_title' => $act_title,
            'sl_act' => $act
        );
        $this->Common_model->insert_db("sa_log", $insert_array);
    }
}
