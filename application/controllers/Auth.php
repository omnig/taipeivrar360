<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';



/*
 * Changes:
 * 1. This project contains .htaccess file for windows machine.
 *    Please update as per your requirements.
 *    Samples (Win/Linux): http://stackoverflow.com/questions/28525870/removing-index-php-from-url-in-codeigniter-on-mandriva
 *
 * 2. Change 'encryption_key' in application\config\config.php
 *    Link for encryption_key: http://jeffreybarke.net/tools/codeigniter-encryption-key-generator/
 * 
 * 3. Change 'jwt_key' in application\config\jwt.php
 *
 */

class Auth extends \Restserver\Libraries\REST_Controller
{   

    /* ===============================
     * 記錄log
     * 
     * 傳入參數：
     * $api_name: API(或存取目標)名稱
     * $result: 處理結果
     * $success: 是否成功(Y/N)
     * =============================== */

    private function log($api_name, $result, $success, $input = false) {
        if (!$input) {
            $input = array(
                "get" => $this->input->get(),
                "post" => $this->input->post()
            );
        }

        $insert_array = array(
            "la_ip" => get_remote_addr(),
            "la_api_name" => $api_name,
            "la_params" => json_encode($input),
            "la_result" => $result,
            "la_success" => $success
        );

        $this->Common_model->insert_db("log_api", $insert_array);

        if ($success == 'N') {
            //顯示呼叫結果
            $ret = array(
                "result" => "false",
                "code" => 202,
                "error_message" => $result,
                "data" => array()
            );
            http_response_code(202);
            echo json_encode($ret);

            exit();
        }
    }

    /**
     * URL: http://localhost/CodeIgniter-JWT-Sample/auth/token
     * Method: GET
     */
    public function token_get()
    {   
        $tokenData = array();
        $input_array["google_id"] = $this->input->GET('google_id');
        $input_array["google_name"] = $this->input->GET('google_name');
        $input_array["google_email"] = $this->input->GET('google_mail');
        

        foreach ($input_array as $index => $value) {
            if ($value === null || $value === '') {
                $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            }
        }

        //必須傳入帳號密碼，或是fb_id
        if (!($input_array["google_id"] && $input_array["google_email"])) {
            $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
        }

        if ($input_array["google_id"]) {
            $user = $this->google_login($input_array["google_id"], $input_array["google_name"], $input_array["google_email"], "");
        }

        
        //準備回傳結果
        $data = array(
            "u_id" => $user->u_id,
            "user_email" => $user->u_email,
            "user_name" => $user->u_name,
            "last_login" => $user->u_last_login_timestamp ? $user->u_last_login_timestamp : '',
            'last_logout' => $user->u_last_logout_timestamp ? $user->u_last_logout_timestamp : '',
            "google_ts" => $user->u_google_ts ? $user->u_google_ts : '',
            "google_id" => $user->u_google_id ? $user->u_google_id : '',
        );

        $tokenData['u_id'] = $data['u_id'];
        $tokenData['user_email'] = $data['user_email'];
        $tokenData['user_name'] = $data['user_name'];
        $tokenData['timestamp'] = time();

        $output['token'] = AUTHORIZATION::generateToken($tokenData);
        
        //更新token到DB
        $update_array = array(
            'u_login_token' => $output['token']
        );

        $where_array = array(
            'u_id' => $data['u_id']
        );

        $this->Common_model->update_db("user", $update_array, $where_array);
        
        
        $this->set_response($output, \Restserver\Libraries\REST_Controller::HTTP_OK);
    }


    /**
     * URL: http://localhost/vara360/auth/token_fb
     * URL: http://vr-api.tycg.gov.tw/auth/token_fb
     * Method: GET
     */
    public function token_fb_get()
    {   
        // $tokenData = array();
        $input_array["facebook_id"] = $this->input->GET('facebook_id');
        $input_array["facebook_name"] = $this->input->GET('facebook_name');
        $input_array["facebook_mail"] = $this->input->GET('facebook_mail');
        

        foreach ($input_array as $index => $value) {
            if ($value === null || $value === '') {
                $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
                exit;
            }
        }

        // //必須傳入帳號密碼，或是fb_id
        if (!($input_array["facebook_id"] && $input_array["facebook_mail"])) {
            $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            exit;
        }

        if ($input_array["facebook_id"]) {
            $user = $this->fb_login($input_array["facebook_id"], $input_array["facebook_name"], $input_array["facebook_mail"], "");
        }

        
        // //準備回傳結果
        $data = array(
            "u_id" => $user->u_id,
            "user_email" => $user->u_email,
            "user_name" => $user->u_name,
            "last_login" => $user->u_last_login_timestamp ? $user->u_last_login_timestamp : '',
            'last_logout' => $user->u_last_logout_timestamp ? $user->u_last_logout_timestamp : '',
            "fb_ts" => $user->u_fb_ts ? $user->u_fb_ts : '',
            "fb_id" => $user->u_fb_id ? $user->u_fb_id : '',
        );

        $tokenData['u_id'] = $data['u_id'];
        $tokenData['user_email'] = $data['user_email'];
        $tokenData['user_name'] = $data['user_name'];
        $tokenData['timestamp'] = time();

        $output['token'] = AUTHORIZATION::generateToken($tokenData);
        
        // //更新token到DB
        $update_array = array(
            'u_login_token' => $output['token']
        );

        $where_array = array(
            'u_id' => $data['u_id']
        );

        $this->Common_model->update_db("user", $update_array, $where_array);
        // $output ="你好呀";

        $this->set_response($output, \Restserver\Libraries\REST_Controller::HTTP_OK);
    }

    /**
     * URL: http://localhost/CodeIgniter-JWT-Sample/auth/token
     * Method: POST
     * Header Key: Authorization
     * Value: Auth token generated in GET call
     */
    public function token_post()
    {
        $headers = $this->input->request_headers();

        if (isset($headers['authorization'])) {
            $$headers['Authorization'] = $headers['authorization'];
        }

        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            //$decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
             $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);

            if ($decodedToken != false) {
                $this->set_response($decodedToken, \Restserver\Libraries\REST_Controller::HTTP_OK);
                return;
            }
        }

        $this->log('auth_token', '', 'Y');
        $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
    }

    private function google_login($google_id, $google_name, $google_email, $device_id) {

        //檢查是否存在
        $where_array = array(
            "u_google_id" => $google_id,
        );
        $user = $this->Common_model->get_one("user", $where_array);

        //產生登入token
        $login_token = "";

        if (!$user) {
            //檢查是否有相同device_id
            $where_array = array(
                "u_email" => $google_email
            );
            $ret = $this->Common_model->get_one('user', $where_array);
            if ($ret) {
                //更新
                $where_array = array(
                    'u_id' => $ret->u_id
                );
                $update_array = array(
                    "u_email_confirmed" => "Y",
                    "u_google_id" => $google_id,
                    "u_google_ts" => date("Y-m-d H:i:s"),
                    'u_login_token' => $login_token,
                    "u_device_id" => $device_id
                );
                $this->Common_model->update_db("user", $update_array, $where_array);
            } else {
                //註冊
                $insert_array = array(
                    "u_name" => $google_name,
                    "u_email" => $google_email,
                    "u_email_confirmed" => "Y",
                    "u_google_id" => $google_id,
                    "u_google_ts" => date("Y-m-d H:i:s"),
                    'u_login_token' => $login_token,
                    "u_device_id" => $device_id
                );
                $this->Common_model->insert_db("user", $insert_array);
                $u_id = $this->Common_model->get_insert_id();
            }
            //取得google註冊資料
            $where_array = array(
                "u_id" => $u_id
            );
            $user = $this->Common_model->get_one("user", $where_array);
        } else if ($user->u_del == "Y") {
            return false;
        } else {
            //更新登入時間及token
            $update_array = array(
                "u_email_confirmed" => "Y",
                // 'u_email' => $google_email,
                'u_login_token' => $login_token,
                'u_device_id' => $device_id,
                'u_last_login_timestamp' => date('Y-m-d H:i:s')
            );
            $where_array = array(
                "u_id" => $user->u_id
            );
            $this->Common_model->update_db("user", $update_array, $where_array);
            $user = $this->Common_model->get_one('user', $where_array);
        }


        return $user;
    }

    private function fb_login($fb_id, $fb_name, $fb_mail, $device_id) {

        //檢查是否存在
        $where_array = array(
            "u_fb_id" => $fb_id,
        );
        $user = $this->Common_model->get_one("user", $where_array);

        //產生登入token
        $login_token = "";

        if (!$user) {
            //檢查是否有相同mail
            $where_array = array(
                "u_email" => $fb_mail
            );
            $ret = $this->Common_model->get_one('user', $where_array);
            if ($ret) {
                //更新
                $where_array = array(
                    'u_id' => $ret->u_id
                );
                $update_array = array(
                    // "u_name" => $fb_name,
                    // "u_email" => $fb_email,
                    "u_email_confirmed" => "Y",
                    "u_fb_id" => $fb_id,
                    "u_fb_ts" => date("Y-m-d H:i:s"),
                    'u_login_token' => $login_token,
                    "u_device_id" => $device_id
                );
                $this->Common_model->update_db("user", $update_array, $where_array);
            } else {
                //註冊
                $insert_array = array(
                    "u_name" => $fb_name,
                    "u_email" => $fb_mail,
                    "u_email_confirmed" => "Y",
                    "u_fb_id" => $fb_id,
                    "u_fb_ts" => date("Y-m-d H:i:s"),
                    'u_login_token' => $login_token,
                    "u_device_id" => $device_id
                );
                $this->Common_model->insert_db("user", $insert_array);
                $u_id = $this->Common_model->get_insert_id();
            }
            //取得google註冊資料
            $where_array = array(
                "u_id" => $u_id
            );
            $user = $this->Common_model->get_one("user", $where_array);
        } else if ($user->u_del == "Y") {
            return false;
        } else {
            //更新登入時間及token
            $update_array = array(
                "u_email_confirmed" => "Y",
                // 'u_email' => $google_email,
                'u_login_token' => $login_token,
                'u_device_id' => $device_id,
                'u_last_login_timestamp' => date('Y-m-d H:i:s')
            );
            $where_array = array(
                "u_id" => $user->u_id
            );
            $this->Common_model->update_db("user", $update_array, $where_array);
            $user = $this->Common_model->get_one('user', $where_array);
        }


        return $user;
    }

    
    public function atw_view_get(){
        $tokenData = array();
        $input_array["login_token"] = $this->input->GET('login_token');
        //必須傳入token
        if (!($input_array["login_token"] && $input_array["login_token"])) {
            $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }

        if ($input_array["login_token"]) {

            $where_array =  array(
                'u_login_token_app' => $input_array["login_token"]
            );

            $user = $this->Common_model->get_one("user",$where_array);
        }

        // 沒有取得登入使用者資料，Token 失效
        
        if ($user) {

            $tokenData['result'] = TRUE;
            $tokenData['timestamp'] = time();
            $this->set_response($tokenData, \Restserver\Libraries\REST_Controller::HTTP_OK);

        }else{

            $tokenData['result'] = FALSE;
            $tokenData['timestamp'] = time();
            $this->set_response($tokenData, \Restserver\Libraries\REST_Controller::HTTP_OK);

        }

    }

    public function atw_view_post(){

        $input_array["login_token"] = $this->input->POST('login_token');
        // $input_array["timestamp"] = $this->input->GET('timestamp');
        // $input_array["mac"] = $this->input->GET('mac');
        // $input_array["device_id"] = $this->input->GET('device_id');

        //必須傳入token
        if (!($input_array["login_token"] && $input_array["login_token"])) {
            $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }

        if ($input_array["login_token"]) {

            $where_array =  array(
                'u_login_token_app' => $input_array["login_token"]
            );

            $user = $this->Common_model->get_one("user",$where_array);
        }

        //沒有取得登入使用者資料，登入失敗
        if (!$user) {
            // $this->log($page, "LOGIN_FAIL", 'N');
            $this->set_response("LOGIN_FAIL", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }

        //準備回傳結果
        $data = array(
            "u_id" => $user->u_id,
            "user_email" => $user->u_email,
            "user_name" => $user->u_name,
            "last_login" => $user->u_last_login_timestamp ? $user->u_last_login_timestamp : '',
            'last_logout' => $user->u_last_logout_timestamp ? $user->u_last_logout_timestamp : '',
            "google_ts" => $user->u_google_ts ? $user->u_google_ts : '',
            "google_id" => $user->u_google_id ? $user->u_google_id : '',
        );

        $tokenData['u_id'] = $data['u_id'];
        $tokenData['user_email'] = $data['user_email'];
        $tokenData['user_name'] = $data['user_name'];
        $tokenData['user_email'] = $data['user_email'];
        $tokenData['timestamp'] = time();

        $output['token'] = AUTHORIZATION::generateToken($tokenData);
        
        //更新token到DB
        // $update_array = array(
        //     'u_login_token' => $output['token']
        // );

        // $where_array = array(
        //     'u_id' => $data['u_id']
        // );

        // $this->Common_model->update_db("user", $update_array, $where_array);
        

        $this->set_response($output, \Restserver\Libraries\REST_Controller::HTTP_OK);

    }

    private function city_card_api($rfid_keyout,$birthday){

        // $rfid_keyout = "9122115314770660";
        // $birthday = "20000939";

        $data = array();

        $data['rfid_keyout'] = $rfid_keyout;
        $data['birthday'] = $birthday ;

        $url = $this->config->item('webcard_url');
        $response = curl_query($url, $http_type = 'post',$data );//他是JSON

        $response = json_decode($response,TRUE);




        if(isset($response['entity_count']) && $response['entity_count']==0){
            //代表驗證失敗
            $response['result'] = FALSE;
            return $response;
        }

        if(isset($response['exception'])){
            echo json_encode($response);
            exit;
        }

        



        //代表驗證成功
         $response['result'] = TRUE;
         $response['entity_id'] = $response['basdat']['entities'][0]['entity_id'];
         unset($response['basdat']);
        return $response; 

    }

    private function citycard_login($rfid_keyout, $entity_id, $device_id) {

        //檢查是否存在
        $where_array = array(
            "rfid_keyout" => $rfid_keyout,
        );
        $user = $this->Common_model->get_one("user", $where_array);

        //產生登入token
        $login_token = substr(md5(uniqid("utobonus", true)), 0, 40);



        if (!$user) {
            
            //註冊
            $insert_array = array(
                "u_name" => "市民卡登入",
                "u_email" => "N/A",
                "u_email_confirmed" => "Y",
                "rfid_keyout" => $rfid_keyout,
                "entity_id" => $entity_id,
                'u_login_token' => $login_token,
                "u_device_id" => $device_id
            );
            $this->Common_model->insert_db("user", $insert_array);

            $u_id = $this->Common_model->get_insert_id();

            //取得市民卡註冊資料
            $where_array = array(
                "u_id" => $u_id
            );
            $user = $this->Common_model->get_one("user", $where_array);

        } else if ($user->u_del == "Y") {
            return false;
        } else {
            //更新登入時間及token
            $update_array = array(
                'u_login_token' => $login_token,
                'u_device_id' => $device_id,
                'u_last_login_timestamp' => date('Y-m-d H:i:s')
            );
            $where_array = array(
                "u_id" => $user->u_id
            );
            $this->Common_model->update_db("user", $update_array, $where_array);
            $user = $this->Common_model->get_one('user', $where_array);
        }


        return $user;
    }


    public function webcard_get()
    {   
        $tokenData = array();
        $input_array["rfid_keyout"] = $this->input->GET('rfid_keyout');
        $input_array["birthday"] = $this->input->GET('birthday');
        
        foreach ($input_array as $index => $value) {
            if ($value === null || $value === '') {
                $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
            }
        }

        //必須傳入帳號密碼，或是fb_id
        if (!($input_array["rfid_keyout"] && $input_array["birthday"])) {
            $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
        }

        //之後開通要打開，下面判斷是要刪除
        //
        $output = $this->city_card_api($input_array["rfid_keyout"],$input_array["birthday"]);

        // if ($input_array["rfid_keyout"]=="9122115314770660"&&$input_array["birthday"]=="20000929") {
        //     $output['result']= TRUE;
        //     $output['entity_id']= 803970;
        // }else{
        //     $output['result']= FALSE;
        // }

        if ($output['result']) {
            //驗證成功
            
            $user = $this->citycard_login($input_array["rfid_keyout"], $output['entity_id'], "");

            //準備回傳結果
            $data = array(
                "u_id" => $user->u_id,
                "user_email" => $user->u_email,
                "user_name" => $user->u_name,
                "last_login" => $user->u_last_login_timestamp ? $user->u_last_login_timestamp : '',
                'last_logout' => $user->u_last_logout_timestamp ? $user->u_last_logout_timestamp : ''
            );

            $tokenData['u_id'] = $data['u_id'];
            $tokenData['user_email'] = $data['user_email'];
            $tokenData['user_name'] = $data['user_name'];
            $tokenData['timestamp'] = time();

            $output1['token'] = AUTHORIZATION::generateToken($tokenData);

            // //更新token到DB
            $update_array = array(
                'u_login_token' => $output1['token']
            );

            $where_array = array(
                'u_id' => $data['u_id']
            );

            $this->Common_model->update_db("user", $update_array, $where_array);
            

            $this->set_response($output1, \Restserver\Libraries\REST_Controller::HTTP_OK);

        }else{
            //驗證失敗
            $this->set_response("市民卡驗證失敗", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
        }



        // exit;

        
    }

    public function webcard_post()
    {
        $headers = $this->input->request_headers();

        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            //$decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
             $decodedToken = AUTHORIZATION::validateTimestamp($headers['Authorization']);

            if ($decodedToken != false) {
                $this->set_response($decodedToken, \Restserver\Libraries\REST_Controller::HTTP_OK);
                return;
            }
        }

        $this->set_response("Unauthorised", \Restserver\Libraries\REST_Controller::HTTP_UNAUTHORIZED);
    }



}