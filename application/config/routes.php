<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['cli/(:any)/(:any)'] = 'cli/view/$1/$2';
$route['cli/(:any)'] = 'cli/view/$1';
$route['cli'] = 'cli/view/home';

$route['api/(:any)/(:any)'] = 'api/view/$1/$2';
$route['api/(:any)'] = 'api/view/$1';
$route['api'] = 'api/view/index';

$route['locapi/(:any)/(:any)'] = 'locapi/view/$1/$2';
$route['locapi/(:any)'] = 'locapi/view/$1';
$route['locapi'] = 'locapi/view/index';

$route['anyplace_api/mapping/(:any)/(:any)'] = 'anyplace_api/mapping/$1/$2';
$route['anyplace_api/navigation/(:any)/(:any)'] = 'anyplace_api/navigation/$1/$2';
// $route['anyplace_api/(:any)/(:any)'] = 'anyplace_api/view/$1/$2';
// $route['anyplace_api/(:any)'] = 'anyplace_api/view/$1';
// $route['anyplace_api'] = 'anyplace_api/view/index';

$route['admin/(:any)/(:any)'] = 'admin/view/$1/$2';
$route['admin/(:any)'] = 'admin/view/$1';
$route['admin'] = 'admin/view/home';

$route['sa/(:any)/(:any)'] = 'sa/view/$1/$2';
$route['sa/(:any)'] = 'sa/view/$1';
$route['sa'] = 'sa/view/home';

$route['upload/(:any)/(:any)'] = 'upload/read/$1/$2';
$route['upload/(:any)'] = 'upload/read/$1';


$route['upload_image/(:any)'] = 'upload_image/read/$1';
$route['upload_image/(:any)/(:any)'] = 'upload_image/read/$1/$2';


$route['upload_video/(:any)/(:any)'] = 'upload_video/read/$1/$2';
$route['upload_video/(:any)'] = 'upload_video/read/$1';

$route['upload_model/(:any)'] = 'upload_model/read/$1';
$route['upload_model/(:any)/(:any)'] = 'upload_model/read/$1/$2';

$route['upload_wtc/(:any)'] = 'upload_wtc/read/$1';
$route['upload_wtc/(:any)/(:any)'] = 'upload_wtc/read/$1/$2';

$route['embed/(:any)/(:any)'] = 'front/embed/$1/$2';
$route['embed/(:any)'] = 'front/embed/$1';

$route['migrate'] = 'migrate';

$route['robots.txt'] = 'front/view/robots';
$route['sitemap.xml'] = 'front/view/sitemap';

$route['auth/(:any)'] = 'auth/$1'; // JWT
$route['auth/(:any)/(:any)'] = 'auth/$1/$2'; // JWT



//Post API
//user_like_topic
$route['api_client/post/user_like_topic/(:num)'] = 'api_client/post/user_like_topic/id/$1'; 
$route['api_client/post/user_like_topic/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api_client/post/user_like_topic/id/$1/format/$3$4';

//user_like_vrphoto
$route['api_client/post/user_like_vrphoto/(:num)'] = 'api_client/post/user_like_vrphoto/id/$1'; 
$route['api_client/post/user_like_vrphoto/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api_client/post/user_like_vrphoto/id/$1/format/$3$4';

//user_like_vrvideo
$route['api_client/post/user_like_vrvideo/(:num)'] = 'api_client/post/user_like_vrvideo/id/$1'; 
$route['api_client/post/user_like_vrvideo/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api_client/post/user_like_vrvideo/id/$1/format/$3$4';

//user_like_document
$route['api_client/post/user_like_document/(:num)'] = 'api_client/post/user_like_document/id/$1'; 
$route['api_client/post/user_like_document/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api_client/post/user_like_document/id/$1/format/$3$4';


//document info
$route['api_client/document/list/'] = 'api_client/document/list/'; 
$route['api_client/document/info/(:num)'] = 'api_client/document/info/id/$1'; 
$route['api_client/document/edit/(:num)'] = 'api_client/document/edit/id/$1'; 
$route['api_client/document/del/(:num)'] = 'api_client/document/del/id/$1'; 
$route['api_client/document/info/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api_client/document/info/id/$1/format/$3$4';


//proprietary info
$route['api_client/proprietary/list/'] = 'api_client/proprietary/list/'; 
$route['api_client/proprietary/info/(:num)'] = 'api_client/proprietary/info/id/$1'; 
$route['api_client/proprietary/info/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api_client/proprietary/info/id/$1/format/$3$4';

//lesson info
$route['api_client/lesson/list/'] = 'api_client/lesson/list/'; 
$route['api_client/lesson/info/(:num)'] = 'api_client/lesson/info/id/$1'; 
$route['api_client/lesson/info/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api_client/lesson/info/id/$1/format/$3$4';

//rank info
$route['api_client/rank/list/'] = 'api_client/rank/list/'; 
$route['api_client/rank/user_vr/(:num)'] = 'api_client/rank/user_vr/id/$1'; 



//contact
$route['api_client/post/contact/'] = 'api_client/post/contact/'; 

//Photo upload API
$route['api_client/photo/post/'] = 'api_client/photo/post/'; 
$route['api_client/photo/del/(:num)'] = 'api_client/photo/del/id/$1'; 
$route['api_client/photo/update/(:num)'] = 'api_client/photo/update/id/$1'; 

//Video upload API
$route['api_client/video/post/'] = 'api_client/video/post/';
$route['api_client/video/del/(:num)'] = 'api_client/video/del/id/$1'; 
$route['api_client/video/update/(:num)'] = 'api_client/video/update/id/$1'; 

//AR upload API
$route['api_client/ar/post/'] = 'api_client/ar/post/';
$route['api_client/ar/del/(:num)'] = 'api_client/ar/del/id/$1';
$route['api_client/ar/update/(:num)'] = 'api_client/ar/update/id/$1'; 


//GET API 
$route['api_client/post/user_topic_vr/(:num)'] = 'api_client/post/user_topic_vr/id/$1'; 
$route['api_client/post/user_index_vr/'] = 'api_client/post/user_topic_vr/'; 
$route['api_client/usr/my_photo/'] = 'api_client/user/my_photo/'; 
$route['api_client/usr/my_photo/'] = 'api_client/user/my_video/'; 
$route['api_client/usr/my_ar/'] = 'api_client/user/my_ar/'; 
$route['api_client/usr/my_upload/'] = 'api_client/user/my_upload/';
$route['api_client/usr/my_name/'] = 'api_client/user/my_name/'; 


$route['(:any)/(:any)'] = 'front/view/$1/$2';
$route['(:any)'] = 'front/view/$1';





$route['default_controller'] = 'sa/view';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
