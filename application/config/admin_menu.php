<?php

/* ====================================
 * 定義所有的menu list
 * ==================================== */
$config['admin_menu_list'] = array(
    "home" => array(
        "title" => "首頁",
        "icon" => "icon-home"
    ),
    "topic_manage" => array(
        "title" => "主題管理",
        "icon" => "fa fa-map-marker",
        "sub_menu" => array(
            'topic_group' => array(
                'title' => '主題群組管理',
                'icon' => ''
            ),
            'topic_manage' => array(
                'title' => '主題管理',
                'icon' => ''
            ),
        )
    ),
    "pois" => array(
        "title" => "POI管理",
        "icon" => "icon-list",
        "sub_menu" => array(
            "ap_building" => array(
                "title" => "建物管理(BETA)",
                "icon" => ""
            )
        )
    ),
    "map" => array(
        "title" => "圖資管理",
        "icon" => "fa fa-map-marker",
        "sub_menu" => array(
            "geofence" => array(
                "title" => "LBS管理",
                "icon" => ""
            )
        )
    ),
    "ar" => array(
        "title" => "AR管理",
        "icon" => "fa fa-puzzle-piece",
        "sub_menu" => array(
            "department_ar" => array(
                "title" => "AR上傳",
                "icon" => "icon-social-dropbox"
            ),
            "department_ar_image" => array(
                "title" => "辨識圖上傳",
                "icon" => "icon-social-dropbox"
            )
        )
    ),
    'mission' => array(
        'title' => '任務管理',
        'icon' => 'fa fa-th',
        'sub_menu' => array(
            'mission' => array(
                'title' => '任務管理',
                'icon' => 'icon-layers'
            ),
            'reward_group' => array(
                'title' => '獎勵管理',
                'icon' => 'icon-star'
            )
        )
    ),
    'admin' => array(
        'title' => '局處管理',
        'icon' => 'fa fa-th',
        'sub_menu' => array(
            'admin' => array(
                'title' => '帳號管理',
                'icon' => ''
            )
        )
    )
);

/* ====================================
 * 在menu_allow_all中的頁面，是不論任何權限等級都可以進入的頁面
 * 不限制是否為menu項目
 * ==================================== */
$config['admin_menu_allow_all'] = array(
    "home", "login", "logout", "profile", "uploader"
);
