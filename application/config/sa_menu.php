<?php

/* ====================================
 * 定義所有的menu list
 * ==================================== */
$config['menu_list'] = array(
    "home" => array(
        "title" => "首頁",
        "icon" => "icon-home"
    ),
    "topic_manage" => array(
        "title" => "主題管理",
        "icon" => "fa fa-map-marker",
        "sub_menu" => array(
            'topic_category' => array(
                'title' => '主題類別管理',
                'icon' => ''
            ),
            'topic_group' => array(
                'title' => '主題群組管理',
                'icon' => ''
            ),
            'topic_manage' => array(
                'title' => '主題管理',
                'icon' => ''
            ),
        )
    ),
    "pois" => array(
        "title" => "POI管理",
        "icon" => "icon-list",
        "sub_menu" => array(
            "ap_pois" => array(
                "title" => "景點POI",
                "icon" => ""
            ),
            "ap_building" => array(
                "title" => "建物管理(BETA)",
                "icon" => ""
            )
        )
    ),
    'map' => array(
        'title' => '圖資管理',
        'icon' => 'icon-map',
        "sub_menu" => array(
            'geofence' => array(
                'title' => 'LBS管理',
                'icon' => ''
            ),
            'radar' => array(
                'title' => '雷達管理',
                'icon' => ''
            ),
            "ap_category" => array(
                'title' => 'POI分類管理',
                'icon' => ''
            ),
        )
    ),
    'beacon' => array(
        'title' => 'Beacon管理',
        'icon' => 'fa fa-wifi'
    ),
    'beacon' => array(
        'title' => 'Beacon管理',
        'icon' => 'fa fa-wifi'
    ),
    'optical_label' => array(
        'title' => '光標籤管理',
        'icon' => 'fa fa-tags'
    ),
    "og_ar" => array(
        "title" => "鴻圖WTC管理",
        "icon" => "icon-social-dropbox"
    ),
    "ar" => array(
        "title" => "AR管理",
        "icon" => "fa fa-puzzle-piece",
        "sub_menu" => array(
            "department_ar" => array(
                "title" => "AR上傳",
                "icon" => ""
            ),
            "department_ar_image" => array(
                "title" => "辨識圖上傳",
                "icon" => ""
            )
        )
    ),
    'mission' => array(
        'title' => '任務管理',
        'icon' => 'fa fa-th',
        'sub_menu' => array(
            'mission' => array(
                'title' => '任務管理',
                'icon' => 'icon-layers'
            ),
            'reward_group' => array(
                'title' => '獎勵管理',
                'icon' => 'icon-star'
            ),
            'reward' => array(
                'title' => '獎勵核銷',
                'icon' => 'icon-star'
            )
        )
    ),
    "系統設置" => array(
        'heading' => true
    ),
    "sa" => array(
        "title" => "權限設定",
        "icon" => "icon-key",
        "sub_menu" => array(
            "sa_group" => array(
                "title" => "用戶群組",
                "icon" => "icon-list"
            ),
            "sa" => array(
                "title" => "系統管理員管理",
                "icon" => "icon-user"
            )
        )
    ),
    "admin" => array(
        "title" => "局處管理",
        "icon" => "icon-key",
        "sub_menu" => array(
            "admin_group" => array(
                "title" => "局處權限",
                "icon" => "icon-list"
            ),
            "admin" => array(
                "title" => "局處帳號管理",
                "icon" => "icon-user"
            )
        )
    ),
    // "backup_manage" => array(
    //     "title" => "備份檔案管理",
    //     "icon" => "fa fa-magic"
    // ),
    "config" => array(
        "title" => "系統參數",
        "icon" => "fa fa-gear"
    ),
    "mysql_dump" => array(
        "title" => "資料庫備份",
        "icon" => "fa fa-database"
    ),
    "api_console" => array(
        "title" => "API清單",
        "icon" => "fa fa-list",
        'sub_menu' => array(
            'api_console' => array(
                'title' => '資料API',
                'icon' => ''
            ),
            'locapi_console' => array(
                'title' => '地圖API',
                'icon' => ''
            )
        )
    )
);

/* ====================================
 * 在menu_allow_all中的頁面，是不論任何權限等級都可以進入的頁面
 * 不限制是否為menu項目，但都必須是已登入使用者
 * ==================================== */
$config['menu_allow_all'] = array(
    "home", "login", "logout", "profile", "uploader"
);
