<?php

//Apple 登入參數

switch (ENVIRONMENT) {
    case "development" :
    case "docker-dev" :
        //測試伺服器
        break;
    case "test" :
        //測試伺服器
        break;
    case "production" :
        //正式伺服器
        break;
}

$config['apple'] = array(
    "team_id" => "9V5M2X2S8X",
    "key_id" => "3K98U24Z69",
    "client_id" => "com.taoyuan.vrar", //app bundle id
    "iss" => "https://appleid.apple.com",
    "auth_keys_url" => "https://appleid.apple.com/auth/keys",
    "auth_token_url" => "https://appleid.apple.com/auth/token",
    "private_key" => <<<EOD
-----BEGIN PRIVATE KEY-----
MIGTAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBHkwdwIBAQQgcXZg/LcdkZ6OGrMw
19KHX3y9Uc5Trpr4JcZbc97t4sugCgYIKoZIzj0DAQehRANCAARF001+WH0YK3G7
NkmOIhMekNhf4J7qHbBS8fBxOTRdTbKaL4s5VSpcaaE7FNewQRlm+OPxW3fHa9tt
NEdvtCYY
-----END PRIVATE KEY-----
EOD
);
