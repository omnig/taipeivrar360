<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Map_lib {

    protected $ci;

    public function __construct() {
        $this->ci = & get_instance();
    }

    /* ==================================
     * 產生靜態地圖的url
     * ================================== */

    public function get_static_map($lat, $lon, $zoom = 18, $width = 640, $height = 480) {
        if ($this->ci->site_config->c_map_type == 'OSM') {
            return "http://staticmap.openstreetmap.de/staticmap.php?center={$lat},{$lon}&markers={$lat},{$lon}&zoom={$zoom}&size={$width}x{$height}&maptype=mapnik";
        }

        return "https://maps.googleapis.com/maps/api/staticmap?center={$lat},{$lon}&zoom={$zoom}&size={$width}x{$height}&maptype=roadmap&markers=color:blue%7Clabel:S%7C{$lat},{$lon}";
    }

}
