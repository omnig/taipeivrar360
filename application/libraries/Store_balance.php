<?php

/*
 * 店家帳務紀錄
 * 執行時間點：
 * 1. 訂單付款-加減帳：此時balance_timestamp為空值
 *      前台ATM, 信用卡付款 callback
 * 2. 主訂單出貨：此訂單balance_timestamp為空值者，定義計帳時間為此時間點
 *      前台-取消訂單，2種後台-設定出貨
 * 3. 退貨-減帳：記錄退貨時間點
 *      前台：取消訂單，店家後台-同意退貨
 * 4. 退貨至全部退光-退運費：減帳
 *      前台：取消訂單，店家後台-同意退貨
 * 5. 取消已成立之退貨-加帳：記錄取消時間點，同時如果取消退貨後應加回運費，則附帶處理加回運費-加帳
 *      管理後台：中止退貨
 * 6. 後台操作-編輯店家運費、後台退貨、後台取消退貨
 * 
 * 另，系統初建立時，全訂單掃描一次(cli/store_balance_parser)，建立初始資料
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Store_balance {

    protected $ci;

    public function __construct() {
        $this->ci = & get_instance();

        $this->ci->load->model("Store_balance_model");
    }

    /* ===============================
     * 取得前一筆帳籍的balance
     * =============================== */

    private function get_last_balance($s_id) {
        //取得最近一筆紀錄
        $where_array = array(
            "s_id" => $s_id,
            "sb_balance_timestamp IS NOT NULL"
        );
        $store_balance = $this->ci->Common_model->get_one("store_balance", $where_array, "sb_balance_timestamp,sb_id", "DESC");

        return $store_balance;
    }

    /* ===============================
     * 更新balance
     * 從傳入的$sb_balance_timestamp時間開始，向後更新sb_balance
     * =============================== */

    private function update_balance($s_id, $sb_balance_timestamp = NULL) {
        //取得$sb_balance_timestamp的前一筆balance
        $balance = 0;

        if ($sb_balance_timestamp) {
            //取得$sb_balance_timestamp以後的store_balance
            $where_array = array(
                "s_id" => $s_id,
                "sb_balance_timestamp IS NOT NULL",
                "sb_balance_timestamp <" => $sb_balance_timestamp
            );

            $last_store_balance = $this->ci->Common_model->get_one("store_balance", $where_array, "sb_balance_timestamp,sb_id", $order = 'DESC');

            if ($last_store_balance) {
                $balance = $last_store_balance->sb_balance;
            }
        }

        //取得$sb_balance_timestamp以後的store_balance
        $where_array = array(
            "s_id" => $s_id,
            "sb_balance_timestamp IS NOT NULL"
        );

        if ($sb_balance_timestamp) {
            $where_array["sb_balance_timestamp >="] = $sb_balance_timestamp;
        }

        $store_balance = $this->ci->Common_model->get_db("store_balance", $where_array, "sb_balance_timestamp,sb_id", 'ASC');

        if (!$store_balance) {
            return;
        }

        foreach ($store_balance as $row) {
            $balance = $balance + $row->sb_increase - $row->sb_decrease;

            if ($balance != $row->sb_balance) {
                $update_array = array(
                    "sb_balance" => $balance
                );
                $where_array = array(
                    "sb_id" => $row->sb_id
                );

                $this->ci->Common_model->update_db("store_balance", $update_array, $where_array);
            }
        }
    }

    /* ===============================
     * 更新process_fee
     * 以傳入的os_id，處理此os_id相關的、所有的process_fee
     * =============================== */

    public function update_process_fee($os_id) {
        //取得店家訂單，確認訂購當時的平台使用費率
        $where_array = array(
            "os_id" => $os_id
        );

        $order_store = $this->ci->Common_model->get_one("order_store", $where_array);

        if (!$order_store) {
            return false;
        }

        //取得此os_id相關、已定義計帳日期的所有交易紀錄
        $where_array = array(
            "os_id" => $os_id,
            "sb_balance_timestamp IS NOT NULL"
        );

        $store_balance = $this->ci->Common_model->get_db("store_balance", $where_array, "sb_balance_timestamp,sb_id", $order = 'ASC');

        if (!$store_balance) {
            return false;
        }

        $process_fee_rate = $order_store->os_process_fee_rate;  //平台使用費率
        $min_process_fee = $order_store->os_min_process_fee;   //最低平台使用費
        $current_process_fee = 0;   //依交易發生時間，逐筆交易計算處理費
        $current_order_price = 0;         //依交易發生時間，逐筆交易計算當時訂單總金額
        foreach ($store_balance as $row) {
            //本次交易金額
            $this_transation = (int) $row->sb_increase - (int) $row->sb_decrease;

            //訂單總價
            $total_order_price = $current_order_price + $this_transation;
            
            //計算應收平台使用費總額
            if ($total_order_price > 0) {
                $total_process_fee = round($total_order_price * $process_fee_rate, 0, PHP_ROUND_HALF_UP);

                //應收平台使用費若不為負值，且低於最低平台使用費，則應以最低平台使用費收取
                if ($total_process_fee < $min_process_fee) {
                    $total_process_fee = $min_process_fee;
                }
            } else {
                //訂單總價 <=0 ，不收平台使用費
                $total_process_fee = 0;
            }

            //本次平台使用費 = 應收平台使用費 - 先前的平台使用費
            $this_process_fee = $total_process_fee - $current_process_fee;

            //若此數字與現有資料不符，則更新此紀錄
            if ($this_process_fee != $row->sb_process_fee) {
                $update_array = array(
                    "sb_process_fee" => $this_process_fee
                );
                $where_array = array(
                    "sb_id" => $row->sb_id
                );

                $this->ci->Common_model->update_db("store_balance", $update_array, $where_array);
            }

            //累加目前收取的process_fee總額
            $current_process_fee += $this_process_fee;
            
            //更新目前訂單總價
            $current_order_price = $total_order_price;
        }
    }

    /* ===============================
     * 新增紀錄
     * $amount: 金額，為正值時為加帳，為負值時為減帳
     * $sb_balance_timestamp: 計帳時間點，以此時間計算結帳週期
     * $related_id: array，可包含
     *          o_id: 訂單id
     *          os_id: 店家訂單id
     *          oi_id: 訂單商品id
     * $type: TRANSATION, PAY_STORE, ADMIN
     * =============================== */

    public function insert_record($s_id, $title, $description, $amount, $sb_balance_timestamp = NULL, $related_id = array(), $type = 'TRANSATION') {
        //資料庫transation起始
        $this->ci->Common_model->trans_start();

        //前次結餘
        if ($sb_balance_timestamp) {
            $last_balance = $this->get_last_balance($s_id);
            $last_balance_amount = ($last_balance) ? $last_balance->sb_balance : 0;
            $balance = $amount + $last_balance_amount;
        } else {
            $balance = NULL;
        }

        if ($amount > 0) {
            $increase = $amount;
            $decrease = NULL;
        } elseif ($amount < 0) {
            $increase = NULL;
            $decrease = $amount * -1;
        } else {
            $increase = NULL;
            $decrease = NULL;
        }

        $insert_array = array(
            "s_id" => $s_id,
            "sb_title" => $title,
            "sb_description" => $description,
            "sb_increase" => $increase,
            "sb_decrease" => $decrease,
            "sb_balance" => $balance,
            "sb_type" => $type,
            "sb_balance_timestamp" => $sb_balance_timestamp
        );

        //記錄相關ID
        foreach ($related_id as $id => $row) {
            $insert_array[$id] = $row;
        }

        $this->ci->Common_model->insert_db("store_balance", $insert_array);

        //執行寫入
        $this->ci->Common_model->trans_complete();

        if ($sb_balance_timestamp) {
            //插入了一筆先前就發生的帳務，要更新balance，更新範圍為從此筆記錄時間以後的全部更新
            if ($sb_balance_timestamp && $last_balance && strtotime($sb_balance_timestamp) < strtotime($last_balance->sb_balance_timestamp)) {
                $this->update_balance($s_id, $sb_balance_timestamp);
            }

            //更新平台處理費
            if (isset($related_id["os_id"])) {
                $this->update_process_fee($related_id["os_id"]);
            }
        }
    }

    /* ===============================
     * 確認出帳訂單
     * 將出帳日期為NULL之資料，明確定義出帳時間
     * =============================== */

    public function commit_balance_timestamp($o_id, $sb_balance_timestamp = NULL) {
        //取得訂單
        $where_array = array(
            "o_id" => $o_id
        );

        $order_store = $this->ci->Common_model->get_db("order_store", $where_array);

        if (!$order_store) {
            return false;
        }

        foreach ($order_store as $row) {
            $this->commit_store_balance_timestamp($row->os_id, $sb_balance_timestamp);
        }
    }

    /* ===============================
     * 確認店家訂單出帳
     * 將出帳日期為NULL之資料，明確定義出帳時間
     * =============================== */

    public function commit_store_balance_timestamp($os_id, $sb_balance_timestamp = NULL) {
        //取得店家訂單
        $where_array = array(
            "os_id" => $os_id
        );

        $order_store = $this->ci->Common_model->get_one("order_store", $where_array);

        if (!$order_store) {
            return false;
        }

        //更新計帳時間
        $update_array = array(
            "sb_balance_timestamp" => ($sb_balance_timestamp) ? $sb_balance_timestamp : $order_store->os_delivery_timestamp
        );

        $where_array = array(
            "o_id" => $order_store->o_id,
            "s_id" => $order_store->s_id,
            "sb_balance_timestamp IS NULL"
        );

        $this->ci->Common_model->update_db("store_balance", $update_array, $where_array);

        //更新店家balance
        $this->update_balance($order_store->s_id, $sb_balance_timestamp);

        //更新平台處理費
        $this->update_process_fee($os_id);
    }

    /* ===============================
     * 新「付款」訂單
     * =============================== */

    public function new_paid_order($o_id) {
        //取得訂單
        $where_array = array(
            "o_id" => $o_id,
            "o_status_paid" => 'Y'
        );

        $order = $this->ci->Common_model->get_one("order", $where_array);

        if (!$order) {
            return false;
        }

        //取得店家訂單
        $where_array = array(
            "o_id" => $o_id
        );

        $order_store = $this->ci->Common_model->get_db("order_store", $where_array);

        if (!$order_store) {
            return false;
        }

        foreach ($order_store as $row) {
            //每個店家都要新增一筆收入紀錄
            $amount = $row->os_item_amount + $row->os_shipment_origin;

            $title = "出貨：{$order->o_order_number}";
            $description = "子訂單：{$order->o_order_number}-{$row->s_id}";

            $sb_balance_timestamp = $row->os_delivery_timestamp;    //以出貨時間作為出帳時間

            $related_id = array(
                "o_id" => $row->o_id,
                "os_id" => $row->os_id
            );
            $this->insert_record($row->s_id, $title, $description, $amount, $sb_balance_timestamp, $related_id);
        }
    }

    /* ===============================
     * 退貨
     * $balance_time: 計帳時間，若未傳入，則使用退貨商品建立時間
     * =============================== */

    public function new_refund_item($oi_id, $balance_time = NULL) {
        //取得訂單商品
        $where_array = array(
            "oi_id" => $oi_id
        );

        $order_item = $this->ci->Common_model->get_one("order_item", $where_array);

        if (!$order_item) {
            return false;
        }

        //取得訂單
        $where_array = array(
            "o_id" => $order_item->o_id,
            "o_status_paid" => 'Y'
        );

        $order = $this->ci->Common_model->get_one("order", $where_array);

        if (!$order) {
            return false;
        }

        //取得店家訂單
        $where_array = array(
            "o_id" => $order_item->o_id,
            "s_id" => $order_item->s_id
        );

        $order_store = $this->ci->Common_model->get_one("order_store", $where_array);

        if (!$order_store) {
            return false;
        }

        //新增一筆支出紀錄
        //減額：退貨商品金額 * 數量
        $amount = $order_item->oi_price * $order_item->oi_qty;

        $title = "退貨：{$order->o_order_number}";
        $description = "子訂單：{$order->o_order_number}-{$order_item->s_id}";

        //計帳時間，如未傳入則使用目前時間
        $sb_balance_timestamp = ($balance_time) ? $balance_time : $order_item->oi_timestamp;

        $related_id = array(
            "o_id" => $order_item->o_id,
            "os_id" => $order_store->os_id,
            "oi_id" => $oi_id
        );

        $this->insert_record($order_store->s_id, $title, $description, $amount, $sb_balance_timestamp, $related_id);
    }

    /* ===============================
     * 取消退貨
     * $qty: 取消退貨數量
     * $balance_time: 計帳時間，若未傳入，則使用目前時間
     * =============================== */

    public function new_cancel_refund_item($oi_id, $qty, $balance_time = NULL) {
        //取得訂單商品
        $where_array = array(
            "oi_id" => $oi_id
        );

        $order_item = $this->ci->Common_model->get_one("order_item", $where_array);

        if (!$order_item) {
            return false;
        }

        //取得訂單
        $where_array = array(
            "o_id" => $order_item->o_id,
            "o_status_paid" => 'Y'
        );

        $order = $this->ci->Common_model->get_one("order", $where_array);

        if (!$order) {
            return false;
        }

        //取得店家訂單
        $where_array = array(
            "o_id" => $order_item->o_id,
            "s_id" => $order_item->s_id
        );

        $order_store = $this->ci->Common_model->get_one("order_store", $where_array);

        if (!$order_store) {
            return false;
        }

        //新增一筆收入紀錄
        //增額：商品金額 * 數量
        $amount = abs($order_item->oi_price * $qty);

        $title = "取消退貨：{$order->o_order_number}";
        $description = "子訂單：{$order->o_order_number}-{$order_item->s_id}";

        //計帳時間，如未傳入則使用目前時間
        $sb_balance_timestamp = ($balance_time) ? $balance_time : date("Y-m-d H:i:s");

        $related_id = array(
            "o_id" => $order_item->o_id,
            "os_id" => $order_store->os_id
        );

        $this->insert_record($order_store->s_id, $title, $description, $amount, $sb_balance_timestamp, $related_id);

        //運費=0且原始運費不為0，則要加回運費
        if ($order_store->os_shipment == 0 && $order_store->os_shipment_origin > 0) {
            //新增一筆收入紀錄
            //增額：原始運費
            $amount = $order_store->os_shipment_origin;

            $title = "取消退貨，運費重新計入：{$order->o_order_number}";
            $description = "子訂單：{$order->o_order_number}-{$order_item->s_id}";

            //計帳時間，如未傳入則使用目前時間
            $sb_balance_timestamp = ($balance_time) ? $balance_time : date("Y-m-d H:i:s");

            $related_id = array(
                "o_id" => $order_item->o_id,
                "os_id" => $order_store->os_id
            );

            $this->insert_record($order_store->s_id, $title, $description, $amount, $sb_balance_timestamp, $related_id);
        }
    }

    /* ===============================
     * 退運費 - 減帳
     * $balance_time: 計帳時間，若未傳入，則使用目前時間
     * =============================== */

    public function cancel_shipment($os_id, $balance_time = NULL) {
        //取得店家訂單
        $where_array = array(
            "os_id" => $os_id
        );

        $order_store = $this->ci->Common_model->get_one("order_store", $where_array);

        if (!$order_store) {
            return false;
        }

        //取得訂單
        $where_array = array(
            "o_id" => $order_store->o_id,
            "o_status_paid" => 'Y'
        );

        $order = $this->ci->Common_model->get_one("order", $where_array);

        if (!$order) {
            return false;
        }

        //若沒有傳入計帳時間，則讀取退貨商品的最後一筆
        if (!$balance_time) {
            $where_array = array(
                "o_id" => $order_store->o_id,
                "s_id" => $order_store->s_id,
                "oi_qty <" => 0
            );

            $last_refund_order_item = $this->ci->Common_model->get_one("order_item", $where_array, "oi_timestamp", "DESC");

            if (!$last_refund_order_item) {
                return false;
            }

            $balance_time = $last_refund_order_item->oi_timestamp;
        }

        $amount = $order_store->os_shipment - $order_store->os_shipment_origin;

        if ($amount != 0) {
            $title = "退運費：{$order->o_order_number}";
            $description = "子訂單：{$order->o_order_number}-{$order_store->s_id}";

            $related_id = array(
                "o_id" => $order_store->o_id,
                "os_id" => $order_store->os_id
            );

            $this->insert_record($order_store->s_id, $title, $description, $amount, $balance_time, $related_id);

            $this->update_balance($order_store->s_id, $balance_time);
        }
    }

    /* ===============================
     * 以傳入的日期，計算計帳週期
     * 
     * 傳入出帳日期，由此日期往前推算前一段關帳週期
     * $date: 檢查日期
     * 
     * 例：$date 傳入 2016-08-17 ，則計帳週期應為 2016-08-01 ~ 2016-08-16
     * =============================== */

    public function get_close_duration($date) {
        //拆解傳入日期
        $time = strtotime($date);
        $year = date("Y", $time);
        $month = date("m", $time);
        $day = date("d", $time);

        //確認週期
        $balance_days_of_month = $this->ci->config->item("balance_days_of_month");

        $duration = false;    //計算取得的週期
        foreach ($balance_days_of_month as $id => $row) {
            if ((int) $day >= $row["since"] && ((int) $day < $row["until"] || $id == count($balance_days_of_month) - 1)) {
                if ($id > 0) {
                    $duration = $balance_days_of_month[$id - 1];
                } else {
                    $duration = $balance_days_of_month[count($balance_days_of_month) - 1];
                }

                break;
            }
        }

        if (!$duration) {
            return false;
        }

        //計算起、迄日期
        if ($duration["since"] < $duration["until"]) {
            //同月份
            $since = sprintf("%04d-%02d-%02d", $year, $month, $duration["since"]);
            $until = sprintf("%04d-%02d-%02d", $year, $month, $duration["until"]);
        } else {
            $until = sprintf("%04d-%02d-%02d", $year, $month, $duration["until"]);

            //since為前月份
            $month--;
            if ($month == 0) {
                $month = 12;
                $year--;
            }

            $since = sprintf("%04d-%02d-%02d", $year, $month, $duration["since"]);
        }
        return array(
            "since" => $since,
            "until" => $until
        );
    }

    /* ===============================
     * 觸發付款給店家的動作
     * 
     * 傳入出帳日期，由此日期往前推算前一段關帳週期
     * $date: 檢查日期
     * 
     * 例：$date 傳入 2016-08-17 ，則計帳週期應為 2016-08-01 ~ 2016-08-16
     * 
     * 先檢查此期間內不可存在sb_type=PAY_STORE的紀錄
     * =============================== */

    public function init_pay_store_record($date) {
        //取得計帳週期
        $duration = $this->get_close_duration($date);

        //確認計帳週期是否已完成關帳
        $where_array = array(
            "sb_type" => "PAY_STORE",
            "sb_balance_timestamp >=" => $duration["since"],
            "sb_balance_timestamp <" => $duration["until"]
        );

        $store_balance = $this->ci->Common_model->get_one("store_balance", $where_array);

        //關帳紀錄存在，跳出
        if ($store_balance) {
            return false;
        }

        $this->insert_pay_store_record($duration["since"], $duration["until"]);
    }

    /* ===============================
     * 插入出帳給店家的紀錄
     * 
     * 應付帳款：計帳時間為 $until 的前1秒，若結餘為負數，則無此紀錄
     * 寫入時，sb_type應設定為 PAY_STORE
     * 
     * 應先檢查此期間內不可存在sb_type=PAY_STORE的紀錄，否則會產生重複紀錄 
     *  
     * 傳入出帳週期
     * $since: 開始日期(含)
     * $until: 結束日期(不含)
     * =============================== */

    private function insert_pay_store_record($since, $until) {
        //取得帳務紀錄
        $where_array = array(
        );

        $store_balance = $this->ci->Store_balance_model->get_close_balance($since, $until, $where_array);

        //每個店家寫入關帳紀錄
        foreach ($store_balance as $row) {
            //結餘小於0者，新增加減帳皆為0的紀錄
            //新增關帳紀錄
            $amount = ($row->sb_balance > 0) ? -1 * $row->sb_balance : 0;

            $title = "--系統關帳--";
            $description = "計帳週期：{$since} ~ {$until}";

            $sb_balance_timestamp = date("Y-m-d H:i:s", strtotime($until) - 1);    //以until時間前1秒為計帳時間

            $related_id = array();
            $this->insert_record($row->s_id, $title, $description, $amount, $sb_balance_timestamp, $related_id, "PAY_STORE");
        }
    }
}
