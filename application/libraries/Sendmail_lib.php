<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sendmail_lib {

    protected $ci;
    private $sender_email;
    private $sender_name;
    private $receiver_email;
    private $receiver_name;
    private $cc_email;
    private $cc_name;
    private $mail_subject;
    private $mail_body;
    private $PHPMailer;

    public function __construct() {
        $this->ci = & get_instance();

        $this->ci->load->helper('email');

        //載入mail內容多國語言
        $this->ci->lang->load("mail", $this->ci->session->i18n);

        //初始化PHPMailer
        require_once APPPATH . 'third_party/PHPMailer/PHPMailerAutoload.php';
        $this->PHPMailer = new PHPMailer;

        //PHPMailer基本設定
        $this->PHPMailer->CharSet = 'UTF-8';
        $this->PHPMailer->SMTPDebug = $this->ci->config->item('mail_SMTPDebug');
        $this->PHPMailer->do_debug = $this->ci->config->item('mail_do_debug');
        $this->PHPMailer->isSMTP();
        $this->PHPMailer->isHTML(true);
        if ($this->ci->config->item('mail_SMTPOptions')) {
            $this->PHPMailer->SMTPOptions = $this->ci->config->item('mail_SMTPOptions');
        }

        //mail service 連線主機
        $this->PHPMailer->Host = $this->ci->config->item('mail_Host');
        $this->PHPMailer->SMTPAuth = $this->ci->config->item('mail_SMTPAuth');
        $this->PHPMailer->Username = $this->ci->config->item('mail_Username');
        $this->PHPMailer->Password = $this->ci->config->item('mail_Password');
        $this->PHPMailer->SMTPSecure = $this->ci->config->item('mail_SMTPSecure');
        $this->PHPMailer->Port = $this->ci->config->item('mail_Port');

        //預設寄件人
        $this->sender_email = $this->ci->site_config->c_service_email;
        $this->sender_name = $this->ci->lang->line("台北AR系統郵件");
        $this->cc_email = array();
        $this->cc_name = array();
    }

    /* ===============================
     * 信件送出
     * 重要：送出前須先完成private參數設定
     * =============================== */

    public function send_mail() {
        //寄件人
        $this->PHPMailer->From = $this->sender_email;
        $this->PHPMailer->FromName = $this->sender_name;

        //清空收件人
        $this->PHPMailer->clearAllRecipients();

        //收件人
        $this->PHPMailer->addAddress($this->receiver_email, $this->receiver_name);

        //副本抄送
        foreach ($this->cc_email as $id => $row) {
            $this->PHPMailer->AddCC($row, $this->cc_name[$id]);
        }

        //信件主題/內容
        $this->PHPMailer->Subject = $this->mail_subject;
        $this->PHPMailer->Body = $this->mail_body;

        return $this->PHPMailer->send();
    }

    /* ===============================
     * 清除收件人
     * =============================== */

    public function clear_recipients() {
        $this->PHPMailer->clearAllRecipients();
    }

    /* ===============================
     * 寄出多重收件人信件
     * 傳入：receivers:  array(
     *                      receiver_email,
     *                      receiver_name
     *                  )
     * =============================== */

    public function send_multi_receiver_mail($receivers) {
        //清空收件人
        $this->PHPMailer->clearAllRecipients();

        //寄件人
        $this->PHPMailer->From = $this->sender_email;
        $this->PHPMailer->FromName = $this->sender_name;

        //副本抄送
        foreach ($this->cc_email as $id => $row) {
            $this->PHPMailer->AddCC($row, $this->cc_name[$id]);
        }

        //收件人
        foreach ($receivers as $row) {
            $this->PHPMailer->addAddress($row["receiver_email"], $row["receiver_name"]);
        }

        //信件主題/內容
        $this->PHPMailer->Subject = $this->mail_subject;
        $this->PHPMailer->Body = $this->mail_body;

        return $this->PHPMailer->send();
    }

    /* ===============================
     * 設定信件內容
     * 傳入值：
     * subject: 主旨
     * body: 信件內容
     * =============================== */

    public function set_content($subject, $body) {

        $this->mail_subject = $subject;
        $this->mail_body = $body;

        return true;
    }

    /* ===============================
     * 設定寄件
     * 傳入值：
     * sender_mail: 收件人email
     * sender_name: 收件人姓名，若未傳入，則使用收件人email
     * =============================== */

    public function set_sender($sender_email, $sender_name = '') {
        //驗證email
        if (!valid_email($sender_email)) {
            return false;
        }

        $this->sender_email = $sender_email;
        if ($sender_name) {
            $this->sender_name = $sender_name;
        } else {
            $this->sender_name = $sender_email;
        }

        return true;
    }

    /* ===============================
     * 設定收件人
     * 傳入值：
     * receiver_mail: 收件人email
     * receiver_name: 收件人姓名，若未傳入，則使用收件人email
     * =============================== */

    public function set_receiver($receiver_email, $receiver_name = '') {
        //驗證email
        if (!valid_email($receiver_email)) {
            return false;
        }

        $this->receiver_email = $receiver_email;
        if ($receiver_name) {
            $this->receiver_name = $receiver_name;
        } else {
            $this->receiver_name = $receiver_email;
        }

        return true;
    }

    /* ===============================
     * 設定副本抄送
     * 傳入值：
     * cc_mail: 收件人email
     * cc_name: 收件人姓名，若未傳入，則使用收件人email
     * =============================== */

    public function set_cc($cc_email, $cc_name = '') {
        //驗證email
        if (!valid_email($cc_email)) {
            return false;
        }

        $this->cc_email[] = $cc_email;
        if ($cc_name) {
            $this->cc_name[] = $cc_name;
        } else {
            $this->cc_name[] = $cc_email;
        }

        return true;
    }

}
