<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Export_csv_lib {

    protected $ci;
    private $filename;
    private $title;
    private $content;

    public function __construct() {
        $this->ci = & get_instance();
        $this->filename = null;
        $this->title = null;
        $this->content = null;
    }

    /* ===============================
     * 執行匯出
     * =============================== */

    public function execute() {
        header("Content-Encoding: UTF-8");
        header("Content-type:application/vnd.ms-excel; charset=UTF-8");
        header("Content-Disposition: attachment; filename={$this->filename}");
        echo "\xEF\xBB\xBF"; //UTF-8 BOM

        if ($this->title) {
            echo $this->title;
        }
        echo $this->content;
    }

    /* ===============================
     * 設定csv 檔名
     * 傳入值：
     * filename: 檔名
     * =============================== */

    public function set_filename($filename) {
        $this->filename = $filename;
    }

    /* ===============================
     * 設定csv 標題列
     * 傳入值：
     * title: 標題
     * =============================== */

    public function set_title($title) {
        $this->title = $title;
    }

    /* ===============================
     * 設定csv 內容
     * 傳入值：
     * content_array: 內容array
     * =============================== */

    public function set_content_array($content_array) {
        if (!$this->content) {
            $this->content = "";
        }
        
        foreach ($content_array as $row) {
            $this->content .= implode(",", $row) . "\n";
        }
    }

}
