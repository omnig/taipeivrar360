<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Taipei_sso_lib
{
    protected $CI;
    protected $ssoConfig;
    protected $api_url_root;
    protected $APP_PRIVATE_ID;
    protected $APP_PRIVATE_PASSWD;
    protected $oauth_url;
    protected $oauth_clientID;
    protected $oauth_clientPASSWD;
    protected $sign_code_url;

    public function __construct()
    {
        $this->CI = &get_instance();

        $this->CI->config->load('sso');
        $this->ssoConfig = $this->CI->config->item('taipei_sso');

        $this->oauth_url = $this->ssoConfig['oauth_url'];
        $this->oauth_clientID = $this->ssoConfig['oauth_clientID'];
        $this->oauth_clientPASSWD = $this->ssoConfig['oauth_clientPASSWD'];
        $this->sign_code_url = $this->ssoConfig['sign_code_url'];
        $this->api_url_root = $this->ssoConfig['api_url_root'];
        $this->APP_PRIVATE_ID = $this->ssoConfig['APP_PRIVATE_ID'];
        $this->APP_PRIVATE_PASSWD = $this->ssoConfig['APP_PRIVATE_PASSWD'];
    }

    private function getBearerToken(): ?string
    {
        $url = $this->oauth_url;
        $clientID = $this->oauth_clientID;
        $clientBlock = $this->oauth_clientPASSWD;
        $body = http_build_query(['grant_type' => 'client_credentials']);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER,
                    array(
                        'Content-Type: application/x-www-form-urlencoded',
                        'Authorization:	Basic ' . base64_encode($clientID . ":" . $clientBlock)
                    )
        );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        $result_string = curl_exec($ch);
        $result = json_decode($result_string, JSON_UNESCAPED_UNICODE);
        curl_close($ch);

        if (!isset($result['access_token'])) {
            log_message('error', __FILE__ . ':' . __FUNCTION__ . ':' . __LINE__ . '=>' . $result_string);
            return null;
        }

        return $result['access_token'];
    }

    private function getSignBlock(): ?string
    {
        $url = $this->sign_code_url;
        $token = $this->getBearerToken();
        if ($token === null) {
            log_message('error', __FILE__ . ':' . __FUNCTION__ . ':' . __LINE__ . '=>' . '無法取得access_token');
            return null;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER,
                    array(
                        'Authorization:	Bearer ' . $token
                    )
        );
        $result_string = curl_exec($ch);
        $result = json_decode($result_string, JSON_UNESCAPED_UNICODE);
        curl_close($ch);

        if ($result === null || $result['ResHeader']['rtnCode'] !== '0000') {
            log_message('error', __FILE__ . ':' . __FUNCTION__ . ':' . __LINE__ . '=>' . $result_string);
            return null;
        }

        return json_encode(
            [
                'access_token' => $token,
                'signBlock' => $result['Res_getSignBlock']['signBlock']
            ]
        );
    }

    private function apiRequest(string $urlCode, string $bodyJson, $verb = 'POST'): ?string
    {
        $preProcessJson = $this->getSignBlock($bodyJson);
        if ($preProcessJson === null) {
            log_message('error', __FILE__ . ':' . __FUNCTION__ . ':' . __LINE__ . '=>' . '無法取得SignBlock');
            return null;
        }
        $preProcessData = json_decode($preProcessJson, true);
        $access_token = $preProcessData['access_token'];
        $signBlock = $preProcessData['signBlock'];
        $signString = $signBlock . $bodyJson;
        $signCode = bin2hex(hash('sha256', $signString, true));

        $url = $this->api_url_root . $urlCode;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ACCEPT_ENCODING, "gzip,deflate");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $verb);
        curl_setopt($ch, CURLOPT_HTTPHEADER,
                    array(
                        'Content-Type: application/json;charset=utf-8',
                        'Authorization:	Bearer ' . $access_token,
                        'SignCode: ' . $signCode,
                    )
        );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $bodyJson);
        $result_string = curl_exec($ch);
        $result = json_decode($result_string, JSON_UNESCAPED_UNICODE);
        curl_close($ch);

        if ($result === null || !isset($result['ERROR_CODE']) || $result['ERROR_CODE'] !== 0) {
            log_message('error', __FILE__ . ':' . __FUNCTION__ . ':' . __LINE__ . '=>' . $result_string);
        }

        return $result_string;
    }

    public function systemAuthentication(): ?array
    {
        $body = new stdClass();
        $body->APP_PRIVATE_ID = $this->APP_PRIVATE_ID;
        $body->APP_PRIVATE_PASSWD = $this->APP_PRIVATE_PASSWD;

        $result_string = $this->apiRequest('WS-Z01-A0-01', json_encode($body));
        $result = json_decode($result_string, true);
        if ($result === null) {
            return ['success' => false, 'msg' => '單一登入系統無法驗證', 'result' => $result_string];
        }

        if ($result['ERROR_CODE'] !== '0') {
            return ['success' => false, 'msg' => '帳號密碼錯誤', 'result' => $result];
        }

        return ['success' => true, 'msg' => '應用程式驗證成功', 'result' => $result];
    }

    public function userAuthentication($privilegedAppSSOToken, $userSSOToken): ?array
    {
        $body = new stdClass();
        $body->PRIVILEGED_APP_SSO_TOKEN = $privilegedAppSSOToken;
        $body->PUBLIC_APP_USER_SSO_TOKEN_TO_QUERY = $userSSOToken;

        $result_string = $this->apiRequest('WS-Z01-B0-06', json_encode($body));
        $result = json_decode($result_string, true);
        if ($result === null) {
            return ['success' => false, 'msg' => '單一登入系統無法驗證', 'result' => $result_string];
        }

        if ($result['ERROR_CODE'] !== '0') {
            return ['success' => false, 'msg' => 'token 錯誤', 'result' => $result];
        }

        return ['success' => true, 'msg' => '使用者驗證成功', 'result' => $result];
    }

    public function userInfo($privilegedAppSSOToken, $userSSOToken, $companyUUID, $userNodeUUID): ?array
    {
        $body = new stdClass();
        $body->PRIVILEGED_APP_SSO_TOKEN = $privilegedAppSSOToken;
        $body->PUBLIC_APP_USER_SSO_TOKEN = $userSSOToken;
        $body->APP_COMPANY_UUID = $companyUUID;
        $body->APP_USER_NODE_UUID = $userNodeUUID;
        // 選取屬性資料
        $basicAttributes = new stdClass();
        $basicAttributes->APP_DEPT_NODE_UUID = '';
        $basicAttributes->APP_USER_LOGIN_ID = '';
        $basicAttributes->APP_USER_CHT_NAME = '';
        $basicAttributes->APP_USER_EMAIL = '';
        $basicAttributes->APP_USER_STATUS = '';
        $body->APP_USER_BASIC_PROFILE = $basicAttributes;
        $basicHRAttributes = new stdClass();
        $body->APP_USER_HR_PROFILE = $basicHRAttributes;

        $result_string = $this->apiRequest('WS-Z01A-S-B05', json_encode($body));
        $result = json_decode($result_string, true);
        if ($result === null) {
            return ['success' => false, 'msg' => '單一登入系統無法取得人員資料', 'result' => $result_string];
        }

        if ($result['ERROR_CODE'] !== 0) {
            return ['success' => false, 'msg' => '人員資料', 'result' => $result];
        }

        return ['success' => true, 'msg' => '使用者資料獲取成功', 'result' => $result];
    }

    public function deptInfo($privilegedAppSSOToken, $companyUUID, $deptNodeUUID): ?array
    {
        $body = new stdClass();
        $body->PRIVILEGED_APP_SSO_TOKEN = $privilegedAppSSOToken;
        $body->APP_COMPANY_UUID = $companyUUID;
        $body->APP_DEPT_NODE_UUID = $deptNodeUUID;
        // 選取屬性資料
        $basicAttributes = new stdClass();
        $basicAttributes->APP_DEPT_NODE_UUID = '';
        $basicAttributes->APP_DEPT_PNODE_UUID = '';
        $basicAttributes->APP_DEPT_NAME = '';
        $basicAttributes->APP_DEPT_CODE_NO = '';
        $basicAttributes->APP_DEPT_STATUS = '';
        $body->APP_DEPT_BASIC_PROFILE = $basicAttributes;

        $result_string = $this->apiRequest('WS-Z01A-D-A05', json_encode($body));
        $result = json_decode($result_string, true);
        if ($result === null) {
            return ['success' => false, 'msg' => '單一登入系統無法取得部門資料', 'result' => $result_string];
        }

        if (isset($result['ERROR_CODE']) && $result['ERROR_CODE'] !== 0) {
            return ['success' => false, 'msg' => '部門資料', 'result' => $result];
        }

        return ['success' => true, 'msg' => '部門資料獲取成功', 'result' => $result];
    }

}
