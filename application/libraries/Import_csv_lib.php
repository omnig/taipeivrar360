<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Import_csv_lib {

    protected $ci;
    private $csv_file;
    private $filename;
    private $upload_tmp;
    private $is_csv_file;

    public function __construct() {
        $this->ci = & get_instance();
        $this->csv_file = null;
        $this->filename = null;
        $this->upload_tmp = null;
        $this->is_csv_file = null;
    }

    /* ===============================
     * 執行匯出
     * =============================== */

    public function execute() {
        header("Content-Encoding: UTF-8");
        header("Content-type:application/vnd.ms-excel; charset=UTF-8");
        header("Content-Disposition: attachment; filename={$this->filename}");
        echo "\xEF\xBB\xBF"; //UTF-8 BOM

        if ($this->title) {
            echo $this->title;
        }
        echo $this->content;
    }

    /* ===============================
     * 設定csv 檔名
     * 傳入值：
     * filename: 檔名
     * =============================== */

    public function init_csv_file($csv_file) {
        $this->csv_file = $csv_file;
        $filename = $csv_file["name"];
        $upload_tmp = $csv_file["tmp_name"];
        $is_csv_file = (substr($filename, -3) == "csv");

        $this->filename = $filename;
        $this->upload_tmp = $upload_tmp;
        $this->is_csv_file = $is_csv_file;

        if (!$upload_tmp || !$is_csv_file) {
            _alert("請上傳csv檔");
            exit();
        }

        $handle = fopen($upload_tmp, "r");

        return $handle;
    }

    /* ===============================
     * 取得CSV資料
     * =============================== */

    public function get_csv_data_array($handle, $title = 1) {

        if ($this->upload_tmp && $this->is_csv_file) {
            $i = 0;
            $field = 0;
            $ret = array();

            //判斷是否為逗號分隔
            $row = fgets($handle);
            if (!strrpos($row, ",")) {
                _alert("檔案格式錯誤！\\n\\n請上傳以「CSV(逗號分隔)」的存檔格式");
                exit();
            } else {
                $field = count(explode(",", $row));
            }

            //開始截取資料
            while ($row = fgetcsv($handle)) {
                //前面已fgets一行，所以要退回一行截取資料
                if ($i >= ($title - 1)) { 
                    $data = array();
                    foreach ($row as $r => $val) {
                        if (!mb_detect_encoding($row[$r])) {
                            $convertedString = iconv("BIG5", "UTF-8", $row[$r]);
                        } else {
                            $convertedString = $row[$r];
                        }
                        array_push($data, $convertedString);
                    }
                    array_push($ret, $data);
                }
                $i++;
            }

            if ((count($ret) + $title - 1) != $i) {
                _alert("第 " . (count($ret) + $title) . " 列檔案內容有誤！");
                exit();
            }

            return $ret;
        } else {
            _alert("請上傳csv檔");
            exit();
        }
    }

    /* ===============================
     * 取得CSV檔名
     * =============================== */

    public function get_filename() {
        return $this->filename;
    }

    /* ===============================
     * 取得CSV暫存路徑
     * =============================== */

    public function get_upload_temp() {
        return $this->upload_tmp;
    }

    /* ===============================
     * 判斷上傳檔案是否為CSV
     * =============================== */

    public function is_csv_file() {
        return $this->is_csv_file;
    }

}
