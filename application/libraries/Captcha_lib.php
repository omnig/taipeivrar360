<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Captcha_lib {

    protected $ci;
    protected $upload_path = 'upload/captcha/';

    public function __construct() {
        $this->ci = & get_instance();
    }

    /* ==================================
     * 建立驗證碼
     * ================================== */

    public function get_captcha() {
        $this->ci->load->helper('captcha');
        $vals = array(
            'word' => (string) rand(1000, 9999),
            'img_path' => FCPATH . $this->upload_path,
            'img_url' => base_url($this->upload_path) . '/',
            'img_height' => 43,
            'font_size' => 30
        );

        $cap = create_captcha($vals);

        $insert_array = array(
            'captcha_time' => $cap['time'],
            'ip_address' => $this->ci->input->ip_address(),
            'word' => $cap['word'],
            'captcha_filename' => "{$this->upload_path}{$cap['filename']}"
        );

        $this->ci->Common_model->insert_db('captcha', $insert_array);

        $cap['id'] = $this->ci->Common_model->get_insert_id();
        
        //圖檔寫入資料庫
        $this->ci->upload_lib->write("{$this->upload_path}{$cap['filename']}");

        //刪除圖檔
        @unlink(FCPATH . "{$this->upload_path}{$cap['filename']}");

        return $cap;
    }

    /* ==================================
     * 確認驗證碼
     * ================================== */

    public function verify_captcha($captcha_id, $captcha_word) {
        //刪除逾時認證圖片
        $expiration = time() - 86400;

        $where_array = array(
            "captcha_time <" => $expiration
        );

        //先取得已逾時的圖片檔
        $expired_captcha = $this->ci->Common_model->get_db('captcha', $where_array);

        foreach ($expired_captcha as $row) {
            $this->delete_captcha_file($row->captcha_filename);
        }

        $this->ci->Common_model->delete_db('captcha', $where_array);

        // 然後驗證是否存在資料:
        $where_array = array(
            "captcha_id" => $captcha_id,
            "word" => $captcha_word,
            "ip_address" => $this->ci->input->ip_address(),
            "captcha_time >" => $expiration
        );

        $captcha = $this->ci->Common_model->get_one('captcha', $where_array);

        if ($captcha) {
            //驗證成功，刪除此驗證碼
            $this->delete_captcha_file($captcha->captcha_filename, true);

            return true;
        }

        return false;
    }

    /* ==================================
     * 刪除驗證碼圖檔(儲存在資料庫中的)
     * $remove_captcha_db若傳入true，則將captcha資料表中，有相同檔名的captcha一併刪除
     * ================================== */

    public function delete_captcha_file($filename, $remove_captcha_db = false) {
        //刪除upload檔案
        $this->ci->upload_lib->delete($filename);

        //刪除captcha資料表
        if ($remove_captcha_db) {
            $where_array = array(
                "captcha_filename" => $filename
            );
            $this->ci->Common_model->delete_db('captcha', $where_array);
        }
    }

}
