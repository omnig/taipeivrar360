<?php

/*
 * 將上傳檔案寫入資料庫輔助函式
 * 適用於多伺服器、Load balanced server環境
 * 可確保上傳伺服器後，從任何server均可正常取得檔案
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Upload_lib {

    protected $ci;

    public function __construct() {
        $this->ci = & get_instance();
        $this->ci->load->library('upload');
        $this->ci->load->model("Image_upload_model");
        $this->ci->load->model("Video_upload_model");
        $this->ci->load->model("Model_upload_model");
    }

    /*
     * 讀取
     */

    public function read($filename = false) {

        if (!$filename) {
            show_404();
        }

        $where_array = array(
            'u_filename' => $filename
        );

        $file = $this->ci->Common_model->get_one('upload', $where_array);

        if (!$file) {
            show_404();
        }

        return $file;
    }


    public function read_image($filename = false) {

        if (!$filename) {
            show_404();
        }

        $where_array = array(
            'u_filename' => $filename
        );

        $file = $this->ci->Image_upload_model->get_one('upload_image', $where_array);

        if (!$file) {
            show_404();
        }

        return $file;
    }

    public function read_video($filename = false) {

        if (!$filename) {
            show_404();
        }

        $where_array = array(
            'u_filename' => $filename
        );


        $file = $this->ci->Video_upload_model->get_one('upload_video', $where_array);

        if (!$file) {
            show_404();
        }

        return $file;
    }

    public function read_model($filename = false) {

        if (!$filename) {
            show_404();
        }

        $where_array = array(
            'u_filename' => $filename
        );


        $file = $this->ci->Model_upload_model->get_one('upload_model', $where_array);

        if (!$file) {
            show_404();
        }

        return $file;
    }

    public function read_wtc($filename = false) {

        if (!$filename) {
            show_404();
        }

        $where_array = array(
            'u_filename' => $filename
        );

        $file = $this->ci->Common_model->get_one('upload_og_ar', $where_array);

        if (!$file) {
            show_404();
        }

        return $file;
    }
    /*
     * 寫入
     * 傳入的filename格式： upload/captcha/some_file.jpg
     * 開檔時須在前面加上 FCPATH 取得真實路徑
     */

    public function write($filename = false) {

        if (!$filename) {
            return false;
        }

        @$handle = fopen(FCPATH . $filename, "r");
        if (!$handle) {
            return false;
        }

        $data = fread($handle, filesize(FCPATH . $filename));

        //取得mime type
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime = finfo_file($finfo, $filename);
        finfo_close($finfo);

        //確認資料庫中是否已有此檔案
        $where_array = array(
            'u_filename' => $filename
        );

        $file = $this->ci->Common_model->get_one('upload', $where_array);

        if (!$file) {
            //不存在，新增一筆
            $insert_array = array(
                'u_filename' => $filename,
                'u_data' => $data,
                'u_mime' => $mime
            );

            $rows_affected = $this->ci->Common_model->insert_db('upload', $insert_array);
            if ($rows_affected == 0) {
                return false;
            }

            return true;
        }

        //已存在，更新為此筆
        $where_array = array(
            'u_filename' => $filename
        );

        $update_array = array(
            'u_data' => $data,
            'u_mime' => $mime
        );

        $rows_affected = $this->ci->Common_model->update_db('upload', $update_array, $where_array);
        if ($rows_affected == 0) {
            return false;
        }

        return true;
    }

    public function write_image($filename = false) {

        if (!$filename) {
            return false;
        }

        @$handle = fopen(FCPATH . $filename, "r");
        if (!$handle) {
            return false;
        }

        $data = fread($handle, filesize(FCPATH . $filename));

        //取得mime type
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime = finfo_file($finfo, $filename);
        finfo_close($finfo);

        //確認資料庫中是否已有此檔案
        $where_array = array(
            'u_filename' => $filename
        );

        $file = $this->ci->Image_upload_model->get_one('upload_image', $where_array);

        if (!$file) {
            //不存在，新增一筆
            $insert_array = array(
                'u_filename' => $filename,
                'u_data' => $data,
                'u_mime' => $mime
            );

            $rows_affected = $this->ci->Image_upload_model->insert_db('upload_image', $insert_array);
            if ($rows_affected == 0) {
                return false;
            }

            return true;
        }

        //已存在，更新為此筆
        $where_array = array(
            'u_filename' => $filename
        );

        $update_array = array(
            'u_data' => $data,
            'u_mime' => $mime
        );

        $rows_affected = $this->ci->Image_upload_model->update_db('upload_image', $update_array, $where_array);
        if ($rows_affected == 0) {
            return false;
        }

        return true;
    }

    public function write_video($filename = false) {

        if (!$filename) {
            return false;
        }

        @$handle = fopen(FCPATH . $filename, "r");
        if (!$handle) {
            return false;
        }

        $data = fread($handle, filesize(FCPATH . $filename));

        //取得mime type
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime = finfo_file($finfo, $filename);
        finfo_close($finfo);

        //確認資料庫中是否已有此檔案
        $where_array = array(
            'u_filename' => $filename
        );

        $file = $this->ci->Video_upload_model->get_one('upload_video', $where_array);

        if (!$file) {
            //不存在，新增一筆
            $insert_array = array(
                'u_filename' => $filename,
                'u_data' => $data,
                'u_mime' => $mime
            );

            $rows_affected = $this->ci->Video_upload_model->insert_db('upload_video', $insert_array);
            if ($rows_affected == 0) {
                return false;
            }

            return true;
        }

        //已存在，更新為此筆
        $where_array = array(
            'u_filename' => $filename
        );

        $update_array = array(
            'u_data' => $data,
            'u_mime' => $mime
        );

        $rows_affected = $this->ci->Video_upload_model->update_db('upload_video', $update_array, $where_array);
        if ($rows_affected == 0) {
            return false;
        }

        return true;
    }


    public function write_model($filename = false) {

        if (!$filename) {
            return false;
        }

        @$handle = fopen(FCPATH . $filename, "r");
        if (!$handle) {
            return false;
        }

        $data = fread($handle, filesize(FCPATH . $filename));

        //取得mime type
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime = finfo_file($finfo, $filename);
        finfo_close($finfo);

        //確認資料庫中是否已有此檔案
        $where_array = array(
            'u_filename' => $filename
        );

        $file = $this->ci->Model_upload_model->get_one('upload_model', $where_array);

        if (!$file) {
            //不存在，新增一筆
            $insert_array = array(
                'u_filename' => $filename,
                'u_data' => $data,
                'u_mime' => $mime
            );

            $rows_affected = $this->ci->Model_upload_model->insert_db('upload_model', $insert_array);
            if ($rows_affected == 0) {
                return false;
            }

            return true;
        }

        //已存在，更新為此筆
        $where_array = array(
            'u_filename' => $filename
        );

        $update_array = array(
            'u_data' => $data,
            'u_mime' => $mime
        );

        $rows_affected = $this->ci->Model_upload_model->update_db('upload_model', $update_array, $where_array);
        if ($rows_affected == 0) {
            return false;
        }

        return true;
    }


    public function write_wtc($filename = false) {

        if (!$filename) {
            return false;
        }

        @$handle = fopen(FCPATH . $filename, "r");
        if (!$handle) {
            return false;
        }

        $data = fread($handle, filesize(FCPATH . $filename));

        //取得mime type
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime = finfo_file($finfo, $filename);
        finfo_close($finfo);

        //確認資料庫中是否已有此檔案
        $where_array = array(
            'u_filename' => $filename
        );

        $file = $this->ci->Common_model->get_one('upload_og_ar', $where_array);

        if (!$file) {
            //不存在，新增一筆
            $insert_array = array(
                'u_filename' => $filename,
                'u_data' => $data,
                'u_mime' => $mime
            );

            $rows_affected = $this->ci->Common_model->insert_db('upload_og_ar', $insert_array);
            if ($rows_affected == 0) {
                return false;
            }

            return true;
        }

        //已存在，更新為此筆
        $where_array = array(
            'u_filename' => $filename
        );

        $update_array = array(
            'u_data' => $data,
            'u_mime' => $mime,
            'u_timestamp'=> date("Y-m-d H:i:s")
        );

        $rows_affected = $this->ci->Common_model->update_db('upload_og_ar', $update_array, $where_array);
        if ($rows_affected == 0) {
            return false;
        }

        return true;
    }
    /*
     * 刪除
     */

    public function delete($filename = false) {

        if (!$filename) {
            return false;
        }

        $where_array = array(
            'u_filename' => $filename
        );

        $rows_affected = $this->ci->Common_model->delete_db('upload', $where_array);

        if ($rows_affected == 0) {
            return false;
        }

        return true;
    }

    /*
     * 從url讀取後寫入
     * 傳入的filename格式： upload/captcha/some_file.jpg
     * 開檔時須在前面加上 FCPATH 取得真實路徑
     */

    public function write_from_url($url = false, $filename = false) {

        if (!$url || !$filename) {
            return false;
        }

        $data = file_get_contents($url);

        //取得mime type
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);

        $mime = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);

        //確認資料庫中是否已有此檔案
        $where_array = array(
            'u_filename' => $filename
        );

        $file = $this->ci->Common_model->get_one('upload', $where_array);

        if (!$file) {
            //不存在，新增一筆
            $insert_array = array(
                'u_filename' => $filename,
                'u_data' => $data,
                'u_mime' => $mime
            );

            $rows_affected = $this->ci->Common_model->insert_db('upload', $insert_array);
            if ($rows_affected == 0) {
                return false;
            }

            return true;
        }

        //已存在，更新為此筆
        $where_array = array(
            'u_filename' => $filename
        );

        $update_array = array(
            'u_data' => $data,
            'u_mime' => $mime
        );

        $rows_affected = $this->ci->Common_model->update_db('upload', $update_array, $where_array);
        if ($rows_affected == 0) {
            return false;
        }

        return true;
    }

    /*
     * 從$_FILES讀取後寫入
     * 傳入的$upload_path格式： upload/path/
     * 開檔時須在前面加上 FCPATH 取得真實路徑
     * fit_type: 縮放模式：
     *      FIT_INNER: 將圖縮至指定寬高以內，不裁剪
     *      FIT_INNER_NO_ZOOM_IN: 將圖縮至指定寬高以內，不裁剪，若圖片大小小於限制尺寸，則不放大
     *      FIT_OUTER: 將圖縮至指定寬高以外,並置中裁剪
     */

    public function write_from_upload($upload_path = 'upload/tmp', $field_name = '', $file_type = 'gif|jpg|jpeg|png', $max_size = 1024, $resize_width = 0, $resize_height = 0, $fit_type = 'FIT_INNER', $max_width = 2048, $max_height = 1440) {
        //相容windows路徑
        $config['upload_path'] = FCPATH . $upload_path;
        if (DIRECTORY_SEPARATOR == '\\') {
            $config['upload_path'] = str_replace('/', '\\', $config['upload_path']);
        }

        if (!file_exists($config['upload_path'])) {
            exec("mkdir " . $config['upload_path']);
        }

        $config['allowed_types'] = $file_type;
        $config['file_name'] = substr(md5(uniqid(rand())), 3, 12);
        
        if ($resize_width == 0 && $resize_height == 0) {
            //沒有處理縮圖，就要限制上傳寬高及大小
            $config['max_size'] = $max_size;
            $config['max_width'] = $max_width;
            $config['max_height'] = $max_height;
        }

        $this->ci->upload->initialize($config);

        if (!$this->ci->upload->do_upload($field_name)) {
            $error = array('error' => $this->ci->upload->display_errors("", ""));
            return $error;
        }

        $upload_info = $this->ci->upload->data();

        $filename = $upload_path . $upload_info['file_name'];

        //縮圖
        if ($resize_width != 0 || $resize_height != 0) {
            $this->resize_image(FCPATH . $filename, $resize_width, $resize_height, $fit_type);
        }

        $this->write($filename);
        @unlink(FCPATH . $filename);

        $data = array('upload_data' => $this->ci->upload->data());
        return $data;
    }


    public function write_from_image($upload_path = 'upload_image/', $field_name = '', $file_type = 'gif|jpg|jpeg|png', $max_size = 1024, $resize_width = 0, $resize_height = 0, $fit_type = 'FIT_INNER') {
        //相容windows路徑
        $config['upload_path'] = FCPATH . $upload_path;
        if (DIRECTORY_SEPARATOR == '\\') {
            $config['upload_path'] = str_replace('/', '\\', $config['upload_path']);
        }

        if (!file_exists($config['upload_path'])) {
            exec("mkdir " . $config['upload_path']);
        }

        $config['allowed_types'] = $file_type;
        $config['file_name'] = substr(md5(uniqid(rand())), 3, 12);
        
        $config['max_size'] = $max_size;
        
        $this->ci->upload->initialize($config);

        if (!$this->ci->upload->do_upload($field_name)) {
            $error = array('error' => $this->ci->upload->display_errors("", ""));
            return $error;
        }

        $upload_info = $this->ci->upload->data();

        $filename = $upload_path . $upload_info['file_name'];

        //縮圖
        if ($resize_width != 0 || $resize_height != 0) {
            $this->resize_image(FCPATH . $filename, $resize_width, $resize_height, $fit_type);
        }

        $this->write_image($filename);
        @unlink(FCPATH . $filename);

        $data = array('upload_data' => $this->ci->upload->data());
        return $data;
    }

    public function write_from_video($upload_path = 'upload_video', $field_name = '', $file_type = 'mp4', $max_size = 1024) {
        //相容windows路徑
        $config['upload_path'] = FCPATH . $upload_path;
        if (DIRECTORY_SEPARATOR == '\\') {
            $config['upload_path'] = str_replace('/', '\\', $config['upload_path']);
        }

        if (!file_exists($config['upload_path'])) {
            exec("mkdir " . $config['upload_path']);
        }

        $config['allowed_types'] = $file_type;
        $config['file_name'] = substr(md5(uniqid(rand())), 3, 12);
        
        $this->ci->upload->initialize($config);

        if (!$this->ci->upload->do_upload($field_name)) {
            $error = array('error' => $this->ci->upload->display_errors("", ""));
            return $error;
        }

        $upload_info = $this->ci->upload->data();

        $filename = $upload_path . $upload_info['file_name'];

        //暫時不寫進資料庫
        // $this->write_video($filename);
        // @unlink(FCPATH . $filename);

        $data = array('upload_data' => $this->ci->upload->data());
        return $data;
    }

    public function write_from_model($upload_path = 'upload_model/', $field_name = '', $file_type = '*', $max_size = 1024) {
        //相容windows路徑
        $config['upload_path'] = FCPATH . $upload_path;
        if (DIRECTORY_SEPARATOR == '\\') {
            $config['upload_path'] = str_replace('/', '\\', $config['upload_path']);
        }

        if (!file_exists($config['upload_path'])) {
            exec("mkdir " . $config['upload_path']);
        }

        $config['allowed_types'] = "*";
        

        $config['file_name'] = substr(md5(uniqid(rand())), 3, 12);
        
        $this->ci->upload->initialize($config);
        // $this->ci->upload->is_allowed_filetype(TRUE);
        if (!$this->ci->upload->do_upload($field_name)) {
            $error = array('error' => $this->ci->upload->display_errors("", ""));
            echo json_encode($error);
            exit();
            return $error;
        }

        $upload_info = $this->ci->upload->data();



        $filename = $upload_path . $upload_info['file_name'];


        $this->write_model($filename);
        @unlink(FCPATH . $filename);

        $data = array('upload_data' => $this->ci->upload->data());
        return $data;
    }

    public function write_from_wtc($upload_path = 'upload_wtc/', $field_name = '' ,$new_name= '') {
        //相容windows路徑
        $config['upload_path'] = FCPATH . $upload_path;
        if (DIRECTORY_SEPARATOR == '\\') {
            $config['upload_path'] = str_replace('/', '\\', $config['upload_path']);
        }

        if (!file_exists($config['upload_path'])) {
            exec("mkdir " . $config['upload_path']);
        }

        $config['allowed_types'] = "*";
        

        $config['file_name'] = $new_name;
        
        $this->ci->upload->initialize($config);
        // $this->ci->upload->is_allowed_filetype(TRUE);
        if (!$this->ci->upload->do_upload($field_name)) {
            $error = array('error' => $this->ci->upload->display_errors("", ""));
            echo json_encode($error);
            exit();
            return $error;
        }

        $upload_info = $this->ci->upload->data();

        // echo json_encode($upload_info);
        // exit();

        $filename = $upload_path .$upload_info['file_name'];


        $this->write_wtc($filename);
        @unlink(FCPATH . $filename);

        $data = array('upload_data' => $this->ci->upload->data());
        return $data;
    }
    /*
     * 從$_FILES讀取後寫入
     * 傳入的$upload_path格式： upload/path/
     * 開檔時須在前面加上 FCPATH 取得真實路徑
     * fit_type: 縮放模式：
     *      FIT_INNER: 將圖縮至指定寬高以內，不裁剪
     *      FIT_INNER_NO_ZOOM_IN: 將圖縮至指定寬高以內，不裁剪，若圖片大小小於限制尺寸，則不放大
     *      FIT_OUTER: 將圖縮至指定寬高以外,並置中裁剪
     */

    public function write_upload_nodb($upload_path = 'upload/tmp', $field_name = '', $file_type = 'gif|jpg|jpeg|png', $max_size = 1024) {
        //相容windows路徑
        $config['upload_path'] = FCPATH . $upload_path;
        if (DIRECTORY_SEPARATOR == '\\') {
            $config['upload_path'] = str_replace('/', '\\', $config['upload_path']);
        }

        if (!file_exists($config['upload_path'])) {
            exec("mkdir " . $config['upload_path']);
        }

        $config['allowed_types'] = $file_type;
        $config['file_name'] = substr(md5(uniqid(rand())), 3, 12);
        

        $this->ci->upload->initialize($config);

        if (!$this->ci->upload->do_upload($field_name)) {
            $error = array('error' => $this->ci->upload->display_errors("", ""));
            return $error;
        }

        $upload_info = $this->ci->upload->data();

        $filename = $upload_path . $upload_info['file_name'];

        return $filename = $upload_path . $upload_info['file_name'];
    }

    /* ==================================
     * 縮圖輔助函式
     * 
     * 傳入值：
     * $source_image: 來源檔案
     * $to_width: 寬度
     * $to_height: 高度
     * $fit_type: 縮放模式，可為 
     *      FIT_INNER: 將圖縮至指定寬高以內，不裁剪
     *      FIT_INNER_NO_ZOOM_IN: 將圖縮至指定寬高以內，不裁剪，若圖片大小小於限制尺寸，則不放大
     *      FIT_OUTER: 將圖縮至指定寬高以外,並置中裁剪
     * 
     * 回傳值：
     * 成功：true
     * 失敗：string of error message
     * ================================== */

    public function resize_image($source_image, $to_width, $to_height, $fit_type = 'FIT_OUTER') {
        if (!file_exists($source_image)) {
            return "圖片不存在";
        }

        $this->ci->load->library('image_lib');  //載入縮圖函式庫
        //縮圖基本設定
        $gd_config['image_library'] = 'gd2';
        $gd_config['source_image'] = $source_image;

        //相容windows路徑
        if (DIRECTORY_SEPARATOR == '\\') {
            str_replace('/', '\\', $gd_config['source_image']);
        }

        $ary = getimagesize($source_image);
        $width = $ary[0];
        $height = $ary[1];

        //計算縮放比例
        $ratio = 1;
        $x_ratio = $width / $to_width;
        $y_ratio = $height / $to_height;

        switch ($fit_type) {
            case 'FIT_INNER' :
                if ($x_ratio > $y_ratio) {//x, y取較大的縮放比例
                    $ratio = $x_ratio;
                } else {
                    $ratio = $y_ratio;
                }

                break;
            case 'FIT_OUTER' :
                if ($x_ratio < $y_ratio) {//x, y取較小的縮放比例
                    $ratio = $x_ratio;
                } else {
                    $ratio = $y_ratio;
                }

                break;
            case 'FIT_INNER_NO_ZOOM_IN' :
                if ($x_ratio > $y_ratio) {//x, y取較大的縮放比例
                    $ratio = $x_ratio;
                } else {
                    $ratio = $y_ratio;
                }
                if ($ratio < 1) {
                    $ratio = 1;
                }
                break;
            default:
                return "縮放模式設定錯誤";
        }

        //計算實際的縮放寬高

        $gd_config['width'] = $width / $ratio;
        $gd_config['height'] = $height / $ratio;

        $this->ci->image_lib->initialize($gd_config); //重置設定值
        if (!$this->ci->image_lib->resize()) {  //進行縮圖
            return false;
        } else {
            //縮圖成功，計算是否需要切圖
            $gd_config['x_axis'] = ($gd_config['width'] - $to_width) / 2;
            $gd_config['y_axis'] = ($gd_config['height'] - $to_height) / 2;
            $gd_config['width'] = $to_width;
            $gd_config['height'] = $to_height;
            $gd_config['maintain_ratio'] = FALSE;
            if ($gd_config['x_axis'] >= 0 && $gd_config['y_axis'] >= 0 && $gd_config['x_axis'] + $gd_config['y_axis'] >= 1) {
                $this->ci->image_lib->clear();
                $this->ci->image_lib->initialize($gd_config); //重置設定值
                if (!$this->ci->image_lib->crop()) {  //進行裁切
                    return false;
                }
            }
            return true;
        }
    }

}
