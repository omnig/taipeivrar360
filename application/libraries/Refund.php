<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* ================================
 * 先初始化訂單(order)
 * 然後自動取得所有相關的order_store、order_item、refund_request...等
 * ================================ */

class Refund {

    protected $ci;
    //處理中的退貨狀態
    protected $in_process_refund_status;
    //以下是現有資料庫中的資訊
    protected $order;
    protected $order_store; //以os_id為key
    protected $order_item;
    protected $refund_request;
    //準備新增的退貨商品
    protected $refund_item; //以oi_id為key記錄數量
    //準備取消的refund_request
    protected $to_cancel;

    public function __construct() {
        $this->ci = & get_instance();
    }

    /* ================================
     * 將訂單相關參數全部建立起來
     * ================================ */

    private function init_params() {
        if (!isset($this->order->o_id)) {
            return false;
        }

        $this->init_order_item();
        $this->init_order_store();
        $this->init_refund_request();
        $this->refund_item = array();
        $this->to_cancel = array();
        $this->to_refund = array();
        $this->in_process_refund_status = array(
            "AGREE", "CARGO_RETURN"
        );

        return true;
    }

    /* ================================
     * 讀取order_item
     * ================================ */

    private function init_order_item() {
        $where_array = array(
            "o_id" => $this->order->o_id
        );
        $this->order_item = $this->ci->Common_model->get_db("order_item", $where_array);
    }

    /* ================================
     * 讀取order_store
     * ================================ */

    private function init_order_store() {
        $where_array = array(
            "o_id" => $this->order->o_id
        );

        $order_store = $this->ci->Common_model->get_db_join("order_store AS os", $where_array, "store AS s", "os.s_id=s.s_id", "INNER");

        //以os_id為key
        $this->order_store = array();
        foreach ($order_store as $row) {
            $this->order_store[$row->os_id] = $row;
        }
    }

    /* ================================
     * 讀取refund_request
     * ================================ */

    private function init_refund_request() {
        $oi_ids = array();
        foreach ($this->order_item as $row) {
            $oi_ids[] = $row->oi_id;
        }

        $where_array = array(
            "oi_id" => $oi_ids,
            "rr_status !=" => "CANCEL" //已取消的退貨申請不具意義，任何狀態下都不用讀取
        );
        $this->refund_request = $this->ci->Common_model->get_db("refund_request", $where_array);
    }

    /* ================================
     * 查詢order_store資料，取得os_id
     * 
     * 傳入：o_id、s_id
     * ================================ */

    private function get_os_id($o_id, $s_id) {
        foreach ($this->order_store as $row) {
            if ($row->o_id == $o_id && $row->s_id == $s_id) {
                return $row->os_id;
            }
        }

        return false;
    }

    /* ================================
     * 查詢refund_request
     * 
     * 傳入：oi_id
     *      rr_status
     * 回傳：refund_request object
     * ================================ */

    public function get_refund_request($oi_id, $rr_status) {
        if (!$this->refund_request) {
            return false;
        }

        foreach ($this->refund_request as $row) {
            if ($row->oi_id == $oi_id && $row->rr_status == $rr_status) {
                return $row;
            }
        }

        return false;
    }

    /* ================================
     * 查詢refund_request
     * 
     * 傳入：rr_id
     * 回傳：refund_request object
     * ================================ */

    public function get_refund_request_by_rr_id($rr_id) {
        if (!$this->refund_request) {
            return false;
        }

        foreach ($this->refund_request as $row) {
            if ($row->rr_id == $rr_id) {
                return $row;
            }
        }

        return false;
    }

    /* ================================
     * 以p_id, qty查詢相對的order_item
     * 
     * 傳入：p_id
     *      qty
     *      
     * 回傳：order_item object
     * ================================ */

    public function get_order_item_by_parm($p_id, $qty) {
        if (!$this->order_item) {
            return false;
        }

        foreach ($this->order_item as $row) {
            if ($row->p_id == $p_id && $row->oi_qty == $qty) {
                return $row;
            }
        }

        return false;
    }

    /* ================================
     * 傳入oi_id初始化
     * ================================ */

    public function init_order_by_oi_id($oi_id, $u_id = false, $trial_expire_time = false) {
        $where_array = array(
            "oi_id" => $oi_id
        );
        $order_item = $this->ci->Common_model->get_one("order_item", $where_array);

        if (!$order_item) {
            return false;
        }

        $where_array = array(
            "o_order_number IS NOT NULL",
            "o_order_number !=" => '',
            "o_payment_timestamp IS NOT NULL",
            "o_id" => $order_item->o_id
        );

        if ($u_id) {
            $where_array["u_id"] = $u_id;
        }

        if ($trial_expire_time) {
            $where_array[] = "(o_delivery_timestamp IS NULL OR o_delivery_timestamp > '{$trial_expire_time}')";
        }

        $order = $this->ci->Common_model->get_one("order", $where_array);

        if (!$order) {
            return false;
        }

        $this->order = $order;

        return $this->init_params();
    }

    /* ================================
     * 傳入os_id初始化
     * ================================ */

    public function init_order_by_os_id($os_id, $u_id, $trial_expire_time) {
        $where_array = array(
            "os_id" => $os_id
        );
        $order_store = $this->ci->Common_model->get_one("order_store", $where_array);

        if (!$order_store) {
            return false;
        }

        $where_array = array(
            "o_order_number IS NOT NULL",
            "o_order_number !=" => '',
            "o_payment_timestamp IS NOT NULL",
            "u_id" => $u_id,
            "(o_delivery_timestamp IS NULL OR o_delivery_timestamp > '{$trial_expire_time}')",
            "o_id" => $order_store->o_id
        );
        $order = $this->ci->Common_model->get_one("order", $where_array);

        if (!$order) {
            return false;
        }

        $this->order = $order;

        return $this->init_params();
    }

    /* ================================
     * 傳入rr_id初始化，通常為後台使用
     * 
     * 傳入owner_s_id過濾權限，未傳入時則一律取回
     * ================================ */

    public function init_order_by_rr_id($rr_id, $owner_s_id = false) {
        //取得refund_request
        $where_array = array(
            "rr_id" => $rr_id,
            "rr_status" => "REQUEST"
        );

        $refund_request = $this->ci->Common_model->get_one("refund_request", $where_array);

        if (!$refund_request) {
            return false;
        }

        //取得order_item
        $where_array = array(
            "oi_id" => $refund_request->oi_id
        );

        if ($owner_s_id) {
            $where_array["s_id"] = $owner_s_id;
        }

        $order_item = $this->ci->Common_model->get_one("order_item", $where_array);

        if (!$order_item) {
            return false;
        }

        //取得order

        $where_array = array(
            "o_order_number IS NOT NULL",
            "o_order_number !=" => '',
            "o_payment_timestamp IS NOT NULL",
            "o_id" => $order_item->o_id
        );
        $order = $this->ci->Common_model->get_one("order", $where_array);

        if (!$order) {
            return false;
        }

        $this->order = $order;

        return $this->init_params();
    }

    /* ================================
     * 取得order_item
     * ================================ */

    public function get_order_item() {
        return $this->order_item;
    }

    /* ================================
     * 取得order_store
     * 
     * 傳入：o_id, s_id
     * ================================ */

    public function get_order_store($o_id, $s_id) {
        foreach ($this->order_store as $row) {
            if ($row->o_id == $o_id && $row->s_id == $s_id) {
                return $row;
            }
        }

        return false;
    }

    /* ================================
     * 以oi_id取得商品
     * ================================ */

    public function get_order_item_by_oi_id($oi_id) {
        foreach ($this->order_item as $row) {
            if ($row->oi_id == $oi_id) {
                return $row;
            }
        }

        return false;
    }

    /* ================================
     * 以os_id取得商品
     * ================================ */

    public function get_order_item_by_os_id($os_id) {
        $order_item = array();
        foreach ($this->order_item as $row) {
            if ($row->s_id == $this->order_store[$os_id]->s_id) {
                $order_item[] = $row;
            }
        }

        return $order_item;
    }

    /* ================================
     * 以rr_id取得商品
     * ================================ */

    public function get_order_item_by_rr_id($rr_id) {
        $oi_id = $this->get_oi_id_from_rr_id($rr_id);

        foreach ($this->order_item as $row) {
            if ($row->oi_id == $oi_id) {
                return $row;
            }
        }

        return false;
    }

    /* ================================
     * 以oi_id取得p_id
     * ================================ */

    public function get_p_id_by_oi_id($oi_id) {
        foreach ($this->order_item as $row) {
            if ($row->oi_id == $oi_id) {
                return $row->p_id;
            }
        }

        return false;
    }

    /* ================================
     * 取得商品最大可退貨數量
     * 即 訂單購買數 - 處理中的退貨數
     * ================================ */

    public function get_max_refundable_num($oi_id) {
        $p_id = $this->get_p_id_by_oi_id($oi_id);

        $refundable_num = 0;

        //加總 購買數 和 已退貨數
        foreach ($this->order_item as $row) {
            if ($row->p_id == $p_id) {
                $refundable_num += $row->oi_qty;
            }
        }

        //計算尚在處理中的退貨數
        foreach ($this->refund_request as $row) {
            if ($row->oi_id == $oi_id && in_array($row->rr_status, $this->in_process_refund_status)) {
                $refundable_num -= $row->rr_qty;
            }
        }

        return $refundable_num;
    }

    /* ================================
     * 新增退貨商品 (店家訂單必須是"已出貨"狀態)
     * 
     * 傳入：oi_id：商品id
     *      num: 退貨數量
     *      reason: 退貨原因
     * 回傳：true or false
     * ================================ */

    public function add_refund_item_by_oi_id($oi_id, $num, $reason) {
        //退貨0件，不處理
        if ($num == 0) {
            return true;
        }

        //退貨數量超過限制
        if ($num > $this->get_max_refundable_num($oi_id)) {
            return false;
        }

        //若此商品已在待退貨商品清單列中，則為非法狀態
        if (isset($this->refund_item[$oi_id])) {
            return false;
        }

        //確定訂單商品存在
        $order_item = $this->get_order_item_by_oi_id($oi_id);
        if (!$order_item) {
            return false;
        }

        //驗證是否是已出貨狀態
        $os_id = $this->get_os_id($order_item->o_id, $order_item->s_id);

        if (!isset($this->order_store[$os_id])) {
            return false;
        }
        $order_store = $this->order_store[$os_id];

        if ($order_store->os_status_shipped !== 'Y') {
            return false;
        }

        $this->refund_item[$oi_id] = array(
            "oi_id" => (string) $oi_id,
            "refund_num" => $num,
            "refund_reason" => $reason,
            "rr_status" => "REQUEST"   //單商品退貨，進入申請中狀態
        );

        return true;
    }

    /* ================================
     * 新增退貨商品 (店家訂單必須是"未出貨"狀態)
     * 
     * 傳入：os_id：店家訂單id
     *      num: 退貨數量
     * 回傳：true or false
     * ================================ */

    public function add_refund_item_by_os_id($os_id, $reason) {
        $to_refund_item = $this->get_order_item_by_os_id($os_id);

        //驗證是否是未出貨狀態
        if (!isset($this->order_store[$os_id])) {
            return false;
        }
        $order_store = $this->order_store[$os_id];

        if ($order_store->os_status_shipped !== 'N') {
            return false;
        }

        foreach ($to_refund_item as $row) {
            //若此商品已在待退貨商品清單列中，則為非法狀態
            if (isset($this->refund_item[$row->oi_id])) {
                return false;
            }

            $this->refund_item[$row->oi_id] = array(
                "oi_id" => $row->oi_id,
                "refund_num" => $this->get_max_refundable_num($row->oi_id),
                "refund_reason" => $reason,
                "rr_status" => "CARGO_RETURN"   //整店家訂單退貨，進入待退款狀態
            );
        }

        return true;
    }

    /* ================================
     * 取得本次退貨清單
     * ================================ */

    public function get_refund_item() {
        return $this->refund_item;
    }

    /* ================================
     * 取得訂單
     * ================================ */

    public function get_order() {
        return $this->order;
    }

    /* ================================
     * 檢查取消退貨
     * 
     * 在此產生 to_cancel 與 to_refund 內容
     * 回傳值：要取消的refund_request
     * ================================ */

    private function check_cancel_refund() {
        foreach ($this->refund_item as $o_id => $row) {
            //確認是否有申請中的退貨商品
            $refund_request = $this->get_refund_request($o_id, "REQUEST");
            if ($refund_request) {
                //加入準備取消的清單
                $this->to_cancel[] = $refund_request;
            }
        }

        return $this->to_cancel;
    }

    /* ================================
     * 取消退貨寫入資料庫
     * 
     * 回傳取消退貨的refund_request清單
     * ================================ */

    public function write_db_cancel() {
        $this->check_cancel_refund();

        foreach ($this->to_cancel as $row) {
            $update_array = array(
                "rr_status" => 'CANCEL'
            );

            $where_array = array(
                'rr_id' => $row->rr_id,
                "rr_status" => 'REQUEST'
            );

            $this->ci->Common_model->update_db("refund_request", $update_array, $where_array);
        }

        //更新訂單資訊
        $this->write_db_update_order();

        return $this->to_cancel;
    }

    /* ================================
     * 整訂單退貨，返還庫存
     * 
     * 傳入：oi_id, qty
     * ================================ */

    public function write_db_return_inventory($oi_id, $qty) {
        $order_item = $this->get_order_item_by_oi_id($oi_id);
        if (!$order_item) {
            return false;
        }

        $update_array = array(
            "p_inventory" => "`p_inventory`+{$qty}",
            "p_sold_count" => "`p_sold_count`-{$qty}"
        );

        $where_array = array(
            "p_id" => $order_item->p_id
        );

        $this->ci->Common_model->update_db_field("product", $update_array, $where_array);
    }

    /* ================================
     * 新增退貨寫入資料庫
     * 
     * 回傳新增退貨的refund_request清單
     * ================================ */

    public function write_db_refund() {
        $now = date("Y-m-d H:i:s");
        foreach ($this->refund_item as $oi_id => $row) {
            $insert_array = array(
                "oi_id" => $oi_id,
                "rr_qty" => $row["refund_num"],
                "rr_reason" => $row["refund_reason"],
                "rr_status" => $row["rr_status"],
                "rr_timestamp" => $now
            );

            if ($row["rr_status"] == "CARGO_RETURN") {
                //整訂單退貨，自動進入貨物已返還狀態
                $insert_array["rr_store_reply"] = "(未出貨訂單，系統自動處理)";
                $insert_array["rr_store_reply_timestamp"] = $now;
                $insert_array["rr_cargo_return_timestamp"] = $now;
            }

            $this->ci->Common_model->insert_db("refund_request", $insert_array);

            if ($row["rr_status"] == "CARGO_RETURN") {
                //整訂單退貨，要處理訂單、訂單商品、店家商品資料庫紀錄
                $this->write_db_order_item_refund($oi_id, $row["refund_num"], $row["rr_status"]);

                //整訂單退貨，表示是未出貨即取消，則庫存返還
                $this->write_db_return_inventory($oi_id, $row["refund_num"]);
            }
        }

        //更新訂單資訊
        $this->write_db_update_order();

        return $this->refund_item;
    }

    /* ================================
     * 退貨商品以負數(-)新增寫入資料庫
     * 
     * 傳入oi_id、qty(退貨數量)
     * ================================ */

    public function write_db_order_item_refund($oi_id, $qty, $rr_status) {
        //訂單商品資料更新
        $order_item = $this->get_order_item_by_oi_id($oi_id);
        if (!$order_item) {
            return false;
        }

        //複製order_item欄位(特定欄位不複製)
        $not_clone_fields = array(
            "oi_id", "oi_qty", "oi_sub_total", "oi_timestamp", "oi_options"
        );
        $insert_array = array();

        foreach ($order_item as $key => $value) {
            if (!in_array($key, $not_clone_fields)) {
                $insert_array[$key] = $value;
            }
        }

        $insert_array["oi_qty"] = -1 * abs($qty);
        $insert_array["oi_sub_total"] = $insert_array["oi_qty"] * $order_item->oi_price;

        if ($rr_status == 'CARGO_RETURN') {
            //整訂單取消，記錄並統計取消數量
            $oi_options = json_decode($order_item->oi_options);
            $oi_options["cancel_count"] = isset($oi_options["cancel_count"]) ? $oi_options["cancel_count"] + $qty : $qty;
            $insert_array["oi_options"] = json_encode($oi_options);
        } else {
            $insert_array["oi_options"] = $order_item->oi_options;
        }

        $this->ci->Common_model->insert_db("order_item", $insert_array);

        return $this->ci->Common_model->get_insert_id();
    }

    /* ================================
     * 更新訂單及店家訂單
     * ================================ */

    public function write_db_update_order() {
        //統計退貨金額
        $order_refund = 0;    //訂單退貨金額
        $shipment = 0;
        foreach ($this->order_store as $os) {
            $order_store_refund = 0;
            //取得退貨資料
            $where_array = array(
                "o_id" => $os->o_id,
                "s_id" => $os->s_id,
                "oi_qty <" => 0
            );

            $refund_order_item = $this->ci->Common_model->get_db("order_item", $where_array);

            if ($refund_order_item) {
                foreach ($refund_order_item as $row) {
                    $refund_amount = abs($row->oi_qty) * $row->oi_price;

                    $order_store_refund += $refund_amount;  //加總商店訂單退貨總價
                    $order_refund += $refund_amount;        //加總訂單退貨總價
                }
            }
            //更新店家訂單
            $update_array = array(
                "os_refund_amount" => $order_store_refund
            );

            if ($order_store_refund == $os->os_item_amount) {
                //退價金額=商品金額，表示全部退了，要把運費取消
                $update_array["os_shipment"] = 0;
                $update_array["os_total_amount"] = $os->os_item_amount;
            }

            $where_array = array(
                "os_id" => $os->os_id
            );
            $this->ci->Common_model->update_db("order_store", $update_array, $where_array);

            if ($order_store_refund != $os->os_item_amount) {
                $shipment += $os->os_shipment;
            }
        }

        //訂單金額更新
        $where_array = array(
            "o_id" => $os->o_id
        );

        $update_array = array(
            "o_refund_amount" => $order_refund,
            "o_shipment" => $shipment,
            "o_total_amount" => $this->order->o_item_amount + $shipment
        );

        $this->ci->Common_model->update_db("order", $update_array, $where_array);
    }

    /* ================================
     * 取得有異動的oi_id
     * ================================ */

    public function get_oi_id_from_rr_id($rr_id) {
        foreach ($this->refund_request as $row) {
            if ($row->rr_id == $rr_id) {
                return $row->oi_id;
            }
        }

        return false;
    }

    /* ================================
     * 取得有異動的oi_id
     * ================================ */

    public function get_modify_oi_ids() {
        $modify_oi_ids = array();

        //記錄有刪除的oi_id
        foreach ($this->to_cancel as $row) {
            if (!in_array($row->oi_id, $modify_oi_ids)) {
                $modify_oi_ids[] = $row->oi_id;
            }
        }

        //記錄新增的refund_request
        foreach ($this->refund_item as $oi_id => $row) {
            if (!in_array($oi_id, $modify_oi_ids)) {
                $modify_oi_ids[] = $oi_id;
            }
        }

        return $modify_oi_ids;
    }

    /* ================================
     * 已出貨店家總數
     * ================================ */

    public function total_shipped_store() {
        $count = 0;

        foreach ($this->order_store as $row) {
            if ($row->os_status_shipped == 'Y') {
                $count++;
            }
        }

        return $count;
    }

    /* ================================
     * 店家總數
     * ================================ */

    public function total_order_store() {
        return count($this->order_store);
    }

    /* ================================
     * 是否有未出貨、可退款的商品
     * 
     * 回傳值：true or false
     * ================================ */

    public function should_return_cash() {
        foreach ($this->refund_item as $row) {
            if ($row["rr_status"] == "CARGO_RETURN") {
                return true;
            }
        }

        return false;
    }

    /* ================================
     * 退貨申請店家審查結果寫入資料庫
     * ================================ */

    public function refund_request_store_reply($rr_id, $rr_status, $rr_store_reply) {
        $update_array = array(
            "rr_status" => $rr_status,
            "rr_store_reply" => $rr_store_reply,
            "rr_store_reply_timestamp" => date("Y-m-d H:i:s")
        );

        $where_array = array(
            'rr_id' => $rr_id,
            "rr_status" => 'REQUEST'
        );

        $this->ci->Common_model->update_db("refund_request", $update_array, $where_array);

        $refund_oi_id = false;

        if ($rr_status == 'AGREE') {
            $oi_id = $this->get_oi_id_from_rr_id($rr_id);
            $refund_request = $this->get_refund_request($oi_id, 'REQUEST');

            //新增一筆負數的order_item
            $refund_oi_id = $this->write_db_order_item_refund($oi_id, $refund_request->rr_qty, $rr_status);

            //更新訂單資訊
            $this->write_db_update_order();
        }

        return $refund_oi_id;
    }

    /* ================================
     * 中止退貨
     * 
     * 
     * ================================ */

    public function refund_stop($rr_id) {
        $refund_request = $this->get_refund_request_by_rr_id($rr_id);

        if (!$refund_request) {
            return false;
        }

        //找出原始order_item
        $order_item = $this->get_order_item_by_oi_id($refund_request->oi_id);

        if (!$order_item) {
            return false;
        }

        //找出相對應的退貨紀錄
        $refund_order_item = $this->get_order_item_by_parm($order_item->p_id, -1 * abs($refund_request->rr_qty));

        if ($refund_order_item) {
            //退貨商品紀錄還在，要刪除
            $where_array = array(
                'oi_id' => $refund_order_item->oi_id
            );

            $this->ci->Common_model->delete_db("order_item", $where_array);

            //更新訂單資訊
            $this->write_db_update_order();
        }

        //refund_request狀態改為拒絕，並加註說明文字
        $now = date("Y-m-d H:i:s");

        $update_array = array(
            "rr_status" => "DENY",
            "rr_store_reply" => $refund_request->rr_store_reply . "\n\n({$now}: 本商品無法接受退貨)"
        );

        $where_array = array(
            'rr_id' => $rr_id
        );

        $this->ci->Common_model->update_db("refund_request", $update_array, $where_array);
    }

}
