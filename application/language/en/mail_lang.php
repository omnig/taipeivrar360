<?php

include FCPATH . "application/language/general/mail_lang.php";

$lang['%s 您好:'] = "Hello %s,";
$lang['register_content'] = "Your account for %s has been established successfully.<br /><br />" .
        "Please click on the link listed blow to activate:<br /><br />" .
        "<a href='%s'>%s</a><br /><br />" .
        "Best Regards<br /><br />" .
        "%s";
$lang['reset_password_content'] = "You required to reset password for %s account.<br /><br />" .
        "Please click on the link listed blow to reset your password:<br /><br />" .
        "<a href='%s'>%s</a><br /><br />" .
        "Best Regards<br /><br />" .
        "%s";
$lang['contact_reply'] = "You comment at %s :<br /><br />" .
        "%s<br />" .
        "%s<br /><br />" .
        "Here is reply:<br /><br />" .
        "%s<br /><br />" .
        "Please feel free to submit any comment to us.<br /><br />" .
        "Best Regards<br /><br />" .
        "%s";
$lang['mail_change'] = "Your account for %s has been established successfully.<br /><br />" .
        "Please click on the link listed blow to activate:<br /><br />" .
        "<a href='%s'>%s</a><br /><br />" .
        "Best Regards<br /><br />" .
        "%s";

$lang["【嬉街商城】已完成EMAIL認證，獲得 %s"] = "[CJ] Email certification has been completed, and you get %s";

$lang["【嬉街商城】帳號修改成功！"] = "[CJ] Profile Changed";
$lang['【嬉街商城】訂單已收到 %s'] = "Order Received (Order Number: %s)";
$lang["【嬉街商城】退貨申請已收到"] = "[CJ] Refund Request Received";

$lang["提醒您：退貨若造成商品未達免運標準，將可能再收取一筆運費。"] = "Note: In some cases, refund will charge an extra shipment";
$lang["您的退貨資訊如下："] = "Here the refund information:";
