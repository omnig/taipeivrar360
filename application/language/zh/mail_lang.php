<?php

include FCPATH . "application/language/general/mail_lang.php";

$lang['%s 您好'] = "%s 您好：";
$lang['register_content'] = "您在 %s 的帳號已建立成功！<br /><br />" .
        "請點擊以下連結以啟用您的帳號：<br /><br />" .
        "<a href='%s'>%s</a><br /><br />" .
        "謝謝<br /><br />" .
        "%s 敬上";
$lang['reset_password_content'] = "您要求重設您在 %s 的密碼！<br /><br />" .
        "請點擊以下連結以進行密碼重設：<br /><br />" .
        "<a href='%s'>%s</a><br /><br />" .
        "謝謝<br /><br />" .
        "%s 敬上";
$lang['contact_reply'] = "您在 %s 提供意見：<br /><br />" .
        "%s<br />" .
        "%s<br /><br />" .
        "回覆如下：<br /><br />" .
        "%s<br /><br />" .
        "如有疑問，歡迎隨時與我們聯繫。<br /><br />" .
        "%s 敬上";
$lang['application_reply'] = "您在 %s 線上申請：<br /><br />" .
        "公司名稱：%s<br />" .
        "統一編號：%s<br />" .
        "聯 絡 人：%s<br />" .
        "聯絡電話：%s<br />" .
        "地址：%s<br /><br />" .
        "回覆如下：<br /><br />" .
        "%s<br /><br /><br />" .
        "如有疑問，歡迎隨時與我們聯繫。<br /><br />" .
        "%s 敬上";
$lang['mail_change'] = "您在 %s 的帳號已修改成功！<br /><br />" .
        "請點擊以下連結以啟用您的帳號：<br /><br />" .
        "<a href='%s'>%s</a><br /><br />" .
        "謝謝<br /><br />" .
        "%s 敬上";

$lang["【嬉街商城】已完成EMAIL認證，獲得 %s"] = "【嬉街商城】已完成EMAIL認證，獲得 %s";

$lang["【嬉街商城】帳號修改成功！"] = "【嬉街商城】帳號修改成功！";
$lang['【嬉街商城】訂單已收到 %s'] = "【嬉街商城】訂單已收到 (訂單編號：%s)";
$lang["【嬉街商城】退貨申請已收到"] = "【嬉街商城】退貨申請已收到";

$lang["提醒您：退貨若造成商品未達免運標準，將可能再收取一筆運費。"] = "提醒您：退貨可能另外收取一筆運費。";
$lang["您的退貨資訊如下："] = "您的退貨資訊如下：";
