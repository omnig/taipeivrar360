<?php

/*
 * 不分語系共用檔
 * 以 # 開頭，與其它語系key區隔
 */

$lang["#親愛的 %s 您好:"] = array(
    "zh" => "親愛的 %s 您好：",
    "en" => "Hi, %s,"
);

$lang["#親愛的 管理者 您好:"] = array(
    "zh" => "親愛的 管理者 您好：",
    "en" => "Hi, Manager,"
);

$lang["#您在 %s 訂購的商品已經出貨，近期請注意收件通知。"] = array(
    "zh" => "您在 %s 訂購的商品已經出貨，近期請注意收件通知。",
    "en" => "The product(s) you ordered from %s has been delivered."
);

$lang["#您在 %s 訂購的商品，部份已經出貨，近期請注意收件通知。"] = array(
    "zh" => "您在 %s 訂購的商品，部份已經出貨，近期請注意收件通知。",
    "en" => "Part of the product(s) you ordered from %s has been delivered."
);

$lang["#訂單編號"] = array(
    "zh" => "訂單編號",
    "en" => "Order Number"
);

$lang["#商品總價"] = array(
    "zh" => "商品總價",
    "en" => "Product(s) Amount"
);

$lang["#運費"] = array(
    "zh" => "運費",
    "en" => "Shipping Fee"
);

$lang["#訂單總價"] = array(
    "zh" => "訂單總價",
    "en" => "Total Amount"
);

$lang["#訂購時間"] = array(
    "zh" => "訂購時間",
    "en" => "Order Time"
);

$lang["#出貨時間"] = array(
    "zh" => "出貨時間",
    "en" => "Delivery Time"
);

$lang["#商品數量"] = array(
    "zh" => "商品數量",
    "en" => "Product(s) QTY"
);

$lang["#收件人姓名"] = array(
    "zh" => "收件人姓名",
    "en" => "Recipient Name"
);

$lang["#收件人地址"] = array(
    "zh" => "收件人地址",
    "en" => "Recipient Address"
);

$lang["#收件人電話"] = array(
    "zh" => "收件人電話",
    "en" => "Recipient Phone"
);

$lang["#公司統一編號"] = array(
    "zh" => "公司統一編號",
    "en" => "Company Tax ID"
);

$lang["#項次"] = array(
    "zh" => "項次",
    "en" => "Item"
);

$lang["#商品"] = array(
    "zh" => "商品",
    "en" => "Product Name"
);

$lang["#款式"] = array(
    "zh" => "款式",
    "en" => "Style"
);

$lang["#單價"] = array(
    "zh" => "單價",
    "en" => "Price"
);

$lang["#數量"] = array(
    "zh" => "數量",
    "en" => "QTY"
);

$lang["#小計"] = array(
    "zh" => "小計",
    "en" => "Sub Total"
);

$lang["#【嬉街商城】出貨通知 %s"] = array(
    "zh" => "【嬉街商城】出貨通知 (訂單編號：%s)",
    "en" => "[CJ] Delivery Notification (Order Number: %s)"
);

$lang["#【嬉街商城】新訂單通知 %s"] = array(
    "zh" => "【嬉街商城】新訂單通知 (訂單編號：%s)",
    "en" => "[CJ] New Order Notification (Order Number: %s)"
);

$lang["#您從 %s 接到一筆新訂單。"] = array(
    "zh" => "您從 %s 接到一筆新訂單。",
    "en" => "You got a new order from %s."
);

$lang["#您從 %s 收到一筆退貨申請單。"] = array(
    "zh" => "您從 %s 收到一筆退貨申請單。",
    "en" => "You got a refund request from %s."
);

$lang["#您從 %s 收到一筆取消退貨通知。"] = array(
    "zh" => "您從 %s 收到一筆取消退貨通知。",
    "en" => "You got a refund request canceled from %s."
);

$lang["#您從 %s 收到庫存不足通知信。"] = array(
    "zh" => "您從 %s 收到庫存不足通知信。",
    "en" => "You got a product inventory shortage alert from %s."
);

$lang["#您在 %s 送出的訂單已經收到了。"] = array(
    "zh" => "您在 %s 送出的訂單已經收到了。",
    "en" => "The order you submitted from %s is received."
);

$lang["#退貨數量"] = array(
    "zh" => "退貨數量",
    "en" => "Refund QTY"
);

$lang["#付款方式"] = array(
    "zh" => "付款方式",
    "en" => "Payment Method"
);

$lang["#【嬉街商城】訂單帳款已收到 %s"] = array(
    "zh" => "【嬉街商城】訂單帳款已收到 %s",
    "en" => "[CJ] Payment Received %s"
);

$lang["#您在 %s 付款已經收到了。"] = array(
    "zh" => "您在 %s 付款已經收到了。",
    "en" => "Payment to %s is received."
);

$lang["#出貨店家"] = array(
    "zh" => "出貨店家",
    "en" => "From Store"
);

$lang["#防詐騙提醒"] = array(
    "zh" => "【防詐騙提醒】<br />若您接獲任何電話要您依照指示操作ATM，提供餘額、變更付款方式或更改定期設定等，請不要依電話指示至ATM操作，並建議您可撥打165防詐騙專線。",
    "en" => "【Anti-fraud Notice】<br />Please do not operate ATM  under instructions of other people in the ways of providing balance, changing payment methods and the setting of systematic investment plan etc.. If any circumstance  above is confronted, please call the number 165, the Anti-fraud line."
);

$lang["#嬉街商城【訂單成立】通知信"] = array(
    "zh" => "嬉街商城【訂單成立】通知信",
    "en" => "CJ Shopping【Order Received】Information"
);

$lang["#嬉街商城【新訂單】通知信"] = array(
    "zh" => "嬉街商城【新訂單】通知信",
    "en" => "CJ Shopping【New Order】Information"
);

$lang["#嬉街商城【已出貨】通知信"] = array(
    "zh" => "嬉街商城【已出貨】通知信",
    "en" => "CJ Shopping【Order Delivered】Information"
);

$lang["#嬉街商城【部份出貨】通知信"] = array(
    "zh" => "嬉街商城【部份出貨】通知信",
    "en" => "CJ Shopping【Part of Order Delivered】Information"
);

$lang["#嬉街商城【已付款】通知信"] = array(
    "zh" => "嬉街商城【已付款】通知信",
    "en" => "CJ Shopping【Payment Received】Information"
);

$lang["#嬉街商城【退貨異動申請】通知信"] = array(
    "zh" => "嬉街商城【退貨異動申請】通知信",
    "en" => "CJ Shopping【Refund Canceled】Information"
);

$lang["#嬉街商城【取消退貨】通知信"] = array(
    "zh" => "嬉街商城【取消退貨】通知信",
    "en" => "CJ Shopping【Refund Canceled】Information"
);

$lang["#嬉街商城【商品問與答】通知信"] = array(
    "zh" => "嬉街商城【商品問與答】通知信",
    "en" => "CJ Shopping【Product Q&A】Information"
);

$lang["#買家已使用信用卡付款，請於<b>七個工作天內</b>，將買家訂購的商品送達，謝謝您。"] = array(
    "zh" => "買家已使用信用卡付款，請於<b>七個工作天內</b>，將買家訂購的商品送達，謝謝您。",
    "en" => "The payment is paid by credit card, please deliver the product(s) <b>in 7 working days</b>, Thank you."
);

$lang["#買家已使用ATM付款，請於<b>七個工作天內</b>，將買家訂購的商品送達，謝謝您。"] = array(
    "zh" => "買家已使用ATM付款，請於<b>七個工作天內</b>，將買家訂購的商品送達，謝謝您。",
    "en" => "The payment is paid by ATM, please deliver the product(s) <b>in 7 working days</b>, Thank you."
);

$lang["#感謝您對 %s 的支持，您已於 %s 完成商品訂購，使用「%s」之訂單已成立。"] = array(
    "zh" => "感謝您對 %s 的支持，您已於 %s 完成商品訂購，使用「%s」之訂單已成立。",
    "en" => "Thank you for using %s. Your order is created on %s. Payment method is %s"
);

$lang["#感謝您對 %s 的支持，已收到您的取消退貨通知。"] = array(
    "zh" => "感謝您對 %s 的支持，已收到您的取消退貨通知。",
    "en" => "Thank you for using %s. Your refund request is already canceled."
);

$lang["#感謝您對 %s 的支持，您已於 %s 完成退貨申請，已通知店家進行確認作業，請耐心等候。"] = array(
    "zh" => "感謝您對 %s 的支持，您已於 %s 完成退貨申請，已通知店家進行確認作業，請耐心等候。",
    "en" => "Thank you for using %s. You had done to send refund request on %s. We already notify the store admin to confirm. Please be patient."
);

$lang["#請於<span style='font-size:18px'><b>%d日內</b></span>完成轉帳付款，若未付款系統將自動取消訂單。"] = array(
    "zh" => "請於<span style='font-size:18px'><b>%d日內</b></span>完成轉帳付款，若未付款系統將自動取消訂單。",
    "en" => "Please complete the payment transfer in <span style='font-size:18px'><b>%d days</b></span>, or the order will be canceled automatically."
);

$lang["#您的訂購資料如下："] = array(
    "zh" => "您的訂購資料如下：",
    "en" => "Here's the order detail:"
);

$lang["#訂單資料"] = array(
    "zh" => "訂單資料",
    "en" => "Order Detail"
);

$lang["#出貨資訊"] = array(
    "zh" => "出貨資訊",
    "en" => "Order Delivered Infomation"
);

$lang["#商品明細"] = array(
    "zh" => "商品明細",
    "en" => "Product Detail"
);

$lang["#出貨明細"] = array(
    "zh" => "出貨明細",
    "en" => "Order Delivered Detail"
);

$lang["#訂單總金額"] = array(
    "zh" => "訂單總金額",
    "en" => "Total Amount"
);

$lang["#退貨資訊"] = array(
    "zh" => "退貨資訊",
    "en" => "Refund Infomation"
);

$lang["#原退貨數"] = array(
    "zh" => "原退貨數",
    "en" => "Refunded QTY"
);

$lang["#退貨狀態"] = array(
    "zh" => "退貨狀態",
    "en" => "Refunded Status"
);

$lang["#取消退貨"] = array(
    "zh" => "取消退貨",
    "en" => "Refund Canceled"
);

$lang["#店家回覆"] = array(
    "zh" => "店家回覆",
    "en" => "Reply Information"
);

$lang["#商品名稱"] = array(
    "zh" => "商品名稱",
    "en" => "Item Name"
);

$lang["#附記"] = array(
    "zh" => "附記",
    "en" => "Note"
);

$lang["#【嬉街商城】取消退貨通知單"] = array(
    "zh" => "【嬉街商城】取消退貨通知單",
    "en" => "[CJ] Cancel Refund Notification"
);

$lang["#【嬉街商城】退貨通知單"] = array(
    "zh" => "【嬉街商城】退貨通知單",
    "en" => "[CJ] Refund Notification"
);

$lang['#嬉街商城【退貨申請】通知信'] = array(
    "zh" => "嬉街商城【退貨申請】通知信",
    "en" => "[CJ] Refund Notification"
);

$lang['#成立時間'] = array(
    "zh" => "成立時間",
    "en" => "Process Time"
);

$lang["#【嬉街商城】同意「%s」辦理退貨"] = array(
    "zh" => "【嬉街商城】同意「%s」辦理退貨",
    "en" => "[CJ] Agree \"%s\" Refund"
);

$lang["#【嬉街商城】不同意「%s」辦理退貨"] = array(
    "zh" => "【嬉街商城】不同意「%s」辦理退貨",
    "en" => "[CJ] Disagree \"%s\" Refund"
);

$lang["#【嬉街商城】拒絕退貨通知單"] = array(
    "zh" => "【嬉街商城】拒絕退貨通知單",
    "en" => "[CJ] Disagree Refund Notification"
);

$lang["#【嬉街商城】退貨商品確認無誤"] = array(
    "zh" => "【嬉街商城】退貨商品確認無誤",
    "en" => "[CJ] Refund Item Is Confirmed"
);

$lang["#退貨商品： %s 確認無誤"] = array(
    "zh" => "退貨商品： %s 確認無誤",
    "en" => "Refund Item: \"%s\" Is Confirmed"
);

$lang["#退貨商品： %s 已退款"] = array(
    "zh" => "退貨商品： %s 已退款",
    "en" => "Refund Complete For Item: \"%s\""
);

$lang["#以上您申請退貨的商品，經檢查確認無誤，已進入退款流程"] = array(
    "zh" => "以上您申請退貨的商品，經檢查確認無誤，已進入退款流程",
    "en" => "The item you require to refund has been confirmed. We're ready to refund."
);

$lang["#【嬉街商城】退款通知單"] = array(
    "zh" => "【嬉街商城】退款通知單",
    "en" => "[CJ] OK To Refund Notification"
);

$lang["#【嬉街商城】退貨商品已退款"] = array(
    "zh" => "【嬉街商城】退貨商品已退款",
    "en" => "[CJ] Refund Complete"
);

$lang["#以上您申請退貨的商品已退款，款項在3個工作天內就會匯入您指定的帳戶"] = array(
    "zh" => "以上您申請退貨的商品已退款，款項在3個工作天內就會匯入您指定的帳戶",
    "en" => "The refund is completed and will be transfered to the bank account you assigned in 3 working days."
);

$lang["#【嬉街商城】收到取消退貨申請"] = array(
    "zh" => "【嬉街商城】收到取消退貨申請",
    "en" => "[CJ] Cancel Refund Received"
);

$lang["#【嬉街商城】訂單已逾期: %s"] = array(
    "zh" => "【嬉街商城】訂單已逾期: %s",
    "en" => "[CJ] Order has expired: %s"
);

$lang["#【嬉街商城】庫存不足通知"] = array(
    "zh" => "【嬉街商城】庫存不足通知",
    "en" => "[CJ] Product Inventory Shortage"
);

$lang["#嬉街商城【庫存不足】通知信"] = array(
    "zh" => "嬉街商城【庫存不足】通知信",
    "en" => "CJ Shopping【Inventory Shortage】Infomation"
);

$lang["#嬉街商城【訂單逾期】通知信"] = array(
    "zh" => "嬉街商城【訂單逾期】通知信",
    "en" => "CJ Shopping【Order Has Expired】Information"
);

$lang["#您的訂單(編號：%s)已逾付款期限，訂單已取消，請勿逕行付款，若您仍要購買訂單商品，請重新訂購。"] = array(
    "zh" => "您的訂單(編號：%s)已逾付款期限，訂單已取消，請勿逕行付款，若您仍要購買訂單商品，請重新訂購。",
    "en" => "The order (S/N: %s) has EXPIRED and canceled. Please DO NOT pay for it. Please re-order the items if you still want them."
);

$lang["#【嬉街商城】訂單即將逾期: %s"] = array(
    "zh" => "【嬉街商城】訂單即將逾期: %s",
    "en" => "[CJ] Order will be expired soon: %s"
);

$lang["#嬉街商城【訂單即將逾期】通知信"] = array(
    "zh" => "嬉街商城【訂單即將逾期】通知信",
    "en" => "CJ Shopping【Order will be expired】Information"
);

$lang["#您的訂單(編號：%s)即將逾付款期限，請儘速完成付款，逾期將不再為您保留。"] = array(
    "zh" => "您的訂單(編號：%s)即將逾付款期限，請儘速完成付款，逾期將不再為您保留。",
    "en" => "The order will be expired soon(S/N: %s). Please finish the payment as soon as possible. The inventory will not keep if the order is expired."
);


$lang["#【嬉街商城】尚未出貨通知: %s"] = array(
    "zh" => "【嬉街商城】尚未出貨通知: %s",
    "en" => "[CJ] Order no shipments"
);

$lang["#【嬉街商城】商品問與答"] = array(
    "zh" => "【嬉街商城】商品問與答",
    "en" => "[CJ] Product Q&A"
);

$lang["#嬉街商城【尚未出貨】通知信"] = array(
    "zh" => "嬉街商城【尚未出貨】通知信",
    "en" => "[CJ] Order no shipments"
);

$lang["#您的訂單編號"] = array(
    "zh" => "您的訂單編號",
    "en" => "Your Order Number"
);

$lang["#買家已完成付款起算第 %s 天，尚未接獲出貨通知，請儘速處理您的訂單，謝謝。"] = array(
    "zh" => "買家已完成付款起算第 %s 天，尚未接獲出貨通知，請儘速處理您的訂單，謝謝。",
    "en" => "The buyer has completed payment for %s days and has not received shipment notification, please process your order as soon as possible, thank you."
);

$lang["#※若您已將商品寄送，請登入後台並設定該筆訂單編號狀態為「出貨」※"] = array(
    "zh" => "※若您已將商品寄送，請登入後台並設定該筆訂單編號狀態為「出貨」※",
    "en" => "If you have goods sent, please sign in the SHOP ADMIN SYS and set shipment status of the order to 'delivered'"
);

$lang["#買家已完成付款起算第 %s 天，尚未接獲出貨通知，請注意店家出貨情形，謝謝。"] = array(
    "zh" => "買家已完成付款起算第 %s 天，尚未接獲出貨通知，請注意店家出貨情形，謝謝。",
    "en" => "The buyer has completed payment for %s days and has not received shipment notification, please notic this order number, thank you."
);
