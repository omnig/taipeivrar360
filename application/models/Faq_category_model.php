<?php

class Faq_category_model extends CI_Model {

    protected $readonly_db;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    /* ==================================
     * 取得列表
     * ================================== */

    function get_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('fc.*');
        $this->readonly_db->from('faq_category AS fc');
        
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                'fc_title_zh', 'fc_title_en'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

    /* ==================================
     * 取得次分類
     * ================================== */

    function get_sub_category($where_array = array()) {
        $this->readonly_db->select('*');
        $this->readonly_db->from('product_sub_category AS psc');

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        $this->readonly_db->order_by("psc_order ASC,psc_title_{$this->session->i18n} ASC");

        $query = $this->readonly_db->get();
        return $query->result();
    }

    /* ==================================
     * 取得樹狀結構
     * ================================== */

    function get_tree_list($where_array = array()) {
        $this->readonly_db->select('*');
        $this->readonly_db->from('product_category AS pc');

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        $this->readonly_db->order_by("pc_order ASC,pc_title_{$this->session->i18n} ASC");

        $query = $this->readonly_db->get();

        $rtn = $query->result();

        if (!$rtn) {
            return false;
        }

        //串接次分類
        foreach ($rtn as $id => $row) {
            $where_array = array(
                "pc_id" => $row->pc_id
            );

            $rtn[$id]->psc = $this->get_sub_category($where_array);
        }

        return $rtn;
    }

    /* ==================================
     * 取得指定商品p_id的分類
     * ================================== */

    function get_product_category_relation($p_id = 0) {
        //取得主分類清單
        $this->readonly_db->select('pc.*,psc.*,pc.pc_id AS pc_id');
        $this->readonly_db->from('product_category_relation AS pcr');
        $this->readonly_db->join("product_category AS pc", "pcr.pc_id=pc.pc_id", "LEFT");
        $this->readonly_db->join("product_sub_category AS psc", "pcr.psc_id=psc.psc_id", "LEFT");

        $this->readonly_db->where("p_id", $p_id);

        $query = $this->readonly_db->get();

        return $query->result();
    }

}
