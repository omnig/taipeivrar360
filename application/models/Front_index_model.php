<?php

class Front_index_model extends CI_Model {

    protected $readonly_db;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    /* ==================================
     * 取得前端首頁市民分享區照片、影片列表 (未登入)
     * ================================== */

    function get_vr_topics_unlogin(){
        $return_data = array();
        $vr_photo_list = array();
        $vr_photo_result = array();
        $vr_video_list = array();
        $vr_video_result = array();

        $this->readonly_db->select('t.d_id,t.dvp_id,t.dvp_name,t.Is_url,t.is_civil,t.dvp_rep_img_path,t.dvp_backup,t.dvp_view_link,t.dvp_capture_timestamp,u.u_name as u_name,a.ag_name as a_name');

        $this->readonly_db->from('department_vr_photo as t');
        $this->readonly_db->join("user as u", "u.u_id = t.d_id AND t.is_civil = 'Y'", "left");
        $this->readonly_db->join("admin_group as a", "a.ag_id = t.d_id AND t.is_civil != 'Y'", "left");

        $where_array = array(
            'dvp_view_link !=' => NULL,
            'dvp_state' => 'Y',
            'dvp_del' => 'N'
        );
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }
        $this->readonly_db->order_by("dvp_validation_timestamp","DESC");
        $query = $this->readonly_db->get();
        if($query){
            $vr_photo_list = $query->result();
        }else{
            $vr_photo_list = NULL;
        }

        // 預存要搜尋的Keyword
        $keywords_value = array();
        $keywords_key = array();
        //整理資料
        foreach ($vr_photo_list as $key => $value) {
            array_push($keywords_key, $value->dvp_id);
            $keywords_value[$value->dvp_id] = array();

            $vr_photo_result[$key]['id'] =  $value->dvp_id;
            $vr_photo_result[$key]['name'] =  $value->dvp_name;
            $vr_photo_result[$key]['photo'] =  $this->config->item('server_base_url').$value->dvp_rep_img_path;

            //判斷照片為環景或全景 URL異動
            if (isset($value->photo_type) && $value->photo_type=="pano") {
            //     //環景圖
            //     $TryStrpos=strpos($value->dvp_view_link,"http");

            //     if($TryStrpos === false){
            //         $vr_photo_result[$key]['url'] =  $this->config->item('api_base_url').'/kr_spherical?image_path='.$this->config->item('api_base_url').$value->dvp_backup;
            //     }else{
                    $vr_photo_result[$key]['url'] =  $value->dvp_view_link;

            //     }
            //         $vr_photo_result[$key]['mobile_url'] =  $this->config->item('api_base_url').'/kr_spherical?image_path='.$this->config->item('api_base_url').$value->dvp_backup;
            // }else{
            //     //全景圖
            //     $vr_photo_result[$key]['url'] =  $this->config->item('server_base_url').'/kr_pano?image_path='.$this->config->item('server_base_url').$value->dvp_backup;
                    $vr_photo_result[$key]['mobile_url'] =  $this->config->item('server_base_url').'/kr_spherical?image_path='.$this->config->item('server_base_url').$value->dvp_backup;
            }else{
                    $vr_photo_result[$key]['url'] =  $value->dvp_view_link;
                    $vr_photo_result[$key]['mobile_url'] =  $this->config->item('server_base_url').'/kr_pano?image_path='.$this->config->item('server_base_url').$value->dvp_backup;
            }

            //直接吃連結
            if($value->Is_url=="Y"){
                $vr_photo_result[$key]['url'] = $value->dvp_view_link;
                $vr_photo_result[$key]['mobile_url'] = $value->dvp_view_link;
            }
            
            $vr_photo_result[$key]['capture_time'] =  date('d M Y H:i',strtotime($value->dvp_capture_timestamp))." UTC";

            $vr_photo_result[$key]['like'] = NULL;


            //判斷是為局處或是民眾  使用者名稱異動
            if ($value->is_civil=="Y") {
                //民眾
                    $vr_photo_result[$key]['uploader'] =  $value->u_name;
            }else{
                //局處
                    $vr_photo_result[$key]['uploader'] =  $value->a_name;
            }

            
        }
        

        //環景KeyWord 
        $where_array = array(
            "dvp_id" => $keywords_key
        );

        // //get_key_word
        $keywords = $this->Common_model->get_db_join("keyword_department_vr_photo as dvp", $where_array , "keywords as ks" , "dvp.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");

        foreach ($keywords as $kwd_key => $kwdvalue) {
            if ($kwdvalue->kwd_name!="") {
                array_push($keywords_value[$kwdvalue->dvp_id], $kwdvalue->kwd_name);
            }
        }

        // 附在輸出
        foreach ($vr_photo_list as $key => $value) {
            $vr_photo_result[$key]['keywords'] = $keywords_value[$value->dvp_id];
        }


        $this->readonly_db->select('t.d_id,t.dvv_id,t.dvv_name,t.dvv_rep_img_path,t.dvv_youtube_link,t.dvv_capture_timestamp,t.is_civil,t.Is_url,u.u_name as u_name,a.ag_name as a_name');
        $this->readonly_db->from('tp_department_vr_video as t');
        $this->readonly_db->join("user as u", "u.u_id = t.d_id AND t.is_civil = 'Y'", "left");
        $this->readonly_db->join("admin_group as a", "a.ag_id = t.d_id AND t.is_civil != 'Y'", "left");
        $where_array = array(
                'dvv_state' => 'Y',
                'dvv_del' => 'N'
        );
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }
        $this->readonly_db->order_by("dvv_validation_timestamp","DESC");
        $query = $this->readonly_db->get();
        if($query){
            $vr_video_list = $query->result();
        }else{
            $vr_video_list = NULL;
        }


        $keywords_value = array();
        $keywords_key = array();
        //整理資料
        foreach ($vr_video_list as $key => $value) {
            array_push($keywords_key, $value->dvv_id);
            $keywords_value[$value->dvv_id] = array();

            $vr_video_result[$key]['id'] =  $value->dvv_id;
            $vr_video_result[$key]['name'] =  $value->dvv_name;
            $vr_video_result[$key]['photo'] =  $this->config->item('server_base_url').$value->dvv_rep_img_path;
            $web_link = str_replace("https://www.youtube.com/watch?v=", "", $value->dvv_youtube_link);
            $vr_video_result[$key]['dvv_web_video'] =  "https://www.youtube.com/watch?v=".$web_link;
            $mobile_link = str_replace("https://www.youtube.com/watch?v=", "", $value->dvv_youtube_link);
            $vr_video_result[$key]['dvv_mobile_video'] =  "vnd.youtube:".$mobile_link;


            $vr_video_result[$key]['capture_time'] =  date('d M Y H:i',strtotime($value->dvv_capture_timestamp))." UTC";

            $vr_video_result[$key]['like'] = NULL;

            //判斷是為局處或是民眾  使用者名稱異動
            if ($value->is_civil=="Y") {
                //民眾
                    $vr_video_result[$key]['uploader'] =  $value->u_name;
            }else{
                //局處
                    $vr_video_result[$key]['uploader'] =  $value->a_name;
            }

        }

        //環景KeyWord 
        $where_array = array(
            "dvv_id" => $keywords_key
        );

        // //get_key_word
        $keywords = $this->Common_model->get_db_join("keyword_department_vr_video as dvv", $where_array , "keywords as ks" , "dvv.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");

        foreach ($keywords as $kwd_key => $kwdvalue) {
            if ($kwdvalue->kwd_name!="") {
                array_push($keywords_value[$kwdvalue->dvv_id], $kwdvalue->kwd_name);
            }
        }

        // 附在輸出
        foreach ($vr_video_list as $key => $value) {
            $vr_video_result[$key]['keywords'] = $keywords_value[$value->dvv_id];
        }

        $return_data["vr_photo_result"] = $vr_photo_result;
        $return_data["vr_video_result"] = $vr_video_result;

        return $return_data;
    }



    /* ==================================
     * 取得前端首頁市民分享區照片、影片列表 (已登入，含使用者是否喜愛)
     * ================================== */

    function get_vr_topics_with_login($u_id){
        $return_data = array();
        $vr_photo_list = array();
        $vr_photo_result = array();
        $vr_video_list = array();
        $vr_video_result = array();

        $this->readonly_db->select('t.d_id,t.dvp_id,t.dvp_name,t.Is_url,t.is_civil,t.dvp_rep_img_path,t.dvp_backup,t.dvp_view_link,t.dvp_capture_timestamp,u.u_name as u_name,a.ag_name as a_name');

        $this->readonly_db->from('department_vr_photo as t');
        $this->readonly_db->join("user as u", "u.u_id = t.d_id AND t.is_civil = 'Y'", "left");
        $this->readonly_db->join("admin_group as a", "a.ag_id = t.d_id AND t.is_civil != 'Y'", "left");
        $where_array = array(
            't.dvp_view_link !=' => NULL,
            't.dvp_state' => 'Y',
            't.dvp_del' => 'N'
        );
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }
        $this->readonly_db->order_by("t.dvp_validation_timestamp","DESC");

        $query = $this->readonly_db->get();
        if($query){
            $vr_photo_list = $query->result();
        }else{
            $vr_photo_list = NULL;
        }

        // 預存要搜尋的Keyword
        $keywords_value = array();
        $keywords_key = array();
        //整理資料
        foreach ($vr_photo_list as $key => $value) {
            array_push($keywords_key, $value->dvp_id);
            $keywords_value[$value->dvp_id] = array();

            $vr_photo_result[$key]['id'] =  $value->dvp_id;
            $vr_photo_result[$key]['name'] =  $value->dvp_name;
            $vr_photo_result[$key]['photo'] =  $this->config->item('server_base_url').$value->dvp_rep_img_path;

            //判斷照片為環景或全景 URL異動
            if ($value->photo_type=="pano") {
            //     //環景圖
            //     $TryStrpos=strpos($value->dvp_view_link,"http");

            //     if($TryStrpos === false){
            //         $vr_photo_result[$key]['url'] =  $this->config->item('api_base_url').'/kr_spherical?image_path='.$this->config->item('api_base_url').$value->dvp_backup;
            //     }else{
                    $vr_photo_result[$key]['url'] =  $value->dvp_view_link;

            //     }
            //         $vr_photo_result[$key]['mobile_url'] =  $this->config->item('api_base_url').'/kr_spherical?image_path='.$this->config->item('api_base_url').$value->dvp_backup;
            // }else{
            //     //全景圖
            //     $vr_photo_result[$key]['url'] =  $this->config->item('server_base_url').'/kr_pano?image_path='.$this->config->item('server_base_url').$value->dvp_backup;
                    $vr_photo_result[$key]['mobile_url'] =  $this->config->item('server_base_url').'/kr_spherical?image_path='.$this->config->item('server_base_url').$value->dvp_backup;
            }else{
                    $vr_photo_result[$key]['url'] =  $value->dvp_view_link;
                    $vr_photo_result[$key]['mobile_url'] =  $this->config->item('server_base_url').'/kr_pano?image_path='.$this->config->item('server_base_url').$value->dvp_backup;
            }

            //直接吃連結
            if($value->Is_url=="Y"){
                $vr_photo_result[$key]['url'] = $value->dvp_view_link;
                $vr_photo_result[$key]['mobile_url'] = $value->dvp_view_link;
            }
            
            $vr_photo_result[$key]['capture_time'] =  date('d M Y H:i',strtotime($value->dvp_capture_timestamp))." UTC";

            $vr_photo_result[$key]['like'] = NULL;

            //判斷是為局處或是民眾  使用者名稱異動
            if ($value->is_civil=="Y") {
                //民眾
                    $vr_photo_result[$key]['uploader'] =  $value->u_name;
            }else{
                //局處
                    $vr_photo_result[$key]['uploader'] =  $value->a_name;
            }

        }

        //環景KeyWord 
        $where_array = array(
            "dvp_id" => $keywords_key
        );

        // //get_key_word
        $keywords = $this->Common_model->get_db_join("keyword_department_vr_photo as dvp", $where_array , "keywords as ks" , "dvp.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");

        foreach ($keywords as $kwd_key => $kwdvalue) {
            if ($kwdvalue->kwd_name!="") {
                array_push($keywords_value[$kwdvalue->dvp_id], $kwdvalue->kwd_name);
            }
        }

        //民眾Like
        $like_data = array();
        $where_array = array(
            "dvp_id" => $keywords_key,
            'user_id'=>$u_id
        );
        $vr_photo_user_like = $this->Common_model->get_db("tp_user_photo", $where_array,"","DESC",0);

        foreach ($vr_photo_user_like as $key => $value) {

            $like_data[$value->dvp_id] = TRUE;            

        }

        // 附在輸出
        foreach ($vr_photo_list as $key => $value) {
            $vr_photo_result[$key]['keywords'] = $keywords_value[$value->dvp_id];
            if(isset($like_data[$value->dvp_id])){
                $vr_photo_result[$key]['like'] = TRUE;
            }else{
                $vr_photo_result[$key]['like'] = FALSE;
            }
        }


        $this->readonly_db->select('t.d_id,t.dvv_id,t.dvv_name,t.dvv_rep_img_path,t.dvv_youtube_link,t.dvv_capture_timestamp,t.is_civil,t.Is_url,u.u_name as u_name,a.ag_name as a_name');
        $this->readonly_db->from('tp_department_vr_video as t');
        $this->readonly_db->join("user as u", "u.u_id = t.d_id AND t.is_civil = 'Y'", "left");
        $this->readonly_db->join("admin_group as a", "a.ag_id = t.d_id AND t.is_civil != 'Y'", "left");

        $where_array = array(
                't.dvv_state' => 'Y',
                't.dvv_del' => 'N'
        );
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }
        $this->readonly_db->order_by("dvv_validation_timestamp","DESC");
        $query = $this->readonly_db->get();
        if($query){
            $vr_video_list = $query->result();
        }else{
            $vr_video_list = NULL;
        }

        $keywords_value = array();
        $keywords_key = array();
        //整理資料
        foreach ($vr_video_list as $key => $value) {
            array_push($keywords_key, $value->dvv_id);
            $keywords_value[$value->dvv_id] = array();

            $vr_video_result[$key]['id'] =  $value->dvv_id;
            $vr_video_result[$key]['name'] =  $value->dvv_name;
            $vr_video_result[$key]['photo'] =  $this->config->item('server_base_url').$value->dvv_rep_img_path;
            $web_link = str_replace("https://www.youtube.com/watch?v=", "", $value->dvv_youtube_link);
            $vr_video_result[$key]['dvv_web_video'] =  "https://www.youtube.com/watch?v=".$web_link;
            $mobile_link = str_replace("https://www.youtube.com/watch?v=", "", $value->dvv_youtube_link);
            $vr_video_result[$key]['dvv_mobile_video'] =  "vnd.youtube:".$mobile_link;


            $vr_video_result[$key]['capture_time'] =  date('d M Y H:i',strtotime($value->dvv_capture_timestamp))." UTC";


            //判斷是為局處或是民眾  使用者名稱異動
            if ($value->is_civil=="Y") {
                //民眾
                    $vr_video_result[$key]['uploader'] =  $value->u_name;
            }else{
                //局處
                    $vr_video_result[$key]['uploader'] =  $value->a_name;
            }

        }

        //環景KeyWord 
        $where_array = array(
            "dvv_id" => $keywords_key
        );

        // //get_key_word
        $keywords = $this->Common_model->get_db_join("keyword_department_vr_video as dvv", $where_array , "keywords as ks" , "dvv.kwd_id = ks.kwd_id " , "LEFT" ,"rwr_id" , "ASC");

        foreach ($keywords as $kwd_key => $kwdvalue) {
            if ($kwdvalue->kwd_name!="") {
                array_push($keywords_value[$kwdvalue->dvv_id], $kwdvalue->kwd_name);
            }
        }


        //民眾Like
        $like_data = array();
        $where_array = array(
            "dvv_id" => $keywords_key,
            'user_id'=>$u_id
        );
        $vr_video_user_like = $this->Common_model->get_db("tp_user_video", $where_array,"","DESC",0);

        foreach ($vr_video_user_like as $key => $value) {

            $like_data[$value->dvv_id] = TRUE;            

        }

        // 附在輸出
        foreach ($vr_video_list as $key => $value) {
            $vr_video_result[$key]['keywords'] = $keywords_value[$value->dvv_id];
            if(isset($like_data[$value->dvv_id])){
                $vr_video_result[$key]['like'] = TRUE;
            }else{
                $vr_video_result[$key]['like'] = FALSE;
            }
        }

        $return_data["vr_photo_result"] = $vr_photo_result;
        $return_data["vr_video_result"] = $vr_video_result;

        return $return_data;


    }


}
