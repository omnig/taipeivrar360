<?php

class Ap_pois_model extends CI_Model {

    protected $readonly_db;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    /* ==================================
     * 取得列表
     * ================================== */

    function get_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('ap.*,ag.*,ac.*,tdar.*,dai.*,b.b_id,b.b_description,b.b_major,b.b_minor,b.b_mac,b.b_lat,b.b_lng,b.b_range');
        $this->readonly_db->from('ap_pois as ap');
        $this->readonly_db->join('admin_group AS ag', 'ap.d_id=ag.ag_id', 'LEFT');
        $this->readonly_db->join('ap_category AS ac', 'ap.ac_id=ac.ac_id', 'LEFT');
        $this->readonly_db->join('department_ar AS tdar', 'ap.tdar_id=tdar.tdar_id', 'LEFT');
        $this->readonly_db->join('department_ar_image AS dai', 'ap.dai_id=dai.dai_id', 'LEFT');
        $this->readonly_db->join('beacon_poi_relation AS bpr', 'ap.ap_id=bpr.ap_id', 'LEFT');
        $this->readonly_db->join('beacon AS b', 'b.b_id=bpr.b_id', 'LEFT');

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                'ap_pois', 'ap_name', 'ap_desc'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        $this->readonly_db->group_by("ap.ap_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }


}
