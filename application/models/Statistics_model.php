<?php

class Statistics_model extends CI_Model {


	protected $readonly_db;

	function __construct() {
	    // 呼叫模型(Model)的建構函數
	    parent::__construct();

	    $this->readonly_db = $this->load->database('read_only', TRUE);
	}


	public function get_document_num(){
		$this->readonly_db->select('*');
		$this->readonly_db->from("document");
		$this->readonly_db->where("d_del", "N");

		$query = $this->readonly_db->get();

		return $query->num_rows();
	}

	public function get_vrvideo_upload_num(){
		$this->readonly_db->select('*');
		$this->readonly_db->from("tp_department_vr_video");
		$this->readonly_db->where("dvv_del", "N");

		$query = $this->readonly_db->get();

		return $query->num_rows();
	}

	public function get_document_share_num(){
		$this->readonly_db->select('SUM(d_share_num) as SUM');
		$this->readonly_db->from("tp_document");
		$this->readonly_db->where("d_del", "N");
		$this->readonly_db->limit(1);

		$query = $this->readonly_db->get();

		return (int)$query->row()->SUM;
	}

	public function get_vrphoto_upload_num(){
		$this->readonly_db->select('*');
		$this->readonly_db->from("tp_department_vr_photo");
		$this->readonly_db->where("dvp_del", "N");

		$query = $this->readonly_db->get();

		return $query->num_rows();
	}

	public function get_document_read_num(){
		$this->readonly_db->select('SUM(d_view_num) as SUM');
		$this->readonly_db->from("tp_document");
		$this->readonly_db->where("d_del", "N");
		$this->readonly_db->limit(1);

		$query = $this->readonly_db->get();

		return (int)$query->row()->SUM;
	}

	public function get_vr_share_num(){

		$video_sum = 0;
		$photo_sum = 0;

		$this->readonly_db->select('SUM(dvv_shares) as SUM');
		$this->readonly_db->from("tp_department_vr_video");
		$this->readonly_db->where("dvv_del", "N");
		$this->readonly_db->limit(1);
		$query = $this->readonly_db->get();
		$video_sum= (int)$query->row()->SUM;


		$this->readonly_db->select('SUM(dvp_shares) as SUM');
		$this->readonly_db->from("tp_department_vr_photo");
		$this->readonly_db->where("dvp_del", "N");
		$this->readonly_db->limit(1);
		$query = $this->readonly_db->get();
		$photo_sum= (int)$query->row()->SUM;



		return $video_sum+$photo_sum;
	}

	public function get_comments_num(){
		$this->readonly_db->select('*');
		$this->readonly_db->from("tp_comment");
		$this->readonly_db->where("c_del", "N");

		$query = $this->readonly_db->get();

		return $query->num_rows();
	}

	public function get_member_num(){
		$this->readonly_db->select('*');
		$this->readonly_db->from("tp_user");
		$this->readonly_db->where("u_del", "N");

		$query = $this->readonly_db->get();

		return $query->num_rows();

	}

	public function get_proprietary_num(){
		$this->readonly_db->select('*');
		$this->readonly_db->from("tp_proprietary");
		$this->readonly_db->where("p_del", "N");

		$query = $this->readonly_db->get();

		return $query->num_rows();
	}


}