<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class AbstractPoints_model extends CI_Model {

    protected $readonly_db;
    protected $last_insert_id;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->db = $this->load->database(ENVIRONMENT, TRUE);
        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    /*     * ***************
     * 點位通用函數-新增點位
     * param      <String> <point_type> { 點位類別 景點:v 美食:f 住宿:h 購物:s 交通:t 宗教:r }
     * param      <Interger> <point_id> { 點位ID }
     *
     * ************** */

    function insert_point_data($point_type = "", $point_id = "") {
        $this->db->select('*');
        $this->db->from("point");


        $where_array = array(
            "p_type" => $point_type,
            "v_id" => $point_id
        );

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->db->where($value);
            } else {
                $this->db->where($index, $value);
            }
        }

        $this->db->limit(1);
        $query = $this->db->get();

        if (count($query->result()) != 0) {
            //假如存在則不做任何事
            return FALSE;
        } else {
            //假如不存在則新增

            $data_array = array(
                "p_type" => $point_type,
                "v_id" => $point_id
            );

            $this->db->insert("point", $data_array);

            $this->last_insert_id = $this->db->insert_id();

            if ($this->db->insert_id() != FALSE) {
                return $this->db->affected_rows();
            }
        }
    }

    function get_point_data_get_all() {


        $query = $this->db->query("
			(SELECT p.* ,v.v_name,v.v_name_en,v.v_lat,v.v_lng,v.v_image,v.v_address
			FROM `n_point` as p 
			JOIN  `n_viewpoints` as v on  p.v_id = v.v_id  AND p.p_type = 'v'
			WHERE v.v_name_en != ''
			)
			UNION ALL
			(SELECT p.* ,f.v_name,f.v_name_en,f.v_lat,f.v_lng,f.v_image,f.v_address
			FROM `n_point` as p 
			JOIN  `n_foodpoints` as f on  p.v_id = f.v_id  AND p.p_type = 'f'
			WHERE f.v_name_en != ''
			)
			UNION ALL
			(SELECT p.* ,s.v_name,s.v_name_en,s.v_lat,s.v_lng,s.v_image,s.v_address
			FROM `n_point` as p 
			JOIN  `n_shoppingpoints` as s on  p.v_id = s.v_id  AND p.p_type = 's'
			WHERE s.v_name_en != ''
			)
			UNION ALL
			(SELECT p.* ,h.v_name,h.v_name_en,h.v_lat,h.v_lng,h.v_image,h.v_address
			FROM `n_point` as p 
			JOIN  `n_hotelpoints` as h on  p.v_id = h.v_id  AND p.p_type = 'h'
			WHERE h.v_name_en != ''
			)
			UNION ALL
			((SELECT p.* ,r.rl_name as v_name ,r.rl_name_en as v_name_en ,r.rl_lat as v_lat, r.rl_lng as v_lng, r.rl_image as v_image,r.rl_address as v_address
			FROM `n_point` as p 
			JOIN  `n_religion_list` as r on  p.v_id = r.rl_id  AND p.p_type = 'r'
			WHERE r.rl_name_en != ''
			))
		");

        $result = $query->result();

        return $result;
    }

    function get_info_with_distance($user_lat = "", $user_lng = "", $object_distance = 50) {


        $query = $this->db->query("
			(SELECT p.* ,v.v_name,v.v_name_en,v.v_lat,v.v_lng,v.v_image,( 6371 * acos( cos( radians($user_lat) ) * cos( radians( v_lat ) ) * cos( radians(v_lng) - radians($user_lng)) + sin(radians($user_lat)) * sin( radians(v_lat)))) AS distance
			FROM `n_point` as p 
			JOIN  `n_viewpoints` as v on  p.v_id = v.v_id  AND p.p_type = 'v'
			WHERE v.v_name_en != ''
			HAVING distance <= $object_distance
			)
			UNION ALL
			(SELECT p.* ,f.v_name,f.v_name_en,f.v_lat,f.v_lng,f.v_image,( 6371 * acos( cos( radians($user_lat) ) * cos( radians( v_lat ) ) * cos( radians(v_lng) - radians($user_lng)) + sin(radians($user_lat)) * sin( radians(v_lat)))) AS distance
			FROM `n_point` as p 
			JOIN  `n_foodpoints` as f on  p.v_id = f.v_id  AND p.p_type = 'f'
			WHERE f.v_name_en != ''
			HAVING distance <= $object_distance
			)
			UNION ALL
			(SELECT p.* ,s.v_name,s.v_name_en,s.v_lat,s.v_lng,s.v_image,( 6371 * acos( cos( radians($user_lat) ) * cos( radians( v_lat ) ) * cos( radians(v_lng) - radians($user_lng)) + sin(radians($user_lat)) * sin( radians(v_lat)))) AS distance
			FROM `n_point` as p 
			JOIN  `n_shoppingpoints` as s on  p.v_id = s.v_id  AND p.p_type = 's'
			WHERE s.v_name_en != ''
			HAVING distance <= $object_distance
			)
			UNION ALL
			(SELECT p.* ,h.v_name,h.v_name_en,h.v_lat,h.v_lng,h.v_image,( 6371 * acos( cos( radians($user_lat) ) * cos( radians( v_lat ) ) * cos( radians(v_lng) - radians($user_lng)) + sin(radians($user_lat)) * sin( radians(v_lat)))) AS distance
			FROM `n_point` as p 
			JOIN  `n_hotelpoints` as h on  p.v_id = h.v_id  AND p.p_type = 'h'
			WHERE h.v_name_en != ''
			HAVING distance <= $object_distance
			)
			UNION ALL
			((SELECT p.* ,r.rl_name as v_name ,r.rl_name_en as v_name_en ,r.rl_lat as v_lat, r.rl_lng as v_lng, r.rl_image as v_image,( 6371 * acos( cos( radians($user_lat) ) * cos( radians( rl_lat ) ) * cos( radians(rl_lng) - radians($user_lng)) + sin(radians($user_lat)) * sin( radians(rl_lat)))) AS distance
			FROM `n_point` as p 
			JOIN  `n_religion_list` as r on  p.v_id = r.rl_id  AND p.p_type = 'r'
			WHERE r.rl_name_en != ''
			HAVING distance <= $object_distance
			))
		");

        $result = $query->result();

        return $result;
    }

    function get_point_data($data_array = array(), $is_order = false, $user_lat = "", $user_lng = "", $object_distance = 50) {

        $ary_to_string = implode(',', $data_array);

        if (count($data_array) > 0) {
            $where_in_pids = "AND  p.p_id in ($ary_to_string)";
        } else {
            $where_in_pids = "";
        }
        if ($is_order) {
            $order_by_search_pids = " ORDER BY FIELD(`p_id`,$ary_to_string)";
        } else {
            $order_by_search_pids = '';
        }

        if ($user_lat && $user_lng) {
            $select = "SELECT p.* ,v_name,v_name_en,v_lat,v_lng,v_image,v_address,v_web,v_tel,v_opentime,a_name, count(fav.f_id) as tot_fav, (6371 * acos( cos( radians($user_lat) ) * cos( radians( v_lat ) ) * cos( radians(v_lng) - radians($user_lng)) + sin(radians($user_lat)) * sin( radians(v_lat)))) AS distance";
            $select_religion = "SELECT p.* ,rl_name as v_name ,rl_name_en as v_name_en ,rl_lat as v_lat, rl_lng as v_lng, rl_image as v_image,rl_address as v_address,rl_web as v_web,rl_tel as v_tel,rl_opentime as v_opentime,a_name, count(fav.f_id) as tot_fav, (6371 * acos( cos( radians($user_lat) ) * cos( radians( rl_lat ) ) * cos( radians(rl_lng) - radians($user_lng)) + sin(radians($user_lat)) * sin( radians(rl_lat)))) AS distance";
            $having_dist = "HAVING distance <= $object_distance";
        } else {
            $select = "SELECT p.* ,v_name,v_name_en,v_lat,v_lng,v_image,v_address,v_web,v_tel,v_opentime,a_name, count(fav.f_id) as tot_fav";
            $select_religion = "SELECT p.* ,rl_name as v_name ,rl_name_en as v_name_en ,rl_lat as v_lat, rl_lng as v_lng, rl_image as v_image,rl_address as v_address,rl_web as v_web,rl_tel as v_tel,rl_opentime as v_opentime,a_name, count(fav.f_id) as tot_fav";
            $having_dist = '';
        }

        $query = $this->db->query("
			($select
			FROM `n_point` as p 
			JOIN  `n_viewpoints` as v on  p.v_id = v.v_id  AND p.p_type = 'v'
                        JOIN  `n_area` as a on  v.a_id = a.a_id
                        LEFT JOIN  `n_favorite` as fav on p.p_id=fav.p_id
			WHERE v_name_en != '' $where_in_pids GROUP BY p.p_id
                        $having_dist)
			UNION ALL
			($select
			FROM `n_point` as p 
			JOIN  `n_foodpoints` as v on  p.v_id = v.v_id  AND p.p_type = 'f'
                        JOIN  `n_area` as a on  v.a_id = a.a_id
                        LEFT JOIN  `n_favorite` as fav on p.p_id=fav.p_id
			WHERE v_name_en != '' $where_in_pids GROUP BY p.p_id
                        $having_dist)
			UNION ALL
			($select
			FROM `n_point` as p 
			JOIN  `n_shoppingpoints` as v on  p.v_id = v.v_id  AND p.p_type = 's'
                        JOIN  `n_area` as a on  v.a_id = a.a_id
                        LEFT JOIN  `n_favorite` as fav on p.p_id=fav.p_id
			WHERE v_name_en != '' $where_in_pids GROUP BY p.p_id
                        $having_dist)
			UNION ALL
			($select
			FROM `n_point` as p 
			JOIN  `n_hotelpoints` as v on  p.v_id = v.v_id  AND p.p_type = 'h'
                        JOIN  `n_area` as a on  v.a_id = a.a_id
                        LEFT JOIN  `n_favorite` as fav on p.p_id=fav.p_id
			WHERE v_name_en != '' $where_in_pids GROUP BY p.p_id
                        $having_dist)
			UNION ALL
			($select_religion
			FROM `n_point` as p 
			JOIN  `n_religion_list` as v on  p.v_id = v.rl_id  AND p.p_type = 'r'
                        JOIN  `n_area` as a on  v.a_id = a.a_id
                        LEFT JOIN  `n_favorite` as fav on p.p_id=fav.p_id
			WHERE rl_name_en != '' $where_in_pids GROUP BY p.p_id
                        $having_dist)
                        $order_by_search_pids
		");

        $result = $query->result();

        return $result;
    }

        function get_point_latlng($data_array = array()) {

            $ary_to_string = implode(',', $data_array);

            if (count($data_array) > 0) {
                $where_in_pids = "AND  p.p_id in ($ary_to_string)";
            } else {
                $where_in_pids = "";
            }
            
            $select = "SELECT p.p_id ,v_lat,v_lng";
            $select_religion = "SELECT p.p_id ,rl_lat as v_lat, rl_lng as v_lng";
            $having_dist = '';


            $query = $this->db->query("
                ($select
                FROM `n_point` as p 
                JOIN  `n_viewpoints` as v on  p.v_id = v.v_id  AND p.p_type = 'v'
                            JOIN  `n_area` as a on  v.a_id = a.a_id
                            LEFT JOIN  `n_favorite` as fav on p.p_id=fav.p_id
                WHERE v_name_en != '' $where_in_pids GROUP BY p.p_id
                            $having_dist)
                UNION ALL
                ($select
                FROM `n_point` as p 
                JOIN  `n_foodpoints` as v on  p.v_id = v.v_id  AND p.p_type = 'f'
                            JOIN  `n_area` as a on  v.a_id = a.a_id
                            LEFT JOIN  `n_favorite` as fav on p.p_id=fav.p_id
                WHERE v_name_en != '' $where_in_pids GROUP BY p.p_id
                            $having_dist)
                UNION ALL
                ($select
                FROM `n_point` as p 
                JOIN  `n_shoppingpoints` as v on  p.v_id = v.v_id  AND p.p_type = 's'
                            JOIN  `n_area` as a on  v.a_id = a.a_id
                            LEFT JOIN  `n_favorite` as fav on p.p_id=fav.p_id
                WHERE v_name_en != '' $where_in_pids GROUP BY p.p_id
                            $having_dist)
                UNION ALL
                ($select
                FROM `n_point` as p 
                JOIN  `n_hotelpoints` as v on  p.v_id = v.v_id  AND p.p_type = 'h'
                            JOIN  `n_area` as a on  v.a_id = a.a_id
                            LEFT JOIN  `n_favorite` as fav on p.p_id=fav.p_id
                WHERE v_name_en != '' $where_in_pids GROUP BY p.p_id
                            $having_dist)
                UNION ALL
                ($select_religion
                FROM `n_point` as p 
                JOIN  `n_religion_list` as v on  p.v_id = v.rl_id  AND p.p_type = 'r'
                            JOIN  `n_area` as a on  v.a_id = a.a_id
                            LEFT JOIN  `n_favorite` as fav on p.p_id=fav.p_id
                WHERE rl_name_en != '' $where_in_pids GROUP BY p.p_id
                            $having_dist)
            ");

            $result = $query->result();

            return $result;
        }

    function get_point_data_detail($data_array = array()) {

        $ary_to_string = "";

        foreach ($data_array as $key => $value) {
            $ary_to_string .= (int) $value . ",";
        }
        $ary_to_string = substr($ary_to_string, 0, -1);

        $query = $this->db->query("
			(SELECT p.* ,v.v_name,v.v_lat,v.v_lng
			FROM `n_point` as p 
			JOIN  `n_viewpoints` as v on  p.v_id = v.v_id  AND p.p_type = 'v'
			where p.p_id in ($ary_to_string))
			UNION ALL
			(SELECT p.* ,f.v_name,f.v_lat,f.v_lng
			FROM `n_point` as p 
			JOIN  `n_foodpoints` as f on  p.v_id = f.v_id  AND p.p_type = 'f'
			where p.p_id in ($ary_to_string))
			UNION ALL
			(SELECT p.* ,s.v_name,s.v_lat,s.v_lng
			FROM `n_point` as p 
			JOIN  `n_shoppingpoints` as s on  p.v_id = s.v_id  AND p.p_type = 's'
			where p.p_id in ($ary_to_string))
			UNION ALL
			(SELECT p.* ,h.v_name,h.v_lat,h.v_lng
			FROM `n_point` as p 
			JOIN  `n_hotelpoints` as h on  p.v_id = h.v_id  AND p.p_type = 'h'
			where p.p_id in ($ary_to_string))
		");

        $result = $query->result();

        return $result;
    }

    function get_point_item($table_name = "", $where_array = array()) {

        $this->readonly_db->select('p.* , v.*');
        $this->readonly_db->from($table_name . ' as v');
        $this->readonly_db->join('point as p', 'p.v_id=v.v_id', 'inner');
        $this->readonly_db->where('v_del', 'N');

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        $query = $this->readonly_db->get();
        $result = $query->result();

        return $result;
    }

    // 更新Place id
    function update_place_id($place_id = "", $p_id = "") {

        $this->db->where("p_id", $p_id);

        $this->db->set("p_place_id", $place_id);

        $this->db->update("n_point");
        return $this->db->affected_rows();
    }

    // 更新Place score
    function update_place_score($p_gscore = "", $p_id = "") {

        $this->db->where("p_id", $p_id);

        $this->db->set("p_gscore", $p_gscore);

        $this->db->update("n_point");
        return $this->db->affected_rows();
    }

    function count_un_en_name($table_name) {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->or_where("v_name_en", "");
        $this->db->or_where("v_name_en", NULL);

        $query = $this->db->get();

        if ($query) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

}
