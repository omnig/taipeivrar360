<?php

class Store_balance_model extends CI_Model {

    protected $readonly_db;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    /* ==================================
     * 取得列表
     * ================================== */

    function get_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('*');
        $this->readonly_db->from('store_balance AS sb');
        $this->readonly_db->join('store AS s', "sb.s_id=s.s_id");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                'sb_title', 'sb_description', 's_name_zh'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();

            return $query->result();
        }
    }

    /* ==================================
     * 取得關帳結餘紀錄
     * ================================== */

    function get_close_balance($since, $until, $where_array = array()) {
        $join = "SELECT *,COUNT(sb_id) AS total_record "
                . "FROM ("
                . "SELECT * "
                . "FROM S_store_balance "
                . "WHERE sb_balance_timestamp < '{$until} 00:00:00'"
                . "ORDER BY sb_balance_timestamp DESC,sb_id DESC "
                . ") AS tmp "
                . "GROUP BY s_id";

        $this->readonly_db->select('s.*,sb.*,IFNULL(sb_balance,0) AS sb_balance,s.s_id AS s_id');
        $this->readonly_db->from('store AS s');
        $this->readonly_db->join("({$join}) AS sb", "s.s_id=sb.s_id", "LEFT");
        $this->readonly_db->where("s.s_del", "N");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        $query = $this->readonly_db->get();

        return $query->result();
    }

    /* ==================================
     * 取得不重複的os_id
     * ================================== */

    function get_distinct_os_id() {
        $sql = "SELECT DISTINCT os_id FROM `S_store_balance` WHERE os_id IS NOT NULL AND `sb_balance_timestamp` IS NOT NULL";

        $query = $this->readonly_db->query($sql);

        return $query->result();
    }
}
