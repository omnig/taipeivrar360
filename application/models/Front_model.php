<?php

class Front_model extends CI_Model {

    protected $readonly_db;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    /* ==================================
     * 取得product_category list
     * 此函式會被每一頁前台頁面開啟，故須保持高效能
     * 傳入s_id為特定商店id
     * ================================== */

    // function get_product_category_list($s_id = false) {

    //     $where_s_id = ($s_id) ? " AND s_id={$s_id} " : '';

    //     $exists_psc = "SELECT NULL
    //             FROM {$this->readonly_db->dbprefix('product_category_relation')} AS A_pcr
    //             JOIN {$this->readonly_db->dbprefix('product_sub_category')} AS A_psc ON A_pcr.psc_id=A_psc.psc_id
    //             JOIN {$this->readonly_db->dbprefix('product')} AS A_p ON A_p.p_id=A_pcr.p_id{$where_s_id}
    //             WHERE A_p.p_enabled='Y' AND A_p.p_del='N' AND A_psc.psc_id=sql_psc_psc.psc_id";

    //     $sql_psc = "SELECT sql_psc_psc.*
    //             FROM {$this->readonly_db->dbprefix('product_sub_category')} AS sql_psc_psc
    //             WHERE EXISTS ($exists_psc)";

    //     $exists_pc = "SELECT NULL
    //             FROM {$this->readonly_db->dbprefix('product_category_relation')} AS B_pcr
    //             JOIN {$this->readonly_db->dbprefix('product_category')} AS B_pc ON B_pcr.pc_id=B_pc.pc_id
    //             JOIN {$this->readonly_db->dbprefix('product')} AS B_p ON B_p.p_id=B_pcr.p_id{$where_s_id}
    //             WHERE B_p.p_enabled='Y' AND B_p.p_del='N' AND B_pc.pc_id=pc.pc_id";

    //     $sql = "SELECT psc.*,pc.*
    //             FROM {$this->readonly_db->dbprefix('product_category')} AS pc
    //             LEFT JOIN ({$sql_psc}) AS psc ON pc.pc_id=psc.pc_id
    //             WHERE EXISTS ({$exists_pc}) OR psc.psc_id IS NOT NULL
    //             ORDER BY pc_order ASC,psc_order ASC";

    //     $query = $this->readonly_db->query($sql);

    //     return $query->result();
    // }

    /* ==================================
     * 取得store_category list
     * 此函式會被大量前台頁面開啟，故須保持高效能
     * ================================== */

    function get_store_category_list() {

        $exists_ssc = "SELECT NULL
                FROM {$this->readonly_db->dbprefix('store')} AS A_s
                JOIN {$this->readonly_db->dbprefix('store_sub_category')} AS A_ssc ON A_s.ssc_id=A_ssc.ssc_id AND A_ssc.ssc_enabled='Y'
                WHERE A_s.s_enabled='Y' AND A_s.s_ec_enabled='Y' AND sql_sc.sc_id=A_ssc.sc_id";

        $sql = "SELECT sql_sc.*
                FROM {$this->readonly_db->dbprefix('store_category')} AS sql_sc
                WHERE EXISTS ($exists_ssc)
                ORDER BY sql_sc.sc_order ASC";

        $query = $this->readonly_db->query($sql);

        return $query->result();
    }

    /* ==================================
     * 取得商品的category
     * ================================== */

    function get_category_of_product($where_array = array()) {
        //主分類
        $this->readonly_db->select('*');
        $this->readonly_db->from('product_category_relation AS pcr');
        $this->readonly_db->join("product_category AS pc", "pcr.pc_id=pc.pc_id AND pc.pc_enabled='Y'");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        $query = $this->readonly_db->get();
        $product_category = $query->result();

        //次分類
        $this->readonly_db->select('*');
        $this->readonly_db->from('product_category_relation AS pcr');
        $this->readonly_db->join("product_sub_category AS psc", "pcr.psc_id=psc.psc_id AND psc.psc_enabled='Y'");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        $query = $this->readonly_db->get();
        $product_sub_category = $query->result();

        return array(
            "pc" => $product_category,
            "psc" => $product_sub_category
        );
    }

    /* ==================================
     * 取得store list
     * ================================== */

    function get_store_list($where_array = array(), $limit = 0) {
        $exists_p = "SELECT NULL
                FROM {$this->readonly_db->dbprefix('product')} AS p
                WHERE p.s_id=s.s_id AND p_enabled='Y' AND p_del='N'";

        $this->readonly_db->select('*');
        $this->readonly_db->from('store AS s');
        $this->readonly_db->join('store_sub_category AS ssc',"s.ssc_id=ssc.ssc_id AND ssc.ssc_enabled='Y'","inner");
        $this->readonly_db->join('store_category AS sc',"sc.sc_id=ssc.sc_id AND sc.sc_enabled='Y'","inner");

        $this->readonly_db->where("EXISTS ({$exists_p})");
        $this->readonly_db->where('s_ec_enabled', 'Y');
        $this->readonly_db->where('s_enabled', 'Y');
        $this->readonly_db->where('s_del', 'N');

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        $this->readonly_db->order_by("s_name_{$this->session->i18n} ASC,s_create_timestamp DESC");

        $this->readonly_db->group_by("s.s_name_{$this->session->i18n}");

        if ($limit > 0) {
            $this->readonly_db->limit($limit);
        }
        $query = $this->readonly_db->get();
        return $query->result();
    }

    /* ==================================
     * 取得trade circle list
     * ================================== */

    function get_trade_circle_list($where_array = array(), $limit = 0) {
        $this->readonly_db->select('*');
        $this->readonly_db->from('trade_circle AS tc');

        $this->readonly_db->where('tc_enabled', 'Y');

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        $this->readonly_db->order_by("tc_name ASC,tc_create_timestamp DESC");

        if ($limit > 0) {
            $this->readonly_db->limit($limit);
        }
        $query = $this->readonly_db->get();
        return $query->result();
    }

    /* ==================================
     * 取得上線店家
     * ================================== */

    function get_online_stores($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false, $group = '') {
        //距離欄位select
        $select_distance = "SQRT(POWER((s_lat-{$this->user_latlon["lat"]}),2)+POWER((s_lon-{$this->user_latlon["lon"]}),2)) AS s_distance";

        $this->readonly_db->select("s.*,sc.*,ssc.*,COUNT(*) AS p_sum,SUM(p_sold_count) AS p_sold_count,{$select_distance}");
        $this->readonly_db->from('store AS s'); 

        $this->readonly_db->where("s.s_ec_enabled", "Y");
        $this->readonly_db->where("s.s_enabled", "Y");
        $this->readonly_db->where("s.s_del", "N");

        //以分類關係串接次分類
        $this->readonly_db->join('store_sub_category AS ssc', "`s`.`ssc_id`=`ssc`.`ssc_id` AND `ssc`.`ssc_enabled`='Y'", "LEFT");
        //以分類關係串接主分類
        $this->readonly_db->join('store_category AS sc', "`ssc`.`sc_id`=`sc`.`sc_id` AND `sc`.`sc_enabled`='Y'", "INNER");

        //店家商品數量
        $this->readonly_db->join('product AS p', "`p`.`s_id`=`s`.`s_id`");

        $this->readonly_db->where("p.p_enabled", "Y");
        $this->readonly_db->where("p.p_del", "N");


        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                's_name_zh', 's_name_en', 's_address'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        if ($group != '') {
            $this->readonly_db->group_by($group);
        }
        else{
            $this->readonly_db->group_by("s.s_id");
        }

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            //die($this->readonly_db->last_query());
            return $query->result();
        }
    }

    /* ==================================
     * 取得上架商品
     * ================================== */

    function get_online_products($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        //距離欄位select
        $select_distance = "SQRT(POWER((s_lat-{$this->user_latlon["lat"]}),2)+POWER((s_lon-{$this->user_latlon["lon"]}),2)) AS s_distance";

        $this->readonly_db->select("p.*, s.* ,pc.*,{$select_distance}");
        $this->readonly_db->from('product AS p');
        //串接分類關係
        $this->readonly_db->join('product_category_relation AS pcr', "p.p_id=pcr.p_id");
        //以分類關係串接次分類
        $this->readonly_db->join('product_sub_category AS psc', "(`pcr`.`psc_id`=`psc`.`psc_id` OR `pcr`.`pc_id`=`psc`.`pc_id`) AND `psc`.`psc_enabled`='Y'", "LEFT");
        //以分類關係串接主分類
        $this->readonly_db->join('product_category AS pc', "(`pcr`.`pc_id`=`pc`.`pc_id` OR `pc`.`pc_id`=`psc`.`pc_id`) AND `pc`.`pc_enabled`='Y'", "INNER");
        //串接商店
        $this->readonly_db->join('store AS s', "p.s_id=s.s_id AND s.s_ec_enabled='Y' AND s.s_enabled='Y' AND s.s_del='N'");

        $this->readonly_db->where("p.p_enabled", "Y");
        $this->readonly_db->where("p.p_del", "N");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                'p_name', 'p_description', 's_name_zh', 's_name_en'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        $this->readonly_db->group_by("p.p_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();

            return $query->result();
        }
    }
    
    function get_temp_products($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        //距離欄位select
        $select_distance = "SQRT(POWER((s_lat-{$this->user_latlon["lat"]}),2)+POWER((s_lon-{$this->user_latlon["lon"]}),2)) AS s_distance";

        $this->readonly_db->select("pt.*, s.*,{$select_distance}");
        $this->readonly_db->from('product_temp AS pt');
        //串接分類關係
        $this->readonly_db->join('product_category_relation AS pcr', "pt.pt_id=pcr.pt_id");
        //以分類關係串接次分類
        $this->readonly_db->join('product_sub_category AS psc', "(`pcr`.`psc_id`=`psc`.`psc_id` OR `pcr`.`pc_id`=`psc`.`pc_id`) AND `psc`.`psc_enabled`='Y'", "LEFT");
        //以分類關係串接主分類
        $this->readonly_db->join('product_category AS pc', "(`pcr`.`pc_id`=`pc`.`pc_id` OR `pc`.`pc_id`=`psc`.`pc_id`) AND `pc`.`pc_enabled`='Y'", "INNER");
        //串接商店
        $this->readonly_db->join('store AS s', "pt.s_id=s.s_id AND s.s_ec_enabled='Y' AND s.s_enabled='Y' AND s.s_del='N'");

        $this->readonly_db->where("pt.pt_del", "N");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                'pt_name', 'pt_description', 's_name_zh', 's_name_en'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        $this->readonly_db->group_by("pt.pt_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();

            return $query->result();
        }
    }
    
    
    function get_sp_list($where_array = array(), $limit = 0) {
        $this->readonly_db->select('*');
        $this->readonly_db->from('sub_topic_planning AS stp');
        //串接主題
        $this->readonly_db->join('topic_planning AS tp', "tp.tp_id=stp.tp_id");
        
        $this->readonly_db->where('stp_enabled', 'Y');
        $this->readonly_db->where('tp_enabled', 'Y');

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        $this->readonly_db->order_by("stp_order ASC");
        
        if ($limit > 0) {
            $this->readonly_db->limit($limit);
        }
        $query = $this->readonly_db->get();

        return $query->result();
    }
    
    
    function get_product_faq_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('*');
        $this->readonly_db->from('product_faq AS pf');
        $this->readonly_db->join('product AS p', "pf.p_id=p.p_id AND p_del='N'", "LEFT");
        $this->readonly_db->join('store AS s', "p.s_id=s.s_id AND s_del='N'", "LEFT");
        $this->readonly_db->join('admin AS adm', "pf.adm_id=adm.adm_id AND adm.adm_enabled='Y'", "LEFT");
        $this->readonly_db->join('user AS u', "pf.u_id=u.u_id AND u.u_enabled='Y' AND u.u_del='N'", "LEFT");
        
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                'pf_question', 'pf_answer'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }
    
    /* ==================================
     * 取得已購買列表
     * ================================== */

    function get_user_ordered_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('*, MAX(o_create_timestamp) as o_timestamp');
        $this->readonly_db->from('order AS o');
        $this->readonly_db->join('order_item AS oi', "oi.o_id=o.o_id");
        $this->readonly_db->join('product AS p', "oi.p_id=p.p_id");
        $this->readonly_db->where("o_del", "N");
        $this->readonly_db->where("o_order_number !=", "");
        $this->readonly_db->where("o_order_number IS NOT NULL");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                'o_buyer_name'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order,"DESC");
        }

        $this->readonly_db->group_by("oi.p_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

}
