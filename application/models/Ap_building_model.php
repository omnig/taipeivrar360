<?php

class Ap_building_model extends CI_Model {

    protected $readonly_db;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    /* ==================================
     * 取得列表
     * ================================== */

    function get_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('ab.*,GROUP_CONCAT(DISTINCT af_name order by af_number) as af_name');
        $this->readonly_db->from('ap_building as ab');
        $this->readonly_db->join('ap_floors as af', 'af.ab_id=ab.ab_id', 'left');

        $this->readonly_db->where('(ab_lat is not null and ab_lng is not null)');

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                ''
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        $this->readonly_db->group_by("ab_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

    function get_all_relation($latitude = '', $longitude = '', $order = '', $keyword = '', $limit = 0, $skip = 0, $where_array = array(), $return_number_of_all_records = false, $group = '') {

        $event_time = date("Y-m-d H:i:s");

        $select = 'c_zone,GROUP_CONCAT(CONCAT(es_id,";",e.e_id,";",es_begin_timestamp,";",es_end_timestamp,";",es_addr_zh,";",es_addr_en) order by es_begin_timestamp) as schedule';
        $select .= ",ap.ap_id,ap_name,ap_name_en,ap_desc,ap_desc_en,aac.aac_id,aac_title,aac_title_en,ac.ac_id,ac_title_en,ac_title_zh,ap_lat,ap_lng,ap_is_virtual_sign,ac_is_show,ap_is_into_show";
        $select .= ",af.af_id,af_number,af.af_enabled,af_name,af_desc,af_order,af_bottom_left_lat,af_bottom_left_lng,af_top_right_lat,af_top_right_lng,af_center_lat,af_center_lng,af_plan_id,af_map";
        $select .= ",ar_id,ar_state,GROUP_CONCAT(ar_id SEPARATOR ',') as ar_group,GROUP_CONCAT(ar_image_path SEPARATOR ',') as ar_img_group";
        if ($latitude && $longitude) {
            $select_distance = "SQRT(POW(ap_lat-{$latitude},2)+POW(ap_lng-{$longitude},2)) AS distance";
            $select = "{$select},{$select_distance}";
        }
        $this->readonly_db->select($select);
        $this->readonly_db->from('config as c');
        $this->readonly_db->join('ap_floors as af', 'c.c_id=af.ab_id', 'left');
        $this->readonly_db->join('ap_pois as ap', 'af.af_id=ap.af_id', 'left');
        $this->readonly_db->join('ap_category as ac', 'ac.ac_id=ap.ac_id', 'left');
        $this->readonly_db->join('ap_app_category as aac', 'ac.aac_id=aac.aac_id', 'left');
        $this->readonly_db->join('event_schedule as es', "(es.ap_id=ap.ap_id and es_end_timestamp >= '{$event_time}')", 'left');
        $this->readonly_db->join('event as e', '(es.e_id=e.e_id and e_enabled="Y")', 'left');

        $this->readonly_db->join('ar_pattern as arn', 'e.e_id=arn.e_id', 'left');
        
        $this->readonly_db->where("(c_lat is not null and c_lng is not null)");


        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                'ap_name', 'e_title_zh', 'e_title_en', 'ac_keyword'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        if ($group != '') {
            $this->readonly_db->group_by($group);
        }

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();

            if ($query) {
                return $query->result();
            }
            return array();
        }
    }

    function set_couchabse_account_data($id, $fields) {


        $id = $id . "_google";
        $value = array(
            "owner_id" => $id,
            "name" => $fields["name"],
            "doc_type" => "account",
            "type" => "google"
        );

        try {
            $this->couchbase->set($id, $value);
        } catch (Exception $e) {
            
        }
    }

    function get_couchbase_building_data($config_name) {
        $result = $this->couchbase->ViewQuery("", 'accounts', 'accounts_all')->total_rows;
        return $result;
    }

    function set_couchabse_building_data($id, $fields) {
        $id = "building_" . $fields["name"];

        $value = array(
            "buid" => $id,
            "address" => "-",
            "owner_id" => $fields["name"] . "_google",
            "is_published" => "true",
            "name" => $fields["name"],
            "description" => "",
            "bucode" => "",
            "coordinates_lat" => (float) $fields["lat"],
            "coordinates_lon" => (float) $fields["lng"],
            "url" => "-",
            "geometry" => array(
                "coordinates" => array(
                    (float) $fields["lat"],
                    (float) $fields["lng"],
                ),
                "type" => "Point"
            ),
            "co_owners" => array()
        );

        try {
            $this->couchbase->set($id, $value);
        } catch (Exception $e) {
            
        }
    }

}
