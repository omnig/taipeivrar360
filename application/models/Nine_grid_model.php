<?php

class Nine_grid_model extends CI_Model {

    protected $readonly_db;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    /* ==================================
     * 取得列表
     * ================================== */

    function get_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('ng.*, ap.*, tac.*, da.*, dai.*, b.*');
        $this->readonly_db->from('nine_grid as ng');
        $this->readonly_db->join('ap_pois as ap', "ng.ap_id=ap.ap_id", "LEFT");
        $this->readonly_db->join('ap_category AS tac', "ap.ac_id=tac.ac_id");
        $this->readonly_db->join('department_ar as da', "ap.tdar_id=da.tdar_id", "LEFT");
        $this->readonly_db->join('department_ar_image AS dai', 'ap.dai_id=dai.dai_id', 'LEFT');
        $this->readonly_db->join('beacon_poi_relation AS bpr', 'ap.ap_id=bpr.ap_id', 'LEFT');
        $this->readonly_db->join('beacon AS b', 'b.b_id=bpr.b_id', 'LEFT');


        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

    function get_view_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('ng.*,ag.*,ap.*');
        $this->readonly_db->from('nine_grid as ng');
        $this->readonly_db->join('mission as m', 'm.m_id=ng.m_id', 'left');
        $this->readonly_db->join('admin_group as ag', 'ag.ag_id=m.dep_id', 'left');
        $this->readonly_db->join('ap_pois as ap', 'ap.ap_id=ng.ap_id', 'left');

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

    function get_max_grid($mid=0) {
        $this->readonly_db->select('*');
        $this->readonly_db->from('nine_grid');
        $this->readonly_db->where('m_id', $mid);
        $this->readonly_db->where('ap_id is not null');
        $query = $this->readonly_db->get();
        return $query->result();
    }

    function check_gird_in_mission($mid=0, $ngid=0){
        $this->readonly_db->select('*');
        $this->readonly_db->from('nine_grid');
        $this->readonly_db->where('m_id', $mid);
        $this->readonly_db->where('ng_id', $ngid);
        $this->readonly_db->where('ap_id is not null');
        $query = $this->readonly_db->get();
        return $query->num_rows();
    }
    

}
