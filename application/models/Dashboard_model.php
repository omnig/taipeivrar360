<?php

class Dashboard_model extends CI_Model {

    protected $readonly_db;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    function get_statistics($fromDate, $endDate){
        
        $result = $this->readonly_db->query("
            SELECT 
                MAX(CASE WHEN d_type = 'CPU_USAGE_MAX' THEN d_number END) AS CPU_USAGE_MAX,
                MAX(CASE WHEN d_type = 'CPU_USAGE_MIN' THEN d_number END) AS CPU_USAGE_MIN,
                MAX(CASE WHEN d_type = 'MEM_USAGE_MAX' THEN d_number END) AS MEM_USAGE_MAX,
                MAX(CASE WHEN d_type = 'MEM_USAGE_MIN' THEN d_number END) AS MEM_USAGE_MIN,
                MAX(CASE WHEN d_type = 'ONLINE_COUNT' THEN d_number END) AS ONLINE_COUNT,
                MAX(CASE WHEN d_type = 'USE_COUNT' THEN d_number END) AS USE_COUNT
            FROM (
                select
                    d_type,
                    IF(d_type in ('CPU_USAGE_MAX', 'MEM_USAGE_MAX', 'ONLINE_COUNT'), MAX(d_number),
                        IF(d_type in ('CPU_USAGE_MIN', 'MEM_USAGE_MIN'), MIN(d_number),
                            IF(d_type = 'USE_COUNT', SUM(d_number), null))) AS d_number,
                    '1' AS group_col
                FROM tp_dashboard
                WHERE
                    d_type in ('CPU_USAGE_MAX', 'CPU_USAGE_MIN', 'MEM_USAGE_MAX', 'MEM_USAGE_MIN', 'ONLINE_COUNT', 'USE_COUNT') AND
                    Date(d_date) >= '".$fromDate."' AND
                    Date(d_date) <= '".$endDate."' 
                GROUP BY d_type
            ) AS t
            GROUP BY group_col;
        ")->row();

        return $result ?? (object)[
            "CPU_USAGE_MAX" => null,
            "CPU_USAGE_MIN" => null,
            "MEM_USAGE_MAX" => null,
            "MEM_USAGE_MIN" => null,
            "ONLINE_COUNT" => null,
            "USE_COUNT" => null,
        ];
    }

}
