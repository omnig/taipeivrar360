<?php

/* =========================================================
 * 此model定義通用的資料庫函數 
 * ========================================================= */

class Image_upload_model extends CI_Model {

    protected $last_insert_id;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        if (in_array(ENVIRONMENT,array("development","test","docker-dev"))) {
            $this->db = $this->load->database(ENVIRONMENT, TRUE);
        }else{
            $this->db = $this->load->database(ENVIRONMENT."_image", TRUE);
        }
        
    }

    /* ==================================
     * transation相關函數
     * ================================== */

    function trans_start() {
        $this->db->trans_start();
    }

    function trans_complete() {
        $this->db->trans_complete();
    }

    /* ==================================
     * 寫入
     * ================================== */

    /* ==================================
     * 新增資料-通用函數
     * 只須傳入資料表名稱和 "欄位"=>"值"的陣列
     * 即可直接寫入資料表
     * 回傳影響列數，確認是否正確寫入資料
     * ================================== */

    function insert_db($table_name, $insert_array) {
        $this->db->insert($table_name, $insert_array);

        $this->last_insert_id = $this->db->insert_id();
        return $this->db->affected_rows();
    }

    /* ==================================
     * 批次新增資料-通用函數
     * 只須傳入資料表名稱和 "欄位"=>"值"的陣列
     * 即可直接寫入資料表
     * 回傳影響列數，確認是否正確寫入資料
     * ================================== */

    function batch_insert_db($table_name, $insert_array) {
        $this->db->insert_batch($table_name, $insert_array);
        return $this->db->affected_rows();
    }

    /* ==================================
     * 取得新增id
     * ================================== */

    function get_insert_id() {
        return $this->last_insert_id;
    }

    /* ==================================
     * 批次新增資料-通用函數
     * 只須傳入資料表名稱和 "欄位"=>"值"的陣列
     * 即可直接寫入資料表
     * 回傳影響列數，確認是否正確寫入資料
     * ================================== */

    function insert_batch($table_name, $insert_array) {
        $this->db->insert_batch($table_name, $insert_array);

        $this->last_insert_id = $this->db->insert_id();
        return $this->db->affected_rows();
    }

    /* ==================================
     * 刪除資料-通用函數
     * 只須傳入資料表名稱和 "欄位"=>"值"的陣列
     * 即可直接刪除資料
     * 回傳影響列數，確認是否正確刪除資料
     * ================================== */

    function delete_db($table_name, $where_array) {
        if (count($where_array) > 0) {
            foreach ($where_array as $index => $value) {
                if (is_array($value)) {
                    $this->db->where_in($index, $value);
                } elseif (is_numeric($index)) {
                    $this->db->where($value);
                } else {
                    $this->db->where($index, $value);
                }
            }
            $this->db->delete($table_name);
            return $this->db->affected_rows();
        } else {
            return 0;
        }
    }

    /* ==================================
     * 更新資料-通用函數
     * 傳入：
     * 資料表名稱
     * 更新欄位array："欄位"=>"值"的陣列
     * 條件欄位array："欄位"=>"值"的陣列
     * 即可直接更新資料表
     * 回傳影響列數，確認是否更新成功
     * ================================== */

    function update_db($table_name, $update_array, $where_array = array(), $set_array = array()) {
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->db->where_in($index, $value);
            } elseif (is_int($index)) {
                $this->db->where($value);
            } else {
                $this->db->where($index, $value);
            }
        }

        foreach ($update_array as $index => $value) {
            $this->db->set($index, $value);
        }

        foreach ($set_array as $index => $value) {
            $this->db->set($index, $value, FALSE);
        }

        $this->db->update($table_name);
        return $this->db->affected_rows();
    }

    /* ==================================
     * 更新欄位-通用函數，可接受直接對欄位運算
     * 傳入：
     * 資料表名稱
     * 更新欄位array："欄位"=>"值"的陣列
     * 條件欄位array："欄位"=>"值"的陣列
     * 即可直接更新資料表
     * 回傳影響列數，確認是否更新成功
     * ================================== */

    function update_db_field($table_name, $update_array, $where_array = array()) {
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->db->where($value);
            } else {
                $this->db->where($index, $value);
            }
        }

        foreach ($update_array as $field => $value) {
            $this->db->set($field, $value, FALSE);
        }

        $this->db->update($table_name);
        return $this->db->affected_rows();
    }

    /* ==================================
     * 讀取
     * ================================== */

    /* ==================================
     * 統計資料表通用函數
     * ================================== */

    function count_db($table_name, $where_array = array()) {
        $this->db->select('*');
        $this->db->from($table_name);
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->db->where($value);
            } else {
                $this->db->where($index, $value);
            }
        }

        $query = $this->db->get();

        return $query->num_rows();
    }

    /* ==================================
     * 取得資料表通用函數
     * $where_array可傳入陣列或字串，若為陣列，則會引用where_in
     * ================================== */

    function get_db($table_name, $where_array = array(), $order_by = '', $order = 'asc', $limit = 0, $display_page = 1, $where_like_array = array()) {
        $this->db->select('*');
        $this->db->from($table_name);
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->db->where($value);
            } else {
                $this->db->where($index, $value);
            }
        }

        $i = 0;
        foreach ($where_like_array as $index => $value) {
            $i++;
            if ($i == 1) {
                $this->db->like($index, $value);
            } else {
                $this->db->or_like($index, $value);
            }
        }

        if ($order_by != '') {
            $this->db->order_by($order_by, $order);
        }

        if ($limit > 0) {
            $offset = ($display_page - 1) * $limit;
            $this->db->limit($limit);
            $this->db->offset($offset);
        }

        $query = $this->db->get();
        if ($query) {
            return $query->result();
        } else {
            return false;
        }
    }

    /* ==================================
     * 取得資料表通用函數
     * $where_array可傳入陣列或字串，若為陣列，則會引用where_in
     * 與get_db的差別，在於傳入的display_page改成skip
     * ================================== */

    function get_db_by_skip($table_name, $where_array = array(), $order_by = '', $order = 'asc', $limit = 0, $skip = 0, $where_like_array = array()) {
        $this->db->select('*');
        $this->db->from($table_name);
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->db->where($value);
            } else {
                $this->db->where($index, $value);
            }
        }

        $i = 0;
        foreach ($where_like_array as $index => $value) {
            $i++;
            if ($i == 1) {
                $this->db->like($index, $value);
            } else {
                $this->db->or_like($index, $value);
            }
        }

        if ($order_by != '') {
            $this->db->order_by($order_by, $order);
        }

        if ($limit > 0) {
            $this->db->limit($limit);
            $this->db->offset($skip);
        }

        $query = $this->db->get();
        if ($query) {
            return $query->result();
        } else {
            return false;
        }
    }

    /* ==================================
     * 取得join資料表通用函數
     * $where_array可傳入陣列或字串，若為陣列，則會引用where_in
     * ================================== */

    function get_db_join($table_name, $where_array = array(), $join = '', $join_on = '', $join_type = 'left', $order_by = '', $order = 'asc') {
        $this->db->select('*');
        $this->db->from($table_name);
        if ($join != '') {
            $this->db->join($join, $join_on, $join_type);
        }
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->db->where($value);
            } else {
                $this->db->where($index, $value);
            }
        }

        if ($order_by != '') {
            $this->db->order_by($order_by, $order);
        }

        $query = $this->db->get();

        if ($query) {
            return $query->result();
        } else {
            return false;
        }
    }

    function get_db_join_by_select($select, $table_name, $where_array = array(), $join = '', $join_on = '', $join_type = 'left', $order_by = '', $order = 'asc', $group_by = '', $where_like_array = array()) {
        $this->db->select($select);
        $this->db->from($table_name);
        if ($join != '') {
            $this->db->join($join, $join_on, $join_type);
        }
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->db->where($value);
            } else {
                $this->db->where($index, $value);
            }
        }

        foreach ($where_like_array as $index => $value) {
            $this->db->like($index, $value);
        }

        if ($order_by != '') {
            $this->db->order_by($order_by, $order);
        }

        if ($group_by != '') {
            $this->db->group_by($group_by);
        }

        $query = $this->db->get();

        if ($query) {
            return $query->result();
        } else {
            return false;
        }
    }

    /* ==================================
     * 檢查特定資料是否存在
     * 用於在新增資料前，檢查是否已有重複的資料
     * ================================== */

    function check_exist($table_name, $where_array = array()) {
        $this->db->select('*');
        $this->db->from($table_name);

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->db->where($value);
            } else {
                $this->db->where($index, $value);
            }
        }

        $query = $this->db->get();
        if ($query) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

    /* ==================================
     * 取得1筆資料-通用函數
     * ================================== */

    function get_one($table_name, $where_array = array(), $order_by = '', $order = 'desc') {
        $this->db->select('*');
        $this->db->from($table_name);
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->db->where($value);
            } else {
                $this->db->where($index, $value);
            }
        }

        if ($order_by != '') {
            $this->db->order_by($order_by, $order);
        }

        $this->db->limit(1);
        $query = $this->db->get();

        if ($query) {
            return $query->row();
        } else {
            return false;
        }
    }

    /* ==================================
     * 取得資料表通用函數-傳入where string
     * ================================== */

    function get_db_by_where_string($table_name, $where_string = 'true', $limit = 200, $order_by = '', $order = 'asc') {
        $this->db->select('*');
        $this->db->from($table_name);

        $this->db->where($where_string);
        if ($order_by != '') {
            $this->db->order_by($order_by, $order);
        }
        $this->db->limit($limit);
        $query = $this->db->get();

        return $query->result();
    }

    /* ==================================
     * 取得資料表特定欄位-
     * ================================== */

    function get_field($table_name, $field_name, $where_array = array(), $order_by = '', $order = 'asc', $limit = 0, $display_page = 1, $where_like_array = array()) {
        $this->db->select($field_name);
        $this->db->from($table_name);
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->db->where($value);
            } else {
                $this->db->where($index, $value);
            }
        }

        $i = 0;
        foreach ($where_like_array as $index => $value) {
            $i++;
            if ($i == 1) {
                $this->db->like($index, $value);
            } else {
                $this->db->or_like($index, $value);
            }
        }

        if ($order_by != '') {
            $this->db->order_by($order_by, $order);
        }

        if ($limit > 0) {
            $offset = ($display_page - 1) * $limit;
            $this->db->limit($limit);
            $this->db->offset($offset);
        }

        $query = $this->db->get();
        if ($query) {
            return $query->result();
        } else {
            return false;
        }
    }

    function get_one_field($table_name, $field_name, $where_array = array(), $order_by = '', $order = 'desc', $where_like_array = array()) {
        $this->db->select($field_name);
        $this->db->from($table_name);
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->db->where($value);
            } else {
                $this->db->where($index, $value);
            }
        }

        $i = 0;
        foreach ($where_like_array as $index => $value) {
            $i++;
            if ($i == 1) {
                $this->db->like($index, $value);
            } else {
                $this->db->or_like($index, $value);
            }
        }

        if ($order_by != '') {
            $this->db->order_by($order_by, $order);
        }

        $this->db->limit(1);
        $query = $this->db->get();

        if ($query) {
            if ($query->row()->$field_name) {
                return $query->row()->$field_name;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function get_heatmap_list($table_name, $where_array = array(), $order_by = '', $order = 'asc', $limit = 0, $display_page = 1, $where_like_array = array()) {
        $this->db->select('*,COUNT(ul_device_id) as weight');
        $this->db->from($table_name);
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->db->where($value);
            } else {
                $this->db->where($index, $value);
            }
        }

        $i = 0;
        foreach ($where_like_array as $index => $value) {
            $i++;
            if ($i == 1) {
                $this->db->like($index, $value);
            } else {
                $this->db->or_like($index, $value);
            }
        }

        if ($order_by != '') {
            $this->db->order_by($order_by, $order);
        }
        $this->db->group_by("ul_device_id, ul_lat, ul_lng, af_plan_id");

        if ($limit > 0) {
            $offset = ($display_page - 1) * $limit;
            $this->db->limit($limit);
            $this->db->offset($offset);
        }

        $query = $this->db->get();
        if ($query) {
            return $query->result();
        } else {
            return false;
        }
    }

}
