<?php

class Beacon_model extends CI_Model {

    protected $readonly_db;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    function get_unbinded_beacons(){
        $query = $this->readonly_db->query("
            select ap.ap_id, b.*, ap.ap_name, max(if(bp.b_id is null or ap.ap_id is null, 0, 1)) as is_bind
            from tp_beacon AS b
            left join (select * from tp_beacon_poi_relation where ap_id in (select ap_id from tp_ap_pois)) as bp on b.b_id=bp.b_id
            left join tp_ap_pois AS ap on ap.ap_id=bp.ap_id
            group by b.b_id;
		");

        return $query->result();
    }

    /* ==================================
     * 取得列表
     * ================================== */

    function get_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false, $group_by = '') {
        $this->readonly_db->select('b.*, f.*, ab.ab_name');
        $this->readonly_db->from('beacon AS b');
        $this->readonly_db->join('ap_floors AS f', 'b.af_id=f.af_id', 'LEFT');
        $this->readonly_db->join('ap_building AS ab', 'f.ab_id=ab.ab_id', 'LEFT');

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                'b_description'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        if ($group_by != '') {
            $this->readonly_db->group_by($group_by);
        }

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

    /* ==================================
     * 檢查狀態
     * ================================== */

    function check_status_list($order = '', $where_array = array(), $group_by = '') {
        $this->readonly_db->select('*');
        $this->readonly_db->from('beacon');

        $this->readonly_db->where('b_enabled', 'Y');

        $i = 0;
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } else {
                $i++;
                if ($i == 1) {
                    $this->readonly_db->where($index, $value);
                } else {
                    $this->readonly_db->or_where($index, $value);
                }
            }
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        if ($group_by != '') {
            $this->readonly_db->group_by($group_by);
        }

        $query = $this->readonly_db->get();
        return $query->result();
    }

}
