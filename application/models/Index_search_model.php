<?php

class Index_search_model extends CI_Model {

    protected $readonly_db;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    /* ==================================
     * 取得列表
     * ================================== */

    function get_topic_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        // $this->readonly_db->select('t.t_id,t.t_name,t.t_image,t.t_describe,t.t_lat,t.t_lng');
        $this->readonly_db->select('*,t.t_id');
        $this->readonly_db->from('tp_topic as t');
        $this->readonly_db->join('keyword_topic as kt', 't.t_id=kt.t_id', 'left');
        $this->readonly_db->join('keywords as ks', 'kt.kwd_id = ks.kwd_id', 'left');
        $this->readonly_db->group_by("t.t_id");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                't_name',
                't_describe',
                'kwd_name'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order,"desc");
        }

        // $this->readonly_db->group_by("ac_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

    function get_vr_photo_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        // $this->readonly_db->select('t.t_id,t.t_name,t.t_image,t.t_describe,t.t_lat,t.t_lng');
        $this->readonly_db->select('*');
        $this->readonly_db->from('department_vr_photo as dvp');
        $this->readonly_db->join('topic as t', 'dvp.t_id = t.t_id', 'left');
        $this->readonly_db->join('keyword_department_vr_photo as kd', 'dvp.dvp_id=kd.dvp_id', 'left');
        $this->readonly_db->join('keywords as ks', 'kd.kwd_id = ks.kwd_id', 'left');
        $this->readonly_db->group_by("dvp.dvp_id");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                'dvp_name',
                'kwd_name'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order,"desc");
        }

        // $this->readonly_db->group_by("ac_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

    function get_vr_video_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        // $this->readonly_db->select('t.t_id,t.t_name,t.t_image,t.t_describe,t.t_lat,t.t_lng');
        $this->readonly_db->select('*');
        $this->readonly_db->from('department_vr_video as dvv');
        $this->readonly_db->join('topic as t', 'dvv.t_id = t.t_id', 'left');
        $this->readonly_db->join('keyword_department_vr_video as kd', 'dvv.dvv_id=kd.dvv_id', 'left');
        $this->readonly_db->join('keywords as ks', 'kd.kwd_id = ks.kwd_id', 'left');
        $this->readonly_db->group_by("dvv.dvv_id");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                'dvv_name',
                'kwd_name'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order,"desc");
        }

        // $this->readonly_db->group_by("ac_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

    function get_poi_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('ap_id,ap_name,ap_lat,ap_lng,ac_title_zh,ac_image,ab_id');
        $this->readonly_db->from('tp_ap_pois as tap');
        $this->readonly_db->join('tp_ap_category AS tac', "tap.ac_id=tac.ac_id");
        $this->readonly_db->join('tp_ap_floors AS taf', "tap.af_id=taf.af_id");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                'ap_name'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        // $this->readonly_db->group_by("ac_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

    function get_viewpoint_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('*');
        // $this->readonly_db->select('*');
        $this->readonly_db->from('tp_viewpoints as tap');


         
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                'v_name'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        // $this->readonly_db->group_by("ac_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }


    function get_department_ar_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        // $this->readonly_db->select('tdar_id,tdar_name,tdar_findpho_path,tdar_description,tdar_lat,tdar_lng');
        $this->readonly_db->select('*');
        $this->readonly_db->from('tp_department_ar as tda');
        $this->readonly_db->join('tp_ap_pois AS tap', "tda.ap_puid=tap.ap_puid","LEFT");
        $this->readonly_db->join('tp_ap_floors AS taf', "tap.af_id=taf.af_id","LEFT");
        $this->readonly_db->join('keyword_department_ar as kda', 'tda.tdar_id=kda.ar_id', 'left');
        $this->readonly_db->join('keywords as ks', 'kda.kwd_id = ks.kwd_id', 'left');
        $this->readonly_db->group_by("tda.tdar_id");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                'tdar_name',
                'tdar_description',
                'kwd_name'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        // $this->readonly_db->group_by("ac_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }
}
