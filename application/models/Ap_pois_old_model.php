<?php

class Ap_pois_old_model extends CI_Model {

    protected $readonly_db;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    /* ==================================
     * 取得列表
     * ================================== */

    function get_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('ap.*,ac.*,er_id,e.e_id,e_title_zh,COUNT(e.e_id) as count');
        $this->readonly_db->from('ap_pois as ap');
        $this->readonly_db->join('ap_category as ac', 'ap.ac_id=ac.ac_id', 'inner');
        $this->readonly_db->join('exh_relation as er', 'ap.ap_id=er.ap_id', 'left');
        $this->readonly_db->join('exh as e', 'e.e_id=er.e_id', 'left');

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                'ap_name', 'ap_desc', 'ap_type'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        $this->readonly_db->group_by("ap_puid");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();

            if($query==FALSE){
                return 0;
            }

            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            if($query==FALSE){
                return NULL;
            }          
            return $query->result();
        }
    }

}
