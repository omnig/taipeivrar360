<?php

class Index_map_model extends CI_Model {

    protected $readonly_db;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    /* ==================================
     * 取得列表
     * ================================== */

    function get_topic_group_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false){
        $this->readonly_db->select('*');
        $this->readonly_db->from('topic_group as tg');
        
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                // 'ac_title'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        // $this->readonly_db->group_by("ac_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

    function get_topic_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        // $this->readonly_db->select('t_id,t_name,t_image,t_describe,t_lat,t_lng,t_image');
        $this->readonly_db->select('*');
        $this->readonly_db->from('topic as t');
        $this->readonly_db->join('topic_category as tc', 't.tc_id=tc.tc_id', 'LEFT');
        

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                // 'ac_title'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        // $this->readonly_db->group_by("ac_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

    function get_topic_list_latlong($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false,$lat = '',$lon = '') {

        $select_distance = "SQRT(POW(t_lat-{$lat},2)+POW(t_lng-{$lon},2)) AS distance";
        $select = "t_id,t_name,t_image,t_describe,t_lat,t_lng,t_image,{$select_distance},";
        $this->readonly_db->select($select);
        // $this->readonly_db->select('*');
        $this->readonly_db->from('tp_topic');
        

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                // 'ac_title'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        // $this->readonly_db->group_by("ac_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

    function get_poi_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $user_lat=null, $user_lng=null, $radius=100, $return_number_of_all_records = false) {
        // $this->readonly_db->select('ap_id,ap_name,ap_lat,ap_lng,ac_title_zh,ac_image,ab_id');
        // // $this->readonly_db->select('*');
        // $this->readonly_db->from('tp_ap_pois as tap');
        // $this->readonly_db->join('tp_ap_category AS tac', "tap.ac_id=tac.ac_id");
        // $this->readonly_db->join('tp_ap_floors AS taf', "tap.af_id=taf.af_id");
        $this->readonly_db->select('ap.*,tac.*,tdar.*,dai.*,b.*,ol.*');
        $this->readonly_db->from('ap_pois as ap');
        $this->readonly_db->join('ap_category AS tac', "ap.ac_id=tac.ac_id");
        $this->readonly_db->join('department_ar AS tdar', 'ap.tdar_id=tdar.tdar_id', 'LEFT');
        $this->readonly_db->join('department_ar_image AS dai', 'ap.dai_id=dai.dai_id', 'LEFT');
        $this->readonly_db->join('beacon_poi_relation AS bpr', 'ap.ap_id=bpr.ap_id', 'LEFT');
        $this->readonly_db->join('beacon AS b', 'b.b_id=bpr.b_id', 'LEFT');
        $this->readonly_db->join('optical_label_poi_relation AS opr', 'ap.ap_id=opr.ap_id', 'LEFT');
        $this->readonly_db->join('optical_label AS ol', 'ol.ol_id=opr.ol_id', 'LEFT');
        if(!empty($user_lat) && !empty($user_lng) && !empty($radius)){
            $this->readonly_db->having("( 6371 * acos( cos( radians($user_lat) ) * cos( radians( ap.ap_lat ) ) * cos( radians(ap.ap_lng) - radians($user_lng)) + sin(radians($user_lat)) * sin( radians(ap.ap_lat)))) <",$radius);
        }


         
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        // if ($keyword != '') {
        //     //要比對的keyword
        //     $entry_array = array(
        //         // 'ac_title'
        //     );
        //     $keyword_string = '';
        //     foreach ($entry_array as $entry) {
        //         if ($keyword_string == '') {
        //             $keyword_string = "`$entry` like '%$keyword%'";
        //         } else {
        //             $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
        //         }
        //     }

        //     $keyword_string = "($keyword_string)";
        //     $this->readonly_db->where($keyword_string);
        // }

        // if ($order != '') {
        //     $this->readonly_db->order_by($order);
        // }

        // $this->readonly_db->group_by("ac_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

    function get_viewpoint_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('*');
        // $this->readonly_db->select('*');
        $this->readonly_db->from('tp_viewpoints as tap');


         
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                'v_name'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        // $this->readonly_db->group_by("ac_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

    function get_poi_list_with_topic($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('ap_id,ap_name,ap_lat,ap_lng,ac_title_zh,ac_image,tab.ab_id');
        $this->readonly_db->from('tp_ap_pois as tap');
        $this->readonly_db->join('tp_ap_category AS tac', "tap.ac_id=tac.ac_id");
        $this->readonly_db->join('tp_ap_floors AS taf', "tap.af_id=taf.af_id");
        $this->readonly_db->join('tp_ap_building AS tab', "taf.ab_id=tab.ab_id");
        $this->readonly_db->join('tp_topic AS tt', "tab.ab_buid=tt.ab_buid");


         
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                // 'ac_title'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }



        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        // $this->readonly_db->group_by("ac_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }


    function get_poi_list_latlong($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false,$lat = '',$lon = '') {

        $select_distance = "SQRT(POW(ap_lat-{$lat},2)+POW(ap_lng-{$lon},2)) AS distance";
        $select = "ap_id,ap_name,ap_lat,ap_lng,ac_title_zh,ac_image,ab_id,{$select_distance},";
        $this->readonly_db->select($select);
        // $this->readonly_db->select('*');
        $this->readonly_db->from('tp_ap_pois as tap');
        $this->readonly_db->join('tp_ap_category AS tac', "tap.ac_id=tac.ac_id");
        $this->readonly_db->join('tp_ap_floors AS taf', "tap.af_id=taf.af_id");


         
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                // 'ac_title'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        // $this->readonly_db->group_by("ac_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

    function get_department_ar_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        // $this->readonly_db->select('tdar_id,tdar_name,tdar_findpho_path,tdar_description,tdar_lat,tdar_lng,ab_id');
        $this->readonly_db->select('*');
        $this->readonly_db->from('tp_department_ar as tda');
        $this->readonly_db->join('tp_ap_pois AS tap', "tda.ap_puid=tap.ap_puid","LEFT");
        $this->readonly_db->join('tp_ap_floors AS taf', "tap.af_id=taf.af_id","LEFT");
        // $this->readonly_db->from('tp_ap_pois as tap');
        
        // $this->readonly_db->join('tp_ap_category AS tac', "tap.ac_id=tac.ac_id");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                // 'ac_title'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        // $this->readonly_db->group_by("ac_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

    function get_department_ar_list_latlong($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false,$lat = '',$lon = '') {
        // $this->readonly_db->select('tdar_id,tdar_name,tdar_findpho_path,tdar_description,tdar_lat,tdar_lng,ab_id');
        $select_distance = "SQRT(POW(ap_lat-{$lat},2)+POW(ap_lng-{$lon},2)) AS distance";
        $select = "*,{$select_distance},";
        $this->readonly_db->select($select);
        $this->readonly_db->from('tp_department_ar as tda');
        $this->readonly_db->join('tp_ap_pois AS tap', "tda.ap_puid=tap.ap_puid","LEFT");
        $this->readonly_db->join('tp_ap_floors AS taf', "tap.af_id=taf.af_id","LEFT");
        // $this->readonly_db->from('tp_ap_pois as tap');
        
        // $this->readonly_db->join('tp_ap_category AS tac', "tap.ac_id=tac.ac_id");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                // 'ac_title'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        // $this->readonly_db->group_by("ac_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }


    function get_view_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('v_id,v_name,v_image,v_desc,v_address,v_web,v_tel,v_lat,v_lng,v_image,v_ticketinfo,v_opentime,v_travellinginfo,v_remarks,v_recommand');
        // $this->readonly_db->select('*');
        $this->readonly_db->from('tp_viewpoints');
        

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                // 'ac_title'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        // $this->readonly_db->group_by("ac_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

    function get_view_list_latlong($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false,$lat = '',$lon = '') {

        $select_distance = "SQRT(POW(v_lat-{$lat},2)+POW(v_lng-{$lon},2)) AS distance";
        $select = "v_id,v_name,v_image,v_desc,v_address,v_web,v_tel,v_lat,v_lng,v_image,v_ticketinfo,v_opentime,v_travellinginfo,v_remarks,v_recommand,{$select_distance}";
        $this->readonly_db->select($select);
        // $this->readonly_db->select('*');
        $this->readonly_db->from('tp_viewpoints');
        

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                // 'ac_title'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        // $this->readonly_db->group_by("ac_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }
}
