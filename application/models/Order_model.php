<?php

class Order_model extends CI_Model {

    protected $readonly_db;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    /* ==================================
     * 取得列表
     * ================================== */

    function get_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('*,SUM(rr_qty * oi_price) AS total_refund_request');
        $this->readonly_db->from('order AS o');
        $this->readonly_db->join('order_item AS oi', "oi.o_id=o.o_id");
        $this->readonly_db->join('refund_request AS rr', "rr.oi_id=oi.oi_id AND (`rr_status`='REQUEST' OR `rr_status`='AGREE' OR `rr_status`='CARGO_RETURN')", "LEFT");
        $this->readonly_db->join('user AS u', "o.u_id=u.u_id AND u_del='N'", "LEFT");
        $this->readonly_db->join('pay_settings AS ps', "o.ps_id=ps.ps_id", "LEFT");
        $this->readonly_db->where("o_del", "N");
        $this->readonly_db->where("o_order_number !=", "");
        $this->readonly_db->where("o_order_number IS NOT NULL");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                'o_buyer_name'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        $this->readonly_db->group_by("o.o_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

    function count_order_store($where_array = array()) {
        $this->readonly_db->select('*');
        $this->readonly_db->from('order AS o');
        $this->readonly_db->join('order_store AS os', "o.o_id=os.o_id");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        $this->readonly_db->order_by("os.s_id", "ASC");

        $query = $this->readonly_db->get();
        //$result = $query->result();
        return $query->num_rows();
    }

    /* ==================================
     * 取得訂單商品
     * ================================== */

    function get_order_item($where_array = array()) {
        $this->readonly_db->select('*,oi.oi_id AS oi_id,oi.s_id AS s_id');
        $this->readonly_db->from('order_item AS oi');
        $this->readonly_db->join('order_store AS os', "oi.s_id=os.s_id AND oi.o_id=os.o_id");
        $this->readonly_db->join('store AS s', "oi.s_id=s.s_id", "LEFT");
        $this->readonly_db->join('refund_request AS rr', "oi.oi_id=rr.oi_id AND (`rr_status`='REQUEST' OR `rr_status`='AGREE' OR `rr_status`='CARGO_RETURN')", "LEFT");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        $this->readonly_db->order_by("oi.oi_id", "ASC");

        $query = $this->readonly_db->get();
        $result = $query->result();

        if (!$result) {
            return $result;
        }

        foreach ($result as $id => $row) {
            $where_array = array(
                "oi_id" => $row->oi_id
            );

            $result[$id]->refund_history = $this->Common_model->get_db("refund_request", $where_array);
        }

        return $result;
    }

    /* ==================================
     * 以order_item取得store_owner
     * ================================== */

    function get_store_owner_by_order_item($where_array = array()) {
        $this->readonly_db->select('adm.*');
        $this->readonly_db->from('order_item AS oi');
        $this->readonly_db->join('store_owner AS so', "oi.s_id=so.s_id", "INNER");
        $this->readonly_db->join('admin AS adm', "so.adm_id=adm.adm_id AND adm.adm_enabled='Y'", "INNER");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        $this->readonly_db->group_by("adm.adm_id");

        $this->readonly_db->order_by("adm.adm_id", "ASC");

        $query = $this->readonly_db->get();
        $result = $query->result();

        return $result;
    }

    /* ==================================
     * 以order_store取得store_owner
     * ================================== */

    function get_store_owner_by_order_store($where_array = array()) {
        $this->readonly_db->select('*');
        $this->readonly_db->from('order_store AS os');
        $this->readonly_db->join('store_owner AS so', "os.s_id=so.s_id", "INNER");
        $this->readonly_db->join('admin AS adm', "so.adm_id=adm.adm_id AND adm.adm_enabled='Y'", "INNER");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        $this->readonly_db->group_by("adm.adm_id");

        $this->readonly_db->order_by("adm.adm_id", "ASC");

        $query = $this->readonly_db->get();
        $result = $query->result();

        return $result;
    }

    /* ==================================
     * 取得商品銷售紀錄
     * ================================== */

    function get_sell_history($p_id, $order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('*');
        $this->readonly_db->from('product AS p');
        $this->readonly_db->join('order_item AS oi', "oi.p_id=p.p_id", "INNER");
        $this->readonly_db->join('order AS o', "o.o_id=oi.o_id", "INNER");
        $this->readonly_db->join('order_store AS os', "o.o_id=os.o_id AND oi.s_id=os.s_id", "INNER");
        $this->readonly_db->where("p.p_id", $p_id);
        $this->readonly_db->where("o.o_order_number IS NOT NULL");
        $this->readonly_db->where("o.o_status_paid", "Y");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                ''
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        } else {
            $this->readonly_db->order_by("o_create_timestamp", "DESC");
        }

        //$this->readonly_db->group_by("o.o_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

}
