<?php

class Group_model extends CI_Model {

    protected $readonly_db;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    /* ==================================
     * 取得列表
     * ================================== */

    function get_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('*,COUNT(g.g_id) as count');
        $this->readonly_db->from('group AS g');
        $this->readonly_db->join('group_relation AS gr', "g.g_id=gr.g_id", "LEFT");
        $this->readonly_db->where('gr_del', 'N');
        $this->readonly_db->where('g_del', 'N');

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }
        $this->readonly_db->group_by("g.g_id");

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

    /* ==================================
     * 取得對應key之所有成員位置
     * ================================== */

    function get_group_user_location($g_ids = '', $start_timestamp) {

        $sql = "SELECT g_id,gr_name,gr_tel,gr_role,ul_device_id,ul_lat,ul_lng,ul_timestamp, af.af_plan_id,af_number FROM {$this->readonly_db->dbprefix('group_relation')}
                INNER JOIN {$this->readonly_db->dbprefix('user_last_location')} AS ul ON gr_device_id = ul_device_id
                LEFT JOIN {$this->readonly_db->dbprefix('ap_floors')} AS af ON ul.af_plan_id = af.af_plan_id
                WHERE g_id IN ({$g_ids}) AND gr_del = 'N' AND ul_timestamp >='{$start_timestamp}' ORDER BY g_id,gr_role";

        $query = $this->readonly_db->query($sql);

        return $query->result();
    }

}
