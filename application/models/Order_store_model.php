<?php

class Order_store_model extends CI_Model {

    protected $readonly_db;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    /* ==================================
     * 取得列表
     * ================================== */

    function get_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('*,SUM(rr_qty * oi_price) AS total_refund_request');
        $this->readonly_db->from('order_store AS os');
        $this->readonly_db->join('order AS o', "o.o_id=os.o_id AND o_del='N'");
        $this->readonly_db->join('order_item AS oi', "oi.o_id=o.o_id AND oi.s_id=os.s_id");
        $this->readonly_db->join('refund_request AS rr', "rr.oi_id=oi.oi_id AND (`rr_status`='REQUEST' OR `rr_status`='AGREE' OR `rr_status`='CARGO_RETURN')", "LEFT");
        $this->readonly_db->join('user AS u', "o.u_id=u.u_id AND u_del='N'", "LEFT");
        $this->readonly_db->join('pay_settings AS ps', "o.ps_id=ps.ps_id", "LEFT");
        $this->readonly_db->join('store AS s', "os.s_id=s.s_id AND s_del='N'", "LEFT");
        $this->readonly_db->where("os_del", "N");
        $this->readonly_db->where("o_order_number !=", "");
        $this->readonly_db->where("o_order_number IS NOT NULL");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                'o_buyer_name'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        $this->readonly_db->group_by("os.os_id");

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

    /* ==================================
     * 取得主訂單
     * ================================== */

    function get_order_list($where_array = array()) {
        $this->readonly_db->select('*');
        $this->readonly_db->from('order_store AS os');
        $this->readonly_db->join('order AS o', "o.o_id=os.o_id AND o_del='N'");
        $this->readonly_db->where("os_del", "N");
        $this->readonly_db->where("o_order_number !=", "");
        $this->readonly_db->where("o_order_number IS NOT NULL");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        $this->readonly_db->group_by("o.o_id");
        $query = $this->readonly_db->get();
        return $query->result();
    }

}
