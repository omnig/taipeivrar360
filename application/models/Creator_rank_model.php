<?php

class Creator_rank_model extends CI_Model {

    protected $readonly_db;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    /* ==================================
     * 取得列表
     * ================================== */

    function get_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select("*");
        $this->readonly_db->from('document as d');
        $this->readonly_db->join('user as u','u.u_id=d.d_uid','left');


        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }



    function get_union_list($order_by = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false){

        $query_string = 
            "   
            SELECT 
                min(create_timestamp) as firsr_create_timestamp,
                max(create_timestamp) as last_create_timestamp,
                sum(views) as views_sum,
                sum(collections) as collections_sum,
                sum(shares) as shares_sum,
                count(*) as upload_sum,
                u_id,
                user_name,
                creator_rank
            FROM
                (
                (SELECT 
                    t.t_name as t_name ,  
                    dvp.dvp_id as id,
                    dvp.dvp_name as name,
                    'photo' as type,
                    dvp.photo_type as subtype,
                    dvp.dvp_rep_img_path as image,
                    dvp.dvp_view_link as link,
                    dvp.dvp_create_timestamp as create_timestamp,
                    dvp.dvp_views as views,
                    dvp.dvp_collection as collections,
                    dvp.dvp_shares as shares,
                    dvp.collection_rank as collection_rank,
                    dvp.share_rank as share_rank,
                    u.u_id as u_id,
                    u.u_name as user_name,
                    u.creator_rank as creator_rank

                FROM `ty_department_vr_photo` as dvp 
                LEFT JOIN  `ty_topic` as t on t.t_id = dvp.t_id
                LEFT JOIN  `ty_user` as u on u.u_id = dvp.d_id
                WHERE dvp.dvp_del = 'N'  AND dvp.dvp_validation = 'Y' AND dvp.is_civil = 'Y'  AND u.u_id != ''
                )
            UNION ALL
                (SELECT 
                    t.t_name as t_name ,  
                    dvv.dvv_id as id,
                    dvv.dvv_name as name,
                    'video' as type,
                    'video' as subtype,
                    dvv.dvv_rep_img_path as image,
                    dvv.dvv_youtube_link as link,
                    dvv.dvv_create_timestamp as create_timestamp,
                    dvv.dvv_views as views,
                    dvv.dvv_collection as collections,
                    dvv.dvv_shares as shares,
                    dvv.collection_rank as collection_rank,
                    dvv.share_rank as share_rank,
                    u.u_id as u_id,
                    u.u_name as user_name,
                    u.creator_rank as creator_rank

                FROM `ty_department_vr_video` as dvv
                LEFT JOIN  `ty_topic` as t on t.t_id = dvv.t_id
                LEFT JOIN  `ty_user` as u on u.u_id = dvv.d_id
                WHERE dvv.dvv_del = 'N'  AND dvv.dvv_validation = 'Y' AND dvv.is_civil = 'Y' AND u.u_id != ''
                )
                ) AS New_Table
        ";

        if ($where_array) {
            foreach ($where_array as $key => $value) {
                if ($key==0) {
                    $query_string.=" WHERE ";
                }
                $query_string.= $value;
            }

        }

        $query_string .=" GROUP BY u_id ";

        if ($order_by) {
            $query_string .=" ORDER BY $order_by";
        }

        if($limit){
            $query_string .= " limit $limit";
        }


        $query = $this->readonly_db->query($query_string);
        $result = $query->result();
        // die(json_encode($result));

        return $query->result();
    }

}
