<?php

class Inventory_model extends CI_Model {

    protected $readonly_db;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    /* ==================================
     * 取得全站帳務
     * 取付款和出貨時間，較晚的時間為訂單完成時間
     * ================================== */

    function get_global_inventory($since, $until) {
        $sql = "SELECT os.os_process_fee_rate, `sb`.*, `s`.*, `o`.* "
                . "FROM `S_store_balance` AS `sb` "
                . "INNER JOIN `S_store` AS `s` ON `sb`.`s_id`=`s`.`s_id` "
                . "LEFT JOIN `S_order` AS `o` ON `sb`.`o_id`=`o`.`o_id` "
                . "LEFT JOIN `S_order_store` AS `os` ON `sb`.`os_id`=`os`.`os_id` "
                . "WHERE `s`.`s_del` = 'N' AND sb.`sb_balance_timestamp` >= '{$since}' AND sb.`sb_balance_timestamp` < '{$until}' "
                . "ORDER BY sb.`sb_balance_timestamp` ASC, sb.`sb_id` ASC";

        $query = $this->readonly_db->query($sql);
        return $query->result();
    }

    /* ==================================
     * 取得店家帳務
     * 取付款和出貨時間，較晚的時間為訂單完成時間
     * ================================== */

    function get_store_inventory($since, $until, $s_id = array()) {
        $s_id_filter = "";
        if ($s_id) {
            $s_id_filter = "sb.s_id IN (" . implode(",", $s_id) . ") AND ";
        }


        $sql = "SELECT os.os_process_fee_rate, `sb`.*, `s`.*, `o`.* "
                . "FROM `S_store_balance` AS `sb` "
                . "INNER JOIN `S_store` AS `s` ON `sb`.`s_id`=`s`.`s_id` "
                . "LEFT JOIN `S_order` AS `o` ON `sb`.`o_id`=`o`.`o_id` "
                . "LEFT JOIN `S_order_store` AS `os` ON `sb`.`os_id`=`os`.`os_id` "
                . "WHERE {$s_id_filter}`s`.`s_del` = 'N' AND sb.`sb_balance_timestamp` >= '{$since}' AND sb.`sb_balance_timestamp` < '{$until}' "
                . "ORDER BY sb.s_id ASC, sb.`sb_balance_timestamp` ASC, sb.`sb_id` ASC";

        $query = $this->readonly_db->query($sql);
        return $query->result();
    }

    /* ==================================
     * 取得入帳日報
     * 取當日付款時間
     * ================================== */

    function get_daily_income($since, $until, $where_array = array()) {
        $this->readonly_db->select('*');
        $this->readonly_db->from('order AS o');
        $this->readonly_db->join('pay_settings AS ps', "o.ps_id=ps.ps_id", "LEFT");
        $this->readonly_db->where("o_del", "N");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        $this->readonly_db->where("o_payment_timestamp >=", $since);
        $this->readonly_db->where("o_payment_timestamp <=", $until);

        $this->readonly_db->order_by("o_order_number", "ASC");

        $query = $this->readonly_db->get();
        return $query->result();
    }

}
