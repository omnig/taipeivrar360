<?php

class Product_faq_model extends CI_Model {

    protected $readonly_db;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    /* ==================================
     * 取得列表
     * ================================== */

    function get_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('*');
        $this->readonly_db->from('product_faq AS pf');
        $this->readonly_db->join('product AS p', "pf.p_id=p.p_id AND p_del='N'", "LEFT");
        $this->readonly_db->join('store AS s', "p.s_id=s.s_id AND s_del='N'", "LEFT");
        $this->readonly_db->join('admin AS adm', "pf.adm_id=adm.adm_id AND adm.adm_enabled='Y'", "LEFT");
        $this->readonly_db->join('user AS u', "pf.u_id=u.u_id AND u.u_enabled='Y' AND u.u_del='N'", "LEFT");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                'pf_question', 'pf_answer'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

    
    
    function get_reply_list($where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('*');
        $this->readonly_db->from('product_faq AS pf');
        $this->readonly_db->join('product_faq_reply AS pfr', "pf.pf_id=pfr.pf_id AND pfr_del='N'", "LEFT");
        $this->readonly_db->join('admin AS adm', "pfr.adm_id=adm.adm_id AND adm.adm_enabled='Y'", "LEFT");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }
    
    
    function get_one($where_array = array()) {
        $this->readonly_db->select('*');
        $this->readonly_db->from('product_faq AS pf');
        $this->readonly_db->join('product AS p', "pf.p_id=p.p_id AND p_del='N'", "LEFT");
        $this->readonly_db->join('store AS s', "p.s_id=s.s_id AND s_del='N'", "LEFT");
        $this->readonly_db->join('admin AS adm', "pf.adm_id=adm.adm_id AND adm.adm_enabled='Y'", "LEFT");
        $this->readonly_db->join('user AS u', "pf.u_id=u.u_id AND u.u_enabled='Y' AND u.u_del='N'", "LEFT");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        $query = $this->readonly_db->get();
        return $query->row();
    }

}
