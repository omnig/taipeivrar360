<?php

class Api_model extends CI_Model {

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();
    }

    /* ==================================
     * 搜尋POI入口
     * 若傳入經緯度不為0，則以距離排序
     * ================================== */

    function search_entrance($latitude = null, $longitude = null, $where_array = array(), $limit = 0, $display_page = 1, $group_by = false) {
        if ($latitude && $longitude) {
            $select_distance = "SQRT(POW(ap_lat-{$latitude},2)+POW(ap_lng-{$longitude},2)) AS distance";
            $select = "*,{$select_distance}";
        } else {
            $select = "*";
        }
        $this->db->select($select);
        $this->db->from("ap_pois as ap");
        $this->db->join('ap_floors as af', 'ap.af_id=af.af_id');

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->db->where($value);
            } else {
                $this->db->where($index, $value);
            }
        }

        if ($group_by) {
            $this->db->group_by($group_by);
        }

        if ($latitude && $longitude) {
            $this->db->order_by("distance", "ASC");
        } else {
            $this->db->order_by("ap_id", "ASC");
        }

        if ($limit > 0) {
            $offset = ($display_page - 1) * $limit;
            $this->db->limit($limit);
            $this->db->offset($offset);
        }

        $query = $this->db->get();

        if ($query) {
            return $query->result();
        } else {
            return false;
        }
    }

    /* ==================================
     * 搜尋點位類型(type)
     * 若傳入經緯度不為0，則以距離排序
     * ================================== */

    function search_nearby_type($latitude = null, $longitude = null, $where_array = array(), $limit = 0, $display_page = 1, $order_by = false, $group_by = false) {
        if ($latitude && $longitude) {
            $select_distance = "SQRT(POW(ap_lat-{$latitude},2)+POW(ap_lng-{$longitude},2)) AS distance";
            $select = "p.*, f.af_number,{$select_distance}";
        } else {
            $select = "*";
        }
        $this->db->select($select);
        $this->db->from("ap_pois as p");
        $this->db->join("ap_floors as f", "p.af_id=f.af_id", "LEFT");
        $this->db->join('ap_category as ac', 'p.ac_id=ac.ac_id', 'inner');

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->db->where($value);
            } else {
                $this->db->where($index, $value);
            }
        }

        if ($order_by) {
            $this->db->order_by($order_by);
        }
        if ($group_by) {
            $this->db->group_by($group_by);
        }

        if ($latitude && $longitude) {
            $this->db->order_by("distance", "ASC");
        } else {
            $this->db->order_by("ap_id", "ASC");
        }

        if ($limit > 0) {
            $offset = ($display_page - 1) * $limit;
            $this->db->limit($limit);
            $this->db->offset($offset);
        }

        $query = $this->db->get();

        if ($query) {
            return $query->result();
        } else {
            return false;
        }
    }
    
    /* ==================================
     * 從stopId取得route
     * ================================== */

    function get_route_by_stopId($where_array = array(), $order = '', $limit = 0, $skip = 0) {
        $this->db->select('r.*,pd.sequenceNo,s.Id AS stopID,s.goBack');
        $this->db->from('stop AS s');
        $this->db->join("path_detail AS pd", "s.Id=pd.stopId");
        $this->db->join("route AS r", "r.pathAttributeId=pd.pathAttributeId");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->db->where($value);
            } else {
                $this->db->where($index, $value);
            }
        }

        $this->db->group_by("r.Id");

        if ($limit > 0) {
            $this->db->limit($limit);
        }
        $query = $this->db->get();
        return $query->result();
    }
    
    /* ==================================
     * 從stop name取得route
     * ================================== */

    function get_route_by_stopName($where_array = array(), $order = '', $limit = 0, $skip = 0) {
        $this->db->select('r.*,pd.sequenceNo,s.Id AS stopID,s.goBack,s.nameZh AS stopName');
        $this->db->from('stop AS s');
        $this->db->join("path_detail AS pd", "s.Id=pd.stopId");
        $this->db->join("route AS r", "r.pathAttributeId=pd.pathAttributeId");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->db->where($value);
            } else {
                $this->db->where($index, $value);
            }
        }

        $this->db->group_by("r.Id,s.goBack");

        if ($limit > 0) {
            $this->db->limit($limit);
        }
        $query = $this->db->get();
        return $query->result();
    }

    /* ==================================
     * 從pathAttributeId取得Stop
     * ================================== */

    function get_stop_by_pathAttributeId($where_array = array(), $order = '', $limit = 0, $skip = 0) {
        $this->db->select('s.*,pd.sequenceNo,r.Id as routeId');
        $this->db->from('path_detail AS pd');
        $this->db->join("route AS r", "pd.pathAttributeId=r.pathAttributeId");
        $this->db->join("stop AS s", "s.Id=pd.stopId");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->db->where($value);
            } else {
                $this->db->where($index, $value);
            }
        }

        $this->db->group_by("s_id");

        $this->db->order_by($order);

        if ($limit > 0) {
            $this->db->limit($limit);
        }
        $query = $this->db->get();
        return $query->result();
    }

    /* ==================================
     * 取得站牌(以stopLocationId分群)
     * ================================== */

    function get_db_groupby($table_name, $where_array = array(), $order_by = '', $order = 'asc', $limit = 0, $display_page = 1, $group_by = false) {
        $this->db->select('*');
        $this->db->from($table_name);
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->db->where($value);
            } else {
                $this->db->where($index, $value);
            }
        }

        if ($group_by) {
            $this->db->group_by($group_by);
        }

        if ($order_by != '') {
            $this->db->order_by($order_by, $order);
        }

        if ($limit > 0) {
            $offset = ($display_page - 1) * $limit;
            $this->db->limit($limit);
            $this->db->offset($offset);
        }

        $query = $this->db->get();
        if ($query) {
            return $query->result();
        } else {
            return false;
        }
    }

    /* ==================================
     * 搜尋站牌
     * 若傳入經緯度不為0，則以距離排序
     * ================================== */

    function search_stop($latitude = null, $longitude = null, $where_array = array(), $limit = 0, $display_page = 1, $group_by = false) {
        if ($latitude && $longitude) {
            $select_distance = "SQRT(POW(latitude-{$latitude},2)+POW(longitude-{$longitude},2)) AS distance";

            $select = "*,{$select_distance}";
        } else {
            $select = "*";
        }
        $this->db->select($select);
        $this->db->from("stop");
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->db->where($value);
            } else {
                $this->db->where($index, $value);
            }
        }

        if ($group_by) {
            $this->db->group_by($group_by);
        }

        if ($latitude && $longitude) {
            $this->db->order_by("distance", "ASC");
        } else {
            $this->db->order_by("nameZh", "ASC");
        }

        if ($limit > 0) {
            $offset = ($display_page - 1) * $limit;
            $this->db->limit($limit);
            $this->db->offset($offset);
        }

        $query = $this->db->get();
        if ($query) {
            return $query->result();
        } else {
            return false;
        }
    }

    /* ==================================
     * 取得沿途到站時間
     * $after_sequenceNo: 只取回在此順序之前
     * $now: 現在時間(timestamp)
     * ================================== */

    function get_next_EstimateTime($routeID, $before_sequenceNo, $now) {
        $this->db->select('et.*,pd.sequenceNo,r.Id as routeId');
        $this->db->from('path_detail AS pd');
        $this->db->join("route AS r", "pd.pathAttributeId=r.pathAttributeId");
        $this->db->join("stop AS s", "s.Id=pd.stopId");
        $this->db->join("EstimateTime AS et", "et.StopID=s.Id AND et.RouteID=r.Id");

        $this->db->where("r.Id", $routeID);
        $this->db->where("pd.sequenceNo <=", $before_sequenceNo);

        $this->db->group_by("s.Id,r.Id");

        $this->db->order_by("pd.sequenceNo", "DESC");

        $query = $this->db->get();

        $time_list = $query->result();
        if (!$time_list) {
            return false;
        }

        $next = array();
        //取回下一站時間比這一站大的值
        $inc_time = 0;
        foreach ($time_list as $id => $row) {
            if ($id == 0) {
                $this_time = $row->EstimateTime;
                $inc_time = $row->EstimateTime;
            } elseif ($row->EstimateTime < 0) {
                continue;
            } elseif ($row->EstimateTime <= $this_time) {
                $this_time = $row->EstimateTime;
            } else {
                $time_diff = $now - strtotime($row->e_timestamp);
                $the_next = $row->EstimateTime + $inc_time - $time_diff;
                $inc_time += $row->EstimateTime;

                $next[] = ($the_next > 0) ? $the_next : -1;

                $this_time = 9999999999;

                //只取2筆
                if (count($next) >= 2 || $the_next <= 0) {
                    return $next;
                }
            }
        }

        return $next;
    }

}
