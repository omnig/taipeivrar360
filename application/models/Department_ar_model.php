<?php

class Department_ar_model
 extends CI_Model {

    protected $readonly_db;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    /* ==================================
     * 取得列表
     * ================================== */

    function get_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('*');
        $this->readonly_db->from('department_ar AS dar');
        $this->readonly_db->join('admin_group AS ag', 'dar.d_id=ag.ag_id', 'LEFT');
        
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            // $entry_array = array(
            //     'dvp_name'
            // );
            // $keyword_string = '';
            // foreach ($entry_array as $entry) {
            //     if ($keyword_string == '') {
            //         $keyword_string = "`$entry` like '%$keyword%'";
            //     } else {
            //         $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
            //     }
            // }

            // $keyword_string = "($keyword_string)";
            // $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }


    function get_share_image(){
        $this->readonly_db->select('tdar_name,tdar_content_type,tdar_content_path');
        $this->readonly_db->from('department_ar AS dar');

        $where_array = array(
            'tdar_content_type' => 'image',
            'is_civil' => 'N',
            'tdar_del'=>'N'
        );

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        $query = $this->readonly_db->get();
        return $query->result();

    }

    function get_share_video(){
        $this->readonly_db->select('tdar_name,tdar_content_type,tdar_content_path');
        $this->readonly_db->from('department_ar AS dar');

        $where_array = array(
            'tdar_content_type' => 'video',
            'is_civil' => 'N',
            'tdar_del'=>'N'
        );

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        $query = $this->readonly_db->get();
        return $query->result();
    }

    function get_share_three_model(){
        $this->readonly_db->select('tdar_id,tdar_name,tdar_content_type,tdar_content_path,tdar_content_wt3');
        $this->readonly_db->from('department_ar AS dar');

        $where_array = array(
            'tdar_content_type' => '3d_model',
            'is_civil' => 'N',
            'tdar_del'=>'N'
        );

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        $query = $this->readonly_db->get();
        return $query->result();
    }


    function get_mulit_ar($pattern_id){
        // $SQL = "
        //     select * from (
        //         select 
        //             ab.ab_id, ab.ab_ar_text, ab.ab_ar_url, ab.ab_ar_heigh, ab.ab_ar_vsize, ab.ab_cover_identify_image,
        //             da.tdar_id, da.tdar_name, da.tdar_content_type, da.tdar_content_path, da.tdar_is_transparent 
        //         from tp_ap_building ab
        //         join tp_department_ar da on da.tdar_state = 'Y' and da.tdar_del = 'N' and ab.tdar_id = da.tdar_id 
        //         where ab_enabled='Y' and dai_id in (
        //             select dai_id from tp_department_ar_image
        //             where dai_del='N' and dai_image_path like '%".$pattern_id."%'
        //         )
        //         order by ab.ab_create_timestamp desc
        //     ) as tmp
        //     group by tmp.tdar_id
        // ";
        $SQL = "
            select * from tp_department_ar_image
            join tp_department_ar on tdar_id = default_tdar_id and tdar_state = 'Y' and tdar_del = 'N'
            where dai_del = 'N' and dai_image_path like '%".$pattern_id."%'
        ";
        $query = $this->readonly_db->query($SQL);
        $result = $query->result();
        return $result;
    }

}
