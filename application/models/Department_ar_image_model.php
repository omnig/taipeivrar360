<?php

class Department_ar_image_model
 extends CI_Model {

    protected $readonly_db;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    /* ==================================
     * 取得列表
     * ================================== */

    function get_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('*');
        $this->readonly_db->from('department_ar_image AS dai');
        $this->readonly_db->join('admin_group AS ag', 'dai.d_id=ag.ag_id', 'LEFT');
        
        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            // $entry_array = array(
            //     'dvp_name'
            // );
            // $keyword_string = '';
            // foreach ($entry_array as $entry) {
            //     if ($keyword_string == '') {
            //         $keyword_string = "`$entry` like '%$keyword%'";
            //     } else {
            //         $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
            //     }
            // }

            // $keyword_string = "($keyword_string)";
            // $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }


    function get_share_image(){
        $this->readonly_db->select('tdar_name,tdar_content_type,tdar_content_path');
        $this->readonly_db->from('department_ar AS dar');

        $where_array = array(
            'tdar_content_type' => 'image',
            'is_civil' => 'N',
            'tdar_del'=>'N'
        );

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        $query = $this->readonly_db->get();
        return $query->result();

    }

    function get_share_video(){
        $this->readonly_db->select('tdar_name,tdar_content_type,tdar_content_path');
        $this->readonly_db->from('department_ar AS dar');

        $where_array = array(
            'tdar_content_type' => 'video',
            'is_civil' => 'N',
            'tdar_del'=>'N'
        );

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        $query = $this->readonly_db->get();
        return $query->result();
    }

    function get_share_three_model(){
        $this->readonly_db->select('tdar_id,tdar_name,tdar_content_type,tdar_content_path,tdar_content_wt3');
        $this->readonly_db->from('department_ar AS dar');

        $where_array = array(
            'tdar_content_type' => '3d_model',
            'is_civil' => 'N',
            'tdar_del'=>'N'
        );

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        $query = $this->readonly_db->get();
        return $query->result();
    }


    function get_mulit_ar($pattern_id){
        $SQL = "
            (
            SELECT ta.*, tag.ag_name as ta_name
            FROM ty_department_ar as ta
            LEFT JOIN ty_admin_group as tag on tag.ag_id = ta.d_id
            WHERE ta.is_civil = 'N' AND ta.tdar_del='N' AND tdar_image_path like '%".$pattern_id."%'
            )
            UNION
            (
            SELECT ta.*, u.u_name as ta_name
            FROM ty_department_ar as ta
            LEFT JOIN ty_user as u on u.u_id = ta.d_id
            WHERE ta.is_civil = 'Y' AND ta.tdar_del='N' AND tdar_image_path like '%".$pattern_id."%'
            )
            ORDER BY tdar_id
        ";
        $query = $this->readonly_db->query($SQL);
        $result = $query->result();
        return $result;
    }

}
