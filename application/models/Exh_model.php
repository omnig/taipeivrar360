<?php

class Exh_model extends CI_Model {

    protected $readonly_db;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    /* ==================================
     * 取得列表
     * ================================== */

    function get_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('e.*,ep.*,ea.*,er.ap_id');
        $this->readonly_db->from('exh as e');
        $this->readonly_db->join('exh_point as ep', 'ep.ep_id=e.ep_id', 'inner');
        $this->readonly_db->join('exh_area as ea', 'ea.ea_id=ep.ea_id', 'inner');
        $this->readonly_db->join('exh_relation as er', 'e.e_id=er.e_id', 'left');
        $this->readonly_db->where('e_del', 'N');

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        $this->readonly_db->group_by('e_id');

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();

            return $query->result();
        }
    }

    function get_exhibits($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('e.*,ep.*,ea.*,er.ap_id');
        $this->readonly_db->from('exh as e');
        $this->readonly_db->join('exh_point as ep', 'ep.ep_id=e.ep_id', 'inner');
        $this->readonly_db->join('exh_area as ea', 'ea.ea_id=ep.ea_id', 'inner');
        $this->readonly_db->join('exh_relation as er', 'e.e_id=er.e_id', 'left');

        $this->readonly_db->where('e_del', 'N');

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();

            return $query->result();
        }
    }

    function get_exh_relation($order = '', $keyword = '', $limit = 0, $skip = 0, $where_array = array(), $return_number_of_all_records = false, $group = '') {
        $this->readonly_db->select('e.*, ea.*, ep.*, ab.*, af.*, ap.*');
        $this->readonly_db->from('exh as e');
        $this->readonly_db->join('exh_relation as er', 'e.e_id=er.e_id', 'left');
        $this->readonly_db->join('exh_point as ep', 'e.ep_id=ep.ep_id', 'inner');
        $this->readonly_db->join('exh_area as ea', 'ep.ea_id=ea.ea_id', 'inner');
        $this->readonly_db->join('exh_area_relation as ear', 'ear.ea_id=ea.ea_id', 'inner');
        $this->readonly_db->join('ap_floors as af', 'af.af_id=ear.af_id', 'inner');
        $this->readonly_db->join('ap_building as ab', 'ab.ab_id=af.ab_id', 'inner');
        $this->readonly_db->join('ap_pois as ap', 'ap.ap_id=er.ap_id', 'left');

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        if ($group != '') {
            $this->readonly_db->group_by($group);
        }

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();

            return $query->result();
        }
    }

    function get_relation($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('ea.*, ab.*, af.*');
        $this->readonly_db->from('exh_area as ea');
        $this->readonly_db->join('exh_point as ep', 'ea.ea_id=ep.ea_id', 'inner');
        $this->readonly_db->join('exh_area_relation as ear', 'ea.ea_id=ear.ea_id', 'inner');
        $this->readonly_db->join('ap_floors as af', 'af.af_id=ear.af_id', 'inner');
        $this->readonly_db->join('ap_building as ab', 'ab.ab_id=af.ab_id', 'inner');

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();

            return $query->result();
        }
    }

}
