<?php

class Radar_model extends CI_Model {

    protected $readonly_db;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    /* ==================================
     * 取得列表
     * ================================== */

    function get_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('*');
        $this->readonly_db->from('radar as r');
        $this->readonly_db->where("r_del", "N");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

    /* ==================================
     * 取得列表
     * ================================== */

    function get_db_by_range($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $lat, $lng) {

        $select_distance = " ( 6371 * acos( cos( radians(r_lat) ) * cos( radians( {$lat} )) * cos( radians( {$lng} ) - radians(r_lng) ) + sin( radians(r_lat) ) * sin( radians( {$lat} ) ) ) ) * 1000 - r_range AS r_distance";
        $this->readonly_db->select("*,{$select_distance}");
        $this->readonly_db->from('radar as r');
        $this->readonly_db->where("r_del", "N");
        $this->readonly_db->having("r_distance < r_range");

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        if ($limit > 0) {
            $this->readonly_db->limit($limit);
        }
        if ($skip > 0) {
            $this->readonly_db->offset($skip);
        }
        $query = $this->readonly_db->get();
        return $query->result();
    }

}
