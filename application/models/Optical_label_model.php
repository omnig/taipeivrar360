<?php

class Optical_label_model extends CI_Model {

    protected $readonly_db;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    /* ==================================
     * 取得列表
     * ================================== */

    function get_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false, $group_by = '') {
        $this->readonly_db->select('ol.*');
        $this->readonly_db->from('optical_label AS ol');
        $this->readonly_db->where('ol.ol_del', 'N');

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                'ol_description'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        if ($group_by != '') {
            $this->readonly_db->group_by($group_by);
        }

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            return $query->num_rows();
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();
            return $query->result();
        }
    }

    

    function get_unbinded_optical_label(){
        $query = $this->readonly_db->query("
            select ol.*, ap.ap_name, max(if(op.ol_id is null or ap.ap_id is null, 0, 1)) as is_bind
            from tp_optical_label AS ol
            left join (select * from tp_optical_label_poi_relation where ap_id in (select ap_id from tp_ap_pois)) as op on ol.ol_id=op.ol_id
            left join tp_ap_pois AS ap on ap.ap_id=op.ap_id
            group by ol.ol_id;
		");

        return $query->result();
    }

}
