<?php

class Viewpoints_model extends AbstractPoints_model {

    protected $readonly_db;

    function __construct() {
        // 呼叫模型(Model)的建構函數
        parent::__construct();

        $this->readonly_db = $this->load->database('read_only', TRUE);
    }

    /* ==================================
     * 取得列表
     * ================================== */

    function get_list($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('v.*');
        $this->readonly_db->from('viewpoints as v');
        $this->readonly_db->where('v_del', 'N');

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        $this->readonly_db->group_by('v.v_id');

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            if ($query) {
                return $query->num_rows();
            } else {
                return 0;
            }
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();

            if ($query) {
                return $query->result();
            } else {
                return NULL;
            }
        }
    }

    function get_info($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false) {
        $this->readonly_db->select('p.p_id,p.p_place_id,p.p_gscore,p_type,v.v_name,v.v_name_en,v.v_image,v_address,v_address_en,v_web,v_tel,v_opentime,v.v_lat,v.v_lng,count(f_id) as tot_fav');
        $this->readonly_db->from('viewpoints as v');
        $this->readonly_db->join('point as p', 'p.v_id=v.v_id and p.p_type="v"', 'inner');
        $this->readonly_db->join('favorite as f', 'f.p_id=p.p_id', 'left');
        $this->readonly_db->where('v_enabled', 'Y');
        $this->readonly_db->where('v_del', 'N');

        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
                'v_name', 'v_name_en'
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        $this->readonly_db->group_by('p.p_id');

        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            if ($query) {
                return $query->num_rows();
            } else {
                return 0;
            }
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();

            if ($query) {
                return $query->result();
            } else {
                return NULL;
            }
        }
    }

    function get_info_with_distance($order = '', $keyword = '', $limit = 25, $skip = 0, $where_array = array(), $return_number_of_all_records = false, $user_lat = "", $user_lng = "", $object_distance = 50) {
        $table_name = "viewpoints";
        $this->readonly_db->select("p.p_id,p.p_place_id,p_type,v.v_name,v.v_name_en,v.v_image,v_address,v_address_en,v_web,v_tel,v_opentime,v.v_lat,v.v_lng,count(f_id) as tot_fav,( 6371 * acos( cos( radians($user_lat) ) * cos( radians( v_lat ) ) * cos( radians(v_lng) - radians($user_lng)) + sin(radians($user_lat)) * sin( radians(v_lat)))) AS distance");
        $this->readonly_db->from("$table_name as v");
        $this->readonly_db->join('point as p', 'p.v_id=v.v_id and p.p_type="v"', 'inner');
        $this->readonly_db->join('favorite as f', 'f.p_id=p.p_id', 'left');
        $this->readonly_db->where('v_enabled', 'Y');
        $this->readonly_db->where('v_del', 'N');
        $this->readonly_db->having('distance <=', $object_distance);
        $this->readonly_db->order_by('distance', 'asc');


        foreach ($where_array as $index => $value) {
            if (is_array($value)) {
                $this->readonly_db->where_in($index, $value);
            } elseif (is_numeric($index)) {
                $this->readonly_db->where($value);
            } else {
                $this->readonly_db->where($index, $value);
            }
        }

        if ($keyword != '') {
            //要比對的keyword
            $entry_array = array(
            );
            $keyword_string = '';
            foreach ($entry_array as $entry) {
                if ($keyword_string == '') {
                    $keyword_string = "`$entry` like '%$keyword%'";
                } else {
                    $keyword_string = $keyword_string . " or `$entry` like '%$keyword%'";
                }
            }

            $keyword_string = "($keyword_string)";
            $this->readonly_db->where($keyword_string);
        }

        if ($order != '') {
            $this->readonly_db->order_by($order);
        }

        $this->readonly_db->group_by('p.p_id');


        if ($return_number_of_all_records) {
            $query = $this->readonly_db->get();
            if ($query) {
                return $query->num_rows();
            } else {
                return 0;
            }
        } else {
            if ($limit > 0) {
                $this->readonly_db->limit($limit);
            }
            if ($skip > 0) {
                $this->readonly_db->offset($skip);
            }
            $query = $this->readonly_db->get();

            if ($query) {
                return $query->result();
            } else {
                return NULL;
            }
        }
    }

}
