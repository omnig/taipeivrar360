# CouchBase 安裝教學

## 作業環境-MacOS
pecl install igbinary
pecl install couchbase 


## 在php.ini 中加入 (正確位置請去phpinfo 看清楚)
extension=igbinary.so
extension=couchbase.so


## Ubuntu
sudo apt-get install php-pear
sudo apt-get install php5.6-dev
sudo apt install php5.6-bcmath
pecl install igbinary


wget http://packages.couchbase.com/releases/couchbase-release/couchbase-release-1.0-4-amd64.deb
sudo dpkg -i couchbase-release-1.0-4-amd64.deb
# Will install or upgrade packages
sudo apt-get update
sudo apt-get install libcouchbase-dev libcouchbase2-bin build-essential -y
sudo pecl install couchbase 


## 在php.ini 中加入 (正確位置請去phpinfo 看清楚)

cp /etc/php/5.6/mods-available/json.ini /etc/php/5.6/mods-available/couchbase.ini
ln -s /etc/php/5.6/mods-available/couchbase.ini /etc/php/5.6/apache2/conf.d/99-couchbase.ini

cp /etc/php/5.6/mods-available/json.ini /etc/php/5.6/mods-available/igbinary.ini
ln -s /etc/php/5.6/mods-available/igbinary.ini /etc/php/5.6/apache2/conf.d/98-igbinary.ini
sudo service apache2 restart




#Ubuntu 安裝放法
# Only needed during first-time setup:
wget http://packages.couchbase.com/releases/couchbase-release/couchbase-release-1.0-4-amd64.deb
sudo dpkg -i couchbase-release-1.0-4-amd64.deb
# Will install or upgrade packages
sudo apt-get update
sudo apt-get install libcouchbase-dev build-essential php-dev zlib1g-dev
sudo pecl install couchbase

cp /etc/php/5.6/mods-available/json.ini /etc/php/5.6/mods-available/couchbase.ini
ln -s /etc/php/5.6/mods-available/couchbase.ini /etc/php/5.6/apache2/conf.d/99-couchbase.ini

sudo service apache2 restart


## 使用方法

1. 認證登入

$bucketName = "bucket-name";

// Establish username and password for bucket-access
$authenticator = new \Couchbase\PasswordAuthenticator();
$authenticator->username('username')->password('password');

// Connect to Couchbase Server
$cluster = new CouchbaseCluster("couchbase://127.0.0.1");

// Authenticate, then open bucket
$cluster->authenticate($authenticator);
$bucket = $cluster->openBucket($bucketName);

2. 新增並存到Couchbase - Insert

// Store a document
echo "Storing u:king_arthur\n";
$result = $bucket->upsert('u:king_arthur', array(
    "email" => "kingarthur@couchbase.com",
    "interests" => array("African Swallows")
));

var_dump($result);


3. 檢索 - SELECT

// Retrieve a document
echo "Getting back u:king_arthur\n";
$result = $bucket->get("u:king_arthur");
var_dump($result->value);

4. 更新替換 - Update

// Replace a document

echo "Replacing u:king_arthur\n";
$doc = $result->value;
array_push($doc->interests, 'PHP 7');
$bucket->replace("u:king_arthur", $doc);
var_dump($result);

echo "Creating primary index\n";
// Before issuing a N1QL Query, ensure that there is
// is actually a primary index.
try {
    // Do not override default name; fail if it already exists, and wait for completion
    $bucket->manager()->createN1qlPrimaryIndex('', false, false);
    echo "Primary index has been created\n";
} catch (CouchbaseException $e) {
    printf("Couldn't create index. Maybe it already exists? (code: %d)\n", $e->getCode());
}

5. 條件查詢 - SELECT WHERE

// Query with parameters
$query = CouchbaseN1qlQuery::fromString("SELECT * FROM `$bucketName` WHERE \$p IN interests");
$query->namedParams(array("p" => "African Swallows"));
echo "Parameterized query:\n";
var_dump($query);
$rows = $bucket->query($query);
echo "Results:\n";
var_dump($rows)


### API Router

AnyplaceAPI.FULL_SERVER = "https://vr-manage.tycg.gov.tw/api/redirect_anyplace?ridect_url=http://210.200.142.171:9000/anyplace";

/**
 * MAPPING API
 */
AnyplaceAPI.Mapping = {};


1. [ok]AnyplaceAPI.Mapping.BUILDING_ALL = "/mapping/building/all_owner";
AnyplaceAPI.Mapping.BUILDING_ALL_URL = AnyplaceAPI.FULL_SERVER + AnyplaceAPI.Mapping.BUILDING_ALL;

2. [ok]AnyplaceAPI.Mapping.BUILDING_ADD = "/mapping/building/add";
AnyplaceAPI.Mapping.BUILDING_ADD_URL = AnyplaceAPI.FULL_SERVER + AnyplaceAPI.Mapping.BUILDING_ADD;

3. [ok]AnyplaceAPI.Mapping.BUILDING_UPDATE = "/mapping/building/update";
AnyplaceAPI.Mapping.BUILDING_UPDATE_URL = AnyplaceAPI.FULL_SERVER + AnyplaceAPI.Mapping.BUILDING_UPDATE;

4. [ok]AnyplaceAPI.Mapping.BUILDING_DELETE = "/mapping/building/delete";
AnyplaceAPI.Mapping.BUILDING_DELETE_URL = AnyplaceAPI.FULL_SERVER + AnyplaceAPI.Mapping.BUILDING_DELETE;

//要刪除Floor 



5. [ok]AnyplaceAPI.Mapping.FLOOR_ALL = "/mapping/floor/all";
AnyplaceAPI.Mapping.FLOOR_ALL_URL = AnyplaceAPI.FULL_SERVER + AnyplaceAPI.Mapping.FLOOR_ALL;

6. [ok]AnyplaceAPI.Mapping.FLOOR_ADD = "/mapping/floor/add";
AnyplaceAPI.Mapping.FLOOR_ADD_URL = AnyplaceAPI.FULL_SERVER + AnyplaceAPI.Mapping.FLOOR_ADD;

7. [1/2ok]AnyplaceAPI.Mapping.FLOOR_UPDATE = "/mapping/floor/update";
AnyplaceAPI.Mapping.FLOOR_UPDATE_URL = AnyplaceAPI.FULL_SERVER + AnyplaceAPI.Mapping.FLOOR_UPDATE;

8. [暫不實作]AnyplaceAPI.Mapping.FLOOR_DELETE = "/mapping/floor/delete";
AnyplaceAPI.Mapping.FLOOR_DELETE_URL = AnyplaceAPI.FULL_SERVER + AnyplaceAPI.Mapping.FLOOR_DELETE;


9. [ok]AnyplaceAPI.Mapping.FLOOR_PLAN_UPLOAD = "/mapping/floor/upload"
AnyplaceAPI.Mapping.FLOOR_PLAN_UPLOAD_URL = AnyplaceAPI.FULL_SERVER + AnyplaceAPI.Mapping.FLOOR_PLAN_UPLOAD;

10. [ok]AnyplaceAPI.Mapping.FLOOR_PLAN_DOWNLOAD = "/floorplans64/"
AnyplaceAPI.Mapping.FLOOR_PLAN_DOWNLOAD_URL = AnyplaceAPI.FULL_SERVER + AnyplaceAPI.Mapping.FLOOR_PLAN_DOWNLOAD;


11. [ok]AnyplaceAPI.Mapping.POIS_ALL_FLOOR = "/mapping/pois/all_floor";
AnyplaceAPI.Mapping.POIS_ALL_FLOOR_URL = AnyplaceAPI.FULL_SERVER + AnyplaceAPI.Mapping.POIS_ALL_FLOOR;

12. [ok]AnyplaceAPI.Mapping.POIS_ADD = "/mapping/pois/add";
AnyplaceAPI.Mapping.POIS_ADD_URL = AnyplaceAPI.FULL_SERVER + AnyplaceAPI.Mapping.POIS_ADD;

13. [ok]AnyplaceAPI.Mapping.POIS_UPDATE = "/mapping/pois/update";
AnyplaceAPI.Mapping.POIS_UPDATE_URL = AnyplaceAPI.FULL_SERVER + AnyplaceAPI.Mapping.POIS_UPDATE;

14. [ok]AnyplaceAPI.Mapping.POIS_DELETE = "/mapping/pois/delete";
AnyplaceAPI.Mapping.POIS_DELETE_URL = AnyplaceAPI.FULL_SERVER + AnyplaceAPI.Mapping.POIS_DELETE;


15. [ok]AnyplaceAPI.Mapping.CONNECTION_ALL_FLOOR = "/mapping/connection/all_floor";
AnyplaceAPI.Mapping.CONNECTION_ALL_FLOOR_URL = AnyplaceAPI.FULL_SERVER + AnyplaceAPI.Mapping.CONNECTION_ALL_
FLOOR;

16. [ok]AnyplaceAPI.Mapping.CONNECTION_ADD = "/mapping/connection/add";
AnyplaceAPI.Mapping.CONNECTION_ADD_URL = AnyplaceAPI.FULL_SERVER + AnyplaceAPI.Mapping.CONNECTION_ADD;

17. [似乎不用,尚未實作]AnyplaceAPI.Mapping.CONNECTION_UPDATE = "/mapping/connection/update";
AnyplaceAPI.Mapping.CONNECTION_UPDATE_URL = AnyplaceAPI.FULL_SERVER + AnyplaceAPI.Mapping.CONNECTION_UPDATE;

18. [ok]AnyplaceAPI.Mapping.CONNECTION_DELETE = "/mapping/connection/delete";
AnyplaceAPI.Mapping.CONNECTION_DELETE_URL = AnyplaceAPI.FULL_SERVER + AnyplaceAPI.Mapping.CONNECTION_DELETE;



19. [做假資料，暫不實作]AnyplaceAPI.Mapping.SIGN = "/mapping/accounts/sign";
AnyplaceAPI.Mapping.SIGN_URL = AnyplaceAPI.FULL_SERVER + AnyplaceAPI.Mapping.SIGN;

/**
 * Navigation API
 */

# Anyplace API - Navigation
20. POST           /anyplace/navigation/building/id                             controllers.AnyplaceNavigation.getBuildingById()

21. POST           /anyplace/navigation/pois/id                                 controllers.AnyplaceNavigation.getPoisById()

22. POST           /anyplace/navigation/route                                   controllers.AnyplaceNavigation.getNavigationRoute()
 
23. POST           /anyplace/navigation/route_xy                                controllers.AnyplaceNavigation.getNavigationRouteXY()




