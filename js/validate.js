
function on_submit(the_form) {
    var result = true;

    the_form.find('.validation').each(function () {
        var name = $(this).attr('for');
        var same_as = $(this).attr('same_as');

        if (same_as) {
            if (the_form.find('*[name=' + name + ']').val() !== the_form.find('*[name=' + same_as + ']').val()) {
                $(this).show();
                the_form.find('*[name=' + name + ']').focus();
                result = false;
            } else {
                $(this).hide();
            }
        } else {
            if (the_form.find('input[type=checkbox][name=' + name + ']').length > 0) {
                if (the_form.find('input[type=checkbox][name=' + name + ']').prop('checked')) {
                    $(this).hide();
                } else {
                    $(this).show();
                    the_form.find('input[type=checkbox][name=' + name + ']').focus();
                    result = false;
                }
            } else if (!the_form.find('*[name=' + name + ']').val()) {
                $(this).show();
                the_form.find('*[name=' + name + ']').focus();
                result = false;
            } else {
                $(this).hide();
            }

        }
    });

    return result;
}

/*
 * 依年、月的值，自動設定日期的select option
 */
var Set_date_select = function () {
    var selector;
    var days_of_month;

    return {
        get_days_of_month: function () {
            var year = $(this.selector).find(".query_year").val();
            var month = $(this.selector).find(".query_month").val();
            if (typeof year === 'undefined' || typeof month === 'undefined') {
                this.days_of_month = 0;
                return;
            }

            this.days_of_month = new Date(year, month, 0).getDate();
        },
        update_select_options: function () {
            var default_text = $(this.selector).find(".set_date").attr("default_text");
            var options = new Option(default_text, "");

            $(this.selector).find(".set_date").html("");
            $(this.selector).find(".set_date").append(options);

            for (var i = 1; i <= this.days_of_month; i++) {
                options = new Option(i.toString(), i.toString());
                $(this.selector).find(".set_date").append(options);
            }
        },
        bind: function () {
            $(this.selector).find(".query_year,.query_month").change(function () {
                Set_date_select.get_days_of_month();
                Set_date_select.update_select_options();
            });
        },
        init: function (selector) {
            this.selector = selector;
            this.get_days_of_month();
            this.update_select_options();

            this.bind();

            var initial_date = $(this.selector).attr('initial_date');
            if (typeof initial_date !== 'undefined' && initial_date != '') {
                var date = new Date(initial_date);

                $(this.selector).find(".query_year").val(date.getFullYear());
                $(this.selector).find(".query_month").val(date.getMonth() + 1).change();
                $(this.selector).find(".set_date").val(date.getDate());
            }
        }
    };
}();

//關閉window時，重整opener
function refreshParent() {
    if (window.opener) {
        window.opener.location.reload();
    }
}

$(document).ready(function () {
    Set_date_select.init(".set_date_select");

    //若有傳入 close_refresh_opener 參數，則關閉時重整opener
    var close_refresh_opener = getParameterByName('close_refresh_opener');

    if (close_refresh_opener) {
        window.onunload = refreshParent;
    }

});

/*
 * Facebook登入
 */

function fb_login(login_url) {
    var open_window = window.open(login_url);
}
