var slide_carousel = function () {
    var active_id;  //目前中央的圖片id
    var add_border = true;
    var auto_slide = true;
    var auto_slide_interval = 4000;
    var auto_slide_direction = 1;
    var auto_slide_timer = null;   //呼叫setInterval時回傳的變數，用於暫停自動輪播
    var slide_pagination = true;

    /*
     * 設定參數
     * 
     * @param {object} params
     * @returns {undefined}
     */
    function set_params(params) {
        if (typeof params === 'undefined') {
            return;
        }

        //讀取傳入參數
        add_border = (typeof params.add_border !== 'undefined') ? params.add_border : add_border;
        auto_slide = (typeof params.auto_slide !== 'undefined') ? params.auto_slide : auto_slide;
        auto_slide_interval = (typeof params.auto_slide_interval !== 'undefined') ? params.auto_slide_interval : auto_slide_interval;
        slide_pagination = (typeof params.slide_pagination !== 'undefined') ? params.slide_pagination : slide_pagination;
    }

    /*
     * 停止自動輪播事項
     * @returns {undefined}
     */
    function unbind_auto_slide() {
        if (auto_slide_timer) {
            clearInterval(auto_slide_timer);
            auto_slide_timer = null;
        }
    }
    /*
     * 啟動自動輪播事件
     * @returns {undefined}
     */
    function bind_auto_slide() {
        if (auto_slide && !auto_slide_timer) {
            auto_slide_timer = setInterval(function () {
                active_id += auto_slide_direction;
                if (active_id >= $('.slide_carousel .slide_item').length) {
                    active_id = $('.slide_carousel .slide_item').length - 1;
                    auto_slide_direction = -1;
                }

                if (active_id < 0) {
                    active_id = 0;
                    auto_slide_direction = 1;
                }

                action();
            }, auto_slide_interval);
        }
    }

    /*
     * 設定監聽事件
     * @returns {undefined}
     */
    function binding() {
        if (add_border) {
            $('.slide_carousel .slide_item > *').addClass("img-thumbnail");
        }

        var slide_items = $('.slide_carousel .slide_item');

        for (var id in slide_items) {
            slide_items.eq(id).data("slide_id", id);
            slide_items.eq(id).click(function () {
                var id = $(this).data("slide_id");

                active_id = id;
                action();
            });
        }

        bind_auto_slide();
        
        $('.slide_carousel').mouseover(unbind_auto_slide);
        $('.slide_carousel').mouseout(bind_auto_slide);
    }

    /*
     * 初始化分頁按鈕
     * @returns {undefined}
     */
    function init_slide_pagination() {
        var total_item = $('.slide_carousel .slide_item').length;
        var html = '';

        for (var i = 0; i < total_item; i++) {
            html += "<div class='slide_pagination_btn' onclick='slide_carousel.set_active(" + i + ")'></div>"
        }

        html = "<div class='slide_pagination'>" + html + "</div>";

        $('.slide_carousel').append(html);
    }

    /*
     * 更新輪播圖的class以達到動作效果
     */
    function action() {
        var imgs = $('.slide_carousel .slide_item');

        for (var i in imgs) {
            imgs.eq(i).removeClass("active prev prev2 next next2 hidden_prev hidden_next");

            switch (i - active_id) {
                case -2:
                    imgs.eq(i).addClass("prev2");
                    break;
                case -1:
                    imgs.eq(i).addClass("prev");
                    break;
                case 0:
                    imgs.eq(i).addClass("active");
                    break;
                case 1:
                    imgs.eq(i).addClass("next");
                    break;
                case 2:
                    imgs.eq(i).addClass("next2");
                    break;
                default:
                    if (i < active_id) {
                        imgs.eq(i).addClass("hidden_prev");
                    } else {
                        imgs.eq(i).addClass("hidden_next");
                    }
            }
        }

        $('.slide_carousel .slide_item').eq(active_id).addClass('active');

        $('.slide_carousel .slide_pagination .slide_pagination_btn.active').removeClass('active');
        $('.slide_carousel .slide_pagination .slide_pagination_btn').eq(active_id).addClass('active');
    }

    return {
        /*
         * 初始化
         */
        init: function (params, id) {
            set_params(params);

            init_slide_pagination();

            binding();

            if (id === 'undefined') {
                active_id = id;
            } else {
                active_id = 0;
            }

            action();
        },
        /*
         * 將特定照片設到中央
         */
        set_active: function (id) {
            active_id = id;
            action();
        }
    };
}();