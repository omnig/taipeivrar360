/*
 * 以ajax方式讀取商品清單
 */
var search_list = function () {
    var base_url = '/';
    var href_base_url = '/';
    var container = 'body';
    var where = {};      //過濾條件(含目前顯示頁數p，會轉成get參數)
    var search_list;   //載入後的展品清單
    var ep_id;   //區域id


    /*
     * 全部重置
     * @returns {undefined}
     */
    function reset_all() {
        where = {
            ep_id: 1,
            limit: 100
        };
        search_list = {};
    }

    /*
     * 重新、從頭載入
     * @returns {undefined}
     */
    function reload() {
        search_list = {};
    }

    /*
     * 顯示載入中
     * @returns {undefined}
     */
    function show_loading() {
        var loading_html = "<div class='loading text-center c-theme-color'><i class='fa fa-spinner fa-spin'></i> loading...</div>";

        $(container).append(loading_html);
    }

    /*
     * 移除載入中
     * @returns {undefined}
     */
    function hide_loading() {
        $(container).find('.loading').remove();
    }

    /*
     * 建立row單一商品的html
     * @param {object} product
     * @returns {String}
     */
    function build_search_html(exhibits) {

        var label_class = (exhibits.enabled === 'N') ? 'label-default' : 'label-warning';
        var span_style = (exhibits.enabled === 'N') ? 'color:lightgrey' : '';
        var enabled = (exhibits.enabled === 'N') ? ' [已關閉]' : '';

        var html =
                "<li class='dd-item' data-id=" + exhibits.id + ">" +
                "   <div class='dd-handle'>" +
                "       <label>" +
                "           <input type='hidden' name='e_order[]' value=" + exhibits.id + ">" +
                "           <label class='badge " + label_class + "'>" + (parseInt(exhibits.order) + 1) + "</label>" +
                "           <span style='" + span_style + "'>" + exhibits.name + enabled + "</span>" +
                "       </label>" +
                "   </div>" +
                "</li>";

        return html;
    }


    /*
     * 整理從server端取回的資料
     * @param {object} data
     * @returns {undefined}
     */
    function search_list_parser(data) {
        if (data.length === 0) {
            $(container).append('此區域尚未有展品資訊。');
        }
        for (var i in data) {
            var id = 'e_id_' + data[i].id;

            //過濾重複的資料不顯示
            if (typeof search_list[id] === 'undefined') {
                search_list[id] = data[i];

                //顯示商品清單
                var html = build_search_html(data[i]);
                $(container).append(html);
            }
        }
    }

    /*
     * ajax查詢
     * @returns {undefined}
     */
    function ajax_go() {
        var url = 'ajax_search_list';

        show_loading();

        $.ajax({
            url: url,
            data: where,
            dataType: 'JSON',
            success: function (ret) {
                if (ret.result) {
                    //顯示展品清單
                    search_list_parser(ret.data.exhibits);
                }
                hide_loading();
            }
        });
    }

    return {
        /*
         * 初始化
         * @param {string} base: 網站base_url
         * @param {string} href_base: 跳轉網址的base_url
         * @param {string} container_selector: 放置html的selector
         * @param {object} init_i18n: 初始化i18n字串
         * @returns {undefined}
         */
        init: function (base, href_base, container_selector) {
            base_url = base;
            href_base_url = href_base;
            container = container_selector;
            reset_all();
        },
        /*
         * 重置條件
         * @returns {undefined}
         */
        clear: function () {
            reload();
            $(container).html("");
        },
        /*
         * 設定過濾條件
         * @param {string} key
         * @param {string} value
         * @returns {undefined}
         */
        set_where: function (key, value) {
            if (value) {
                where[key] = value;
            }
        },
        /*
         * 查詢
         * 傳入參數:
         * where: 過濾條件
         * limit: 每頁數量
         */
        load: function () {
            ajax_go();
        }
    };
}();