var heatmap = function () {
    var init_keyword = '';  //初始時使用的keyword
    var container = 'body';
    var where = {};      //過濾條件(含目前顯示頁數p，會轉成get參數)
    var heatmap_list;   //載入後的資料清單
    var heatmap_gradient;
    var heatmap_radius;
    var heatmap_opacity;
    var heatmap_options = 'people';

    var markers = {};   //商店顯示marker
    var markers_circle = {};
    var info_windows = {};  //info_window清單
    var base_url;   //網站base_url
    var geocoder;
    var maptiler;
    var mapBounds;
    var lat = 25.0477387;
    var lng = 121.5170396;
    var infoWindow;
    var desc = [];
    var ii = 0;
    var heatmap;
    var heatmapData = [];
    var mapMinZoom = 16;
    var mapMaxZoom = 23;


    /*
     * 全部重置
     * @returns {undefined}
     */
    function reset_all() {
        heatmap_list = {};
        heatmapData = [];
        if (heatmap) {
            heatmap.setData(heatmapData);
            heatmap.setMap(null);
        }
    }


    /*
     * 繪製markers
     * @returns {undefined}
     */
    function draw_marker(idx, name, minor, blat, blng, range, content) {
        //繪製markers
        var icon = 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=' + minor + '|80bfff|000000';
        var image = new google.maps.MarkerImage(
                icon, null, null, null, null
                );
        info_windows[idx] = new google.maps.InfoWindow({
            content: content
        });

        markers[idx] = new google.maps.Marker({
            icon: image,
            title: name,
            info_window: info_windows,
            position: {lat: blat, lng: blng}
            //,map: map
        });

        //繪製最大距離
        markers_circle[idx] = new google.maps.Circle({
            strokeColor: '#0066cc',
            strokeOpacity: 0.5,
            strokeWeight: 1,
            fillColor: '#0066cc',
            fillOpacity: 0.15,
            zIndex: 10,
            map: map,
            info_window: info_windows,
            center: new google.maps.LatLng(blat, blng),
            radius: range
        });
        markers_circle[idx].setMap(map);

        //新增event
        google.maps.event.addListener(markers[idx], 'click', function () {
            infowindow_close();
            info_windows[idx].open(map, this);
        });
        google.maps.event.addListener(markers_circle[idx], 'click', function (ev) {
            infowindow_close();
            info_windows[idx].setPosition(ev.latLng);
            info_windows[idx].open(map, this);
        });
        map.addListener('mousedown', function () {
            infowindow_close();
        });
    }

    function infowindow_close() {
        for (var i in info_windows) {
            info_windows[i].close();
        }
    }


    /*
     * 繪製heatmap
     * @returns {undefined}
     */
    function draw_heatmap() {
        if (heatmapData.length != heatmap_list.length) {
            return;
        }
        // 生成熱圖圖層
        var heatmapArray = new google.maps.MVCArray(heatmapData);
        heatmap.setData(heatmapArray);
        heatmap.setMap(map);

    }


    function get_gradient(idx) {
        var gradient = null;
        idx = parseInt(idx);
        switch (idx) {
            case 0:
                gradient = null;
                break;
            case 1:
                gradient = [
                    'rgba(0, 255, 255, 0)',
                    'rgba(0, 255, 255, 1)',
                    'rgba(0, 191, 255, 1)',
                    'rgba(0, 127, 255, 1)',
                    'rgba(0, 63, 255, 1)',
                    'rgba(0, 0, 255, 1)',
                    'rgba(0, 0, 223, 1)',
                    'rgba(0, 0, 191, 1)',
                    'rgba(0, 0, 159, 1)',
                    'rgba(0, 0, 127, 1)',
                    'rgba(63, 0, 91, 1)',
                    'rgba(127, 0, 63, 1)',
                    'rgba(191, 0, 31, 1)',
                    'rgba(255, 0, 0, 1)'
                ];
                break;
            case 2:
                gradient = [
                    'transparent',
                    '#ffffff',
                    '#006837',
                    '#1a9850',
                    '#66bd63',
                    '#a6d96a',
                    '#d9ef8b',
                    '#fee08b',
                    '#fdae61',
                    '#f46d43',
                    '#d73027',
                    '#a50026',
                    '#aa0000',
                    '#000000'
                ];
                break;
        }
        return gradient;
    }


    function ajax_go() {
        heatmapData = [];
        //載入使用者經緯度資料
        var url = base_url + 'sa/heatmap/ajax_list';
        $.ajax({
            url: url,
            data: where,
            dataType: 'JSON',
            success: function (ret) {
                //設置資料
                heatmap_list = ret.data.locations;
                heatmap_list_parser(heatmap_list);
                if (ret.result) {
                    //繪製heatmap
                    if (heatmap_list.length > 0) {
                        draw_heatmap();
                        fit_bounce("heatmap");
                    }
                } else {
                    //顯示錯誤訊息
                    var result = ret.error_message;
                    $("#result").html("<label class='label label-danger'>" + result + "</label>");
                }
            }
        });
    }

    function fit_bounce(type) {
        if (heatmapData.length != heatmap_list.length)
            return;
        var bounds = new google.maps.LatLngBounds();
        if (type == "heatmap") {
            for (var i in heatmapData) {
                bounds.extend(heatmapData[i].location);
            }
        } else {
            for (var i in markers) {
                var myLatLng = markers[i].position;
                bounds.extend(myLatLng);
            }
        }
        map.fitBounds(bounds);
    }

    /*
     * 整理從server端取回的資料
     * @param {object} data
     * @returns {undefined}
     */
    function heatmap_list_parser(data) {
        if (heatmapData.length != 0 && heatmapData.length != heatmap_list.length)
            return;
        var total = 0;
        for (var i in data) {
            var latlng = new google.maps.LatLng(parseFloat(data[i].lat), parseFloat(data[i].lng));
            var weight = parseInt(data[i].weight);
            switch (heatmap_options) {
                case "people": //人流熱點
                    total += weight;
                    weight = 1;
                    var location_data = {location: latlng, weight: weight};
                    heatmapData.push(location_data);
                    break;
                case "stay": //停留熱點
                    weight = parseInt(data[i].weight);
                    if (weight > 5) {
                        var location_data = {location: latlng, weight: weight};
                        heatmapData.push(location_data);
                        total += weight;
                    }
                    break;
                default:
                    break;
            }
            //draw_marker(i, data[i].device_id, i, parseFloat(data[i].lat), parseFloat(data[i].lng), 0.5, data[i].timestamp);
        }
        heatmap_list = heatmapData;
        //顯示結果總筆數
        var result = "";
        if (total == 0) {
            result = "<label>查無資料，請重新設定搜尋內容。</label>";
        } else {
            result = "<label>搜尋結果共計 " + total + " 筆資料</label>";
        }
        $("#result").html(result);
    }


    /*
     * 載入tile map
     * @returns {undefined}
     */

    function load_tile_map(url_path, plan_id, bl_lat, bl_lng, tr_lat, tr_lng) {

        mapBounds = new google.maps.LatLngBounds(
                new google.maps.LatLng(bl_lat, bl_lng),
                new google.maps.LatLng(tr_lat, tr_lng));

        maptiler = new google.maps.ImageMapType({
            getTileUrl: function (coord, zoom) {
                var proj = map.getProjection();
                var z2 = Math.pow(2, zoom);
                var tileXSize = 256 / z2;
                var tileYSize = 256 / z2;
                var tileBounds = new google.maps.LatLngBounds(
                        proj.fromPointToLatLng(new google.maps.Point(coord.x * tileXSize, (coord.y + 1) * tileYSize)),
                        proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * tileXSize, coord.y * tileYSize))
                        );
                var y = coord.y;
                var x = coord.x >= 0 ? coord.x : z2 + coord.x
                if (mapBounds.intersects(tileBounds) && (mapMinZoom <= zoom) && (zoom <= mapMaxZoom))
                    return url_path + "/" + plan_id + "/" + zoom + "/" + x + "/" + y + ".png";
                else
                    return url_path + "/" + "none.png";
            },
            tileSize: new google.maps.Size(256, 256),
            isPng: true,
            opacity: 1.0
        });
        map.overlayMapTypes.insertAt(0, maptiler);

    }

    /* *************************
     * public functions
     * *************************/
    return {
        /*
         * 本物件初始化函式
         * @param {type} init_base_url
         * @returns {undefined}
         */
        init: function (init_base_url, container_selector) {
            base_url = init_base_url;
            container = container_selector;
        },
        ajax_go: function () {
            reset_all();
            ajax_go();
        },
        reset_all: function () {
            reset_all();
        },
        /*
         * google map js 載入後自動call back
         */
        initMap: function () {
            map = new google.maps.Map(document.getElementById('map'), {
                mapTypeControl: false,
                streetViewControl: false,
                center: {lat: lat, lng: lng},
                zoom: 15
            });

            infoWindow = new google.maps.InfoWindow({pixelOffset: new google.maps.Size(0, 0)});

            //設置heatmap
            heatmap = new google.maps.visualization.HeatmapLayer({
                //dissipating: true,
                //maxIntensity: 5,
                gradient: heatmap_gradient,
                radius: heatmap_radius,
                opacity: heatmap_opacity
            });
            heatmap.setMap(map);
        },
        set_option: function (option) {
            heatmap_options = option;
        },
        set_heatmap_radius: function (radius) {
            if (heatmap) {
                heatmap.set('radius', radius);
                heatmap_radius = radius;
            }
        },
        set_heatmap_opacity: function (opacity) {
            if (heatmap) {
                heatmap.set('opacity', opacity);
                heatmap_opacity = opacity;
            }
        },
        set_heatmap_gradient: function (val) {
            if (heatmap) {
                var gradient = get_gradient(val);
                heatmap.set('gradient', gradient);
                heatmap_gradient = gradient;
            }
        },
        /*
         * 設定過濾條件
         * @param {string} key
         * @param {string} value
         * @returns {undefined}
         */
        set_where: function (key, value) {
            //if (value) {
            where[key] = value;
            //}
        },
        set_limit: function (value) {
            where.limit = value;
        },
        set_tile_map: function (plan_id, bl_lat, bl_lng, tr_lat, tr_lng) {

            if (maptiler) {
                map.overlayMapTypes.setAt(0, null);
            }
            if (!plan_id)
            {
                return;
            }
            var tile_map_url = base_url + "map/tile";
            load_tile_map(tile_map_url, plan_id, bl_lat, bl_lng, tr_lat, tr_lng);
        }
    };
}();
