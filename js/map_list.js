var map_list = function () {
    var init_keyword = '';  //初始時使用的keyword
    var active_info_window = false;
    var bounce_marker;
    var bounce_marker_animation = null;
    var bounce_info_windows;
    var bounce_circle;  //bounce marker下繪製的圓圈
    var bounce_circle_radius = parseInt($('.range').val());    //bounce marker下的圓圈範圍
    var markers = {};   //商店顯示marker
    var markers_circle = {};
    var info_windows = {};  //info_window清單
    var base_url;   //網站base_url
    var geocoder;
    var keyword_search = false;
    var maptiler;
    var mapBounds;
    var lat = 23.1008629;
    var lng = 120.2838288;
    var mapMinZoom = 16;
    var mapMaxZoom = 23;
    var infoWindow;
    var desc = [];
    var nearby_idx = 0;
    var ii = 0;

    function clear_all() {
        //清除markers、circles
        for (var i in markers) {
            markers[i].setMap(null);
            markers_circle[i].setMap(null);
        }
        nearby_idx = 0;
        markers = [];
        markers_circle = [];
        info_windows = [];
    }
    /*
     * 重繪bounce marker所在位置下方圓圈
     * @param {type} lat
     * @param {type} lon
     * @returns {undefined}
     */
    function update_bounce_circle(lat, lon) {
        if (bounce_circle) {
            bounce_circle.setMap(null);
        }

        if (keyword_search) { //以keyword搜尋時，就不管位置距離
            return;
        }

        $('.search_input').val("");

        bounce_circle = new google.maps.Circle({
            strokeColor: '#ff6600',
            strokeOpacity: 0.5,
            strokeWeight: 1,
            fillColor: '#ff6600',
            fillOpacity: 0.15,
            zIndex: 10,
            map: map,
            center: new google.maps.LatLng(lat, lon),
            radius: bounce_circle_radius
        });
        bounce_circle.setMap(map);

        $('.newlat').html(lat);
        $('.newlng').html(lon);
    }

    /*
     * 按下marker時處理
     * @returns {undefined}
     */
    function on_bounce_marker_mousedown() {
        if (bounce_marker_animation !== google.maps.Animation.dr) {
            bounce_marker_animation = google.maps.Animation.dr;
            bounce_marker.setAnimation(bounce_marker_animation);
        }
    }

    /*
     * 拖拉marker期間畫面處理
     * @returns {undefined}
     */
    function on_bounce_marker_drag() {
        var new_lat = bounce_marker.position.lat();
        var new_lon = bounce_marker.position.lng();
        update_bounce_circle(new_lat, new_lon);
        bounce_info_windows.close();
    }

    /*
     * 拖拉marker放開時處理
     * @returns {undefined}
     */

    function on_bounce_marker_drop(keyword) {
        var new_lat = bounce_marker.position.lat();
        var new_lon = bounce_marker.position.lng();

        bounce_info_windows.open(map, bounce_marker);
        if (bounce_marker_animation !== google.maps.Animation.br) {
            bounce_marker_animation = google.maps.Animation.br;
            bounce_marker.setAnimation(bounce_marker_animation);
        }

        update_bounce_circle(new_lat, new_lon);
    }

    /*
     * 載入tile map
     * @returns {undefined}
     */

    function load_tile_map(idx, url_path, plan_id, bl_lat, bl_lng, tr_lat, tr_lng) {

        //清除tilemap
        if (maptiler) {
            map.overlayMapTypes.setAt(ii - 1, null);
        }

        mapBounds = new google.maps.LatLngBounds(
                new google.maps.LatLng(bl_lat, bl_lng),
                new google.maps.LatLng(tr_lat, tr_lng));

        maptiler = new google.maps.ImageMapType({
            getTileUrl: function (coord, zoom) {
                var proj = map.getProjection();
                var z2 = Math.pow(2, zoom);
                var tileXSize = 256 / z2;
                var tileYSize = 256 / z2;
                var tileBounds = new google.maps.LatLngBounds(
                        proj.fromPointToLatLng(new google.maps.Point(coord.x * tileXSize, (coord.y + 1) * tileYSize)),
                        proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * tileXSize, coord.y * tileYSize))
                        );
                var y = coord.y;
                var x = coord.x >= 0 ? coord.x : z2 + coord.x;
                if (mapBounds.intersects(tileBounds) && (mapMinZoom <= zoom) && (zoom <= mapMaxZoom)) {
                    return url_path + "/" + plan_id + "/" + zoom + "/" + x + "/" + y + ".png";
                } else {
                    return url_path + "/" + "none.png";
                }
            },
            tileSize: new google.maps.Size(256, 256),
            isPng: true,
            opacity: 1.0
        });
        map.overlayMapTypes.insertAt(ii, maptiler);
        ii++;

    }


    /*
     * 繪製markers
     * @returns {undefined}
     */
    function draw_marker(idx, name, minor, blat, blng, range, voltage, content) {

        //繪製markers
        var icon;
        if (voltage >= 2.3) {
            icon = "https://maps.google.com/mapfiles/ms/micons/green.png";
        } else if (voltage <= 2.2 && voltage >= 2.1) {
            icon = "http://maps.google.com/mapfiles/ms/micons/yellow.png";
        } else if (voltage <= 2.0) {
            icon = "http://maps.google.com/mapfiles/ms/micons/red.png";
        }
        if (voltage === '') {
            icon = "http://maps.google.com/mapfiles/ms/micons/blue.png";
        }

        var image = new google.maps.MarkerImage(
                icon, null, null, null, null
                );

        desc[idx] = content;

        var marker = new google.maps.Marker({
            icon: image,
            title: name,
            label: "" + minor + "",
            info_window: info_windows,
            position: {lat: blat, lng: blng},
            map: map
        });

        //繪製最大距離
        var marker_circle = new google.maps.Circle({
            strokeColor: '#0066cc',
            strokeOpacity: 0.5,
            strokeWeight: 1,
            fillColor: '#0066cc',
            fillOpacity: 0.15,
            zIndex: 10,
            map: map,
            center: new google.maps.LatLng(blat, blng),
            radius: range
        });
        markers_circle.push(marker_circle);

        //新增event
        google.maps.event.addListener(marker, 'click', (function (marker, idx) {
            return function () {
                infoWindow.setContent(desc[idx]);
                infoWindow.open(map, marker);
            };
        })(marker, idx));
        map.addListener('mousedown', function () {
            infoWindow.close();
        });

        markers.push(marker);
    }

    function draw_geofence_marker(idx, gname, glat, glng, range, enabled) {

        //繪製markers
        var icon, icon_color;
        if (enabled === "N") {
            icon_color = "cccccc";
            icon = "https://maps.google.com/mapfiles/kml/paddle/wht-blank-lv.png";
        } else {
            icon_color = "ffa366";
            icon = "http://maps.google.com/mapfiles/kml/paddle/ylw-blank-lv.png";
        }

        var image = new google.maps.MarkerImage(
                icon, null, null, null, null
                );

        var marker = new google.maps.Marker({
            icon: image,
            title: gname + "(" + range + "m)",
            label: gname,
            position: {lat: glat, lng: glng},
            map: map
        });

        //繪製最大距離
        var marker_circle = new google.maps.Circle({
            strokeColor: '#' + icon_color,
            strokeOpacity: 0.6,
            strokeWeight: 1,
            fillColor: '#' + icon_color,
            fillOpacity: 0.3,
            zIndex: 10,
            map: map,
            center: new google.maps.LatLng(glat, glng),
            radius: range
        });
        markers_circle.push(marker_circle);

        markers.push(marker);
    }


    function draw_views_marker(idx, name, vlat, vlng, type, enabled, content) {

        //繪製markers
        var icon_color;
        if (enabled === "N") {
            icon_color = "cccccc";
        } else {
            icon_color = "F5A9D0";
        }
        var type_str = type.substr(0, 1);
        var icon = 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=' + type_str + '|' + icon_color + '|000000';
        var image = new google.maps.MarkerImage(
                icon, null, null, null, null
                );

        var marker = new google.maps.Marker({
            icon: image,
            title: name,
            info_window: info_windows,
            position: {lat: vlat, lng: vlng},
            map: map
        });

        desc[idx] = content;

        //繪製最大距離
        var marker_circle = new google.maps.Circle({
            strokeColor: '#' + icon_color,
            strokeOpacity: 0.6,
            strokeWeight: 1,
            fillColor: '#' + icon_color,
            fillOpacity: 0.3,
            zIndex: 10,
            map: map,
            center: new google.maps.LatLng(vlat, vlng),
            radius: 0
        });
        markers_circle.push(marker_circle);

        //新增event
        google.maps.event.addListener(marker, 'click', (function (marker, idx) {
            return function () {
                infoWindow.setContent(desc[idx]);
                infoWindow.open(map, marker);
            };
        })(marker, idx));
        map.addListener('mousedown', function () {
            infoWindow.close();
        });

        markers.push(marker);
    }

    function draw_traffic_marker(idx, name, tlat, tlng, type, content) {

        //繪製markers
        var icon_color = '';
        var icon_str = '';
        switch (type) {
            case 'S':
                icon_color = '99ccff';
                icon_str = 'bus';
                break;
            case 'P':
                icon_color = 'ADDE63';
                icon_str = 'parking';
                break;
            case 'T':
                icon_color = 'ff9933';
                icon_str = 'bicycle';
                break;
        }
        var icon = 'https://chart.googleapis.com/chart?chst=d_map_pin_icon&chld=' + icon_str + '|' + icon_color;
        var image = new google.maps.MarkerImage(
                icon, null, null, null, null
                );

        desc[idx] = content;
        var marker = new google.maps.Marker({
            icon: image,
            title: name,
            info_window: info_windows,
            position: {lat: tlat, lng: tlng},
            map: map
        });

        //繪製最大距離
        var marker_circle = new google.maps.Circle({
            map: map,
            center: new google.maps.LatLng(tlat, tlng),
            radius: 0
        });
        markers_circle.push(marker_circle);

        //新增event
        google.maps.event.addListener(marker, 'click', (function (marker, idx) {
            return function () {
                infoWindow.setContent(desc[idx]);
                infoWindow.open(map, marker);
            };
        })(marker, idx));
        map.addListener('mousedown', function () {
            infoWindow.close();
        });

        markers.push(marker);
    }

    function infowindow_close() {
        for (var i in info_windows) {
            info_windows[i].close();
        }
    }


    function get_gradient(idx) {
        var gradient = null;
        idx = parseInt(idx);
        switch (idx) {
            case 0:
                gradient = null;
                break;
            case 1:
                gradient = [
                    'rgba(0, 255, 255, 0)',
                    'rgba(0, 255, 255, 1)',
                    'rgba(0, 191, 255, 1)',
                    'rgba(0, 127, 255, 1)',
                    'rgba(0, 63, 255, 1)',
                    'rgba(0, 0, 255, 1)',
                    'rgba(0, 0, 223, 1)',
                    'rgba(0, 0, 191, 1)',
                    'rgba(0, 0, 159, 1)',
                    'rgba(0, 0, 127, 1)',
                    'rgba(63, 0, 91, 1)',
                    'rgba(127, 0, 63, 1)',
                    'rgba(191, 0, 31, 1)',
                    'rgba(255, 0, 0, 1)'
                ];
                break;
            case 2:
                gradient = [
                    'transparent',
                    '#ffffff',
                    '#006837',
                    '#1a9850',
                    '#66bd63',
                    '#a6d96a',
                    '#d9ef8b',
                    '#fee08b',
                    '#fdae61',
                    '#f46d43',
                    '#d73027',
                    '#a50026',
                    '#aa0000',
                    '#000000'
                ];
                break;
        }
        return gradient;
    }


    function ajax_get_nearby_stop() {

        var url = base_url + "sa/nearby_traffic/stop";
        var where = {
            "minLatitude": map.getBounds().getSouthWest().lat().toString(),
            "maxLatitude": map.getBounds().getNorthEast().lat().toString(),
            "minLongitude": map.getBounds().getSouthWest().lng().toString(),
            "maxLongitude": map.getBounds().getNorthEast().lng().toString()
        };
        $.ajax({
            url: url,
            data: where,
            dataType: 'JSON',
            success: function (ret) {
                if (ret.result) {
                    //繪製marker
                    for (var i = 0; i < ret.data.length; i++) {
                        var obj = ret.data[i];
                        var content = '<iframe src="' + obj.url + '" style="border:0;height:300px"></iframe>';
                        draw_traffic_marker(nearby_idx, obj.name_zh + '(' + obj.name_en + ')', obj.latitude, obj.longitude, 'S', content);
                        nearby_idx++;
                    }
                }
            }
        });
    }

    function ajax_get_nearby_parking() {

        var url = base_url + "sa/nearby_traffic/parking";
        var where = {
            "minLatitude": map.getBounds().getSouthWest().lat().toString(),
            "maxLatitude": map.getBounds().getNorthEast().lat().toString(),
            "minLongitude": map.getBounds().getSouthWest().lng().toString(),
            "maxLongitude": map.getBounds().getNorthEast().lng().toString()
        };
        $.ajax({
            url: url,
            data: where,
            dataType: 'JSON',
            success: function (ret) {
                if (ret.result) {
                    //繪製marker
                    for (var i = 0; i < ret.data.length; i++) {
                        var obj = ret.data[i];
                        var content = "名稱：" + obj.name + "<br>地址：" + obj.address + "<br>大型車(一般)：" + obj.total_largecar + "<br>小型車(一般)：" + obj.total_car + "<br>小型車(身障)：" + obj.total_cardis + "<br>小型車(婦女)：" + obj.total_carwoman + "<br>小型車(綠能)：" + obj.total_cargreen + "<br>機車(一般)：" + obj.total_moto + "<br>機車(身障)：" + obj.total_motodis + "<br>收費時間：" + (obj.charge_time ? obj.charge_time : "無資料") + "<br>收費費率：" + (obj.charge_fee ? obj.charge_fee : "無資料") + (obj.available_car ? "<br>即時車位：" + obj.available_car : '');
                        if (obj.available_car) {
                            content += "<br>即時車位：" + obj.available_car;
                        }
                        draw_traffic_marker(nearby_idx, obj.name, obj.latitude, obj.longitude, 'P', content);
                        nearby_idx++;
                    }
                }
            }
        });
    }

    function ajax_get_nearby_tbike() {

        var url = base_url + "sa/nearby_traffic/tbike";
        var where = {
            "minLatitude": map.getBounds().getSouthWest().lat().toString(),
            "maxLatitude": map.getBounds().getNorthEast().lat().toString(),
            "minLongitude": map.getBounds().getSouthWest().lng().toString(),
            "maxLongitude": map.getBounds().getNorthEast().lng().toString()
        };
        $.ajax({
            url: url,
            data: where,
            dataType: 'JSON',
            success: function (ret) {
                if (ret.result) {
                    //繪製marker
                    for (var i = 0; i < ret.data.length; i++) {
                        var obj = ret.data[i];
                        var content = "站名：" + obj.name + "<br>地址：" + obj.address + "<br>格位數：" + obj.capacity + "<br>可借車輛數：" + obj.available_bike + "<br>可停空位數：" + obj.available_space + "<br>更新時間：" + obj.update_timestamp;
                        draw_traffic_marker(nearby_idx, obj.name, obj.latitude, obj.longitude, 'T', content);
                        nearby_idx++;
                    }
                }
            }
        });
    }

    function get_nearby_ajax() {

        if (!map.getBounds())
            return;

        clear_all();

        var val = $("#select-category").val();
        switch (val) {
            case "all":
                ajax_get_nearby_stop();
                ajax_get_nearby_parking();
                ajax_get_nearby_tbike();
                break;
            case 'stop':
                ajax_get_nearby_stop();
                break;
            case 'parking':
                ajax_get_nearby_parking();
                break;
            case 'tbike':
                ajax_get_nearby_tbike();
                break;
        }
    }


    /* *************************
     * public functions
     * *************************/
    return {
        /*
         * 本物件初始化函式
         * @param {type} init_base_url
         * @returns {undefined}
         */
        init: function (init_base_url, keyword) {
            base_url = init_base_url;
            if (typeof keyword !== 'undefined' && keyword !== '') {
                init_keyword = keyword;
                keyword_search = true;
            }
        },
        ajax_go: function () {
            google.maps.event.addListener(map, 'idle', function () {
                get_nearby_ajax();
            });
        },
        /*
         * google map顯示完成後call back
         */
        googlemap_callback: function () {

            google.maps.event.addListener(map, "click", function (event) {
                if (active_info_window) {
                    active_info_window.close();
                    active_info_window = false;
                }
            });
            var image = {
                url: base_url + 'images/bounce_marker.png',
                size: new google.maps.Size(33, 45),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 45)
            };
            bounce_info_windows = new google.maps.InfoWindow({
                content: '拖曳取得經緯度坐標'
            });
            bounce_marker = new google.maps.Marker({
                position: map.center,
                map: map,
                icon: image,
                info_window: bounce_info_windows,
                draggable: true
            });
            bounce_marker.setMap(map);
            bounce_info_windows.open(map, bounce_marker);
            bounce_marker.addListener('mousedown', on_bounce_marker_mousedown);
            bounce_marker.addListener('drag', on_bounce_marker_drag);
            bounce_marker.addListener('dragend', function () {
                on_bounce_marker_drop();
            });
            on_bounce_marker_drop(init_keyword);
        },
        /*
         * google map js 載入後自動call back
         */
        initMap: function () {
            map = new google.maps.Map(document.getElementById('map'), {
                mapTypeControl: false,
                streetViewControl: false,
                center: {lat: lat, lng: lng},
                zoom: 15
            });

            if (typeof googlemap_callback !== 'undefined') {
                googlemap_callback();
            }

            infoWindow = new google.maps.InfoWindow({pixelOffset: new google.maps.Size(0, 0)});

        },
        set_beacon: function (idx, name, minor, blat, blng, range, voltage, content) {
            draw_marker(idx, name, minor, blat, blng, range, voltage, content);
        },
        set_geofence: function (idx, name, glat, glng, range, enabled) {
            draw_geofence_marker(idx, name, glat, glng, range, enabled);
        },
        set_views: function (idx, name, vlat, vlng, type, enabled, content) {
            draw_views_marker(idx, name, vlat, vlng, type, enabled, content);
        },
        set_nearby: function (idx, name, vlat, vlng, type, content) {
            draw_views_marker(idx, name, vlat, vlng, type, content);
        },
        get_nearby: function () {
            get_nearby_ajax();
        },
        /*
         * 設定搜尋範圍
         */
        set_search_radius: function (radius) {
            bounce_circle_radius = radius;
            on_bounce_marker_drop();
        },
        /*
         * 返回使用者目前位置
         */
        center_to_user_location: function () {
            google_map.set_center_to_user_location();
            bounce_marker.setPosition(new google.maps.LatLng(google_map.get_user_lat(), google_map.get_user_lon()));
            on_bounce_marker_drop();
        },
        /*
         * 以地址搜尋附近商店
         */
        search_address: function (address) {

            geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                'address': address
            }, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    var map_options = {
                        center: results[0].geometry.location
                    };

                    map.setCenter(results[0].geometry.location);
                    bounce_marker.setPosition(results[0].geometry.location);
                    on_bounce_marker_drop();
                }
            });
        },
        /*
         更新經緯度
         */
        set_new_latlng: function (newlat, newlng) {
            if (!newlat) {
                newlat = $('.newlat')[0].innerText;
            }
            if (!newlng) {
                newlng = $('.newlng')[0].innerText;
            }
            $('.lat').val(newlat);
            $('.lng').val(newlng);
        },
        set_center: function (newlat, newlng) {
            var newlatlng = new google.maps.LatLng(newlat, newlng);
            map.panTo(newlatlng);
            bounce_marker.setPosition(newlatlng);
            on_bounce_marker_drop();
        },
        base_tile_map: function (idx, plan_id, bl_lat, bl_lng, tr_lat, tr_lng) {
            if (!plan_id)
            {
                return;
            }
            var tile_map_url = base_url + "map/tile";
            load_base_tile_map(idx, tile_map_url, plan_id, parseFloat(bl_lat), parseFloat(bl_lng), parseFloat(tr_lat), parseFloat(tr_lng));
        },
        set_tile_map: function (idx, plan_id, bl_lat, bl_lng, tr_lat, tr_lng) {
            if (!plan_id)
            {
                return;
            }
            var tile_map_url = base_url + "map/tile";
            load_tile_map(idx, tile_map_url, plan_id, parseFloat(bl_lat), parseFloat(bl_lng), parseFloat(tr_lat), parseFloat(tr_lng));
        },
        clear_all: function () {
            //清除markers、circles
            for (var i in markers) {
                markers[i].setMap(null);
                markers_circle[i].setMap(null);
            }
            markers = [];
            markers_circle = [];
            info_windows = [];
        },
        fit_bounce: function () {
            var bounds = new google.maps.LatLngBounds();
            for (var i in markers) {
                var myLatLng = markers[i].position;
                bounds.extend(myLatLng);
            }
            map.fitBounds(bounds);
        }
    };
}();

//var googlemap_callback = beacon_map.googlemap_callback;


/*
 * 互動操作
 */

$(document).ready(function () {
    $('.range').change(function () {
        var radius = parseInt($(this).val());
        beacon_map.set_search_radius(radius);
    });
    $('.searchaddr').change(function () {
        var radius = $(this).val();
        beacon_map.search_address(radius);
    });
});
