/*
 * 
 * google map地圖物件
 * @type google.maps.Map
 */
var map;

/*
 * google map callback function
 * @returns {undefined}
 */
function initMap() {
    google_map.init();
}

var google_map = function () {
    var lat = parseFloat($('.lat').val());
    var lon = parseFloat($('.lng').val());
    var zoom = 19;

    /*
     * 初始化google 地圖
     * @returns {undefined}
     */
    function init_map() {
        map = new google.maps.Map(document.getElementById('map'), {
            mapTypeControl: false,
            streetViewControl: false,
            center: {lat: lat, lng: lon},
            zoom: zoom
        });

        //draw_me(lat, lon);

        if (typeof googlemap_callback !== 'undefined') {
            googlemap_callback();
        }
    }

    function draw_me(latitude, longitude) {
        var markerLatLng = {lat: latitude, lng: longitude};

        new google.maps.Marker({
            position: markerLatLng,
            map: map
        });
    }

    return {
        /*
         * 初始化地圖
         */
        init: function () {
            /*
             //取得使用者目前經緯度
             user_location.get_user_latlon(function (latlon) {
             lat = parseFloat(latlon.lat);
             lon = parseFloat(latlon.lon);
             
             init_map();
             //取得經緯度後，啟動定期更新使用者座標
             user_location.regular_update_user_latlon(30000);
             });
             */
            init_map();
        },
        set_center: function (latitude, longitude) {
            map.setCenter({lat: latitude, lng: longitude});
        }
    };
}();