/*
 * 
 * 購物車
 * @type shopping_cart_L4.shopping_cartAnonym$1|Function
 */
var cart = function () {
    var base_url;
    var folder_name = "";
    var total_price;
    var total_items;
    var items;
    var cached_img = [];

    /*
     * 將購物車中的商品圖片快取保存，避免前端網頁跳動
     * @param {type} img_url
     * @returns {undefined}
     */
    function image_cache(img_url) {
        if (cached_img.indexOf(img_url) === -1) {
            cached_img[cached_img.length] = img_url;
            $('body').append("<div style='background-image:url(" + img_url + ");display:none;width:0px;height:0px;opacity:0'></div>");
        }
    }

    function update_cart_items() {
        //建立menu列商品清單
        var html = '';

        for (var row_id in items) {
            var show_invalid_text = '';
            var serial_style = '';
            if (items[row_id].invalid) {
                show_invalid_text = '<p><div class="label label-warning fa fa-exclamation-triangle"> 庫存不足！</div></p>';
            }

            if (parseInt(items[row_id].qty) < 0) {
                show_invalid_text = '<p><div class="label label-warning fa fa-exclamation-triangle"> 數量錯誤！</div></p>';
            }

            if (items[row_id].serial_enabled == 'Y') {
                serial_style = " - " + items[row_id].style;
            }

            html += "<li>" +
                    "   <div class='c-cart-menu-close'>" +
                    "       <a href='javascript:cart.remove_item(\"" + row_id + "\")' class='c-theme-link'>×</a>" +
                    "   </div>" +
                    "   <img src='" + items[row_id].p_image + "' />" +
                    "   <div class='c-cart-menu-content'>" +
                    "       <p>" + items[row_id].qty + " x" +
                    "           <span class='c-item-price c-theme-font'>NTD. " + $.number(items[row_id].price) + "</span>" +
                    "       </p>" +
                    "       <a href='" + base_url + folder_name + "product/" + items[row_id].id + "' class='c-item-name c-font-sbold'>" + items[row_id].name + serial_style + "</a>" +
                    show_invalid_text +
                    "   </div>" +
                    "</li>";
        }

        $('.cart_items_brief').html(html);

        //建立購物車列商品清單
        if ($('.cart_items').length > 0) {
            //移除原有的商品
            html = '';

            var i = 1;
            for (var row_id in items) {
                var item_inc = (items[row_id].qty < 99) ? items[row_id].qty + 1 : items[row_id].qty;
                var item_dec = (items[row_id].qty > 1) ? items[row_id].qty - 1 : items[row_id].qty;

                var show_invalid_text = '';
                var serial_style = '';

                if (items[row_id].invalid) {
                    show_invalid_text = '<p><div class="label label-warning fa fa-exclamation-triangle"> 庫存不足！</div></p>';
                }

                if (parseInt(items[row_id].qty) < 0) {
                    show_invalid_text = '<p><div class="label label-warning fa fa-exclamation-triangle"> 數量錯誤！</div></p>';
                }

                if (items[row_id].serial_enabled === 'Y') {
                    serial_style = items[row_id].style;
                }

                html += '<div class="row c-cart-table-row">' +
                        '   <h2 class="c-font-uppercase c-font-bold c-theme-bg c-font-white c-cart-item-title c-cart-item-first">商品 ' + i + '</h2>' +
                        '   <div class="col-md-2 col-sm-3 col-xs-5 c-cart-image">' +
                        '       <div class="rect_bg_image" style="background-image:url(' + items[row_id].p_image + ')"></div>' +
                        '   </div>' +
                        '   <div class="col-md-4 col-sm-9 col-xs-7 c-cart-desc">' +
                        '       <h3>' +
                        '           <a href="product/' + items[row_id].id + '" class="c-font-bold c-theme-link c-font-22 c-font-dark">' + items[row_id].name + '</a>' +
                        '       </h3>' +
                        '   </div>' +
                        '   <div class="col-md-1 col-sm-4 col-xs-4 c-cart-ref">' +
                        '       <p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">貨號</p>' +
                        '       <p>' + items[row_id].id + '</p>' +
                        '   </div>' +
                        '   <div class="col-md-1 col-sm-4 col-xs-4 c-cart-qty">' +
                        '       <p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">款式</p>' +
                        '       <p>' + serial_style + '</p>' +
                        '   </div>' +
                        '   <div class="col-md-1 col-sm-4 col-xs-4 c-cart-qty">' +
                        '       <p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">數量</p>' +
                        '       <div class="c-input-group c-spinner">' +
                        '           <input type="text" class="form-control c-item-' + i + '" onchange="cart.update_item(\'' + row_id + '\',$(this).val())" value="' + items[row_id].qty + '">' +
                        '           <div class="c-input-group-btn-vertical">' +
                        '               <button class="btn btn-default" type="button" onclick="cart.update_item(\'' + row_id + '\',' + item_inc + ')" data_input="c-item-' + i + '">' +
                        '                   <i class="fa fa-caret-up"></i>' +
                        '               </button>' +
                        '               <button class="btn btn-default" type="button" onclick="cart.update_item(\'' + row_id + '\',' + item_dec + ')" data_input="c-item-' + i + '">' +
                        '                   <i class="fa fa-caret-down"></i>' +
                        '               </button>' +
                        '           </div>' +
                        '       </div>' +
                        show_invalid_text +
                        '   </div>' +
                        '   <div class="col-md-1 col-sm-3 col-xs-6 c-cart-price">' +
                        '       <p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">單價</p>' +
                        '       <p class="c-cart-price c-font-bold">NTD. ' + $.number(items[row_id].price) + '</p>' +
                        '   </div>' +
                        '   <div class="col-md-1 col-sm-3 col-xs-6 c-cart-total">' +
                        '       <p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">小計</p>' +
                        '       <p class="c-cart-price c-font-bold">NTD. ' + $.number(items[row_id].price * items[row_id].qty) + '</p>' +
                        '   </div>' +
                        '   <div class="col-md-1 col-sm-12 c-cart-remove">' +
                        '       <a href="javascript:cart.remove_item(\'' + row_id + '\')" class="c-theme-link c-cart-remove-desktop">×</a>' +
                        '       <a href="javascript:cart.remove_item(\'' + row_id + '\')" class="c-cart-remove-mobile btn c-btn c-btn-md c-btn-square c-btn-red c-btn-border-1x c-font-uppercase">從購物車中移除</a>' +
                        '   </div>' +
                        '</div>';
                i++;
            }
            $('.cart_items').html(html);
        }
    }

    function update_cart_info() {
        $(".cart_num").html(total_items);
        $(".cart_total_price").html($.number(total_price));

        if (total_items > 0) {
            //建立商品清單
            update_cart_items();

            $('.have_cart_item').show();
            $('.no_cart_item').hide();
        } else {
            //顯示沒有商品
            $('.have_cart_item').hide();
            $('.no_cart_item').show();
        }
    }

    function callback_update_html(data) {
        total_price = data.total_price;
        total_items = data.total_items;
        items = data.items;

        update_cart_info();
    }

    function query_cart() {
        var url = base_url + 'ajax/cart_get';

        $.ajax({
            url: url,
            data: '',
            dataType: 'JSON',
            success: function (data) {
                if (data.result) {
                    callback_update_html(data.cart);
                }
            }
        });
    }

    return {
        /*
         * 將商品加入購物車
         */
        add_item: function (p_id, qty) {
            var url = base_url + 'ajax/cart_add';
            var params = {
                p_id: p_id
            };

            if (typeof qty !== 'undefined') {
                if (qty < 1 || qty > 99) {
                    return false;
                }
                params.qty = qty;
            }

            show_info("加入購物車", "處理中，請稍候...");

            $.ajax({
                url: url,
                data: params,
                method: 'POST',
                dataType: 'JSON',
                success: function (data) {
                    if (data.result) {
                        callback_update_html(data.cart);

                        show_info("加入購物車", "成功！", 800);
                    } else {
                        var error_message = "加入失敗！<br>可能商品不存在或已經下架...";

                        if (typeof data.error_message !== 'undefined') {
                            error_message = data.error_message;
                        }
                        show_info("加入購物車", error_message);
                    }
                }
            });
        },
        /*
         * 將商品自購物車中刪除
         */
        remove_item: function (row_id) {
            var url = base_url + 'ajax/cart_remove';

            $.ajax({
                url: url,
                data: {
                    row_id: row_id
                },
                method: 'POST',
                dataType: 'JSON',
                success: function (data) {
                    if (data.result) {
                        callback_update_html(data.cart);
                    }
                }
            });
        },
        /*
         * 更新購物車中數量
         */
        update_item: function (row_id, qty) {
            var url = 'ajax/cart_update';

            $.ajax({
                url: url,
                data: {
                    row_id: row_id,
                    qty: qty
                },
                method: 'POST',
                dataType: 'JSON',
                success: function (data) {
                    if (data.result) {
                        callback_update_html(data.cart);
                    } else {
                        var error_message;

                        if (typeof data.error_message !== 'undefined') {
                            error_message = data.error_message;
                        } else {
                            error_message = "加入失敗！<br>可能商品不存在或已經下架...";
                        }

                        if (typeof data.error_message !== 'undefined') {
                            error_message = data.error_message;
                        }
                        show_info("加入購物車", error_message);
                    }

                }
            });
        },
        /*
         * 初始化
         */
        init: function (init_url) {
            base_url = init_url;

            query_cart();
        },
        set_folder_name: function (folder) {
            folder_name = folder;
        }
    };
}();

/*
 * 追蹤清單/我的最愛管理
 * @type Function|shopping_cart_L238.shopping_cartAnonym$7
 */
var favorite = function () {

    var base_url;
    var user_id;

    /*
     * 加入最愛
     */

    function add_favorite(p_id) {
        var url = base_url + 'ajax/add_favorite';

        $.ajax({
            url: url,
            data: {
                p_id: p_id
            },
            method: 'POST',
            dataType: 'JSON',
            success: function (data) {
                if (data.result) {
                    show_info("加入追蹤", "成功", 800);
                } else if (data.error_message) {
                    show_info("加入追蹤失敗", data.error_message);
                }
            }
        });
    }

    function add_store_favorite(s_id) {
        var url = base_url + 'ajax/add_favorite';

        $.ajax({
            url: url,
            data: {
                s_id: s_id
            },
            method: 'POST',
            dataType: 'JSON',
            success: function (data) {
                if (data.result) {
                    show_info("加入追蹤", "成功", 800);
                } else if (data.error_message) {
                    show_info("加入追蹤失敗", data.error_message);
                }
            }
        });
    }

    /*
     * 從我的最愛中移除
     */
    function remove_favorite(p_id) {
        var url = base_url + 'ajax/remove_favorite';

        $.ajax({
            url: url,
            data: {p_id: p_id},
            method: 'POST',
            dataType: 'JSON',
            success: function (data) {
                if (data.result) {
                    show_info("從追蹤清單移除", "成功", 800);
                    setTimeout(function () {
                        location.reload();
                    }, 800);
                }
            }
        });
    }

    function remove_store_favorite(s_id) {
        var url = base_url + 'ajax/remove_favorite';

        $.ajax({
            url: url,
            data: {s_id: s_id},
            method: 'POST',
            dataType: 'JSON',
            success: function (data) {
                if (data.result) {
                    show_info("從追蹤清單移除", "成功", 800);
                    setTimeout(function () {
                        location.reload();
                    }, 800);
                }
            }
        });
    }

    /*
     * 未登入時，要求使用者先登入
     */
    function require_login() {
        show_info("", "請先登入", 800);
        setTimeout(function () {
            $('#login-form').modal();
        }, 800);
    }

    /*
     * 觸發事件
     */
    function binding() {
        if (user_id > 0) {
            $('.add_favorite_btn').click(function () {
                add_favorite($(this).attr("p_id"));
            });
            $('.remove_favorite_btn').click(function () {
                remove_favorite();
            });
        } else {
            $('.add_favorite_btn,.remove_favorite_btn').click(require_login);
        }
    }

    return {
        /*
         * 初始化
         */
        init: function (base, u_id) {
            base_url = base;
            user_id = u_id;

            binding();
        },
        add: function (p_id) {
            if (user_id > 0) {
                add_favorite(p_id);
            } else {
                require_login();
            }
        },
        addstore: function (s_id) {
            if (user_id > 0) {
                add_store_favorite(s_id);
            } else {
                require_login();
            }
        },
        remove: function (p_id) {
            if (user_id > 0) {
                remove_favorite(p_id);
            } else {
                require_login();
            }
        },
        removestore: function (s_id) {
            if (user_id > 0) {
                remove_store_favorite(s_id);
            } else {
                require_login();
            }
        }
    };
}();