//更新管理員
var store_admin = function () {
    var s_id;

    /*
     * 觸發事件
     */
    function binding() {
        var timeout;

        $(".admin_search").on("keyup", function () {
            var keyword = $(this).val();

            //避免太快呼叫造成負擔
            clearTimeout(timeout);
            timeout = setTimeout(function () {
                $.get("ajax_admin_search?s_id=" + s_id + "&keyword=" + keyword, function (data) {
                    var html = '';

                    if (data.length > 0) {
                        for (var i in data) {
                            html += item_html(data[i].id, data[i].name, "fa fa-plus", "javascript:store_admin.add(" + data[i].id + ")", "default");
                        }
                    }

                    //更新html
                    $('.admin_search_result').html(html);
                }, "json");

            }, 100);
        });
    }

    /*
     * 組合管理員清單中單一項目的html
     * @param {int} id
     * @param {string} name
     * @param {string} btn_class
     * @param {string} onclick
     * @returns {String}
     */
    function item_html(id, name, btn_class, onclick, color) {
        var html =
                "<div class='pull-left btn " + color + "'>" +
                name +
                " <a href='" + onclick + "' style='color:#c00'>" +
                "<i class='" + btn_class + "'></i>" +
                "</a>" +
                "</div>";

        return html;
    }

    return {
        /*
         * 新增管理員
         * @param {int} adm_id
         * @returns {undefined}
         */
        add: function (adm_id) {
            $.get("ajax_owner_add?s_id=" + s_id + "&adm_id=" + adm_id, function (data) {
                store_admin.update();
            });
        },
        /*
         * 刪除管理員
         * @param {int} adm_id
         * @returns {undefined}
         */
        remove: function (adm_id) {
            $.get("ajax_owner_del?s_id=" + s_id + "&adm_id=" + adm_id, function (data) {
                store_admin.update();
            });
        },
        /*
         * 更新管理員清單
         */
        update: function () {
            $.get("ajax_owner_list?s_id=" + s_id, function (data) {
                var html = '';

                if (data.length > 0) {
                    //有管理員
                    for (var i in data) {
                        html += item_html(data[i].id, data[i].name, "fa fa-remove", "javascript:store_admin.remove(" + data[i].id + ")", "green");
                    }
                }

                //更新html
                $('.owner_list').html(html);

            }, "json");
        },
        /*
         * 初始化
         */
        init: function (store_id) {
            s_id = store_id;
            this.update();
            binding();
        }
    };
}();