var traffic_map = function () {
    var init_keyword = '';  //初始時使用的keyword
    var active_info_window = false;
    var bounce_marker;
    var bounce_marker_animation = null;
    var bounce_info_windows;

    var markers = {};   //商店顯示marker
    var markers_circle = {};
    var info_windows = {};  //info_window清單
    var base_url;   //網站base_url
    var geocoder;
    var keyword_search = false;
    var maptiler;
    var mapBounds;
    var lat = 24.156744;
    var lng = 120.665551;
    var mapMinZoom = 16;
    var mapMaxZoom = 23;
    var infoWindow;
    var desc = [];
    var nearby_idx = 0;
    var ii = 0;

    var stops;
    var stop_data = [];
    var goBack;
    var intervalId_route;
    var busInfo = [];
    var intervalId_businfo;
    var visible_bus = false;
    var visible_ibike = false;
    var visible_parking = false;

    function clear_all() {
        //清除markers、circles
        for (var i in markers) {
            markers[i].setMap(null);
            //markers_circle[i].setMap(null);
        }
        nearby_idx = 0;
        markers = [];
        markers_circle = [];
        info_windows = [];
    }
    /*
     * 重繪bounce marker所在位置下方圓圈
     * @param {type} lat
     * @param {type} lon
     * @returns {undefined}
     */
    function update_bounce_circle(lat, lon) {
        if (bounce_circle) {
            bounce_circle.setMap(null);
        }

        if (keyword_search) { //以keyword搜尋時，就不管位置距離
            return;
        }

        $('.search_input').val("");

        bounce_circle = new google.maps.Circle({
            strokeColor: '#ff6600',
            strokeOpacity: 0.5,
            strokeWeight: 1,
            fillColor: '#ff6600',
            fillOpacity: 0.15,
            zIndex: 10,
            map: map,
            center: new google.maps.LatLng(lat, lon),
            radius: bounce_circle_radius
        });
        bounce_circle.setMap(map);

        $('.newlat').html(lat);
        $('.newlng').html(lon);
    }

    /*
     * 按下marker時處理
     * @returns {undefined}
     */
    function on_bounce_marker_mousedown() {
        if (bounce_marker_animation !== google.maps.Animation.dr) {
            bounce_marker_animation = google.maps.Animation.dr;
            bounce_marker.setAnimation(bounce_marker_animation);
        }
    }

    /*
     * 拖拉marker期間畫面處理
     * @returns {undefined}
     */
    function on_bounce_marker_drag() {
        var new_lat = bounce_marker.position.lat();
        var new_lon = bounce_marker.position.lng();
        update_bounce_circle(new_lat, new_lon);
        bounce_info_windows.close();
    }

    /*
     * 拖拉marker放開時處理
     * @returns {undefined}
     */

    function on_bounce_marker_drop(keyword) {
        var new_lat = bounce_marker.position.lat();
        var new_lon = bounce_marker.position.lng();

        bounce_info_windows.open(map, bounce_marker);
        if (bounce_marker_animation !== google.maps.Animation.br) {
            bounce_marker_animation = google.maps.Animation.br;
            bounce_marker.setAnimation(bounce_marker_animation);
        }

        update_bounce_circle(new_lat, new_lon);
    }

    /*
     * 載入tile map
     * @returns {undefined}
     */

    function load_tile_map(idx, url_path, plan_id, bl_lat, bl_lng, tr_lat, tr_lng) {

        //清除tilemap
        if (maptiler) {
            map.overlayMapTypes.setAt(ii - 1, null);
        }

        mapBounds = new google.maps.LatLngBounds(
                new google.maps.LatLng(bl_lat, bl_lng),
                new google.maps.LatLng(tr_lat, tr_lng));

        maptiler = new google.maps.ImageMapType({
            getTileUrl: function (coord, zoom) {
                var proj = map.getProjection();
                var z2 = Math.pow(2, zoom);
                var tileXSize = 256 / z2;
                var tileYSize = 256 / z2;
                var tileBounds = new google.maps.LatLngBounds(
                        proj.fromPointToLatLng(new google.maps.Point(coord.x * tileXSize, (coord.y + 1) * tileYSize)),
                        proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * tileXSize, coord.y * tileYSize))
                        );
                var y = coord.y;
                var x = coord.x >= 0 ? coord.x : z2 + coord.x;
                if (mapBounds.intersects(tileBounds) && (mapMinZoom <= zoom) && (zoom <= mapMaxZoom)) {
                    return url_path + "/" + plan_id + "/" + zoom + "/" + x + "/" + y + ".png";
                } else {
                    return url_path + "/" + "none.png";
                }
            },
            tileSize: new google.maps.Size(256, 256),
            isPng: true,
            opacity: 1.0
        });
        map.overlayMapTypes.insertAt(ii, maptiler);
        ii++;

    }


    /*
     * 繪製markers
     * @returns {undefined}
     */
    function draw_marker(idx, name, minor, blat, blng, range, voltage, content) {

        //繪製markers
        var color;
        if (voltage >= 2.7) {
            color = "00cc00";
        } else if (voltage <= 2.6 && voltage >= 2.1) {
            color = "ffff00";
        } else if (voltage <= 2.0) {
            color = "ff0000";
        }
        if (voltage === '') {
            color = "80bfff";
        }

        var icon = 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=' + minor + '|' + color + '|000000';
        var image = {
            url: icon
        };
        desc[idx] = content;

        var marker = new google.maps.Marker({
            icon: image,
            title: name,
            info_window: info_windows,
            position: {lat: blat, lng: blng},
            map: map
        });

        //繪製最大距離
        var marker_circle = new google.maps.Circle({
            strokeColor: '#0066cc',
            strokeOpacity: 0.5,
            strokeWeight: 1,
            fillColor: '#0066cc',
            fillOpacity: 0.15,
            zIndex: 10,
            map: map,
            center: new google.maps.LatLng(blat, blng),
            radius: range
        });
        markers_circle.push(marker_circle);

        //新增event
        google.maps.event.addListener(marker, 'click', (function (marker, idx) {
            return function () {
                infoWindow.setContent(desc[idx]);
                infoWindow.open(map, marker);
            };
        })(marker, idx));
        map.addListener('mousedown', function () {
            infoWindow.close();
        });

        markers.push(marker);
    }

    function draw_geofence_marker(idx, gname, glat, glng, range, enabled) {

        //繪製markers
        var icon_color;
        if (enabled === "N") {
            icon_color = "cccccc";
        } else {
            icon_color = "ffa366";
        }
        var icon = 'https://chart.googleapis.com/chart?chst=d_bubble_text_small&chld=bbT|' + gname + '|' + icon_color + '|000';
        var image = {
            url: icon
        };
        var marker = new google.maps.Marker({
            icon: image,
            title: gname + "(" + range + "m)",
            position: {lat: glat, lng: glng},
            map: map
        });

        //繪製最大距離
        var marker_circle = new google.maps.Circle({
            strokeColor: '#' + icon_color,
            strokeOpacity: 0.6,
            strokeWeight: 1,
            fillColor: '#' + icon_color,
            fillOpacity: 0.3,
            zIndex: 10,
            map: map,
            center: new google.maps.LatLng(glat, glng),
            radius: range
        });
        markers_circle.push(marker_circle);

        markers.push(marker);
    }


    function draw_views_marker(idx, name, vlat, vlng, type, enabled, content) {

        //繪製markers
        var icon_color;
        if (enabled === "N") {
            icon_color = "cccccc";
        } else {
            icon_color = "F5A9D0";
        }
        var type_str = type.substr(0, 1);
        var icon = 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=' + type_str + '|' + icon_color + '|000000';
        var image = {
            url: icon
        };
        var marker = new google.maps.Marker({
            icon: image,
            title: name,
            info_window: info_windows,
            position: {lat: vlat, lng: vlng},
            map: map
        });

        desc[idx] = content;

        //繪製最大距離
        var marker_circle = new google.maps.Circle({
            strokeColor: '#' + icon_color,
            strokeOpacity: 0.6,
            strokeWeight: 1,
            fillColor: '#' + icon_color,
            fillOpacity: 0.3,
            zIndex: 10,
            map: map,
            center: new google.maps.LatLng(vlat, vlng),
            radius: 0
        });
        markers_circle.push(marker_circle);

        //新增event
        google.maps.event.addListener(marker, 'click', (function (marker, idx) {
            return function () {
                infoWindow.setContent(desc[idx]);
                infoWindow.open(map, marker);
            };
        })(marker, idx));
        map.addListener('mousedown', function () {
            infoWindow.close();
        });

        markers.push(marker);
    }

    function draw_traffic_marker(idx, name, tlat, tlng, type, content) {

        //繪製markers
        var icon_color = '';
        var icon_str = '';
        switch (type) {
            case 'S':
                icon_color = '99ccff';
                icon_str = 'bus';
                break;
            case 'P':
                icon_color = 'ADDE63';
                icon_str = 'parking';
                break;
            case 'B':
                icon_color = 'ff9933';
                icon_str = 'bicycle';
                break;
        }
        var icon = 'https://chart.googleapis.com/chart?chst=d_map_pin_icon&chld=' + icon_str + '|' + icon_color;
        var image = {
            url: icon
        };
        desc[idx] = content;
        var marker = new google.maps.Marker({
            icon: image,
            title: name,
            info_window: info_windows,
            position: {lat: tlat, lng: tlng},
            map: map
        });

        //繪製最大距離
        var marker_circle = new google.maps.Circle({
            map: map,
            center: new google.maps.LatLng(tlat, tlng),
            radius: 0
        });
        //markers_circle.push(marker_circle);

        //新增event
        google.maps.event.addListener(marker, 'click', (function (marker, idx) {
            return function () {
                if (visible_bus) {
                    getRoutebyLocation(idx);
                } else {
                    infoWindow.setContent(desc[idx]);
                    infoWindow.open(map, marker);
                }
            };
        })(marker, idx));
        map.addListener('mousedown', function () {
            infoWindow.close();
        });

        markers.push(marker);
    }

    function infowindow_close() {
        for (var i in info_windows) {
            info_windows[i].close();
        }
    }

    function getRoutebyLocation(i) {
        if (!intervalId_businfo) {
            loadStopsPage(i);
        }

        if (stops.length === 0) {
            return;
        }

        stop_data = [];

        var url = base_url + "api/Route_by_location";
        var where = {
            "stopLocationId": stops[i].stopLocationId
        };
        $.ajax({
            url: url,
            data: where,
            dataType: 'JSON',
            success: function (ret) {
                if (ret.result) {
                    stop_data[i] = ret.data;
                    getBusInfo(i);
                }
            }
        });
    }


    function getBusInfo(s) {

        busInfo = stop_data[s];

        if (busInfo.length === 0) {
            return;
        }

        var html = "";

        if (!intervalId_businfo) {
            html += '<div class="stop_title" onclick="traffic_map.closeDiv(\'StopBusInfo\')">' + stops[s].nameZh + '</div>';
            html += '<div style="width:100%;height:' + (window.innerHeight - 50) + 'px;overflow-x:hidden; overflow-y:auto;">';
        }
        for (var i = 0; i < busInfo.length; i++) {
            var estimateTime = checkSec(busInfo[i].EstimateTime.EstimateTime);
            //if (estimateTime.indexOf("分") > 0) estimateTime += "到站";
            if (!intervalId_businfo) {
                var dest = busInfo[i].departureZh + " - " + busInfo[i].destinationZh;
                var pathattributed_id = busInfo[i].pathAttributeId;
                html += "<div class='stop_line' onclick='traffic_map.getPathDetailData(" + '"' + pathattributed_id + '"' + " ," + i + "," + busInfo[i].goBack + ")'>";

                var fonsize = 20;
                if (isNaN(estimateTime.substr(0, 1)))
                    fonsize = 14;
                html += '<div id="td_estimateTime' + i + '" style="font-size:' + fonsize + 'px;padding-right:10px;float:right;">' + estimateTime + '</div>';
                html += '<div style="line-height:56px;font-size:22px;">' + busInfo[i].nameZh + '</div>';
                html += '<div style="line-height:12px;font-size:12px;">' + dest + '</div>';
                html += '<div style="line-height:12px;font-size:12px;">　</div>';
                html += '</div>';
            } else {
                var fonsize = 20;
                if (isNaN(estimateTime.substr(0, 1)))
                    fonsize = 14;
                document.getElementById("td_estimateTime" + i).style.fontSize = fonsize + "px";
                document.getElementById("td_estimateTime" + i).innerHTML = estimateTime;
            }
        }
        if (!intervalId_businfo) {
            html += '</div>';
            document.getElementById("StopBusInfo").style.visibility = "visible";
            document.getElementById("StopBusInfo").innerHTML = html;
        }
        clearInterval(intervalId_businfo);
        intervalId_businfo = setInterval(function () {
            getRoutebyLocation(s);
            //console.log(Stops.data[s].nameZh + ":" + new Date().getSeconds());
        }, 30000);
    }

    function closeDiv(div) {

        document.getElementById(div).style.visibility = "hidden";

        if (div == "StopBusInfo") {
            clearInterval(intervalId_businfo);
            intervalId_businfo = "";
        } else {
            clearInterval(intervalId_route);
            intervalId_route = "";
            clearInterval(countdown);
            countdown = "";
        }
    }


    var pathAttribute = [];
    function getPathDetailData(pathAttributeId, p, gobackIdx) {

        if (!intervalId_route)
            loadRoutePage(p, gobackIdx);

        var url = base_url + "api/Stop_by_pathAttributeId";
        var where = {
            "pathAttributeId": pathAttributeId
        };
        $.ajax({
            url: url,
            data: where,
            dataType: 'JSON',
            success: function (ret) {
                if (ret.result) {
                    pathAttribute = ret.data;
                    getRoute(p, gobackIdx);
                }
            }
        });

    }


    function getRoute(p, goIdx) {

        goBack = goIdx;
        var selected = " class='route_direction_s' ";
        var unselected = " class='route_direction' ";
        var gbStyle = (goBack == "0") ? [selected, unselected] : [unselected, selected];

        var html = "";

        if (!intervalId_route) {
            var html = '<table style="width:100%;height:54px" cellspacing="0px" onclick="traffic_map.closeDiv(\'pathDetail\')">';
            html += '<tr><td valign="middle" style="font-size:24px; font-weight:bolder;color:#00763E;width:100%;">';
            html += '<div style="width:30px;float:left;position:relative;top:10px;"><input type="image" src="images/gt/back.png" onclick="traffic_map.closeDiv(\'pathDetail\')" style="top:10px;left:20px;height:30px" value="" /></div>';
            html += "<div style='float:left'><img src='images/gt/icon_bus_namw.png' height='50px' /></div>";
            var style = "position:relative;top:15px;line-height:26px;";
            if (busInfo[p].nameZh.length > 11) {
                style = "position:relative;top:0px;line-height:26px;";
            }
            html += "<div style='" + style + "'>" + busInfo[p].nameZh + "</div></td><td style='width:50%'></td></tr>";
            html += "</table>";
            var pathattributed_id = busInfo[p].pathAttributeId;
            html += "<div onclick='traffic_map.clear_interval_route();traffic_map.getPathDetailData(\"" + pathattributed_id + "\"," + p + ",\"0\");'" + gbStyle[0] + ">往" + busInfo[p].destinationZh + "(去)</div>"; //0:去程
            html += "<div onclick='traffic_map.clear_interval_route();traffic_map.getPathDetailData(\"" + pathattributed_id + "\"," + p + ",\"1\");'" + gbStyle[1] + ">往" + busInfo[p].departureZh + "(返)</div>"; //1:回程

            html += '<div style="width:100%;height:' + (window.innerHeight - 94) + 'px;overflow-x:hidden; overflow-y:auto;">';
            html += '<div class="route_line_h"><div style="border-bottom:1px solid #f5f5f5;">　<div style="border-bottom:1px solid #f5f5f5;"></div></div></div>';
        }

        var gotoIdx = 0;
        var tdColor = "white";
        var fColor = "#b5b5b5", fColor_et = "";
        var routeIcon = "icon_route";
        var rClass = "route_line";
        var style = "color:#b5b5b5;font-weight:bold;padding-left:10px;border-bottom:1px solid #f5f5f5;";
        var routeNum = false;
        for (var i = 0; i < pathAttribute.length; i++) {
            var pd = pathAttribute[i];
            if (pd.goBack == goBack) {
                var estimateTime = "更新中";
                estimateTime = checkSec(pd.EstimateTime.EstimateTime);

                if (estimateTime == "準備進站") {
                    tdColor = "orange";
                    fColor = "white";
                    fColor_et = fColor;
                    routeIcon = "icon_route_o";
                    rClass = "route_line_o";
                    style = "background-color:" + tdColor + ";font-color:" + fColor + ";font-weight:bold;padding-left:10px;border-bottom:1px solid #f5f5f5;";
                } else if (estimateTime.substr(1, 1) == "分") {
                    tdColor = "white";
                    fColor = "#0080ff";
                    fColor_et = fColor;
                    routeIcon = "icon_route_b";
                    rClass = "route_line_b";
                    style = "background-color:" + tdColor + ";font-color:" + fColor + ";font-weight:bold;padding-left:10px;border-bottom:1px solid #f5f5f5;";
                } else if (estimateTime == "尚未發車" || estimateTime == "交管不停靠" || estimateTime == "末班車已過" || estimateTime == "今日未營運") {
                    tdColor = "white";
                    fColor = "#7dbf20";
                    fColor_et = "#00763e";
                    routeIcon = "icon_route";
                    rClass = "route_line";
                    style = "background-color:" + tdColor + ";font-color:" + fColor + ";font-weight:bold;padding-left:10px;border-bottom:1px solid #f5f5f5;";
                    style_et = "background-color:" + tdColor + ";font-color:" + fColor_et + ";font-weight:bold;padding-left:10px;border-bottom:1px solid #f5f5f5;";
                }

                var stopName = pd.nameZh;
                if (!intervalId_route) {
                    if (stops[stop_data.length - 1].nameZh == stopName)
                        gotoIdx = i;

                    html += '<div id="td_bus_c' + i + '" class="' + rClass + '">';
                    html += '<div id="td_bus_b' + i + '" style="' + style + '" >' + stopName;
                    html += '<div id="td_bus_a' + i + '" style="' + style + '; padding-right:10px;float:right">' + estimateTime + '</div>';
                    html += '</div></div>';
                } else {
                    document.getElementById("td_bus_c" + i).className = rClass;
                    document.getElementById("td_bus_b" + i).style.backgroundColor = tdColor;
                    document.getElementById("td_bus_b" + i).style.color = fColor;
                    document.getElementById("td_bus_a" + i).style.backgroundColor = tdColor;
                    document.getElementById("td_bus_a" + i).style.color = fColor_et;
                    document.getElementById("td_bus_a" + i).innerHTML = estimateTime;
                }
                routeNum = true;
            }

        }
        if (!intervalId_route) {
            //        if (!routeNum) {
            //            html += '<div id="td_bus_c' + i + '" class="' + rClass + '">';
            //            html += '<div id="td_bus_b' + i + '" style="' + style + '" >' + '無載入資料';
            //            html += '<div id="td_bus_a' + i + '" style="' + style + '; padding-right:10px;float:right">' + '---' + '</div>';
            //            html += '</div></div>';
            //        }
            html += '<div class="route_line_f">　</div>';
            html += "</div>";

            document.getElementById("pathDetail").style.visibility = "visible";
            document.getElementById("pathDetail").innerHTML = html;
        }
        clearInterval(intervalId_route);
        intervalId_route = setInterval(function () {
            getPathDetailData(busInfo[p].pathAttributeId, p, goBack);
            //console.log(busInfo[p].nameZh + ":" + new Date().getSeconds());
        }, 30000);

        window.location.hash = gotoIdx;
        setTimeout(countdownRoute(p, goBack), 1000);
        //failTime = 0;
    }


    var countdown;
    function countdownRoute(p, goBack) {
        clearInterval(countdown);
        for (var i = 0; i < pathAttribute.length; i++) {
            var pd = pathAttribute[i];
            if (pd.goBack == goBack) {
                var estimateTime = "更新中";
                if (pd.EstimateTime.EstimateTime > 0)
                    pd.EstimateTime.EstimateTime -= 1;
                estimateTime = checkSec(pd.EstimateTime.EstimateTime);
                //var tdColor = (estimateTime == "進站中") ? "orange" : (estimateTime.substr(1, 1) == "分") ? "deepskyblue" : "gray";
                //var busIcon = (estimateTime == "進站中") ? "bus-icon.png" : "circle-icon.png";
                var tdColor = "white";
                var fColor = "#b5b5b5", fColor_et = "";
                var routeIcon = "icon_route";
                var rClass = "route_line";
                var style = "color:#b5b5b5;font-weight:bold;padding-left:10px;";
                if (estimateTime == "準備進站") {
                    tdColor = "orange";
                    fColor = "white";
                    fColor_et = fColor;
                    routeIcon = "icon_route_o";
                    rClass = "route_line_o";
                    style = "background-color:" + tdColor + ";font-color:" + fColor + ";font-weight:bold;padding-left:10px;";
                } else if (estimateTime.substr(1, 1) == "分") {
                    tdColor = "white";
                    fColor = "#0080ff";
                    fColor_et = fColor;
                    routeIcon = "icon_route_b";
                    rClass = "route_line_b";
                    style = "background-color:" + tdColor + ";font-color:" + fColor + ";font-weight:bold;padding-left:10px;";
                } else if (estimateTime == "尚未發車" || estimateTime == "交管不停靠" || estimateTime == "末班車已過" || estimateTime == "今日未營運") {
                    tdColor = "white";
                    fColor = "#7dbf20";
                    fColor_et = "#00763e";
                    routeIcon = "icon_route";
                    rClass = "route_line";
                    style = "background-color:" + tdColor + ";font-color:" + fColor + ";font-weight:bold;padding-left:10px;border-bottom:1px solid #f5f5f5;";
                    style_et = "background-color:" + tdColor + ";font-color:" + fColor_et + ";font-weight:bold;padding-left:10px;border-bottom:1px solid #f5f5f5;";
                }

                if (intervalId_route) {
                    document.getElementById("td_bus_c" + i).className = rClass;
                    document.getElementById("td_bus_b" + i).style.backgroundColor = tdColor;
                    document.getElementById("td_bus_b" + i).style.color = fColor;
                    document.getElementById("td_bus_a" + i).style.backgroundColor = tdColor;
                    document.getElementById("td_bus_a" + i).style.color = fColor_et;
                    document.getElementById("td_bus_a" + i).innerHTML = estimateTime;
                }
            }
        }
        //console.log(new Date().getSeconds());
        countdown = setInterval(function () {
            countdownRoute(p, goBack);
        }, 1000);
    }

    function loadStopsPage(s) {

        document.getElementById("StopBusInfo").style.visibility = "visible";
        var html = '';
        html += '<div class="stop_title" onclick="traffic_map.closeDiv(\'StopBusInfo\')">' + (stops[s] ? stops[s].nameZh : '') + '</div>';
        html += '<div style="width:100%;height:' + (window.innerHeight - 50) + 'px;overflow-x:hidden; overflow-y:hidden;">';
        html += '<div class="stop_line">載入中</div>';
        html += '<div class="stop_line">　</div>';
        html += '<div class="stop_line">　</div>';
        html += '<div class="stop_line">　</div>';
        html += '<div class="stop_line">　</div>';
        html += '<div class="stop_line">　</div>';
        html += '<div class="stop_line">　</div>';
        html += '<div class="stop_line">　</div>';
        html += '<div class="stop_line">　</div>';
        html += '<div class="stop_line">　</div>';
        html += '<div class="stop_line">　</div>';
        html += '<div class="stop_line">　</div>';
        html += '<div class="stop_line">　</div>';
        html += '<div class="stop_line">　</div>';
        html += "</div>";
        document.getElementById("StopBusInfo").innerHTML = html;
    }


    function loadRoutePage(p, goBack) {

        document.getElementById("pathDetail").style.visibility = "visible";

        var selected = " class='route_direction_s' ";
        var unselected = " class='route_direction' ";
        var gbStyle = (goBack == "0") ? [selected, unselected] : [unselected, selected];

        var html = '<table style="width:100%;height:54px" cellspacing="0px">';
        html += '<tr><td valign="middle" style="font-size:24px; font-weight:bolder;color:#00763E;width:100%;">';
        html += '<div style="width:30px;float:left;position:relative;top:10px;"><input type="image" src="images/gt/back.png" onclick="traffic_map.closeDiv(\'pathDetail\')" style="top:10px;left:20px;height:30px" value="" /></div>';
        html += "<div style='float:left'><img src='images/gt/icon_bus_namw.png' height='50px' /></div>";
        var style = "position:relative;top:15px;line-height:26px;";
        if (busInfo[p].nameZh.length > 11) {
            style = "position:relative;top:0px;line-height:26px;";
        }
        html += "<div style='" + style + "'>" + busInfo[p].nameZh + "</div></td><td style='width:50%'></td></tr>";
        html += "</table>";
        var pathattributed_id = busInfo[p].pathAttributeId;
        html += "<div onclick='traffic_map.clear_interval_route();traffic_map.getPathDetailData(" + pathattributed_id + "," + p + ",\"0\");'" + gbStyle[0] + ">往" + busInfo[p].destinationZh + "(去)</div>"; //0:去程
        html += "<div onclick='traffic_map.clear_interval_route();traffic_map.getPathDetailData(" + pathattributed_id + "," + p + ",\"1\");'" + gbStyle[1] + ">往" + busInfo[p].departureZh + "(返)</div>"; //1:回程

        html += '<div style="width:100%;height:' + (window.innerHeight - 94) + 'px;overflow-x:hidden; overflow-y:auto;">';
        html += '<div class="route_line_h"><div style="border-bottom:1px solid #f5f5f5;">　<div style="border-bottom:1px solid #f5f5f5;"></div></div></div>';
        //html += '<div class="route_line">載入中</div>';
        html += '<div class="route_line_f">　</div>';
        html += "</div>";

        document.getElementById("pathDetail").innerHTML = html;

    }

    function checkSec(sec) {
        if (sec == "0" || sec == "-1")
            return "尚未發車";
        if (sec == "-2")
            return "交管不停靠";
        if (sec == "-3")
            return "末班車已過";
        if (sec == "-4")
            return "今日未營運";
        if (sec == "-99")
            return "---";

        var min = 0;
        if (sec > 59)
            min = Math.ceil(sec / 60);
        if (min < 1)
            return "準備進站";
        else
            return min + "分";
    }


    function get_gradient(idx) {
        var gradient = null;
        idx = parseInt(idx);
        switch (idx) {
            case 0:
                gradient = null;
                break;
            case 1:
                gradient = [
                    'rgba(0, 255, 255, 0)',
                    'rgba(0, 255, 255, 1)',
                    'rgba(0, 191, 255, 1)',
                    'rgba(0, 127, 255, 1)',
                    'rgba(0, 63, 255, 1)',
                    'rgba(0, 0, 255, 1)',
                    'rgba(0, 0, 223, 1)',
                    'rgba(0, 0, 191, 1)',
                    'rgba(0, 0, 159, 1)',
                    'rgba(0, 0, 127, 1)',
                    'rgba(63, 0, 91, 1)',
                    'rgba(127, 0, 63, 1)',
                    'rgba(191, 0, 31, 1)',
                    'rgba(255, 0, 0, 1)'
                ];
                break;
            case 2:
                gradient = [
                    'transparent',
                    '#ffffff',
                    '#006837',
                    '#1a9850',
                    '#66bd63',
                    '#a6d96a',
                    '#d9ef8b',
                    '#fee08b',
                    '#fdae61',
                    '#f46d43',
                    '#d73027',
                    '#a50026',
                    '#aa0000',
                    '#000000'
                ];
                break;
        }
        return gradient;
    }


    function ajax_get_nearby_stop() {

        var url = base_url + "api/Stop";
        var where = {
            "minLatitude": map.getBounds().getSouthWest().lat().toString(),
            "maxLatitude": map.getBounds().getNorthEast().lat().toString(),
            "minLongitude": map.getBounds().getSouthWest().lng().toString(),
            "maxLongitude": map.getBounds().getNorthEast().lng().toString()
        };
        $.ajax({
            url: url,
            data: where,
            dataType: 'JSON',
            success: function (ret) {
                if (ret.result) {
                    stops = ret.data;
                    //繪製marker
                    for (var i = 0; i < ret.data.length; i++) {
                        var obj = ret.data[i];
                        var content = obj.nameZh;
                        draw_traffic_marker(nearby_idx, obj.nameZh + '(' + obj.nameEn + ')', obj.latitude, obj.longitude, 'S', content);
                        nearby_idx++;
                    }
                }
            }
        });
    }

    function ajax_get_nearby_parking() {

        var url = base_url + "api/Parking";
        var where = {
            "minLatitude": map.getBounds().getSouthWest().lat().toString(),
            "maxLatitude": map.getBounds().getNorthEast().lat().toString(),
            "minLongitude": map.getBounds().getSouthWest().lng().toString(),
            "maxLongitude": map.getBounds().getNorthEast().lng().toString()
        };
        $.ajax({
            url: url,
            data: where,
            dataType: 'JSON',
            success: function (ret) {
                if (ret.result) {
                    //繪製marker
                    for (var i = 0; i < ret.data.length; i++) {
                        var obj = ret.data[i];
                        var content = "名稱：" + obj.name + "<br>地址：" + obj.address;
                        draw_traffic_marker(nearby_idx, obj.name, obj.lat, obj.lng, 'P', content);
                        nearby_idx++;
                    }
                }
            }
        });
    }

    function ajax_get_nearby_bike() {

        var url = base_url + "api/Bike";
        var where = {
            "minLatitude": map.getBounds().getSouthWest().lat().toString(),
            "maxLatitude": map.getBounds().getNorthEast().lat().toString(),
            "minLongitude": map.getBounds().getSouthWest().lng().toString(),
            "maxLongitude": map.getBounds().getNorthEast().lng().toString()
        };
        $.ajax({
            url: url,
            data: where,
            dataType: 'JSON',
            success: function (ret) {
                if (ret.result) {
                    //繪製marker
                    for (var i = 0; i < ret.data.length; i++) {
                        var obj = ret.data[i];
                        var content = "站名：" + obj.title + "<br>地址：" + obj.address + "<br>格位數：" + obj.total + "<br>可借車輛數：" + obj.total_bike + "<br>可停空位數：" + obj.total_bemp + "<br>更新時間：" + obj.update_time;
                        draw_traffic_marker(nearby_idx, obj.title, obj.lat, obj.lng, 'B', content);
                        nearby_idx++;
                    }
                }
            }
        });
    }

    function get_nearby_ajax() {

        if (!map.getBounds())
            return;

        clear_all();

        var val = $("#select-category").val();
        switch (val) {
            case "all":
                ajax_get_nearby_stop();
                ajax_get_nearby_parking();
                ajax_get_nearby_tbike();
                break;
            case 'stop':
                ajax_get_nearby_stop();
                break;
            case 'parking':
                ajax_get_nearby_parking();
                break;
            case 'tbike':
                ajax_get_nearby_tbike();
                break;
        }
    }


    /* *************************
     * public functions
     * *************************/
    return {
        /*
         * 本物件初始化函式
         * @param {type} init_base_url
         * @returns {undefined}
         */
        init: function (init_base_url, keyword) {
            base_url = init_base_url;
            if (typeof keyword !== 'undefined' && keyword !== '') {
                init_keyword = keyword;
                keyword_search = true;
            }
        },
        ajax_parking_go: function () {
            visible_parking = true;
            ajax_get_nearby_parking();
        },
        ajax_ibike_go: function () {
            visible_ibike = true;
            ajax_get_nearby_bike();
        },
        ajax_bus_go: function () {
            visible_bus = true;
            ajax_get_nearby_stop();
        },
        /*
         * google map顯示完成後call back
         */
        googlemap_callback: function () {

            google.maps.event.addListener(map, "click", function (event) {
                if (active_info_window) {
                    active_info_window.close();
                    active_info_window = false;
                }
            });

            google.maps.event.addListener(map, 'idle', function () {

                clear_all();

                if (visible_bus) {
                    ajax_get_nearby_stop();
                }
                if (visible_ibike) {
                    ajax_get_nearby_bike();
                }
                if (visible_parking) {
                    ajax_get_nearby_parking();
                }
            });

        },
        /*
         * google map js 載入後自動call back
         */
        initMap: function () {
            map = new google.maps.Map(document.getElementById('map'), {
                mapTypeControl: false,
                streetViewControl: false,
                navigationControl: false,
                scaleControl: false,
                zoomControl: false,
                fullscreenControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: {lat: lat, lng: lng},
                zoom: 17
            });

            if (typeof googlemap_callback !== 'undefined') {
                googlemap_callback();
            }

            infoWindow = new google.maps.InfoWindow({pixelOffset: new google.maps.Size(0, 0)});

        },
        set_views: function (idx, name, vlat, vlng, type, enabled, content) {
            draw_views_marker(idx, name, vlat, vlng, type, enabled, content);
        },
        set_nearby: function (idx, name, vlat, vlng, type, content) {
            draw_views_marker(idx, name, vlat, vlng, type, content);
        },
        get_nearby: function () {
            get_nearby_ajax();
        },
        closeDiv: function (div) {
            closeDiv(div);
        },
        getPathDetailData: function (pathAttributeId, p, gobackIdx) {
            getPathDetailData(pathAttributeId, p, gobackIdx);
        },
        clear_interval_route: function () {
            clearInterval(intervalId_route);
            intervalId_route = '';
        },
        /*
         * 返回使用者目前位置
         */
        center_to_user_location: function () {
            google_map.set_center_to_user_location();
            bounce_marker.setPosition(new google.maps.LatLng(google_map.get_user_lat(), google_map.get_user_lon()));
            on_bounce_marker_drop();
        },
        my_location: function () {
            var mylatlng = new google.maps.LatLng(lat, lng);
            map.panTo(mylatlng);
        },
        /*
         更新經緯度
         */
        set_new_latlng: function (newlat, newlng) {
            if (!newlat) {
                newlat = $('.newlat')[0].innerText;
            }
            if (!newlng) {
                newlng = $('.newlng')[0].innerText;
            }
            $('.lat').val(newlat);
            $('.lng').val(newlng);
        },
        set_center: function (newlat, newlng) {
            var newlatlng = new google.maps.LatLng(newlat, newlng);
            map.panTo(newlatlng);
            bounce_marker.setPosition(newlatlng);
            on_bounce_marker_drop();
        },
        base_tile_map: function (idx, plan_id, bl_lat, bl_lng, tr_lat, tr_lng) {
            if (!plan_id)
            {
                return;
            }
            var tile_map_url = base_url + "map/tile";
            load_base_tile_map(idx, tile_map_url, plan_id, parseFloat(bl_lat), parseFloat(bl_lng), parseFloat(tr_lat), parseFloat(tr_lng));
        },
        set_tile_map: function (idx, plan_id, bl_lat, bl_lng, tr_lat, tr_lng) {
            if (!plan_id)
            {
                return;
            }
            var tile_map_url = base_url + "map/tile";
            load_tile_map(idx, tile_map_url, plan_id, parseFloat(bl_lat), parseFloat(bl_lng), parseFloat(tr_lat), parseFloat(tr_lng));
        },
        clear_all: function () {
            //清除markers、circles
            for (var i in markers) {
                markers[i].setMap(null);
                markers_circle[i].setMap(null);
            }
            markers = [];
            markers_circle = [];
            info_windows = [];
        },
        fit_bounce: function () {
            var bounds = new google.maps.LatLngBounds();
            for (var i in markers) {
                var myLatLng = markers[i].position;
                bounds.extend(myLatLng);
            }
            map.fitBounds(bounds);
        }
    };
}();

var googlemap_callback = traffic_map.googlemap_callback;
