let geo_latitude = 0;
let geo_longitude = 0;
let div_heading = 0
let div_heading_horizontal = 0;
let div_orientation = 0;
let div_rotation = 0;
let FOV_Left = 0;
let FOV_Right = 0;
let FOV_Range = 0;
let locations = [];
let deviceType = "";
let panelID = "plt_";
let angleID = "agl_"
let panelWidth = 0;
let panelHeight = 0;
let fontSize = 20;
let dailogID = "info_dailog";
let dailogTitleID = "d_title";
let dailogContentID = "d_content";
let debugMode = false;
let enableGGMap = true;
let map = null;
let ggmap_headingPointerBoard = 40;
let radarWidth = 100;
let initAngle = null;

//計算兩標的之間的角度
let getAngle = function(px1, py1, px2, py2) {
    let dx = px2 - px1
    let dy = py2 - py1
    let ang = Math.atan2(dy, dx) * 180 / Math.PI;

    return ang > 0 ? ang : 360 + ang;
}

//計算兩座標的距離 (unit : 單位 K-公里 / N-英里 / N-海里)
let getDistance = function(lat1, lon1, lat2, lon2) {
    if ((lat1 == lat2) && (lon1 == lon2)) {
        return 0;
    } else {
        var radlat1 = Math.PI * lat1 / 180;
        var radlat2 = Math.PI * lat2 / 180;
        var theta = lon1 - lon2;
        var radtheta = Math.PI * theta / 180;
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
            dist = 1;
        }
        dist = Math.acos(dist);
        dist = dist * 180 / Math.PI;
        dist = dist * 60 * 1.1515;
        return dist;
    }
}

//設定招牌大小
let setPlateSize = function(pWidth, pHeight) {
    panelWidth = pWidth;
    panelHeight = pHeight;
}

//設定可視視角範圍
let setFOVRange = function(FOV_range) {
    FOV_Range = FOV_range;
}

//設定目標資訊
let setLocations = function(locObj) {
    locations = locObj;

    locations.forEach(function(coordinate, index) {
        let pno = index + 1;
        let PID = panelID + pno;
        let tagPanel = document.createElement("div");
        $(tagPanel).attr("id", PID);
        coordinate.top = pno * (panelHeight * 2);

        let tagPanel_Style = "top : " + coordinate.top + "px;";
        tagPanel_Style += "left : 0px;";
        tagPanel_Style += "width : " + panelWidth + "px; ";
        tagPanel_Style += "min-height : " + panelHeight + "px; ";
        tagPanel_Style += "position : fixed; ";
        tagPanel_Style += "z-index : " + pno * 10 + "; ";

        $(tagPanel).attr("style", tagPanel_Style);
        $(tagPanel).on("click", function() {
            $("#" + dailogTitleID).html(coordinate.name);

            let contentContainer = $("#" + dailogContentID);
            contentContainer.html("");

            if (coordinate.image != '') {
                let contentImg = document.createElement("img");
                contentImg.setAttribute("class", "contentImg");
                contentImg.setAttribute("src", "/images/webAR/upload/" + coordinate.image);

                contentContainer.append(contentImg);
            }

            if (coordinate.youtube != "") {
                let youtubeContainer = document.createElement("div");
                $(youtubeContainer).attr("class", "embed-container");

                let youtubeBody = document.createElement("iframe");
                $(youtubeBody).attr("id", "yt_player");
                $(youtubeBody).attr("width", "100%");
                $(youtubeBody).attr("src", "https://www.youtube-nocookie.com/embed/" + coordinate.youtube + "?rel=0");
                $(youtubeBody).attr("frameborder", "0");
                $(youtubeBody).attr("allow", "accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture");

                $(youtubeContainer).append(youtubeBody);
                contentContainer.append(youtubeContainer);

                setTimeout(() => {
                    $("#yt_player").trigger("click");
                }, 1000);
            }

            let contentText = document.createElement("div");
            contentText.setAttribute("class", "dialog_content");
            contentText.innerHTML = coordinate.text;

            contentContainer.append(contentText);
            $("#" + dailogID).css({ "height": "84vh", "opacity": "1" });
        });

        let baseObj = document.createElement("div");
        let baseObj_Style = "padding : 5px;";
        baseObj_Style += "line-height : 30px; ";
        baseObj_Style += "font-size : " + fontSize + "px; ";
        baseObj_Style += "background : rgba(255, 255, 255, .8);";
        baseObj_Style += "border-radius : 5px;";

        $(baseObj).attr("style", baseObj_Style);
        $(tagPanel).append(baseObj);

        let baseBoard = document.createElement("table");
        let BL1 = document.createElement("tr");
        let logoField = document.createElement("td");
        $(logoField).attr("width", panelHeight * 2 + 5);
        $(logoField).attr("rowspan", 2);
        $(BL1).append(logoField);

        let logoObj = document.createElement("div");
        let logoObj_Style = "width : " + (panelHeight * 2) + "px; ";
        logoObj_Style += "height : " + (panelHeight * 2) + "px; ";
        logoObj_Style += "line-height : " + (panelHeight * 2) + "px; ";
        logoObj_Style += "background : " + coordinate.color + "; ";
        logoObj_Style += "font-family : Arial; ";
        logoObj_Style += "font-size : " + Math.round(panelHeight * 0.8) + "px; ";
        logoObj_Style += "color : rgb(255, 255, 255); ";
        logoObj_Style += "text-align : center; ";
        logoObj_Style += "vertical-align : middle; ";
        logoObj_Style += "border-radius : 5px; ";
        $(logoObj).attr("style", logoObj_Style);
        $(logoObj).html("AR");

        $(logoField).append(logoObj);

        let locNameField = document.createElement("td");
        $(locNameField).attr("style", "font-weight : 800");
        $(BL1).append(locNameField);

        let BL2 = document.createElement("tr");
        let locDistanceField = document.createElement("td");
        $(BL2).append(locDistanceField);

        $(baseBoard).append(BL1);
        $(baseBoard).append(BL2);
        $(baseObj).append(baseBoard);

        let mainBoard = document.createElement("div");
        let mainBoard_Style = "width : 100%;";
        mainBoard_Style += "min-height : " + panelHeight + "px;";
        mainBoard_Style += "line-height : 30px;";

        $(mainBoard).attr("style", mainBoard_Style);
        $(mainBoard).html(coordinate.name);

        let aglBoard = document.createElement("div");
        $(aglBoard).attr("id", angleID + pno);

        let AnchorPanel = document.createElement("div");
        $(tagPanel).append(AnchorPanel);

        let AnchorObj = document.createElement("div");
        let Anchor_Style = "width : 0px;";
        Anchor_Style += "height : 0px;";
        Anchor_Style += " border-top : 10px rgba(255, 255, 255, .8) solid;";
        Anchor_Style += " border-left : 6px transparent solid;";
        Anchor_Style += " border-right : 6px transparent solid;";
        Anchor_Style += " margin-left : auto;";
        Anchor_Style += " margin-right : auto;";

        $(AnchorObj).attr("style", Anchor_Style);

        $(AnchorPanel).append(AnchorObj);

        $(locNameField).append(mainBoard);
        $(locDistanceField).append(aglBoard);
        document.body.appendChild(tagPanel);
    });
}

//繪製資訊展示板
let createDailog = function(DID, DTID, DCID) {
    if (DID != null && DID.length > 0) dailogID = DID;
    if (DTID != null && DTID.length > 0) dailogTitleID = DTID;
    if (DCID != null && DCID.length > 0) dailogContentID = DCID;

    //資訊展示板底板
    let dailogMask = document.createElement("div");
    let dailogMask_Style = "top: 1vh; ";
    dailogMask_Style += "left: 2vw; ";
    dailogMask_Style += "width: 96vw; ";
    dailogMask_Style += "height: 0px; ";
    dailogMask_Style += "padding: 10px; ";
    dailogMask_Style += "position: fixed; ";
    dailogMask_Style += "z-index: 6000; ";
    dailogMask_Style += "overflow: hidden; ";
    dailogMask_Style += "opacity: 0; ";
    dailogMask_Style += "transition: all 0.5s;";

    $(dailogMask).attr("id", dailogID);
    $(dailogMask).attr("style", dailogMask_Style);

    //youtube 用樣式
    let styleTag = document.createElement("style");
    let styleTag_Content = "";
    //.embed-container
    styleTag_Content += ".embed-container {";
    styleTag_Content += "position: relative; ";
    styleTag_Content += "padding-bottom: 56.25%; ";
    styleTag_Content += "margin : 20px 0px; ";
    styleTag_Content += "height: 0; ";
    styleTag_Content += "overflow: hidden; ";
    styleTag_Content += "max-width: 100%; ";
    styleTag_Content += "} ";
    styleTag_Content += ".embed-container iframe, ";
    styleTag_Content += ".embed-container object, ";
    styleTag_Content += ".embed-container embed {";
    styleTag_Content += "position: absolute; ";
    styleTag_Content += "top: 0; ";
    styleTag_Content += "left: 0; ";
    styleTag_Content += "width: 100%; ";
    styleTag_Content += "height: 100%; ";
    styleTag_Content += "} ";
    //.dailog_title
    styleTag_Content += ".dailog_title {";
    //styleTag_Content += "font-size: Zpx; ";
    styleTag_Content += "font-weight: 800; ";
    styleTag_Content += "letter-spacing: 1px;";
    styleTag_Content += "} ";
    //.dialog_content
    styleTag_Content += ".dialog_content {";
    styleTag_Content += "height: 90%; ";
    styleTag_Content += "color: rgba(0, 0, 0, .8); ";
    styleTag_Content += "text-align: justify; ";
    styleTag_Content += "letter-spacing: 1px; ";
    styleTag_Content += "overflow-y: auto;";
    styleTag_Content += "} ";
    //.contentImg
    styleTag_Content += ".contentImg {";
    styleTag_Content += "width: 100%; ";
    styleTag_Content += "margin-bottom: 20px; ";
    styleTag_Content += "border-radius: 5px; ";
    styleTag_Content += "} ";
    //.gg_map
    styleTag_Content += ".gg_map {";
    styleTag_Content += "height: 50vw; ";
    styleTag_Content += "width: 99vw; ";
    styleTag_Content += "border-radius: 100vw 100vw 0 0; ";
    styleTag_Content += "position: fixed !important; ";
    styleTag_Content += "left: 0; ";
    styleTag_Content += "bottom: 0; ";
    styleTag_Content += "background: rgba(255, 0, 0, .3); ";
    styleTag_Content += "overflow: hidden; ";
    styleTag_Content += "}";
    //.map_portrait
    styleTag_Content += ".map_portrait {";
    styleTag_Content += "top : calc(100vh - 51vw); ";
    styleTag_Content += "left : 0px; ";
    styleTag_Content += "}";
    //.map_landscape
    styleTag_Content += ".map_landscape {";
    styleTag_Content += "top : 0px; ";
    styleTag_Content += "left : calc(100vw - 50vh); ";
    styleTag_Content += "}";
    //.gg_map
    styleTag_Content += "@media (orientation: portrait) {";
    styleTag_Content += ".gg_map {";
    styleTag_Content += "top : calc(100vh - 80vw); ";
    styleTag_Content += "left : 0px; ";
    styleTag_Content += "width : 100vw; ";
    styleTag_Content += "height : 100vw; ";
    styleTag_Content += "border-radius : 100vw; ";
    styleTag_Content += "background : rgba(255, 0, 0, .3); ";
    styleTag_Content += "position : fixed; ";
    styleTag_Content += "z-index : 3000; ";
    styleTag_Content += "}";
    styleTag_Content += ".gg_headingBoard {";
    styleTag_Content += "left : calc((100vw - " + ggmap_headingPointerBoard + "px) / 2); ";
    styleTag_Content += "bottom : 0px; ";
    styleTag_Content += "}";
    styleTag_Content += "}";
    styleTag_Content += "@media (orientation: landscape) {";
    styleTag_Content += ".gg_map {";
    styleTag_Content += "top : 0px; ";
    styleTag_Content += "left : calc(100vw - 50vh); ";
    styleTag_Content += "width : 90vh; ";
    styleTag_Content += "height : 90vh; ";
    styleTag_Content += "border-radius : 100vh; ";
    styleTag_Content += "background : rgba(255, 0, 0, .3); ";
    styleTag_Content += "position : fixed; ";
    styleTag_Content += "z-index : 3000; ";
    styleTag_Content += "}";
    styleTag_Content += ".gg_headingBoard {";
    styleTag_Content += "right : 0px; ";
    styleTag_Content += "top : calc((100vh - " + ggmap_headingPointerBoard + "px) / 2); ";
    styleTag_Content += "}";
    styleTag_Content += "}";

    $(styleTag).html(styleTag_Content);
    $(dailogMask).append(styleTag);

    //關閉視窗按鈕
    let colseBtnIcon = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    let colseBtnIcon_Style = "top : 20px; ";
    colseBtnIcon_Style += "right : 15px; ";
    colseBtnIcon_Style += "width : 20px; ";
    colseBtnIcon_Style += "opacity : .5; ";
    colseBtnIcon_Style += "position : absolute;";

    //$(colseBtnIcon).attr("xmlns", "http://www.w3.org/2000/svg");
    $(colseBtnIcon).attr("viewBox", "0 0 512 512");
    $(colseBtnIcon).attr("style", colseBtnIcon_Style);
    $(colseBtnIcon).on("click", function() {
        $("#" + dailogID).css({ "height": "0px", "opacity": "0" });
    });

    //關閉視窗按鈕 SVG 路徑
    let colseBtnIcon_Path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    let colseBtnIcon_Path_Direction = "M0 256C0 114.6 114.6 0 256 0C397.4 ";
    colseBtnIcon_Path_Direction += "0 512 114.6 512 256C512 397.4 ";
    colseBtnIcon_Path_Direction += "397.4 512 256 512C114.6 512 0 ";
    colseBtnIcon_Path_Direction += "397.4 0 256zM175 208.1L222.1 255.1L175 303C165.7 ";
    colseBtnIcon_Path_Direction += "312.4 165.7 327.6 175 336.1C184.4 346.3 ";
    colseBtnIcon_Path_Direction += "199.6 346.3 208.1 336.1L255.1 289.9L303 336.1C312.4 ";
    colseBtnIcon_Path_Direction += "346.3 327.6 346.3 336.1 336.1C346.3 327.6 ";
    colseBtnIcon_Path_Direction += "346.3 312.4 336.1 303L289.9 255.1L336.1 208.1C346.3 ";
    colseBtnIcon_Path_Direction += "199.6 346.3 184.4 336.1 175C327.6 165.7 ";
    colseBtnIcon_Path_Direction += "312.4 165.7 303 175L255.1 222.1L208.1 175C199.6 ";
    colseBtnIcon_Path_Direction += "165.7 184.4 165.7 175 175C165.7 184.4 ";
    colseBtnIcon_Path_Direction += "165.7 199.6 175 208.1V208.1z";

    $(colseBtnIcon_Path).attr("d", colseBtnIcon_Path_Direction);
    $(colseBtnIcon).append(colseBtnIcon_Path);

    //資訊展示板主版
    let dailogBody = document.createElement("div");
    //let dailogBody_style = "max-width : 375px; ";
    //dailogBody_style += "width : 100%; ";
    let dailogBody_style = "width : 100%; ";
    dailogBody_style += "padding : 20px; ";
    dailogBody_style += "min-height : 540px; ";
    dailogBody_style += "height : 100%; ";
    dailogBody_style += "background : rgb(255, 255, 255); ";
    dailogBody_style += "border-radius : 20px; ";
    dailogBody_style += "box-shadow: 5px 5px 10px rgba(0, 0, 0, 0.5);";

    $(dailogBody).attr("style", dailogBody_style);
    $(dailogBody).append(colseBtnIcon);

    //資訊展示板主版-抬頭列
    let dailogTitle = document.createElement("div");
    /*
    let dailogTitle_style = "font-size : 24px; ";
    dailogTitle_style += "font-weight : 800; ";
    dailogTitle_style += "letter-spacing : 1px;";

    $(dailogTitle).attr("id", dailogTitleID);
    $(dailogTitle).attr("style", dailogTitle_style);
    */
    let dailogTitleContent = document.createElement("h3");
    $(dailogTitleContent).attr("id", dailogTitleID);
    $(dailogTitleContent).css({ "letter-spacing": "1px", "height": "36px", "line-height": "36px", "font-size": "24px", "margin-bottom": "10px" });

    $(dailogTitle).append(dailogTitleContent);

    //資訊展示板主版-內容列
    let dailogContent = document.createElement("div");
    let dailogContent_style = "height : 90%; ";
    dailogContent_style += "lineHeight:  : 24px; ";
    dailogContent_style += "color : rgba(0, 0, 0, .8); ";
    dailogContent_style += "font-size : 16px; ";
    dailogContent_style += "text-align : justify; ";
    dailogContent_style += "letter-spacing : 1px; ";
    dailogContent_style += "overflow-y : auto;";

    $(dailogContent).attr("id", dailogContentID);
    $(dailogContent).attr("style", dailogContent_style);

    $(dailogBody).append(dailogTitle);
    $(dailogBody).append(dailogContent);
    $(dailogMask).append(dailogBody);

    document.body.appendChild(dailogMask);
}

//顯示目標物件
let showLocaton = function() {
    let debugStr = "div_heading => " + div_heading + "<br>";
    debugStr += "locations => " + locations.length + "<br>";
    debugStr += "deviceType => " + deviceType + "<br>";
    if (locations.length > 0 && FOV_Range > 0 && panelID && panelID != "") {
        FOV_Left = div_heading - (FOV_Range / 2);
        if (FOV_Left < 0) FOV_Left += 360;

        FOV_Right = div_heading + (FOV_Range / 2);
        if (FOV_Right > 360) FOV_Right -= 360;

        debugStr += "heading => " + div_heading + "<br>";
        debugStr += "horizontal => " + div_heading_horizontal + "<br>";
        debugStr += "orientation => " + div_orientation + "<br>";
        debugStr += "FOV_Left => " + FOV_Left + "<br>";
        debugStr += "FOV_Right => " + FOV_Right + "<br>";

        locations.sort(function(a, b) {
            return a.distance - b.distance;
        });

        locations.forEach(function(coordinate, index) {
            let pno = index + 1;
            let objAgl = getAngle(geo_latitude, geo_longitude, coordinate.latitude, coordinate.longitude);
            let distance = Math.round(getDistance(geo_latitude, geo_longitude, coordinate.latitude, coordinate.longitude) * 1000, 2);

            let inFOV = false;

            if (FOV_Left > FOV_Right) {
                inFOV = objAgl >= FOV_Left || objAgl <= FOV_Right;
            } else {
                inFOV = objAgl >= FOV_Left && objAgl <= FOV_Right;
            }

            if (inFOV) {
                let scale = 1.2 + -0.4 * (coordinate.distance / locations[locations.length - 1].distance);

                debugStr += index + ". " + coordinate.name + " => " + objAgl + "<br>";
                let aglSpace = objAgl - FOV_Left > 0 ? (objAgl - FOV_Left) / FOV_Range : ((360 - FOV_Left) + objAgl) / FOV_Range;
                debugStr += "aglSpace => " + aglSpace + "<br>";
                let paneltop = "";
                if (window.matchMedia("(orientation: portrait)").matches) {
                    panelLeft = "calc(" + Math.round(aglSpace * 100) + "vw - " + Math.round(panelWidth / 2) + "px)";
                    paneltop = "calc(5vh + " + Math.round(coordinate.top + (div_heading_horizontal - 90) * 10) + "px)";
                } else {
                    panelLeft = "calc(" + Math.round(aglSpace * 100 - 50) + "vw - " + Math.round(panelWidth / 2) + "px)";
                    paneltop = Math.round(coordinate.top + (div_heading_horizontal - 90)) + "px";
                }
                debugStr += "distance => " + distance + "<br>";
                debugStr += "scale => " + scale + "<br>";

                $("#" + angleID + pno).html("距離 : " + distance + "M");
                $("#" + panelID + pno).css("display", "block");
                $("#" + panelID + pno).css("left", panelLeft);
                $("#" + panelID + pno).css("top", paneltop);
                $("#" + panelID + pno).css("transform", "scale(" + scale + " , " + scale + ")");
                $("#" + panelID + pno).css("z-index", (locations.length - index) * 10);
            } else {
                $("#" + panelID + pno).css("display", "none");
            }

            coordinate.distance = distance;

            let dotLeft = radarWidth / 2 + (geo_latitude - coordinate.latitude) * radarWidth * 300;
            let dotTop = radarWidth / 2 + (geo_longitude - coordinate.longitude) * radarWidth * 300;
            $("#dot_" + index).css("margin-left", dotLeft + "px");
            $("#dot_" + index).css("margin-top", dotTop + "px");
        });
    }

    $("#infoPanel").html(debugMode ? debugStr : "");
}

//建立 google map
let createGoogleMap = function() {
    let googleMapObj = document.createElement("div");
    $(googleMapObj).attr("id", "map");
    $(googleMapObj).attr("class", "gg_map");

    document.body.appendChild(googleMapObj);

    let ggm_headingBoard = document.createElement("div");
    $(ggm_headingBoard).attr("id", "gg_headingBoard");
    let ggm_headingBoard_style = "width : " + ggmap_headingPointerBoard + "px; ";
    ggm_headingBoard_style += "height : " + ggmap_headingPointerBoard + "px; ";
    ggm_headingBoard_style += "position : fixed; ";
    //ggm_headingBoard_style += "background : rgba(0, 255, 255, .3); ";
    ggm_headingBoard_style += "z-index : 3001; ";
    $(ggm_headingBoard).attr("style", ggm_headingBoard_style);
    $(ggm_headingBoard).attr("class", "gg_headingBoard");

    let ggm_headingPointerArrow = document.createElement("div");
    let ggmap_headingPointerBoard_space = Math.floor(ggmap_headingPointerBoard / 8);
    let ggm_headingPointerArrow_style = "width : 0px; ";
    ggm_headingPointerArrow_style += "height : 0px; ";
    ggm_headingPointerArrow_style += "margin-top : " + ggmap_headingPointerBoard_space + "px; ";
    ggm_headingPointerArrow_style += "margin-left : " + (ggmap_headingPointerBoard_space * 3) + "px; ";
    ggm_headingPointerArrow_style += "border-left : " + ggmap_headingPointerBoard_space + "px transparent solid; ";
    ggm_headingPointerArrow_style += "border-right : " + ggmap_headingPointerBoard_space + "px transparent solid; ";
    ggm_headingPointerArrow_style += "border-bottom : " + ggmap_headingPointerBoard_space + "px rgba(0,66,255,.5) solid; ";
    $(ggm_headingPointerArrow).attr("style", ggm_headingPointerArrow_style);

    $(ggm_headingBoard).append(ggm_headingPointerArrow);

    let ggm_headingPointerCore = document.createElement("div");
    let ggm_headingPointerCore_space = Math.floor(ggmap_headingPointerBoard / 2 - 2);
    let ggm_headingPointerCore_style = "width : " + ggm_headingPointerCore_space + "px; ";
    ggm_headingPointerCore_style += "height : " + ggm_headingPointerCore_space + "px; ";
    ggm_headingPointerCore_style += "margin-top : 1px; ";
    ggm_headingPointerCore_style += "margin-left : " + (ggm_headingPointerCore_space / 2 + 1) + "px; ";
    ggm_headingPointerCore_style += "border-radius : " + Math.floor(ggm_headingPointerCore_space / 2) + "px; ";
    ggm_headingPointerCore_style += "background : rgba(0,66,255,.5); ";
    $(ggm_headingPointerCore).attr("style", ggm_headingPointerCore_style);

    $(ggm_headingBoard).append(ggm_headingPointerCore);
    document.body.appendChild(ggm_headingBoard);

}

let createRadar = function() {
    let radarBoard = document.createElement("div");
    let radarBoardCSS = "top : 4vh; ";
    radarBoardCSS += "left : 5vw; ";
    radarBoardCSS += "width : " + radarWidth + "px; ";
    radarBoardCSS += "height : " + radarWidth + "px; ";
    radarBoardCSS += "border-radius : " + Math.round(radarWidth / 2) + "px; ";
    radarBoardCSS += "background : rgba(114, 177, 194, .5); ";
    radarBoardCSS += "position : fixed; ";
    radarBoardCSS += "z-index : 1000; ";

    radarBoard.setAttribute("style", radarBoardCSS);

    let radaInner = document.createElement("div");
    let radarInnerCSS = "width : " + radarWidth / 2 + "px; ";
    radarInnerCSS += "height : " + radarWidth / 2 + "px; ";
    radarInnerCSS += "margin-top : " + radarWidth / 4 + "px; ";
    radarInnerCSS += "margin-left : " + radarWidth / 4 + "px; ";
    radarInnerCSS += "border-radius : " + Math.round(radarWidth / 4) + "px; ";
    radarInnerCSS += "background : rgba(114, 177, 194, .5); ";
    radarInnerCSS += "position : absolute; ";
    radarInnerCSS += "z-index : 1001; ";

    radaInner.setAttribute("style", radarInnerCSS);

    let dotBoard = document.createElement("div");
    let dotBoardCSS = "width : " + radarWidth + "px; ";
    dotBoardCSS += "height : " + radarWidth + "px; ";
    dotBoardCSS += "position : absolute; ";
    dotBoardCSS += "z-index : 1002; ";
    //dotBoardCSS += "background : rgba(255, 255, 0, .2); ";

    dotBoard.setAttribute("id", "dotBoard");
    dotBoard.setAttribute("style", dotBoardCSS);

    locations.forEach(function(dot, index) {
        //dot.distance = Math.round(getDistance(latitude, longitude, dot.latitude, dot.longitude, "K") * 1000, 2);
        //產生虛擬招牌
        let dotObj = document.createElement("div");
        let dotObjCss = "width : 3px; ";
        dotObjCss += "height : 3px; ";

        dotObjCss += "margin-left : " + radarWidth + "px; ";
        dotObjCss += "margin-top : " + radarWidth + "px; ";
        dotObjCss += "background : " + dot.color + "; ";
        dotObjCss += "position : absolute; ";
        dotObjCss += "z-index : " + (1003 + index) + "; ";
        //console.log(dotObjCss);

        dotObj.setAttribute("id", "dot_" + index);
        dotObj.setAttribute("style", dotObjCss);
        dotBoard.append(dotObj);
    });

    let centerDotObj = document.createElement("div");
    let centerDotObjCss = "width : 3px; ";
    centerDotObjCss += "height : 3px; ";
    centerDotObjCss += "margin-left : " + radarWidth / 2 + "px; ";
    centerDotObjCss += "margin-top : " + radarWidth / 2 + "px; ";
    centerDotObjCss += "background : rgb(255, 0, 0); ";
    centerDotObjCss += "position : absolute; ";
    centerDotObjCss += "z-index : 2000; ";
    centerDotObj.setAttribute("style", centerDotObjCss);
    dotBoard.append(centerDotObj);

    radarBoard.append(radaInner);
    radarBoard.append(dotBoard);
    document.body.append(radarBoard);
}

window.addEventListener("deviceorientation", function(event) {
    let ori_alpha = event.alpha;
    let ori_beta = event.beta;
    div_heading_horizontal = event.beta;
    let ori_gamma = event.gamma;
    div_rotation = event.alpha;
    div_orientation = Math.abs(window.orientation);

    // 判斷是否為 iOS 裝置
    if (event.webkitCompassHeading) {
        deviceType = "IOS";
        div_heading = event.webkitCompassHeading; // iOS 裝置必須使用 event.webkitCompassHeading
    } else {
        deviceType = "Android";
        div_heading = ori_alpha;
    }

    if (div_orientation == 90) {
        $("#map").removeClass("map_portrait");
        $("#map").addClass("map_landscape");
    } else {
        $("#map").removeClass("map_landscape");
        $("#map").addClass("map_portrait");
    }

    navigator.geolocation.getCurrentPosition((position) => {
        geo_latitude = position.coords.latitude; //緯度
        geo_longitude = position.coords.longitude; //經度
    });

    if (map == null && geo_latitude != null && geo_longitude != null) {
        map = new google.maps.Map(document.getElementById("map"), {
            center: {
                lat: geo_latitude,
                lng: geo_longitude
            },
            zoom: 18,
            heading: Math.round(div_heading)
        });
    } else {
        //map.setHeading(heading);
        try {
            if (map != null) {
                map.moveCamera({
                    center: new google.maps.LatLng(geo_latitude, geo_longitude),
                    heading: Math.round(div_heading)
                });
                $("#logger_panel").html(map.getHeading());
            }
        } catch (e) {
            $("#logger_panel").html(e);
        }
    }

    $("#gg_headingBoard").css("transform", "rotate(" + div_heading + "deg)");
    let radarHeading = initAngle - div_heading - 90;
    if (radarHeading < 0) radarHeading += 360;
    $("#dotBoard").css("transform", "rotate(" + radarHeading + "deg)");

    //$("#infoPanel").html("heading => " + div_heading);
    showLocaton();
});

window.onload = function() {
    initAngle = div_heading;
    createDailog();
    createGoogleMap();
    createRadar();
}