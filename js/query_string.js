var pager = {};  //分頁用全域變數

var query_obj = {}; //此物件會在on ready後，將query string自動整理進此 object

$(document).ready(function () {
    query_obj_initialize();
    
});

/* ==============================
 * 利用這個function在頁面之間跳轉，並保留原始的query string
 * 可傳入額外的extra_parm參數，附加在後面，格式為 'parm1=value1&parm2=value2'
 * ============================== */

function query_redir(url, extra_parm, open_window) {
    if (typeof extra_parm !== 'undefined' && extra_parm !== '') {
        var hashes = extra_parm.split('&');
        var hash;

        for (var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            if (hash[0] !== '' && typeof hash[1] !== 'undefined') {
                if (hash[1] !== '') {
                    query_obj[hash[0]] = hash[1];
                } else {    //傳入空字串可移除此參數
                    delete query_obj[hash[0]];
                }
            }
        }
    }

    var qs = query_obj_to_string();

    if (qs !== '') {
        if (open_window) {
            window.open(url + '?' + qs);
        } else {
            location.href = url + '?' + qs;
        }
    } else {
        if (open_window) {
            window.open(url);
        } else {
            location.href = url;
        }
    }
}

function confirm_redir(msg, url, parm_string) {
    if (confirm(msg)) {
        query_redir(url, parm_string);
    } else {
        return false;
    }
}

/*
 * 取得query string，並以object的方式回傳
 */
function getUrlVars()
{
    var vars = {};
    var hash;
    var hashes = location.search.slice(1).split('&')
    for (var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        if (hash[0] !== '' && typeof hash[1] !== 'undefined' && hash[1] !== '') {
            vars[hash[0]] = hash[1];
        }
    }
    return vars;
}

function query_obj_initialize() {
    query_obj = getUrlVars();
}

function on_query_obj_modified() {
    var url = '/' + page;
    var query_string = query_obj_to_string();

    if (query_string !== '') {
        url = url + '?' + query_string;
    }

    location.href = url;
}

function query_obj_to_string() {
    var query_string = '';

    for (var index in query_obj) {
        if (query_string === '') {
            query_string = index + '=' + query_obj[index];
        }
        else {
            query_string += '&' + index + '=' + query_obj[index];
        }
    }

    return query_string;
}
