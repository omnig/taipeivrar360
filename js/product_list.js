/*
 * 以ajax方式讀取商品清單
 */
var product_list = function () {
    var base_url = '/';
    var href_base_url = '/';
    var container = 'body';
    var where;      //過濾條件(含目前顯示頁數p，會轉成get參數)
    var order;      //排序方式
    var product_list;   //載入後的商品清單
    var i18n;       //多國語言文字
    var autoload;   //是否自動載入
    var list_style;      //商品顯示樣式，可為row: 每商品一列；grid: 格網式顯示
    var s_id;   //商店id

    /*
     * 全部重置
     * @returns {undefined}
     */
    function reset_all() {
        where = {
            p: 1,
            limit: 20
        };
        order = 0;
        product_list = {};
        autoload = true;
        read_local();
    }

    /*
     * 重新、從頭載入
     * @returns {undefined}
     */
    function reload() {
        where.p = 1;
        product_list = {};
        read_local();
    }

    /*
     * 記錄在本地端
     * @returns {undefined}
     */
    function save_local(style) {
        // Store
        var expire_time = new Date();
        var days = 1;
        expire_time.setTime(expire_time.getTime() + (days * 24 * 60 * 60 * 1000));

        $.cookie("list_style", style, {path: '/', expires: expire_time});
        list_style = $.cookie("list_style");
    }

    /*
     * 讀取本地端記錄
     * @returns {undefined}
     */
    function read_local() {
        if (!$.cookie("list_style")) {
            save_local("grid");
        }
        list_style = $.cookie("list_style");
    }

    /*
     * 顯示載入中
     * @returns {undefined}
     */
    function show_loading() {
        var loading_html = "<div class='loading text-center c-theme-color'><i class='fa fa-spinner fa-spin'></i> loading...</div>";

        $(container).append(loading_html);
    }

    /*
     * 移除載入中
     * @returns {undefined}
     */
    function hide_loading() {
        $(container).find('.loading').remove();
    }

    /*
     * 建立row單一商品的html
     * @param {object} product
     * @returns {String}
     */
    function build_row_product_html(product) {
        var sale_mark = (parseInt(product.p_price) < parseInt(product.p_origin_price)) ? '<div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Sale</div>' : '';
        var new_mark = (product.is_new) ? '<div class="c-label c-label-right c-theme-bg c-font-uppercase c-font-white c-font-13 c-font-bold">New</div>' : '';
        var origin_price = (parseInt(product.p_price) < parseInt(product.p_origin_price)) ? "<span class='c-font-26 c-font-line-through c-font-red'>NTD. " + $.number(product.p_origin_price) + "</span>" : '';
        var control_btns;

        if (product.p_serial_enabled === "N") {
            if (product.p_inventory > 0) {
                control_btns = "<button type='button' onclick='cart.add_item(" + product.p_id + ")' class='btn btn-sm c-theme-btn c-btn-square c-btn-uppercase c-btn-bold'>" +
                        "<i class='fa fa-shopping-cart'></i>" + i18n["加入購物車"] + "</button>";
            } else {
                control_btns = "<button type='button' class='btn btn-sm c-btn-red c-btn-square c-btn-uppercase c-btn-bold'>" +
                        "<i class='fa fa-remove'></i>" + i18n["商品已售完"] + "</button>";
            }
        } else {
            control_btns = "<a href='" + href_base_url + "product/" + product.p_id + "' class='btn btn-sm c-theme-btn c-btn-square c-btn-uppercase c-btn-bold'>" +
                    "<i class='fa fa-list-ul'></i>" + i18n["商品內容"] + "</a>";
        }

        var html =
                "<div class='row c-margin-b-40'>" +
                "   <div class='c-content-product-2 c-bg-white'>" +
                "       <div class='col-md-4'>" +
                "          <div class='c-content-overlay c-margin-b-15'>" +
                "               <a href='" + href_base_url + "product?p_id=" + product.p_id + "'>" + sale_mark + new_mark +
                "                   <div class='c-bg-img-center c-overlay-object hover_zoom' data-height='height' style='height: 230px; background-image: url(" + product.p_image + ");'></div>" +
                "               </a>" +
                "          </div>" +
                "       </div>" +
                "       <div class='col-md-8'>" +
                "           <div class='c-info-list'>" +
                "               <h3 class='c-title c-font-bold c-font-22 c-font-dark'>" +
                "                   <a class='c-theme-link' href='" + href_base_url + "product/" + product.p_id + "'>" + product.p_name + "</a>" +
                "                   <div class='pull-right c-font-13 c-font-thin margin_top_6'>" +
                "                       (<a href='" + href_base_url + "store/" + product.s_id + "'>" + product.s_name + "</a> / " + product.last_update_date + ")" +
                "                   </div>" +
                "               </h3>" +
                "               <p class='c-desc c-font-16 c-font-thin'>" + product.p_description + "</p>" +
                "               <p class='c-price c-font-26 c-font-thin'>NTD. " + $.number(product.p_price) + "&nbsp;" + origin_price + "</p>" +
                "           </div>" +
                "       <div>" + control_btns +
                "           <button type='button' onclick='favorite.add(" + product.p_id + ")' p_id='" + product.p_id + "' class='add_favorite_btn btn btn-sm btn-default c-btn-square c-btn-uppercase c-btn-bold'>" +
                "           <i class='fa fa-heart-o'></i>" + i18n["加入願望清單"] + "</button>" +
                "       </div>" +
                "   </div>" +
                "</div>";

        return html;
    }

    /*
     * 建立grid單一商品的html
     * @param {object} product
     * @returns {String}
     */
    function build_grid_product_html(product) {
        var sale_mark = (parseInt(product.p_price) < parseInt(product.p_origin_price)) ? '<div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Sale</div>' : '';
        var new_mark = (product.is_new) ? '<div class="c-label c-label-right c-theme-bg c-font-uppercase c-font-white c-font-13 c-font-bold">New</div>' : '';
        var origin_price = (parseInt(product.p_price) < parseInt(product.p_origin_price)) ? "<span class='c-font-16 c-font-line-through c-font-red'>NTD. " + $.number(product.p_origin_price) + "</span>" : '';
        var control_btns;

        if (product.p_serial_enabled === "N") {
            if (product.p_inventory > 0) {
                control_btns = "<a href='javascript:cart.add_item(" + product.p_id + ")' class='btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product'>" + i18n["加入購物車"] + "</a>";
            } else {
                control_btns = "<a href='javascript:;' class='btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product'>" + i18n["商品已售完"] + "</a>";
            }
        } else {
            control_btns = "<a href='" + href_base_url + "product/" + product.p_id + "' class='btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product'>" + i18n["商品內容"] + "</a>";
        }

        var html =
                "<div class='col-sm-6 col-md-6 col-lg-4 c-margin-b-20 grid_small_space'>" +
                "   <div class='c-content-product-2 c-bg-white light_gray_border'>" +
                "       <div class='c-content-overlay'>" +
                "           <a href='" + href_base_url + "product?p_id=" + product.p_id + "'>" + sale_mark + new_mark +
                "               <div class='c-bg-img-center c-overlay-object hover_zoom' data-height='height' style='height: 270px; background-image: url(" + product.p_image + ");'></div>" +
                "               </div>" +
                "           </a>" +
                "           <div class='c-info'>" +
                "               <p class='c-title c-font-18 c-font-slim' style='height: 50px; overflow: hidden'>" + product.p_name + "</p>" +
                "               <p class='c-price c-font-16 c-font-slim'>NTD. " + $.number(product.p_price) + " &nbsp;" + origin_price + "</p>" +
                "           </div>" +
                "           <div class='btn-group btn-group-justified' role='group'>" +
                "               <div class='btn-group c-border-top' role='group'>" +
                "                   <a href='javascript:favorite.add(" + product.p_id + ")' p_id='" + product.p_id + "' class='add_favorite_btn btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product'>" + i18n["加入願望清單"] + "</a>" +
                "               </div>" +
                "               <div class='btn-group c-border-left c-border-top' role='group'>" + control_btns +
                "               </div>" +
                "           </div>" +
                "       </div>" +
                "   </div>" +
                "</div>";

        return html;
    }

    /*
     * 建立單一商品的html，依list_style分別呼叫相對函式
     * @param {object} product
     * @returns {String}
     */
    function build_product_html(product) {
        switch (list_style) {
            case 'row':
                return build_row_product_html(product);
                break;
            case 'grid':
            default:
                return build_grid_product_html(product);
                break;
        }
    }

    /*
     * 整理從server端取回的資料
     * @param {object} data
     * @returns {undefined}
     */
    function product_list_parser(data) {
        for (var i in data) {
            var id = 'p_id_' + data[i].p_id;

            //過濾重複的資料不顯示
            if (typeof product_list[id] === 'undefined') {
                product_list[id] = data[i];

                //顯示商品清單
                var html = build_product_html(data[i]);
                $(container).append(html);
            }
        }
    }

    /*
     * 顯示載入下一頁的按鈕
     * @returns {undefined}
     */
    function show_load_next_page_btn() {
        var btn_html =
                "<div class='load_next_page_btn text-center c-theme-color'>" +
                "   <button type='button' class='btn btn-success fa fa-arrow-circle-down' onclick='product_list.load_next_page()'> More</button>" +
                "</div>";

        $(container).append(btn_html);

        //自動載入下一頁資料
        if (autoload) {
            $(window).scroll("autoload", function () {
                if ($('.load_next_page_btn').length > 0) {
                    var scroll_pos = $(window).scrollTop();
                    var window_height = $(window).height();
                    var btn_position = $('.load_next_page_btn').position().top;

                    //剩半個螢幕高就要捲完時，就載入下一批
                    if (btn_position < scroll_pos + window_height * 1.5) {
                        $(window).off("scroll", "autoload");
                        $(container).find(".load_next_page_btn button").click();
                    }
                }
            });
        }
    }

    /*
     * 移除載入下一頁按鈕
     * @returns {undefined}
     */
    function hide_load_next_page_btn() {
        $(container).find('.load_next_page_btn').remove();
    }

    /*
     * ajax查詢
     * @returns {undefined}
     */
    function ajax_go() {
        var url = base_url + 'ajax/product_list?sort=' + order;

        hide_load_next_page_btn();
        show_loading();

        if (typeof s_id !== 'undefined' && s_id) {
            where.s_id = s_id;
        }

        $.ajax({
            url: url,
            data: where,
            dataType: 'JSON',
            success: function (ret) {
                if (ret.result) {
                    //顯示商品清單
                    product_list_parser(ret.data.products);

                    //底部顯示繼續載入按鈕
                    if (ret.data.pagination.current_page < ret.data.pagination.total_page) {
                        show_load_next_page_btn();
                    }
                }
                hide_loading();
            }
        });
    }

    return {
        /*
         * 初始化
         * @param {string} base: 網站base_url
         * @param {string} href_base: 跳轉網址的base_url
         * @param {string} container_selector: 放置html的selector
         * @param {object} init_i18n: 初始化i18n字串
         * @returns {undefined}
         */
        init: function (base, href_base, container_selector, init_i18n) {
            base_url = base;
            href_base_url = href_base;
            i18n = init_i18n;
            container = container_selector;
            reset_all();

            //排列方式選取按鈕設定active
            $(".list_style_btns[data-list_style=" + list_style + "]").addClass("active");
        },
        /*
         * 重置條件
         * @returns {undefined}
         */
        clear: function () {
            reload();
            $(container).html("");
        },
        /*
         * 設定過濾條件
         * @param {string} key
         * @param {string} value
         * @returns {undefined}
         */
        set_where: function (key, value) {
            if (value) {
                where[key] = value;
            }
        },
        /*
         * 設定排序規則
         * @param {string} value
         * @returns {undefined}
         */
        set_sort: function (value) {
            order = value;
        },
        /*
         * 設定每頁筆數規則
         * @param {string} value
         * @returns {undefined}
         */
        set_limit: function (value) {
            where.limit = value;
        },
        /*
         * 設定顯示模式
         * @param {bool} value
         * @returns {undefined}
         */
        set_list_style: function (value) {
            save_local(value);   //local儲存
        },
        /*
         * 設定店家id
         * @param {number} value
         * @returns {undefined}
         */
        set_s_id: function (value) {
            s_id = value;
        },
        /*
         * 設定是否自動載入
         * @param {bool} value
         * @returns {undefined}
         */
        set_autoload: function (value) {
            autoload = value;
        },
        /*
         * 查詢
         * 傳入參數:
         * where: 過濾條件
         * order: 排序
         * limit: 每頁數量
         */
        load: function () {
            ajax_go();
        },
        /*
         * 查詢並載入下一頁
         */
        load_next_page: function () {
            where.p++;
            ajax_go();
        }
    };
}();