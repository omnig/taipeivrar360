let locations = [];

let FOV_Range = 120;

let FOV_Far = 500;

let panelWidth = 250;

let panelHeight = 30;

let fontSize = 20;

let action_panels = [];

let startRendering = false;

let debugMode = true;

let debugStr = "";

let geo_latitude, geo_longitude, initHeading;

let isInit = false;

//計算兩標的之間的角度
let getAngle = function(px1, py1, px2, py2) {
    let dx = px2 - px1
    let dy = py2 - py1
    let ang = Math.atan2(dy, dx) * 180 / Math.PI;

    return ang > 0 ? ang : 360 + ang;
}

//計算兩座標的距離 (unit : 單位 K-公里 / N-英里 / N-海里)
let getDistance = function(lat1, lon1, lat2, lon2) {
    if ((lat1 == lat2) && (lon1 == lon2)) {
        return 0;
    } else {
        var radlat1 = Math.PI * lat1 / 180;
        var radlat2 = Math.PI * lat2 / 180;
        var theta = lon1 - lon2;
        var radtheta = Math.PI * theta / 180;
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
            dist = 1;
        }
        dist = Math.acos(dist);
        dist = dist * 180 / Math.PI;
        dist = dist * 60 * 1.1515;
        return dist;
    }
}

let init = function() {

    locations.forEach(function(coordinate, index) {
        let pno = index + 1;
        let plateID = "plt_" + pno;
        let tagPanel = document.createElement("div");
        tagPanel.setAttribute("id", plateID);
        coordinate.top = pno * (panelHeight * 2);

        let strCSS = "top : " + coordinate.top + "px;";
        strCSS += "left : 0px;";
        strCSS += "width : " + panelWidth + "px; ";
        strCSS += "min-height : " + panelHeight + "px; ";
        strCSS += "position : fixed; ";
        strCSS += "z-index : " + pno * 10 + "; ";

        tagPanel.setAttribute("style", strCSS);
        tagPanel.setAttribute("onclick", "show_dailog(" + index + ")");

        let baseObj = document.createElement("div");

        let strBOCSS = "padding : 5px;";
        strBOCSS += "line-height : 30px; ";
        strBOCSS += "font-size : " + fontSize + "px; ";
        strBOCSS += "background : rgba(255, 255, 255, .8);";
        strBOCSS += "border-radius : 5px;";

        baseObj.setAttribute("style", strBOCSS);
        tagPanel.append(baseObj);

        let baseBoard = document.createElement("table");
        let BL1 = document.createElement("tr");
        let logoField = document.createElement("td");
        logoField.setAttribute("width", panelHeight * 2 + 5);
        logoField.setAttribute("rowspan", 2);
        BL1.append(logoField);

        let logoObj = document.createElement("div");
        let logoObjCSS = "width : " + (panelHeight * 2) + "px; ";
        logoObjCSS += "height : " + (panelHeight * 2) + "px; ";
        logoObjCSS += "line-height : " + (panelHeight * 2) + "px; ";
        logoObjCSS += "background : " + coordinate.color + "; ";
        logoObjCSS += "font-family : Arial; ";
        logoObjCSS += "font-size : " + Math.round(panelHeight * 0.8) + "px; ";
        logoObjCSS += "color : rgb(255, 255, 255); ";
        logoObjCSS += "text-align : center; ";
        logoObjCSS += "vertical-align : middle; ";
        logoObjCSS += "border-radius : 5px; ";
        logoObj.setAttribute("style", logoObjCSS);
        logoObj.innerHTML = "AR";

        logoField.append(logoObj);

        let locNameField = document.createElement("td");
        locNameField.setAttribute("style", "font-weight : 800");
        BL1.append(locNameField);

        let BL2 = document.createElement("tr");
        let locDistanceField = document.createElement("td");
        BL2.append(locDistanceField);

        baseBoard.append(BL1);
        baseBoard.append(BL2);
        baseObj.append(baseBoard);

        let mainBoard = document.createElement("div");
        let mainBoardCSS = "width : 100%;";
        mainBoardCSS += "min-height : " + panelHeight + "px;";
        mainBoardCSS += "line-height : 30px;";

        mainBoard.setAttribute("style", mainBoardCSS);
        mainBoard.innerHTML = coordinate.name;

        let aglBoard = document.createElement("div");
        aglBoard.setAttribute("id", "agl_" + pno);

        let AnchorPanel = document.createElement("div");
        tagPanel.append(AnchorPanel);

        let AnchorObj = document.createElement("div");
        let AnchorCSS = "width : 0px;";
        AnchorCSS += "height : 0px;";
        AnchorCSS += " border-top : 10px rgba(255, 255, 255, .8) solid;";
        AnchorCSS += " border-left : 6px transparent solid;";
        AnchorCSS += " border-right : 6px transparent solid;";
        AnchorCSS += " margin-left : auto;";
        AnchorCSS += " margin-right : auto;";

        AnchorObj.setAttribute("style", AnchorCSS);

        AnchorPanel.append(AnchorObj);

        locNameField.append(mainBoard);
        locDistanceField.append(aglBoard);
        document.body.appendChild(tagPanel);
    });
}

let setPlate = function(alpha, beta, gamma, heading) {
    let lat = geo_latitude;
    let lon = geo_longitude;

    if (debugMode) {
        debugStr = "ori_alpha => " + alpha + "<br>";
        debugStr += "ori_beta => " + beta + "<br>";
        debugStr += "ori_gamma => " + gamma + "<br>";
        debugStr += "geo_latitude => " + lat + "<br>";
        debugStr += "geo_longitude => " + lon + "<br>";
        debugStr += "initHeading => " + initHeading + "<br>";
        debugStr += "dvi_heading => " + heading + "<br>";
        debugStr += "locations => " + locations.length + "<br>";

        $("#infoPanel").html(debugStr);
    }

    if (locations.length > 0) {
        //重新排序虛擬招牌的順序
        locations.sort(function(a, b) {
            return a.distance - b.distance;
        });

        locations.forEach(function(coordinate, index) {
            let pno = index + 1;
            //let VOFStart = heading - (FOV_Range / 2) > 0 ? heading - (FOV_Range / 2) : 
            let viewAgl = getAngle(lat, lon, coordinate.latitude, coordinate.longitude) - heading;
            let objAgl = getAngle(lat, lon, coordinate.latitude, coordinate.longitude) - heading;
            let inFOV = viewAgl < (FOV_Range / 2 + 1) && viewAgl > -(FOV_Range / 2 + 1);
            let scale = 1.2 + -0.4 * (coordinate.distance / locations[locations.length - 1].distance);
            let distance = Math.round(getDistance(lat, lon, coordinate.latitude, coordinate.longitude) * 1000, 2);
            let plateID = "plt_" + pno;
            let FOV_Left = heading - (FOV_Range / 2) >= 0 ? heading - (FOV_Range / 2) : 360 + (heading - (FOV_Range / 2));
            let FOV_Right = heading + (FOV_Range / 2) <= 360 ? heading + (FOV_Range / 2) : heading + (FOV_Range / 2) - 360;
            debugStr = "heading => " + heading + "<br>";
            debugStr += "FOV_Left => " + FOV_Left + "<br>";
            debugStr += "FOV_Right => " + FOV_Right + "<br>";
            debugStr += "objAgl => " + objAgl + "<br>";

            if (objAgl >= FOV_Left && objAgl <= FOV_Right) {
                let panelLeft = "";
                let moveAgl = 0;

                if (FOV_Left > FOV_Right) {
                    if (objAgl < FOV_Left) {
                        panelLeft = "calc(" + Math.round(((FOV_Range / 2 + (objAgl - heading)) / FOV_Range) * 100) + "vw - " + Math.round(panelWidth / 2) + "px)";
                    } else {
                        panelLeft = "calc(" + Math.round(((objAgl - FOV_Left) / FOV_Range) * 100) + "vw - " + Math.round(panelWidth / 2) + "px)";
                    }
                } else {
                    panelLeft = "calc(" + Math.round(((objAgl - FOV_Left) / FOV_Range) * 100) + "vw - " + Math.round(panelWidth / 2) + "px)";
                }

                //let panelLeft = "calc(" + Math.round(viewAgl / (FOV_Range / 2) * 100) + "vw - " + panelWidth / 2 + "px)";
                $("#agl_" + pno).html("距離 : " + distance + "M");
                $("#" + plateID).css("display", "block");
                $("#" + plateID).css("left", panelLeft);
                $("#" + plateID).css("transform", "scale(" + scale + " , " + scale + ")");
                if (beta > 0) {
                    $("#" + plateID).css("top", coordinate.top + (beta - 90) * 10);
                }
            }

            coordinate.distance = distance;
            $("#infoPanel").html(debugStr + "<br>");
        });
    }
}

window.addEventListener("deviceorientation", function(event) {
    // alpha: rotation around z-axis
    let ori_alpha = event.alpha;
    let ori_beta = event.beta;
    let ori_gamma = event.gamma;
    let div_heading = 0

    let rotateDegrees_IOS = null;
    let rotateDegrees_Android = null;

    // 判斷是否為 iOS 裝置
    if (event.webkitCompassHeading) {
        dvi_heading = event.webkitCompassHeading; // iOS 裝置必須使用 event.webkitCompassHeading

        rotateDegrees_IOS = dvi_heading;
    } else {
        dvi_heading = ori_alpha;

        rotateDegrees_Android = dvi_heading;
    }

    if (initHeading == null) initHeading = dvi_heading;

    navigator.geolocation.getCurrentPosition((position) => {
        geo_latitude = position.coords.latitude; //緯度
        geo_longitude = position.coords.longitude; //經度
    });

    setPlate(ori_alpha, ori_beta, ori_gamma, dvi_heading);
});