/*
 * 對話框
 */
var dialog = function () {
    function inject_base_html() {
        var html = '<div class="modal fade" id="dialog" tabindex="-1" role="dialog">' +
                '<div class="modal-dialog" role="document">' +
                '<div class="modal-content">' +
                '<div class="modal-header">' +
                '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                '<h4 class="modal-title"></h4>' +
                '</div>' +
                '<div class="modal-body"></div>' +
                '</div>' +
                '</div>' +
                '</div>';
        $('body').append(html);
    }

    return {
        //初始化
        init: function () {
            inject_base_html();
        },
        //設定對話框大小
        set_size: function (size) {
            $('#dialog .modal-dialog').removeClass("modal-sm modal-lg modal-").addClass("modal-" + size);
        },
        //alert形式之對話框
        alert: function (title, msg, close_timer) {
            $('#dialog').find('.modal-title').html(title);
            $('#dialog').find('.modal-body').html(msg);

            if (typeof title !== 'undefined' && title.length > 0) {
                $('.modal-header').show();
            } else {
                $('.modal-header').hide();
            }
            
            $('#dialog').modal("show");

            if (typeof close_timer !== 'undefined') {
                setTimeout(function () {
                    $('#dialog').modal("hide");
                }, close_timer);
            }
        },
        show_iframe: function (title, src) {
            $('#dialog').find('.modal-title').html(title);
            $('#dialog').find('.modal-body').html('<iframe width="100%" class="modal_iframe" src=""></iframe>');
            
            if (typeof title !== 'undefined' && title.length > 0) {
                $('.modal-header').show();
            } else {
                $('.modal-header').hide();
            }
            
            $('#dialog').on('shown.bs.modal', function () {
                $('#dialog').unbind('shown.bs.modal');
                $(this).find('.modal_iframe').attr('src', src);
            });

            $('#dialog').modal("show");
        },
        close: function () {
            $('#dialog').modal("hide");
        }
    };
}();

$(document).ready(function () {
    dialog.init();
});