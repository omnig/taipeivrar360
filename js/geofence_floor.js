var area_select_map = function () {
    var init_keyword = '';  //初始時使用的keyword
    var active_info_window = false;
    var office_marker;
    var bounce_marker;
    var bounce_marker_animation = null;
    var bounce_info_windows;
    var bounce_circle;  //bounce marker下繪製的圓圈
    var bounce_circle_radius = $('.range').val() ? parseInt($('.range').val()) : 0;    //bounce marker下的圓圈範圍
    var markers = {};   //商店顯示marker
    var info_windows = {};  //info_window清單
    var base_url;   //網站base_url
    var geocoder;
    var keyword_search = false;
    var maptiler;
    var mapBounds;
    var mapMinZoom = 16;
    var mapMaxZoom = 23;

    /*
     * 重繪bounce marker所在位置下方圓圈
     * @param {type} lat
     * @param {type} lon
     * @returns {undefined}
     */
    function update_bounce_circle(lat, lon) {
        if (bounce_circle) {
            bounce_circle.setMap(null);
        }

        if (keyword_search) { //以keyword搜尋時，就不管位置距離
            return;
        }

        $('.search_input').val("");

        bounce_circle = new google.maps.Circle({
            strokeColor: '#ff6600',
            strokeOpacity: 0.5,
            strokeWeight: 1,
            fillColor: '#ff6600',
            fillOpacity: 0.15,
            zIndex: 10,
            map: map,
            center: new google.maps.LatLng(lat, lon),
            radius: bounce_circle_radius
        });
        bounce_circle.setMap(map);

        $('.newlat').html(lat);
        $('.newlng').html(lon);
        $('.lat').val(lat);
        $('.lng').val(lon);
    }

    function update_bounce_circle_rt(lat, lon) {
        if (bounce_circle) {
            bounce_circle.setMap(null);
        }

        if (keyword_search) { //以keyword搜尋時，就不管位置距離
            return;
        }

        $('.search_input').val("");

        bounce_circle_rt = new google.maps.Circle({
            strokeColor: '#ff6600',
            strokeOpacity: 0.5,
            strokeWeight: 0,
            fillColor: '#ff6600',
            fillOpacity: 0.15,
            zIndex: 10,
            map: map,
            center: new google.maps.LatLng(lat, lon),
            radius: bounce_circle_radius
        });
        bounce_circle_rt.setMap(map);

        $('.new_rt_lat').html(lat);
        $('.new_rt_lng').html(lon);
        $('.rt_lat').val(lat);
        $('.rt_lng').val(lon);
    }

    /*
     * 按下marker時處理
     * @returns {undefined}
     */
    function on_bounce_marker_mousedown() {
        if (bounce_marker_animation !== google.maps.Animation.dr) {
            bounce_marker_animation = google.maps.Animation.dr;
            bounce_marker.setAnimation(bounce_marker_animation);
        }
    }

    function on_bounce_marker_mousedown_rt() {
        if (bounce_marker_animation !== google.maps.Animation.dr) {
            bounce_marker_animation = google.maps.Animation.dr;
            bounce_marker_rt.setAnimation(bounce_marker_animation);
        }
    }

    /*
     * 拖拉marker期間畫面處理
     * @returns {undefined}
     */
    function on_bounce_marker_drag() {
        var new_lat = bounce_marker.position.lat();
        var new_lon = bounce_marker.position.lng();
        update_bounce_circle(new_lat, new_lon);
        bounce_info_windows.close();
    }

    function on_bounce_marker_drag_rt() {
        var new_lat = bounce_marker_rt.position.lat();
        var new_lon = bounce_marker_rt.position.lng();
        update_bounce_circle_rt(new_lat, new_lon);
        bounce_info_windows_rt.close();
    }

    /*
     * 拖拉marker放開時處理
     * @returns {undefined}
     */

    function on_bounce_marker_drop(keyword) {
        var new_lat = bounce_marker.position.lat();
        var new_lon = bounce_marker.position.lng();

        bounce_info_windows.open(map, bounce_marker);
        if (bounce_marker_animation !== google.maps.Animation.br) {
            bounce_marker_animation = google.maps.Animation.br;
            bounce_marker.setAnimation(bounce_marker_animation);
        }

        update_bounce_circle(new_lat, new_lon);
    }

    function on_bounce_marker_drop_rt(keyword) {
        var new_lat = bounce_marker_rt.position.lat();
        var new_lon = bounce_marker_rt.position.lng();

        bounce_info_windows_rt.open(map, bounce_marker_rt);
        if (bounce_marker_animation !== google.maps.Animation.br) {
            bounce_marker_animation = google.maps.Animation.br;
            bounce_marker_rt.setAnimation(bounce_marker_animation);
        }

        update_bounce_circle_rt(new_lat, new_lon);
    }

    /*
     * 載入tile map
     * @returns {undefined}
     */

    function load_tile_map(url_path, plan_id, bl_lat, bl_lng, tr_lat, tr_lng) {

        mapBounds = new google.maps.LatLngBounds(
                new google.maps.LatLng(bl_lat, bl_lng),
                new google.maps.LatLng(tr_lat, tr_lng));

        maptiler = new google.maps.ImageMapType({
            getTileUrl: function (coord, zoom) {
                var proj = map.getProjection();
                var z2 = Math.pow(2, zoom);
                var tileXSize = 256 / z2;
                var tileYSize = 256 / z2;
                var tileBounds = new google.maps.LatLngBounds(
                        proj.fromPointToLatLng(new google.maps.Point(coord.x * tileXSize, (coord.y + 1) * tileYSize)),
                        proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * tileXSize, coord.y * tileYSize))
                        );
                var y = coord.y;
                var x = coord.x >= 0 ? coord.x : z2 + coord.x;
                if (mapBounds.intersects(tileBounds) && (mapMinZoom <= zoom) && (zoom <= mapMaxZoom))
                    return url_path + "/" + plan_id + "/" + zoom + "/" + x + "/" + y + ".png";
                else
                    return url_path + "/" + "none.png";
            },
            tileSize: new google.maps.Size(256, 256),
            isPng: true,
            opacity: 1.0
        });
        map.overlayMapTypes.insertAt(0, maptiler);

    }



    /* *************************
     * public functions
     * *************************/
    return {
        /*
         * 本物件初始化函式
         * @param {type} init_base_url
         * @returns {undefined}
         */
        init: function (init_base_url, keyword) {
            base_url = init_base_url ? init_base_url : '';
            if (typeof keyword !== 'undefined' && keyword !== '') {
                init_keyword = keyword;
                keyword_search = true;
            }
        },
        /*
         * google map顯示完成後call back
         */
        googlemap_callback: function () {
            google.maps.event.addListener(map, "click", function (event) {
                if (active_info_window) {
                    active_info_window.close();
                    active_info_window = false;
                }
            });

            var office_info_window = new google.maps.InfoWindow({
                content: '若位置有誤請洽系統管理員'
            });
            office_marker = new google.maps.Marker({
                title: '局處位置',
                position: map.center,
                info_window: office_info_window
            });
            office_info_window.open(map, office_marker);


            var image = {
                url: base_url + 'images/bounce_marker.png',
                size: new google.maps.Size(33, 45),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 45)
            };
            bounce_info_windows = new google.maps.InfoWindow({
                content: '拖曳取得[左下位置]經緯度坐標'
            });
            bounce_marker = new google.maps.Marker({
                position: map.center,
                map: map,
                icon: image,
                info_window: bounce_info_windows,
                draggable: true
            });
            bounce_marker.setMap(map);
            bounce_info_windows.open(map, bounce_marker);
            bounce_marker.addListener('mousedown', on_bounce_marker_mousedown);
            bounce_marker.addListener('drag', on_bounce_marker_drag);
            bounce_marker.addListener('dragend', function () {
                on_bounce_marker_drop();
            });
            on_bounce_marker_drop(init_keyword);


            bounce_info_windows_rt = new google.maps.InfoWindow({
                content: '拖曳取得[右上位置]經緯度坐標'
            });

            var lat_rt = $('.rt_lat').val();
            var lon_rt = $('.rt_lng').val();

            bounce_marker_rt = new google.maps.Marker({
                position: new google.maps.LatLng(lat_rt, lon_rt),
                map: map,
                icon: image,
                info_window: bounce_info_windows_rt,
                draggable: true
            });
            bounce_marker_rt.setMap(map);
            bounce_info_windows_rt.open(map, bounce_marker_rt);
            bounce_marker_rt.addListener('mousedown', on_bounce_marker_mousedown_rt);
            bounce_marker_rt.addListener('drag', on_bounce_marker_drag_rt);
            bounce_marker_rt.addListener('dragend', function () {
                on_bounce_marker_drop_rt();
            });
        },
        /*
         * google map js 載入後自動call back
         */
        initMap: function () {
            google_map.init();

            /*
             var autooptions = {
             componentRestrictions: {'country': 'tw'}
             };
             
             var autocomplete_searchaddr = new google.maps.places.Autocomplete($('.searchaddr'), autooptions);
             autocomplete_searchaddr.addListener('place_changed', function () {
             var place = autocomplete_addrfrom.getPlace();
             if (!place.geometry) {
             return;
             } else {
             lat = place.geometry.location.lat();
             lng = place.geometry.location.lng();
             origin_place_id = place.place_id;
             }
             });
             */
        },
        /*
         * 設定搜尋範圍
         */
        set_search_radius: function (radius) {
            bounce_circle_radius = radius;
            on_bounce_marker_drop();
            on_bounce_marker_drop_rt();
        },
        /*
         * 返回使用者目前位置
         */
        center_to_user_location: function () {
            google_map.set_center_to_user_location();
            bounce_marker.setPosition(new google.maps.LatLng(google_map.get_user_lat(), google_map.get_user_lon()));
            on_bounce_marker_drop();
            on_bounce_marker_drop_rt();
        },
        /*
         * 以地址搜尋附近商店
         */
        search_address: function (address) {

            geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                'address': address
            }, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    var map_options = {
                        center: results[0].geometry.location
                    };

                    map.setCenter(results[0].geometry.location);
                    bounce_marker.setPosition(results[0].geometry.location);
                    on_bounce_marker_drop();
                }
            });
        },
        /*
         更新經緯度
         */
        set_new_latlng: function (newlat, newlng) {
            if (!newlat) {
                newlat = $('.newlat')[0].innerText;
                rt_newlat = $('.newlat')[0].innerText;
            }
            if (!newlng) {
                newlng = $('.newlng')[0].innerText;
                rt_newlat = $('.newlng')[0].innerText;
            }
            $('.lat').val(newlat);
            $('.lng').val(newlng);

            $('.rt_lng').val(rt_newlat);
            $('.rt_lng').val(rt_newlat);
        },
        set_center: function (newlat, newlng) {
            var newlatlng = new google.maps.LatLng(newlat, newlng);
            map.panTo(newlatlng);
            bounce_marker.setPosition(newlatlng);

            var newlatlng_rt = new google.maps.LatLng(rt_newlat, rt_newlat);
            bounce_marker_rt.setPosition(newlatlng_rt);

            on_bounce_marker_drop();
            on_bounce_marker_drop_rt();
        },
        set_office_marker: function (lat, lng) {
            bounce_marker.setMap(null);
            var newlatlng = new google.maps.LatLng(lat, lng);
            office_marker.setPosition(newlatlng);
            office_marker.setMap(map);
            map.panTo(newlatlng);
        },
        set_tile_map: function (plan_id, bl_lat, bl_lng, tr_lat, tr_lng) {

            if (maptiler) {
                map.overlayMapTypes.setAt(0, null);
            }
            if (!plan_id)
            {
                return;
            }
            var tile_map_url = base_url + "map/tile";
            load_tile_map(tile_map_url, plan_id, bl_lat, bl_lng, tr_lat, tr_lng);
        }
    };
}();

var googlemap_callback = area_select_map.googlemap_callback;


/*
 * 互動操作
 */

$(document).ready(function () {
    $('.range').change(function () {
        var radius = parseInt($(this).val());
        area_select_map.set_search_radius(radius);
    });
    $('.searchaddr').change(function () {
        var radius = $(this).val();
        area_select_map.search_address(radius);
    });

    $('.lat').change(function () {
        var nlat = $(this).val();
        var nlng = $('.lng').val();
        area_select_map.set_center(nlat, nlng);
    });
    $('.lng').change(function () {
        var nlat = $('.lat').val();
        var nlng = $(this).val();
        area_select_map.set_center(nlat, nlng);
    });
});
