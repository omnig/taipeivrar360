/* 
 * 呼叫API取得縣市輸入欄位資訊
 */

var city_area = function () {
    var api_base_url = '/';
    var city_select;
    var area_select;
    var zip_input;
    var city_area_obj;

    //取得行政區資訊
    function get_city_area(params) {
        var api_url = params.api_base_url + "api/get_city_area";

        $.ajax({
            url: api_url,
            data: '',
            dataType: 'JSON',
            success: function (data) {
                if (data.result) {
                    city_area_obj = data.data;
                    ajax_city(params);
                }
            }
        });
    }

    function ajax_city(params) {
        var html = "";
        var obj = city_area_obj;
        for (var i in obj) {
            var selected = (params.c_id === i) ? "selected" : "";
            html += "<option value='" + i + "' " + selected + ">" + obj[i].c_name + "</option>";
        }

        params.city_select.html(html);
        ajax_area(params);

    }

    function ajax_area(params) {
        var c_id = params.city_select.val();
        var html = "";
        var obj = city_area_obj;
        for (var i in obj[c_id].area) {
            var selected = (params.a_id === i) ? "selected" : "";
            html += "<option value='" + i + "' " + selected + ">" + obj[c_id].area[i].a_name + "</option>";
        }
        params.area_select.html(html);
        on_area_change(params);

    }

    //選取城市
    function on_city_change(params) {
        var c_id = params.city_select.val();
        var html = "";
        var obj = city_area_obj;
        for (var i in obj[c_id].area) {
            html += "<option value='" + i + "'>" + obj[c_id].area[i].a_name + "</option>";
        }
        params.area_select.html(html);
        on_area_change(params);
    }

    //選取地區
    function on_area_change(params) {
        var obj = city_area_obj;
        var c_id = params.city_select.val();
        var a_id = params.area_select.val();
        var zip = obj[c_id].area[a_id].a_zip;

        params.zip_input.val(zip);
    }


    return {
        init: function (base_url, c_id, a_id, selector) {

            var params = {
                api_base_url: base_url,
                city_select: $(selector).find('.city'),
                area_select: $(selector).find('.area'),
                zip_input: $(selector).find('.zip'),
                c_id: c_id,
                a_id: a_id
            };

            get_city_area(params);

            params.city_select.change(function () {
                on_city_change(params);
            });
            params.area_select.change(function () {
                on_area_change(params);
            });

        }
    };
}();
