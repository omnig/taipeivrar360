/*
 * 以ajax方式讀取商品清單
 */
var product_qa = function () {
    var base_url = '/';
    var href_base_url = '/';
    var container = 'body';
    var where;      //過濾條件(含目前顯示頁數p，會轉成get參數)
    var order;      //排序方式
    var product_qa;   //載入後的商品清單
    var i18n;       //多國語言文字
    var autoload;   //是否自動載入
    var s_id;   //商店id

    /*
     * 全部重置
     * @returns {undefined}
     */
    function reset_all() {
        where = {
            p: 1,
            limit: 10,
            query: 7
        };
        order = 0;
        product_qa = {};
        autoload = true;
    }

    /*
     * 重新、從頭載入
     * @returns {undefined}
     */
    function reload() {
        where.p = 1;
        product_qa = {};
    }

    /*
     * 顯示載入中
     * @returns {undefined}
     */
    function show_loading() {
        var loading_html = "<div class='loading text-center c-theme-color'><i class='fa fa-spinner fa-spin'></i> loading...</div>";

        $(container).append(loading_html);
    }

    /*
     * 移除載入中
     * @returns {undefined}
     */
    function hide_loading() {
        $(container).find('.loading').remove();
    }

    /*
     * 建立row單一商品的html
     * @param {object} product
     * @returns {String}
     */
    function build_non_data() {
        var html =
                "<div class='row' style='text-align:center'>" +
                "   <i class='fa fa-search'> " + i18n["查詢無紀錄"] + "</i>" +
                "</div>";
        return html;
    }

    function build_product_qa_list_title() {
        var html =
                "<thead>" +
                "   <tr>" +
                "       <td class='align_left'></td>" +
                "       <td class='align_center hidden-sm hidden-xs'>" + i18n["商品"] + "</td>" +
                "       <td class='align_center'>" + i18n["問題"] + "</td>" +
                "       <td class='align_center hidden-sm hidden-xs'>" + i18n["提問時間"] + "</td>" +
                "       <td class='align_center'>" + i18n["回覆狀態"] + "</td>" +
                "       <td class='align_center hidden-sm hidden-xs'>" + i18n["操作"] + "</td>" +
                "   </tr>" +
                "</thead>";
        return html;
    }

    function build_product_qa_list_html(product) {

        var html =
                "<tr style='font-size:14px'>" +
                "   <td class='align_left' scope='row'>" +
                "       <a href='" + href_base_url + "product/" + product.p_id + "'><img width='70px' src='" + product.p_image + "' />" +
                "   </td>" +
                "   <td class='align_left hidden-sm hidden-xs'>" +
                "       <a href='" + href_base_url + "product/" + product.p_id + "'>" + product.p_name + "</a>" +
                "   </td>" +
                "   <td class='align_left'>" + product.pf_question + "   </td>" +
                "   <td class='align_center hidden-sm hidden-xs' style='width:15%'>" +
                product.pf_enabled + product.pf_create_timestamp +
                "   </td>" +
                "   <td class='align_center' style='width:15%'>" +
                product.pf_answer_status +
                "   </td>" +
                "   <td class='align_center hidden-sm hidden-xs' style='width:15%'>" +
                "       <a href='" + href_base_url + "product/" + product.p_id + "?q=true#tab-3'>" + i18n["我要留言"] + "</a>" + "<br>" +
                "       <a href='" + href_base_url + "product/" + product.p_id + "#tab-3'>" + i18n["查看全部"] + "</a>" +
                "   </td>" +
                "</tr>";

        return html;
    }

    /*
     * 整理從server端取回的資料
     * @param {object} data
     * @returns {undefined}
     */
    function product_qa_list_parser(data) {

        if (data.length === 0) {
            var html = build_non_data();
            $(container).append(html);
        } else {
            //var title = build_product_qa_list_title();
            //$(container).append(title);
        }

        for (var i in data) {
            var id = 'pf_id_' + data[i].pf_id;

            //過濾重複的資料不顯示
            if (typeof product_qa[id] === 'undefined') {
                product_qa[id] = data[i];

                //顯示商品清單
                var html = build_product_qa_list_html(data[i]);
                $(container).append(html);
            }
        }
    }

    /*
     * 顯示載入下一頁的按鈕
     * @returns {undefined}
     */
    function show_load_next_page_btn() {
        var btn_html =
                "<div class='load_next_page_btn text-center c-theme-color'>" +
                "   <button type='button' class='btn btn-success fa fa-arrow-circle-down' onclick='product_qa.load_next_page()'> More</button>" +
                "</div>";

        $(container).append(btn_html);

        //自動載入下一頁資料
        if (autoload) {
            $(window).scroll("autoload", function () {
                if ($('.load_next_page_btn').length > 0) {
                    var scroll_pos = $(window).scrollTop();
                    var window_height = $(window).height();
                    var btn_position = $('.load_next_page_btn').position().top;

                    //剩半個螢幕高就要捲完時，就載入下一批
                    if (btn_position < scroll_pos + window_height * 1.5) {
                        $(window).off("scroll", "autoload");
                        $(container).find(".load_next_page_btn button").click();
                    }
                }
            });
        }
    }

    /*
     * 移除載入下一頁按鈕
     * @returns {undefined}
     */
    function hide_load_next_page_btn() {
        $(container).find('.load_next_page_btn').remove();
    }

    /*
     * ajax查詢
     * @returns {undefined}
     */
    function ajax_go() {
        var url = base_url + 'ajax/product_qa?sort=' + order;

        hide_load_next_page_btn();
        show_loading();

        if (typeof s_id !== 'undefined' && s_id) {
            where.s_id = s_id;
        }

        $.ajax({
            url: url,
            data: where,
            dataType: 'JSON',
            success: function (ret) {
                if (ret.result) {
                    
                    //顯示table標題列
                    if (ret.data.pagination.current_page === "1") {
                        var title = build_product_qa_list_title();
                        $(container).append(title);
                    }
                    //顯示商品Q&A清單
                    product_qa_list_parser(ret.data.products);

                    //底部顯示繼續載入按鈕
                    if (ret.data.pagination.current_page < ret.data.pagination.total_page) {
                        show_load_next_page_btn();
                    }
                }
                hide_loading();
            }
        });
    }

    return {
        /*
         * 初始化
         * @param {string} base: 網站base_url
         * @param {string} href_base: 跳轉網址的base_url
         * @param {string} container_selector: 放置html的selector
         * @param {object} init_i18n: 初始化i18n字串
         * @returns {undefined}
         */
        init: function (base, href_base, container_selector, init_i18n) {
            base_url = base;
            href_base_url = href_base;
            i18n = init_i18n;
            container = container_selector;
            reset_all();
        },
        /*
         * 重置條件
         * @returns {undefined}
         */
        clear: function () {
            reload();
            $(container).html("");
        },
        /*
         * 設定過濾條件
         * @param {string} key
         * @param {string} value
         * @returns {undefined}
         */
        set_where: function (key, value) {
            if (value) {
                where[key] = value;
            }
        },
        /*
         * 設定排序規則
         * @param {string} value
         * @returns {undefined}
         */
        set_sort: function (value) {
            order = value;
        },
        /*
         * 設定查詢天數
         * @param {string} value
         * @returns {undefined}
         */
        set_query: function (value) {
            where.query = value;
        },
        /*
         * 設定查詢天數
         * @param {string} value
         * @returns {undefined}
         */
        set_status: function (value) {
            where.status = value;
        },
        /*         
         * 設定每頁筆數規則
         * @param {string} value
         * @returns {undefined}
         */
        set_limit: function (value) {
            where.limit = value;
        },
        /*
         * 設定是否自動載入
         * @param {bool} value
         * @returns {undefined}
         */
        set_autoload: function (value) {
            autoload = value;
        },
        /*
         * 查詢
         * 傳入參數:
         * where: 過濾條件
         * order: 排序
         * limit: 每頁數量
         */
        load: function () {
            ajax_go();
        },
        /*
         * 查詢並載入下一頁
         */
        load_next_page: function () {
            where.p++;
            ajax_go();
        }
    };
}();