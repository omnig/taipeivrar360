/*
 * 檢查帳號是否存在
 */

var account_check = function () {
    var base_url;

    return {
        /*
         * 初始化
         */
        init: function (init_url) {
            base_url = init_url;

            $('.account_check').keyup(function () {
                account_check.check_account($(this).val());
            });
        },
        /*
         * 檢查帳號是否存在
         */
        check_account: function (account) {
            var url = base_url + 'ajax/check_account';
            var params = {
                account: account
            };

            $.ajax({
                url: url,
                data: params,
                dataType: 'JSON',
                success: function (data) {
                    if (data.result) {
                        $('.account_check_result').html('');
                    } else {
                        $('.account_check_result').html(data.error_message);
                    }
                }
            });
        }
    };
}();