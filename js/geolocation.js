/*
 * 取得座標
 */
/* global dialog */

var user_location = function () {
    var callback_function;
    var regular_update_timeout;

    /*
     * 將經緯度寫入cookie
     * 
     * @param {float} lat
     * @param {float} lon
     * @returns {undefined}
     */
    function write_cookie(lat, lon) {
        var expire_time = new Date();
        var minutes = 3;
        expire_time.setTime(expire_time.getTime() + (minutes * 60 * 1000));

        $.cookie("user_location", lat + ',' + lon, {expires: expire_time});
    }

    /*
     * 取得座標後callback處理
     *  
     * @param {type} position
     * @returns {undefined}
     */
    function positionCallback(position) {
        var lat = position.coords.latitude;
        var lon = position.coords.longitude;

        result_user_location(lat, lon, "success");
    }

    function errorCallback() {
        //預設位置在台北車站
        var lat = 25.0477505;
        var lon = 121.5170599;

        result_user_location(lat, lon, "error");
    }

    function result_user_location(lat, lon, type) {

        if (type === "error") {
            dialog.alert("提示", "系統無法取得您的位置，將預設位置於台北車站。", 3000);
        } else {
            dialog.set_size("sm");
            dialog.alert("提示", "定位中，請稍候", 2000);
        }

        var latlon = {lat: lat, lon: lon};

        write_cookie(lat, lon);

        callback_function(latlon);
    }

    return {
        /*
         * 取得使用者座標
         * 
         * @param {function} callback
         * @param {string} waiting_message
         * @returns {undefined}
         */
        get_user_latlon: function (callback, waiting_message) {
            var loc = $.cookie("user_location");

            if (typeof loc === 'undefined') {
                //未曾取得使用者座標
                callback_function = callback;

                if (typeof $.blockUI !== 'undefined') {
                    //$.blockUI(waiting_message);
                }
                navigator.geolocation.getCurrentPosition(positionCallback, errorCallback);
            } else {
                //已取得過座標
                var latlon_array = loc.split(",");
                var latlon = {lat: latlon_array[0], lon: latlon_array[1]};

                callback(latlon);
            }
        },
        /*
         * 定期更新使用者座標，傳入毫秒，為時間間隔
         * 
         * @param {int} interval
         * @returns {undefined}
         */
        regular_update_user_latlon: function (interval) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var lat = position.coords.latitude;
                var lon = position.coords.longitude;

                var latlon = {lat: lat, lon: lon};
                $.cookie("user_location", lat + ',' + lon);

                if (typeof interval !== 'undefined') {
                    if (typeof regular_update_timeout !== 'undefined') {
                        clearTimeout(regular_update_timeout);
                    }

                    regular_update_timeout = setTimeout(function () {
                        user_location.regular_update_user_latlon(interval);
                    }, interval);
                }
            });
        },
        /*
         * 停止定期更新使用者座標
         */
        stop_regular_update_user_latlon: function () {
            clearTimeout(regular_update_timeout);
        }
    };
}();

//取得靜態地圖
function get_static_map(map_type, lat, lon, zoom, width, height) {
    if (map_type === 'OSM') {
        return "http://staticmap.openstreetmap.de/staticmap.php?center=" + lat + "," + lon + "&markers=" + lat + "," + lon + "&zoom=" + zoom + "&size=" + width + "x" + height + "&maptype=mapnik";
    }

    return "https://maps.googleapis.com/maps/api/staticmap?center=" + lat + "," + lon + "&zoom=" + zoom + "&size=" + width + "x" + height + "&maptype=roadmap&markers=color:blue%7Clabel:S%7C" + lat + "," + lon;
}

function get_static_map_circle(lat, lng, rad, width, height, g_key) {

    var url = "https://maps.googleapis.com/maps/api/staticmap?center=" + lat + "," + lng + "&size=" + width + "x" + height + "&maptype=roadmap&markers=color:blue%7Clabel:G%7C" + lat + "," + lng;
    url += "&path=color:0xff660080|weight:1|fillcolor:0xff660033";
    var staticMapSrc = "";

    var r = 6371;
    var pi = Math.PI;

    var _lat = (lat * pi) / 180;
    var _lng = (lng * pi) / 180;
    var d = (rad / 1000) / r;

    var i = 0;
    var detail = 20;

    for (i = 0; i <= 360; i += detail) {
        var brng = i * pi / 180;

        var pLat = Math.asin(Math.sin(_lat) * Math.cos(d) + Math.cos(_lat) * Math.sin(d) * Math.cos(brng));
        var pLng = ((_lng + Math.atan2(Math.sin(brng) * Math.sin(d) * Math.cos(_lat), Math.cos(d) - Math.sin(_lat) * Math.sin(pLat))) * 180) / pi;
        pLat = (pLat * 180) / pi;

        staticMapSrc += "|" + pLat + "," + pLng;
    }

    return url + staticMapSrc + '&key=' + g_key;
}