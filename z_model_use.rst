//$this->Common_model->get_one

$where_array = array(
    "{$data["field_prefix"]}_account" => $input_array["{$data["field_prefix"]}_account"]
);

$duplicated = $this->Common_model->get_one($data['table_name'], $where_array);
if ($duplicated) {
    _alert("{$this->lang->line("重複的")}{$data["item_name"]}");
    exit();
}



$this->readonly_db->join('ap_floors AS f', 'b.af_id=f.af_id', 'LEFT');

$this->load->model($model_name);
$this->$model_name->get_list();

$update_array = array(
    "{$data["field_prefix"]}_enabled" => 'N'
);
$this->Common_model->update_db($data["table_name"], $update_array, $where_array);

// ci 
$ci = & get_instance();

//user session
$this->session->access_token['access_token']


//設定API 回傳代碼
http_response_code(400);



//計算距離
SELECT 
  name, 
   ( 6371 * acos( cos( radians(42.290763) ) * cos( radians( locations.lat ) ) 
   * cos( radians(locations.lng) - radians(-71.35368)) + sin(radians(42.290763)) 
   * sin( radians(locations.lat)))) AS distance 
FROM locations 
WHERE active = 1 
HAVING distance < 10 
ORDER BY distance;


SELECT
	t_name,( 6371 * acos( cos( radians(24.99398817241575) ) * cos( radians( ty_topic.t_lat ) ) 
   * cos( radians(ty_topic.t_lng) - radians(121.30027696413163)) + sin(radians(24.99398817241575)) 
   * sin( radians(ty_topic.t_lat)))) AS distance ,
   t_lat,
   t_lng
FROM ty_topic
HAVING distance < 10 
ORDER BY distance;


字數顯示點點點
mb_substr($row->{"{$data["field_prefix"]}_describe"}, 0, 25)."...";,


//0314
ALTER TABLE `ty_department_ar` ADD `share_other` ENUM('Y','N') NOT NULL DEFAULT 'N' AFTER `tdar_state`;


ssl


openssl x509 -in vr-server.cer  -inform DER -out server.crt


1.
cd /etc/apache2/
vim apache2.conf
在最后一行加上

LoadModule ssl_module /usr/lib/apache2/modules/mod_ssl.so


2. (可審略)
sudo vim tyvr360.conf
# 自动将80端口的访问,重定向到https的域名
    Redirect permanent / https://safe.example.com

3.
vim ports.conf

<IfModule ssl_module>
        Listen 443
        <VirtualHost _default_:443>
            ServerName vr.tycg.gov.tw
            # 此处为网站根目录
            DocumentRoot /var/www/vrar360_web_front/dist

            ErrorLog ${APACHE_LOG_DIR}/ty_vr_ssl_error.log
            CustomLog ${APACHE_LOG_DIR}/ty_vr_ssl_access.log combined

            <Directory /var/www/vrar360_web_front/dist>
               # Options FollowSymLinks
               # AllowOverride All
               # Order allow,deny
               # allow from all
              RewriteEngine on

              # Don't rewrite files or directories
              RewriteRule ^index\.html$ - [L]
              RewriteCond %{REQUEST_FILENAME} -f [OR]
              RewriteCond %{REQUEST_FILENAME} -d
              RewriteRule ^ - [L]

              # Rewrite everything else to index.html
              # to allow html5 state links
              RewriteRule ^ index.html [L]
            </Directory>

            SSLEngine On
            # 证书文件的路径(需要自己申请,或找IT部门的同事要)
            SSLCertificateFile "/etc/apache2/cert/vr-server.cer"
            # key文件的路径(需要自己申请,或找IT部门的同事要)
            SSLCertificateKeyFile "/etc/apache2/cert/vr-server.key"
        </VirtualHost>
</IfModule>

<IfModule ssl_module>
        Listen 443
        <VirtualHost vr-manage.tycg.gov.tw:443>
            ServerName vr-manage.tycg.gov.tw
            # 此处为网站根目录
            DocumentRoot /var/www/vrar360

            ErrorLog ${APACHE_LOG_DIR}/ty_vr_ssl_error.log
            CustomLog ${APACHE_LOG_DIR}/ty_vr_ssl_access.log combined

            <Directory /var/www/vrar360>
                Options FollowSymLinks MultiViews
                AllowOverride All
                Order allow,deny
                allow from all
            </Directory>

            SSLEngine On
            # 证书文件的路径(需要自己申请,或找IT部门的同事要)
            SSLCertificateFile "/etc/apache2/cert/server.crt"
            # key文件的路径(需要自己申请,或找IT部门的同事要)
            SSLCertificateKeyFile "/etc/apache2/cert/vr-server.key"
            SSLCertificateChainFile "/etc/apache2/cert/GRCA1_5_GCA2.crt"
        </VirtualHost>
</IfModule>

<IfModule ssl_module>
        Listen 443
        <VirtualHost vr-api.tycg.gov.tw:443>
            ServerName vr-api.tycg.gov.tw
            # 此处为网站根目录
            DocumentRoot /var/www/vrar360_api

            ErrorLog ${APACHE_LOG_DIR}/ty_vr_ssl_error.log
            CustomLog ${APACHE_LOG_DIR}/ty_vr_ssl_access.log combined

            <Directory /var/www/vrar360_api>
                Options FollowSymLinks MultiViews
                AllowOverride All
                Order allow,deny
                allow from all
            </Directory>

            SSLEngine On
            # 证书文件的路径(需要自己申请,或找IT部门的同事要)
            SSLCertificateFile "/etc/apache2/cert/server.crt"
            # key文件的路径(需要自己申请,或找IT部门的同事要)
            SSLCertificateKeyFile "/etc/apache2/cert/vr-server.key"
            SSLCertificateChainFile "/etc/apache2/cert/GRCA1_5_GCA2.crt"
        </VirtualHost>
</IfModule>

4.
$ apachectl configtest # 检查apache配置是否正确
$ sudo service apache2 reload # 检查配置正确后,重新加载配置
$ sudo a2enmod ssl # 启用SSL服务
$ sudo service apache2 restart #重启apache




單登原始參數
{
  "/sa/login": "",
  "_ga": "GA1.1.836972624.1521715857",
  "G_AUTHUSER_H": "0",
  "RE5EOERHSDJNVDdWWkdCTU5CUFA_fit": "1522462636749",
  "RE5EOERHSDJNVDdWWkdCTU5CUFA_fs": "eyJiYSI6MTUyMjYzOTk5MTIwNywiYmMiOi0xLCJldmVudENvdW50ZXIiOjAsInB1cmNoYXNlQ291bnRlciI6MCwiZXJyb3JDb3VudGVyIjowLCJ0aW1lZEV2ZW50cyI6W119",
  "RE5EOERHSDJNVDdWWkdCTU5CUFA_flp": "1522639996211",
  "_hp2_id_2213510609": "{\"userId\":\"4309630426858033\",\"pageviewId\":\"1092998358006753\",\"sessionId\":\"5512330343759054\",\"identity\":null,\"trackerVersion\":\"3.0\"}",
  "ci_session": "5b7716c5e481ddbaaa1b11abc67551b2e408fadf"
}

