<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'NMC' => array($vendorDir . '/nmcteam/image-with-text/src'),
    'HTTP_Request2' => array($vendorDir . '/pear/http_request2'),
    'Google_' => array($vendorDir . '/google/apiclient/src'),
);
