#!/bin/bash
#10 4 * * * sh /var/www/vrar360/scripts/couchbase_bk_production.sh
#mkdir /var/www/vrar360/couchbase/
TIMESTAMP=`date +%w`


USER_NAME="Administrator"
USER_PASSWORD="123456"
BACKUPS_DIR="/var/www/vrar360/couchbase/"
DB_NAME="anyplace"

cd $BACKUPS_DIR
/opt/couchbase/bin/cbbackup http://localhost:8091 $BACKUPS_DIR -u $USER_NAME -p $USER_PASSWORD -b $DB_NAME -m diff
