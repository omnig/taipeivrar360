#!/bin/bash
#0 4 * * * sh /var/www/vrar360/scripts/mysql_bk_production.sh
#sudo mkdir /var/www/vrar360/sql/
TIMESTAMP=`date +%w`


MYSQL_HOST="172.25.160.25"
USER_NAME="ogadmin_server"
USER_PASSWORD="og!QAZ2wsx"
BACKUPS_DIR="/var/www/taipeivrar360/sql/"
DB_NAME="taipeivrar360"

cd $BACKUPS_DIR
mysqldump  --user=$USER_NAME --host=$MYSQL_HOST --password=$USER_PASSWORD  $DB_NAME --ignore-table=$DB_NAME.tp_ci_sessions  --ignore-table=$DB_NAME.tp_log_api --ignore-table=$DB_NAME.tp_log_locapi > mysqldump-$TIMESTAMP.sql
chown www-data:www-data mysqldump-$TIMESTAMP.sql
